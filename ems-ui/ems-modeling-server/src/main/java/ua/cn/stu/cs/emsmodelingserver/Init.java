package ua.cn.stu.cs.emsmodelingserver;

import java.io.IOException;
import ua.cn.stu.cs.ems.hlamodelingserver.HlaModelingServer;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Leonid Tatarchuk <tatarchuk90@tut.by>
 */
public class Init extends HttpServlet {

    @Override
    public void init(ServletConfig _config) throws ServletException {
        super.init(_config);
        try {
            doGet(null, null);
            ServletContext context = _config.getServletContext();
            String home = context.getRealPath("");
            String fileSeparator = System.getProperty("file.separator");

            System.setProperty("java.net.preferIPv4Stack", "true");
            System.setProperty("rti.home", home
                    + fileSeparator + "WEB-INF");
            System.setProperty("RTI_RID_FILE", home + fileSeparator + "WEB-INF"
                    + fileSeparator + "RTI.rid");
            HlaModelingServer.main(new String[]{});
        } catch (IOException e) {
            System.out.println("Error when initialize: " + e);
        }
    }

    @Override
    public void destroy() {
        super.destroy();
        HlaModelingServer.destroy();
        System.out.println("Init Servlet Destroy");
    }

    @Override
    public void doGet(HttpServletRequest _request, HttpServletResponse _response)
            throws IOException, ServletException {
    }
}