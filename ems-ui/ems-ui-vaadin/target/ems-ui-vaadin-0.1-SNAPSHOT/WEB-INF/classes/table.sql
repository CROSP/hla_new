CREATE TABLE models
(
  id bigserial NOT NULL,
  name character varying(128) NOT NULL,
  data text NOT NULL,
  type character varying(32) NOT NULL,
  CONSTRAINT models_pkey PRIMARY KEY (id )
)
WITH (
  OIDS=FALSE
);