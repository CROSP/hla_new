package ua.cn.stu.cs.ems.ui.vaadin.ui.experiment;

import com.vaadin.data.Property;
import com.vaadin.data.Validator;
import com.vaadin.ui.AbstractField;
import com.vaadin.ui.TextField;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import ua.cn.stu.cs.ems.ui.vaadin.ui.experiment.ExperimentSettings.FactorEntry;
import ua.cn.stu.cs.ems.ui.vaadin.ui.input.table.CachingFieldCreator;
import ua.cn.stu.cs.ems.ui.vaadin.ui.input.table.CachingInputTable;
import ua.cn.stu.cs.ems.ui.vaadin.ui.input.table.ConcurrentFieldCreator;
import ua.cn.stu.cs.ems.ui.vaadin.validation.AbstractValidator;
import ua.cn.stu.cs.ems.ui.vaadin.validation.EmptyStringValidator;
import ua.cn.stu.cs.ems.ui.vaadin.validation.ValidatorFactory;

/**
 * @author n0weak
 */
class FactorsEditor extends CachingInputTable<ExperimentSettings.FactorEntry> {

    private static final String WIDTH = "417px";
    private final Collection<String> availableVars;
    private final FactorFieldCreator fieldCreator = new FactorFieldCreator();
    private final static String COLUMN_IDS[] = {"variable", "begin", "end", "step", ""};

    public FactorsEditor(String caption, Collection<String> availableVars,
                         Collection<ExperimentSettings.FactorEntry> entries) {
        super(ExperimentSettings.FactorEntry.class, caption);
        
        this.availableVars = availableVars;
        
        setWidth(WIDTH);
        initContainer(entries);
        initFactory(fieldCreator);
        setRequiredFields(COLUMN_IDS[0]);
        setVisibleColumns(COLUMN_IDS);
        this.setColumnExpandRatio(COLUMN_IDS[0], 3.0f);
        this.setColumnExpandRatio(COLUMN_IDS[1], 1.0f);
        this.setColumnExpandRatio(COLUMN_IDS[2], 1.0f);
        this.setColumnExpandRatio(COLUMN_IDS[3], 1.0f);
        this.setColumnExpandRatio(COLUMN_IDS[4], 0.7f);

        addValidator(new Validator() {

            public void validate(Object o) throws InvalidValueException {
                if (!isValid(o)) {
                    throw new InvalidValueException("At least one factor must be set!");
                }
            }

            public boolean isValid(Object o) {
                AbstractField af = getFieldCreator().getField(COLUMN_IDS[0], getLast());
                return af != null && af.isValid();
//                return getInnerList().size() > 0; //the list always contains at least one empty stub item
            }
        });
        expandAllowed = false;
    }
    
    public void selectItem(String id, String value) {
        for (FactorEntry sel: getInnerList()) {
            if (sel != null && sel.getVariable() != null && sel.getVariable().equals(id)) {
                return; // we dont need identical IDs
            };
        }
        expandAllowed = false;
        AbstractField varname = getFieldCreator().getField(COLUMN_IDS[0], getLast());
        expandAllowed = false;
        AbstractField varvalue = getFieldCreator().getField(COLUMN_IDS[1], getLast());
        if (null != varname && null != varvalue) {
            varname.setValue(id);
            varvalue.setValue(value);
        } else {
            // throw exception!!!!
        }
        expandAllowed = false;
    }
    
    public Set<FactorEntry> getSelectedFactors() {
        return new HashSet<FactorEntry>(getInnerList());
    }

    @Override
    public CachingFieldCreator<ExperimentSettings.FactorEntry> getFieldCreator() {
        return fieldCreator;
    }

    private class FactorFieldCreator extends ConcurrentFieldCreator<ExperimentSettings.FactorEntry> implements
            ExpandListener<ExperimentSettings.FactorEntry> {

        public FactorFieldCreator() {
            super("variable");
            initCache("begin", "end", "step");
            setExpandListener(this);
        }

        public AbstractField createNewField(final ExperimentSettings.FactorEntry item, Object propertyId) {
            TextField f = new TextField();
            if ("variable".equals(propertyId)) {
                f.setWidth("100%");
                f.addValidator(new EmptyStringValidator());
            }
            else if ("begin".equals(propertyId) || "end".equals(propertyId) || "step".equals(propertyId)) {
                f.setInvalidCommitted(true);
                f.setWidth("100%");
 
                if ("step".equals(propertyId)) {
                    f.addValidator(ValidatorFactory.getPositiveDoubleValidator());
                    f.addValidator(new StepValueValidator(item));
                }

                if ("begin".equals(propertyId) || "end".equals(propertyId)) {
                    f.addValidator(ValidatorFactory.getDoubleValidator());
                    f.addListener(new RepaintingListener("step", item));
                }

                if ("begin".equals(propertyId)) {
                    f.addValidator(new BeginValueValidator(item));
                    f.addListener(new RepaintingListener("end", item));
                }

                if ("end".equals(propertyId)) {
                    f.addListener(new RepaintingListener("begin", item));
                }
            }

            return f;
        }

        public void tableExpanded(ExperimentSettings.FactorEntry newItem) {
            String[] fields = new String[]{"begin", "end", "step"};
            for (String field : fields) {
                ValidatorFactory.addRequiredValidator(getField(field, newItem));
            }
        }

        private class RepaintingListener implements ValueChangeListener {

            private final ExperimentSettings.FactorEntry item;
            private final String fieldName;

            private RepaintingListener(String fieldName, ExperimentSettings.FactorEntry item) {
                this.fieldName = fieldName;
                this.item = item;
            }

            public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
                expandAllowed = false;
                getCache(fieldName).get(item).requestRepaint();
            }
        }
    }

    private static class BeginValueValidator extends AbstractValidator {

        private final ExperimentSettings.FactorEntry factor;

        private BeginValueValidator(ExperimentSettings.FactorEntry factor) {
            super("Begin value must be lower than end one");
            this.factor = factor;
        }

        public boolean isValid(Object o) {
            Double begin;
            Double end;
            try {
                begin = Double.parseDouble(String.valueOf(o));
                end = Double.parseDouble(factor.getEnd());
            } catch (Exception e) {
                return true;
            }

            return begin < end;
        }
    }

    private static class StepValueValidator extends AbstractValidator {

        private final ExperimentSettings.FactorEntry factor;

        private StepValueValidator(ExperimentSettings.FactorEntry factor) {
            super("Step is too big");
            this.factor = factor;
        }

        public boolean isValid(Object o) {
            Double begin;
            Double end;
            Double step;
            try {
                step = Double.parseDouble(String.valueOf(o));
                begin = Double.parseDouble(factor.getStep());
                end = Double.parseDouble(factor.getEnd());

            } catch (Exception e) {
                return true;
            }

            return end - begin >= step;
        }
    }
}
