package ua.cn.stu.cs.ems.ui.vaadin.ui;

import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import ua.cn.stu.cs.ems.ui.vaadin.dto.GlobalSettings;

/**
 * @author n0weak
 */
class UISettingsForm extends AbstractForm {

    private static final String WIDTH = "400px";

    UISettingsForm(GlobalSettings.UISettings settings) {
        super(WIDTH, new BeanItem<GlobalSettings.UISettings>(settings), new UISettingsFiledFactory());
    }

    private static class UISettingsFiledFactory extends DefaultFieldFactory {

        @Override
        public Field createField(Item item, Object propertyId, Component uiContext) {
            Field f = super.createField(item, propertyId, uiContext);
            if ("animationSetupDialogShown".equals(propertyId)) {
                CheckBox cb = (CheckBox) f;
                cb.setCaption("Show animation settings dialog before each run");
            }
            if ("validateModelsOnSaving".equals(propertyId)) {
                CheckBox cb = (CheckBox) f;
                cb.setCaption("Validate workspaces before saving");
            }

            return f;
        }
    }
}
