package ua.cn.stu.cs.ems.ui.vaadin.ui;

import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import ua.cn.stu.cs.ems.ui.vaadin.dto.GlobalSettings;

/**
 * @author n0weak
 */
class CanvasSettingsForm extends AbstractForm {

    private static final String WIDTH = "400px";

    CanvasSettingsForm(GlobalSettings.CanvasSettings settings) {
        super(WIDTH, new BeanItem<GlobalSettings.CanvasSettings>(settings), new CanvasSettingsFiledFactory());
    }

    private static class CanvasSettingsFiledFactory extends DefaultFieldFactory {

        @Override
        public Field createField(Item item, Object propertyId, Component uiContext) {
            Field f = super.createField(item, propertyId, uiContext);
            if ("gridAdjustEnabled".equals(propertyId)) {
                CheckBox cb = (CheckBox) f;
                cb.setCaption("Adjust components to the grid");
            }
            else if ("gridPainted".equals(propertyId)) {
                CheckBox cb = (CheckBox) f;
                cb.setCaption("Paint the grid");
            }
            else if ("pathCorrectionEnabled".equals(propertyId)) {
                CheckBox cb = (CheckBox) f;
                cb.setCaption("Enable enhanced connection mode (hold CTRL to toggle)");
            }
            else if ("horizontalLinesDrawnFirst".equals(propertyId)) {
                CheckBox cb = (CheckBox) f;
                cb.setCaption("Draw horizontal lines before vertical");
            }
            else if ("previewDrawn".equals(propertyId)) {
                CheckBox cb = (CheckBox) f;
                cb.setCaption("Draw previews");
            }
            return f;
        }
    }
}
