package ua.cn.stu.cs.ems.ui.vaadin.ui.experiment.report;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author proger
 */
public class GraphicResult {
    private double[] xValues;
    private List<Double[]> yValues = new ArrayList<Double[]>();
    
    private String xAxeName;
    private List<String> yAxesNames = new ArrayList<String>();
    
    private List<String> captions = new ArrayList<String>();
    
    public GraphicResult(double[] theXValues, String theXAxeName) {
        xValues = theXValues;
        xAxeName = theXAxeName;
    }
    
    public void addYValues(Double[] theYValues, String theYAxeName, String theGraphCaption) {
        assert theYValues.length == xValues.length;
        
        yValues.add(theYValues);
        yAxesNames.add(theYAxeName);
        captions.add(theGraphCaption);
    }

    /**
     * @return the xValues
     */
    public double[] getXValues() {
        return xValues;
    }

    /**
     * @return the yValues
     */
    public List<Double[]> getYValues() {
        return yValues;
    }

    /**
     * @return the xAxeName
     */
    public String getXAxeName() {
        return xAxeName;
    }

    /**
     * @return the yAxesNames
     */
    public List<String> getYAxesNames() {
        return yAxesNames;
    }

    /**
     * @return the caption
     */
    public List<String> getCaptions() {
        return captions;
    }
}
