package ua.cn.stu.cs.ems.ui.vaadin;

import com.github.wolfie.sessionguard.SessionGuard;
import com.vaadin.Application;
import com.vaadin.event.Action;
import com.vaadin.event.ShortcutAction;
import com.vaadin.service.ApplicationContext;
import com.vaadin.terminal.Terminal;
import com.vaadin.terminal.gwt.server.WebApplicationContext;
import com.vaadin.ui.*;
import com.vaadin.ui.MenuBar.MenuItem;

import java.util.Collection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaadin.artur.icepush.ICEPush;
import ua.cn.stu.cs.ems.core.aggregates.Aggregate;
import ua.cn.stu.cs.ems.core.aggregates.AggregateDefinition;
import ua.cn.stu.cs.ems.ui.vaadin.dto.AggregateVariables;
import ua.cn.stu.cs.ems.ui.vaadin.dto.GlobalSettings;
import ua.cn.stu.cs.ems.ui.vaadin.ui.*;
import ua.cn.stu.cs.ems.ui.vaadin.ui.components.*;
import ua.cn.stu.cs.ems.ui.vaadin.ui.experiment.ExperimentProgressWindow;
import ua.cn.stu.cs.ems.ui.vaadin.ui.experiment.ExperimentSettings;
import ua.cn.stu.cs.ems.ui.vaadin.ui.experiment.ExperimentSettingsWindow;
import ua.cn.stu.cs.ems.ui.vaadin.utils.UIUtils;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.JessCanvas;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.DrawingMode;

import ua.cn.stu.cs.ems.core.experiment.ExperimentReport;
import ua.cn.stu.cs.ems.core.utils.Coordinates;
import ua.cn.stu.cs.ems.ui.vaadin.ui.components.MainContent.WorkspaceCreatingEvent;
import ua.cn.stu.cs.ems.ui.vaadin.ui.experiment.report.ExperimentReportWindow;

/**
 * @author n0weak
 */
/* 
 * TODO mutiple session in one browser (in different allowed, this is VAADIN espacial)
 * Need testing from different computest width one version of browsers
 */
public class JessApp extends Application implements UIManager, ApplicationContext.TransactionListener {
// ------------------------------ FIELDS ------------------------------

    final static Logger logger = LoggerFactory.getLogger(JessApp.class);
    private static final String CAPTION = "EMS 2016";    //TODO hide buildNumber in production
    private Window window;
    private ICEPush pusher = new ICEPush(); /*Used for update client's side page from server.*/

    private ConcurrentButtonGroup modeSwitchers;
    private int scale = 100;
    private Button selectionModeBtn;
    private WidgetManager wm;
    private MainContent mainContent;
    private static ThreadLocal<JessApp> sharedApps = new ThreadLocal<JessApp>();
    public Console console;
    /**
     * Holders containing application settings
     */
    private GlobalSettings globalSettings;
    private ExperimentSettings settings = new ExperimentSettings();

// ------------------------ INTERFACE METHODS ------------------------
// --------------------- Interface ErrorListener ---------------------
    @Override
    public void terminalError(Terminal.ErrorEvent event) {
        if (event.getThrowable() != null) {
            event.getThrowable().printStackTrace();
            logger.debug("Terminal error caught: " + event.getThrowable().toString());
        } else {
            logger.debug("Terminal error: " + event.toString());
        }
    }

// --------------------- Interface UIManager ---------------------
    public void showSaveDialog(String defaultName, FileManager.FileType fileType,
            final PropertiesListener<SaveDialog.SaveRecord> propertiesListener) {
        Collection<String> listFiles = fileType == FileManager.FileType.MODEL
                ? wm.getSaveManager().listAllModels() : wm.getSaveManager().listAllAggregateTypes();
        SaveDialog sd = new SaveDialog(listFiles, fileType, defaultName, propertiesListener);
        window.addWindow(sd);
    }

    public void showOpenDialog(Collection<String> modelsFiles,
            Collection<String> aggTypesFiles,
            PropertiesListener<OpenDialog.OpenRecord> propertiesListener) {
        window.addWindow(new OpenDialog(modelsFiles, aggTypesFiles, propertiesListener, wm));
    }

// -------------------------- OTHER METHODS --------------------------
    @Override
    public void init() {
//        currentApplication.remove();

        setTheme(UIUtils.THEME);
        window = new Window(CAPTION);
        wm = new WidgetManager(this, window);

        modeSwitchers = new ConcurrentButtonGroup();
        globalSettings = new GlobalSettings();
        globalSettings.getCanvasSettings().addListener(new CanvasPropertiesListener());

        wm.setModeSwitchers(modeSwitchers);
        wm.setIcePusher(pusher);
        wm.setGlobalSetings(globalSettings);
        wm.setExperimentSettings(settings);

        //TODO uncomment
//        window.addComponent(new CloseInterceptor("Are you sure you wish to close this page?"));
        window.setSizeFull();
        window.getContent().setSizeFull();
        window.setImmediate(true);
        window.addActionHandler(new WindowHandler());

        //closing application as soon as user closes browser window
        window.addListener(new Window.CloseListener() {

            public void windowClose(Window.CloseEvent e) {
                //TODO autosave
                mainContent.stopDispatchers();
                close();
            }
        });

        //initial canvas size is dependent on user's screen size, implementation:
        window.addListener(new Window.ResizeListener() {

            public void windowResized(Window.ResizeEvent e) {
                wm.resizeComponents(e);
            }
        });

        setMainWindow(window);

        VerticalLayout mainLayout = new VerticalLayout();
        mainLayout.setSizeFull();
        mainLayout.addComponent(createMenuBar());
        mainLayout.addComponent(createToolbar());

        mainContent = new MainContent(this, pusher, wm);
        mainContent.addStyleName("v-horizontallayout-no-padding-top");
        wm.setMainContent(mainContent);
        mainContent.init();
        mainContent.requestRepaintAll();
        console = (Console) wm.getRegisteredComponent(Console.CID);
        mainContent.addComponent(pusher);

        mainLayout.addComponent(mainContent);
        mainLayout.setExpandRatio(mainContent, 1.0f);

        SessionGuard sessionGuard = new SessionGuard();
        sessionGuard.setKeepalive(true);
        mainLayout.addComponent(sessionGuard);
        window.setContent(mainLayout);

        getContext().addTransactionListener(this);
    }

    private MenuBar createMenuBar() {
        MainMenuBar menuBar = new MainMenuBar();

        /*
         * "File" menu
         */
        menuBar.setItem("File->New->Model", new ActionHandler() {

            public void handle() {
                newModel();
            }
        });
        menuBar.setItem("File->New->Aggregate", new ActionHandler() {

            public void handle() {
                newAggregateType();
            }
        });

        menuBar.setItem("File->Open", new ActionHandler() {

            public void handle() {
                load();
            }
        });

        menuBar.addSeparatorToPath("File");

        menuBar.setItem("File->Save", new ActionHandler() {

            public void handle() {
                save();
            }
        });

        menuBar.setItem("File->Save as..", new ActionHandler() {

            public void handle() {
                saveAs();
            }
        });

        menuBar.setItem("File->Save all", new ActionHandler() {

            public void handle() {
                showConfirmDialog("200px", "Are you shoure?", new PropertiesListener<Boolean>() {

                    public void propertiesSet(Boolean props) {
                        if (props) {
                            // TODO
//                            mainContent.saveAll();
                        }
                    }
                });
            }
        });

        menuBar.addSeparatorToPath("File");
        menuBar.setItem("File->Close", new ActionHandler() {

            public void handle() {
                MainContent.WsRec ws = wm.getMainContent().getActiveWorkspace();
                if (ws != null) {
                    wm.getMainContent().closeWorkspace(ws.getWsType().prefix() + ws.getDispatcher().getModelName());
                }
            }
        });
        menuBar.setItem("File->Close all", new ActionHandler() {

            public void handle() {
                MainContent.WsRec ws = wm.getMainContent().getActiveWorkspace();
                while (ws != null) {
                    wm.getMainContent().closeWorkspace(ws.getWsName());
                    ws = wm.getMainContent().getActiveWorkspace();
                }
            }
        });

        menuBar.addSeparatorToPath("File");
        menuBar.setItem("File->Import", null);
        menuBar.setItem("File->Export", null);
        menuBar.addSeparatorToPath("File");
        menuBar.setItem("File->Exit", new ActionHandler() {

            public void handle() {
                exit();
            }
        });

        /*
         * End "File" menu
         * Begin menu "Edit"
         */
        menuBar.setItem("Edit->Cut", null);
        menuBar.setItem("Edit->Copy", null);
        menuBar.setItem("Edit->Paste", null);
        menuBar.setItem("Edit->Delete", null);
        menuBar.setItem("Edit->Select all", null);

        /*
         * End menu "Edit"
         * Begin menu "View"
         */
        menuBar.setItem("View->Variables", new ActionHandler() {

            public void handle() {
                showVariablesEditor();
            }
        });

        menuBar.setItem("View->Draw", new ActionHandler() {

            public void handle() {
                showDrawSettings();
            }
        });
        menuBar.addSeparatorToPath("View");

//        menuBar.addSeparatorToPath("Edit");

        menuBar.setItem("Model->Start Experiment", new ActionHandler() {

            public void handle() {
                startExperiment();
            }
        });
        menuBar.setItem("Model->Start Debug Experiment", new ActionHandler() {

            public void handle() {
                startDebugExperiment();
            }
        });

        menuBar.setItem("Model->Start Distributed Experiment", null);
        
        menuBar.setItem("Model->View Reports->Primary reports", null);
        menuBar.setItem("Model->View Reports->Tables", null);
        menuBar.setItem("Model->View Reports->Graphics", null);
        menuBar.setItem("Model->View Reports->Histograms", null);

        // HLA menu
        menuBar.setItem("HLA", null);

        menuBar.setItem("Help->Help", null);
        menuBar.setItem("Help->About", null);

        wm.setDefaultMenu(menuBar);
        return menuBar;
    }

    private PropertiesListener<WorkspaceCreatingEvent> createCreationListener(final boolean model) {
        final PropertiesListener<WorkspaceCreatingEvent> listener = new PropertiesListener<WorkspaceCreatingEvent>() {

            public void propertiesSet(WorkspaceCreatingEvent props) {
                mainContent.createNewWorkspace(props.getWsName(), model ? MainContent.WorkspaceType.MODEL : MainContent.WorkspaceType.AGG_TYPE);

                MainContent.WsRec ws = mainContent.getActiveWorkspace();
                if (props.getTemplateName() != null && !props.getTemplateName().isEmpty()) {
                    AggregateDefinition child = ws.getRegistry().
                            getAggregateDefinition(props.getTemplateName());
                    ws.getUidGenerator().registerCustomId(child.getName());
                    Aggregate a = ws.getDispatcher().getRootAggregate().
                            addChildAggreagateDefinition(child,
                            ws.getUidGenerator().nextCustomId(child.getName()));
                    a.setObjectCoordinates(new Coordinates(300, 300));
                    a.setNameCoordinates(new Coordinates(0, 0));

                    // displaying
                    ws.getDispatcher().getLoader().onLoad(ws.getDispatcher().getRootAggregate());
                    ws.setSaveNeeded(true);
                }
            }
        };
        return listener;
    }
    
    private void newModel() {

        Collection<String> listModels = wm.getSaveManager().listAllModels();
        listModels.add(Dispatcher.MODEL_NAME);
        window.addWindow(new WorkspaceCreationChooser("Choose model", "400px",
                Dispatcher.MODEL_NAME, listModels, wm.getSaveManager().listAllAggregateTypes(),
                true, createCreationListener(true)));
    }

    private void newAggregateType() {
        window.addWindow(new WorkspaceCreationChooser("Create aggregate", "400px",
                wm.getMainContent().generateAggregateName(),
                null, wm.getSaveManager().listAllAggregateTypes(),
                true, createCreationListener(false)));
    }

    // TODO
    private void exit() {
//    	getMainWindow().getApplication().setLogoutURL("javascript:close()");
        getMainWindow().getApplication().close();
    }

    public void showConfirmDialog(String width, String msg, PropertiesListener<Boolean> listener) {
        window.addWindow(new ConfirmDialog(width, msg, listener));
    }

    private void clearAll() {
//        clear();
        MainContent.WsRec activeWs = mainContent.getActiveWorkspace();
//        savesManager.setCurrentFile(null);
//        uidGenerator.clear();
        activeWs.getDispatcher().clear();
        TreeModelView tree = (TreeModelView) wm.getRegisteredComponent(TreeModelView.CID);
        tree.renameItem(tree.getRoot(), activeWs.getDispatcher().getRootAggregate().getName());
//        savesManager.setSaveNeeded(false);
    }

    public void clear() {
        TreeModelView tree = (TreeModelView) wm.getRegisteredComponent(TreeModelView.CID);
        tree.removeAllItems();
        MainContent.WsRec activeWs = mainContent.getActiveWorkspace();
        activeWs.getJessCanvas().clear();
        activeWs.getJessCanvas().setDrawingMode(DrawingMode.SELECT);
        modeSwitchers.press(selectionModeBtn);
    }

    private void save() {
        mainContent.save(false);
    }

    private void saveAs() {
        mainContent.save(true);
    }

    private void load() {
        mainContent.load();
    }

    private void showVariablesEditor() {
        MainContent.WsRec workspace = mainContent.getActiveWorkspace();
        if (workspace != null) {
            final Aggregate aggregate = workspace.getDispatcher().getRootAggregate();
            window.addWindow(new AggregateVariablesWindow(AggregateVariables.readFrom(aggregate),
                    new PropertiesListener<AggregateVariables>() {

                        public void propertiesSet(AggregateVariables props) {
                            props.writeTo(aggregate);
                            TreeModelView tmv = (TreeModelView) wm.getRegisteredComponent(TreeModelView.CID);
                            tmv.initVariablesInTree();
                            wm.getMainContent().getActiveWorkspace().setSaveNeeded(true);
                        }
                    }));
        }
    }

    private void showDrawSettings() {
        window.addWindow(new DrawSettingsWindow(globalSettings));
    }

    private Panel createToolbar() {
        Toolbar toolbar = new Toolbar();

        String layout = "base";
        toolbar.createToolLayout(layout);

        Button b = toolbar.createButton("New Model", "agg.png");
        b.addListener(new Button.ClickListener() {

            public void buttonClick(Button.ClickEvent clickEvent) {
                newModel();
            }
        });
        toolbar.addIntoLayout(layout, b);
        b = toolbar.createButton("New Aggregate", "agg_t.png");
        b.addListener(new Button.ClickListener() {

            public void buttonClick(Button.ClickEvent clickEvent) {
                newAggregateType();
            }
        });
        toolbar.addIntoLayout(layout, b);

        b = toolbar.createButton("Open", "folder.png");
        b.addListener(new Button.ClickListener() {

            public void buttonClick(Button.ClickEvent clickEvent) {
                load();
            }
        });
        toolbar.addIntoLayout(layout, b);

        b = toolbar.createButton("Save", "disk.png");
        b.addListener(new Button.ClickListener() {

            public void buttonClick(Button.ClickEvent clickEvent) {
                save();
            }
        });
        toolbar.addIntoLayout(layout, b);

        layout = "system";
        toolbar.createToolLayout(layout);

        b = toolbar.createButton("Variables Editor", "key_v.png");
        b.addListener(new Button.ClickListener() {

            public void buttonClick(Button.ClickEvent clickEvent) {
                showVariablesEditor();
            }
        });
        toolbar.addIntoLayout(layout, b);


        layout = "zoom";
        toolbar.createToolLayout(layout);

        b = toolbar.createButton("Zoom in", "zoom_in.png");
        b.addListener(new Button.ClickListener() {

            public void buttonClick(Button.ClickEvent clickEvent) {
                if (scale < JessCanvas.SCALE_MAX) {
                    scale += JessCanvas.SCALE_STEP;
                    if (scale == JessCanvas.UNWANTED_SCALE) {
                        scale += JessCanvas.SCALE_STEP;
                    }
                    mainContent.scaleCanvasPalette(scale);
                }
            }
        });
        toolbar.addIntoLayout(layout, b);

        b = toolbar.createButton("Zoom out", "zoom_out.png");
        b.addListener(new Button.ClickListener() {

            public void buttonClick(Button.ClickEvent clickEvent) {
                if (scale > JessCanvas.SCALE_MIN) {
                    scale -= JessCanvas.SCALE_STEP;
                    if (scale == JessCanvas.UNWANTED_SCALE) {
                        scale -= JessCanvas.SCALE_STEP;
                    }
                    mainContent.scaleCanvasPalette(scale);
                }
            }
        });
        toolbar.addIntoLayout(layout, b);

        b = toolbar.createButton("Zoom to 100", "zoom_one.png");
        b.addListener(new Button.ClickListener() {

            public void buttonClick(Button.ClickEvent clickEvent) {
                mainContent.scaleCanvasPalette(100);
            }
        });
        toolbar.addIntoLayout(layout, b);

        layout = "experiment";
        toolbar.createToolLayout(layout);

        b = toolbar.createButton("Start experiment", "start_experiment.png");
        b.addListener(new Button.ClickListener() {

            public void buttonClick(Button.ClickEvent clickEvent) {
                startExperiment();
            }
        });
        toolbar.addIntoLayout(layout, b);

        b = toolbar.createButton("Start distributed experiment", "start_experiment_d.png");
        b.addListener(new Button.ClickListener() {

            public void buttonClick(Button.ClickEvent clickEvent) {
                menuExec("Model->Start Distributed Experiment");
            }
        });
        toolbar.addIntoLayout(layout, b);

        b = toolbar.createButton("Start debug experiment", "debug_exc.png");
        b.addListener(new Button.ClickListener() {

            public void buttonClick(Button.ClickEvent clickEvent) {
                startDebugExperiment();
            }
        });
        toolbar.addIntoLayout(layout, b);

        b = toolbar.createButton("Step debug experiment", "nresume.png");
        b.addListener(new Button.ClickListener() {

            public void buttonClick(Button.ClickEvent clickEvent) {
                stepDebugExperiment();
            }
        });
        toolbar.addIntoLayout(layout, b);

        b = toolbar.createButton("Stop debug experiment", "control_stop_blue.png");
        b.addListener(new Button.ClickListener() {

            public void buttonClick(Button.ClickEvent clickEvent) {
                stopDebugExperiment();
            }
        });
        toolbar.addIntoLayout(layout, b);

        layout = "selection_mode";
        toolbar.createToolLayout(layout);
        b = toolbar.createButton("Selection mode", "cursor.png");
        b = wm.createModeSwitcher(b, DrawingMode.SELECT);
        toolbar.addIntoLayout(layout, b);
        selectionModeBtn = b;

        b = toolbar.createButton("Moving mode", "distribution_partnerships.png");
        b = wm.createModeSwitcher(b, DrawingMode.MOVE);
        toolbar.addIntoLayout(layout, b);

        wm.setDefaultToolbar(toolbar);
        return toolbar;
    }
    
    private void menuExec(String path) {
        MenuItem item = wm.getDefaultMenu().getMenuItem(path);
        item.getCommand().menuSelected(item);
    }

    private void startExperiment() {
        MainContent.WsRec ws = mainContent.getActiveWorkspace();
        if (ws == null || ws.getWsType() != MainContent.WorkspaceType.MODEL) {
            return;
        }
        final Dispatcher dispatcher = ws.getDispatcher();
        if (dispatcher.getCurrentExperimentRunning() != null) {
            window.showNotification("experiment debug is running");
        } else if (dispatcher.validationOK()) {
            window.addWindow(new ExperimentSettingsWindow(wm.getExperimentSettings(), dispatcher,
                    new PropertiesListener<ExperimentSettings>() {

                        public void propertiesSet(ExperimentSettings settings) {
                            ExperimentManager experimentManager = dispatcher.createExperimentManager(settings);
                            ExperimentProgressWindow experimentProgressWindow =
                                    new ExperimentProgressWindow(
                                    experimentManager,
                                    pusher,
                                    new JessExperimentWindowListener(experimentManager));
                            window.addWindow(experimentProgressWindow);
                            experimentManager.start();
                        }
                    }, false, wm));
        }
    }

    private void startDebugExperiment() {
        MainContent.WsRec ws = mainContent.getActiveWorkspace();
        if (ws == null || ws.getWsType() != MainContent.WorkspaceType.MODEL) {
            return;
        }
        final Dispatcher dispatcher = ws.getDispatcher();
        if (dispatcher.getCurrentExperimentRunning() != null) {
            window.showNotification("Experiment debug already running");
        } else if (dispatcher.validationOK()) {
            window.addWindow(new ExperimentSettingsWindow(this.settings, dispatcher,
                    new PropertiesListener<ExperimentSettings>() {

                        public void propertiesSet(ExperimentSettings settings) {
                            ExperimentManager experimentManager = dispatcher.createExperimentManager(settings);
                            experimentManager.debug();
                            window.showNotification("Debug started");
                            logger.info("Debug started");
                        }
                    }, true, wm));
        }
    }

    private void stepDebugExperiment() {
        MainContent.WsRec ws = mainContent.getActiveWorkspace();
        if (ws == null || ws.getWsType() != MainContent.WorkspaceType.MODEL) {
            return;
        }
        final Dispatcher dispatcher = ws.getDispatcher();
        if (dispatcher.getCurrentExperimentRunning() == null) {
            window.showNotification("Debug not started");
        } else {
            dispatcher.getCurrentExperimentRunning().debug();
        }
        if (dispatcher.getCurrentExperimentRunning() == null) {
            window.showNotification("Debug finished");
        }
    }

    private void stopDebugExperiment() {
        MainContent.WsRec ws = mainContent.getActiveWorkspace();
        if (ws == null || ws.getWsType() != MainContent.WorkspaceType.MODEL) {
            return;
        }
        final Dispatcher dispatcher = ws.getDispatcher();
        if (dispatcher.getCurrentExperimentRunning() != null) {
            dispatcher.getCurrentExperimentRunning().stopDebug();
            window.showNotification("Debug stopped");
            logger.info("debug stopped");
        } else {
            window.showNotification("Nothing to stop");
            logger.info("debug not running");
        }
    }

    public void transactionStart(Application application, Object transactionData) {
        if (application == JessApp.this) {
            sharedApps.set(this);
        }
    }

    public void transactionEnd(Application application, Object transactionData) {
        sharedApps.set(null);
        sharedApps.remove();
    }

    public static Console getConsoleInstance() {
        return sharedApps.get().console;
    }

// -------------------------- INNER CLASSES --------------------------
    private class WindowHandler implements Action.Handler {

        private final Action escAction = new ShortcutAction("Esc", ShortcutAction.KeyCode.ESCAPE, null);

        public Action[] getActions(Object o, Object o1) {
            return new Action[]{escAction};
        }

        public void handleAction(Action action, Object o, Object o1) {
            if (escAction == action) {
                //in connection mode esc is handled by the canvas itself - it removes the drown connector from the pallete
                if (DrawingMode.CONNECT != wm.getMainContent().getActiveWorkspace().getJessCanvas().getDrawingMode()) {
                    modeSwitchers.press(selectionModeBtn);
                    wm.getMainContent().getActiveWorkspace().getJessCanvas().setDrawingMode(DrawingMode.SELECT);
                }
            }
        }
    }

    private class CanvasPropertiesListener implements GlobalSettings.PropertyChangeListener {

        public void propertyChanged(String propertyName, Object value) {

            MainContent.WsRec active = wm.getMainContent().getActiveWorkspace();
            if (active != null) {
                JessCanvas jessCanvas = active.getJessCanvas();
                if ("gridAdjustEnabled".equals(propertyName)) {
                    jessCanvas.setGridAdjustEnabled((Boolean) value);
                }
                if ("gridPainted".equals(propertyName)) {
                    jessCanvas.setGridPainted((Boolean) value);
                }
                if ("pathCorrectionEnabled".equals(propertyName)) {
                    jessCanvas.setPathCorrectionEnabled((Boolean) value);
                }
                if ("horizontalLinesDrawnFirst".equals(propertyName)) {
                    jessCanvas.setHorizontalLinesDrawnFirst((Boolean) value);
                }
                if ("previewDrawn".equals(propertyName)) {
                    jessCanvas.setPreviewDrawn((Boolean) value);
                }
            }
        }
    }

    private class JessExperimentWindowListener implements ExperimentProgressWindow.ExperimentWindowListener {

        private final ExperimentManager experimentManager;

        private JessExperimentWindowListener(ExperimentManager experimentManager) {
            this.experimentManager = experimentManager;
        }

        public void stopExperiment() {
            experimentManager.stop();
        }

        public void showReport(ExperimentReport experimentReport) {
            window.addWindow(new ExperimentReportWindow(experimentReport, wm.getExperimentSettings()));
        }
    }
}
