package ua.cn.stu.cs.ems.ui.vaadin.ui;

import com.vaadin.ui.TextArea;
import ua.cn.stu.cs.ems.ui.vaadin.validation.EclSyntaxValidator;

/**
 * @author n0weak
 */
public class EclEditor extends TextArea {

    public EclEditor(String width, EclSyntaxValidator validator) {
        setWidth(width);
        addValidator(validator);
        setImmediate(true);
    }
}
