package ua.cn.stu.cs.ems.ui.vaadin.validation;

import com.vaadin.data.Validator;
import org.codehaus.plexus.util.StringInputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ua.cn.stu.cs.ems.core.ecli.IdleModelAccessor;
import ua.cn.stu.cs.ems.ecli.EclInterpreter;
import ua.cn.stu.cs.ems.ecli.ValidationReport;
import ua.cn.stu.cs.ems.ecli.errors.ErrorCode;
import ua.cn.stu.cs.ems.ecli.errors.ParsingError;

/**
 * @author n0weak
 */
public class EclSyntaxValidator implements Validator {
    
    final static Logger logger = LoggerFactory.getLogger(EclSyntaxValidator.class);

    private final static boolean CASE_SENSITIVE = true;
    private final EclInterpreter interpreter = new EclInterpreter(true, CASE_SENSITIVE);
    private final IdleModelAccessor modelAccessor;

    public EclSyntaxValidator(IdleModelAccessor modelAccessor) {
        this.modelAccessor = modelAccessor;
    }

    public void validate(Object o) throws InvalidValueException {
        ValidationReport report = getValidationReport(o);
        if (report != null) {
            if (!report.getParsingErrors().isEmpty()) {
                StringBuilder sb = new StringBuilder();
                for (ParsingError error : report.getParsingErrors()) {
                    if (error.getErrorCode().getSeverity() == ErrorCode.Severity.ERROR) {
                        sb.append(error.getMessage()).append("<br/>");
                    }
                }
                throw new InvalidValueException(sb.toString());
            }
        }
        else {
            throw new InvalidValueException("Validation failed");
        }
    }

    public boolean isValid(Object o) {
        ValidationReport report = getValidationReport(o);

        boolean valid = true;
        if (null != report) {
            for (ParsingError error : report.getParsingErrors()) {
                if (error.getErrorCode().getSeverity() == ErrorCode.Severity.ERROR) {
                    valid = false;
                    break;
                }
            }
        }
        else {
            valid = false;
        }
        return valid;
    }

    private ValidationReport getValidationReport(Object o) {
        if (null != o) {
            try {
                modelAccessor.clearModifications();
                return interpreter.validate(new StringInputStream(o.toString()), modelAccessor);
            } catch (Exception e) {
                logger.error("Failed to validate ECL code", e);
            }
        }
        return null;
    }
}
