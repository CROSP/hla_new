package ua.cn.stu.cs.ems.ui.vaadin;

import java.io.File;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.cn.stu.cs.ems.core.JessException;

/**
 *
 * @author leonid
 */
public class FileSystemStore {
    final static Logger logger = LoggerFactory.getLogger(FileSystemStore.class);
    
    private static final String DEFAULT_EXT = ".xml";

    private String home;
    private String profile;
    
    public FileSystemStore(String home) {
        if (home == null) {
            throw new JessException("Home can be null");
        }
        File hm = new File(home);
        if (!hm.exists()) {
            hm.mkdirs();
        }
        this.home = home;
    }    
    
    public enum FileType {
        MODEL(DEFAULT_EXT), 
        AGGREGATE(".aggregateType"), 
        FOLDER(""), 
        FOM(DEFAULT_EXT);
        
        private String ext;
        
        private FileType(String ext) {
            this.ext = ext;
        }
        
        public String getExtension() {
            return ext;
        }
    }
}
