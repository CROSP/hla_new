package ua.cn.stu.cs.ems.ui.vaadin.ui.experiment.report;

import com.invient.vaadin.charts.InvientCharts;
import com.invient.vaadin.charts.InvientChartsConfig;
import com.invient.vaadin.charts.InvientChartsConfig.AxisBase;
import com.invient.vaadin.charts.InvientChartsConfig.CategoryAxis;
import com.invient.vaadin.charts.InvientChartsConfig.MarkerState;
import com.invient.vaadin.charts.InvientChartsConfig.NumberXAxis;
import com.invient.vaadin.charts.InvientChartsConfig.NumberYAxis;
import com.invient.vaadin.charts.InvientChartsConfig.SymbolMarker;
import com.invient.vaadin.charts.InvientChartsConfig.XAxis;
import com.invient.vaadin.charts.InvientChartsConfig.YAxis;
import com.vaadin.ui.Component;
import com.vaadin.ui.VerticalLayout;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import ua.cn.stu.cs.ems.core.utils.ArrayUtils;

/**
 *
 * @author proger
 */
public class SeveralGraphicsOutputer implements GraphicResultOutputer {

    public Component createOutput(GraphicResult graphicResult) {

        VerticalLayout layout = new VerticalLayout();

        List<Double[]> yValsList = graphicResult.getYValues();
        List<String> yAxesNamesList = graphicResult.getYAxesNames();
        List<String> captions = graphicResult.getCaptions();

        for (int i = 0; i < yValsList.size(); i++) {
            double[] yVals = ArrayUtils.createUnboxedArray(graphicResult.getYValues().get(i));

            InvientCharts chart = drawChart(graphicResult.getXValues(), yVals,
                    captions.get(i), graphicResult.getXAxeName(), yAxesNamesList.get(i));

            layout.addComponent(chart);
        }

        return layout;
    }

    public Component createOutput(HistoResult histoResult) {

        VerticalLayout layout = new VerticalLayout();

        List<Double[]> yValsList = histoResult.getYValues();
        List<String> yAxesNamesList = histoResult.getYAxesNames();
        List<String> xAxesNamesList = histoResult.getXAxesNames();
        List<String> captions = histoResult.getCaptions();

        for (int i = 0; i < yValsList.size(); i++) {
            double[] yVals = ArrayUtils.createUnboxedArray(histoResult.getYValues().get(i));
            double[] xVals = ArrayUtils.createUnboxedArray(histoResult.getXValues().get(i));

            InvientCharts chart = drawChartHisto(xVals, yVals,
                    captions.get(i), xAxesNamesList.get(i), yAxesNamesList.get(i));

            layout.addComponent(chart);
        }

        return layout;
    }

    private InvientCharts drawChart(double[] xVals, double[] yVals, String caption, String xAxesTitle, String yAxeTitle) {
        if (xVals.length != yVals.length) {
            throw new IllegalArgumentException("Arrays of X and Y values must have equal size!");
        }

        InvientChartsConfig chartConfig = new InvientChartsConfig();
        chartConfig.getGeneralChartConfig().setType(InvientCharts.SeriesType.SCATTER);
        chartConfig.getGeneralChartConfig().setZoomType(InvientChartsConfig.GeneralChartConfig.ZoomType.XY);        
        chartConfig.getLegend().setEnabled(false);
        chartConfig.getTitle().setText(caption);

        NumberXAxis xAxis = new NumberXAxis();
        xAxis.setTitle(new AxisBase.AxisTitle(xAxesTitle));
        xAxis.setMaxZoom(5);

        LinkedHashSet<XAxis> xAxesSet = new LinkedHashSet<XAxis>();
        xAxesSet.add(xAxis);
        chartConfig.setXAxes(xAxesSet);

        NumberYAxis yAxis = new NumberYAxis();
        yAxis.setTitle(new AxisBase.AxisTitle("parameter"));

        yAxis.setMin(0.0);
        yAxis.setMinorGrid(new InvientChartsConfig.AxisBase.MinorGrid());
        yAxis.getMinorGrid().setLineWidth(0);
        yAxis.setGrid(new InvientChartsConfig.AxisBase.Grid());
        yAxis.getGrid().setLineWidth(0);
        yAxis.setTitle(new AxisBase.AxisTitle(yAxeTitle));

        LinkedHashSet<YAxis> yAxesSet = new LinkedHashSet<YAxis>();
        yAxesSet.add(yAxis);
        chartConfig.setYAxes(yAxesSet);

        SymbolMarker symbolMarker = new SymbolMarker(true);

        symbolMarker.setRadius(4);
        symbolMarker.setSymbol(SymbolMarker.Symbol.CIRCLE);
        symbolMarker.setHoverState(new MarkerState());
        symbolMarker.getHoverState().setEnabled(true);
        symbolMarker.getHoverState().setRadius(7);
        symbolMarker.getHoverState().setLineWidth(0);

        InvientChartsConfig.ScatterConfig scatterCfg = new InvientChartsConfig.ScatterConfig();

        scatterCfg.setMarker(symbolMarker);
        chartConfig.addSeriesConfig(scatterCfg);

        InvientChartsConfig.Tooltip tooltip = new InvientChartsConfig.Tooltip();
        tooltip.setFormatterJsFunc("function() {" + " return ''+ '" + xAxesTitle
                + "'+ ': ' + this.x + '<br/>' + '" + yAxeTitle + "'+': '+ this.y +''; " + "}");
        chartConfig.setTooltip(tooltip);

        InvientCharts chart = new InvientCharts(chartConfig);

        InvientCharts.XYSeries series = new InvientCharts.XYSeries(caption, InvientCharts.SeriesType.SCATTER);


        for (int i = 0; i < xVals.length; i++) {
            series.addPoint(new InvientCharts.DecimalPoint(series, xVals[i], yVals[i]));
        }

        chart.addSeries(series);
        return chart;
    }

    private InvientCharts drawChartHisto(double[] xVals, double[] yVals, String caption, String xAxesTitle, String yAxeTitle) {
        if (xVals.length - 1 != yVals.length) {
            throw new IllegalArgumentException("Arrays of X and Y values must have equal size!");
        }

        InvientChartsConfig chartConfig = new InvientChartsConfig();
        chartConfig.getGeneralChartConfig().setType(InvientCharts.SeriesType.COLUMN);
        chartConfig.getLegend().setEnabled(false);
        chartConfig.getTitle().setText(caption);

        CategoryAxis xAxis = new CategoryAxis();
        xAxis.setTitle(new AxisBase.AxisTitle(xAxesTitle));
        ArrayList<String> categories = createCategories(xVals);
        xAxis.setCategories(categories);

        InvientChartsConfig.XAxisDataLabel dataLabel = new InvientChartsConfig.XAxisDataLabel(true);
        dataLabel.setRotation(-45);
        dataLabel.setAlign(InvientChartsConfig.HorzAlign.RIGHT);
        xAxis.setLabel(dataLabel);

        LinkedHashSet<XAxis> xAxesSet = new LinkedHashSet<XAxis>();

        xAxesSet.add(xAxis);
        chartConfig.setXAxes(xAxesSet);

        NumberYAxis yAxis = new NumberYAxis();
        yAxis.setTitle(new AxisBase.AxisTitle("parameter"));

        yAxis.setMin(0.0);
        yAxis.setMinorGrid(new InvientChartsConfig.AxisBase.MinorGrid());
        yAxis.getMinorGrid().setLineWidth(1);
        yAxis.setGrid(new InvientChartsConfig.AxisBase.Grid());
        yAxis.getGrid().setLineWidth(0);
        yAxis.setTitle(new AxisBase.AxisTitle(yAxeTitle));

        LinkedHashSet<YAxis> yAxesSet = new LinkedHashSet<YAxis>();
        yAxesSet.add(yAxis);
        chartConfig.setYAxes(yAxesSet);

        InvientChartsConfig.Tooltip tooltip = new InvientChartsConfig.Tooltip();
        tooltip.setFormatterJsFunc("function() {" + " return '' "
                + "+ this.series.name +': '+ this.y +''; " + "}");
        chartConfig.setTooltip(tooltip);

        InvientCharts chart = new InvientCharts(chartConfig);


        InvientCharts.XYSeries series = new InvientCharts.XYSeries(caption,
                InvientCharts.SeriesType.COLUMN);

        for (int i = 0; i < yVals.length; i++) {
            series.addPoint(new InvientCharts.DecimalPoint(series, yVals[i]));
        }

        chart.addSeries(series);

        return chart;
    }

    private ArrayList<String> createCategories(double[] xVals) {
        ArrayList<String> result = new ArrayList<String>();
        NumberFormat formatter = new DecimalFormat("0.00E00");
        for (int i = 1; i < xVals.length; i++) {
            result.add(formatter.format(xVals[i - 1]) + "-" + formatter.format(xVals[i]));
        }
        return result;
    }
}