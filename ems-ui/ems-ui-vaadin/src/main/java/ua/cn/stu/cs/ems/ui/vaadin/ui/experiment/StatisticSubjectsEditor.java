package ua.cn.stu.cs.ems.ui.vaadin.ui.experiment;

import com.vaadin.addon.chameleon.ChameleonTheme;
import com.vaadin.ui.*;
import org.vaadin.spacewrapper.SpaceWrapper;
import ua.cn.stu.cs.ems.ui.vaadin.EntityClass;
import ua.cn.stu.cs.ems.ui.vaadin.Informant;

import java.util.Collection;
import java.util.Set;

/**
 * @author n0weak
 */
class StatisticSubjectsEditor extends VerticalLayout {

    private static final String WIDTH = "100%";
    private static final String HEIGHT = "150px";
    private final StatisticSubjectsTable table;

    public StatisticSubjectsEditor(String caption, Informant informant) {
        Collection<String> places = informant.listAll(EntityClass.PLACE);
        Collection<String> queues = informant.listAll(EntityClass.QUEUE);
        Collection<String> transitions = informant.listAll(EntityClass.TRANSITION);
        table = new StatisticSubjectsTable(WIDTH, HEIGHT, caption, places, queues, transitions);
        table.setColumnWidth(StatisticSubjectsTable.COLUMN_IDS[1], 35);
        addComponent(table);
        addComponent(createControls(!places.isEmpty(), !queues.isEmpty(), !transitions.isEmpty()));
//        AbsoluteLayout l = new AbsoluteLayout();
//        l.addComponent(table);
//        l.setWidth(WIDTH);
//        l.setHeight(HEIGHT);
    }

    public boolean isValid() {
        return table.isValid();
    }

    public void commit() {
        table.commit();
    }

    public void discard() {
        table.discard();
    }

    public Set<String> getSelectedEntities() {
        return table.getSelectedEntities();
    }

    private Component createControls(boolean places, boolean queues, boolean transitions) {
        HorizontalLayout layout = new HorizontalLayout();
        layout.setWidth(WIDTH);

        createPresetButton("all places", layout, Alignment.TOP_LEFT, EntityClass.PLACE, places);
        createPresetButton("all queues", layout, Alignment.TOP_CENTER, EntityClass.QUEUE, queues);
        createPresetButton("all transitions", layout, Alignment.TOP_RIGHT, EntityClass.TRANSITION, transitions);

        SpaceWrapper sw = new SpaceWrapper(layout);
        sw.setMarginBottom(10);
        return sw;
    }

    private Button createPresetButton(String caption, HorizontalLayout layout, Alignment alignment,
                                      final EntityClass entityClass, boolean enabled) {
        Button b = new Button(caption);
        b.setStyleName(ChameleonTheme.BUTTON_LINK);
//        b.setEnabled(enabled);

        b.addListener(new Button.ClickListener() {

            public void buttonClick(Button.ClickEvent clickEvent) {
                table.selectAll(entityClass);
            }
        });

        layout.addComponent(b);
        layout.setComponentAlignment(b, alignment);
        return b;
    }
}
