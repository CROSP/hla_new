package ua.cn.stu.cs.ems.ui.vaadin.utils;

import com.vaadin.Application;
import com.vaadin.terminal.DownloadStream;
import com.vaadin.terminal.StreamResource;
import java.io.InputStream;

/**
 *
 * @author leonid
 */
public class TemporaryDownloadResource extends StreamResource {
    
    public TemporaryDownloadResource(final Application application, 
            final String fileName, 
            final String contentType, final InputStream stream) {
        super(null, fileName, application);
        setStreamSource(new DownloadSource(stream));
        setMIMEType(contentType);
    }
    
    @Override
    public DownloadStream getStream() {
        final DownloadStream stream =
                new DownloadStream(getStreamSource().getStream(), getMIMEType(), getFilename());
        stream.setParameter("Content-Disposition", "attachment;filename=" + getFilename());
        // This magic incantation should prevent anyone from caching the data
        stream.setParameter("Cache-Control", "private,no-cache,no-store");
        // In theory <=0 disables caching. In practice Chrome, Safari (and, apparently, IE) all
        // ignore <=0. Set to 1s
        stream.setCacheTime(1000);
        return stream;
    }
    
    public class DownloadSource implements StreamSource {
        
        private final InputStream stream;

        private DownloadSource(InputStream stream) {
            this.stream = stream;
        }
        
        public InputStream getStream() {
            return stream;
        }
        
    }
}
