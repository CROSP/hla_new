package ua.cn.stu.cs.ems.ui.vaadin.ui.components;

import ua.cn.stu.cs.ems.ui.vaadin.WidgetManager;
import ua.cn.stu.cs.ems.ui.vaadin.ui.components.MainContent.Position;

import com.vaadin.ui.Component;

public interface RegisteredComponent extends Component {

    /**
     * Setting self defaults, i.e. self size, color, default caption and other
     */
    public void init(WidgetManager wm);
	
    public String getTabCaption();
	
    public Position getDefaultPosition();
    
    public String getCid();
}
