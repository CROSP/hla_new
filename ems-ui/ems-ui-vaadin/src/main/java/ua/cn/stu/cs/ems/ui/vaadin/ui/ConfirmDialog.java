package ua.cn.stu.cs.ems.ui.vaadin.ui;

import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import ua.cn.stu.cs.ems.ui.vaadin.PropertiesListener;

/**
 * @author n0weak
 */
public class ConfirmDialog extends AbstractSettingsWindow {

    private final static String DEFAULT_HEIGHT = "78px";
    static final String CAPTION = "Confirm";
    private final PropertiesListener<Boolean> propertiesListener;
    private final String message;

    public ConfirmDialog(String width, String message, final PropertiesListener<Boolean> propertiesListener) {
        super(CAPTION, width);
        this.propertiesListener = propertiesListener;
        this.message = message;
        paint();
    }

    @Override
    protected String getCommitButtonName() {
        return "Yes";
    }

    @Override
    protected String getDiscardButtonName() {
        return "No";
    }

    protected void paint() {
        VerticalLayout layout = (VerticalLayout) getContent();
        layout.setSizeFull();
        layout.setHeight(DEFAULT_HEIGHT);

        Label l = new Label();
        l.setValue(message);
        layout.addComponent(l);
        paintFooter(layout);
    }

    @Override
    protected boolean apply() {
        propertiesListener.propertiesSet(true);
        return true;
    }

    @Override
    protected void discard() {
        propertiesListener.propertiesSet(false);
        getParent().removeWindow(this);
    }
}
