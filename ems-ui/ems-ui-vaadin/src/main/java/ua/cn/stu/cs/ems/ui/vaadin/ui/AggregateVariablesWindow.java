package ua.cn.stu.cs.ems.ui.vaadin.ui;

import com.vaadin.addon.chameleon.ChameleonTheme;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;
import ua.cn.stu.cs.ems.ui.vaadin.PropertiesListener;
import ua.cn.stu.cs.ems.ui.vaadin.dto.AggregateVariables;
import ua.cn.stu.cs.ems.ui.vaadin.ui.experiment.ExperimentSettingsWindow;
import ua.cn.stu.cs.ems.ui.vaadin.ui.input.table.VariablesEditor;

/**
 * @author n0weak
 */
public class AggregateVariablesWindow extends AbstractSettingsWindow {

    private static final String CAPTION = "Variables Editor";
    private static final String WIDTH = "600px";
    private static final String FOOTER_BUTTONS_WIDTH = "70px";
    private final PropertiesListener<AggregateVariables> listener;
    private final AggregateVariables aggregateVariables;
    private Table table;

    public AggregateVariablesWindow(AggregateVariables aggregateVariables,
                                    PropertiesListener<AggregateVariables> listener) {
        super(CAPTION, WIDTH);
        this.listener = listener;
        this.aggregateVariables = aggregateVariables;
        paint();
    }

    protected final void paint() {
        VerticalLayout layout = (VerticalLayout) getContent();
        table = new VariablesEditor(null, aggregateVariables.getVariables());
        table.setSizeFull();
        layout.addComponent(table);
        layout.addComponent(createFooter());
    }
    
    private Layout createFooter() {
        HorizontalLayout hLayout = new HorizontalLayout();
        hLayout.setWidth("100%");
        hLayout.setMargin(true, false, false, false);

        Button btn = createDiscardButton();
        hLayout.addComponent(btn);
        hLayout.setComponentAlignment(btn, Alignment.MIDDLE_LEFT);

        btn = new Button("Clear all");
        btn.setStyleName(ChameleonTheme.BUTTON_SMALL);
        btn.setWidth(FOOTER_BUTTONS_WIDTH);
        btn.addListener(new Button.ClickListener() {

            public void buttonClick(ClickEvent event) {
                AggregateVariablesWindow.this.clearAll();
            }
        });
        hLayout.addComponent(btn);
        hLayout.setComponentAlignment(btn, Alignment.MIDDLE_CENTER);

        btn = createCommitButton();
        hLayout.addComponent(btn);
        hLayout.setComponentAlignment(btn, Alignment.BOTTOM_RIGHT);

        return hLayout;
    }

    @Override
    protected void discard() {
        table.discard();
    }

    @Override
    protected boolean apply() {
        if (table.isValid()) {
            table.commit();
            listener.propertiesSet(aggregateVariables);
            return true;
        }
        else {
            return false;
        }
    }

    private void clearAll() {
        table.removeAllItems();
        table.addItem();
    }
}
