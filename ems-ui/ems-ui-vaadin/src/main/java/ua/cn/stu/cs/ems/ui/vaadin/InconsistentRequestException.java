package ua.cn.stu.cs.ems.ui.vaadin;

/**
 * Exception to be thrown when incoming request is invalid or does not correlate with internal state.
 *
 * @author n0weak
 */
public class InconsistentRequestException extends RuntimeException {

    public InconsistentRequestException(String message) {
        super(message);
    }
}
