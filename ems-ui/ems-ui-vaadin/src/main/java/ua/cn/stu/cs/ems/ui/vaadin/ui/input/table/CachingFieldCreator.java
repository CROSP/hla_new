package ua.cn.stu.cs.ems.ui.vaadin.ui.input.table;

import com.vaadin.ui.AbstractField;
import com.vaadin.ui.Field;
import com.vaadin.ui.TextField;
import ua.cn.stu.cs.ems.core.utils.Pair;

import java.util.HashMap;
import java.util.Map;

/**
 * Field creator which caches created fields.
 * Must be used with {@link CachingInputTable}
 *
 * @author n0weak
 */
public abstract class CachingFieldCreator<T> implements FieldCreator<T> {

    private Map<String, Map<T, AbstractField>> cache = new HashMap<String, Map<T, AbstractField>>();

    public CachingFieldCreator(String... cachedFields) {
        initCache(cachedFields);
    }

    public void initCache(String... cachedFields) {
        for (String field : cachedFields) {
            if (!cache.containsKey(field)) {
                cache.put(field, new HashMap<T, AbstractField>());
            }
        }
    }

    public Map<T, AbstractField> getCache(String fieldName) {
        return cache.get(fieldName);
    }

    public void clearCache() {
        for (Map<T, AbstractField> c : cache.values()) {
            c.clear();
        }
    }

    public void removeItem(T item) {
        for (Map<T, AbstractField> c : cache.values()) {
            c.remove(item);
        }
    }

    public Pair<Field, Boolean> createField(T item, Object propertyId) {
        Map<T, AbstractField> fieldCache = cache.get(propertyId.toString());
        if (null != fieldCache && fieldCache.containsKey(item)) {
            return new Pair<Field, Boolean>(fieldCache.get(item), false);
        }
        else {
            AbstractField f = createNewField(item, propertyId);
            if (null != fieldCache && null != f) {
                fieldCache.put(item, f);
            }

            if (null == f) {
                f = new TextField();
            }

            return new Pair<Field, Boolean>(f, true);
        }
    }

    public AbstractField getField(String fieldName, T item) {
        return getCache(fieldName).get(item);
    }

    public abstract AbstractField createNewField(T item, Object propertyId);
}
