package ua.cn.stu.cs.ems.ui.vaadin;

/**
 * @author n0weak
 */
public enum EntityClass {
	
    PLACE, TRANSITION, QUEUE, AGGREGATE, VARIABLE
}
