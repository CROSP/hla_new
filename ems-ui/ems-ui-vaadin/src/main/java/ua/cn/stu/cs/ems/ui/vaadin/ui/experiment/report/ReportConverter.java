package ua.cn.stu.cs.ems.ui.vaadin.ui.experiment.report;

import com.vaadin.ui.Table;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import ua.cn.stu.cs.ems.core.experiment.ExperimentReport;
import ua.cn.stu.cs.ems.core.experiment.PrimaryStatisticsElement;
import ua.cn.stu.cs.ems.core.experiment.PrimaryStatisticsResult;
import ua.cn.stu.cs.ems.core.experiment.SecondaryStatisticsElement;
import ua.cn.stu.cs.ems.core.experiment.statistics.AggregateVariableStateCollector;
import ua.cn.stu.cs.ems.core.experiment.statistics.StatisticsParameters;
import ua.cn.stu.cs.ems.core.experiment.statistics.StatisticsResult;
import ua.cn.stu.cs.ems.core.utils.JessUtils;

/**
 *
 * @author proger
 */
public class ReportConverter {

    public static GraphicResult secondaryStatisticsToGraphicResult(ExperimentReport experimentReport) {
        List<SecondaryStatisticsElement> sseList = experimentReport.getSecondaryStatistics();
        double[] factorValues = new double[sseList.size()];
        Double[] respondValues = new Double[sseList.size()];

        for (int i = 0; i < sseList.size(); i++) {
            SecondaryStatisticsElement sse = sseList.get(i);

            factorValues[i] = sse.getFactorValue();
            respondValues[i] = sse.getRespond();
        }

        GraphicResult result = new GraphicResult(factorValues, "Factor values");
        result.addYValues(respondValues, "Respond", "Secondary statistics");

        return result;
    }

    public static HistoResult secondaryStatisticsToHistoResult(ExperimentReport experimentReport) {
//        List<SecondaryStatisticsElement> sseList = experimentReport.getSecondaryStatistics();
//        double[] factorValues = new double[sseList.size()];
//        Double[] respondValues = new Double[sseList.size()];
//
//        for (int i = 0; i < sseList.size(); i++) {
//            SecondaryStatisticsElement sse = sseList.get(i);
//
//            factorValues[i] = sse.getFactorValue();
//            respondValues[i] = sse.getRespond();
//        }
//
//        GraphicResult result = new GraphicResult(factorValues, "Factor values");
//        result.addYValues(respondValues, "Respond", "Secondary statistics");
//
//        return result;
        return null;
    }

    public static GraphicResult primaryStatisticsToGraphResult(ExperimentReport normalizedReport, String objectName, Double factorValue) {
        List<PrimaryStatisticsResult> primaryStatistics = normalizedReport.getPrimaryStatisticsFor(objectName);

        List<PrimaryStatisticsElement> primaryStatisticsForFactorValue = getPrimaryStatisticsForFactorValue(primaryStatistics, factorValue);

        return createGraphicResult(primaryStatisticsForFactorValue);
    }

    public static HistoResult primaryStatisticsToHistoResult(ExperimentReport normalizedReport, String objectName, Double factorValue) {
        List<PrimaryStatisticsResult> primaryStatistics = normalizedReport.getPrimaryStatisticsFor(objectName);

        List<PrimaryStatisticsElement> primaryStatisticsForFactorValue = getPrimaryStatisticsForFactorValue(primaryStatistics, factorValue);

        return createHistoResult(primaryStatisticsForFactorValue);
    }
    
    public static GraphicResult primaryVariableStaticticstoGrapthResult(ExperimentReport normalizedReport, String objectName) {
        Map<Double,String> data = normalizedReport.getVariablesState().getChildStatistic(objectName).getValues();

        List<Double> times = new ArrayList<Double> (data.keySet());
        Collections.sort(times);

        double[] values = new double[data.size()];


        for (int i = 0; i < values.length; i++) {
            values[i] = Double.valueOf(data.get(times.get(i)));
            if (Double.isInfinite(values[i]) || Double.isNaN(values[i])) {
                values[i] = 0d;
            }
        }

        GraphicResult graphicResult = new GraphicResult(values, objectName);
        graphicResult.addYValues(times.toArray(new Double[times.size()]), "Time", "Time");
        
        return graphicResult;
    }

    private static GraphicResult createGraphicResult(List<PrimaryStatisticsElement> primaryStatisticsForFactorValue) {
        double[] timeStamps = new double[primaryStatisticsForFactorValue.size() + 1];
        HashMap<StatisticsParameters, ArrayList<Double>> parameterValues = new HashMap<StatisticsParameters, ArrayList<Double>>();

        timeStamps[0] = 0;

        for (int i = 0; i < primaryStatisticsForFactorValue.size(); i++) {
            PrimaryStatisticsElement pse = primaryStatisticsForFactorValue.get(i);
            timeStamps[i + 1] = pse.getTime();

            StatisticsResult statRes = pse.getStatisticsResult();

            for (StatisticsParameters sp : statRes.getKeys()) {
                ArrayList<Double> list = parameterValues.get(sp);
                if (list == null) {
                    list = new ArrayList<Double>();
                    parameterValues.put(sp, list);
                }

                list.add(statRes.getResultValue(sp));
            }
        }

        GraphicResult result = new GraphicResult(timeStamps, "Time");


        for (StatisticsParameters param : parameterValues.keySet()) {
            ArrayList<Double> valuesList = parameterValues.get(param);
            valuesList.add(0, 0d);
            Double[] values = valuesList.toArray(new Double[0]);

            for (int i = 0; i < values.length; i++) {
                if (Double.isInfinite(values[i]) || Double.isNaN(values[i])) {
                    values[i] = 0d;
                }
            }

            result.addYValues(values, param.getPresentation(), param.getPresentation());
        }

        return result;
    }

    private static HistoResult createHistoResult(List<PrimaryStatisticsElement> primaryStatisticsForFactorValue) {
        //HARDCODE!
        int numOfIntervals = 10;
        //
        HashMap<StatisticsParameters, ArrayList<Double>> parameterValues = new HashMap<StatisticsParameters, ArrayList<Double>>();

        double[] timeStamps = new double[primaryStatisticsForFactorValue.size()];

        for (int i = 0; i < primaryStatisticsForFactorValue.size(); i++) {
            PrimaryStatisticsElement pse = primaryStatisticsForFactorValue.get(i);
            timeStamps[i] = pse.getTime();

            StatisticsResult statRes = pse.getStatisticsResult();

            for (StatisticsParameters sp : statRes.getKeys()) {
                ArrayList<Double> list = parameterValues.get(sp);
                if (list == null) {
                    list = new ArrayList<Double>();
                    parameterValues.put(sp, list);
                }

                list.add(statRes.getResultValue(sp));
            }
        }

        HistoResult result = new HistoResult();

        for (StatisticsParameters param : parameterValues.keySet()) {
            ArrayList<Double> valuesList = parameterValues.get(param);
            //Validation on NaN and Infinite values
            validationValues(valuesList);
            HashMap<Double, ArrayList<Double>> newValuesList = new HashMap<Double, ArrayList<Double>>();

//            if (!param.equals(StatisticsParameters.NUMBER_OF_PASSED_TOKENS)
//                    && !param.equals(StatisticsParameters.OCCUPIED_COEFFICIENT)) {

            HistoInterval histoInterval = new HistoInterval();

            double interval = histoInterval.calculateInterval(valuesList, numOfIntervals);
            double min = histoInterval.getMin();

            Double[] intervalStamps;
            //if interval=0
            if (interval <= JessUtils.EPSILON) {
                intervalStamps = new Double[2];
            } else {
                intervalStamps = new Double[numOfIntervals + 1];
            }

            intervalStamps[0] = min;

            for (int i = 1; i < intervalStamps.length; i++) {
                intervalStamps[i] = intervalStamps[i - 1] + interval;
                newValuesList.put(intervalStamps[i], new ArrayList<Double>());
            }

            int sample = valuesList.size();

            for (int i = 1; i < intervalStamps.length; i++) {
                int j = 0;
                if (i == 1) {
                    while (j < valuesList.size()) {
                        if (valuesList.get(j) >= intervalStamps[i - 1] && valuesList.get(j) <= intervalStamps[i]) {
                            double currValue = valuesList.get(j);
                            newValuesList.get(intervalStamps[i]).add(currValue);
                            valuesList.remove(currValue);
                        } else {
                            j++;
                        }
                    }
                } else {
                    while (j < valuesList.size()) {
                        if (valuesList.get(j) > intervalStamps[i - 1] && valuesList.get(j) <= intervalStamps[i]) {
                            double currValue = valuesList.get(j);
                            newValuesList.get(intervalStamps[i]).add(currValue);
                            valuesList.remove(currValue);
                        } else {
                            j++;
                        }
                    }
                }
            }

            Double[] values = new Double[intervalStamps.length - 1];

            for (int i = 1; i < intervalStamps.length; i++) {
                double currVal = ((double) newValuesList.get(intervalStamps[i]).size()) / sample;
                currVal = new BigDecimal(currVal).setScale(4, RoundingMode.UP).doubleValue();
                values[i - 1] = Double.valueOf(currVal);
            }

            result.addXYValues(intervalStamps, "Intervals", values, "Frequency of " + param.getPresentation(), "Frequency of " + param.getPresentation());
        }
//        }

        return result;
    }

//    private static GraphicResult createGraphResNumOfPassedTokensFreq(List<PrimaryStatisticsElement> primaryStatisticsForFactorValue, boolean graphOrHisto) {
//        double[] timeStamps = new double[primaryStatisticsForFactorValue.size() + 1];
//        ArrayList<Double> parameterValues = new ArrayList<Double>();
//
//        timeStamps[0] = 0;
//
//        if (primaryStatisticsForFactorValue.size() < 1) {
//            return new GraphicResult(timeStamps, "Time");
//        }
//
//        PrimaryStatisticsElement pseFinishResult = primaryStatisticsForFactorValue.get(primaryStatisticsForFactorValue.size() - 1);
//        Double finishResult = pseFinishResult.getStatisticsResult().getResultValue(StatisticsParameters.NUMBER_OF_PASSED_TOKENS);
//        Double prevParamValue = 0d;
//
//        for (int i = 0; i < primaryStatisticsForFactorValue.size(); i++) {
//            PrimaryStatisticsElement pse = primaryStatisticsForFactorValue.get(i);
//            timeStamps[i + 1] = pse.getTime();
//
//            StatisticsResult statRes = pse.getStatisticsResult();
//
//            Double res;
//
//            res = statRes.getResultValue(StatisticsParameters.NUMBER_OF_PASSED_TOKENS) - prevParamValue;
//            if (!graphOrHisto) {
//                res = res / finishResult;
//                res = new BigDecimal(res).setScale(2, RoundingMode.UP).doubleValue();
//            }
//            prevParamValue = statRes.getResultValue(StatisticsParameters.NUMBER_OF_PASSED_TOKENS);
//            parameterValues.add(res);
//        }
//
//        GraphicResult result = new GraphicResult(timeStamps, "Time");
//
//        ArrayList<Double> valuesList = parameterValues;
//        valuesList.add(0, 0d);
//        Double[] values = valuesList.toArray(new Double[0]);
//
//        for (int i = 0; i < values.length; i++) {
//            if (Double.isInfinite(values[i]) || Double.isNaN(values[i])) {
//                values[i] = 0d;
//            }
//        }
//
//        result.addYValues(values, "The relative frequency", "The relative frequency of passed tokens");
//
//        return result;
//    }
    private static List<PrimaryStatisticsElement> getPrimaryStatisticsForFactorValue(List<PrimaryStatisticsResult> primaryStatistics, Double factorValue) {
        for (PrimaryStatisticsResult psr : primaryStatistics) {
            if (psr.getFactorValue() == factorValue) {
                return psr.getDumpedPrimaryStatisticsElements();
            }
        }

        return null;
    }

    static GraphicResult primaryStatisticsToGraphResult(ExperimentReport normalizedReport, String objectName) {
        List<PrimaryStatisticsResult> primaryStatistics = normalizedReport.getPrimaryStatisticsFor(objectName);
        List<PrimaryStatisticsElement> primaryStatisticsElements = primaryStatistics.get(0).getDumpedPrimaryStatisticsElements();

        return createGraphicResult(primaryStatisticsElements);
    }

    static HistoResult primaryStatisticsToHistoResult(ExperimentReport normalizedReport, String objectName) {
        List<PrimaryStatisticsResult> primaryStatistics = normalizedReport.getPrimaryStatisticsFor(objectName);
        List<PrimaryStatisticsElement> primaryStatisticsElements = primaryStatistics.get(0).getDumpedPrimaryStatisticsElements();

        return createHistoResult(primaryStatisticsElements);
    }

//    static GraphicResult numPassedTokensFreqToGraphResult(ExperimentReport normalizedReport, String objectName) {
//        List<PrimaryStatisticsResult> primaryStatistics = normalizedReport.getPrimaryStatisticsFor(objectName);
//        List<PrimaryStatisticsElement> primaryStatisticsElements = primaryStatistics.get(0).getDumpedPrimaryStatisticsElements();
//
//        return createGraphResNumOfPassedTokensFreq(primaryStatisticsElements, true);
//    }
//
//    static GraphicResult numPassedTokensFreqToHistoResult(ExperimentReport normalizedReport, String objectName) {
//        List<PrimaryStatisticsResult> primaryStatistics = normalizedReport.getPrimaryStatisticsFor(objectName);
//        List<PrimaryStatisticsElement> primaryStatisticsElements = primaryStatistics.get(0).getDumpedPrimaryStatisticsElements();
//
//        return createGraphResNumOfPassedTokensFreq(primaryStatisticsElements, false);
//    }
    /**
     *
     * @param experimentReport
     * @return HashMap<String, TableResult>, where key is name of aggregate and
     * value is tables with results
     */
    public static HashMap<String, TableResult> primaryStatisticsToTableResult(ExperimentReport experimentReport) {
        ArrayList<String> statisticsObjects = new ArrayList<String>(experimentReport.getStatisticsObjectsNames());
        if (!statisticsObjects.isEmpty()) {
            Collections.sort(statisticsObjects);

            HashMap<String, TableResult> result = new HashMap<String, TableResult>();

            for (int i = 0; i < statisticsObjects.size(); i++) {
                String objectName = statisticsObjects.get(i);
                //Statistics result for objectName for all runs
                List<PrimaryStatisticsResult> prStResult = experimentReport.getPrimaryStatisticsFor(objectName);
                if (!prStResult.isEmpty()) {
                    String aggregateName = prStResult.get(0).getParentAggregate();
                    if (!result.containsKey(aggregateName)) {
                        result.put(aggregateName, new TableResult(aggregateName));
                    }
                    result.get(aggregateName).addElementInTable(prStResult);
                }
            }
            return result;
        } else {
            return null;
        }
    }
    
    public static ArrayList<Table> primaryStatisticToVariablesTables(ExperimentReport report) {
        ArrayList<Table> result = new ArrayList<Table>();
        
        for (String varName: report.getVariablesState().getChildNames()) {
            result.add(createTableVariablesState(varName, report.getVariablesState().getChildStatistic(varName)));
        }
        
        return result;
    }
    
    private static Table createTableVariablesState(String tableCaption, AggregateVariableStateCollector values) {
        Table t = new Table(tableCaption);
        t.addContainerProperty("Time", Double.class, "");
        t.addContainerProperty("Value", String.class, "");
        t.setStyleName("tableReport");
        t.setWidth("100%");
        List<Double> keys = new ArrayList<Double> (values.getValues().keySet());
        Collections.sort(keys);
        for (Double time: keys) {
            t.addItem(new Object[] {time, values.getValue(time)}, null);
        }
        return t;
    }

    /**
     *
     * @param experimentReport
     * @return TableResult
     */
    public static TableResult secondaryStatisticsToTableResult(ExperimentReport experimentReport) {
        List<SecondaryStatisticsElement> ssElements = experimentReport.getSecondaryStatistics();
        if (!ssElements.isEmpty()) {
            TableResult result = new TableResult("Test", "Test");
            for (int i = 0; i < ssElements.size(); i++) {
                result.addElementInTable(ssElements.get(i));
            }
            return result;
        } else {
            return null;
        }
    }

    private static void validationValues(ArrayList<Double> valuesList) {
        for (int i = 0; i < valuesList.size(); i++) {
            if (Double.isNaN(valuesList.get(i)) || Double.isInfinite(valuesList.get(i))) {
                valuesList.set(i, 0d);
            }
        }
    }

    public static final class HistoInterval {

        private double min;
        private double max;
        private double interval;

        /**
         * @return the min
         */
        public double getMin() {
            return min;
        }

        /**
         * @return the max
         */
        public double getMax() {
            return max;
        }

        /**
         * @return the numOfIntervals
         */
        public double getInterval() {
            return interval;
        }

        public double calculateInterval(ArrayList<Double> valuesList, int numOfIntervals) {

            min = valuesList.get(0);
            max = valuesList.get(0);

            for (int i = 0; i < valuesList.size(); i++) {
                if (valuesList.get(i) > max) {
                    max = valuesList.get(i);
                }
                if (valuesList.get(i) < min) {
                    min = valuesList.get(i);
                }
            }

            double sub = max - min;
            interval = sub / numOfIntervals;
            return interval;
        }
    }
}