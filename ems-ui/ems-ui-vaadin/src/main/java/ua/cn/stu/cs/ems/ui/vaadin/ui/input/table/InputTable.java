package ua.cn.stu.cs.ems.ui.vaadin.ui.input.table;

import com.vaadin.data.Container;
import com.vaadin.data.Property;
import com.vaadin.data.Validator;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.terminal.PaintException;
import com.vaadin.terminal.PaintTarget;
import com.vaadin.ui.*;
import org.apache.commons.lang.StringUtils;
import ua.cn.stu.cs.ems.core.utils.Pair;
import ua.cn.stu.cs.ems.ui.vaadin.utils.UIUtils;

import java.util.*;

/**
 * @author n0weak
 */
public class InputTable<T extends Editable<T>> extends Table {
// ------------------------------ FIELDS ------------------------------

    private final Class<T> clazz;
    private Collection<T> clientList;
    private List<T> innerList;
    private Set<AbstractField> fields = new HashSet<AbstractField>();
    protected boolean expandAllowed = false;
    /**
     * Used to forbid visible columns reinitialization via {@link #setVisibleColumns(Object[])},
     * because of the bug in {@link #setContainerDataSource(com.vaadin.data.Container)} method 
     * (visible columns set by user are cleared).
     * <p/>
     * Does not affect {@link #setVisibleColumns(String...)}
     */
    private boolean visibleColumnsSet = false;
    private ExpandListener<T> expandListener;

// --------------------------- CONSTRUCTORS ---------------------------
    public InputTable(Class<T> clazz, String caption) {
        super(caption);
        this.clazz = clazz;
        setEditable(true);
        addStyleName("input-table");
        setSortDisabled(true);
    }

// --------------------- GETTER / SETTER METHODS ---------------------
    protected Collection<T> getInnerList() {
        return innerList;
    }

    public void setExpandListener(ExpandListener<T> expandListener) {
        this.expandListener = expandListener;
    }

// ------------------------ INTERFACE METHODS ------------------------
// --------------------- Interface Buffered ---------------------
    @Override
    public void commit() throws SourceException, Validator.InvalidValueException {
        if (isValid()) {
            if (!innerList.isEmpty()) {
                innerList.remove(innerList.size() - 1); //removing stub element
            }
            clientList.clear();
            clientList.addAll(innerList);
        }
    }

    @Override
    public void discard() throws SourceException {
        initContainer(clientList);
        super.discard();
    }

    public void setVisibleColumns(String... visibleColumns) {
        super.setVisibleColumns(visibleColumns);
        visibleColumnsSet = true;
    }

    @Override
    public void setVisibleColumns(Object[] visibleColumns) {
        if (!visibleColumnsSet) {
            super.setVisibleColumns(visibleColumns);
            visibleColumnsSet = true;
        }
    }

    // --------------------- Interface Property ---------------------
    /**
     * @return stub value to enable validation
     */
    @Override
    public Object getValue() {
        return "value";
    }

// --------------------- Interface Validatable ---------------------
    @Override
    public boolean isValid() {
        for (AbstractField f : fields) {
            if (!f.isValid()) {
                return false;
            }
        }
        return super.isValid();
    }

// -------------------------- OTHER METHODS --------------------------
    // WARNING !!! Using an VAADIN special fix 
    @SuppressWarnings("unchecked")
    public T getLast() {
        int maxIndex = size() - getPageLength(); // DON'T 
        setCurrentPageFirstItemIndex(maxIndex);  // DELETE
        return (T) ((ArrayList) getItemIds()).get(getItemIds().size() - 1);
    }

    @Override
    protected Object getPropertyValue(Object rowId, Object colId, Property property) {
        Object o = super.getPropertyValue(rowId, colId, property);
        if (o instanceof AbstractField) {
            fields.add((AbstractField) o);
        }
        return o;
    }

    public void initContainer(Collection<T> items) {
        clientList = items;

        innerList = new ArrayList<T>(items.size());
        for (T var : items) {
            innerList.add(var.copy());
        }

        innerList.add(newInstance()); //adding empty attribute to allow new item addition

        BeanItemContainer<T> container = new BeanItemContainer<T>(clazz, innerList);
        setContainerDataSource(container);
    }

    private T newInstance() {
        T item;
        try {
            item = clazz.newInstance(); //adding empty attribute to allow new item addition
        } catch (Exception e) {
            throw new IllegalArgumentException("Editable entity must have non-arg constructor!");
        }
        return item;
    }

    public void initFactory(FieldCreator<T> fieldCreator) {
        setTableFieldFactory(new InputTableFieldFactory(fieldCreator));

        addGeneratedColumn("", new Table.ColumnGenerator() {

            public Component generateCell(Table source, final Object itemId, Object columnId) {
                if (!isLast(itemId)) {
                    Button b = UIUtils.createIconizedButton("Delete attribute", "delete.png");
                    b.addListener(new Button.ClickListener() {

                        @SuppressWarnings("unchecked")
                        public void buttonClick(Button.ClickEvent clickEvent) {
                            expandAllowed = false;
                            remove((T) itemId);
                        }
                    });

                    return b;
                }
                else {
                    return UIUtils.createIconizedButton("", "empty.png");
                }
            }
        });
    }

    public boolean isLast(Object itemId) {
        return ((ArrayList) getItemIds()).get(getItemIds().size() - 1).equals(itemId);
    }

    protected void remove(T item) {
        removeItem(item);
        innerList.remove(item);
    }

    @Override
    public void paintContent(PaintTarget target) throws PaintException {
        super.paintContent(target);
        expandAllowed = true;
    }

    @Override
    protected void refreshRenderedCells() {
        if (null != fields) {
            fields.clear();
        }
        super.refreshRenderedCells();
    }

// -------------------------- INNER CLASSES --------------------------
    private class InputTableFieldFactory implements TableFieldFactory {

        private final FieldCreator<T> fieldCreator;

        private InputTableFieldFactory(FieldCreator<T> fieldCreator) {
            this.fieldCreator = fieldCreator;
        }

        @SuppressWarnings("unchecked")
        public Field createField(final Container container, final Object itemId, Object propertyId, Component uiContext) {
            Pair<Field, Boolean> pair = fieldCreator.createField((T) itemId, propertyId);
            if (pair.getSecond()) {
                Field f = pair.getFirst();
                if (f instanceof AbstractField) {
                    ((AbstractField) f).setImmediate(true);
                }
                f.addListener(new Property.ValueChangeListener() {

                    public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
                        Object value = valueChangeEvent.getProperty().getValue();
                        if (expandAllowed && isLast(itemId) && null != value && !StringUtils.isBlank(value.toString())) {
                            expandAllowed = false;
                            expand();
                        }
                    }
                });
            }
            return pair.getFirst();
        }

        private void expand() {
            if (null != expandListener) {
                expandListener.tableExpanded(getLast());
            }
            T emptyAttr = newInstance();
            addItem(emptyAttr);   //adding empty attribute to allow new item addition
            innerList.add(emptyAttr);
        }
    }

    public interface ExpandListener<T> {

        /**
         * Event is fired just before the table is expanded. Stub entity is not added to the inner list at this stage.
         *
         * @param newItem item which initialization caused expanding. Actually, it is the last item in the inner list
         */
        void tableExpanded(T newItem);
    }
}
