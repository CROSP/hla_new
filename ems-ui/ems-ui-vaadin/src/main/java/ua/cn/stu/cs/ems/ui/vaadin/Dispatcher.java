package ua.cn.stu.cs.ems.ui.vaadin;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.cn.stu.cs.ems.core.*;
import ua.cn.stu.cs.ems.core.aggregates.*;
import ua.cn.stu.cs.ems.core.ecli.IdleModelAccessor;
import ua.cn.stu.cs.ems.core.experiment.*;
import ua.cn.stu.cs.ems.core.experiment.statistics.StatisticsParameters;
import ua.cn.stu.cs.ems.core.models.ENetworksModel;
import ua.cn.stu.cs.ems.core.models.Model;
import ua.cn.stu.cs.ems.core.models.ModelListener;
import ua.cn.stu.cs.ems.core.queues.*;
import ua.cn.stu.cs.ems.core.queues.Queue;
import ua.cn.stu.cs.ems.core.transitions.*;
import ua.cn.stu.cs.ems.core.transitions.functions.DefaultDelayFunction;
import ua.cn.stu.cs.ems.core.transitions.functions.DefaultPermitingFunction;
import ua.cn.stu.cs.ems.core.transitions.functions.DefaultTransformationFunction;
import ua.cn.stu.cs.ems.core.utils.Coordinates;
import ua.cn.stu.cs.ems.core.utils.JessUtils;
import ua.cn.stu.cs.ems.ui.vaadin.dto.GlobalSettings;
import ua.cn.stu.cs.ems.ui.vaadin.ui.experiment.ExperimentSettings;
import ua.cn.stu.cs.ems.ui.vaadin.ui.experiment.ExperimentSettings.FactorEntry;
import ua.cn.stu.cs.ems.ui.vaadin.utils.UIDGenerator;
import ua.cn.stu.cs.ems.ui.vaadin.validation.NameValidator;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.JessCanvas;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.EntityType;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.event.*;

/**
 * Control modeling. Receiving and sending information to GUI.
 *
 * @author n0weak
 */
public class Dispatcher implements JessCanvas.CanvasListener, Informant {
// ------------------------------ FIELDS ------------------------------

    final static Logger logger = LoggerFactory.getLogger(Dispatcher.class);
    public static final String MODEL_NAME = "Model";
    private volatile ExecutionType currentExecutionType;
    private PlaceStateChangedListener placeStateChangedListener = new FiringPlaceStateChangeListener();
    private AggregateDefinition rootAggregate;
    /**
     * Dispatcher events listeners, collection can be accessed concurrently
     * during execution.
     */
    private Set<DispatcherListener> listeners =
            Collections.newSetFromMap(new ConcurrentHashMap<DispatcherListener, Boolean>());
    private Set<ModelChangeListener> modelChangeListeners = new HashSet<ModelChangeListener>();
    private final GlobalSettings.AnimationSettings settings;
    private final ErrorRenderer errorRenderer;
    private final UIDGenerator uidGenerator;
    private final SavesManager savesManager;
    private int inputIdx;
    private int outputIdx;
    private JessExperimentManager currentExperimentManager = null;

// --------------------------- CONSTRUCTORS ---------------------------
    public Dispatcher(ErrorRenderer errorRenderer, UIDGenerator uidGenerator,
            GlobalSettings.AnimationSettings settings, SavesManager savesManager) {
        this.errorRenderer = errorRenderer;
        this.settings = settings;
        this.uidGenerator = uidGenerator;
        this.savesManager = savesManager;

        rootAggregate = new AggregateDefinition(MODEL_NAME);
    }

// --------------------- GETTER / SETTER METHODS ---------------------
    public AggregateDefinition getRootAggregate() {
        return rootAggregate;
    }

// ------------------------ INTERFACE METHODS ------------------------
// --------------------- Interface CanvasListener ---------------------
    public String entityCreated(CreationEvent event) {
        AggregateChild child;
        String id = event.getEntityDetails().getId();
        switch (event.getEntityDetails().getEntityType()) {
            case PLACE:
                Place place = new Place(id);
                place.addStateChangeListener(placeStateChangedListener);
                rootAggregate.addPlace(place);
                child = place;
                break;
            case TTRANSITION:
                child = new TTransition(id);
                break;
            case FTRANSITION:
                child = new FTransition(id);
                break;
            case JTRANSITION:
                child = new JTransition(id);
                break;
            case YTRANSITION:
                child = new YTransition(id);
                ((YTransition) child).setPermitingFunction(new DefaultPermitingFunction());
                break;
            case XTRANSITION:
                child = new XTransition(id);
                ((XTransition) child).setPermitingFunction(new DefaultPermitingFunction());
                break;
            case FIFO:
                child = new FIFOQueue(id);
                break;
            case LIFO:
                child = new LIFOQueue(id);
                break;
            case PFIFO:
                child = new FIFOPriorityQueue(id, new DefaultTokenPriorityFunction());
                break;
            case PLIFO:
                child = new LIFOPriorityQueue(id, new DefaultTokenPriorityFunction());
                break;
            case INPUT:
                child = new InputImpl(id);
                rootAggregate.addInput((Input) child, inputIdx++);
                break;
            case OUTPUT:
                child = new OutputImpl(id);
                rootAggregate.addOutput((Output) child, outputIdx++);
                break;
            case AGGREGATE:
                logger.debug("pre loading model");
                AggregateDefinition aggregate = savesManager.getAggregateType(id.substring(0, id.lastIndexOf("_")));
                child = rootAggregate.addChildAggreagateDefinition(aggregate, id);
                break;
            default:
                throw new InconsistentRequestException("Entity type is not known: "
                        + event.getEntityDetails().getEntityType());
        }


        child.setObjectCoordinates(new Coordinates(event.getEntityDetails().getCenterCoords().x,
                event.getEntityDetails().getCenterCoords().y));
        child.setNameCoordinates(new Coordinates(0, 0));

        if (child instanceof Transition) {
            Transition transition = (Transition) child;
            transition.setDelayFunction(new DefaultDelayFunction());
            rootAggregate.addTransition(transition);
            transition.setTransformationFunction(new DefaultTransformationFunction());
        } else if (child instanceof Queue) {
            Queue queue = (Queue) child;
            rootAggregate.addQueue(queue);
        }

        child.setParentAggregate(rootAggregate);

        for (DispatcherListener listener : listeners) {
            listener.entityCreated(event.getEntityDetails().getEntityType(), id);
        }
        fireModelChangeEvent();

        if (child instanceof Aggregate) {
            id = uidGenerator.nextCustomId(id.substring(0, id.lastIndexOf("_")));
        } else {
            id = uidGenerator.nextId(event.getEntityDetails().getEntityType());
        }
        return id;
    }

    public void entitiesConnected(ConnectionEvent event) {
        logger.debug("Connecting event");
        String sourceParent = null;
        String source = null;
        String targetParent = null;
        String target = null;
        if (event.getFrom().getEntityType() == EntityType.PLACE) {
            switch (event.getTo().getEntityType()) {
                case XTRANSITION:
                case TTRANSITION:
                case YTRANSITION:
                case FTRANSITION:
                case JTRANSITION:
                case FIFO:
                case LIFO:
                case PFIFO:
                case PLIFO:
                case OUTPUT:
                case AGGREGATE:
                    AggregateChild child = findAggregateChild(event.getTo().getId());
                    Place place = findPlace(event.getFrom().getId());
                    source = event.getFrom().getId();
                    target = event.getTo().getId();

                    if (child instanceof Transition) {
                        Transition transition = (Transition) child;
                        transition.addInputPlace(place, transition.getInputPlaces().size());
                    } else if (child instanceof Queue) {
                        Queue queue = (Queue) child;
                        queue.connectInputPlace(place);
                    } else if (child instanceof Output) {
                        Output output = (Output) child;
                        output.connectInputPlace(place);
                    } else if (child instanceof Aggregate) {
                        logger.debug("connecting place to aggregate:");
                        AggregateDefinitionReference ref = (AggregateDefinitionReference) rootAggregate.getAggregateChild(event.getTo().getId());
                        String connectionPath = event.getTo().getId() + "." + event.getInputName();
                        AggregateChild ac = findAggregateChild(connectionPath);
                        if (ac != null) { // all OK, input found
                            Input i = (Input) ac;
                            logger.debug("input found: i=" + i);
                            i.setInputPlace(place);
                            target = event.getInputName();
                            targetParent = event.getTo().getId();
                        } else {
                            throw new InconsistentRequestException("Cannot connect place to aggregate " + event.getTo().getId());
                        }
                    } else {
                        throw new InconsistentRequestException("Connection is not supported for " + child.getClass());
                    }
                    break;
                default:
                    throw new InconsistentRequestException("Cannot connect place to " + event.getTo().getEntityType());
            }
        } else if (event.getTo().getEntityType() == EntityType.PLACE) {
            switch (event.getFrom().getEntityType()) {
                case XTRANSITION:
                case TTRANSITION:
                case YTRANSITION:
                case FTRANSITION:
                case JTRANSITION:
                case FIFO:
                case LIFO:
                case PFIFO:
                case PLIFO:
                case INPUT:
                case AGGREGATE:
                    AggregateChild child = findAggregateChild(event.getFrom().getId());
                    Place place = findPlace(event.getTo().getId());
                    source = event.getFrom().getId();
                    target = event.getTo().getId();

                    if (child instanceof Transition) {
                        Transition transition = (Transition) child;
                        transition.addOutputPlace(place, transition.getOutputPlaces().size());
                    } else if (child instanceof Queue) {
                        Queue queue = (Queue) child;
                        queue.connectOutputPlace(place);
                    } else if (child instanceof Input) {
                        Input input = (Input) child;
                        input.setOutputPlace(place);
                    } else if (child instanceof Aggregate) {
                        AggregateDefinitionReference ref = (AggregateDefinitionReference) child;
                        AggregateChild ac = findAggregateChild(ref, event.getOutputName(), true);
                        if (ac != null) {
                            Output o = (Output) ac;
                            o.connectOutputPlace(place);
                            sourceParent = ref.getName();
                            source = event.getOutputName();
                        } else {
                            throw new InconsistentRequestException("Cannot connect aggregate to place " + event.getTo().getId());
                        }
                    } else {
                        throw new InconsistentRequestException("Connection is not supported for " + child.getClass());
                    }
                    break;
                default:
                    throw new InconsistentRequestException("Cannot connect place to "
                            + event.getFrom().getEntityType());
            }
        } else if (event.getFrom().getEntityType() == EntityType.AGGREGATE
                && event.getTo().getEntityType() == EntityType.AGGREGATE) {
            sourceParent = event.getFrom().getId();
            source = event.getOutputName();
            targetParent = event.getTo().getId();
            target = event.getInputName();

            Aggregate sourceAggregate = (Aggregate) JessUtils.getAggregateChild(getRootAggregate(), sourceParent);
            Aggregate targetAggregate = (Aggregate) JessUtils.getAggregateChild(getRootAggregate(), targetParent);

            if (sourceAggregate != null && targetAggregate != null) {
                Output o = (Output) JessUtils.getAggregateChild(sourceAggregate, source);
                Input i = (Input) JessUtils.getAggregateChild(targetAggregate, target);

                if (o != null && i != null) {
                    if (o.getConnectedInputs().isEmpty()) {
                        o.addInput(i, 0);
                    } else {
                        throw new IncorrectOutputConnection("Output " + o.getName() + " already have connected input");
                    }
                } else {
                    throw new InconsistentRequestException("Cant connect aggregates: one of object not found");
                }
            } else {
                throw new InconsistentRequestException("Childs not found: " + sourceParent + " and " + targetParent);
            }
        }

        logger.debug("adding arc: sp=" + sourceParent + " s=" + source + " tp=" + targetParent + " t=" + target);
        rootAggregate.addArcCoordinates(new ArcCoordinates(sourceParent, source, targetParent, target, convertCoords(event.getFixedCoords())));
        fireModelChangeEvent();
    }

    public void positionStateChanged(String s) {
        Place place = findPlace(s);
        if (place.isEmpty()) {
            Token token = new Token();
            place.setToken(token);
        } else {
            place.setToken(null);
        }
        firePlaceStateChangedEvent(place);
    }

    public void entitiesDeleted(DeletionEvent event) {
        List<String> ids = new ArrayList<String>();
        List<Input> deletedInputs = new ArrayList<Input>();
        List<Output> deletedOutputs = new ArrayList<Output>();
        for (EntityDetails details : event.getDeletedEntities()) {
            ids.add(details.getId());
            switch (details.getEntityType()) {
                case PLACE:
                    Place p = rootAggregate.getPlace(details.getId());
                    if (p.getInputActiveInstance() instanceof Output) {
                        ((Output) p.getInputActiveInstance()).disconnectOutputPlace(details.getId());
                    }
                    if (p.getOutputActiveInstance() instanceof Input) {
                        ((Input) p.getOutputActiveInstance()).disconnectInputPlace(details.getId());
                    }
                    rootAggregate.disconnectChildPlace(rootAggregate.getPlace(details.getId()));
                    break;
                case INPUT:
                    deletedInputs.add(rootAggregate.getInput(details.getId()));
                    break;
                case OUTPUT:
                    deletedOutputs.add(rootAggregate.getOutput(details.getId()));
                    break;
                default:
                    AggregateChild ac = findAggregateChild(details.getId());
                    if (ac instanceof Transition) {
                        rootAggregate.disconnectChildTransition((Transition) ac);
                    } else if (ac instanceof Queue) {
                        rootAggregate.disconnectQueue((Queue) ac);
                    } else if (ac instanceof Aggregate) {
                        List<String> inputsList = JessUtils.getAggregateInputs(((AggregateDefinitionReference) ac).getAggregateDefinition());
                        List<String> outputsList = JessUtils.getAggregateOutputs(((AggregateDefinitionReference) ac).getAggregateDefinition());

                        for (String iName : inputsList) {
                            Input i = (Input) JessUtils.getAggregateChild((Aggregate) ac, iName);
                            if (i != null) {
                                if (i.getConnectedOutput() != null) {
                                    i.getConnectedOutput().disconnectInput(0);
                                } else if (i.getInputPlace() != null) {
                                    i.disconnectInputPlace(i.getInputPlace().getName());
                                }
                            }
                        }

                        for (String oName : outputsList) {
                            Output o = (Output) JessUtils.getAggregateChild((Aggregate) ac, oName);
                            if (o != null) {
                                while (!o.getConnectedInputs().isEmpty()) {
                                    o.disconnectInput(0);
                                }
                                if (o.getOutputPlace() != null) {
                                    o.disconnectOutputPlace(o.getOutputPlace().getName());
                                }
                            }
                        }
                        rootAggregate.disconnectChildAggregate((Aggregate) ac);
                    } else {
                        throw new InconsistentRequestException("Disconnection is not supported for " + ac.getClass());
                    }
                    break;
            }
        }

        // THIS is must be after deleting.
        for (DispatcherListener listener : listeners) {
            listener.entitiesDeleted(ids);
        }

        if (!deletedInputs.isEmpty()) {
            inputDeleted(deletedInputs);
        }
        if (!deletedOutputs.isEmpty()) {
            outputDeleted(deletedOutputs);
        }

        fireModelChangeEvent();
    }

    public void entitiesDisconnected(DisconnectionEvent event) {
        for (DisconnectionEvent.DisconnectionDetails details : event.getDetails()) {
            logger.debug("DISKONNECTION EVENT: tio name=" + details.getTransitionIOName());
            logger.debug("event: placeId=" + details.getPlaceId() + " transitionId=" + details.getTransitionId() + " transitionIOName=" + details.getTransitionIOName());
            AggregateChild ac = findAggregateChild(details.getTransitionId());
            if (ac instanceof ActiveInstance) {
                String sourceName;
                String targetName;
                ActiveInstance ai = (ActiveInstance) ac;
                if (DisconnectionEvent.DisconnectionDetails.ConnectionType.INPUT == details.getConnectionType()) {
                    ai.disconnectInputPlace(details.getPlaceId());
                    sourceName = details.getPlaceId();
                    targetName = details.getTransitionId();
                } else {
                    ai.disconnectOutputPlace(details.getPlaceId());
                    targetName = details.getPlaceId();
                    sourceName = details.getTransitionId();
                }
                rootAggregate.removeArcCoordinates(null, sourceName, null, targetName);
            } else if (ac instanceof Aggregate) {
                logger.debug("disconnect aggregate");
                String placeName = details.getPlaceId();
                String transitionName = details.getTransitionId();

                logger.debug("event details: sourceId=" + placeName + " targetId=" + transitionName);

                AggregateChild sourceEntity = findAggregateChild(placeName);
                AggregateChild targetEntity = ac;
                boolean ok = false;
                if (sourceEntity instanceof Place) {
                    if (DisconnectionEvent.DisconnectionDetails.ConnectionType.INPUT == details.getConnectionType()) {
                        logger.debug("found place thet connected to the input");
                        Input ci = (Input) ((Place) sourceEntity).getOutputActiveInstance();
                        ci.disconnectInputPlace(placeName);
                    }
                    if (DisconnectionEvent.DisconnectionDetails.ConnectionType.OUTPUT == details.getConnectionType()) {
                        logger.debug("found place thet connected to the output");
                        Output co = (Output) ((Place) sourceEntity).getInputActiveInstance();
                        co.disconnectOutputPlace(placeName);
                    }
                } else if (sourceEntity instanceof Aggregate) {
                    logger.debug("found aggreaget thet connected to the input of aggregate");
                    // source = aggregate, target = aggregate
                    Input i = (Input) findAggregateChild((Aggregate) targetEntity, details.getTransitionIOName(), false);
                    i.getConnectedOutput().disconnectInput(0);
                } else {
                    throw new JessException("Unknown source entity. Cant disconnect");
                }
            } else {
                throw new InconsistentRequestException(
                        "Disconnection is supported only by the ActiveInstance implementors!");
            }
        }
        fireModelChangeEvent();
    }

    public void entityDoubleClicked(EntityEvent event) {
        EntityType type = event.getEntityDetails().getEntityType();
        firePropertiesRequestEvent(find(type, event.getEntityDetails().getId()));
    }

    private Collection<ArcCoordinates> findCords(String s) {
        ArrayList<ArcCoordinates> list = new ArrayList<ArcCoordinates>();

        for (ArcCoordinates ac : rootAggregate.getArcCoordinates()) {
            if ((ac.getSourceParent() != null && ac.getSourceParent().equals(s))
                    || (ac.getTargetParent() != null && ac.getTargetParent().equals(s))
                    || (ac.getSourceParent() == null && ac.getSourceName().equals(s))
                    || (ac.getTargetParent() == null && ac.getTargetName().equals(s))) {
                list.add(ac);
            }
        }

        return list;
    }

    public void entityMoved(MoveEvent event) {
        Collection<ArcCoordinates> list = findCords(event.getEntityDetails().getId());
        for (ConnectionEvent connection : event.getConnections()) {
            ArcCoordinates arcCoordinates = list.iterator().next();
            if (arcCoordinates != null) {
                arcCoordinates.setCoordinates(convertCoords(connection.getFixedCoords()));
            }
        }

        AggregateChild entity = find(event.getEntityDetails().getEntityType(), event.getEntityDetails().getId());
        if (null != entity) {
            logger.debug("x=" + event.getEntityDetails().getCenterCoords().x + " y=" + event.getEntityDetails().getCenterCoords().y);
            Coordinates newCords = new Coordinates(event.getEntityDetails().getCenterCoords().x,
                    event.getEntityDetails().getCenterCoords().y);
            entity.setObjectCoordinates(newCords);
            fireModelChangeEvent();
        }
    }

    public void canvasResized(ResizeEvent event) {
        for (DispatcherListener listener : listeners) {
            listener.canvasResized(event);
        }
    }

// --------------------- Interface Informant ---------------------
    public Collection<String> listAllEntities() {
        Collection<String> result = new HashSet<String>();
        for (Place p : rootAggregate.getPlaces()) {
            result.add(p.getName());
        }
        for (Transition t : rootAggregate.getTransitions()) {
            result.add(t.getName());
        }
        for (Queue q : rootAggregate.getQueues()) {
            result.add(q.getName());
        }

        return result;
    }

    public Collection<String> listAllVariables() {
        return rootAggregate.getVariablesNames();
    }

    public Collection<String> listAll(EntityClass entityClass) {
        return listAll(null, entityClass);
    }

    public List<String> listAll(String aggPath, EntityClass type) {
        List<String> list = new ArrayList<String>();

        Aggregate agg = (Aggregate) JessUtils.getAggregateChild(rootAggregate, aggPath);

        Collection<AggregateChild> childs = new ArrayList<AggregateChild>();
        if (agg != null) {
            switch (type) {
                case PLACE: {
                    childs.addAll(agg.getPlaces());
                    break;
                }
                case QUEUE: {
                    childs.addAll(agg.getQueues());
                    break;
                }
                case TRANSITION: {
                    childs.addAll(agg.getTransitions());
                    break;
                }
                case AGGREGATE: {
                    childs.addAll(agg.getChildAggregates());
                    break;
                }
            }
        }

        for (AggregateChild ac : childs) {
            list.add(ac.getName());
        }

        return list;
    }

    public List<String> listAllVariables(String aggPath) {
        Aggregate agg = (Aggregate) JessUtils.getAggregateChild(rootAggregate, aggPath);
        List<String> list = new ArrayList<String>();
        if (agg != null) {
            for (String var : agg.getVariablesNames()) {
                list.add(var);
            }
        }
        return list;
    }

    public StatisticsParameters[] getApplicableParameters(String entityName) {
        return StatisticsParameters.getApplicableParameters(find(entityName, true));
    }

    public String getModelName() {
        return getRootAggregate().getName();
    }

// -------------------------- OTHER METHODS --------------------------
    public void addListener(DispatcherListener listener) {
        listeners.add(listener);
    }

    public void addModelChangeListener(ModelChangeListener listener) {
        modelChangeListeners.add(listener);
    }

    public void clear() {
        rootAggregate = new AggregateDefinition(MODEL_NAME);
        inputIdx = 0;
        outputIdx = 0;
        fireModelChangeEvent();
    }

    /**
     * Fix coordinates when entity name was changed
     *
     * @param entity
     */
    public void fixCoordinates(AggregateChild entity, String newName) {
        String name = entity.getName();
        List<ArcCoordinates> cords = rootAggregate.getArcCoordinates();
        for (ArcCoordinates ac : cords) {
            if (name.equals(ac.getSourceName())) {
                ac.setSourceName(newName);
            }
            if (name.equals(ac.getTargetName())) {
                ac.setTargetName(newName);
            }
        }
    }

    private List<Coordinates> convertCoords(
            Collection<ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.Coordinates> jessCoords) {
        List<Coordinates> coords = new ArrayList<Coordinates>();
        for (ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.Coordinates c : jessCoords) {
            coords.add(new Coordinates(c.x, c.y));
        }

        return coords;
    }

    public ExperimentManager createExperimentManager(ExperimentSettings settings) {
        this.currentExperimentManager = new JessExperimentManager(settings);
        return currentExperimentManager;
    }

    public ExperimentManager getCurrentExperimentRunning() {
        return currentExperimentManager;
    }

    public ExecutionType getCurrentExecutionType() {
        return currentExecutionType;
    }

    private AggregateChild find(EntityType type, String id) {
        switch (type) {
            case PLACE:
                return findPlace(id);
            default:
                return findAggregateChild(id);
        }
    }

    private Place findPlace(String id) {
        return findPlace(id, false);
    }

    public AggregateChild findAggregateChild(String id) {
        return findAggregateChild(rootAggregate, id, false);
    }

    private AggregateChild find(String id, boolean silent) {
        AggregateChild entity = findPlace(id, silent);
        if (null == entity) {
            return findAggregateChild(rootAggregate, id, silent);
        } else {
            return entity;
        }
    }

    private Place findPlace(String id, boolean silent) {
        Place place = rootAggregate.getPlace(id);
        if (null == place && !silent) {
            throw new InconsistentRequestException("No place with id " + id + " found");
        }
        return place;
    }

    private AggregateChild findAggregateChild(Aggregate a, String id, boolean silent) {
        int pos = id.indexOf('.');
        while (pos > 0) {
            String agName = id.substring(0, pos);
            id = id.substring(pos + 1);
            a = a.getAggregate(agName);
            pos = id.indexOf('.');
        }

        AggregateChild child = a.getAggregateChild(id);
        if (null == child && !silent) {
            throw new InconsistentRequestException("No aggregate child with id " + id + " found");
        }
        return child;
    }

    private void fireConnectionRequest(Collection<ConnectionEvent> events) {
        for (DispatcherListener listener : listeners) {
            listener.entityConnectionRequested(events);
        }
    }

    private void fireCreationRequest(Collection<CreationEvent> events) {
        for (DispatcherListener listener : listeners) {
            listener.entityCreationRequested(events);
        }
    }

    private void fireModelChangeEvent() {
        for (ModelChangeListener listener : modelChangeListeners) {
            listener.modelStructureChanged();
        }
    }

    private void firePlaceStateChangedEvent(Place place) {
        for (DispatcherListener listener : listeners) {
            listener.placeStateChanged(place);
        }
        fireModelChangeEvent();
    }

    private void firePropertiesRequestEvent(AggregateChild entity) {
        for (DispatcherListener listener : listeners) {
            listener.propertiesRequested(entity);
        }
    }

    public void propertiesRequested(String id) {
        firePropertiesRequestEvent(findAggregateChild(id));
    }

    private void fireStartedEvent(ExecutionType executionType) {
        for (DispatcherListener listener : listeners) {
            listener.executionStarted(executionType);
        }
    }

    public IdleModelAccessor getIdleModelAccessor() {
        Model tempModel = new ENetworksModel();
        tempModel.setRootAggregate(rootAggregate.createInstance(getModelName()));
        return new IdleModelAccessor(tempModel, tempModel.getRootAggregate());
    }

    public SavesManager.LoadListener getLoader() {
        return new Loader();
    }

    public NameValidator.NameManager getNameManager() {
        return new JessNameManager();
    }

    private void inputDeleted(Collection<Input> deletedInputs) {
        for (Input deletedInput : deletedInputs) {
            uidGenerator.setCurrentIdx(EntityType.INPUT, --inputIdx);
            Input[] inputs = rootAggregate.getInputs().toArray(new Input[rootAggregate.getInputs().size()]);
            int deletedIdx = rootAggregate.getInputNumber(deletedInput.getName());
            rootAggregate.disconnectInput(deletedInput);
            for (int i = deletedIdx; i < inputs.length - 1; i++) {
                Input input = inputs[i + 1];
                rootAggregate.disconnectInput(input);
                String oldName = input.getName();
                input.changeName(oldName.replaceAll((i + 1) + "$", Integer.toString(i)));
                rootAggregate.addInput(input, i);
                fireNameUpdatedEvent(oldName, input.getName());
            }
        }
    }

    private void fireNameUpdatedEvent(String oldName, String newName) {
        for (DispatcherListener listener : listeners) {
            listener.nameUpdated(oldName, newName);
        }
        fireModelChangeEvent();
    }

    private void outputDeleted(Collection<Output> deletedOutputs) {
        for (Output deletedOutput : deletedOutputs) {
            uidGenerator.setCurrentIdx(EntityType.OUTPUT, --outputIdx);
            Output[] outputs = rootAggregate.getOutputs().toArray(new Output[rootAggregate.getOutputs().size()]);
            int deletedIdx = rootAggregate.getOutputNumber(deletedOutput.getName());
            rootAggregate.disconnectOutput(deletedOutput);
            for (int i = deletedIdx; i < outputs.length - 1; i++) {
                Output output = outputs[i + 1];
                rootAggregate.disconnectOutput(output);
                String oldName = output.getName();
                output.changeName(oldName.replaceAll((i + 1) + "$", Integer.toString(i)));
                rootAggregate.addOutput(output, i);
                fireNameUpdatedEvent(oldName, output.getName());
            }
        }
    }

    // only when called onLoad
    private void populateEvents(List<CreationEvent> events, Collection<? extends AggregateChild> list) {
        for (AggregateChild entity : list) {
            EntityType type = getEntityType(entity);
//            logger.debug("entity=" + entity.getName() + " in=" + type.getCountInputs() + " out=" + type.getCountOutputs() + " serialized: " + type.serialize());
            CreationEvent evt = new CreationEvent(new EntityDetails(entity.getName(), type,
                    new ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.Coordinates(entity.getObjectCoordinates().
                    getX(), entity.getObjectCoordinates().getY())));
            events.add(evt);
        }
    }

    private Collection<ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.Coordinates> copyCoordinates(ArcCoordinates ac) {
        Collection<ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.Coordinates> coords =
                new ArrayList<ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.Coordinates>(
                ac.getCoordinates().size());
        for (Coordinates c : ac.getCoordinates()) {
            coords.add(new ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.Coordinates(c.getX(),
                    c.getY()));
        }
        return coords;
    }

    // only when called onLoad
    private void populateConnections(List<ConnectionEvent> events, Collection<? extends AggregateChild> list) {
        for (AggregateChild entity : list) {
            EntityDetails ed = new EntityDetails(entity.getName(), getEntityType(entity),
                    new ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.Coordinates(entity.getObjectCoordinates().getX(),
                    entity.getObjectCoordinates().getY()));
            if (entity instanceof Transition) {
                Transition at = (AbstractTransition) entity;

                for (int i = 0; i < at.getInputPlaces().size(); i++) {
                    Place p = at.getInputPlaces().get(i);
                    EntityType type = getEntityType(p);
                    EntityDetails from = new EntityDetails(p.getName(), type,
                            new ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.Coordinates(p.getObjectCoordinates().getX(),
                            p.getObjectCoordinates().getY()));

                    ArcCoordinates ac = rootAggregate.getArcCoordinates(null, p.getName(), null, entity.getName());
                    Collection<ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.Coordinates> coords = copyCoordinates(ac);

                    events.add(new ConnectionEvent(from, ed, "O0", "I" + i, coords));
                }

                for (int i = 0; i < at.getOutputPlaces().size(); i++) {
                    Place p = at.getOutputPlaces().get(i);
                    EntityType type = getEntityType(p);
                    EntityDetails to = new EntityDetails(p.getName(), type,
                            new ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.Coordinates(p.getObjectCoordinates().getX(),
                            p.getObjectCoordinates().getY()));

                    ArcCoordinates ac = rootAggregate.getArcCoordinates(null, entity.getName(), null, p.getName());
                    Collection<ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.Coordinates> coords = copyCoordinates(ac);

                    events.add(new ConnectionEvent(ed, to, "O" + i, "I0", coords));
                }
            } // instance of Transition
            else if (entity instanceof Queue) {
                Queue q = (Queue) entity;
                Place p = q.getInputPlace();
                if (p != null) {
                    EntityType type = getEntityType(p);
                    EntityDetails from = new EntityDetails(p.getName(), type,
                            new ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.Coordinates(p.getObjectCoordinates().getX(),
                            p.getObjectCoordinates().getY()));

                    ArcCoordinates ac = rootAggregate.getArcCoordinates(null, p.getName(), null, entity.getName());
                    Collection<ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.Coordinates> coords = copyCoordinates(ac);

                    events.add(new ConnectionEvent(from, ed, "O0", "I0", coords));
                }
                p = q.getOutputPlace();
                if (p != null) {
                    EntityType type = getEntityType(p);
                    EntityDetails to = new EntityDetails(p.getName(), type,
                            new ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.Coordinates(p.getObjectCoordinates().getX(),
                            p.getObjectCoordinates().getY()));

                    ArcCoordinates ac = rootAggregate.getArcCoordinates(null, entity.getName(), null, p.getName());
                    Collection<ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.Coordinates> coords = copyCoordinates(ac);

                    events.add(new ConnectionEvent(ed, to, "O0", "I0", coords));
                }
            } else if (entity instanceof Input) {
                Input i = (Input) entity;
                Place p = i.getOutputPlace();
                if (p != null) {
                    EntityType type = getEntityType(p);
                    EntityDetails to = new EntityDetails(p.getName(), type,
                            new ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.Coordinates(p.getObjectCoordinates().getX(),
                            p.getObjectCoordinates().getY()));

                    ArcCoordinates ac = rootAggregate.getArcCoordinates(null, entity.getName(), null, p.getName());
                    Collection<ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.Coordinates> coords = copyCoordinates(ac);

                    events.add(new ConnectionEvent(ed, to, "O0", "I0", coords));
                }
            } else if (entity instanceof Output) {
                Output o = (Output) entity;
                Place p = o.getInputPlace();
                if (p != null) {
                    EntityType type = getEntityType(p);
                    EntityDetails from = new EntityDetails(p.getName(), type,
                            new ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.Coordinates(p.getObjectCoordinates().getX(),
                            p.getObjectCoordinates().getY()));

                    ArcCoordinates ac = rootAggregate.getArcCoordinates(null, p.getName(), null, entity.getName());
                    Collection<ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.Coordinates> coords = copyCoordinates(ac);

                    events.add(new ConnectionEvent(from, ed, "O0", "I0", coords));
                }
            } else if (entity instanceof Aggregate) {
                List<String> inputs = JessUtils.getAggregateInputs(((AggregateDefinitionReference) entity).getAggregateDefinition());
                for (String iName : inputs) {
                    AggregateChild ac = findAggregateChild((Aggregate) entity, iName, true);
                    if (ac != null) {
                        String fullInputPath = JessUtils.getAggregateChildFullName(rootAggregate, (AggregateChild) ac);
                        fullInputPath = fullInputPath.substring(fullInputPath.indexOf('.') + 1);
                        EntityDetails from = null;
                        String sp = null; // source parent
                        String s = null; // source
                        String tp = entity.getName(); // source parent
                        String t = iName; // target
                        String connectionSource = null;
                        String connectionTarget = null;
                        Input in = (Input) ac;
                        if (in.getInputPlace() != null) {
                            Place p = in.getInputPlace();
                            from = new EntityDetails(p.getName(), getEntityType(p),
                                    new ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.Coordinates(p.getObjectCoordinates().getX(),
                                    p.getObjectCoordinates().getY()));
                            sp = null;
                            s = p.getName();
                            connectionSource = "O0";
                            connectionTarget = iName;
                        } // TODO check how it works
                        else if (in.getConnectedOutput() != null) {
                            continue; // will connect when testings outputs
                        } else {
                            continue;
                        }

                        ArcCoordinates acords = rootAggregate.getArcCoordinates(sp, s, tp, t);
//                        logger.debug("sp=" + sp + " s=" + s + " tp=" + tp + " t=" + t);
//                        logger.debug("getting arc coords: cords=" + acords);
                        Collection<ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.Coordinates> coords = copyCoordinates(acords);

                        events.add(new ConnectionEvent(from, ed, connectionSource, connectionTarget, coords));
                    }
                }

                List<String> outputs = JessUtils.getAggregateOutputs(((AggregateDefinitionReference) entity).getAggregateDefinition());
                for (String oName : outputs) {
//                    logger.debug("found output: " + oName + ". searching real");
                    AggregateChild ac = findAggregateChild((Aggregate) entity, oName, true);
//                    logger.debug("found real=" + ac);
                    if (ac != null) {
                        String fullOutputPath = JessUtils.getAggregateChildFullName(rootAggregate, (AggregateChild) ac);
//                        logger.debug("full output path=" + fullOutputPath);
                        fullOutputPath = fullOutputPath.substring(fullOutputPath.indexOf('.') + 1);
//                        logger.debug("corrected full output path=" + fullOutputPath);
                        EntityDetails to = null;
                        String sp = entity.getName(); // source parent
                        String s = oName; // source
                        String tp = null; // source parent
                        String t = null; // target
                        String connectionSource = null;
                        String connectionTarget = null;
                        Output out = (Output) ac;
                        if (out.getOutputPlace() != null) {
                            Place p = out.getOutputPlace();
                            to = new EntityDetails(p.getName(), getEntityType(p),
                                    new ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.Coordinates(p.getObjectCoordinates().getX(),
                                    p.getObjectCoordinates().getY()));
                            tp = null;
                            t = p.getName();
                            connectionSource = oName;
                            connectionTarget = "I0";
                        } // TODO check how it works
                        else if (!out.getConnectedInputs().isEmpty()) {
                            String inputPath = JessUtils.getAggregateChildName(getRootAggregate(), out.getConnectedInputs().get(0));
                            if (inputPath != null) {
                                tp = inputPath.substring(0, inputPath.indexOf('.'));
                                t = inputPath.substring(inputPath.indexOf('.') + 1);
                                AggregateChild aggregate = JessUtils.getAggregateChild(rootAggregate, tp);
                                to = new EntityDetails(aggregate.getName(), getEntityType(aggregate),
                                        new ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.Coordinates(aggregate.getObjectCoordinates().getX(),
                                        aggregate.getObjectCoordinates().getY()));
                                connectionSource = s;
                                connectionTarget = t;
                            }
                        } else {
                            continue;
                        }

                        ArcCoordinates acords = rootAggregate.getArcCoordinates(sp, s, tp, t);
                        Collection<ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.Coordinates> coords = copyCoordinates(acords);

                        events.add(new ConnectionEvent(ed, to, connectionSource, connectionTarget, coords));
                    }
                }
            }
        }
    }

    public EntityType getEntityType(AggregateChild entity) {
        EntityType type;
        if (entity instanceof Place) {
            type = EntityType.PLACE;
            type.setEntityConnectorsCount(1, 1);
        } else if (entity instanceof JTransition) {
            type = EntityType.JTRANSITION;
            type.setEntityConnectorsCount(((JTransition) entity).getInputPlaces().size(), 1);
            if (type.getCountInputs() < 2) {
                type.setEntityConnectorsCount(2, 1);
            }
        } else if (entity instanceof FTransition) {
            type = EntityType.FTRANSITION;
            type.setEntityConnectorsCount(1, ((FTransition) entity).getOutputPlaces().size());
            if (type.getCountOutputs() < 2) {
                type.setEntityConnectorsCount(1, 2);
            }
        } else if (entity instanceof XTransition) {
            type = EntityType.XTRANSITION;
            type.setEntityConnectorsCount(1, ((XTransition) entity).getOutputPlaces().size());
            if (type.getCountOutputs() < 2) {
                type.setEntityConnectorsCount(1, 2);
            }
        } else if (entity instanceof YTransition) {
            type = EntityType.YTRANSITION;
            type.setEntityConnectorsCount(((YTransition) entity).getInputPlaces().size(), 1);
            if (type.getCountInputs() < 2) {
                type.setEntityConnectorsCount(2, 1);
            }
        } else if (entity instanceof TTransition) {
            type = EntityType.TTRANSITION;
            type.setEntityConnectorsCount(1, 1);
        } else if (entity instanceof FIFOQueue) {
            type = EntityType.FIFO;
            type.setEntityConnectorsCount(1, 1);
        } else if (entity instanceof FIFOPriorityQueue) {
            type = EntityType.PFIFO;
            type.setEntityConnectorsCount(1, 1);
        } else if (entity instanceof LIFOQueue) {
            type = EntityType.LIFO;
        } else if (entity instanceof LIFOPriorityQueue) {
            type = EntityType.PLIFO;
        } else if (entity instanceof Input) {
            type = EntityType.INPUT;
            type.setEntityConnectorsCount(0, 1);
        } else if (entity instanceof Output) {
            type = EntityType.OUTPUT;
            type.setEntityConnectorsCount(1, 0);
        } else if (entity instanceof Aggregate) {
            // TODO fix inputs and outputs set
            type = EntityType.AGGREGATE;
            List<String> inputs;
            List<String> outputs;
            AggregateDefinition ad;
            if (entity.getClass().getCanonicalName().contains("AggregateDefinitionReference")) {
                ad = ((AggregateDefinitionReference) entity).getAggregateDefinition();
            } else {
                ad = (AggregateDefinition) entity;
            }
            Aggregate instance = ad.createInstance(ad.getName());
            inputs = JessUtils.getAggregateInputs(instance);
            outputs = JessUtils.getAggregateOutputs(instance);
            type.setEntityConnectorsNames(inputs.toArray(new String[inputs.size()]), outputs.toArray(new String[outputs.size()]));
        } else {
            throw new IllegalArgumentException("Entity type is not known for class " + entity.getClass());
        }
        return type;
    }

    public void stop() {
        if (isModelRunning()) {
            //TODO use proper api (when implemented in the core) for experiment stoppage
            fireFinishedEvent(currentExecutionType);
            currentExecutionType = null;
            currentExperimentManager = null;
        }
    }

    private boolean isModelRunning() {
        return null != currentExecutionType;
    }

    private void fireFinishedEvent(ExecutionType executionType) {
        for (DispatcherListener listener : listeners) {
            listener.executionFinished(executionType);
        }
    }

    public boolean validationOK() throws JessException {
        AggregateValidator validator = new AggregateValidator();
        ValidationResult result = validator.validate(
                rootAggregate.createInstance(rootAggregate.getName()), true);
        if (!result.passedValidation()) {
            StringBuilder sb = new StringBuilder();
            for (ValidationError err : result.getErrors()) {

                String fullAggrName = JessUtils.getAggregateChildFullName(err.getObjectWithError());
                fullAggrName = fullAggrName.substring(0, fullAggrName.length() - err.getObjectWithError().getName().length() - 1);

                sb.append("In ").append(fullAggrName).append(":<br/>").append(err.getObjectWithError().getName()).append(": ").append(err.getErrorMessage()).append("<br/>");
            }
            errorRenderer.renderError("Model validation failed<br/>", sb.toString());
            return false;
        } else {
            return true;
        }
    }

    public AggregateVariable getAggregateVariable(Aggregate a, String var) {
        Aggregate ad = a;
        int dotPos = var.lastIndexOf(".");
        if (dotPos > -1) {
            ad = (Aggregate) JessUtils.getAggregateChild(ad, var.substring(0, dotPos));
        }
        logger.debug("search var: " + var);
        if (ad != null) {
            logger.debug("ad=" + ad);
            return ad.getVariable(var.substring(dotPos + 1));
        }
        return null;
    }

    public String getVariableValue(Aggregate a, String var) {
        return getAggregateVariable(a, var).getValue().strValue();
    }

    public String getVariableValue(String var) {
        return getVariableValue(rootAggregate, var);
    }

// -------------------------- ENUMERATIONS --------------------------
    public enum ExecutionType {

        EXPERIMENT, EXPERIMENT_DEBUG
    }

// -------------------------- INNER CLASSES --------------------------
    public interface ModelChangeListener {

        void modelStructureChanged();
    }

    public interface DispatcherListener {

        void executionStarted(ExecutionType executionType);

        void executionFinished(ExecutionType executionType);

        /**
         * Executed when {@link JessCanvas.CanvasListener#entityCreated(
         * ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.event.CreationEvent)}
         * happens.
         *
         * @param entityType type of the newly created entity
         * @param id identifier of the newly created entity
         * @see JessCanvas.CanvasListener#entityCreated(
         * ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.event.CreationEvent)
         */
        void entityCreated(EntityType entityType, String id);

        void entityCreationRequested(Collection<CreationEvent> events);

//        void entityCreationFromListRequested(Collection<String> events);
        void entityConnectionRequested(Collection<ConnectionEvent> events);

        void entitiesDeleted(List<String> ids);

        /**
         * Event is fired in animation mode when the place loses or gains the
         * token.
         *
         * @param place place which state has changed
         */
        void placeStateChanged(Place place);

        void propertiesRequested(AggregateChild entity);

        void nameUpdated(String oldName, String newName);

        void canvasResized(ResizeEvent event);
    }

    public interface ExperimentProgressListener {

        void modelTimeChanged(double l);

        void modelStarted();

        void modelFinished();
    }

    private class FiringPlaceStateChangeListener implements PlaceStateChangedListener {

        public void placeStateChanged(Place place) {
        }
    }

    private class JessNameManager implements NameValidator.NameManager {

        public boolean isNameFree(String name, AggregateChild entity) {
            AggregateChild place = findPlace(name, true);
            AggregateChild activeInstance = findAggregateChild(rootAggregate, name, true);
            return ((place == null || place == entity) && (activeInstance == null || activeInstance == entity));
        }
    }

    private class Loader implements SavesManager.LoadListener {

        public void onLoad(final AggregateDefinition aggregate) {
            rootAggregate = aggregate;
            inputIdx = rootAggregate.getInputs().size();
            outputIdx = rootAggregate.getOutputs().size();

            for (Place p : rootAggregate.getPlaces()) {
                p.addStateChangeListener(placeStateChangedListener);
            }

//            List<String> events = new ArrayList<String>();
            List<CreationEvent> events = new ArrayList<CreationEvent>();
            populateEvents(events, rootAggregate.getPlaces());
            populateEvents(events, rootAggregate.getTransitions());
            populateEvents(events, rootAggregate.getQueues());
            populateEvents(events, rootAggregate.getInputs());
            populateEvents(events, rootAggregate.getOutputs());
            populateEvents(events, rootAggregate.getChildAggregates());
            fireCreationRequest(events);
            events.clear();

            List<ConnectionEvent> connectionEvents = new ArrayList<ConnectionEvent>();
            populateConnections(connectionEvents, rootAggregate.getTransitions());
            populateConnections(connectionEvents, rootAggregate.getQueues());
            populateConnections(connectionEvents, rootAggregate.getInputs());
            populateConnections(connectionEvents, rootAggregate.getOutputs());
            populateConnections(connectionEvents, rootAggregate.getChildAggregates());
            fireConnectionRequest(connectionEvents);

            for (Place p : rootAggregate.getPlaces()) {
                if (null != p.getToken()) {
                    firePlaceStateChangedEvent(p);
                }
            }

            if (uidGenerator != null) {
                for (Aggregate a : rootAggregate.getChildAggregates()) {
                    String template = a.getName();
                    Integer id = null;
                    try {
                        id = Integer.parseInt(template.substring(template.lastIndexOf('_') + 1));
                        template = template.substring(0, template.lastIndexOf('_'));
                    } catch (Exception e) {
                        template = a.getName();
                    }
                    uidGenerator.registerCustomId(template);
                    if (id != null) {
                        uidGenerator.addIndexToCustomID(template, id);
                    }
                }
            }
        }
    }

    /**
     * This class is responsible for starting of experiment.
     */
    private class JessExperimentManager implements ExperimentManager, ModelListener {

        private Experiment experiment;
        private final ENetworksModel model;
        private final AggregateInstance rootAggregateInstance;
        private final Collection<AggregateChild> subjectEntities = new HashSet<AggregateChild>();
        private final ExperimentSettings settings;
        private long factorRuns;
        private ExperimentProgressListener experimentListener;

        /**
         * Creates new instance of JessExperimentManager.
         *
         * @param settings Settings for launching the experiment.
         */
        private JessExperimentManager(ExperimentSettings settings) {
            this.settings = settings;
            model = new ENetworksModel();
            rootAggregateInstance = getRootAggregate().createInstance(getModelName());
            model.setRootAggregate(rootAggregateInstance);
            model.setFinishTime(settings.getModelingTime());
            model.addModelListener(JessExperimentManager.this);

            //ToDo Only one factor
            FactorEntry factorEntry = null;
            Iterator<FactorEntry> iterator = settings.getSecondaryStatisticsSettins().getFactors().iterator();
            if (iterator.hasNext()) {
                factorEntry = iterator.next();
            }

            if (this.settings.getSecondaryStatisticsSettins().isEnabled()) {
                AggregateVariable av = Dispatcher.this.getAggregateVariable(rootAggregateInstance, factorEntry.getVariable());
                Factor factor = new FactorImpl(av,
                        new StepValueGenerator(Double.parseDouble(factorEntry.getBegin()),
                        Double.parseDouble(factorEntry.getEnd()),
                        Double.parseDouble(factorEntry.getStep())));
                this.factorRuns = factor.numberOfValues();

                if (settings.getAutostopSettings().isEnabled()) {
                    Respond respondAutostop;
                    if (settings.getAutostopSettings().getParameter() == null) {
                        respondAutostop = new Respond(Dispatcher.this.getAggregateVariable(rootAggregateInstance, settings.getAutostopSettings().getResponse()));
                    } else {
                        respondAutostop = new Respond(this.settings.getAutostopSettings().getParameter().getPresentation());
                    }
                    AggregateChild autostopObject = findAggregateChild(rootAggregateInstance, settings.getAutostopSettings().getResponse(), true);
                    experiment = new AutoStopExperiment(model, factor,
                            respondAutostop,
                            autostopObject,
                            settings.getAutostopSettings().getConfidenceInterval(),
                            settings.getAutostopSettings().getConfidenceProbability());
                } else {
                    Respond respond;

                    if (this.settings.getSecondaryStatisticsSettins().getResponseType() == null) {
                        respond = new Respond(Dispatcher.this.getAggregateVariable(rootAggregateInstance, settings.getSecondaryStatisticsSettins().getResponse()));
                    } else {
                        respond = new Respond(this.settings.getSecondaryStatisticsSettins().getResponseType().getPresentation());
                    }


                    experiment = new CountDownExperiment(model, factor,
                            respond,
                            settings.getNumberOfRuns());
                }


                if (this.settings.getSecondaryStatisticsSettins().getResponseType() != null) {
                    AggregateChild aggregateChild = JessUtils.getAggregateChild(rootAggregateInstance, this.settings.getSecondaryStatisticsSettins().getResponse());
                    if (null != aggregateChild) {
                        if (aggregateChild instanceof Place) {
                            experiment.collectSecondaryStatisticsOn((Place) aggregateChild);
                        } else if (aggregateChild instanceof Queue) {
                            experiment.collectSecondaryStatisticsOn((Queue) aggregateChild);
                        } else if (aggregateChild instanceof Transition) {
                            experiment.collectSecondaryStatisticsOn((Transition) aggregateChild);
                        }
                    }
                }
            } else if (this.settings.getPrimaryStatisticSettings().isEnabled()) {
                if (this.settings.getAutostopSettings().isEnabled()) {
                    Respond respondAutostop;

                    if (settings.getAutostopSettings().getParameter() == null) {
                        respondAutostop = new Respond(Dispatcher.this.getAggregateVariable(rootAggregateInstance, settings.getAutostopSettings().getResponse()));
                    } else {
                        respondAutostop = new Respond(this.settings.getAutostopSettings().getParameter().getPresentation());
                    }

                    AggregateChild autostopObject = findAggregateChild(rootAggregateInstance, settings.getAutostopSettings().getResponse(), true);

                    this.experiment = new AutoStopExperiment(model,
                            respondAutostop,
                            autostopObject,
                            settings.getAutostopSettings().getConfidenceInterval(),
                            settings.getAutostopSettings().getConfidenceProbability());
                } else {
                    this.experiment = new CountDownExperiment(model, this.settings.getNumberOfRuns());
                }
            } else {
                throw new RuntimeException("Must be enabled at least one type of collection of statistics!");
            }


            if (settings.getPrimaryStatisticSettings().isEnabled()) {
                experiment.setIsPrimaryStatisticsShouldBeCollected(true);
                experiment.setDelayBeforeCollectingPrimaryStatistics(settings.getPrimaryStatisticSettings().
                        getStatisticsStart());

                if (this.settings.getPrimaryStatisticSettings().isCollectWithPeriod()) {
                    experiment.setPrimaryStatisticsDumpPeriod(settings.getPrimaryStatisticSettings().getStatisticsPeriod());
                } else {
                    experiment.setCollectedStatOnEvent(true);
                }
                AggregateChild aggregateChild = null;
                for (String entity : settings.getPrimaryStatisticSettings().getSubjectEntities()) {
                    aggregateChild = JessUtils.getAggregateChild(rootAggregateInstance, entity);
                    if (null != aggregateChild) {
                        if (aggregateChild instanceof Place) {
                            experiment.collectStatisticsOn((Place) aggregateChild);
                        } else if (aggregateChild instanceof Queue) {
                            experiment.collectStatisticsOn((Queue) aggregateChild);
                        } else if (aggregateChild instanceof Transition) {
                            experiment.collectStatisticsOn((Transition) aggregateChild);
                        }
                        subjectEntities.add(aggregateChild);
                    }
                }
            }
            logger.debug("created experiment: " + experiment.toString());
        }

        public Double[] getStatisticsDumpTimes() {
            return null; //ToDo Dummy
//            return experiment.StatisticsDumpTimes();
        }

        public void setExperimentListener(ExperimentProgressListener experimentListener) {
            this.experimentListener = experimentListener;
        }

        public void start() {
            currentExecutionType = Dispatcher.ExecutionType.EXPERIMENT;
            new Thread(new Runnable() {

                public void run() {
                    fireStartedEvent(currentExecutionType);
                    experiment.startExperiment();
                    stop();
                }
            }).start();
        }

        public void modelStarted(Model model) {
            if (null != experimentListener) {
                experimentListener.modelStarted();
            }
        }

        public void modelFinished(Model model) {
            if (null != experimentListener) {
                experimentListener.modelFinished();
            }
        }

        public void modelTimeChanged(Model model, double l) {
            if (null != experimentListener) {
                experimentListener.modelTimeChanged(l);
            }
        }

        public void stop() {
            if (!experiment.isExperimentFinished()) {
                experiment.stopExperiment();
            }

            model.removeModelListener(this);
            Dispatcher.this.stop();
        }

        public long getFactorRuns() {
            return factorRuns;
        }

        public double getModelingTime() {
            return settings.getModelingTime();
        }

        public int getExperimentRuns() {
            // TODO This is a braindead solution. It works in case of CountDownExperiment when user specifies number 
            // of runs, but if auto-stop is used number of experiment runs depends on experiment implementation
            return settings.getNumberOfRuns();
        }

        public ExperimentReport getExperimentReport() {
            return experiment.getReport();
        }

        public boolean isPrimatyStatisticsEnabled() {
            return this.settings.getPrimaryStatisticSettings().isEnabled();
        }

        public boolean isSecondaryStatisticsEnabled() {
            return this.settings.getSecondaryStatisticsSettins().isEnabled();
        }

        public void debug() {
            currentExecutionType = Dispatcher.ExecutionType.EXPERIMENT_DEBUG;
            experiment.stepExperiment();
            if (experiment.isExperimentFinished()) {
                Dispatcher.this.currentExperimentManager = null;
                Dispatcher.this.currentExecutionType = null;
            }
        }

        public void stopDebug() {
            model.removeModelListener(this);
            experiment.stopStepExperiment();
            currentExperimentManager = null;
        }
    }
}
