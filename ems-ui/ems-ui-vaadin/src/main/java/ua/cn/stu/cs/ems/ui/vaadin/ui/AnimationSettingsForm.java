package ua.cn.stu.cs.ems.ui.vaadin.ui;

import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.TextField;
import ua.cn.stu.cs.ems.ui.vaadin.dto.GlobalSettings;
import ua.cn.stu.cs.ems.ui.vaadin.validation.ValidatorFactory;

/**
 * @author n0weak
 */
class AnimationSettingsForm extends AbstractForm {

    private static final String WIDTH = "200px";

    AnimationSettingsForm(GlobalSettings.AnimationSettings settings) {
        super(WIDTH, new BeanItem<GlobalSettings.AnimationSettings>(settings), new AnimationSettingsFiledFactory());
    }

    private static class AnimationSettingsFiledFactory extends DefaultFieldFactory {

        @Override
        public Field createField(Item item, Object propertyId, Component uiContext) {
            Field f = super.createField(item, propertyId, uiContext);
            if ("animationDelay".equals(propertyId)) {
                TextField tf = (TextField) f;
                tf.addValidator(ValidatorFactory.getPositiveIntegerValidator());
                tf.setCaption("Delay (ms)");
            }
            else if ("animationTime".equals(propertyId)) {
                TextField tf = (TextField) f;
                tf.addValidator(ValidatorFactory.getPositiveIntegerValidator());
                tf.setCaption("Finish Time");
            }

            if (null != f) {
                ValidatorFactory.addRequiredValidator(f);
            }

            return f;
        }
    }
}
