package ua.cn.stu.cs.ems.ui.vaadin.ui;

import com.vaadin.addon.chameleon.ChameleonTheme;
import com.vaadin.ui.*;

/**
 * @author n0weak
 */
public abstract class AbstractSettingsWindow extends Window {

    private final static String COMMIT_BTN_NAME = "Apply";
    private final static String DISCARD_BTN_NAME = "Discard";

    protected AbstractSettingsWindow(String caption) {
        super(caption);

        setSizeUndefined();
        getContent().setSizeUndefined();

        setModal(true);
        setResizable(false);
    }

    protected AbstractSettingsWindow(String caption, String width) {
        super(caption);

        setModal(true);
        setWidth(width);
        setResizable(false);
    }

    protected void paintFooter(Layout layout) {
        HorizontalLayout footer = new HorizontalLayout();
        footer.setMargin(true, false, false, false);
        Button discardButton = createDiscardButton();
        footer.addComponent(discardButton);
        footer.setComponentAlignment(discardButton, Alignment.MIDDLE_LEFT);

        Button commitButton = createCommitButton();
        footer.addComponent(commitButton);
        footer.setComponentAlignment(commitButton, Alignment.MIDDLE_RIGHT);

        footer.setWidth("100%");
        footer.setHeight((commitButton.getHeight()*2) + "px");
        layout.addComponent(footer);
        
        if (layout instanceof AbstractOrderedLayout) {
            ((AbstractOrderedLayout)layout).setExpandRatio(footer, 1.0f);
        }
    }

    protected Button createDiscardButton() {
        Button btn = new Button(getDiscardButtonName(), new Button.ClickListener() {

            public void buttonClick(Button.ClickEvent event) {
                discard();
            }
        });
        btn.setStyleName(getButtonStyle());
        return btn;
    }

    protected Button createCommitButton() {
        Button btn = new Button(getCommitButtonName(), new Button.ClickListener() {

            public void buttonClick(Button.ClickEvent event) {
                if (apply()) {
                    if (getParent()!= null) {
                        (getParent()).removeWindow(AbstractSettingsWindow.this);
                    }
                }
            }
        });
        btn.setStyleName(getButtonStyle());
        return btn;
    }

    protected String getButtonStyle() {
        return ChameleonTheme.BUTTON_SMALL;
    }

    protected String getCommitButtonName() {
        return COMMIT_BTN_NAME;
    }

    protected String getDiscardButtonName() {
        return DISCARD_BTN_NAME;
    }

    protected void discard() {
    }

    protected abstract boolean apply();
}
