package ua.cn.stu.cs.ems.ui.vaadin.ui.hla;

import com.vaadin.data.Validator;
import com.vaadin.data.Validator.InvalidValueException;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.cn.stu.cs.ems.ui.vaadin.PropertiesListener;
import ua.cn.stu.cs.ems.ui.vaadin.WidgetManager;
import ua.cn.stu.cs.ems.ui.vaadin.ui.AbstractSettingsWindow;

/**
 *
 * @author slava
 */
public class ChooseFederationWindow extends AbstractSettingsWindow {

    final static Logger logger = LoggerFactory.getLogger(ActionsFederationWindow.class);
    private ComboBox federationName;
    private final WidgetManager wm;
    private final PropertiesListener secondStage;

    public ChooseFederationWindow(String caption, String width,
            WidgetManager wm, PropertiesListener listener) {
        super(caption);
        setWidth(width);
        this.wm = wm;
        secondStage = listener;
        paint();
    }

    @Override
    protected boolean apply() {
        boolean empty = wm.getHlaDispatcher().getFederates(
                (String) federationName.getValue()).isEmpty();
        if (federationName.isValid() && !empty) {
            Window parent = getParent();
            parent.removeWindow(this);
            secondStage.propertiesSet(federationName.getValue());
            return true;
        }
        if (federationName.isValid() && empty) {
            wm.getWindow().showNotification(
                    "Federation " + federationName.getValue() + " is empty",
                    Notification.TYPE_WARNING_MESSAGE);
        }
        return false;
    }

    protected void paint() {
        VerticalLayout layout = (VerticalLayout) getContent();
        layout.setSizeFull();

        federationName = new ComboBox();
        federationName.setCaption("Federation name");
        federationName.setWidth("100%");
        federationName.setRequired(true);

        for (String name : wm.getHlaDispatcher().listFederations()) {
            federationName.addItem(name);
        }

        federationName.addValidator(new Validator() {

            public void validate(Object value) throws InvalidValueException {
                if (!isValid(value)) {
                    if (value == null || value.toString().isEmpty()) {
                        throw new InvalidValueException("No name selected");
                    }
                }
            }

            public boolean isValid(Object value) {
                if (value != null && wm.getHlaDispatcher().federationExist(value.toString())) {
                    return true;
                }
                return false;
            }
        });
        layout.addComponent(federationName);
        layout.setExpandRatio(federationName, 1.0f);

        paintFooter(layout);
    }

    @Override
    protected String getCommitButtonName() {
        return "Choose";
    }
}
