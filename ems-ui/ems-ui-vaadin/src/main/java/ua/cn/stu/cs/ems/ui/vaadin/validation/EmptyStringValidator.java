package ua.cn.stu.cs.ems.ui.vaadin.validation;

import com.vaadin.data.Validator;
import org.apache.commons.lang.StringUtils;

/**
 * This validator is used for validating properties that do or do not allow empty values.
 *
 * @author n0weak
 */
public class EmptyStringValidator implements Validator {

    private static final String MSG = "Value cannot be empty!";

    public void validate(Object o) throws InvalidValueException {
        if (!isValid(o)) {
            throw new InvalidValueException(MSG);
        }
    }

    public boolean isValid(Object o) {
        return o != null && StringUtils.isNotBlank(o.toString());
    }
}
