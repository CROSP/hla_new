package ua.cn.stu.cs.ems.ui.vaadin.utils;

import com.vaadin.addon.chameleon.ChameleonTheme;
import com.vaadin.ui.Button;

import java.util.HashMap;
import java.util.Map;

/**
 * @author n0weak
 */
public class ButtonSelector {

    private static final String[] UNSELECTABLE_STYLES = {ChameleonTheme.BUTTON_BORDERLESS,
                                                         ChameleonTheme.BUTTON_ICON_ONLY};
    private static final String STYLE_SELECTED = ChameleonTheme.BUTTON_DOWN + " " + "selected";
    private static Map<Button, String[]> erasedStyles = new HashMap<Button, String[]>();

    public static void deselect(Button b) {
        String[] styles = erasedStyles.remove(b);
        if (null != styles) {
            for (String style : styles) {
                b.addStyleName(style);
            }
        }
        b.removeStyleName(STYLE_SELECTED);
    }

    public static void select(Button b) {
        String[] erased = new String[UNSELECTABLE_STYLES.length];

        String bStyle = b.getStyleName();
        b.addStyleName(STYLE_SELECTED);
        int i = 0;
        for (String style : UNSELECTABLE_STYLES) {
            if (bStyle.startsWith(style + " ") || bStyle.contains(" " + style + " ") || bStyle.endsWith(" " + style)) {
                b.removeStyleName(style);
                erased[i] = style;
                i++;
            }
        }

        if (i > 0) {
            erasedStyles.put(b, erased);
        }

        b.focus();
    }

    private ButtonSelector() {
    }
}
