package ua.cn.stu.cs.ems.ui.vaadin;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.AppenderBase;
import ch.qos.logback.core.encoder.Encoder;
import ua.cn.stu.cs.ems.ui.vaadin.ui.components.Console;

/**
 * @author Dmitry Krivenko <dmitrykrivenko@gmal.com>
 */
public class EmsAppenderGui extends AppenderBase<ILoggingEvent> {

    private Console console;
    Encoder<Object> encoder;

    public Encoder<Object> getEncoder() {
        return encoder;
    }

    public void setEncoder(Encoder<Object> encoder) {
        this.encoder = encoder;
    }

    public void setComponent(Console component) {
        this.console = component;
    }

    public Console getComponent() {
        return this.console;
    }

    @Override
    protected void append(ILoggingEvent e) {
        console = JessApp.getConsoleInstance();
        if (console != null) {
            console.log(e.getFormattedMessage());
            console.requestRepaint();
        }
    }

    @Override
    public void start() {
        super.start();
    }
}
