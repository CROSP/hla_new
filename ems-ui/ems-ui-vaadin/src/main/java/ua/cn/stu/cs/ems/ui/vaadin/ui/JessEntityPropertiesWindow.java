package ua.cn.stu.cs.ems.ui.vaadin.ui;

import com.vaadin.ui.Form;
import com.vaadin.ui.VerticalLayout;
import ua.cn.stu.cs.ems.ui.vaadin.PropertiesListener;
import ua.cn.stu.cs.ems.ui.vaadin.dto.JessEntityProperties;
import ua.cn.stu.cs.ems.ui.vaadin.validation.EclSyntaxValidator;
import ua.cn.stu.cs.ems.ui.vaadin.validation.NameValidator;

/**
 * @author n0weak
 */
public class JessEntityPropertiesWindow extends AbstractSettingsWindow {

    private final PropertiesListener<JessEntityProperties> propertiesListener;
    private final JessEntityProperties properties;
    private final EclSyntaxValidator eclValidator;
    private final NameValidator nameValidator;
    private Form form;

    public JessEntityPropertiesWindow(JessEntityProperties properties, EclSyntaxValidator eclValidator,
                                      NameValidator nameValidator,
                                      PropertiesListener<JessEntityProperties> propertiesListener) {
        super(properties.getName());
        this.propertiesListener = propertiesListener;
        this.properties = properties;
        this.eclValidator = eclValidator;
        this.nameValidator = nameValidator;
        paint();
    }

    protected void paint() {
        VerticalLayout layout = (VerticalLayout) getContent();
        form = new JessEntityPropertiesForm(properties, eclValidator, nameValidator);
        layout.addComponent(form);
        paintFooter(layout);
    }

    @Override
    protected boolean apply() {
        if (form.isValid()) {
            form.commit();
            propertiesListener.propertiesSet(properties);            
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    protected void discard() {
        form.discard();
    }
}
