package ua.cn.stu.cs.ems.ui.vaadin.ui;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;

import ua.cn.stu.cs.ems.ui.vaadin.FileManager.FileType;
import ua.cn.stu.cs.ems.ui.vaadin.PropertiesListener;
import ua.cn.stu.cs.ems.ui.vaadin.WidgetManager;

import java.util.Collection;
import ua.cn.stu.cs.ems.ui.vaadin.FileManager;

/**
 * @author n0weak
 */
public class OpenDialog extends AbstractSettingsWindow {

    static final String CAPTION = "Open";
    private static final String WIDTH = "500px";
    private final PropertiesListener<OpenRecord> propertiesListener;
    private final Collection<String> modelsFiles;
    private final Collection<String> aggTypesFiles;
    private FileManager.FileType fileType = null;
    private WidgetManager wm;
    private String fileName;

    public OpenDialog(Collection<String> modelsFiles, 
            Collection<String> aggTypesFiles, 
            final PropertiesListener<OpenRecord> propertiesListener, 
            WidgetManager wm) {
        super(CAPTION, WIDTH);
        this.propertiesListener = propertiesListener;
        this.modelsFiles = modelsFiles;
        this.aggTypesFiles = aggTypesFiles;
        this.wm = wm;
        paint();
    }

    protected void paint() {
        VerticalLayout layout = (VerticalLayout) getContent();
        HorizontalLayout listsLayout = new HorizontalLayout();
        listsLayout.setMargin(false);
        listsLayout.setWidth("100%");
        
        Panel p = new FilePanel(modelsFiles.toArray(new String[modelsFiles.size()]), aggTypesFiles.toArray(new String[aggTypesFiles.size()]),
                new FilePanel.SelectionListener() {

                    public void fileSelected(String fileName, FileManager.FileType fileType) {
                        OpenDialog.this.fileName = fileName;
                        OpenDialog.this.fileType = fileType;
                    }
                });

        p.setWidth("100%");
        p.setHeight("300px");
        p.addStyleName("padding-top-open-button");
        
        layout.addComponent(p);
        layout.setExpandRatio(p, 1.0f);

        Button b = createCommitButton();
        layout.addComponent(b);
        layout.setComponentAlignment(b, Alignment.BOTTOM_RIGHT);
    }

    @Override
    protected String getCommitButtonName() {
        return "Open";
    }

    @Override
    protected boolean apply() {
        if (null != fileName && fileType != null) {
             propertiesListener.propertiesSet(new OpenRecord(fileName, fileType));
            return true;
        }
        else {
            return false;
        }
    }
    
    public static class OpenRecord {
        private final String fileName;
        private final FileManager.FileType fileType;

        public OpenRecord(String fileName, FileType fileType) {
            this.fileName = fileName;
            this.fileType = fileType;
        }
        
        public String getFileName() {
            return fileName;
        }
        
        public FileManager.FileType getFileType() {
            return fileType;
        }
    }
}
