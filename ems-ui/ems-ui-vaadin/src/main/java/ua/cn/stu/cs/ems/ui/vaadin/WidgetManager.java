package ua.cn.stu.cs.ems.ui.vaadin;

import java.util.HashMap;
import java.util.Map;

import ua.cn.stu.cs.ems.ui.vaadin.ui.components.MainMenuBar;
import ua.cn.stu.cs.ems.ui.vaadin.ui.components.RegisteredComponent;
import ua.cn.stu.cs.ems.ui.vaadin.ui.components.Toolbar;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.DrawingMode;

import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.Window;
import org.vaadin.artur.icepush.ICEPush;
import ua.cn.stu.cs.ems.ui.vaadin.dto.GlobalSettings;
import ua.cn.stu.cs.ems.ui.vaadin.ui.components.MainContent;
import ua.cn.stu.cs.ems.ui.vaadin.ui.experiment.ExperimentSettings;
import ua.cn.stu.cs.ems.ui.vaadin.ui.hla.HlaDispatcher;

public class WidgetManager {

    private MainMenuBar menu = null;    /*  1 instanse  */
    private Toolbar toolbar = null;     /*  1 instanse  */
    private MainContent content;        /*  1 instanse  */
    private FileManager fileManager;    /*  1 instanse  */
    private SavesManager saveManager;   /*  1 instanse  */
    private ConcurrentButtonGroup modeSwitchers;    /*  1 instanse  */
    private ICEPush pusher;
    private GlobalSettings globalSettings;
    private ExperimentSettings experimentSettings;
    private HlaDispatcher hlaDispatcher;
    private Window mainWindow;
    private JessApp app;

    private Map<String, RegisteredComponent> regedit = new HashMap<String, RegisteredComponent>();

    /**
     * Default contructor, used for reset components data
     */
    public WidgetManager(JessApp app, Window w) {
        this.mainWindow = w;
        this.app = app;
    }
    
    public JessApp getApp() {
        return app;
    }
    
    public Window getWindow() {
        return mainWindow;
    }

    public void setDefaultMenu(MainMenuBar menu) {
        this.menu = menu;
    }

    public MainMenuBar getDefaultMenu() {
        return menu;
    }

    public void setDefaultToolbar(Toolbar tb) {
        this.toolbar = tb;
    }

    public Toolbar getDefaultToolbar() {
        return toolbar;
    }

    public ConcurrentButtonGroup getModeSwitchers() {
        return modeSwitchers;
    }

    public void setModeSwitchers(ConcurrentButtonGroup modeSwitchers) {
        this.modeSwitchers = modeSwitchers;
    }
    
    public void setFileManager(FileManager fileManager) {
        this.fileManager = fileManager;
    }
    
    public FileManager getFileManager() {
        return fileManager;
    }
    
    public void setSaveManager(SavesManager saveManager) {
        this.saveManager = saveManager;
    }
    
    public SavesManager getSaveManager() {
        return saveManager;
    }
    
    public void setIcePusher(ICEPush pusher) {
        this.pusher = pusher;
    }
    
    public ICEPush getIcePusher() {
        return pusher;
    }
    
    public void setGlobalSetings(GlobalSettings globalSettings) {
        this.globalSettings = globalSettings;
    }
    
    
    public GlobalSettings getGlobalSettings() {
        return globalSettings;
    }
    
    public void setExperimentSettings(ExperimentSettings experimentSettings) {
        this.experimentSettings = experimentSettings;
    }
    
    public ExperimentSettings getExperimentSettings() {
        return experimentSettings;
    }
    
    public void setHlaDispatcher(HlaDispatcher disp) {
        this.hlaDispatcher = disp;
    }
    
    public HlaDispatcher getHlaDispatcher() {
        return hlaDispatcher;
    }

    public void registerComponent(RegisteredComponent c, String id) {
        if (regedit.containsKey(id)) {
            return;
        }
        regedit.put(id, c);
        c.init(this);
    }

    public Component getRegisteredComponent(String id) {
        return regedit.get(id);
    }
    
    public boolean isRegistered(String id) {
    	return regedit.get(id) != null;
    }

    public void resizeComponents(Window.ResizeEvent e) {
        if (toolbar != null) {
            toolbar.resize();
        }
        if (content != null) {
            content.requestRepaintAll();
        }
    }

    public Button createModeSwitcher(Button b, final DrawingMode drawingMode) {
        b.addListener(new Button.ClickListener() {

            public void buttonClick(Button.ClickEvent clickEvent) {
                MainContent.WsRec activeWs = content.getActiveWorkspace();
                if (activeWs != null) {
                    activeWs.getJessCanvas().setDrawingMode(drawingMode);
                }
            }
        });

        modeSwitchers.addButton(b);
        return b;
    }
    
    public void setMainContent(MainContent content) {
        this.content = content;
    }
    
    public MainContent getMainContent() { 
        return content;
    }
    
    public float getNonHeight() {
        return (toolbar != null ? toolbar.getHeight() : 0) 
                + (menu != null ? menu.getHeight() : 0);
    }
}