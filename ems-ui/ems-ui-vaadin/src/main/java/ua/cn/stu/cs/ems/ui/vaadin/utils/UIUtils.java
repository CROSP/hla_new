package ua.cn.stu.cs.ems.ui.vaadin.utils;

import com.vaadin.addon.chameleon.ChameleonTheme;
import com.vaadin.terminal.Resource;
import com.vaadin.terminal.ThemeResource;
import com.vaadin.ui.Button;

/**
 * @author n0weak
 */
public class UIUtils {
	
    public static final String THEME = PropertiesReader.read(PropertiesReader.Property.THEME);

    // TODO remove all used this
    public static Button createIconizedButton(String description, String icon) {
        Button b = new Button();

        b.setWidth("30");
        b.setHeight("24");
        b.addStyleName(ChameleonTheme.BUTTON_ICON_ONLY);
        b.addStyleName(ChameleonTheme.BUTTON_BORDERLESS);
        b.setDescription(description);
        b.setIcon(getThemeResource("icons/" + icon));

        return b;
    }

    public static Resource getThemeResource(String relativeName) {
        return new ThemeResource("../" + THEME + "/" + relativeName);
    }
}
