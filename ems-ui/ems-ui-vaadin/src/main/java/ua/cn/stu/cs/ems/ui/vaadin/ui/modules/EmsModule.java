package ua.cn.stu.cs.ems.ui.vaadin.ui.modules;

import ua.cn.stu.cs.ems.ui.vaadin.WidgetManager;

/**
 *
 * @author leonid
 */
public interface EmsModule {
    
    void init(WidgetManager wm);
    
}
