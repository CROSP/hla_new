package ua.cn.stu.cs.ems.ui.vaadin.dto;

import ua.cn.stu.cs.ems.ui.vaadin.ui.input.table.Editable;

/**
 * @author n0weak
 */
@SuppressWarnings("unused")
public class Variable implements Editable<Variable> {

    private String name;
    private String value;

    public Variable() {
        this.name = "";
        this.value = "";
    }

    public Variable(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Attribute{" + "name='" + name + '\'' + ", value='" + value + '\'' + '}';
    }

    public Variable copy() {
        return new Variable(getName(), getValue());
    }
}
