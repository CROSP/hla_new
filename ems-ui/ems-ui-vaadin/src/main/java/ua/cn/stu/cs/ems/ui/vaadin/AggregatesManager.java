package ua.cn.stu.cs.ems.ui.vaadin;

import java.util.ArrayList;
import java.util.logging.Level;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.List;
import java.util.Vector;
import ua.cn.stu.cs.ems.ui.vaadin.utils.PropertiesReader;
import ua.cn.stu.cs.ems.ui.vaadin.utils.PropertiesReader.Property;
import ua.cn.stu.cs.ems.ui.vaadin.utils.db.AggregateEntity;
import ua.cn.stu.cs.ems.ui.vaadin.utils.db.AggregateEntityFactory;

/**
 *
 * @author leonid
 */
public class AggregatesManager {
    
    final static Logger logger = LoggerFactory.getLogger(AggregatesManager.class);
    
    private static AggregatesManager manager;
    private AggregateEntityFactory factory;
    private static String data_table_name;
    private static String data_table_pk;
    private static String data_table_cdata;
    private static String data_table_cname;
    private static String data_table_ctype;
    
    public static enum AggregateType {
        MODEL("model"), TYPE("type");
        String name;
        
        private AggregateType(String name) {
            this.name = name;
        }
        
        public String getName() {
            return name;
        }
        
        public String toString() {
            return getName();
        }
    }
    
    private AggregatesManager() {
        logger.debug("create");
        factory = AggregateEntityFactory.getFactory();
        logger.debug("factory=" + factory);
        
        data_table_name = PropertiesReader.read(Property.DB_TABLE_NAME);
        data_table_pk = PropertiesReader.read(Property.DB_TABLE_PK);
        data_table_cname = PropertiesReader.read(Property.DB_TABLE_C_NAME);
        data_table_cdata = PropertiesReader.read(Property.DB_TABLE_C_DATA);
        data_table_ctype = PropertiesReader.read(Property.DB_TABLE_C_TYPE);
    }
    
    public static AggregatesManager getFactory() {
        if (manager == null)
            manager = new AggregatesManager();
        return manager;
    }
    
    public List<AggregateEntity> list(AggregateType type) {
        return list(type, null);
    }
    
    public List<AggregateEntity> list(AggregateType type, String criteria) {
        String aCriteria = data_table_ctype + "='" + type + "'";
        
        if (criteria != null && !criteria.isEmpty()) {
            aCriteria += " AND " + criteria;
        }
        
        logger.debug("query: " + aCriteria);
        Vector<AggregateEntity> result = null;
        try {
            result = factory.findByCriteria(aCriteria);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        
        return result;
    }
    
    public List<String> listNames(AggregateType type) {
        List<AggregateEntity> ls = list(type);
        List<String> ret = new ArrayList<String>();
        
        for (AggregateEntity ae: ls) {
            ret.add(ae.getName());
        }
        return ret;
    }
    
    public List<AggregateEntity> get(String name, AggregateType type) throws Exception {
        return factory.findByCriteria(data_table_ctype + "='" + type.getName() 
                + "' AND " + data_table_cname + "='" + name + "'");
    }
    
    public void save(AggregateEntity ae) throws Exception {
        logger.debug("saving ae");
        factory.storeAggregateEntity(ae);
    }
    
    public boolean exist(String name, AggregateType type) throws Exception {
        return factory.countByCriteria(data_table_cname + "='" + name + 
                "' AND " + data_table_ctype + "='" + type.name() + "'") > 0;
    }
}
