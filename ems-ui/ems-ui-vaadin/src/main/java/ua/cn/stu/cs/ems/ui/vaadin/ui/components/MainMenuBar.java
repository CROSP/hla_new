/**
 * 
 */
package ua.cn.stu.cs.ems.ui.vaadin.ui.components;

import java.util.List;

import com.vaadin.terminal.Resource;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.MenuItem;

/**
 * @author leonid
 */
public class MainMenuBar extends MenuBar {

    public MainMenuBar() {
        super();
        setWidth("100%");
    }
    
    /**
     * Adding item to menu
     * @param path
     * @param command
     */
    public MenuItem setItem(String path, final ActionHandler command) {
        return setItem(path, null, command);
    }
    
    public MenuItem setItem(String path, Resource icon, final ActionHandler command) {
        if (path == null || path.isEmpty()) {
            return null;
        }
        
        MenuItem item = getMenuItem(path);
        if (item != null) {
            if (command != null) {
                item.setCommand(new MenuBar.Command() {
                    public void menuSelected(MenuItem selectedItem) {
                        
                        command.handle();
                    }
		});
            }
            if (icon != null) {
                item.setIcon(icon);
            }
        }
        return item;
    }
    
    /**
     * @param path
     * @return MenuItem - if not exist - create
     * Example: getMenuItem("System->Properties->PluginProps"); Separator is "->"
     */
    public MenuItem getMenuItem(String path) {
        MenuItem parent = null;

        String[] parsed_path = path.split("->");
        
        for (int i=0;i<parsed_path.length;i++) {
            String caption = parsed_path[i];
            MenuItem subitem = findMenuItem(parent, caption);
            parent = subitem;
        }
        return parent;
    }
    
    /**
     * Find item in MenuBar
     * @param parent
     * @param caption
     * @return MenuItem - if not exist create one
     */
    private MenuItem findMenuItem(MenuItem parent, String caption) {
        MenuBar.MenuItem found = null;
        if (caption != null && !caption.isEmpty()) {
            List<MenuBar.MenuItem> items = null;
            if (parent == null) {
                items = getItems();
            } else {
                items = parent.getChildren();
            }
            if (items != null) {
                for (int i=0;i<items.size();i++) {
                    MenuBar.MenuItem item = items.get(i);
                    if (caption.equals(item.getText())) {
                        found = item;
                        break;
                    }
                }
            }
            
            if (found == null) {
                if (parent == null) {
                    found = addItem(caption, null);
                } else {
                    found = parent.addItem(caption, null);
                }
            }
        }
        
        return found;
    }
    
    public void addSeparatorToPath(String path) {
        MenuItem item = getMenuItem(path);
        if (item != null) {
            item.addSeparator();
        }
    }
}
