package ua.cn.stu.cs.ems.ui.vaadin;

import com.vaadin.terminal.gwt.server.WebApplicationContext;
import java.io.IOException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author leonid
 */
public class EmsInit extends HttpServlet {

    public void init(ServletConfig _config) throws ServletException {
        super.init(_config);
        try {
            doGet(null, null);
            ServletContext context = _config.getServletContext();
            String home = context.getRealPath("");
            System.setProperty("EMS_HOME", home);
            System.setProperty("java.net.preferIPv4Stack", "true");

            String fileSeparator = System.getProperty("file.separator");
            System.setProperty("rti.home", home
                    + fileSeparator + "WEB-INF");
            System.setProperty("RTI_RID_FILE", home + fileSeparator + "WEB-INF"
                    + fileSeparator + "RTI.rid");
        } catch (IOException e) {
            System.out.println("Error when initialize");
        }
    }

    public void destroy() {
        super.destroy();
        // TODO add call stoped Hla server (if need, check!)
        System.out.println("Init Servlet Destroy");
    }

    public void doGet(HttpServletRequest _request, HttpServletResponse _response)
            throws IOException, ServletException {
    }
}
