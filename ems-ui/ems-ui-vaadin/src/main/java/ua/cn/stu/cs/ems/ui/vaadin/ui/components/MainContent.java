package ua.cn.stu.cs.ems.ui.vaadin.ui.components;

import com.vaadin.ui.TabSheet.SelectedTabChangeEvent;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ua.cn.stu.cs.ems.ui.vaadin.WidgetManager;

import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Layout;
import com.vaadin.ui.MenuBar.Command;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.TabSheet.CloseHandler;
import com.vaadin.ui.TabSheet.Tab;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Window;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import org.vaadin.artur.icepush.ICEPush;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.aggregates.Aggregate;
import ua.cn.stu.cs.ems.core.aggregates.AggregateChild;
import ua.cn.stu.cs.ems.core.aggregates.AggregateDefinition;
import ua.cn.stu.cs.ems.core.aggregates.AggregateDefinitionReference;
import ua.cn.stu.cs.ems.core.aggregates.AggregateRegistry;
import ua.cn.stu.cs.ems.ui.vaadin.Dispatcher;
import ua.cn.stu.cs.ems.ui.vaadin.ErrorRenderer;
import ua.cn.stu.cs.ems.ui.vaadin.FileManager;
import ua.cn.stu.cs.ems.ui.vaadin.PropertiesListener;
import ua.cn.stu.cs.ems.ui.vaadin.SavesManager;
import ua.cn.stu.cs.ems.ui.vaadin.UIManager;
import ua.cn.stu.cs.ems.ui.vaadin.dto.JessEntityProperties;
import ua.cn.stu.cs.ems.ui.vaadin.ui.JessEntityPropertiesWindow;
import ua.cn.stu.cs.ems.ui.vaadin.ui.modules.*;
import ua.cn.stu.cs.ems.ui.vaadin.utils.UIDGenerator;
import ua.cn.stu.cs.ems.ui.vaadin.utils.UIUtils;
import ua.cn.stu.cs.ems.ui.vaadin.validation.EclSyntaxValidator;
import ua.cn.stu.cs.ems.ui.vaadin.validation.NameValidator;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.JessCanvas;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.EntityType;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.event.ConnectionEvent;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.event.CreationEvent;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.event.ResizeEvent;

public class MainContent extends HorizontalLayout {

    final static Logger logger = LoggerFactory.getLogger(MainContent.class);
    private static final int DEFAULT_HEIGHT = 160;
    private static final int DEFAULT_WIDTH = 230;
    private static final Class[] enabledWidgets = new Class[]{
        TreeModelView.class,
        InstrumentsPanel.class,
        AgragatesPanel.class,
        Console.class
    };
    private static final Class[] enabledModules = new Class[]{
        ExportVariables.class, HlaManagmentModule.class
    };
    private static final String[] DEFAULT_CIDS_SHOW = new String[]{
        Console.CID
    };
    private static final String[] DEFAULT_CIDS_HIDDEN = new String[]{
        TreeModelView.CID, InstrumentsPanel.CID, AgragatesPanel.CID
    };
    public static final String VIEW_PATH = "View->";
    public static final String ICONS_PATH = "icons/menu/";
    private WidgetManager wm;
    private TabSheet lwp;
    private TabSheet rwp;
    private TabSheet bwp;
    private TabSheet cwp;
    private Map<RegisteredComponent, Tab> openedTabs = new HashMap<RegisteredComponent, Tab>();
    private Map<RegisteredComponent, Position> positions = new HashMap<RegisteredComponent, Position>();
    private Set<RegisteredComponent> closedTabs = new HashSet<RegisteredComponent>();
    private Map<Tab, WsRec> workspaces = new HashMap<Tab, WsRec>();
    private List<String> createdWorkspaceAggregatesNames =
            new LinkedList<String>();
    private static final String AGG_TEMPLATE_PREFIX = "Aggregate";
    private UIManager uiManager;
    private ErrorRenderer errorRenderer;
    private FileManager fileManager;
    private SavesManager savesManager;
    private ICEPush pusher;
    private DispatcherHandler dispatcherHandler = new DispatcherHandler();
    private SwitchWorkspaceListener wsSwitchListener = new SwitchWorkspaceListener();

    /**
     * Contractor that initialize main display components
     * @param wm
     * @param workComponent 
     */
    public MainContent(UIManager uiManager, ICEPush pusher, WidgetManager wm) {
        this.wm = wm;

        this.pusher = pusher;
        this.uiManager = uiManager;
        errorRenderer = new JessErrorRenderer();
        fileManager = new FileManager(errorRenderer);
        savesManager = new SavesManager(uiManager, errorRenderer, fileManager, wm);
        wm.setFileManager(fileManager);
        wm.setSaveManager(savesManager);

        setSizeFull();
        lwp = new TabSheet();
        rwp = new TabSheet();
        bwp = new TabSheet();
        cwp = new TabSheet();

        lwp.setWidth(DEFAULT_WIDTH + "px");
        rwp.setWidth(DEFAULT_WIDTH + "px");
        bwp.setHeight(DEFAULT_HEIGHT + "px");
        cwp.setSizeFull();

        lwp.setStyleName("tab");
        rwp.setStyleName("tab");
        bwp.setStyleName("tab");
        cwp.setStyleName("tab");

        addComponent(lwp);

        VerticalLayout layout = new VerticalLayout();
        layout.setSizeFull();

        layout.addComponent(cwp);
        layout.setExpandRatio(cwp, 1.0f);

        layout.addComponent(bwp);

        addComponent(layout);
        setExpandRatio(layout, 1.0f);

        addComponent(rwp);

        CloseTabHandler closer = new CloseTabHandler();
        lwp.setCloseHandler(closer);
        rwp.setCloseHandler(closer);
        bwp.setCloseHandler(closer);
        cwp.setCloseHandler(closer);
        cwp.addListener(wsSwitchListener);
    }

    public void init() {
        for (Class clazz : enabledWidgets) {
            try {
                Object o = clazz.newInstance();
                RegisteredComponent rc = (RegisteredComponent) o;
                rc.init(wm);
                addNewWidget(rc, rc.getCid(), rc.getDefaultPosition());
            } catch (Exception e) {
                logger.debug("INIT " + clazz.getName() + ": " + e.toString());
            }
        }
        for (TabSheet sheet : new TabSheet[]{lwp, bwp, rwp}) {
            if (sheet.getComponentCount() == 0) {
                sheet.setVisible(false);
            }
        }
        onStartWs();

        for (Class clazz : enabledModules) {
            try {
                Object o = clazz.newInstance();
                EmsModule module = (EmsModule) o;
                module.init(wm);
            } catch (Exception e) {
                logger.debug("INIT " + clazz.getName() + ": " + e.toString());
            }
        }
    }

    public void addNewWidget(RegisteredComponent widgetComponent, String cid,
            MainContent.Position position) {
        if (widgetComponent == null || null == widgetComponent.getTabCaption()
                || cid == null || cid.isEmpty()) {
            return;
        }

        positions.put(widgetComponent, position);
        openWidget(widgetComponent);
        setWidgetHandlers(widgetComponent);

        wm.registerComponent(widgetComponent, cid);
    }

    private TabSheet getSheet(Position pos) {
        switch (pos) {
            case LEFT: {
                return lwp;
            }
            case RIGHT: {
                return rwp;
            }
            case BOTTOM: {
                return bwp;
            }
            case CENTER: {
                return cwp;
            }
            default: {
                return null;
            }
        }
    }

    public void addWidget(RegisteredComponent widgetComponent,
            MainContent.Position position) {

        Tab tab = null;
        TabSheet sheet = getSheet(position);

        if (sheet != null) {
            sheet.setVisible(true);
            widgetComponent.setVisible(true);
            tab = sheet.addTab(widgetComponent,
                    widgetComponent.getTabCaption(), widgetComponent.getIcon());
            tab.setClosable(true);
            openedTabs.put(widgetComponent, tab);
            closedTabs.remove(widgetComponent);
            if (sheet.getComponentCount() != 0) {
                sheet.setVisible(true);
            }
            requestRepaintAll();
        } else {
            // TODO add exception code
        }
    }

    // TODO switch icons when changing visibility of component
    public void setWidgetHandlers(final RegisteredComponent widget) {
        MainMenuBar menu = wm.getDefaultMenu();
        MenuItem item = menu.getMenuItem(VIEW_PATH + widget.getTabCaption());

        String icon_name = widget.getTabCaption().replaceAll(" ", "_");
        logger.debug("setting icon " + icon_name + " for menu item for widget " + widget.getTabCaption());
        com.vaadin.terminal.Resource icon = UIUtils.getThemeResource(ICONS_PATH + icon_name + ".png");
        item.setIcon(icon);

        item.setCommand(new Command() {

            private final RegisteredComponent w = widget;

            public void menuSelected(MenuItem selectedItem) {
                switchWidgetVisibility(w);
            }
        });
    }

    public void switchWidgetVisibility(RegisteredComponent widget) {
        logger.debug("switching visibility of " + widget.getClass().getName());
        if (widget.isVisible()) {
            closeWidget(widget);
        } else {
            openWidget(widget);
        }
    }

    public void closeWidget(RegisteredComponent widget) {
        Tab tab = openedTabs.get(widget);
        if (tab != null) {
            openedTabs.remove(widget);
            closedTabs.add(widget);
            TabSheet sheet = getSheet(positions.get(widget));
            sheet.removeTab(tab);
            if (sheet.getComponentCount() == 0) {
                sheet.setVisible(false);
                requestRepaintAll();
            }
            widget.setVisible(false);
        }
    }

    public void openWidget(RegisteredComponent widgetComponent) {
        addWidget(widgetComponent, positions.get(widgetComponent));
    }

    public class CloseTabHandler implements CloseHandler {

        public void onTabClose(TabSheet tabsheet, Component tabContent) {
            if (tabContent instanceof RegisteredComponent) {
                RegisteredComponent content = (RegisteredComponent) tabContent;
                closeWidget(content);
            } else if (tabsheet == getSheet(Position.CENTER)) {
                closeWorkspace(tabsheet.getTab(tabContent));
            }
        }
    }

    public class SwitchWorkspaceListener implements TabSheet.SelectedTabChangeListener {

        public void selectedTabChange(SelectedTabChangeEvent event) {
            // select tree of this dispatcher
            Tab selected = event.getTabSheet().getTab(event.getTabSheet().getSelectedTab());
            if (selected != null) {
                logger.debug("switching ws to: " + selected.getCaption());
                TreeModelView tmv = (TreeModelView) wm.getRegisteredComponent(TreeModelView.CID);
                tmv.switchWorkspace(selected.getCaption());
            }
        }
    }

    public void requestRepaintCanvase(WsRec ws, int w, int h) {
//        logger.debug("repainting: " + w + "x" + h);
        JessCanvas jessCanvas = ws.getJessCanvas();
        Layout canvasPalette = ws.getJessPalette();

        canvasPalette.setHeight(h + "px");
        canvasPalette.setWidth(w + "px");
        jessCanvas.setHeight(h);
        jessCanvas.setWidth(w);
        jessCanvas.requestRepaintRequests();
        jessCanvas.requestRepaint();
    }

    private void onStartWs() {
        boolean openedTabsIsEmpty = true;
        for (RegisteredComponent rc : openedTabs.keySet()) {
            for (String cid : DEFAULT_CIDS_HIDDEN) {
                if (cid.equals(rc.getCid())) {
                    openedTabsIsEmpty = false;
                    break;
                }
            }
            if (!openedTabsIsEmpty) {
                break;
            }
        }

        if (openedTabsIsEmpty && workspaces.size() == 1) {
            for (String cid : DEFAULT_CIDS_HIDDEN) {
                RegisteredComponent widget = (RegisteredComponent) wm.getRegisteredComponent(cid);
                openWidget(widget);
            }
        } else if (!openedTabsIsEmpty && workspaces.size() == 0) {
            for (String cid : DEFAULT_CIDS_HIDDEN) {
                RegisteredComponent widget = (RegisteredComponent) wm.getRegisteredComponent(cid);
                closeWidget(widget);
            }
            for (String cid : DEFAULT_CIDS_SHOW) {
                RegisteredComponent widget = (RegisteredComponent) wm.getRegisteredComponent(cid);
                if (widget != null && widget.isVisible()) {
                    closeWidget(widget);
                    openWidget(widget);
                }
            }
        }
        requestRepaintAll();
    }

    @Override
    public void requestRepaintAll() {
        if (getWindow() == null) {
            return; // cant repaint on undefined window
        }

        float w = getWindow().getWidth()
                - (lwp.isVisible() ? lwp.getWidth() : 0)
                - (rwp.isVisible() ? rwp.getWidth() : 0);
        float h = getWindow().getHeight() - wm.getNonHeight();
        lwp.setWidth((DEFAULT_WIDTH) + "px");
        lwp.setHeight(h + "px");

        rwp.setWidth((DEFAULT_WIDTH) + "px");
        rwp.setHeight(h + "px");

        bwp.setWidth(w + "px");
        bwp.setHeight(DEFAULT_HEIGHT + "px");

        lwp.setVisible(lwp.getComponentCount() > 0);
        rwp.setVisible(rwp.getComponentCount() > 0);
        bwp.setVisible(bwp.getComponentCount() > 0);

        for (RegisteredComponent rc : positions.keySet()) {
            if (rc.isVisible()) {
                Position pos = positions.get(rc);
                if (pos == Position.LEFT || pos == Position.RIGHT) {
                    rc.setWidth((DEFAULT_WIDTH - 6) + "px"); // 10 - scroll
                    rc.setHeight((h - 55) + "px");
                } else if (pos == Position.BOTTOM) {
                    rc.setWidth((w - 2) + "px");
                    rc.setHeight((DEFAULT_HEIGHT - 28) + "px");
                }
            }
        }

        float cHeight = h - 24 - (bwp.isVisible() ? bwp.getHeight() : 0);
        cwp.setWidth((w - 1) + "px");
        cwp.setHeight((cHeight - 5) + "px");

        super.requestRepaintAll();
    }

    public WsRec getActiveWorkspace() {
        TabSheet sheet = getSheet(Position.CENTER);
        for (Tab wsTab : workspaces.keySet()) {
            if (sheet.getSelectedTab() == wsTab.getComponent()) {
                return workspaces.get(wsTab);
            }
        }
        return null;
    }

    public void scaleCanvasPalette(double scale) {
        WsRec ws = getActiveWorkspace();
        JessCanvas canvas = ws.getJessCanvas();
        Layout canvasPalette = ws.getJessPalette();

        canvas.scale((int) scale);
        canvasPalette.setWidth(canvas.getBaseWidth() * scale / (double) 100 + "px");
        canvasPalette.setHeight(canvas.getBaseHeight() * scale / (double) 100 + "px");
    }

    public Tab createNewWorkspace(WorkspaceType wsType) {
        return createNewWorkspace(Dispatcher.MODEL_NAME, wsType, null);
    }

    public Tab createNewWorkspace(String name, WorkspaceType wsType) {
        return createNewWorkspace(name, wsType, null);
    }

    /**
     * Using for creating new instances of jess canvas, dispatcher and display
     * their to new tab
     */
    public Tab createNewWorkspace(String name, WorkspaceType wsType, WsRec parent) {
//        logger.debug("creating workspace: name=" + name + " type=" + wsType
//                + " parent=" + parent);

        if (isWorkspaceExist(name, wsType)) {
//            logger.debug("ws " + name + " exist. Activating");
            activateWorkspace(name, wsType);
            return null; // not saved or default workspace
        }

//        logger.debug("initing variables for new workspace");
        UIDGenerator uidGenerator = new UIDGenerator();

        Panel palettePanel = new Panel();
        palettePanel.setSizeFull();
        Layout canvasPalette = new HorizontalLayout();
        canvasPalette.setSizeFull();
        JessCanvas jessCanvas = new JessCanvas();
        jessCanvas.setImmediate(true);

//        logger.debug("creating dispatcher");
        Dispatcher dispatcher =
                new Dispatcher(errorRenderer, uidGenerator,
                wm.getGlobalSettings().getAnimationSettings(),
                savesManager);

//        logger.debug("changing name to: " + name);
        dispatcher.getRootAggregate().changeName(name);
        dispatcher.addListener(dispatcherHandler);

        jessCanvas.setCanvasListener(dispatcher);
        canvasPalette.addComponent(jessCanvas);
        palettePanel.setContent(canvasPalette);
        palettePanel.setScrollable(true);

//        logger.debug("creating WsRec for workspace");
        final WsRec ws = new WsRec(name, dispatcher, uidGenerator, jessCanvas, canvasPalette, wsType);

//        logger.debug("created WsRes: " + ws);
        if (parent != null) {
//            logger.debug("this is child: adding to parent " + parent);
            parent.addChildWorkspace(ws);
        } else {
//            logger.debug("setting aggregate registry copy");
            ws.setRegistry(savesManager.getAggregateRegestryCopy());
//            logger.debug("setting aggregate registry copy: done");
        }

//        logger.debug("updating tree");
        TreeModelView tmv = (TreeModelView) wm.getRegisteredComponent(TreeModelView.CID);
        tmv.createWorkspace(ws.getWsName());

//        logger.debug("adding to main tab sheet workspace tab");
        TabSheet sheet = getSheet(Position.CENTER);
        Tab wsTab = sheet.addTab(palettePanel);

//        logger.debug("setting new caption");
        wsTab.setCaption(ws.getWsName());
        wsTab.setClosable(true);

        workspaces.put(wsTab, ws);
        activateWorkspace(ws.getWsName());

//        logger.debug("calling onStartWs");
        onStartWs();

//        logger.debug("adding model change listener");
        dispatcher.addModelChangeListener(new Dispatcher.ModelChangeListener() {

            private final WsRec workspace = ws;

            public void modelStructureChanged() {
                workspace.setSaveNeeded(true);
            }
        });

//        logger.debug("setting canvas size: width=" + sheet.getWidth() + " height=" + sheet.getHeight());
        requestRepaintCanvase(ws, (int) sheet.getWidth(), (int) sheet.getHeight());
//        logger.debug("workspace created: return tab");
        return wsTab;
    }

    public String generateAggregateName() {
        String name = AGG_TEMPLATE_PREFIX;
        for (int i = 0;; i++) {
            String newName = name + i;
            if (!createdWorkspaceAggregatesNames.contains(newName)) {
                createdWorkspaceAggregatesNames.add(newName);
                return newName;
            }
        }
    }

    /**
     * Using for detect if workspace exist
     * @param name
     * @param isChild false if it model, true if aggregate type
     * @return 
     */
    public boolean isWorkspaceExist(String name, WorkspaceType wsType) {
        name = wsType.prefix() + name;
        Tab ws = findWorkspaceByName(name);
        if (ws == null) {
            return false;
        } else {
            return true;
        }
    }

    public Tab findWorkspaceByName(String name) {
        for (Tab ws : workspaces.keySet()) {
            String search = ws.getCaption();
            if (name.equals(search)) {
                return ws;
            }
        }
        return null;
    }

    public void activateWorkspace(String name) {
        Tab ws = findWorkspaceByName(name);
        if (ws != null) {
            logger.debug("activating ws: " + name);
            getSheet(Position.CENTER).setSelectedTab(ws.getComponent());

            TreeModelView tmv = (TreeModelView) wm.getRegisteredComponent(TreeModelView.CID);
            tmv.switchWorkspace(name);
        }
    }

    public void activateWorkspace(String name, WorkspaceType wsType) {
        activateWorkspace(wsType.prefix() + name);
    }

    public void closeWorkspace(String name) {
        Tab wsTab = findWorkspaceByName(name);
        logger.debug("closing ... " + name + " tab=" + wsTab);
        closeWorkspace(wsTab);
    }

    public void closeWorkspace(final Tab ws) {
        if (ws != null) {

            logger.debug("Closing workspace " + ws.getCaption());
            final WsRec workspace = workspaces.get(ws);

            final PropertiesListener actionHandler = new PropertiesListener() {

                public void propertiesSet(Object props) {
                    String wsName = workspace.getWsName();
                    if (workspace.getWsType() == WorkspaceType.AGG_TYPE) {
                        createdWorkspaceAggregatesNames.remove(workspace.getDispatcher().getModelName());
                    }
                    workspaces.remove(ws);
                    workspace.setSaveNeeded(false);
                    getSheet(Position.CENTER).removeTab(ws);
                    WsRec active = getActiveWorkspace();
                    TreeModelView tmv = (TreeModelView) wm.getRegisteredComponent(TreeModelView.CID);
                    Collection<WsRec> childs = workspace.getChilds();
                    for (WsRec child : childs) {
                        child.resetSaveNeed();
                        closeWorkspace(child.getWsName());
                    }

                    if (active != null) {
                        tmv.removeWorkspace(wsName, active.getWsName());
                    } else {
                        tmv.removeWorkspace(wsName, null);
                    }
                    if (!workspace.isParent()) {
                        workspace.getParent().removeChildWorkspace(workspace);
                    }
                    onStartWs();
                    AgragatesPanel ap = (AgragatesPanel) wm.getRegisteredComponent(AgragatesPanel.CID);
                    ap.refresh();
                }
            };

            if (workspace.isSaveNeeded() || workspace.isChildsSaveNeed()) {
                if (wm.getGlobalSettings().getUISettings().isValidateModelsOnSaving()) {
                    if (!workspace.getWorkspaceValidator().validate()) {
                        return;
                    }
                }

                String msg = "Save before closing?";
                String width = "200px";

                if (workspace.getWsType() == WorkspaceType.AGG_TYPE) {
                    msg += " (Aggregate is using by others)";
                    width = "350px";
                } else {
                    if (workspace.isChildsSaveNeed()) {
                        msg += " (Model aggregates was changed and using by others)";
                        width = "500px";
                    }
                }

                uiManager.showConfirmDialog(width, msg, new PropertiesListener<Boolean>() {

                    public void propertiesSet(Boolean props) {
                        if (props) {
                            savesManager.onSave(workspace, actionHandler);
                        } else {
                            actionHandler.propertiesSet(null);
                        }
                    }
                });
            } else {
                actionHandler.propertiesSet(null);
            }
        }
    }

    public void renameWorkspace(WsRec workspace, String oldName) {
        Tab tab = findWorkspaceByName(oldName);
        tab.setCaption(workspace.getWsName());
    }

    public void reloadWorkspace(final WsRec root) {
        logger.debug("reloading workspace: " + root.getWsName());
        WsRec active = getActiveWorkspace();
        if (root == null || active == null) {
            return;
        }

        activateWorkspace(root.getWsName());
        TreeModelView tmv = (TreeModelView) wm.getRegisteredComponent(TreeModelView.CID);

        root.getJessCanvas().clear();
        tmv.removeAllItems();

        if (root.isParent()) {
            AggregateRegistry ar = savesManager.getAggregateRegestryCopy();
            root.setRegistry(ar);
            if (root.getWsType() == WorkspaceType.MODEL) {
                root.getDispatcher().getLoader().onLoad(
                        savesManager.getModel(root.getName(), ar));
            } else if (root.getWsType() == WorkspaceType.AGG_TYPE) {
                root.getDispatcher().getLoader().onLoad(
                        savesManager.getAggregateType(ar, root.getDispatcher().getModelName()));
            }
        } else {
            AggregateDefinition ad = ((AggregateDefinitionReference) root.getParent().getDispatcher().getRootAggregate().getAggregate(
                    root.getName())).getAggregateDefinition();
            root.getDispatcher().getLoader().onLoad(ad);
        }
        root.setSaveNeeded(false);

        logger.debug("realoaded workspace: " + root.getWsName());

        if (!root.getChilds().isEmpty()) {
            logger.debug("ROOT childs not empty");
            for (WsRec w : root.getChilds()) {
                logger.debug("parent " + root.getWsName() + " has child: " + w.getWsName());
            }

            for (WsRec child : new ArrayList<WsRec>() {

                {
                    addAll(root.getChilds());
                }
            }) {
                logger.debug("realoding child: " + child.getWsName());
                if (root.getDispatcher().getRootAggregate().getAggregate(child.getName()) != null) {
                    logger.debug("child exist after saving. reloading: " + child.getWsName());
                    reloadWorkspace(child);
                } else {
                    logger.debug("child not exist in " + root.getWsName()
                            + " after saving. child: " + child.getWsName());
                    logger.debug("closing child: " + child.getWsName());
                    child.setSaveNeeded(false);
                    closeWorkspace(child.getWsName());
                }
            }
        }

        activateWorkspace(active.getWsName());
        logger.debug("finish reloading ws: " + root.getWsName());
    }

    public void save(boolean saveAs) {
        final WsRec ws = getActiveWorkspace();
        logger.debug("save: saveAs=" + saveAs + " ws=" + ws);

        if (ws != null) {
            if (saveAs) {
                if (wm.getGlobalSettings().getUISettings().isValidateModelsOnSaving()) {
                    if (!ws.getWorkspaceValidator().validate()) {
                        return;
                    }
                }

                savesManager.onSaveAs(ws);
                return;
            }
            WsRec parent = ws;
            while (!parent.isParent()) {
                parent = parent.getParent();
            }

            if (wm.getGlobalSettings().getUISettings().isValidateModelsOnSaving()) {
                if (!parent.getWorkspaceValidator().validate()) {
                    return;
                }
            }

            final WsRec parentWorkspace = parent;
            if (ws.getWsType() == WorkspaceType.AGG_TYPE) {
                uiManager.showConfirmDialog("200px", "Save aggregate?", new PropertiesListener<Boolean>() {

                    public void propertiesSet(Boolean props) {
                        if (props) {
                            savesManager.onSave(parentWorkspace, new PropertiesListener() {

                                public void propertiesSet(Object props) {
                                    logger.debug("saving done. reloading");
                                    reloadWorkspace(parentWorkspace);
                                    AgragatesPanel ap = (AgragatesPanel) wm.getRegisteredComponent(AgragatesPanel.CID);
                                    ap.refresh();
                                }
                            });
                        }
                    }
                });
            } else {
                savesManager.onSave(ws, new PropertiesListener() {

                    public void propertiesSet(Object props) {
                        TreeModelView tmv = (TreeModelView) wm.getRegisteredComponent(TreeModelView.CID);
                        tmv.renameRoot(ws.getWsName());
                        AgragatesPanel ap = (AgragatesPanel) wm.getRegisteredComponent(AgragatesPanel.CID);
                        ap.refresh();
                        reloadWorkspace(ws);
                    }
                });
            }
        }
    }

    public void load() {
        savesManager.load();
    }

    private class JessErrorRenderer implements ErrorRenderer {

        public void renderError(String caption, String message) {
            MainContent.this.getWindow().showNotification(caption, message, Window.Notification.TYPE_ERROR_MESSAGE);
            wm.getIcePusher().push();
        }

        public void renderError() {
            renderError(DEFAULT_CAPTION, DEFAULT_MESSAGE);
        }
    }

    public void stopDispatchers() {
        for (WsRec ws : workspaces.values()) {
            ws.getDispatcher().stop();
        }
    }

    private class DispatcherHandler implements Dispatcher.DispatcherListener {

        private static final int DELAY = 1000;
        /**
         * One object is used for start/stop notifications to avoid misunderstandings in cases when execution 
         * is finished too fast and both notifications are shown
         */
        private final Window.Notification notification;

        private DispatcherHandler() {
            notification = new Window.Notification("", Window.Notification.TYPE_HUMANIZED_MESSAGE);
            notification.setDelayMsec(DELAY);
        }

        public void placeStateChanged(Place place) {
            synchronized (this) {
                TreeModelView tree = (TreeModelView) wm.getRegisteredComponent(TreeModelView.CID);

                if (place.isEmpty()) {
                    getActiveWorkspace().getJessCanvas().unmark(place.getName());
                    tree.pointPlace(place.getName(), false);
                } else {
                    getActiveWorkspace().getJessCanvas().mark(place.getName());
                    tree.pointPlace(place.getName(), true);
                }
                wm.getIcePusher().push();
            }
        }

        public void nameUpdated(String oldName, String newName) {
            getActiveWorkspace().getJessCanvas().updateName(oldName, newName);
            TreeModelView tree = (TreeModelView) wm.getRegisteredComponent(TreeModelView.CID);
            tree.renameItem(oldName, newName);
        }

        public void propertiesRequested(final AggregateChild entity) {
            if (entity instanceof Aggregate) {
                // WORK
                logger.debug("try to open entity: " + entity.getName());
                WsRec parent = getActiveWorkspace();
                createNewWorkspace(entity.getName(), WorkspaceType.AGG_TYPE, parent);
                WsRec child = getActiveWorkspace();
                AggregateDefinition childAd = ((AggregateDefinitionReference) entity).getAggregateDefinition();
                child.getDispatcher().getLoader().onLoad(childAd);
                child.setSaveNeeded(false);
            } else {
                final WsRec activeWs = getActiveWorkspace();
                MainContent.this.getWindow().addWindow(new JessEntityPropertiesWindow(
                        JessEntityProperties.readFrom(entity),
                        new EclSyntaxValidator(activeWs.getDispatcher().getIdleModelAccessor()),
                        new NameValidator(entity, activeWs.getDispatcher().getNameManager()),
                        new PropertiesListener<JessEntityProperties>() {

                            public void propertiesSet(JessEntityProperties props) {
                                if (!props.getName().equals(entity.getName())) {
                                    activeWs.getUidGenerator().take(props.getName(),
                                            activeWs.getDispatcher().getEntityType(entity));
                                    nameUpdated(entity.getName(), props.getName());
                                    // when name changed cords not changing automatically, so fixing
                                    activeWs.getDispatcher().fixCoordinates(entity, props.getName());
                                }
                                if (entity instanceof Place) {
                                    Place place = (Place) entity;
                                    Token t = place.getToken();
                                    props.commit();
                                    if (t == null && place.getToken() != null) {
                                        placeStateChanged(place);
                                    }
                                } else {
                                    props.commit();
                                }
                                activeWs.setSaveNeeded(true);
                            }
                        }));
            }
        }

        public void canvasResized(ResizeEvent event) {
            // Not used. Replaced with canvasResized(width, height);
            logger.debug("canvas resize need: " + event.getWidth() + " x " + event.getHeight());
            WsRec ws = getActiveWorkspace();
            if (ws != null) {
                requestRepaintCanvase(ws, event.getWidth(), event.getHeight());
            }
        }

        private void setReadOnly(boolean readOnly) {
            getActiveWorkspace().getJessCanvas().setReadOnly(readOnly);
            TreeModelView tree = (TreeModelView) wm.getRegisteredComponent(TreeModelView.CID);
            tree.setReadOnly(readOnly);
        }

        public void executionStarted(Dispatcher.ExecutionType executionType) {
            synchronized (this) {
                setReadOnly(true);
                wm.getIcePusher().push();
            }
        }

        public void executionFinished(Dispatcher.ExecutionType executionType) {
            synchronized (this) {
                setReadOnly(false);
                wm.getIcePusher().push();
            }
        }

        public void entityCreated(EntityType entityType, String id) {
            this.register(id, entityType);
        }

        public void entityCreationRequested(Collection<CreationEvent> events) {
            WsRec activeWs = getActiveWorkspace();
            for (CreationEvent event : events) {
                activeWs.getUidGenerator().take(event.getEntityDetails().getId(), event.getEntityDetails().getEntityType());
                register(event.getEntityDetails().getId(), event.getEntityDetails().getEntityType());
            }
            getActiveWorkspace().getJessCanvas().create(events);
        }

        public void entityConnectionRequested(Collection<ConnectionEvent> events) {
            getActiveWorkspace().getJessCanvas().connect(events);
        }

        public void entitiesDeleted(List<String> ids) {
            TreeModelView tmv = (TreeModelView) wm.getRegisteredComponent(TreeModelView.CID);
            for (String id : ids) {
                tmv.removeItem(id);
            }
        }

        /**
         * Addes new item to object tree.
         *
         * @param id         New item.
         * @param entityType Type of new entity. Used for group items into categories.
         */
        private void register(String id, EntityType entityType) {
            TreeModelView tree = (TreeModelView) wm.getRegisteredComponent(TreeModelView.CID);
            tree.register(id, entityType);
        }
//        public void entityCreationFromListRequested(Collection<String> events) {
//            getActiveWorkspace().getJessCanvas().createFromList(events);
//        }
    }

    public static class WorkspaceCreatingEvent {

        private WorkspaceType wsType;
        private String wsName;
        private String templateName;

        public WorkspaceCreatingEvent(String wsName, String templateName) {
            this.wsName = wsName;
            this.templateName = templateName;
        }

        public String getWsName() {
            return wsName;
        }

        public String getTemplateName() {
            return templateName;
        }
    }

    public static enum Position {

        LEFT, RIGHT, BOTTOM, CENTER
    };

    public static enum WorkspaceType {

        MODEL("M: "), AGG_TYPE("A: "), FEDERATION("F:");
        private String caption;

        WorkspaceType(String prefix) {
            this.caption = prefix;
        }

        @Override
        public String toString() {
            return caption;
        }

        public String prefix() {
            return caption;
        }
    };

    public static class WsRec {

        private String name;
        private Dispatcher dispatcher;
        private AggregateRegistry ar;
        private UIDGenerator uidGenerator;
        private JessCanvas canvas;
        private Layout jessPalette;
        private WsRec parent = null;
        // used only in parent Workspace
        private List<WsRec> childs = new ArrayList<WsRec>();
        private final WorkspaceType type;
        private boolean saveNeeded = false;
        private WorkspaceValidator workspaceValidator;

        public WsRec(String name, Dispatcher d, UIDGenerator uidg, JessCanvas canvas,
                Layout palette, WorkspaceType wsType) {
            this.dispatcher = d;
            this.uidGenerator = uidg;
            this.canvas = canvas;
            this.jessPalette = palette;
            this.ar = null;
            type = wsType;
            this.name = name;
            this.workspaceValidator = new WorkspaceValidator(this);
        }

        public WorkspaceType getWsType() {
            return type;
        }

        public Dispatcher getDispatcher() {
            return dispatcher;
        }

        // used for reseting
        public void setDispatcher(Dispatcher dispatcher) {
            this.dispatcher = dispatcher;
        }

        public String getWsName() {
            return getWsType().prefix() + name;
        }

        public UIDGenerator getUidGenerator() {
            return uidGenerator;
        }

        public JessCanvas getJessCanvas() {
            return canvas;
        }

        public Layout getJessPalette() {
            return jessPalette;
        }

        public void resetSaveNeed() {
            setSaveNeeded(false);
            for (WsRec child : getChilds()) {
                child.setSaveNeeded(false);
            }
        }

        public void setSaveNeeded(boolean save) {
            saveNeeded = save;
        }

        public boolean isChildsSaveNeed() {
            for (WsRec ws : getChilds()) {
                if (ws.isSaveNeeded() || ws.isChildsSaveNeed()) {
                    return true;
                }
            }
            return false;
        }

        public boolean isSaveNeeded() {
            return saveNeeded || isChildsSaveNeed();
        }

        public boolean isParent() {
            return parent == null;
        }

        public void setParent(WsRec parent) {
            this.parent = parent;
        }

        public WsRec getParent() {
            return parent;
        }

        public void addChildWorkspace(WsRec child) {
            child.setParent(this);
            childs.add(child);
        }

        public void removeChildWorkspace(WsRec child) {
            child.setParent(null);
            childs.remove(child);
        }

        public Collection<WsRec> getChilds() {
            return new ArrayList<WsRec>(childs);
        }

        public void setRegistry(AggregateRegistry ar) {
            if (isParent()) {
                this.ar = ar;
            } else {
                getParent().setRegistry(ar);
            }
        }

        public AggregateRegistry getRegistry() {
            if (ar == null) {
                return getParent().getRegistry();
            }
            return ar;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<WsRec> getChildsList() {
            List<WsRec> list = new ArrayList<WsRec>();
            for (WsRec child : getChilds()) {
                list.addAll(child.getChildsList());
            }
            list.add(this);
            return list;
        }

        public WorkspaceValidator getWorkspaceValidator() {
            return workspaceValidator;
        }
    }

    private static class WorkspaceValidator {

        private final WsRec workspace;

        public WorkspaceValidator(WsRec workspace) {
            this.workspace = workspace;
        }

        public boolean validate() {

            WsRec root = workspace;
            while (!root.isParent()) {
                root = root.getParent();
            }

            return root.getDispatcher().validationOK();
        }
    }
}
