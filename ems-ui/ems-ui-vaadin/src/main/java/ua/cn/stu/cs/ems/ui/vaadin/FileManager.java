package ua.cn.stu.cs.ems.ui.vaadin;

import ua.cn.stu.cs.ems.ui.vaadin.utils.PropertiesReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;

/**
 * @author n0weak
 */
public class FileManager {

    final static Logger logger = LoggerFactory.getLogger(FileManager.class);
    private static final String EXT = ".xml";
    private static final String MODELS_DIR = "models";
    private static final String AGGREGATES_DIR = "aggregates";
    private static final String FOM_FILES_DIR = "hla";
    private File dir;
    private final ErrorRenderer errorRenderer;
    
    public static enum FileType {
        MODEL, AGGREGATE_TYPE, FOM
    }

    public FileManager(ErrorRenderer errorRenderer) {
        this.errorRenderer = errorRenderer;
    }
    
    public Collection<String> listFiles(FileType listType) {
        Collection<String> list = null;
        
        switch (listType) {
            case MODEL: {
                list = listFiles(MODELS_DIR);
                break;
            }
            case AGGREGATE_TYPE: {
                list = listFiles(AGGREGATES_DIR);
                break;
            }
            case FOM:
                list = listFiles(FOM_FILES_DIR);
                break;
            default: {
                list = new ArrayList<String>();
                break;
            }
        }
        
        return list;
    }
    
    public Collection<String> listFiles(String path) {
        try {
            File f;
            f = getFile(path);
            f.mkdirs();
            File[] files = f.listFiles();
            Collection<String> fileNames = new ArrayList<String>(files.length);
            for (File file : files) {
                fileNames.add(file.getName());
            }
            return fileNames;
        } catch (Exception e) {
            String msg = "Failed to obtain list of the saved files in path " + path;
            logger.error(msg, e);
            errorRenderer.renderError("Error", msg);
            return new ArrayList<String>();
        }
    }
    
    public Collection<String> listFiles() {
        try {
            File f;
            f = getFile(AGGREGATES_DIR);
            f.mkdirs();
            f = getFile(MODELS_DIR);
            f.mkdirs();
            File[] files = f.listFiles();
            Collection<String> fileNames = new ArrayList<String>(files.length);
            for (File file : files) {
                fileNames.add(file.getName());
            }
            return fileNames;
        } catch (Exception e) {
            String msg = "Failed to obtain list of the saved files";
            logger.error(msg, e);
            errorRenderer.renderError("Error", msg);
            return new ArrayList<String>();
        }
    }

    public File getFile(String fileName) {
        return new File(getDir(), fileName);
    }
    
    public File getFomFile(String fomName) {
        new File(getDir() + System.getProperty("file.separator") + 
                getFomDir()).mkdirs();
        return new File(getDir() + System.getProperty("file.separator") + 
                getFomDir(), fomName + getDefaultExtension());
    }

    public String getDefaultExtension() {
        return EXT;
    }

    public String getModelsDir() {
        return MODELS_DIR;
    }
    
    public String getFomDir() {
        return FOM_FILES_DIR;
    }

    private File getDir() {
        if (null == dir) {
            String path = getPath();
            dir = new File(path);
            if (!dir.exists()) {
                dir.mkdirs();
            }
        }
        return dir;
    }

    private String getPath() {
        String value = PropertiesReader.read(PropertiesReader.Property.DATA_DIR);
        // Get path to user home directory
        String homeDir = System.getProperty("user.home");
        // Java doesn't substitute "~" symbol with path of user home dir
        String path = value.replaceFirst("~", homeDir);
        
        return path;
    }
}
