package ua.cn.stu.cs.ems.ui.vaadin.ui.input.table;

/**
 * Represents the entity which can be edited via {@link InputTable}.
 * All the implementing classes must have an empty-arg constructor.
 *
 * @author n0weak
 */
public interface Editable<T> {

    T copy();
}
