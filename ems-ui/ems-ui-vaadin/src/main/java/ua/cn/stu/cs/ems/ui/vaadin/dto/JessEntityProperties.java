package ua.cn.stu.cs.ems.ui.vaadin.dto;

import org.apache.commons.lang.StringUtils;
import ua.cn.stu.cs.ems.core.AbstractTransition;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.aggregates.AggregateChild;
import ua.cn.stu.cs.ems.core.transitions.AbstractSpecialTransition;
import ua.cn.stu.cs.ems.core.transitions.functions.*;
import ua.cn.stu.cs.ems.ecli.Value;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import ua.cn.stu.cs.ems.core.queues.AbstractPriorityQueue;
import ua.cn.stu.cs.ems.core.queues.DefaultTokenPriorityFunction;
import ua.cn.stu.cs.ems.core.queues.InterpretedTokenPriorityFunction;
import ua.cn.stu.cs.ems.core.queues.TokenPriorityFunction;

/**
 * @author n0weak
 */
@SuppressWarnings("unused")
public class JessEntityProperties {

    private String name;
    protected final AggregateChild entity;

    public static JessEntityProperties readFrom(AggregateChild entity) {
        if (entity instanceof Place) {
            return new PlaceProperties((Place) entity);
        }
        else if (entity instanceof AbstractSpecialTransition) {
            return new SpecialTransitionProperties((AbstractSpecialTransition) entity);
        }
        else if (entity instanceof AbstractTransition) {
            return new TransitionProperties((AbstractTransition) entity);
        }
        else if (entity instanceof AbstractPriorityQueue) {
            return new PriorityQueueProperties((AbstractPriorityQueue)entity);
        }
        else {
            return new JessEntityProperties(entity);
        }
    }

    public JessEntityProperties(AggregateChild entity) {
        this.name = entity.getName();
        this.entity = entity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public AggregateChild getEntity() {
        return entity;
    }

    public void commit() {
        entity.changeName(getName());
    }

    public static class TransitionProperties extends JessEntityProperties {

        private String delayFunction;
        private String transformationFunction;

        public TransitionProperties(AbstractTransition t) {
            super(t);

            TransformationFunction tf = t.getTransformationFunction();
            DelayFunction df = t.getDelayFunction();
            transformationFunction = tf instanceof InterpretedTransformationFunction
                                     ? ((InterpretedTransformationFunction) tf).getSource() : "";
            delayFunction = df instanceof InterpretedDelayFunction ? ((InterpretedDelayFunction) df).getSource() : "";
        }

        public String getDelayFunction() {
            return delayFunction;
        }

        public void setDelayFunction(String delayFunction) {
            this.delayFunction = delayFunction;
        }

        public String getTransformationFunction() {
            return transformationFunction;
        }

        public void setTransformationFunction(String transformationFunction) {
            this.transformationFunction = transformationFunction;
        }

        @Override
        public void commit() {
            super.commit();
            AbstractTransition t = (AbstractTransition) entity;
            TransformationFunction tf = StringUtils.isNotBlank(transformationFunction)
                                        ? new InterpretedTransformationFunction(transformationFunction)
                                        : new DefaultTransformationFunction();
            DelayFunction df = StringUtils.isNotBlank(delayFunction) ? new InterpretedDelayFunction(delayFunction)
                               : new DefaultDelayFunction();
            t.setTransformationFunction(tf);
            t.setDelayFunction(df);
        }
    }

    public static class SpecialTransitionProperties extends TransitionProperties {

        private String permittingFunction;

        public SpecialTransitionProperties(AbstractSpecialTransition t) {
            super(t);
            PermitingFunction pf = t.getPermitingFunction();
            permittingFunction = pf instanceof InterpretedPermitingFunction ? ((InterpretedPermitingFunction) pf).
                    getSource() : "";
        }

        public String getPermittingFunction() {
            return permittingFunction;
        }

        public void setPermittingFunction(String permittingFunction) {
            this.permittingFunction = permittingFunction;
        }

        @Override
        public void commit() {
            super.commit();
            AbstractSpecialTransition t = (AbstractSpecialTransition) entity;
            PermitingFunction pf = StringUtils.isNotBlank(permittingFunction) ? new InterpretedPermitingFunction(
                    permittingFunction) : new DefaultPermitingFunction();
            t.setPermitingFunction(pf);
        }
    }

    public static class PlaceProperties extends JessEntityProperties {

        public PlaceProperties(Place place) {
            super(place);
            List<Variable> attributes = new ArrayList<Variable>();
            if (place.getToken() != null) {
                for (String name : place.getToken().getAttributeNames()) {
                    Object value = place.getToken().getValue(name);
                    attributes.add(new Variable(name, value != null ? value.toString() : ""));
                }
            }
            setAttributes(attributes);
        }
        private Collection<Variable> attributes;
        private String name;

        public Collection<Variable> getAttributes() {
            return attributes;
        }

        public void setAttributes(Collection<Variable> attributes) {
            this.attributes = attributes;
        }

        @Override
        public void commit() {
            super.commit();

            Place place = (Place) entity;
            if (!attributes.isEmpty()) {
                Token token = new Token();
                for (Variable attribute : attributes) {
                    token.setValue(attribute.getName(),
                                   attribute.getValue() != null && !attribute.getValue().equals("")
                                   ? new Value(attribute.getValue()) : new Value());
                }
                place.setToken(null);
                place.setToken(token);
            }
        }
    }
    
    public static class PriorityQueueProperties extends JessEntityProperties {

        private String priorityFunction;

        public PriorityQueueProperties(AbstractPriorityQueue q) {
            super(q);

            TokenPriorityFunction tf = q.getPriorityFunction();
            
            priorityFunction = tf instanceof InterpretedTokenPriorityFunction ?
                    ((InterpretedTokenPriorityFunction)tf).getSource() : "";
        }

        public String getPriorityFunction() {
            return priorityFunction;
        }

        public void setPriorityFunction(String priorityFunction) {
            this.priorityFunction = priorityFunction;
        }


        @Override
        public void commit() {
            super.commit();
            AbstractPriorityQueue q = (AbstractPriorityQueue) entity;
            TokenPriorityFunction tpf = StringUtils.isNotBlank(priorityFunction)
                                        ? new InterpretedTokenPriorityFunction(priorityFunction)
                                        : new DefaultTokenPriorityFunction();
            q.setPriorityFunction(tpf);
        }
    }
}
