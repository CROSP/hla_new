package ua.cn.stu.cs.ems.ui.vaadin.validation;

import com.vaadin.data.Validator;

/**
 * @author n0weak
 */
public abstract class AbstractValidator implements Validator {

    private final String errMessage;

    protected AbstractValidator(String errMessage) {
        this.errMessage = errMessage;
    }

    public void validate(Object o) throws InvalidValueException {
        if (!isValid(o)) {
            throw new InvalidValueException(errMessage);
        }
    }
}
