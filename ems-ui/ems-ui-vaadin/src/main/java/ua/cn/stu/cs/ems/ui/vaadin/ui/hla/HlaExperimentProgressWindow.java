package ua.cn.stu.cs.ems.ui.vaadin.ui.hla;

import com.vaadin.addon.chameleon.ChameleonTheme;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Window;
import org.vaadin.artur.icepush.ICEPush;
import ua.cn.stu.cs.ems.ui.vaadin.Dispatcher;
import ua.cn.stu.cs.ems.ui.vaadin.ExperimentManager;
import ua.cn.stu.cs.ems.ui.vaadin.ui.experiment.ExperimentProgressWindow;

/**
 *
 * @author Dmitry Krivenko <dmitrykrivenko@gmal.com>
 */
public class HlaExperimentProgressWindow extends Window implements Dispatcher.ExperimentProgressListener {

    /**
     * Defines period of {@link #currentProgress} updates. Too intense updates
     * lead to progress indication failure - its value freezes.
     */
    private static final String START_CAPTION = "Experiment is running...";
    private static final String FINISH_CAPTION = "Experiment finished";
    private static final String WINDOW_WIDTH = "300px";
    private static final String WINDOW_HEIGHT = "90px";
//    private final Button stopBtn;
    private final Button reportBtn;
    private final Button exitButton;
    private final ICEPush pusher;

    public HlaExperimentProgressWindow(final ExperimentManager manager, ICEPush pusher, final ExperimentProgressWindow.ExperimentWindowListener listener) {
        super(START_CAPTION);
        this.pusher = pusher;

        manager.setExperimentListener(this);

        getContent().setSizeUndefined();
        getContent().setWidth(WINDOW_WIDTH);
        setResizable(false);
        setClosable(false);
        setModal(true);

        HorizontalLayout h_layout = new HorizontalLayout();
        h_layout.setSizeFull();

        reportBtn = new Button("Report");
        reportBtn.setStyleName(ChameleonTheme.BUTTON_SMALL);
        reportBtn.setEnabled(false);
        reportBtn.addListener(new Button.ClickListener() {

            public void buttonClick(Button.ClickEvent clickEvent) {
                listener.showReport(manager.getExperimentReport());
                HlaExperimentProgressWindow.this.getParent().removeWindow(HlaExperimentProgressWindow.this);
            }
        });
        h_layout.addComponent(reportBtn);
        h_layout.setComponentAlignment(this.reportBtn, Alignment.MIDDLE_LEFT);
        h_layout.setExpandRatio(reportBtn, 1.0f);
        this.reportBtn.setWidth("100px");

//        stopBtn = new Button("Stop");
//        stopBtn.setStyleName(ChameleonTheme.BUTTON_SMALL);
//        stopBtn.addListener(new Button.ClickListener() {
//
//            public void buttonClick(Button.ClickEvent clickEvent) {
//                listener.stopExperiment();
//                HlaExperimentProgressWindow.this.getParent().removeWindow(HlaExperimentProgressWindow.this);
//            }
//        });
//        h_layout.addComponent(stopBtn);
//        h_layout.setComponentAlignment(this.stopBtn, Alignment.MIDDLE_CENTER);

        this.exitButton = new Button("Exit", new Button.ClickListener() {

            public void buttonClick(Button.ClickEvent event) {
                HlaExperimentProgressWindow.this.getParent().removeWindow(HlaExperimentProgressWindow.this);
            }
        });
        exitButton.setWidth("100px");
        exitButton.setStyleName(ChameleonTheme.BUTTON_SMALL);
        exitButton.setEnabled(false);
        h_layout.addComponent(exitButton);
        h_layout.setComponentAlignment(exitButton, Alignment.MIDDLE_RIGHT);
        h_layout.setExpandRatio(exitButton, 1.0f);

        this.addComponent(h_layout);
    }

    public void modelTimeChanged(double l) {
    }

    public void modelStarted() {
        synchronized (getApplication()) {
            //stopBtn.setEnabled(true);
            reportBtn.setEnabled(false);
            exitButton.setEnabled(false);
        }
    }

    public void modelFinished() {
        synchronized (getApplication()) {
            setCaption(FINISH_CAPTION);
            //stopBtn.setEnabled(false);
            reportBtn.setEnabled(true);
            this.exitButton.setEnabled(true);
            pusher.push();
        }
    }
}
