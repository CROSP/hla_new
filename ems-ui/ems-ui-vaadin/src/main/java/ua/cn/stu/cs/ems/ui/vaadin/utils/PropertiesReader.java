package ua.cn.stu.cs.ems.ui.vaadin.utils;

import java.io.InputStream;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author n0weak
 */
public class PropertiesReader {

    final static Logger logger = LoggerFactory.getLogger(PropertiesReader.class);
    private static final String PROPS_FILE = "jess.properties";
    private static final Properties props = loadProperties();

    public enum Property {

        DATA_DIR("data.dir"),
        THEME("ui.theme"),
        
        DB_CONNECTION_HOST("db.connection.host"),
        DB_CONNECTION_PORT("db.connection.port"),
        DB_CONNECTION_USERNAME("db.connection.username"),
        DB_CONNECTION_PASSWORD("db.connection.password"),
        DB_CONNECTION_DRIVER("db.connection.driver"),
        DB_CONNECTION_CHARSET("db.connection.charset"),
        DB_CONNECTION_COUNT("db.connection.count"),
        
        DB_TABLE_NAME("db.table.name"),
        DB_TABLE_PK("db.table.pk"),
        DB_TABLE_C_NAME("db.table.c.name"),
        DB_TABLE_C_DATA("db.table.c.data"),
        DB_TABLE_C_TYPE("db.table.c.type"),
        
        DISCOVER_HOSTS_TIME("discover.host.time");
        
        private final String key;

        Property(String key) {
            this.key = key;
        }

        public String getKey() {
            return key;
        }
    }

    public static String read(Property property) {
        return props.getProperty(property.getKey());
    }

    private static Properties loadProperties() {
        Properties properties = new Properties();
        InputStream in = PropertiesReader.class.getClassLoader().getResourceAsStream(PROPS_FILE);

        try {
            properties.load(in);
        } catch (Exception e) {
            logger.error("Failed to load properties from " + PROPS_FILE, e);
            System.exit(0);
        }

        return properties;
    }
}
