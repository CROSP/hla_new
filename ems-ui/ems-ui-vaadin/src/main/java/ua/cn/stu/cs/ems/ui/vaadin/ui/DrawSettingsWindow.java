package ua.cn.stu.cs.ems.ui.vaadin.ui;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Form;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import ua.cn.stu.cs.ems.ui.vaadin.dto.GlobalSettings;

/**
 * @author n0weak
 */
public class DrawSettingsWindow extends AbstractSettingsWindow {

    private static final String WIDTH = "400px";
    private static final String CAPTION = "Draw Preferences";
    private final GlobalSettings settings;
    private Form canvasForm;
    private Form uiForm;

    public DrawSettingsWindow(GlobalSettings settings) {
        super(CAPTION, WIDTH);
        this.settings = settings;
        paint();
    }

    protected void paint() {
        VerticalLayout layout = (VerticalLayout) getContent();

//        layout.addComponent(new Label(AnimationSettingsWindow.CAPTION));

//        animationForm = new AnimationSettingsForm(settings.getAnimationSettings());
//        layout.addComponent(animationForm);
//        layout.setComponentAlignment(animationForm, Alignment.MIDDLE_CENTER);

        layout.addComponent(new Label("Canvas Settings"));

        canvasForm = new CanvasSettingsForm(settings.getCanvasSettings());
        layout.addComponent(canvasForm);
        layout.setComponentAlignment(canvasForm, Alignment.MIDDLE_CENTER);

        layout.addComponent(new Label("UI Settings"));

        uiForm = new UISettingsForm(settings.getUISettings());
        layout.addComponent(uiForm);
        layout.setComponentAlignment(uiForm, Alignment.MIDDLE_CENTER);

        paintFooter(layout);
    }

    @Override
    protected boolean apply() {
        if (canvasForm.isValid() && uiForm.isValid()) {
            canvasForm.commit();
            uiForm.commit();
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    protected void discard() {
        canvasForm.discard();
        uiForm.discard();
    }
}
