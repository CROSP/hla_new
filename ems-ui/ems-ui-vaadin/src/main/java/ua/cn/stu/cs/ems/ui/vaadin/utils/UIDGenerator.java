package ua.cn.stu.cs.ems.ui.vaadin.utils;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.EntityType;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Map;
import java.util.Set;

/**
 * @author n0weak
 */
// TODO uids with prefixes
// TODO update uids when loading model, for preventing model crashes when adding component
public class UIDGenerator {

    final static Logger logger = LoggerFactory.getLogger(UIDGenerator.class);
    private final Map<EntityType, Integer> indices = new HashMap<EntityType, Integer>();
    private final Map<EntityType, String> ids = new HashMap<EntityType, String>();
    private final Map<EntityType, Set<Integer>> takenIndicies = new HashMap<EntityType, Set<Integer>>();
    private final Map<String, Set<Integer>> customIndexes = new Hashtable<String, Set<Integer>>();

    {
        ids.put(EntityType.PLACE, "P");
        ids.put(EntityType.TTRANSITION, "T");
        ids.put(EntityType.XTRANSITION, "X");
        ids.put(EntityType.YTRANSITION, "Y");
        ids.put(EntityType.FTRANSITION, "F");
        ids.put(EntityType.JTRANSITION, "J");
        ids.put(EntityType.FIFO, "QF");
        ids.put(EntityType.PFIFO, "QFP");
        ids.put(EntityType.LIFO, "QL");
        ids.put(EntityType.PLIFO, "QLP");
        ids.put(EntityType.INPUT, "I");
        ids.put(EntityType.OUTPUT, "O");
        ids.put(EntityType.AGGREGATE, "AGR");
    }
    
    public String currentId(EntityType entityType) {
        return getId(entityType) + getIndex(entityType);
    }
    
    public void registerCustomId(String id) {
        Set<Integer> ids = customIndexes.get(id);
        if (null == ids) {
            ids = new HashSet<Integer>();
            customIndexes.put(id, ids);
        }
    }
    
    public void addIndexToCustomID(String id, Integer index) {
        Set<Integer> ids = customIndexes.get(id);
        if (null != ids) {
            ids.add(index);
        }
    }
    

    
    /**
     * Generating custom id in format "Id_1", "Idddd_2". Dont RENAME MODELS !!!
     * @param id
     * @return 
     */
    public String nextCustomId(String id) {
        Set<Integer> ids = customIndexes.get(id);
        if (null == ids) {
            registerCustomId(id);
            ids = customIndexes.get(id);
        }
        Integer i = new Integer(0);
        while (ids.contains(i)) {
            i++;
        }

        ids.add(i);
        
        return id + "_" + i;
    }

    public String nextId(EntityType entityType) {
        int i = getIndex(entityType);
        i = getValidIdx(entityType, ++i);

        indices.put(entityType, i);
        return getId(entityType) + i;
    }

    public void setCurrentIdx(EntityType entityType, int idx) {
        indices.put(entityType, idx);
        Set<Integer> takenIndices = this.takenIndicies.get(entityType);
        if (null != takenIndices) {
            takenIndicies.clear();
        }
    }

    public void take(String uid, EntityType type) {
        String id = ids.get(type);
        if (uid.startsWith(id) && uid.length() > id.length()) {
            try {
                Integer idx = Integer.parseInt(uid.substring(id.length(), uid.length()));
                Integer lastIdx = getIndex(type);
                if (idx >= lastIdx && idx <= lastIdx + 1) {
                    indices.put(type, idx + 1);
                }
                else if (idx > lastIdx) {
                    Set<Integer> takenIdxSet = takenIndicies.get(type);
                    if (null == takenIdxSet) {
                        takenIdxSet = new HashSet<Integer>();
                        takenIndicies.put(type, takenIdxSet);
                    }
                    takenIdxSet.add(idx);
                }
            } catch (Exception e) {
                logger.error("Failed to register taken uid: " + uid, e);
                
            }
        }

    }

    public void clear() {
        indices.clear();
        customIndexes.clear();
        takenIndicies.clear();
    }

    private int getValidIdx(EntityType entityType, int i) {
        Set<Integer> takenIdxSet = takenIndicies.get(entityType);
        if (null != takenIdxSet) {
            while (takenIdxSet.contains(i)) {
                i++;
            }
        }
        return i;
    }

    private Integer getIndex(EntityType entityType) {
        Integer idx = indices.get(entityType);
        if (null == idx) {
            idx = 0;
        }
        return getValidIdx(entityType, idx);
    }

    private String getId(EntityType entityType) {
        String id = ids.get(entityType);
        if (null == id) {
            throw new IllegalArgumentException("No id for entity " + entityType + "found");
        }
        return id;
    }
}
