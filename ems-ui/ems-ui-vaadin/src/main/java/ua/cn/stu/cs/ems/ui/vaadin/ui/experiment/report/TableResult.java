package ua.cn.stu.cs.ems.ui.vaadin.ui.experiment.report;

import com.vaadin.ui.Table;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import ua.cn.stu.cs.ems.core.experiment.PrimaryStatisticsElement;
import ua.cn.stu.cs.ems.core.experiment.PrimaryStatisticsResult;
import ua.cn.stu.cs.ems.core.experiment.SecondaryStatisticsElement;
import ua.cn.stu.cs.ems.core.experiment.statistics.StatisticsParameters;
import ua.cn.stu.cs.ems.core.experiment.statistics.StatisticsResult;

/**
 * Table result for primary and secondary statistics
 *
 * @author Dmitry Krivenko <dmitrykrivenko@gmal.com>
 */
public class TableResult {

    private Table places;
    private Table transitions;
    private Table responds;
    private String parentAggregateName;
    private String factorName;
    private String respondName;

    /**
     * Constructor for table results for primary statistics
     *
     * @param agrName
     */
    public TableResult(String agrName) {
        parentAggregateName = agrName;
    }

    /**
     * Constructor for table results for secondary statistics
     *
     * @param nameOfFactor
     * @param nameOfRespond
     */
    public TableResult(String nameOfFactor, String nameOfRespond) {
        factorName = nameOfFactor;
        respondName = nameOfRespond;
    }

    /**
     * Add element in the table for primary statistics
     *
     * @param prStResult
     */
    public final void addElementInTable(List<PrimaryStatisticsResult> prStResult) {

        ArrayList<PrimaryStatisticsElement> elements = new ArrayList<PrimaryStatisticsElement>();

        for (int j = 0; j < prStResult.size(); j++) {
            int size = prStResult.get(j).getDumpedPrimaryStatisticsElements().size();
            //If statistics accumulates result of last element is result for run
            //NEED TO BE FIXED for statistic on event(when size can be 0)
            PrimaryStatisticsElement el = prStResult.get(j).getDumpedPrimaryStatisticsElements().get(size - 1);
            elements.add(el);
        }

        double coefficient = 0;
        double numOfTokens = 0;
        double averageTime = 0;
        //If statistics result for place
        if (prStResult.get(0).getObjectType().equals(PrimaryStatisticsResult.OBJECT_TYPE_PLACE)) {
            for (int i = 0; i < elements.size(); i++) {
                StatisticsResult stResult = elements.get(i).getStatisticsResult();
                coefficient += stResult.getResultValue(StatisticsParameters.OCCUPIED_COEFFICIENT);
                numOfTokens += stResult.getResultValue(StatisticsParameters.NUMBER_OF_PASSED_TOKENS);
                averageTime += stResult.getResultValue(StatisticsParameters.AVERAGE_OCCUPIED_TIME);
            }
            addElementToPrimaryTable(getTablePlaces(), coefficient, numOfTokens, averageTime,
                    prStResult.size(), prStResult.get(0).getObjectName());
        } else //If statistics result fo transition
        if (prStResult.get(0).getObjectType().equals(PrimaryStatisticsResult.OBJECT_TYPE_TRANSITION)) {
            for (int i = 0; i < elements.size(); i++) {
                StatisticsResult stResult = elements.get(i).getStatisticsResult();
                coefficient += stResult.getResultValue(StatisticsParameters.OCCUPIED_COEFFICIENT);
                numOfTokens += stResult.getResultValue(StatisticsParameters.NUMBER_OF_PASSED_TOKENS);
                averageTime += stResult.getResultValue(StatisticsParameters.AVERAGE_TIME_IN_DELAY);
            }
            addElementToPrimaryTable(getTableTransitions(), coefficient, numOfTokens, averageTime,
                    prStResult.size(), prStResult.get(0).getObjectName());
        } else //If statistics result fo queue
        if (prStResult.get(0).getObjectType().equals(PrimaryStatisticsResult.OBJECT_TYPE_QUEUE)) {
            for (int i = 0; i < elements.size(); i++) {
                StatisticsResult stResult = elements.get(i).getStatisticsResult();
                coefficient += stResult.getResultValue(StatisticsParameters.AVERAGE_QUEUE_LENGTH);
                numOfTokens += stResult.getResultValue(StatisticsParameters.NUMBER_OF_PASSED_TOKENS);
                averageTime += stResult.getResultValue(StatisticsParameters.AVERAGE_TIME_IN_QUEUE);
            }
            addElementToPrimaryTable(getTableTransitions(), coefficient, numOfTokens, averageTime,
                    prStResult.size(), prStResult.get(0).getObjectName());
        }
    }

    /**
     * Add element in the table for secondary statistics
     *
     * @param el
     */
    public final void addElementInTable(SecondaryStatisticsElement el) {
        NumberFormat formatter = new DecimalFormat("0.000E00");

        String factor = formatter.format(el.getFactorValue());

        String respond;
        if (!Double.isNaN(el.getRespond()) && !Double.isInfinite(el.getRespond())) {
            respond = formatter.format(el.getRespond());
        } else {
            respond = Double.toString(el.getRespond());
        }

        String dispersion;
        if (!Double.isNaN(el.getVariance()) && !Double.isInfinite(el.getVariance())) {
            dispersion = formatter.format(el.getVariance());
        } else {
            dispersion = Double.toString(el.getVariance());
        }

        String coefOfVar;
        if (!Double.isNaN(el.getCoefficientOfVariation()) && !Double.isInfinite(el.getCoefficientOfVariation())) {
            coefOfVar = formatter.format(el.getCoefficientOfVariation());
        } else {
            coefOfVar = Double.toString(el.getCoefficientOfVariation());
        }

        String minRespond;
        if (!Double.isNaN(el.getMinRespond()) && !Double.isInfinite(el.getMinRespond())) {
            minRespond = formatter.format(el.getMinRespond());
        } else {
            minRespond = Double.toString(el.getMinRespond());
        }

        String maxRespond;
        if (!Double.isNaN(el.getMaxRespond()) && !Double.isInfinite(el.getMaxRespond())) {
            maxRespond = formatter.format(el.getMaxRespond());
        } else {
            maxRespond = Double.toString(el.getMaxRespond());
        }

        String sampleSize;
        if (!Double.isNaN(el.getSampleSize()) && !Double.isInfinite(el.getSampleSize())) {
            sampleSize = formatter.format(el.getSampleSize());
        } else {
            sampleSize = Double.toString(el.getSampleSize());
        }

        getTableResponds().addItem(new Object[]{factor, respond, dispersion,
                    coefOfVar, minRespond, maxRespond, sampleSize}, null);
    }

    private Table getTablePlaces() {
        if (getPlaces() == null) {
            places = new Table("Places of aggregate " + parentAggregateName);
            getPlaces().addContainerProperty("Name", String.class, null);
            getPlaces().addContainerProperty(StatisticsParameters.OCCUPIED_COEFFICIENT.getPresentation(), String.class, null);
            getPlaces().addContainerProperty(StatisticsParameters.NUMBER_OF_PASSED_TOKENS.getPresentation(), String.class, null);
            getPlaces().addContainerProperty(StatisticsParameters.AVERAGE_OCCUPIED_TIME.getPresentation(), String.class, null);
            getPlaces().setSelectable(true);
            getPlaces().setImmediate(true);
            getPlaces().setStyleName("tableReport");
            return getPlaces();
        } else {
            return getPlaces();
        }
    }

    private Table getTableTransitions() {
        if (getTransitions() == null) {
            transitions = new Table("Transitions of aggregate " + parentAggregateName);
            getTransitions().addContainerProperty("Name", String.class, null);
            getTransitions().addContainerProperty(StatisticsParameters.OCCUPIED_COEFFICIENT.getPresentation(), String.class, null);
            getTransitions().addContainerProperty(StatisticsParameters.NUMBER_OF_PASSED_TOKENS.getPresentation(), String.class, null);
            getTransitions().addContainerProperty(StatisticsParameters.AVERAGE_OCCUPIED_TIME.getPresentation(), String.class, null);
            getTransitions().setSelectable(true);
            getTransitions().setImmediate(true);
            getTransitions().setStyleName("tableReport");
            return getTransitions();
        } else {
            return getTransitions();
        }
    }

    private Table getTableResponds() {
        if (getResponds() == null) {
            responds = new Table("Interval data on the response of the " + respondName);
            getResponds().addContainerProperty("Value of " + factorName, String.class, null);
            getResponds().addContainerProperty("Avg value of responce", String.class, null);
            getResponds().addContainerProperty("Dispersion of responce", String.class, null);
            getResponds().addContainerProperty("Coefficient of variation", String.class, null);
            getResponds().addContainerProperty("Min of responce", String.class, null);
            getResponds().addContainerProperty("Max of responce", String.class, null);
            getResponds().addContainerProperty("Sample size", String.class, null);
            getResponds().setSelectable(true);
            getResponds().setImmediate(true);
            getResponds().setStyleName("tableReport");
            return getResponds();
        } else {
            return getResponds();
        }
    }

    private void addElementToPrimaryTable(Table table, double coefficient, double numOfTokens,
            double averageTime, int sampleSize, String objName) {

        coefficient = coefficient / sampleSize;
        numOfTokens = numOfTokens / sampleSize;
        averageTime = averageTime / sampleSize;

        NumberFormat formatter = new DecimalFormat("0.000E00");

        String coefficientStr;
        if (!Double.isNaN(coefficient) && !Double.isInfinite(coefficient)) {
            coefficientStr = formatter.format(coefficient);
        } else {
            coefficientStr = Double.toString(coefficient);
        }

        String numOfTokenstStr;
        if (sampleSize == 1) {
            numOfTokenstStr = String.valueOf((int) numOfTokens);
        } else {
            numOfTokenstStr = formatter.format(numOfTokens);
        }

        String averageTimeStr;
        if (!Double.isNaN(averageTime) && !Double.isInfinite(averageTime)) {
            averageTimeStr = formatter.format(averageTime);
        } else {
            averageTimeStr = Double.toString(averageTime);
        }

        table.addItem(new Object[]{objName, coefficientStr, numOfTokenstStr, averageTimeStr}, null);
    }

    /**
     * @return the places
     */
    public final Table getPlaces() {
        return places;
    }

    /**
     * @return the transitions
     */
    public final Table getTransitions() {
        return transitions;
    }

    /**
     * @return the responds
     */
    public final Table getResponds() {
        return responds;
    }
}