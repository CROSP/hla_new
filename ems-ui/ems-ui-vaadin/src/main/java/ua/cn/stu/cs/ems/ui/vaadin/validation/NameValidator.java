package ua.cn.stu.cs.ems.ui.vaadin.validation;

import com.vaadin.data.Validator;
import ua.cn.stu.cs.ems.core.aggregates.AggregateChild;

/**
 * @author n0weak
 */
public class NameValidator implements Validator {

    private static final String MSG = "Name is not unique!";
    private final NameManager nameManager;
    private final AggregateChild entity;

    public NameValidator(AggregateChild entity, NameManager nameManager) {
        this.entity = entity;
        this.nameManager = nameManager;
    }

    public void validate(Object o) throws InvalidValueException {
        if (!isValid(o)) {
            throw new InvalidValueException(MSG);
        }
    }

    public boolean isValid(Object o) {
        return o != null && nameManager.isNameFree(o.toString(), entity);
    }

    public interface NameManager {

        boolean isNameFree(String desiredName, AggregateChild currentEntity);
    }
}
