package ua.cn.stu.cs.ems.ui.vaadin.ui.experiment.report;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Dmitry Krivenko <dmitrykrivenko@gmal.com>
 */
public class HistoResult {

    private List<Double[]> xValues = new ArrayList<Double[]>();
    private List<Double[]> yValues = new ArrayList<Double[]>();
    private List<String> xAxesNames = new ArrayList<String>();
    private List<String> yAxesNames = new ArrayList<String>();
    private List<String> captions = new ArrayList<String>();

    public void addXYValues(Double[] theXValues, String theXAxeName, Double[] theYValues, String theYAxeName, String theHistoCaption) {
        xValues.add(theXValues);
        xAxesNames.add(theXAxeName);

        assert theYValues.length == xValues.get(xValues.size() - 1).length-1;

        yValues.add(theYValues);
        yAxesNames.add(theYAxeName);
        captions.add(theHistoCaption);

        assert theYValues.length == theXValues.length-1;
    }

    /**
     * @return the xValues
     */
    public List<Double[]> getXValues() {
        return xValues;
    }

    /**
     * @return the yValues
     */
    public List<Double[]> getYValues() {
        return yValues;
    }

    /**
     * @return the xAxesNames
     */
    public List<String> getXAxesNames() {
        return xAxesNames;
    }

    /**
     * @return the yAxesNames
     */
    public List<String> getYAxesNames() {
        return yAxesNames;
    }

    /**
     * @return the caption
     */
    public List<String> getCaptions() {
        return captions;
    }
}
