/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.cn.stu.cs.ems.ui.vaadin.validation;

import com.vaadin.data.Validator;
import com.vaadin.ui.DateField.UnparsableDateString;

/**
 * Allow to validate positive long value (allow values &gt=0).
 * @author AlexanderBartash@mail.ru
 * @version 1.0
 */
public class PositiveLongValidator implements Validator {

    public void validate(Object value) throws InvalidValueException {
        if (null == value) {
            throw new EmptyValueException("Value can't be empty!");
        }
        long lvalue = 0;
        try {
            lvalue = Long.parseLong(value.toString());
        } catch (NumberFormatException ex) {
            throw new UnparsableDateString("Invalid number format!");
        }
        if (0 > lvalue) {
            throw new InvalidValueException("Value can't be less then zero!");
        }
    }

    public boolean isValid(Object value) {
        if (null == value) {
            return false;
        }
        long lvalue = 0;
        try {
            lvalue = Long.parseLong(value.toString());
        } catch (NumberFormatException ex) {
            return false;
        }
        return !(0 > lvalue);
    }
}
