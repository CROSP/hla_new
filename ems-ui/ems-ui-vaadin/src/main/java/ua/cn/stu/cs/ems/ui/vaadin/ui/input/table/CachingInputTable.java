package ua.cn.stu.cs.ems.ui.vaadin.ui.input.table;

import com.vaadin.data.Item;
import ua.cn.stu.cs.ems.ui.vaadin.validation.ValidatorFactory;

/**
 * Table to be used with {@link CachingFieldCreator}
 *
 * @author n0weak
 */
public abstract class CachingInputTable<T extends Editable<T>> extends InputTable<T> {

    private String[] requiredFields;

    public CachingInputTable(Class<T> clazz, String caption) {
        super(clazz, caption);
    }

    @Override
    public void discard() throws SourceException {
        getFieldCreator().clearCache();
        super.discard();
    }

    @Override
    @SuppressWarnings("unchecked")
    public boolean removeItem(Object itemId) {
        getFieldCreator().removeItem((T) itemId);
        return super.removeItem(itemId);
    }

    @Override
    public Item addItem(Object itemId) throws UnsupportedOperationException {
        if (null != requiredFields) {
            for (String field : requiredFields) {
                ValidatorFactory.addRequiredValidator(getFieldCreator().getCache(field).get(getLast()));
            }
        }
        return super.addItem(itemId);
    }

    public void setRequiredFields(String... requiredFields) {
        this.requiredFields = requiredFields;
    }

    public abstract CachingFieldCreator<T> getFieldCreator();
}
