package ua.cn.stu.cs.ems.ui.vaadin.ui.experiment;

import com.vaadin.data.Property;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import ua.cn.stu.cs.ems.ui.vaadin.validation.ValidatorFactory;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Component which is used in experiment report to choose values of secondary factors.
 *
 * @author n0weak
 */
class SecondaryFactorsControl extends Panel {

    private final Map<String, ComboBox> fields;
    private ComboBox hiddenField;

    public SecondaryFactorsControl(Collection<ExperimentSettings.FactorEntry> factors,
                                   Property.ValueChangeListener listener) {
        addComponent(new Label("Secondary factors: "));

        fields = new HashMap<String, ComboBox>(factors.size());

        for (ExperimentSettings.FactorEntry factor : factors) {
            ComboBox cb = new ComboBox(factor.getVariable());
            cb.addValidator(ValidatorFactory.getDoubleValidator());
            cb.addListener(listener);
            cb.setImmediate(true);

            double begin = Double.valueOf(factor.getBegin());
            double end = Double.valueOf(factor.getEnd());
            double step = Double.valueOf(factor.getStep());
            for (double d = begin; d < end; d += step) {
                cb.addItem(d);
            }
            cb.setValue(begin);
            addComponent(cb);
            fields.put(factor.getVariable(), cb);
        }

    }

    public void hide(String factorName) {
        ComboBox cb = fields.get(factorName);
        if (null != cb) {
            cb.setVisible(false);
            if (null != hiddenField) {
                hiddenField.setVisible(true);
            }
            hiddenField = cb;
        }
    }

    public Map<String, Double> getValue() {
        Map<String, Double> result = new HashMap<String, Double>(fields.size() - 1);
        for (Map.Entry<String, ComboBox> entry : fields.entrySet()) {
            if (hiddenField != entry.getValue()) {
                result.put(entry.getKey(), Double.valueOf(entry.getValue().toString()));
            }
        }
        return result;
    }
}
