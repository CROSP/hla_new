package ua.cn.stu.cs.ems.ui.vaadin.ui.experiment;

import java.util.HashSet;
import java.util.Set;
import ua.cn.stu.cs.ems.core.experiment.statistics.StatisticsParameters;
import ua.cn.stu.cs.ems.ui.vaadin.ui.input.table.Valuable;

/**
 * Class-container for the settings of the experiment.
 * @author AlexanderBartash@mail.ru
 * @version 1.0
 */
public class ExperimentSettings {

    private String modelName = "";
    private double modelingTime = 100;
    private int numberOfRuns = 1;
    private PrimaryStatisticsSettings primaryStatisticSettings = new PrimaryStatisticsSettings();
    private SecondaryStatisticsSettins secondaryStatisticsSettins = new SecondaryStatisticsSettins();
    private AutostopSettings autostopSettings = new AutostopSettings();

    public ExperimentSettings() {
    }

    public AutostopSettings getAutostopSettings() {
        return autostopSettings;
    }

    public void setAutostopSettings(AutostopSettings autostopSettings) {
        this.autostopSettings = autostopSettings;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public double getModelingTime() {
        return modelingTime;
    }

    public void setModelingTime(double modelingTime) {
        this.modelingTime = modelingTime;
    }

    public int getNumberOfRuns() {
        return numberOfRuns;
    }

    public void setNumberOfRuns(int numberOfRuns) {
        this.numberOfRuns = numberOfRuns;
    }

    public PrimaryStatisticsSettings getPrimaryStatisticSettings() {
        return primaryStatisticSettings;
    }

    public void setPrimaryStatisticSettings(PrimaryStatisticsSettings primaryStatisticSettings) {
        this.primaryStatisticSettings = primaryStatisticSettings;
    }

    public SecondaryStatisticsSettins getSecondaryStatisticsSettins() {
        return secondaryStatisticsSettins;
    }

    public void setSecondaryStatisticsSettins(SecondaryStatisticsSettins secondaryStatisticsSettins) {
        this.secondaryStatisticsSettins = secondaryStatisticsSettins;
    }

    public class PrimaryStatisticsSettings {

        private boolean enabled = true;
        private double statisticsStart = 0;
        private boolean collectOnEvent = false;
        private boolean collectWithPeriod = true;
        private double statisticsPeriod = 1;
        private Set<String> subjectEntities = new HashSet<String>();

        PrimaryStatisticsSettings() {
        }

        public boolean isEnabled() {
            return enabled;
        }

        public void setEnabled(boolean enabled) {
            this.enabled = enabled;
        }

        public double getStatisticsPeriod() {
            return statisticsPeriod;
        }

        public void setStatisticsPeriod(double statisticsPeriod) {
            this.statisticsPeriod = statisticsPeriod;
        }

        public double getStatisticsStart() {
            return statisticsStart;
        }

        public void setStatisticsStart(double statisticsStart) {
            this.statisticsStart = statisticsStart;
        }

        public Set<String> getSubjectEntities() {
            return subjectEntities;
        }

        public void setSubjectEntities(Set<String> subjectEntities) {
            this.subjectEntities = subjectEntities;
        }
        
        public boolean isCollectOnEvent() {
            return collectOnEvent;
        }
        
        public void setCollectOnEvent(boolean collectOnEvent) {
            this.collectOnEvent = collectOnEvent;
        }
        
        public boolean isCollectWithPeriod() {
            return collectWithPeriod;
        }
        
        public void setCollectWithPeriod(boolean collectWithPeriod) {
            this.collectWithPeriod = collectWithPeriod;
        }
    }

    public static class FactorEntry implements Valuable<FactorEntry> {

        private String variable = "";
        private String begin = "";
        private String end = "";
        private String step = "";

        public FactorEntry() {
        }

        public FactorEntry(String begin, String end, String step, String variable) {
            this.begin = begin;
            this.end = end;
            this.step = step;
            this.variable = variable;
        }

        public String getBegin() {
            return begin;
        }

        public void setBegin(String begin) {
            this.begin = begin;
        }

        public String getEnd() {
            return end;
        }

        public void setEnd(String end) {
            this.end = end;
        }

        public String getStep() {
            return step;
        }

        public void setStep(String step) {
            this.step = step;
        }

        public String getVariable() {
            return variable;
        }

        public void setVariable(String variable) {
            this.variable = variable;
        }

        public String getValue() {
            return variable;
        }

        public FactorEntry copy() {
            return new FactorEntry(begin, end, step, variable);
        }
    }

    public class SecondaryStatisticsSettins {

        private boolean enabled = false;
        private String response = ""; /*Must contain name of variable from AggregateVariables or name of object.*/
        private StatisticsParameters responseType;
        private Set<FactorEntry> factors = new HashSet<FactorEntry>();

        SecondaryStatisticsSettins() {
        }

        public boolean isEnabled() {
            return enabled;
        }

        public void setEnabled(boolean enabled) {
            this.enabled = enabled;
        }

        public String getResponse() {
            return response;
        }

        public void setResponse(String response) {
            this.response = response;
        }

        public Set<FactorEntry> getFactors() {
            return factors;
        }

        public void setFactors(Set<FactorEntry> factors) {
            this.factors = factors;
        }
        
        public void setResponseType(StatisticsParameters representation) {
            this.responseType = representation;
        }
        
        public StatisticsParameters getResponseType() {
            return responseType;
        }
    }

    public class AutostopSettings {

        public final double [] ALLOWED_PROBABILITIES = {0.9d, 0.95d, 0.99d};
        private boolean enabled = false;
        private String response = ""; /*Must contain name of variable from AggregateVariables or name of object.*/
        private StatisticsParameters parameter;
        private double  confidenceInterval = 0.0d;
        private double  confidenceProbability = ALLOWED_PROBABILITIES[0];

        AutostopSettings() {
        }

        public double  getConfidenceInterval() {
            return confidenceInterval;
        }

        public void setConfidenceInterval(double  confidenceInterval) {
            this.confidenceInterval = confidenceInterval;
        }

        public double  getConfidenceProbability() {
            return confidenceProbability;
        }

        public void setConfidenceProbability(double  confidenceProbability) throws Exception {
            for (double probability : ALLOWED_PROBABILITIES) {
                if (probability == confidenceProbability) {
                    this.confidenceProbability = confidenceProbability;
                    return;
                }
            }
            throw new Exception("Illegal probability.");
        }

        public boolean isEnabled() {
            return enabled;
        }

        public void setEnabled(boolean enabled) {
            this.enabled = enabled;
        }

        public StatisticsParameters getParameter() {
            return parameter;
        }

        public void setParameter(StatisticsParameters parameter) {
            this.parameter = parameter;
        }
        
        public String getResponse() {
            return response;
        }

        public void setResponse(String response) {
            this.response = response;
        }
    }
}
