package ua.cn.stu.cs.ems.ui.vaadin.dto;

import ua.cn.stu.cs.ems.core.aggregates.Aggregate;
import ua.cn.stu.cs.ems.ecli.Value;

import java.util.ArrayList;
import java.util.List;

/**
 * @author n0weak
 */
public class AggregateVariables {

    private final List<Variable> variables;

    public static AggregateVariables readFrom(Aggregate aggregate) {
        List<Variable> variables = new ArrayList<Variable>();
        for (String name : aggregate.getVariablesNames()) {
            Object value = aggregate.getVariable(name).getValue();
            variables.add(new Variable(name, value != null ? value.toString() : ""));
        }
        return new AggregateVariables(variables);
    }

    public AggregateVariables(List<Variable> variables) {
        this.variables = variables;
    }

    public List<Variable> getVariables() {
        return variables;
    }

    public void writeTo(Aggregate aggregate) {
        aggregate.clearVariables();
        for (Variable variable : variables) {
            aggregate.setVariable(variable.getName(),
                    variable.getValue() != null && !variable.getValue().equals("") ? new Value(variable.getValue())
                    : new Value());
        }
    }
}
