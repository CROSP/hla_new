package ua.cn.stu.cs.ems.ui.vaadin.validation;

/**
 * @author n0weak
 */
public class PositiveDoubleValidator extends AbstractValidator {

    public PositiveDoubleValidator(String errMessage) {
        super(errMessage);
    }

    public boolean isValid(Object o) {
        try {
            return Double.parseDouble(String.valueOf(o)) > 0;
        } catch (Exception e) {
            return false;
        }
    }
}
