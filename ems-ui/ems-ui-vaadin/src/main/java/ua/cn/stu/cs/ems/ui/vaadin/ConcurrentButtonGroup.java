package ua.cn.stu.cs.ems.ui.vaadin;

import com.vaadin.ui.Button;
import ua.cn.stu.cs.ems.ui.vaadin.utils.ButtonSelector;

/**
 * @author n0weak
 */
public class ConcurrentButtonGroup implements Button.ClickListener {

    private Button pressedButton;

    public void addButton(Button b) {
        b.addListener(this);
    }

    public void buttonClick(Button.ClickEvent clickEvent) {
        buttonPressed(clickEvent.getButton());
    }

    public void press(Button b) {
        buttonPressed(b);
    }

    private void buttonPressed(Button b) {

        if (b != pressedButton) {
            if (null != pressedButton) {
                ButtonSelector.deselect(pressedButton);
            }
            ButtonSelector.select(b);
            pressedButton = b;
        }

    }
}
