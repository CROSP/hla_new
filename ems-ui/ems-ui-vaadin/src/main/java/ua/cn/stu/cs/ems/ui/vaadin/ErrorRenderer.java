package ua.cn.stu.cs.ems.ui.vaadin;

/**
 * @author n0weak
 */
public interface ErrorRenderer {

    static final String DEFAULT_CAPTION = "Internal error</br>";
    static final String DEFAULT_MESSAGE = "Please notify the site administrator.";

    /**
     * Renders error with specified caption and message.
     *
     * @param caption error's caption to be rendered
     * @param message error's message to be rendered
     */
    void renderError(String caption, String message);

    /**
     * Renders error with {@link #DEFAULT_MESSAGE} and {@link #DEFAULT_CAPTION}.
     */
    void renderError();
}
