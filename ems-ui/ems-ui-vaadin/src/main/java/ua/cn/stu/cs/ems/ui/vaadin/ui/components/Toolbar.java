package ua.cn.stu.cs.ems.ui.vaadin.ui.components;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import ua.cn.stu.cs.ems.ui.vaadin.utils.UIUtils;

import com.vaadin.addon.chameleon.ChameleonTheme;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Window;

public class Toolbar extends Panel {

    private static String ICONS_PATH = "icons/toolbar/";
    public static int DEFAULT_BUTTON_WIDTH = 30;
    public static int DEFAULT_BUTTON_HEIGHT = 24;
    private static int DEFAULT_SPACING_TB = 16; // top - bottom
    private static int DEFAULT_SPACING_LR = 5; // left - right
    private Map<String, Panel> toolPanels = new HashMap<String, Panel>();

    public Toolbar() {
        setStyleName("toolbar_panel");
        setWidth("100%");
        setImmediate(true);
        Layout l = new CssLayout();
        l.setStyleName("toolbar");
        l.setMargin(false);
        setContent(l);
        setScrollable(false);
    }

    public Button createButton(String description, String icon) {
        Button b = new Button();
        b.setWidth(DEFAULT_BUTTON_WIDTH + "px");
        b.setHeight(DEFAULT_BUTTON_HEIGHT + "px");

        b.addStyleName(ChameleonTheme.BUTTON_ICON_ONLY);
        b.addStyleName(ChameleonTheme.BUTTON_BORDERLESS);
        b.setDescription(description);

        if (icon != null && !icon.isEmpty()) {
            b.setIcon(UIUtils.getThemeResource(ICONS_PATH + icon));
        }

        return b;
    }

    public void createToolLayout(String layoutId) {
        Panel hl = toolPanels.get(layoutId);
        if (hl == null) {
            hl = new Panel();
            Layout l = new CssLayout();
            l.setSizeFull();
            l.setStyleName("tool_panel_layout");
            l.setMargin(false);
            hl.setStyleName("tool_panel");
            hl.setContent(l);
            hl.setWidth("5px");
            hl.setHeight((DEFAULT_BUTTON_HEIGHT + DEFAULT_SPACING_TB) + "px");
            toolPanels.put(layoutId, hl);
            addComponent(hl);
        }
    }

    public void addIntoLayout(String layoutId, Component c) {
        Panel hl = toolPanels.get(layoutId);
        if (hl != null) {
            hl.addComponent(c);
            if (c.isVisible()) {
                hl.setWidth((hl.getWidth() + c.getWidth() + DEFAULT_SPACING_LR) + "px");
            }
        }
    }

    public void resize() {
        Window w = getWindow();
        if (w == null) {
            return;
        }
        float width = w.getWidth();
        float subPanelsWidth = 0;
        Set<String> keys = toolPanels.keySet();
        for (String key : keys) {
            Panel p = toolPanels.get(key);
            subPanelsWidth += p.getWidth() + 5;
            // + 5 - becouse 5px right margin in css style 
        }
        int y = (int) (subPanelsWidth / width);
        int y1 = (int) (subPanelsWidth - y);
        if (y1 > 0) {
            y++;
        }

        float newHeight = y * (DEFAULT_BUTTON_HEIGHT + DEFAULT_SPACING_TB);
        getContent().setHeight(((int) newHeight - 4) + "px");
        setHeight((int) newHeight + "px");
    }
}
