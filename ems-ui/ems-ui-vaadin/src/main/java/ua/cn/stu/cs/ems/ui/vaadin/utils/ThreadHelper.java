package ua.cn.stu.cs.ems.ui.vaadin.utils;

import ua.cn.stu.cs.ems.ui.vaadin.ui.components.ActionHandler;

/**
 *
 * @author leonid
 */
public class ThreadHelper {
    
    private ThreadHelper(){
    }
    
    public static Thread create(final ActionHandler action) {
        try {
            Thread t = new Thread() {
                
                @Override
                public void run() {
                    action.handle();
                }
            };
            
            t.start();
            return t;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    
}
