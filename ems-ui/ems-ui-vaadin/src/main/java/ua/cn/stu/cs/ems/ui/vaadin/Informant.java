package ua.cn.stu.cs.ems.ui.vaadin;

import ua.cn.stu.cs.ems.core.experiment.statistics.StatisticsParameters;

import java.util.Collection;
import java.util.List;
import ua.cn.stu.cs.ems.core.aggregates.AggregateChild;

/**
 * @author n0weak
 */
public interface Informant {

    Collection<String> listAllEntities();

    Collection<String> listAllVariables();

    Collection<String> listAll(EntityClass entityClass);

    StatisticsParameters[] getApplicableParameters(String entityName);
    
    String getModelName();
    
    String getVariableValue(String var);
    
    AggregateChild findAggregateChild(String id);
    
    List<String> listAll(String aggPath, EntityClass type);
    
    List<String> listAllVariables(String aggPath);
}
