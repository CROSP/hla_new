package ua.cn.stu.cs.ems.ui.vaadin.ui.experiment;

import com.vaadin.ui.Panel;
import ua.cn.stu.cs.ems.core.aggregates.AggregateChild;
import ua.cn.stu.cs.ems.core.experiment.statistics.StatisticsParameters;
import ua.cn.stu.cs.ems.core.experiment.statistics.StatisticsResult;

import java.util.Map;

/**
 * @author n0weak
 */
class PrimaryExperimentReport extends ExperimentReport {
// ------------------------------ FIELDS ------------------------------

    private final double[] times;

// --------------------------- CONSTRUCTORS ---------------------------
    public PrimaryExperimentReport(Map<AggregateChild, StatisticsResult[]> results, Long[] times) {
        super(results);

        if (times.length == 0 || results.isEmpty()) {
            throw new IllegalArgumentException("Report data must be fully initialized");
        }

        this.times = new double[times.length];
        for (int i = 0; i < times.length; i++) {
            this.times[i] = times[i].doubleValue();
        }

        paintContent();
    }

    @Override
    protected void drawCharts(Panel mainContent, AggregateChild child) {
        for (StatisticsParameters param : StatisticsParameters.getApplicableParameters(child)) {
            mainContent.addComponent(createChartTypeSelect(param));
            mainContent.addComponent(drawChart(times, getStatistic(child, param), param.toString(), "time",
                                               getChartType(param)));
        }
    }

    private double[] getStatistic(AggregateChild entity, StatisticsParameters parameter) {
        StatisticsResult[] srs = results.get(entity);
        double[] result = new double[srs.length];
        for (int i = 0; i < srs.length; i++) {
            StatisticsResult sr = srs[i];
            double value = sr.getResultValue(parameter);
            result[i] = Double.isNaN(value) ? 0 : value;
        }
        return result;
    }

    @Override
    protected boolean validateAdditionalControls() {
        return true;
    }
}
