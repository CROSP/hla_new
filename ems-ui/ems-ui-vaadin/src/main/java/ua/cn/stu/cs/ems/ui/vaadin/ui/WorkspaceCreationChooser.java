package ua.cn.stu.cs.ems.ui.vaadin.ui;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.Validator;
import com.vaadin.data.Validator.InvalidValueException;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Form;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import java.util.ArrayList;
import java.util.Collection;
import ua.cn.stu.cs.ems.ui.vaadin.PropertiesListener;
import ua.cn.stu.cs.ems.ui.vaadin.ui.components.MainContent.WorkspaceCreatingEvent;

/**
 *
 * @author leonid
 */
public class WorkspaceCreationChooser extends AbstractSettingsWindow {

//    private static final String TEXT_FIELD_WIDTH = "100%";
    
    private PropertiesListener<WorkspaceCreatingEvent> listener;
    private Table templatesTable;
    private TextField name = new TextField("Choose new name");
    private Form form;
    
    private CheckBox cbEmpty;
        
    private final Collection<String> denyNames;
    private final Collection<String> templates;
    private String newGeneratedName;
    private boolean emptyAllowed;
    
    public WorkspaceCreationChooser(String caption, String width, String newGeneratedName,
            Collection<String> denyNames, Collection<String> templates, boolean emptyAllowed,
            final PropertiesListener<WorkspaceCreatingEvent> listener) {
        super(caption, width);
        
        this.listener = listener;
        this.templates = templates == null ? new ArrayList<String>() : templates;
        this.denyNames = denyNames == null ? new ArrayList<String>() : denyNames;
        this.emptyAllowed = emptyAllowed;
        this.newGeneratedName = newGeneratedName;
        paint();
    }
    
    protected void paint() {
        VerticalLayout layout = (VerticalLayout) getContent();
        form = new Form(new VerticalLayout());
        
        layout.addComponent(form);
        layout.setExpandRatio(form, 1.0f);
        name.setRequired(true);
        name.setValue(newGeneratedName);
        name.setImmediate(true);
        name.addValidator(new Validator() {

            public void validate(Object value) throws InvalidValueException {
                if (!isValid(value)) {
                    throw new InvalidValueException("Wrong name (already exist or is reserved)");
                }
            }

            public boolean isValid(Object value) {
                if (denyNames.contains(value)) {
                    return false;
                }
                return true;
            }
        });
        name.setWidth("100%");
        form.addField("name", name);
        
        cbEmpty = new CheckBox("Create empty");
        cbEmpty.setWidth("100%");
        form.addField("createEmpty", cbEmpty);
        cbEmpty.addListener(new ValueChangeListener() {

            public void valueChange(ValueChangeEvent event) {
               templatesTable.setEnabled(!cbEmpty.booleanValue());
            }
        });
        cbEmpty.setImmediate(true);
        cbEmpty.setEnabled(emptyAllowed);

        if (!emptyAllowed) {
            cbEmpty.setReadOnly(true);
        }
        
        templatesTable = new Table();
        form.addField("template", templatesTable);
        
        templatesTable.addValidator(new Validator() {

            public void validate(Object value) throws InvalidValueException {
                if (!isValid(value)) {
                    throw new InvalidValueException("Template cant be empty");
                }
            }

            public boolean isValid(Object value) {
                return value != null;
            }
        });
        templatesTable.setRequired(true);
        templatesTable.setImmediate(true);
        templatesTable.setCaption("Choose template from list:");
        
        templatesTable.addContainerProperty("Template", String.class, "");
        templatesTable.setVisibleColumns(new String[] {"Template"});
        templatesTable.setMultiSelect(false);
        templatesTable.setSelectable(true);
        templatesTable.setNewItemsAllowed(false);

        templatesTable.setWidth("100%");
        for (String name: templates) {
            templatesTable.addItem(new String[] {name}, null);
        }
        cbEmpty.setValue(emptyAllowed);
        
        paintFooter(layout);
    }

    @Override
    protected boolean apply() {
        boolean apply = false;
        if (name.isValid()) {
            if (cbEmpty.booleanValue()) {
                apply = true;
            }
            else if (templatesTable.isValid()) {
                apply = true;
            }
        }
        if (apply) {
            if (listener != null) {
                String wsName = (String)name.getValue();
                String templateName = null;
                if (templatesTable.isEnabled()) {
                    templateName = String.valueOf(templatesTable.getItem(templatesTable.getValue()));
                }
                listener.propertiesSet(new WorkspaceCreatingEvent(wsName, templateName));
            }
            return true;
        }
        return false;
    }
    
    @Override
    protected String getCommitButtonName() {
        return "Choose";
    }

    @Override
    protected String getDiscardButtonName() {
        return "Cancel";
    }
    
}
