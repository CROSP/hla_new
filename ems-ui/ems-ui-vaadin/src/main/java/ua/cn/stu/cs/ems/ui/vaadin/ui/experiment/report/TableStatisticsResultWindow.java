/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.cn.stu.cs.ems.ui.vaadin.ui.experiment.report;

import com.vaadin.ui.*;
import java.util.*;
import ua.cn.stu.cs.ems.core.experiment.statistics.AggregateVariableStateCollector;
import ua.cn.stu.cs.ems.ui.vaadin.ui.experiment.ExperimentSettings;

/**
 * Window with table results for primary or for secondary statistics
 * @author Dmitry Krivenko <dmitrykrivenko@gmal.com>
 */
public class TableStatisticsResultWindow extends Window {

    private static final String CAPTION = "Report";
    private Panel mainContent;

    /**
     * Constructor for window with results of primary statistics
     * @param tableResult 
     */
    public TableStatisticsResultWindow(HashMap<String, TableResult> tableResult, ArrayList<Table> variables, ExperimentSettings settings) {
        super(CAPTION);

        setResizable(false);

        HorizontalLayout content = new HorizontalLayout();
        setContent(content);
        mainContent = new Panel();
        mainContent.setHeight("575px");
        mainContent.setWidth("650px");
        mainContent.setScrollable(true);
        content.addComponent(mainContent);

        VerticalLayout layout = new VerticalLayout();
        ArrayList<String> keys = new ArrayList<String>(tableResult.keySet());
        Collections.sort(keys);
        for (int i = 0; i < keys.size(); i++) {
            if (tableResult.get(keys.get(i)).getPlaces() != null) {
                layout.addComponent(tableResult.get(keys.get(i)).getPlaces());
                layout.setComponentAlignment(tableResult.get(keys.get(i)).getPlaces(), Alignment.MIDDLE_CENTER);
            }
            if (tableResult.get(keys.get(i)).getTransitions() != null) {
                layout.addComponent(tableResult.get(keys.get(i)).getTransitions());
                layout.setComponentAlignment(tableResult.get(keys.get(i)).getTransitions(), Alignment.MIDDLE_CENTER);
            }
        }
//        for (Table t: variables) {
//            layout.addComponent(t);
//            layout.setComponentAlignment(t, Alignment.MIDDLE_CENTER);
//        }
        
        layout.setSpacing(true);

        layout.setMargin(true);

        mainContent.addComponent(layout);

        this.setPositionX(350);
        this.setPositionY(60);
    }

    /**
     * Constructor for window with results of secondary statistics
     * @param tableResult 
     */
    public TableStatisticsResultWindow(TableResult tableResult, ExperimentSettings settings) {
        super(CAPTION);

        setResizable(false);

        HorizontalLayout content = new HorizontalLayout();
        setContent(content);
        mainContent = new Panel();
        mainContent.setHeight("375px");
        mainContent.setWidth("1000px");
        mainContent.setScrollable(true);
        content.addComponent(mainContent);

        VerticalLayout layout = new VerticalLayout();
        /*
        Calendar cal = new GregorianCalendar();
        
        Label date = new Label("Date:             " + new SimpleDateFormat("dd.MM.yyyy").format(cal.getTime()));
        Label modelName = new Label("Model name:       " + settings.getModelName());
        Label modelTime = new Label("Modeling time:    " + settings.getModelingTime());
        Label numOfRuns = new Label("Number of runs:   " + settings.getNumberOfRuns());
        Label autostop = new Label();
        if (settings.getAutostopSettings().isEnabled()) {
        autostop.setCaption("Autostop is enabled");
        } else {
        autostop.setCaption("Autostop is disabled");
        }
        
        layout.addComponent(modelName);
        layout.addComponent(modelTime);
        layout.addComponent(numOfRuns);
        layout.addComponent(autostop);
        layout.addComponent(date);*/
        layout.addComponent(tableResult.getResponds());
        
        layout.setSpacing(true);
        layout.setMargin(true);
        /*
        layout.setComponentAlignment(modelName, Alignment.MIDDLE_LEFT);
        layout.setComponentAlignment(date, Alignment.MIDDLE_LEFT);
        layout.setComponentAlignment(modelTime, Alignment.MIDDLE_LEFT);
        layout.setComponentAlignment(numOfRuns, Alignment.MIDDLE_LEFT);
        layout.setComponentAlignment(autostop, Alignment.MIDDLE_LEFT);*/
        layout.setComponentAlignment(tableResult.getResponds(), Alignment.MIDDLE_CENTER);

        mainContent.addComponent(layout);

        this.setPositionX(175);
        this.setPositionY(60);
    }
    
//    /**
//     * Constructor for window with results of secondary statistics
//     */
//    public TableStatisticsResultWindow(String target, AggregateVariableStateCollector result, ExperimentSettings settings) {
//        super(CAPTION);
//
//        setResizable(false);
//
//        HorizontalLayout content = new HorizontalLayout();
//        setContent(content);
//        mainContent = new Panel();
//        mainContent.setHeight("575px");
//        mainContent.setWidth("650px");
//        mainContent.setScrollable(true);
//        content.addComponent(mainContent);
//
//        VerticalLayout layout = new VerticalLayout();
//
//        Table t = createTableVariablesState(target, result);
//        layout.addComponent(t);
//        layout.setSpacing(true);
//        layout.setMargin(true);
//        layout.setComponentAlignment(t, Alignment.MIDDLE_CENTER);
//        
//        mainContent.addComponent(layout);
//
//        this.setPositionX(175);
//        this.setPositionY(60);
//    }
    
//    private Table createTableVariablesState(String tableCaption, AggregateVariableStateCollector values) {
//        Table t = new Table(tableCaption);
//        t.addContainerProperty("Time", Double.class, "");
//        t.addContainerProperty("Value", String.class, "");
//        t.setStyleName("tableReport");
//        t.setWidth("100%");
//        List<Double> keys = new ArrayList<Double> (values.getValues().keySet());
//        Collections.sort(keys);
//        for (Double time: keys) {
//            t.addItem(new Object[] {time, values.getValue(time)}, null);
//        }
//        return t;
//    }
}