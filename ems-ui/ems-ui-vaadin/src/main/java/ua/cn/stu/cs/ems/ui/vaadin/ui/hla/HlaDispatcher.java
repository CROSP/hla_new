package ua.cn.stu.cs.ems.ui.vaadin.ui.hla;

import hla.rti.ConcurrentAccessAttempted;
import hla.rti.FederatesCurrentlyJoined;
import hla.rti.FederationExecutionDoesNotExist;
import hla.rti.RTIinternalError;
import java.io.FileNotFoundException;
import java.util.*;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.aggregates.Aggregate;
import ua.cn.stu.cs.ems.core.aggregates.AggregateChild;
import ua.cn.stu.cs.ems.core.aggregates.AggregateDefinition;
import ua.cn.stu.cs.ems.core.experiment.ExperimentReport;
import ua.cn.stu.cs.ems.core.experiment.PrimaryStatisticsElement;
import ua.cn.stu.cs.ems.core.experiment.PrimaryStatisticsResult;
import ua.cn.stu.cs.ems.core.experiment.statistics.StatisticsParameters;
import ua.cn.stu.cs.ems.core.experiment.statistics.StatisticsResult;
import ua.cn.stu.cs.ems.core.transitions.Transition;
import ua.cn.stu.cs.ems.core.utils.JessUtils;
import ua.cn.stu.cs.ems.ui.vaadin.*;
import ua.cn.stu.cs.ems.ui.vaadin.ui.components.MainContent;
import ua.cn.stu.cs.ems.ui.vaadin.ui.experiment.ExperimentSettings;
import ua.cn.stu.cs.hla.core.FederationExecution;
import ua.cn.stu.cs.hla.core.FomData;
import ua.cn.stu.cs.hla.core.FomXmlParsing;
import ua.cn.stu.cs.hla.core.XmlInfo;
import ua.cn.stu.cs.hla.core.utils.HlaUtils;
import ua.cn.stu.cs.hla.network.managementserver.ManagementServer;
import ua.cn.stu.cs.hla.network.managementserver.ModelingServerClient;
import ua.cn.stu.cs.hla.network.managementserver.requests.FederationDestroyedRequest;
import ua.cn.stu.cs.hla.network.managementserver.requests.PrepareToRunRequest;
import ua.cn.stu.cs.hla.network.managementserver.requests.PrepareToStartRequest;
import ua.cn.stu.cs.hla.network.managementserver.requests.StartRequest;
import ua.cn.stu.cs.hla.network.modelingserver.responses.MdlnSResponse;
import ua.cn.stu.cs.hla.network.modelingserver.responses.ReportResponse;

/**
 *
 * @author leonid
 */
public class HlaDispatcher implements Informant {

    private static final Logger logger = LoggerFactory.getLogger(HlaDispatcher.class);
    private FederationExecution federationExecution;
    public static final String HLA_MODEL_NAME = "HlaModel";
    private WidgetManager wm;
    private ManagementServer server;
    private AggregateDefinition rootAggregate;
    private HashMap<String, ArrayList<Federate>> federations = new HashMap<String, ArrayList<Federate>>();
    private HlaExperimentManager currentExperimentManager = null;

    /**
     * Convenience constructor with parameters.
     *
     * @param wm widget manager
     */
    public HlaDispatcher(WidgetManager wm) {
        this.wm = wm;
        wm.setHlaDispatcher(this);
        server = new ManagementServer();
        rootAggregate = null;
    }

    /**
     * Get the management server.
     *
     * @return the management server
     */
    public ManagementServer getServer() {
        return server;
    }

    public AggregateDefinition getRootAggregate() {
        return rootAggregate;
    }

    /**
     * Add name of the new federation to the federations list.
     *
     * @param name
     */
    public boolean createFederation(String name) {
        if (federations.containsKey(name)) {
            return false;
        }
        federations.put(name, new ArrayList<Federate>());
        return true;
    }

    public boolean createFederate(Federate federate) {
        if (federateExist(federate.getFederationName(), federate.getFederateName(), federate.getAggregateName())) {
            return false;
        }
        federations.get(federate.getFederationName()).add(federate);
        if (rootAggregate == null) {
            MainContent.WsRec ws = wm.getMainContent().getActiveWorkspace();
            rootAggregate = ws.getDispatcher().getRootAggregate();
        }
        return true;
    }

    public List<String> listFederations() {
        return new ArrayList<String>(federations.keySet());
    }

    public boolean federationExist(String name) {
        return federations.containsKey(name);
    }

    public boolean federateExist(String federation, String federate, String aggregate) {
        if (federationExist(federation)) {
            for (Federate fed : federations.get(federation)) {
                if (fed.getFederateName().equals(federate)
                        || (fed.getAggregateName().equals(aggregate))) {
                    return true;
                }
            }
        }
        return false;
    }

    public List<Federate> getFederates(String federation) {
        return new ArrayList<Federate>(federations.get(federation));
    }

    public ExperimentManager createExperimentManager(ExperimentSettings settings) {
        this.currentExperimentManager = new HlaDispatcher.HlaExperimentManager(settings);
        return currentExperimentManager;
    }

    public void destroy(String federationName) {
        if (federationExist(federationName)) {

            try {
                federationExecution.destroyFederation(federationName);
                federations.remove(federationName);
            } catch (Exception e) {
                logger.error("Exception while federation destroying");
            }
        }
    }

    private void printReport(ExperimentReport report) {
        if (report == null) {
            return;
        }
        Set<String> list = report.getStatisticsObjectsNames();
        for (String object : list) {
            List<PrimaryStatisticsResult> statresult = report.getPrimaryStatisticsFor(object);
            for (PrimaryStatisticsResult primaryStatisticsResult : statresult) {
                for (PrimaryStatisticsElement se : primaryStatisticsResult.getDumpedPrimaryStatisticsElements()) {
                    StatisticsResult result = se.getStatisticsResult();
                    System.out.println(result);
                }
            }
        }
    }

    private FomData createTmpFom(ManagementServer server, String federationName) {
        List<String> aggs = new ArrayList<String>();
        for (ModelingServerClient client : server.getMsClients()) {
            aggs.add(client.getAggregateName());
        }
        if (aggs.isEmpty()) {
            return null;
        }
        List<XmlInfo> classData = new ArrayList<XmlInfo>();
        FomData fomData = null;
        Set<Aggregate> aggSet = rootAggregate.getChildAggregates();
        for (Aggregate aggregate : aggSet) {
            for (String aggregateName : aggs) {
                if (aggregateName.equals(aggregate.getName())) {
                    Set<String> attrs = new HashSet<String>();
                    fillAttributes(aggregate, attrs);
                    List<String> attributes = new ArrayList<String>();
                    attributes.addAll(attrs);
                    XmlInfo classInfo = new XmlInfo(aggregateName, attributes);
                    classData.add(classInfo);
                }
            }
        }
        if (!classData.isEmpty()) {
            try {
                FomXmlParsing.createFomFile(classData, null, federationName + "FOM.xml");
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            } catch (TransformerException e) {
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            fomData = new FomData();
            fomData.setClassData(classData);
            return fomData;
        }
        return fomData;
    }

    private void destroyTmpFom(String federationName) {
        FomXmlParsing.removeFomFile(federationName + "FOM.xml");
    }

    private void fillAttributes(Aggregate aggregate, Set<String> attrs) {
        for (Aggregate agg : aggregate.getChildAggregates()) {
            fillAttributes(agg, attrs);
        }
        for (Place place : aggregate.getPlaces()) {
            if (place.getToken() != null) {
                for (String attrName : place.getToken().getAttributeNames()) {
                    attrs.add(attrName);
                }
            }
        }
    }

    public Collection<String> listAllEntities() {
        Collection<String> result = new HashSet<String>();
        for (Place p : rootAggregate.getPlaces()) {
            result.add(p.getName());
        }
        for (Transition t : rootAggregate.getTransitions()) {
            result.add(t.getName());
        }
        for (ua.cn.stu.cs.ems.core.queues.Queue q : rootAggregate.getQueues()) {
            result.add(q.getName());
        }

        return result;
    }

    public Collection<String> listAllVariables() {
        return rootAggregate.getVariablesNames();
    }

    public Collection<String> listAll(EntityClass entityClass) {
        return listAll(null, entityClass);
    }

    public StatisticsParameters[] getApplicableParameters(String entityName) {
        return StatisticsParameters.getApplicableParameters(find(entityName, true));
    }

    public String getModelName() {
        return getRootAggregate().getName();
    }

    public AggregateChild findAggregateChild(String id) {
        return null;
    }

    public List<String> listAll(String aggPath, EntityClass type) {
        List<String> list = new ArrayList<String>();

        Aggregate agg = (Aggregate) JessUtils.getAggregateChild(rootAggregate, aggPath);

        Collection<AggregateChild> childs = new ArrayList<AggregateChild>();
        if (agg != null) {
            switch (type) {
                case PLACE: {
                    childs.addAll(agg.getPlaces());
                    break;
                }
                case QUEUE: {
                    childs.addAll(agg.getQueues());
                    break;
                }
                case TRANSITION: {
                    childs.addAll(agg.getTransitions());
                    break;
                }
                case AGGREGATE: {
                    childs.addAll(agg.getChildAggregates());
                    break;
                }
            }
        }

        for (AggregateChild ac : childs) {
            list.add(ac.getName());
        }

        return list;
    }

    public List<String> listAllVariables(String aggPath) {
        Aggregate agg = (Aggregate) JessUtils.getAggregateChild(rootAggregate, aggPath);
        List<String> list = new ArrayList<String>();
        if (agg != null) {
            for (String var : agg.getVariablesNames()) {
                list.add(var);
            }
        }
        return list;
    }

    private AggregateChild find(String id, boolean silent) {
        AggregateChild entity = findPlace(id, silent);
        if (null == entity) {
            return findAggregateChild(rootAggregate, id, silent);
        } else {
            return entity;
        }
    }

    private AggregateChild findAggregateChild(Aggregate a, String id, boolean silent) {
        int pos = id.indexOf('.');
        while (pos > 0) {
            String agName = id.substring(0, pos);
            id = id.substring(pos + 1);
            a = a.getAggregate(agName);
            pos = id.indexOf('.');
        }

        AggregateChild child = a.getAggregateChild(id);
        if (null == child && !silent) {
            throw new InconsistentRequestException("No aggregate child with id " + id + " found");
        }
        return child;
    }

    private Place findPlace(String id, boolean silent) {
        Place place = rootAggregate.getPlace(id);
        if (null == place && !silent) {
            throw new InconsistentRequestException("No place with id " + id + " found");
        }
        return place;
    }

    public String getVariableValue(String var) {
        return null;
    }

    /**
     * This class is responsible for starting of experiment.
     */
    private class HlaExperimentManager implements ExperimentManager {

        private final ExperimentSettings settings;
        private Dispatcher.ExperimentProgressListener experimentListener;
        private ExperimentReport experimentReport;

        /**
         * Creates new instance of JessExperimentManager.
         *
         * @param settings Settings for launching the experiment.
         */
        private HlaExperimentManager(ExperimentSettings settings) {
            this.settings = settings;
        }

        public Double[] getStatisticsDumpTimes() {
            return null; //ToDo Dummy
//            return experiment.StatisticsDumpTimes();
        }

        public void setExperimentListener(Dispatcher.ExperimentProgressListener experimentListener) {
            this.experimentListener = experimentListener;
        }

        public void start() {
            new Thread(new Runnable() {

                public void run() {
                    experimentListener.modelStarted();
                    String federationName = settings.getModelName();
                    if (!settings.getPrimaryStatisticSettings().isEnabled()) {
                        throw new RuntimeException("Must be enabled at least one type of collection of statistics!");
                    }
                    List<Federate> feds = federations.get(federationName);
                    if (feds == null || feds.isEmpty()) {
                        throw new RuntimeException("Federation with name" + federationName + "doesn't exist");
                    }
                    ManagementServer mServer = new ManagementServer();
                    for (Federate federate : feds) {
                        ModelingServerClient modelingServerClient = new ModelingServerClient(federate.getFederationName(), federate.getFederateName(), federate.getRootAggregateName(), federate.getAggregateName(), federate.getServerAdress(), mServer.getResponsePool());
                        mServer.addMSClient(modelingServerClient);
                    }

                    PrepareToRunRequest request = new PrepareToRunRequest();
                    //set params of experiment
                    request.collectPrimaryStat = settings.getPrimaryStatisticSettings().isEnabled();
                    request.collectStatOnEvent = settings.getPrimaryStatisticSettings().isCollectOnEvent();
                    if (!request.collectStatOnEvent) {
                        request.dumpPeriod = settings.getPrimaryStatisticSettings().getStatisticsPeriod();
                    }
                    request.finishTime = settings.getModelingTime();

                    List<String> objectsForStat = new ArrayList<String>();
                    objectsForStat.addAll(settings.getPrimaryStatisticSettings().getSubjectEntities());
                    request.objectsForCollectStat = objectsForStat;

                    mServer.sendRequest(request);
                    //wait for responce from all modeling servers
                    mServer.waitForResponses();
                    //when all modeling servers has responded we shoyl clear response pool
                    mServer.getResponsePool().clear();
                    //create FOM
                    FomData fomData = createTmpFom(mServer, federationName);
                    //create Federation
                    federationExecution = new FederationExecution();
                    federationExecution.createFederation(federationName, federationName + "FOM.xml");
                    //wait unlil federation will be created
                    while (!federationExecution.isFederationCreated()) {
                    }
                    System.out.println("Federation created");
                    //start modeling
                    PrepareToStartRequest ptosrequest = new PrepareToStartRequest();
                    ptosrequest.fomData = fomData;
                    mServer.sendRequest(ptosrequest);
                    //wait for report responses from all modeling servers
                    mServer.waitForResponses();
                    //crear again
                    mServer.getResponsePool().clear();
                    //all federates has runned and now they all can to start
                    mServer.sendRequest(new StartRequest());
                    //wait for report responses from all modeling servers
                    mServer.waitForResponses();
                    experimentReport = mServer.getResponsePool().getReport();
                   // printReport(experimentReport);
                    try {
                        federationExecution.destroyFederation(federationName);
                    } catch (FederatesCurrentlyJoined ex) {
                        logger.error("FederatesCurrentlyJoined", ex);
                    } catch (FederationExecutionDoesNotExist ex) {
                        logger.error("FederationExecutionDoesNotExist", ex);
                    } catch (RTIinternalError ex) {
                        logger.error("RTIinternalError", ex);
                    } catch (ConcurrentAccessAttempted ex) {
                        logger.error("ConcurrentAccessAttempted", ex);
                    }
                    mServer.sendRequest(new FederationDestroyedRequest());
                    mServer.stop();
                    destroyTmpFom(federationName);
                    experimentListener.modelFinished();
                    System.out.println("End");

                }
            }).start();
        }

        public void stop() {
        }

        public long getFactorRuns() {
            return 0;
        }

        public double getModelingTime() {
            return settings.getModelingTime();
        }

        public int getExperimentRuns() {
            // TODO This is a braindead solution. It works in case of CountDownExperiment when user specifies number 
            // of runs, but if auto-stop is used number of experiment runs depends on experiment implementation
            return settings.getNumberOfRuns();
        }

        public ExperimentReport getExperimentReport() {
            return experimentReport;
        }

        public boolean isPrimatyStatisticsEnabled() {
            return this.settings.getPrimaryStatisticSettings().isEnabled();
        }

        public void debug() {
        }

        public void stopDebug() {
        }

        public boolean isSecondaryStatisticsEnabled() {
            return false;
        }
    }
}
