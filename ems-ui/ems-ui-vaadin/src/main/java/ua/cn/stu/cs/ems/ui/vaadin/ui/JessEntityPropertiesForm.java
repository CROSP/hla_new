package ua.cn.stu.cs.ems.ui.vaadin.ui;

import com.vaadin.data.Item;
import com.vaadin.data.Validator;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.TextField;
import ua.cn.stu.cs.ems.ui.vaadin.dto.JessEntityProperties;
import ua.cn.stu.cs.ems.ui.vaadin.ui.input.table.VariablesEditor;
import ua.cn.stu.cs.ems.ui.vaadin.validation.EclSyntaxValidator;
import ua.cn.stu.cs.ems.ui.vaadin.validation.EmptyStringValidator;
import ua.cn.stu.cs.ems.ui.vaadin.validation.NameValidator;

/**
 * @author n0weak
 */
class JessEntityPropertiesForm extends AbstractForm {

    private static final String EDITOR_WIDTH = "600px";
    private static final String TEXT_FIELD_WIDTH = "150px";

    JessEntityPropertiesForm(JessEntityProperties properties, EclSyntaxValidator eclValidator,
                             NameValidator nameValidator) {
        super(new BeanItem<JessEntityProperties>(properties), new JessEntityPropertiesFiledFactory(eclValidator,
                                                                                                   nameValidator,
                                                                                                   properties));
        setVisibleItemProperties(new Object[]{"name", "attributes", "delayFunction", "transformationFunction",
                                              "permittingFunction", "priorityFunction"});
    }

    private static class JessEntityPropertiesFiledFactory extends DefaultFieldFactory {

        private final EclSyntaxValidator eclValidator;
        private final JessEntityProperties properties;
        private final NameValidator nameValidator;
        private final Validator emptyStringValidator = new EmptyStringValidator();

        private JessEntityPropertiesFiledFactory(EclSyntaxValidator eclValidator, NameValidator nameValidator,
                                                 JessEntityProperties properties) {
            this.eclValidator = eclValidator;
            this.nameValidator = nameValidator;
            this.properties = properties;
        }

        @Override
        public Field createField(Item item, Object propertyId, Component uiContext) {
            Field f = null;
            if ("name".equals(propertyId)) {
                f = new TextField();
                f.setWidth(TEXT_FIELD_WIDTH);
                f.setCaption("Name");
                f.addValidator(nameValidator);
                f.addValidator(emptyStringValidator);
                ((TextField) f).setImmediate(true);
            }
            else if ("attributes".equals(propertyId)) {
                f = new VariablesEditor("Token Attributes", ((JessEntityProperties.PlaceProperties) properties).
                        getAttributes());
                f.setWidth(EDITOR_WIDTH);
            }
            else if ("delayFunction".equals(propertyId)) {
                f = new EclEditor(EDITOR_WIDTH, eclValidator);
                f.setCaption("Delay Function");
            }
            else if ("transformationFunction".equals(propertyId)) {
                f = new EclEditor(EDITOR_WIDTH, eclValidator);
                f.setCaption("Transformation Function");
            }
            else if ("permittingFunction".equals(propertyId)) {
                f = new EclEditor(EDITOR_WIDTH, eclValidator);
                f.setCaption("Permitting Function");
            }
            else if ("priorityFunction".equals(propertyId)) {
                f = new EclEditor(EDITOR_WIDTH, eclValidator);
                f.setCaption("Priority Function");
            }
//            else if ("name".equals(propertyId)) {
//                f = new TextField();
//                f.setWidth(TEXT_FIELD_WIDTH);
//                f.setCaption("Name");
//            }
            return f;
        }
    }
}
