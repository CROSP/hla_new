package ua.cn.stu.cs.ems.ui.vaadin.utils.db;

import java.beans.PropertyDescriptor;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;
import javax.sql.PooledConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.cn.stu.cs.ems.ui.vaadin.utils.PropertiesReader;
import ua.cn.stu.cs.ems.ui.vaadin.utils.PropertiesReader.Property;

/**
 * The AggregateEntityFactory is class for AggregateEntity creation.
 */
public class AggregateEntityFactory {

    final static Logger logger = LoggerFactory.getLogger(AggregateEntityFactory.class);
    protected static AggregateEntityFactory factorySingleton = null;
    protected static HashMap entities = null;
    protected static int maxSizeOfCache = -1; // default no cache
    protected static PropertyDescriptor propertyDescriptors[];
    protected static ConnectionPool pool;
    protected static String data_table_name;
    protected static String data_table_pk;
    protected static String data_table_cdata;
    protected static String data_table_cname;
    protected static String data_table_ctype;

    /**
     * set max size of cache
     */
    public void setMaxSizeOfCache(int maxSize) {
        maxSizeOfCache = maxSize;
    }

    /**
     * return size of cache
     */
    public int getCountInCache() {
        return entities.size();
    }

    /**
     * put XEntity in cache
     */
    public void putInEntities(String key, AggregateEntity entity) {
        // entities.put(key,entity);

        if (maxSizeOfCache == -1) {
            return;
        }

        if (entities.size() < maxSizeOfCache) {
            logger.debug("Aggregate " + entity + " in cache!");
            entities.put(key, entity);
        } else {
            logger.debug("get last aggregate in cache for remove....");
            logger.debug("size of cache: " + entities.size());
            Set keys = entities.keySet();

            String keyForRemove = (String) keys.iterator().next();
            Iterator i = keys.iterator();
            while (i.hasNext()) {
                keyForRemove = (String) i.next();
                // Log.print(".");
            }

            AggregateEntity removeXEntity = (AggregateEntity) entities.remove(keyForRemove);
            if (removeXEntity != null) {
                logger.debug("remove AggregateEntity: " + removeXEntity + " from cache!");
            }

            logger.debug("and put AggregateEntity: " + entity + " in cache!");
            entities.put(key, entity);
        }
    }

    /**
     * Finder method. Finds by Primary Key with appended fields.
     * 
     * @roseuid 3BB32E4C03C0
     */
    public AggregateEntity findByPrimaryKey(String aPK) throws Exception {
        PooledConnection pconn = null;
        Statement st = null;
        ResultSet rs = null;
        try {
            AggregateEntity entity;
            synchronized (entities) {
                if (entities.containsKey(aPK)) {
                    logger.debug("byPrimaryKey:" + aPK + ":from cache.");
                    entity = (AggregateEntity) entities.get(aPK);
                    return entity;
                }
                logger.debug("byPrimaryKey:" + aPK + ":from database.");
                pconn = pool.getPooledConnection();
                st = pconn.getConnection().createStatement();
                rs = st.executeQuery("SELECT * FROM " + data_table_name + 
                        " WHERE id=" + aPK);

                if (!rs.next()) {
                    rs.close();
                    st.close();
                    pconn.close();
                    return null;
                }

                entity = new AggregateEntity(rs.getLong(data_table_pk), 
                        rs.getString(data_table_cname), 
                        rs.getString(data_table_cdata), 
                        rs.getString(data_table_ctype));
                
                putInEntities("" + rs.getLong(data_table_pk), entity);

                rs.close();
                st.close();
                pconn.close();
                return entity;
            } // end of synchronized block
        } catch (Exception _e) {
            logger.debug("XEntityFactory: Exception:" + _e);
            try {
                rs.close();
            } catch (Exception _ex) {
                logger.debug("AggregateEntityFactory: Exception:" + _ex);
            }
            try {
                st.close();
            } catch (Exception _ex) {
                logger.debug("AggregateEntityFactory: Exception:" + _ex);
            }
            try {
                pconn.close();
            } catch (Exception _ex) {
                logger.debug("AggregateEntityFactory: Exception:" + _ex);
            }
            throw new Exception(_e.getMessage());
        }

    }

    private boolean testExistInResult(Vector result, AggregateEntity entity) {
        if (result != null) {            
            for (int c = 0; c < result.size(); c++) {
                Object x = result.get(c);
                if (x.equals(entity)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Finder method. Finds by any criteria with appended fields.
     */
    public java.util.Vector<AggregateEntity> findByCriteria(String aCriteria) throws Exception {
        logger.debug("query=" + aCriteria);
        PooledConnection pconn = null;
        Statement st = null;
        ResultSet rs = null;
        try {
            String query = "SELECT * FROM " + data_table_name + " ";

            if ((aCriteria != null) && (!aCriteria.equals(""))) {
                query += " WHERE " + aCriteria;
            }

            Vector result = new Vector();

            logger.debug("findByCriteria:query:" + query);
            synchronized (entities) {
                pconn = pool.getPooledConnection();
                st = pconn.getConnection().createStatement();
                rs = st.executeQuery(query);
                AggregateEntity entity;
                int c = 0;
                while (rs.next()) {
                    entity = (AggregateEntity) entities.get(rs.getLong("id"));

                    if (entity != null) {
                        logger.debug("byCriteria:" + entity.getId() + ":from cache.");
                        if (!testExistInResult(result, entity)) {
                            result.add(entity);
                        }
                    } else {
                        logger.debug("byCriteria:" + rs.getString(data_table_pk) + ":from database.");

                        entity = new AggregateEntity(rs.getLong(data_table_pk), 
                                rs.getString(data_table_cname), 
                                rs.getString(data_table_cdata),
                                rs.getString(data_table_ctype));

                        putInEntities("" + rs.getLong(data_table_pk), entity);

                        if (!testExistInResult(result, entity)) {
                            result.add(entity);
                        }
                    }
                }
                rs.close();
                st.close();
                pconn.close();
                return result;
            }
        } catch (Exception _e) {
            logger.debug("Exception:" + _e);
            try {
                rs.close();
            } catch (Exception _ex) {
                logger.debug("Exception:" + _ex);
            }
            try {
                st.close();
            } catch (Exception _ex) {
                logger.debug("Exception:" + _ex);
            }
            try {
                pconn.close();
            } catch (Exception _ex) {
                logger.debug(": Exception:" + _ex);
            }
            throw new Exception(_e.getMessage());
        }
    }

    /**
     * Counter method. Counts by any criteria.
     */
    public int countByCriteria(String aCriteria) throws Exception {
        PooledConnection pconn = null;
        Statement st = null;
        ResultSet rs = null;
        try {
            String query = "SELECT count(" + data_table_pk + ") AS result FROM "
                    + data_table_name;

            if ((aCriteria != null) && (!aCriteria.equals(""))) {
                query += " WHERE " + aCriteria;
            }

            logger.debug("countByCriteria:query:" + query);
            pconn = pool.getPooledConnection();
            st = pconn.getConnection().createStatement();
            rs = st.executeQuery(query);
            int result = 0;
            if (rs.next()) {
                result = rs.getInt("result");
            }
            rs.close();
            st.close();
            pconn.close();
            return result;
        } catch (Exception _e) {
            logger.debug("Exception:" + _e);
            try {
                rs.close();
            } catch (Exception _ex) {
                logger.debug("Exception:" + _ex);
            }
            try {
                st.close();
            } catch (Exception _ex) {
                logger.debug("Exception:" + _ex);
            }
            try {
                pconn.close();
            } catch (Exception _ex) {
                logger.debug("Exception:" + _ex);
            }
            throw new Exception(_e.getMessage());
        }
    }

    public void removeAggregateEntity(long pk) throws Exception {
        PooledConnection pconn = null;
        Statement st = null;
        try {
            synchronized (entities) {
                pconn = pool.getPooledConnection();
                st = pconn.getConnection().createStatement();

                st.executeUpdate("DELETE FROM " + data_table_name + " WHERE " + data_table_pk + "=" + pk);
                st.close();
                pconn.close();

                entities.remove(pk);
            }
        } catch (Exception _e) {
            logger.debug("Exception:" + _e);
            try {
                st.close();
            } catch (Exception _ex) {
                logger.debug("Exception:" + _ex);
            }
            try {
                pconn.close();
            } catch (Exception _ex) {
                logger.debug("Exception:" + _ex);
            }
            throw new Exception(_e.getMessage());
        }
    }
    
    public void storeAggregateEntity(AggregateEntity anEntity) throws Exception {
        logger.debug("storing AE: " + anEntity.getName());
        logger.debug("data=" + anEntity.getData());
        logger.debug("id=" + anEntity.getId());
        PooledConnection pconn = null;
        Statement st = null;
        try {
            synchronized (anEntity) {
                pconn = pool.getPooledConnection();
                st = pconn.getConnection().createStatement();

                String query;
                if (anEntity.getId() == -1) {
                    query = "INSERT into " + data_table_name + "(" 
                        + data_table_cname + "," 
                        + data_table_cdata + "," 
                        + data_table_ctype + ") values("
                        + "'" + anEntity.getName() + "'," 
                        + "'" + anEntity.getData().replaceAll("'", "''") + "',"
                        + "'" + anEntity.getType().getName() + "')";
                }
                else {
                    query = "UPDATE "
                        + data_table_name + " SET " + data_table_cname + "='"
                        + anEntity.getName() + "'," + data_table_cdata + "='"
                        + anEntity.getData().replaceAll("'", "''") + "'," + data_table_ctype + "='"
                        + anEntity.getType() + "' WHERE " + data_table_pk + "="
                        + anEntity.getId();
                }
                
                logger.debug("storeXEntity:query:" + query);
                st.executeUpdate(query);
                st.close();
                pconn.close();
            }
        } catch (Exception _e) {
            logger.debug("Exception:" + _e);
            try {
                st.close();
            } catch (Exception _ex) {
                logger.debug("Exception:" + _ex);
            }
            try {
                pconn.close();
            } catch (Exception _ex) {
                logger.debug("Exception:" + _ex);
            }
            throw new Exception(_e.getMessage());
        }
    }

    /**
     * @roseuid 3BB3331601CC
     */
    public static synchronized AggregateEntityFactory getFactory() {
        logger.debug("getting factory");
        synchronized (AggregateEntityFactory.class) {
            try {
                if (factorySingleton == null) {
                    factorySingleton = new AggregateEntityFactory();
                    logger.debug("getFactory:creating!");
                } else {
                    logger.debug("getFactory:getting!");
                }
            } catch (Exception _e) {
                //_e.printStackTrace();
                logger.debug("Exception:" + _e);
                return null;
            }
        }
        return factorySingleton;
    }

    /**
     * @roseuid 3BB44C9B0014
     */
    protected AggregateEntityFactory() throws ClassNotFoundException,
            SQLException {
        logger.debug("creating entity factory");
        entities = new HashMap();
        data_table_name = PropertiesReader.read(Property.DB_TABLE_NAME);
        data_table_pk = PropertiesReader.read(Property.DB_TABLE_PK);
        data_table_cname = PropertiesReader.read(Property.DB_TABLE_C_NAME);
        data_table_cdata = PropertiesReader.read(Property.DB_TABLE_C_DATA);
        data_table_ctype = PropertiesReader.read(Property.DB_TABLE_C_TYPE);
        
        Class.forName(PropertiesReader.read(Property.DB_CONNECTION_DRIVER));

        pool = ConnectionPool.getConnectionPool();
        
        logger.debug("getter vars: " + data_table_name + " " + data_table_pk);
    }
}