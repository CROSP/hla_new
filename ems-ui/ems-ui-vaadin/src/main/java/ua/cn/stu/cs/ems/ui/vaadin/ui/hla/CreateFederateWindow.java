package ua.cn.stu.cs.ems.ui.vaadin.ui.hla;

import com.vaadin.ui.*;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.cn.stu.cs.ems.core.aggregates.Aggregate;
import ua.cn.stu.cs.ems.core.aggregates.AggregateDefinition;
import ua.cn.stu.cs.ems.ui.vaadin.WidgetManager;
import ua.cn.stu.cs.ems.ui.vaadin.ui.AbstractSettingsWindow;
import ua.cn.stu.cs.ems.ui.vaadin.ui.components.MainContent;
import ua.cn.stu.cs.ems.ui.vaadin.utils.PropertiesReader;
import ua.cn.stu.cs.ems.ui.vaadin.validation.EmptyStringValidator;

/**
 *
 * @author slava
 */
public class CreateFederateWindow extends AbstractSettingsWindow {

    final static Logger logger = LoggerFactory.getLogger(CreateFederateWindow.class);
    private ComboBox federationName;
    private TextField federateName;
    private ComboBox modelName;
    private ComboBox modelingServerAddress;
    private final WidgetManager wm;
    private final HlaDispatcher dispatcher;
    private int stage = 1;
    private Layout stageLayout;
    private Button commitButton;
    private AggregateDefinition rootAggregate;

    public CreateFederateWindow(String caption, String width, WidgetManager wm,
            HlaDispatcher dispatcher) {
        super(caption);
        setWidth(width);

        this.wm = wm;
        this.dispatcher = dispatcher;
        MainContent.WsRec ws = wm.getMainContent().getActiveWorkspace();
        rootAggregate = ws.getDispatcher().getRootAggregate();
        paint();
    }

    @Override
    protected boolean apply() {
        if (federationName.isValid() && rootAggregate != null) {
            Federate federate;
            federate = new Federate((String) federationName.getValue(),
                    (String) federateName.getValue(), rootAggregate.getName(), (String) modelName.getValue(), (InetAddress) (modelingServerAddress.getValue()));
            dispatcher.createFederate(federate);
            return true;
        }
        return false;

//        switch (stage) {
//            case 1: {
//                if (federationName.isValid() && federateName.isValid()
//                        && modelingServerAddress.isValid() && modelName.isValid()) {
//                    switchStage(2);
//                    return false;
//                }
//                break;
//            }
//            case 2: {
        // checking all CM params
//                if (federate == null) {
//                    return false;
//                }

//                if (dispatcher.createFederate((String)federationName.getValue(), federate)) {
//                    return true;
//                }
//                return false;
//            }
//        }
//
//        return false;
    }

    protected final void paint() {
        VerticalLayout layout = (VerticalLayout) getContent();
        layout.setSizeFull();

        logger.debug("creating stage1");
        stageLayout = createStage1();
        layout.addComponent(stageLayout);
        layout.setExpandRatio(stageLayout, 1.0f);

        logger.debug("switching stage1");

        layout.addComponent(createFooter());

        switchStage(1);
    }

    private Layout createStage1() {
        VerticalLayout layout = new VerticalLayout();
        layout.setSizeFull();
        layout.setHeight("165px");

        federationName = new ComboBox();
        federationName.setCaption("Federation name");
        federationName.setRequired(true);
        federationName.addValidator(new EmptyStringValidator());
        federationName.setWidth("100%");

        for (String federation : dispatcher.listFederations()) {
            federationName.addItem(federation);
        }

        layout.addComponent(federationName);
        layout.setExpandRatio(federationName, 1.0f);

        modelingServerAddress = new ComboBox();
        modelingServerAddress.setCaption("Modeling server address:");
        modelingServerAddress.setRequired(true);
        modelingServerAddress.addValidator(new EmptyStringValidator());
        modelingServerAddress.setWidth("100%");

        String timeMillsString = PropertiesReader.read(PropertiesReader.Property.DISCOVER_HOSTS_TIME);
        int timeMills = Integer.valueOf(timeMillsString).intValue();
        Set<InetAddress> mdlslist = dispatcher.getServer().discoverModelingServers(timeMills);
        mdlslist = filterIpAddresses(mdlslist);
        for (InetAddress inetAddress : mdlslist) {
            modelingServerAddress.addItem(inetAddress);
        }

        layout.addComponent(modelingServerAddress);
        layout.setExpandRatio(modelingServerAddress, 1.0f);

        federateName = new TextField();
        federateName.setCaption("Federate name");
        federateName.setWidth("100%");
        federateName.addValidator(new EmptyStringValidator());
        federateName.setRequired(true);
        layout.addComponent(federateName);
        layout.setExpandRatio(federateName, 1.0f);

        modelName = new ComboBox();
        modelName.setCaption("Aggregate name");
        modelName.setWidth("100%");
        modelName.setRequired(true);
        modelName.addValidator(new EmptyStringValidator());

        if (rootAggregate != null) {
            for (Aggregate aggregate : rootAggregate.getChildAggregates()) {
                modelName.addItem(aggregate.getName());
            }
        }

        layout.addComponent(modelName);
        layout.setExpandRatio(modelName, 1.0f);

        return layout;
    }

//    private Layout createStage2() {
//        VerticalLayout layout = new VerticalLayout();
//        layout.setSizeFull();
//        layout.setHeight("165px");
//
//        logger.debug("creating hsp");
//        HorizontalSplitPanel hsp = new HorizontalSplitPanel();
//        hsp.setLocked(true);
//
//        Panel p1 = new Panel();
//        Panel p2 = new Panel();
//        hsp.setFirstComponent(p1);
//        hsp.setSecondComponent(p2);
//        p1.setSizeFull();
//        p2.setSizeFull();
//
//        logger.debug("creating hsp: panels setted");
//
//        String model = modelName.getValue().toString();
//        logger.debug("model name=" + model);
//        AggregateRegistry ar = wm.getSaveManager().getAggregateRegestryCopy();
//        AggregateDefinition adModel = wm.getSaveManager().getModel(model, ar);
//        logger.debug("model loaded: " + adModel);
//
//        adModel.changeName((String)federateName.getValue());
//        federate = new Federate(adModel, (String) clientAddress.getValue());
//
//        Collection<String> inputs = federate.getInputsList();
//        Collection<String> outputs = federate.getOutputsList();
//
//        for (String io: inputs) {
//            logger.debug("adding input: " + io);
//            createSecondStageField(io, p1);
//        }
//        for (String io: outputs) {
//            logger.debug("adding output: " + io);
//            createSecondStageField(io, p2);
//        }
//
//        layout.addComponent(hsp);
//        return layout;
//    }
//    private Window createConnectModuleIO(final String name) {
//        VerticalLayout layout = new VerticalLayout();
//        final Window w = new Window("Configure attributes of \"" + name + "\"");
//        w.setWidth("700px");
//        w.setHeight("280px");
//        w.setContent(layout);
//        w.setClosable(true);
//        w.setResizable(false);
//        w.setPositionX((int) (wm.getWindow().getWidth() - w.getWidth()) / 2);
//        w.setPositionY((int) (wm.getWindow().getHeight() / 2 - w.getHeight() / 2));
//
//        layout.setMargin(true);
//        FormLayout fl = new FormLayout();
//        layout.addComponent(fl);
//
//        final TwinColSelect tcs = new TwinColSelect();
//        tcs.setCaption(name);
//        tcs.setLeftColumnCaption("Available attributes");
//        tcs.setRightColumnCaption("Enabled attributes");
//        tcs.setSizeFull();
//
////        for (String attrName: federate.getGlobalAttributesList()) {
////            tcs.addItem(attrName);
////        }
//
//        SpaceWrapper sw = new SpaceWrapper(tcs);
//        sw.setMargins(new int[]{0, 0, 10, 0});
//        sw.setWidth("677px");
//        layout.addComponent(sw);
//
//        Button b;
//        HorizontalLayout hl = new HorizontalLayout();
//        hl.setSizeFull();
//
//        b = new Button("Discard");
//        b.setStyleName(getButtonStyle());
//        b.addListener(new Button.ClickListener() {
//
//            public void buttonClick(ClickEvent event) {
//                w.getParent().removeWindow(w);
//            }
//        });
//        hl.addComponent(b);
//        hl.setComponentAlignment(b, Alignment.MIDDLE_LEFT);
//
//        b = new Button("Apply");
//        b.setStyleName(getButtonStyle());
//        b.addListener(new Button.ClickListener() {
//
//            public void buttonClick(ClickEvent event) {
//                Set<String> attributes = (Set<String>) tcs.getValue();
//                logger.debug("setting user attrs for io: " + name);
////                federate.setUserAttributes(name, new ArrayList<String>(attributes));
//                w.getParent().removeWindow(w);
//            }
//        });
//
//        hl.addComponent(b);
//        hl.setComponentAlignment(b, Alignment.MIDDLE_RIGHT);
//
//        layout.addComponent(hl);
//
//        return w;
//    }
//    private void createSecondStageField(final String name, final Panel target) {
//        Button b = new Button(name);
//        b.addListener(new Button.ClickListener() {
//
//            public void buttonClick(ClickEvent event) {
//                wm.getWindow().addWindow(createConnectModuleIO(name));
//            }
//        });
//        b.setStyleName(ChameleonTheme.BUTTON_LINK);
//        target.addComponent(b);
//    }
    private Layout createFooter() {
        HorizontalLayout footer = new HorizontalLayout();

        Button discardButton = createDiscardButton();
        footer.addComponent(discardButton);
        footer.setComponentAlignment(discardButton, Alignment.MIDDLE_LEFT);

        commitButton = createCommitButton();
        footer.addComponent(commitButton);
        footer.setComponentAlignment(commitButton, Alignment.MIDDLE_RIGHT);

        footer.setWidth("100%");

        return footer;
    }

    private void switchStage(int stage) {
        this.stage = stage;
        switch (stage) {
            case 1: {
                commitButton.setCaption("Create");
                break;
            }
            case 2: {
//                commitButton.setCaption("Create");
//                stageLayout.removeAllComponents();
//
//                Layout l = createStage2();
//                getContent().replaceComponent(stageLayout, l);
//                stageLayout = l;
                break;
            }
        }
    }

    /**
     * Filter addresses. Management server and modeling servers should be in the
     * same local subnet.
     *
     * @param addresses
     * @return filtered addresses
     */
    private Set<InetAddress> filterIpAddresses(Set<InetAddress> addresses) {
        String pathToRID = System.getProperty("RTI_RID_FILE");

        Properties properties = new Properties();
        InputStream is = null;
        try {
            is = new FileInputStream(pathToRID);
        } catch (FileNotFoundException ex) {
            logger.error("Can't find properties on the path" + pathToRID, ex);
        }
        try {
            properties.load(is);
        } catch (IOException ex) {
            logger.error("Failed to load properties from " + pathToRID, ex);
            System.exit(0);
        }
        try {
            is.close();
        } catch (IOException ex) {
            logger.error("Failed to close inputstream while properties loading", ex);
        }
        String bindAddress = properties.getProperty("portico.jgroups.udp.bindAddress");

        int indexBindAddress = bindAddress.indexOf(".");
        String beginBindAddress = bindAddress.substring(0, indexBindAddress);

        Set<InetAddress> result = new HashSet<InetAddress>();
        for (InetAddress inetAddress : addresses) {
            int indexAddress = inetAddress.getHostAddress().indexOf(".");
            String beginInetAddress = inetAddress.getHostAddress().substring(0, indexAddress);
            if (beginInetAddress.equals(beginBindAddress)) {
                result.add(inetAddress);
            }
        }
        return result;
    }
}
