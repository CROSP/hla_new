package ua.cn.stu.cs.ems.ui.vaadin.ui.hla;

import com.vaadin.data.Validator;
import com.vaadin.data.Validator.InvalidValueException;
import com.vaadin.ui.*;
import java.util.Collection;
import ua.cn.stu.cs.ems.ui.vaadin.WidgetManager;
import ua.cn.stu.cs.ems.ui.vaadin.ui.AbstractSettingsWindow;

/**
 *
 * @author slava
 */
public class DestroyFederationWindow extends AbstractSettingsWindow {

    private ComboBox federationName;
    private final Collection<String> federationsList;
    private static final String HEIGHT = "128px";
    private WidgetManager wm;

    public DestroyFederationWindow(String caption, String width, Collection<String> federationsList,
            WidgetManager wm) {
        super(caption);
        setWidth(width);
        setHeight(HEIGHT);
        this.federationsList = federationsList; 
        this.wm = wm;
        paint();
    }

    @Override
    protected boolean apply() {
        if (federationName.isValid()) {
            wm.getHlaDispatcher().destroy(federationName.getValue().toString());
            return true;
        }
        return false;
    }

    protected void paint() {
        VerticalLayout layout = (VerticalLayout) getContent();
        layout.setSizeFull();

        federationName = new ComboBox();
        federationName.setCaption("Select federation from list");
        federationName.setWidth("100%");
        federationName.addValidator(new Validator() {

            public void validate(Object value) throws InvalidValueException {
                if (!isValid(value)) {
                    throw new InvalidValueException("federation not exist");
                }
            }

            public boolean isValid(Object value) {
                if (value != null && federationsList.contains(value)) {
                    return true;
                }
                return false;
            }
        });
        
        for (String fedName: federationsList) {
            federationName.addItem(fedName);
        }
        
        layout.addComponent(federationName);

        paintFooter(layout);
    }
}
