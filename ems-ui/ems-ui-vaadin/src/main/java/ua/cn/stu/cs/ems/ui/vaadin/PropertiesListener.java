package ua.cn.stu.cs.ems.ui.vaadin;

/**
 * @author n0weak
 */
public interface PropertiesListener<T> {

    void propertiesSet(T props);
}
