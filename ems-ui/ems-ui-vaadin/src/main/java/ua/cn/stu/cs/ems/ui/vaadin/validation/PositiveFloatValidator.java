/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.cn.stu.cs.ems.ui.vaadin.validation;

import com.vaadin.data.Validator;
import com.vaadin.ui.DateField.UnparsableDateString;

/**
 * Allow to validate positive float value (allow values &gt=0).
 * @author AlexanderBartash@mail.ru
 * @version 1.0
 */
public class PositiveFloatValidator implements Validator {

    public void validate(Object value) throws InvalidValueException {
        if (null == value) {
            throw new EmptyValueException("Value can't be empty!");
        }
        float fvalue = 0;
        try {
            fvalue = Float.parseFloat(value.toString());
        } catch (NumberFormatException ex) {
            throw new UnparsableDateString("Invalid number format!");
        }
        if (0 > fvalue) {
            throw new InvalidValueException("Value can't be less then zero!");
        }
    }

    public boolean isValid(Object value) {
        if (null == value) {
            return false;
        }
        float fvalue = 0;
        try {
            fvalue = Float.parseFloat(value.toString());
        } catch (NumberFormatException ex) {
            return false;
        }
        return !(0 > fvalue);
    }
}
