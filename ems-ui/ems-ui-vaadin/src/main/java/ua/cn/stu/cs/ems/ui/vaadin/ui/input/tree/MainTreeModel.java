package ua.cn.stu.cs.ems.ui.vaadin.ui.input.tree;

/**
 *
 * @author slava
 */
import com.vaadin.data.Container;
import com.vaadin.data.util.IndexedContainer;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import javax.swing.tree.TreePath;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class MainTreeModel extends IndexedContainer implements Container.Hierarchical {
    
    private MapNode root;
    MapNode parent;
    Boolean setRes, stopRecursion;
    final static Logger logger = LoggerFactory.getLogger(MainTreeModel.class);
	
    public MainTreeModel(Object data){
        super();
        MapNode newRoot = new MapNode(data);
	this.root = newRoot;
    }

    public MapNode getRoot() {
        return root;
    }

    public Object getChild(Object parent, int index) {
        MapNode node = (MapNode)parent;
        return node.getChildAt(index);
    }

    public int getChildCount(Object parent) {
        MapNode node = (MapNode)parent;
        return node.getChildCount();
    }
    public boolean isLeaf(Object node) {
        return ((MapNode)node).isLeaf();
    }
    public void valueForPathChanged(TreePath path, Object newValue) {
        System.out.println(newValue.toString());
    }

    public int getIndexOfChild(Object parent, Object child) {
        MapNode node = (MapNode)parent;
        return node.getIndexOfChild((MapNode) child);
    }

    @Override
    public Collection<?> getChildren(Object itemId) {
       LinkedList<Object> c = new LinkedList<Object>();
        for (int i = 0; i < getChildCount(itemId); i++) {
            c.add(getChild(itemId, i));
        }          
       return Collections.unmodifiableCollection(c);          
    }

    
    @Override
    public Object getParent(Object itemId) {
        return findParent(itemId);
    }
    
    @Override
    public Collection<?> rootItemIds() {
       LinkedList<Object> c = new LinkedList<Object>();
            c.add(root);
       return Collections.unmodifiableCollection(c);    
    }
    
    @Override
    public boolean setParent(Object itemId, Object newParentId) {
         try {  
               MapNode oldParent =  findParent(itemId);
               oldParent.cutNode(itemId);
               ((MapNode)newParentId).addSonToFirst(itemId);    
               return true;
         } catch (Exception e) {
               return false;
         }
     }

    @Override
    public boolean areChildrenAllowed(Object itemId) {
       return (getChildCount(itemId) != 0);
    }

    @Override
    public boolean setChildrenAllowed(Object itemId, boolean areChildrenAllowed) {
       if (!containsId(itemId)) {
            return false;
        }
       return true;
    }

    @Override
    public boolean isRoot(Object itemId) {
         return itemId.equals(root);
    }

    @Override
    public boolean hasChildren(Object itemId) {
        return ((MapNode)itemId).isLeaf();
    }

        
    public MapNode forwardTraverseFind(MapNode abstractNode, Object key,
                boolean activeFind) {
        if (abstractNode != null) {
                if (!activeFind) {
                        System.out.println(abstractNode.toString());
                        int sonNum = 0;
                        int count = abstractNode.getChildCount();
                        for (sonNum = 0; sonNum < count; sonNum++) {
                                this.forwardTraverseFind(abstractNode.getChildAt(sonNum), key,
                                                activeFind);
                        }
                        return null;
                }
                if ((activeFind) && (abstractNode.toString().compareTo(key.toString()) == 0)) {
                        stopRecursion = true;
                        return abstractNode;
                }
                int sonNum = 0;
                int count = abstractNode.getChildCount();
                MapNode res = null;
                for (sonNum = 0; sonNum < count; sonNum++) {
                        if (!stopRecursion) {
                                res = this.forwardTraverseFind(abstractNode.getChildAt(sonNum),
                                                key, activeFind);
                        } else {
                                break;
                        }
                }
                if ((stopRecursion) && (setRes)) {
                        parent = abstractNode;
                        setRes = false;
                        return res;
                } else {
                        return null;
                }
        }
        return null;
    }

    public MapNode findParent(Object key) {
            parent = null;
            setRes = true;
            stopRecursion = false;
            this.forwardTraverseFind(this.root, key, true);
            return parent;
    }

    public void delNode(Object del) {
            this.findParent(del).delSon(del);
    }

    public MapNode cutNode(Object cut) {
            return this.findParent(cut).cutNode(cut);
    }    
}
