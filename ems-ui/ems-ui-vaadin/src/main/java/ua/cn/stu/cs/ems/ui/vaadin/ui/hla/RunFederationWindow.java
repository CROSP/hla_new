package ua.cn.stu.cs.ems.ui.vaadin.ui.hla;

import com.vaadin.data.Validator;
import com.vaadin.data.Validator.InvalidValueException;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.VerticalLayout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.cn.stu.cs.ems.ui.vaadin.ui.AbstractSettingsWindow;

/**
 *
 * @author slava
 */
public class RunFederationWindow extends AbstractSettingsWindow {

    final static Logger logger = LoggerFactory.getLogger(ActionsFederationWindow.class);
    
    private ComboBox federationName;
    private final HlaDispatcher dispatcher;

    public RunFederationWindow(String caption, String width, HlaDispatcher dispatcher) {
        super(caption);
        setWidth(width);
        this.dispatcher = dispatcher;
        paint();
    }

    @Override
    protected boolean apply() {
        if (federationName.isValid()) {
            //dispatcher.start(federationName.getValue().toString());
            return true;
        }
        return false;
    }

    protected void paint() {
        VerticalLayout layout = (VerticalLayout) getContent();
        layout.setSizeFull();
        
        federationName = new ComboBox();
        federationName.setCaption("Federation name");
        federationName.setWidth("100%");
        federationName.setRequired(true);
        
        for (String name: dispatcher.listFederations()) {
            federationName.addItem(name);
        }
        
        federationName.addValidator(new Validator() {

            public void validate(Object value) throws InvalidValueException {
                if (!isValid(value)) {
                    if (value == null || value.toString().isEmpty()) {
                        throw new InvalidValueException("No name selected");
                    }
                }
            }

            public boolean isValid(Object value) {
                if (value != null && dispatcher.federationExist(value.toString())) {
                    return true;
                }
                return false;
            }
        });
        layout.addComponent(federationName);
        layout.setExpandRatio(federationName, 1.0f);
        
        paintFooter(layout);
    }
    
    @Override
    protected String getCommitButtonName() {
        return "Run";
    }
}
