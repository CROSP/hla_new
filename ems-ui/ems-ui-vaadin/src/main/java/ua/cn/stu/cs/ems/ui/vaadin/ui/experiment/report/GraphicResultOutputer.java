package ua.cn.stu.cs.ems.ui.vaadin.ui.experiment.report;

import com.vaadin.ui.Component;

/**
 *
 * @author proger
 */
public interface GraphicResultOutputer {

    Component createOutput(GraphicResult graphicResult);

    Component createOutput(HistoResult graphicResult);
}
