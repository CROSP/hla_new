package ua.cn.stu.cs.ems.ui.vaadin;

import ua.cn.stu.cs.ems.core.experiment.ExperimentReport;

/**
 * @author n0weak
 */
public interface ExperimentManager {

    void start();
    
    void debug();

    void stop();
    
    void stopDebug();
    

    /**
     * @return number of model runs needed to evaluate all factors
     */
    long getFactorRuns();

    /**
     * @return number of runs specified be user.
     */
    int getExperimentRuns();

    double getModelingTime();

    void setExperimentListener(Dispatcher.ExperimentProgressListener experimentListener);
    
    ExperimentReport getExperimentReport();

     //ToDo Dummy
//    ExperimentSettings.ExperimentType getExperimentType();

    Double[] getStatisticsDumpTimes();
    
    /**
     * Return true if primary statistisc should be collected.
     * @return True if primary statistisc should be collected, otherwise {@code null}
     */
    boolean isPrimatyStatisticsEnabled();
    
    /**
     * Return true if secondary statistisc should be collected.
     * @return True if secondary statistisc should be collected, otherwise {@code null}
     */
    boolean isSecondaryStatisticsEnabled();
}
