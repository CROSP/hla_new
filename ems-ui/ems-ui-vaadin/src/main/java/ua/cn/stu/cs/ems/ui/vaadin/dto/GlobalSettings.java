package ua.cn.stu.cs.ems.ui.vaadin.dto;

import java.util.HashSet;
import java.util.Set;

/**
 * @author n0weak
 */
public class GlobalSettings {

    private AnimationSettings animationSettings = new AnimationSettings();
    private CanvasSettings canvasSettings = new CanvasSettings();
    private UISettings uiSettings = new UISettings();

    public UISettings getUISettings() {
        return uiSettings;
    }

    public CanvasSettings getCanvasSettings() {
        return canvasSettings;
    }

    public AnimationSettings getAnimationSettings() {
        return animationSettings;
    }

    public static class UISettings {

        private boolean animationSetupDialogShown = true;
        private boolean validateModelsOnSaving = false;

        public boolean isAnimationSetupDialogShown() {
            return animationSetupDialogShown;
        }

        public void setAnimationSetupDialogShown(boolean animationSetupDialogShown) {
            this.animationSetupDialogShown = animationSetupDialogShown;
        }
        
        public boolean isValidateModelsOnSaving() {
            return validateModelsOnSaving;
        }
        
        public void setValidateModelsOnSaving(boolean validateModelsOnSaving) {
            this.validateModelsOnSaving = validateModelsOnSaving;
        }
    }

    public static class AnimationSettings {

        private int animationDelay = 1000;
        private long animationTime = 100;

        public int getAnimationDelay() {
            return animationDelay;
        }

        public void setAnimationDelay(int animationDelay) {
            if (animationDelay > 0) {
                this.animationDelay = animationDelay;
            }
        }

        public long getAnimationTime() {
            return animationTime;
        }

        public void setAnimationTime(long animationTime) {
            if (animationTime > 0) {
                this.animationTime = animationTime;
            }
        }
    }

    public static class CanvasSettings {

        private boolean gridAdjustEnabled = true;
        private boolean gridPainted = true;
        private boolean pathCorrectionEnabled = false;
        private boolean horizontalLinesDrawnFirst = true;
        private boolean previewDrawn = false;
        private Set<PropertyChangeListener> listeners = new HashSet<PropertyChangeListener>();

        public boolean isGridAdjustEnabled() {
            return gridAdjustEnabled;
        }

        public void setGridAdjustEnabled(boolean gridAdjustEnabled) {
            if (this.gridAdjustEnabled != gridAdjustEnabled) {
                fireEvent("gridAdjustEnabled", gridAdjustEnabled);
            }
            this.gridAdjustEnabled = gridAdjustEnabled;
        }

        public boolean isGridPainted() {
            return gridPainted;
        }

        public void setGridPainted(boolean gridPainted) {
            if (this.gridPainted != gridPainted) {
                fireEvent("gridPainted", gridPainted);
            }
            this.gridPainted = gridPainted;
        }

        public boolean isPathCorrectionEnabled() {
            return pathCorrectionEnabled;
        }

        public void setPathCorrectionEnabled(boolean pathCorrectionEnabled) {
            if (this.pathCorrectionEnabled != pathCorrectionEnabled) {
                fireEvent("pathCorrectionEnabled", pathCorrectionEnabled);
            }
            this.pathCorrectionEnabled = pathCorrectionEnabled;
        }

        public boolean isHorizontalLinesDrawnFirst() {
            return horizontalLinesDrawnFirst;
        }

        public boolean isPreviewDrawn() {
            return previewDrawn;
        }

        public void setPreviewDrawn(boolean previewDrawn) {
            if (previewDrawn != this.previewDrawn) {
                fireEvent("previewDrawn", previewDrawn);
            }
            this.previewDrawn = previewDrawn;
        }

        public void setHorizontalLinesDrawnFirst(boolean horizontalLinesDrawnFirst) {
            if (horizontalLinesDrawnFirst != this.horizontalLinesDrawnFirst) {
                fireEvent("horizontalLinesDrawnFirst", horizontalLinesDrawnFirst);
            }
            this.horizontalLinesDrawnFirst = horizontalLinesDrawnFirst;
        }

        public void addListener(PropertyChangeListener listener) {
            listeners.add(listener);
        }

        private void fireEvent(String propertyName, Object value) {
            for (PropertyChangeListener listener : listeners) {
                listener.propertyChanged(propertyName, value);
            }
        }
    }

    public interface PropertyChangeListener {

        void propertyChanged(String propertyName, Object value);
    }
}
