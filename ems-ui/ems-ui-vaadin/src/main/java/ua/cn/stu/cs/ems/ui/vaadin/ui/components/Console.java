package ua.cn.stu.cs.ems.ui.vaadin.ui.components;

import com.vaadin.data.Item;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property;
import com.vaadin.event.MouseEvents.ClickEvent;
import com.vaadin.event.MouseEvents.ClickListener;
import com.vaadin.terminal.Resource;
import com.vaadin.terminal.gwt.client.ApplicationConfiguration;
import com.vaadin.terminal.gwt.client.ApplicationConnection;
import com.vaadin.terminal.gwt.client.Paintable;
import com.vaadin.terminal.gwt.client.UIDL;
import com.vaadin.terminal.gwt.client.ValueMap;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.ListSelect;
import com.vaadin.ui.Panel;
import java.util.Set;
import ua.cn.stu.cs.ems.ui.vaadin.WidgetManager;
import ua.cn.stu.cs.ems.ui.vaadin.ui.components.MainContent.Position;

import java.util.Collection;
import java.util.LinkedList;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaadin.peter.contextmenu.ContextMenu;
import org.vaadin.peter.contextmenu.ContextMenu.ContextMenuItem;
import ua.cn.stu.cs.ems.ui.vaadin.utils.UIUtils;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.Clipboard;

// TODO scrolling in Chrome
public class Console extends Panel implements RegisteredComponent,
        com.vaadin.terminal.gwt.client.Console, ClickListener {

    public static final String CID = "console";
    private static final String caption = "Console";
    private WidgetManager wm;
    private ListSelect list;
    private Logger logger = LoggerFactory.getLogger(Console.class);
    final Clipboard cp = new Clipboard();
    private static final String COPY_LINE = "Copy line";
    private static final String COPY_ALL = "Copy all";
    private static final String CLEAR_ALL = "Clear";
    private static final String ONLY_LOG_MESSAGES = "Only log messages";
    private static final String ONLY_ERROR_MESSAGES = "Only error messages";
    private static final String ALL_MESSAGES = "All messages";
    public static final int DEFAULT_ICON_HEIGHT = 13;
    public static final int DEFAULT_ICON_WIDTH = 14;
    public static final String ICONS_PATH = "icons/console/";
    private LinkedList<ConsoleMessage> messages = new LinkedList<ConsoleMessage>();
    private HorizontalLayout layout = new HorizontalLayout();
    private int countOfShowMessages = 0;
    private SelectItemListener listListener;

    public Console() {
        super();
        addListener((ClickListener) this);

        list = new ListSelect();
        listListener = new SelectItemListener(list);
        list.addListener(listListener);

        addStyleName("hide_scroll_x");
        layout.addComponent(list);
        layout.setExpandRatio(list, 1.0f);
        layout.setMargin(false);
        setScrollable(true);
        layout.setSpacing(false);
        
        setContent(layout);
    }

    public void init(WidgetManager wm) {
        this.wm = wm;
    }

    public void registerMenu() {
    }

    public String getTabCaption() {
        return caption;
    }

    public Position getDefaultPosition() {
        return Position.BOTTOM;
    }

    private void println(String msg) {
        if (msg.compareTo("") != 0) {
            println(msg, Mode.LOG);
        }
    }

    private void println(String msg, Mode mode) {
        if ((msg.compareTo("") != 0) && (msg.compareTo("\n") != 0)) {
            ConsoleMessage newmsg = new ConsoleMessage(messages.size(), msg, mode);
            messages.add(newmsg.getMessageID(), newmsg);
            countOfShowMessages++;
            addNewMessageToScreen(newmsg);
        }
    }

    private void addNewMessageToScreen(ConsoleMessage msg) {
        Item item = list.addItem(messageIDConvert(msg.getMessageID()) + " : " + msg.getMessage());
        // TODO display icon
        list.setItemIcon(item, msg.getMessageType().getIcon());
        requestRepaintAll();
    }

    private String messageIDConvert(int msg) {
        if (msg < 10) {
            return "00" + Integer.toString(msg);
        }
        if ((msg >= 10) && (msg < 100)) {
            return "0" + Integer.toString(msg);
        }
        if ((msg >= 100) && (msg < 1000)) {
            return Integer.toString(msg);
        }
        return "000";
    }

    private void clearMessagesOnScreen() {
        countOfShowMessages = 0;
        list.removeAllItems();
        update();
        requestRepaintAll();
    }

    private void clearMessages() {
        clearMessagesOnScreen();
        messages.clear();
    }

    private String printThrowable(Throwable t) {
        StringBuilder buf = new StringBuilder("");
        for (int i = 0; i < t.getStackTrace().length; i++) {
            buf.append("\n\t").append(t.getStackTrace()[i].getClassName()).
                    append("(").append(t.getStackTrace()[i].getFileName()).
                    append(":").append(Integer.toString(t.getStackTrace()[i].getLineNumber())).
                    append(")  Method name: ").append(t.getStackTrace()[i].getMethodName());
        }
        return buf.toString();
    }

    public void log(String msg) {
        println(msg + "\n", Mode.LOG);
    }

    public void log(Throwable e) {
        println(e.toString() + printThrowable(e), Mode.LOG);
    }

    public void error(Throwable e) {
        println(e.toString() + printThrowable(e), Mode.ERROR);
    }

    public void error(String msg) {
        println(msg, Mode.ERROR);
    }

    public void printObject(Object msg) {
        println(ReflectionToStringBuilder.toString(msg, ToStringStyle.SHORT_PREFIX_STYLE), Mode.LOG);

    }

    public void dirUIDL(UIDL u, ApplicationConfiguration cnf) {
//        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void printLayoutProblems(ValueMap meta, ApplicationConnection applicationConnection, Set<Paintable> zeroHeightComponents, Set<Paintable> zeroWidthComponents) {
//        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void clear() {
        clearMessages();
    }

    public String getCid() {
        return CID;
    }
    
    // TODO correct list size detection in chrome
    private void update() {
        list.setWidth(getWidth() + "px");
        layout.setWidth(getWidth() + "px");

        if (list.getHeight() < getHeight()-15) {
            int height = (int)getHeight();
            if (height > 0) {
                list.setHeight(height-5 + "px");
                layout.setHeight(height-5 + "px");
            }
        }
        else 
        {
            list.setHeight(((list.getItemIds().size()+1)*15+5) + "px");
            layout.setHeight(((list.getItemIds().size()+1)*15+5) + "px");
        }   

        setScrollTop((list.getItemIds().size()+1)*15+5);
    }
    
    @Override
    public void requestRepaintAll() {
        super.requestRepaintAll();
        update();
    }
    
    private class SelectItemListener implements Property.ValueChangeListener {

        private final ListSelect list;

        private SelectItemListener(ListSelect list) {
            this.list = list;
        }

        private String getSelectedItems() {
            return (String) list.getValue();
        }

        public void valueChange(ValueChangeEvent event) {
            
        }
    }

    public void click(ClickEvent event) {
        if (event.getButton() == ClickEvent.BUTTON_RIGHT) {

            getApplication().getMainWindow().addComponent(cp);

            ContextMenu menu = new ContextMenu();
            menu.addListener(new ContextMenu.ClickListener() {

                public void contextItemClick(ContextMenu.ClickEvent e) {

                    if (e.getClickedItem().getName().compareTo(COPY_LINE) == 0) {
                        cp.setClipboardText(listListener.getSelectedItems());
                        return;
                    }

                    if (e.getClickedItem().getName().compareTo(COPY_ALL) == 0) {
                        Collection<String> items = (Collection<String>) list.getItemIds();
                        StringBuilder clipboard = new StringBuilder("");
                        for (String id : items) {
                            clipboard.append(id);
                        }
                        logger.debug(clipboard.toString());
                        cp.setClipboardText(clipboard.toString());
                        return;
                    }

                    if (e.getClickedItem().getName().compareTo(CLEAR_ALL) == 0) {
                        clearMessages();
                        return;
                    }

                    if (e.getClickedItem().getName().compareTo(ONLY_LOG_MESSAGES) == 0) {
                        clearMessagesOnScreen();
                        for (int i = 0; i < messages.size(); i++) {
                            if (messages.get(i).messageType == Mode.LOG) {
                                countOfShowMessages++;
                                addNewMessageToScreen(messages.get(i));
                            }
                        }
                    }

                    if (e.getClickedItem().getName().compareTo(ONLY_ERROR_MESSAGES) == 0) {
                        clearMessagesOnScreen();
                        for (int i = 0; i < messages.size(); i++) {
                            if (messages.get(i).messageType == Mode.ERROR) {
                                countOfShowMessages++;
                                addNewMessageToScreen(messages.get(i));
                            }
                        }

                    }

                    if (e.getClickedItem().getName().compareTo(ALL_MESSAGES) == 0) {
                        clearMessagesOnScreen();
                        for (int i = 0; i < messages.size(); i++) {
                            countOfShowMessages++;
                            addNewMessageToScreen(messages.get(i));
                        }

                    }
                }
            });

            ContextMenuItem copyLine = menu.addItem(COPY_LINE);
            copyLine.setIcon(UIUtils.getThemeResource(ICONS_PATH + "copy.png"));


            ContextMenuItem copyAll = menu.addItem(COPY_ALL);
            copyAll.setIcon(UIUtils.getThemeResource(ICONS_PATH + "copy.png"));

            ContextMenuItem clear = menu.addItem(CLEAR_ALL);
            clear.setIcon(UIUtils.getThemeResource(ICONS_PATH + "clear.png"));

            ContextMenuItem logMessages = menu.addItem(ONLY_LOG_MESSAGES);
            logMessages.setIcon(UIUtils.getThemeResource(ICONS_PATH + "context_log.png"));

            ContextMenuItem errorMessages = menu.addItem(ONLY_ERROR_MESSAGES);
            errorMessages.setIcon(UIUtils.getThemeResource(ICONS_PATH + "context_error.png"));

            ContextMenuItem allMessages = menu.addItem(ALL_MESSAGES);
            allMessages.setIcon(UIUtils.getThemeResource(ICONS_PATH + "context_show_all.png"));



            this.addComponent(menu);

            menu.show(event.getClientX(), event.getClientY());
        }
    }

    private static enum Mode {

        LOG("log.png"), ERROR("error.png");
        
        private Resource icon;
        
        private Mode(String icon) {
            this.icon = UIUtils.getThemeResource(ICONS_PATH + icon);
        }
        
        public Resource getIcon() {
            return icon;
        }
    }

    private class ConsoleMessage {

        private int messageID = 0;
        private String message = "";
        private Mode messageType;

        public ConsoleMessage(int id, String msg, Mode type) {
            this.messageID = id;
            this.message = msg;
            this.messageType = type;
        }

        public int getMessageID() {
            return this.messageID;
        }

        public void setMessageID(int id) {
            this.messageID = id;
        }

        public String getMessage() {
            return this.message;
        }

        public void setMessage(String msg) {
            this.message = msg;
        }

        public Mode getMessageType() {
            return this.messageType;
        }

        public void setMessageType(Mode type) {
            this.messageType = type;
        }
    }
}