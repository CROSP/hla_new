package ua.cn.stu.cs.ems.ui.vaadin.ui.input.tree;

import com.vaadin.data.util.HierarchicalContainer;
import com.vaadin.ui.Tree;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.EntityType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author leonid
 */
public class ModelTree extends Tree {

    final static Logger logger = LoggerFactory.getLogger(ModelTree.class);
    // ID => {Node => Childs}
    private HashMap<String, HashMap<String, Collection<String>>> cache = 
            new HashMap<String, HashMap<String, Collection<String>>>();
    
    private static final String DEFAULT_ROOT = "Model";
    public static final String TRANSITIONS = "Transitions";
    public static final String QUEUES = "Queues";
    public static final String PLACES = "Places";
    public static final String IO = "IO";
    public static final String VARIABLES = "Variables";
    public static final String AGGREGATES = "Aggregates";
    private HierarchicalContainer container;
    
    private String root;
    
    public ModelTree() {
        addItem(DEFAULT_ROOT);
        this.root = DEFAULT_ROOT;
        init();
    }
    
    public ModelTree(String root) {
        this.root = root;
        container = new HierarchicalContainer();
        setContainerDataSource(container);
        addItem(root);
        init();
    }
    
    public String getRoot() {
        return root;
    }
    
    public void init() {
        addItem(root);
        addItem(PLACES);
        setParent(PLACES, this.root);
        addItem(IO);
        setParent(IO, PLACES);
        addItem(TRANSITIONS);
        setParent(TRANSITIONS, this.root);
        addItem(QUEUES);
        setParent(QUEUES, this.root);
        addItem(VARIABLES);
        setParent(VARIABLES, this.root);
        addItem(AGGREGATES);
        setParent(AGGREGATES, this.root);
        expandItem(root);
    }
    
    public void register(String id, EntityType entityType) {
        addItem(id);
        setChildrenAllowed(id, false);
        if (EntityType.FIFO == entityType
            || EntityType.PFIFO == entityType
            || EntityType.LIFO == entityType
            || EntityType.PLIFO == entityType) {
            setParent(id, QUEUES);
        }
        else if (EntityType.FTRANSITION == entityType
                 || EntityType.JTRANSITION == entityType
                 || EntityType.TTRANSITION == entityType
                 || EntityType.XTRANSITION == entityType
                 || EntityType.YTRANSITION == entityType) {
            setParent(id, TRANSITIONS);
        }
        else if (EntityType.PLACE == entityType) {
            setParent(id, PLACES);
        }
        else if (EntityType.INPUT == entityType
                 || EntityType.OUTPUT == entityType) {
            setParent(id, IO);
        }
        else if (EntityType.AGGREGATE == entityType) {
            setParent(id, AGGREGATES);
        }
    }
    
    public void renameItem(String oldName, String newName) {
        if (getItem(newName) != null || oldName == null || getItem(oldName) == null) {
            return; // throw error = cant be 2 identical nodeNames
        }
        
        if (isRoot(oldName)) {
            this.root = newName;
        }
        
        Object parent = getParent(oldName);
        addItem(newName);
        if (isRoot(newName)) {
            expandItem(newName);
        }
        setChildrenAllowed(newName, areChildrenAllowed(oldName));
        
        Collection t = getChildren(oldName);
        Collection nodeTree = new ArrayList(t != null ? t : new ArrayList());
        if (nodeTree != null) {
            for (Object nodeName: nodeTree) {
                setParent(nodeName, newName);
            }
        }
        
        if (parent != null) {
            setParent(newName, parent);
            container.moveAfterSibling(newName, oldName);
        }
        removeItem(oldName);
    }
    
    // Both elements must be with same parent
    public void moveElement(String element, String after) {
        container.moveAfterSibling(element, after);
    }
    
    public void clear() {
        removeAllItems();
        init();
    }
        
    public void addItem(String parent, String id) {
        addItem(id);
        if (parent != null) {
            setParent(id, parent);
        }
        setChildrenAllowed(id, false);
    }
    
    public void clearNode(String id) {
        Collection childs = getChildren(id);
        Collection c = new ArrayList(childs != null ? childs : new ArrayList());
        
        if (c != null) {
            for (Object child: c) {
                if (!IO.equals(child)) {
                    removeItem(child);
                }
            }
        }
    }
    
    public void clearNodes() {
        String[] spath = new String[] {PLACES, IO, TRANSITIONS, QUEUES, VARIABLES, AGGREGATES};
        for (String id: spath) {
            clearNode(id);
        }
    }
    
    public void removeEmptyNodes() {
        String[] searchPath = new String[] {
            TRANSITIONS, QUEUES, IO, VARIABLES, AGGREGATES
        };
        
        for (String s: searchPath) {
            Collection childs = getChildren(s);
            if (childs == null || childs.isEmpty()) {
                removeItem(s);
            }
        }
        Collection childs = getChildren(PLACES);
        if (childs == null || childs.isEmpty()) {
            removeItem(PLACES);
        }
    }
        
    /**
     * Save all values to cache
     * @param id
     * @param save 
     */
    public void saveInCache() {
        String id = getRoot();
        HashMap<String, Collection<String>> data = new HashMap<String, Collection<String>>();
        Collection childs = null;
        // saving places
        childs = getChildren(PLACES);
        if (childs != null) {
            LinkedList copy = new LinkedList(childs);
            copy.remove(IO);
            data.put(PLACES, copy);
        }
        
        // save all other
        String[] spath = new String[] {IO, QUEUES, TRANSITIONS, AGGREGATES, VARIABLES};
        for (String p: spath) {
            childs = getChildren(p);
            if (childs != null) {
                LinkedList copy = new LinkedList(childs);
                data.put(p, copy);
            }
        }
        cache.put(id, data);
    }
    
    public void restoreFromCache(String id) {
        renameItem(getRoot(), id);
        clearNodes();

        HashMap<String, Collection<String>> data = cache.remove(id);
        if (data != null) {
            
            Collection childs = null;
            // saving places
            childs = data.get(PLACES);
            if (childs != null) {
                for (Object el: childs) {
                    addItem(PLACES, el.toString());
                }
            }

            // save all other
            String[] spath = new String[] {IO, QUEUES, TRANSITIONS, AGGREGATES, VARIABLES};
            for (String p: spath) {
                childs = data.get(p);
                if (childs != null) {
                    for (Object o: childs) {
                        addItem(p, o.toString());
                    }
                }
            }
        }
        else {
            init();
        }
    }
    
    public void clearCache(String id) {
        if (id != null) {
            cache.remove(id);
        }
        else {
            cache.clear();
        }
    }
}