package ua.cn.stu.cs.ems.ui.vaadin;

import java.util.Collection;
import ua.cn.stu.cs.ems.ui.vaadin.ui.OpenDialog;
import ua.cn.stu.cs.ems.ui.vaadin.ui.SaveDialog;

/**
 * @author n0weak
 */
public interface UIManager {

    void showConfirmDialog(String width, String msg, PropertiesListener<Boolean> listener);

    void showSaveDialog(String defaultName, FileManager.FileType fileType, 
            PropertiesListener<SaveDialog.SaveRecord> propertiesListener);

    void showOpenDialog(Collection<String> modelsFiles, 
            Collection<String> aggTypesFiles, 
            PropertiesListener<OpenDialog.OpenRecord> propertiesListener);

    void clear();
}
