package ua.cn.stu.cs.ems.ui.vaadin.validation;

import com.vaadin.data.Validator;
import com.vaadin.data.validator.DoubleValidator;
import com.vaadin.ui.AbstractField;
import com.vaadin.ui.Field;

/**
 * @author n0weak
 */
public class ValidatorFactory {

    private static final String REQUIRED_MSG = "Value cannot be empty!";
    private final static Validator doubleValidator = new DoubleValidator("Only double values are accepted.");
    private final static Validator emptyStringValidator = new EmptyStringValidator();
    private final static Validator positiveDoubleValidator = new PositiveDoubleValidator(
            "Only positive double values are accepted.");
    private final static Validator positiveIntegerValidator = new PositiveIntegerValidator();

    public static Validator getPositiveIntegerValidator() {
        return positiveIntegerValidator;
    }

    public static Validator getPositiveDoubleValidator() {
        return positiveDoubleValidator;
    }

    public static Validator getDoubleValidator() {
        return doubleValidator;
    }

    public static Validator getEmptyStringValidator() {
        return emptyStringValidator;
    }

    public static Validator createDoubleValidator(double from, double to) {
        return new BoundedDoubleValidator(from, to);
    }

    public static void addRequiredValidator(Field f) {
        if (null != f && (null == f.getValidators() || !f.getValidators().contains(emptyStringValidator))) {
            f.setRequiredError(REQUIRED_MSG);
            f.setRequired(true);
            f.addValidator(emptyStringValidator);
        }
    }

    public static void addRequiredValidator(AbstractField tf) {
        addRequiredValidator((Field) tf);
    }
}
