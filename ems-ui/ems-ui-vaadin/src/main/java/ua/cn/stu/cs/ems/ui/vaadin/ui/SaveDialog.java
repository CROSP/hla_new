package ua.cn.stu.cs.ems.ui.vaadin.ui;

import com.vaadin.data.Validator;
import com.vaadin.data.Validator.InvalidValueException;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.ui.*;
import ua.cn.stu.cs.ems.ui.vaadin.FileManager.FileType;
import ua.cn.stu.cs.ems.ui.vaadin.PropertiesListener;
import ua.cn.stu.cs.ems.ui.vaadin.validation.EmptyStringValidator;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import ua.cn.stu.cs.ems.ui.vaadin.Dispatcher;
import ua.cn.stu.cs.ems.ui.vaadin.FileManager;

/**
 * @author n0weak
 */
public class SaveDialog extends AbstractSettingsWindow {

    private static final String CAPTION = "Save as";
    private static final String WIDTH = "500px";
    private final PropertiesListener<SaveRecord> propertiesListener;
    private final String defaultName;
    private final Collection<String> files;
    private final FileManager.FileType fileType;
    private TextField fileNameTF;
    
    private static final List<String> unwantedNames = new LinkedList<String>() {{
        add(Dispatcher.MODEL_NAME);
    }};

    public SaveDialog(Collection<String> files, FileManager.FileType fileType, 
            String defaultName, final PropertiesListener<SaveRecord> propertiesListener) {
        super(CAPTION, WIDTH);
        this.propertiesListener = propertiesListener;
        this.defaultName = defaultName;
        this.files = files;
        this.fileType = fileType;
        paint();
    }

    protected void paint() {
        VerticalLayout layout = (VerticalLayout) getContent();
        Panel p = new FilePanel(files.toArray(new String[files.size()]), fileType,
                new FilePanel.SelectionListener() {

                    public void fileSelected(String fileName, FileType mode) {
                        fileNameTF.setValue(fileName);
                    }
                });

        p.setWidth("100%");
        p.setHeight("300px");
        layout.addComponent(p);

        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.setSizeFull();
        horizontalLayout.setMargin(true, false, false, false);

        fileNameTF = new TextField();
        fileNameTF.setValue(defaultName);
        fileNameTF.addValidator(new EmptyStringValidator());
        fileNameTF.setWidth("400px");

        //This RegEx can be used to verify (esp. when receiving data from forms) file names. No Path is allowed. 
        //German Special Characters are allowed.
        fileNameTF.addValidator(new RegexpValidator(
                "^[\\w0-9&#228;&#196;&#246;&#214;&#252;&#220;&#223;\\-_]+$", "Invalid file name"));
        fileNameTF.addValidator(new Validator() {

            public void validate(Object value) throws InvalidValueException {
                if (!isValid(value)) {
                    throw new InvalidValueException("This value cant be used");
                }
            }

            public boolean isValid(Object value) {
                return !unwantedNames.contains(value);
            }
        });
//        fileNameTF.addValidator(new RegexpValidator(
//                "^[\\w0-9&#228;&#196;&#246;&#214;&#252;&#220;&#223;\\-_]+\\.[a-zA-Z0-9]{2,6}$", "Invalid file name"));

        fileNameTF.setImmediate(true);
        fileNameTF.setRequired(true);
        horizontalLayout.addComponent(fileNameTF);
        horizontalLayout.setComponentAlignment(fileNameTF, Alignment.BOTTOM_LEFT);

        Button b = createCommitButton();
        horizontalLayout.addComponent(b);
        horizontalLayout.setComponentAlignment(b, Alignment.BOTTOM_RIGHT);

        layout.addComponent(horizontalLayout);

    }

    @Override
    protected String getCommitButtonName() {
        return "Save";
    }

    @Override
    protected boolean apply() {
        if (fileNameTF.isValid()) {
            final String fileName = fileNameTF.getValue().toString();
            if (files.contains(fileName)) {
                getParent().addWindow(new ConfirmDialog("200px", "Overwrite existing file?",
                        new PropertiesListener<Boolean>() {

                            public void propertiesSet(Boolean props) {
                                if (props) {
                                    commit(fileName);
                                }
                            }
                        }));
            }
            else {
                commit(fileName);
            }
        }
        return false;
    }

    protected void commit(String fileName) {
        (SaveDialog.this.getParent()).removeWindow(SaveDialog.this);
        propertiesListener.propertiesSet(new SaveRecord(fileName, fileType));
    }
    
    public static class SaveRecord {
        private final String fileName;
        private final FileManager.FileType fileType;

        public SaveRecord(String fileName, FileType fileType) {
            this.fileName = fileName;
            this.fileType = fileType;
        }
        
        public String getFileName() {
            return fileName;
        }
        
        public FileManager.FileType getFileType() {
            return fileType;
        }
    }
}
