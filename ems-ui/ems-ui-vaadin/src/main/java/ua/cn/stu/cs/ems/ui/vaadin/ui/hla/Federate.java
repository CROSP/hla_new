package ua.cn.stu.cs.ems.ui.vaadin.ui.hla;

import java.net.InetAddress;

/**
 *
 * @author Dmitry Krivenko <dmitrykrivenko@gmal.com>
 */
public class Federate {

    private String federationName;
    private String federateName;
    private String rootAggregateName;
    private String aggregateName;
    private InetAddress serverAdress;

    /**
     * Default constructor without parameters.
     */
    public Federate() {
    }

    /**
     * Convenience constructor with parameters.
     *
     * @param federationName
     * @param federateName
     * @param rootAggregateName
     * @param aggregateName
     * @param serverAdress
     */
    public Federate(String federationName, String federateName, String rootAggregateName,
            String aggregateName, InetAddress serverAdress) {
        this.federationName = federationName;
        this.federateName = federateName;
        this.rootAggregateName = rootAggregateName;
        this.aggregateName = aggregateName;
        this.serverAdress = serverAdress;
    }

    /**
     * @return the federationName
     */
    public String getFederationName() {
        return federationName;
    }

    /**
     * @param federationName the federationName to set
     */
    public void setFederationName(String federationName) {
        this.federationName = federationName;
    }

    /**
     * @return the federateName
     */
    public String getFederateName() {
        return federateName;
    }

    /**
     * @param federateName the federateName to set
     */
    public void setFederateName(String federateName) {
        this.federateName = federateName;
    }

    /**
     * @return the rootAggregateName
     */
    public String getRootAggregateName() {
        return rootAggregateName;
    }

    /**
     * @param rootAggregateName the rootAggregateName to set
     */
    public void setRootAggregateName(String rootAggregateName) {
        this.rootAggregateName = rootAggregateName;
    }

    /**
     * @return the aggregateName
     */
    public String getAggregateName() {
        return aggregateName;
    }

    /**
     * @param aggregateName the aggregateName to set
     */
    public void setAggregateName(String aggregateName) {
        this.aggregateName = aggregateName;
    }

    /**
     * @return the serverAdress
     */
    public InetAddress getServerAdress() {
        return serverAdress;
    }

    /**
     * @param serverAdress the serverAdress to set
     */
    public void setServerAdress(InetAddress serverAdress) {
        this.serverAdress = serverAdress;
    }
}
