package ua.cn.stu.cs.ems.ui.vaadin.ui.hla;

import com.vaadin.data.Validator;
import com.vaadin.data.Validator.InvalidValueException;
import com.vaadin.ui.AbstractField;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.cn.stu.cs.ems.ui.vaadin.PropertiesListener;
import ua.cn.stu.cs.ems.ui.vaadin.ui.AbstractSettingsWindow;

/**
 *
 * @author slava
 */
public class ActionsFederationWindow extends AbstractSettingsWindow {

    final static Logger logger = LoggerFactory.getLogger(ActionsFederationWindow.class);
    
    private AbstractField federationName;
    private final PropertiesListener<String> listener;
    private List<String> variants = null;
    private final String actionName;

    public ActionsFederationWindow(String caption, String width, String actionName,
            PropertiesListener<String> listener) {
        super(caption);
        setWidth(width);
        this.listener = listener;
        this.actionName = actionName;
        paint();
    }
    
    public ActionsFederationWindow(String caption, String width, String actionName,
            List<String> variants, PropertiesListener<String> listener) {
        super(caption);
        setWidth(width);
        this.listener = listener;
        this.variants = variants;
        this.actionName = actionName;
        paint();
    }

    @Override
    protected boolean apply() {
        if (federationName.isValid()) {
            listener.propertiesSet((String) federationName.getValue());
            return true;
        }
        return false;
    }

    protected void paint() {
        VerticalLayout layout = (VerticalLayout) getContent();
        layout.setSizeFull();
        
        if (variants == null) {
            federationName = new TextField();
        }
        else {
            ComboBox list = new ComboBox();
            for (String name: variants) {
                list.addItem(name);
            }
            federationName = list;
        }
        federationName.setCaption("Federation name");
        federationName.setWidth("100%");
        federationName.setRequired(true);
        federationName.addValidator(new Validator() {

            public void validate(Object value) throws InvalidValueException {
                if (!isValid(value)) {
                    if (value == null || value.toString().isEmpty()) {
                        throw new InvalidValueException("No name given");
                    }
                    else {
                        throw new InvalidValueException("Feferation exist");
                    }
                }
            }

            public boolean isValid(Object value) {
                if (value != null && !value.toString().isEmpty()) {
                    return true;
                }
                return false;
            }
        });
        layout.addComponent(federationName);
        layout.setExpandRatio(federationName, 1.0f);
        
        paintFooter(layout);
    }
    
    @Override
    protected String getCommitButtonName() {
        return actionName;
    }
}
