package ua.cn.stu.cs.ems.ui.vaadin.ui.modules;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vaadin.addon.chameleon.ChameleonTheme;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Layout;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import ua.cn.stu.cs.ems.core.aggregates.Aggregate;
import ua.cn.stu.cs.ems.core.aggregates.AggregateDefinition;
import ua.cn.stu.cs.ems.core.utils.JessUtils;
import ua.cn.stu.cs.ems.ui.vaadin.Dispatcher;
import ua.cn.stu.cs.ems.ui.vaadin.WidgetManager;
import ua.cn.stu.cs.ems.ui.vaadin.ui.components.ActionHandler;
import ua.cn.stu.cs.ems.ui.vaadin.ui.components.MainContent.WorkspaceType;
import ua.cn.stu.cs.ems.ui.vaadin.utils.TemporaryDownloadResource;

/**
 *
 * @author leonid
 */
public class ExportVariables implements EmsModule {
    
    final static Logger logger = LoggerFactory.getLogger(ExportVariables.class);
    
    private final static String MENU_PATH = "File->Export->Variables";
    private final static String CAPTION = "Export variables";
    private static final String FOOTER_BUTTONS_WIDTH = "100px";
    private static final String DEFAULT_WIDTH = "500px";
    private static final String DEFAULT_HEIGHT = "400px";
    private WidgetManager wm;
    
    private Window activeWindow;
    private TextArea exportArea = new TextArea();
    private String fileName = "";
    
    public ExportVariables() {
        
    }

    public void init(WidgetManager wm) {
        this.wm = wm;
        addMenuHandlers();
    }
    
    
    private void addMenuHandlers() {
        wm.getDefaultMenu().setItem(MENU_PATH, new ActionHandler() {

            public void handle() {
                wm.getWindow().addWindow(createWindow());
            }
        });
    }
    
    private Collection<String> listAllVariables(AggregateDefinition root, Aggregate child) {
        Collection<String> list = new ArrayList<String>();
        
        if (null == child) {
            child = root;
        }

        String prefix = JessUtils.getAggregateFullName(root, child);
        for (String varName: child.getVariablesNames()) {
            String var = prefix + "." + varName;
            var = var.substring(var.indexOf(".")+1);
            list.add(var);
        }

        for (Aggregate a: child.getChildAggregates()) {
            list.addAll(listAllVariables(root, a));
        }
        
        return list;
    }
    
    private void export() {
        Dispatcher d = wm.getMainContent().getActiveWorkspace().getDispatcher();
        fileName = d.getModelName();
        StringBuilder sb = new StringBuilder();
        sb.append("Variables of " + 
                (wm.getMainContent().getActiveWorkspace().getWsType() == WorkspaceType.MODEL ? 
                "model" : " aggregate") + " '" + d.getModelName() + "'").append("\n");
        
        Collection<String> list = listAllVariables(d.getRootAggregate(), null);
        for (String var: list) {
            logger.debug("finding var: " + var);
            sb.append(var).append(" = " + d.getVariableValue(var)).append("\n");
        }
        exportArea.setValue(sb.toString());
    }
    
    private Window createWindow() {
        activeWindow = new Window(CAPTION);
        activeWindow.setModal(true);
        activeWindow.setResizable(false);
        activeWindow.setWidth(DEFAULT_WIDTH);
        activeWindow.setHeight(DEFAULT_HEIGHT);
        
        VerticalLayout vl = new VerticalLayout();
        vl.setSizeFull();
        vl.setMargin(true);
        activeWindow.setContent(vl);
        
        exportArea.setWidth("476px");
        exportArea.setHeight("320px");
        
        vl.addComponent(exportArea);
        vl.setExpandRatio(exportArea, 1.0f);
        Layout l = createFooter();
        l.setHeight("50px");
        vl.addComponent(l);
        
        export();
        
        return activeWindow;
    }
    
    private Layout createFooter() {
        HorizontalLayout hLayout = new HorizontalLayout();
        hLayout.setWidth("100%");
        hLayout.setMargin(true, false, false, false);
        Button btn = new Button("Save");
        btn.addListener(new Button.ClickListener() {

            public void buttonClick(ClickEvent event) {
//                WebApplicationContext context = 
//                        (WebApplicationContext) wm.getWindow().getApplication().getContext();
//                String path = context.getHttpSession().getServletContext().getContextPath();
                InputStream is = new ByteArrayInputStream(exportArea.getValue().toString().getBytes());
//                DownloadStream ds = new DownloadStream(is, "text/plain", fileName + ".txt");
//                ds.setParameter("Content-Disposition", "attachment; filename=" + fileName + ".txt");
                wm.getWindow().open(
                        new TemporaryDownloadResource(
                                wm.getWindow().getApplication(), 
                                fileName + ".txt", "text/plain", is), "_blank");
            }
        });
        
        btn.setStyleName(ChameleonTheme.BUTTON_SMALL);
        btn.setWidth(FOOTER_BUTTONS_WIDTH);
        hLayout.addComponent(btn);
        hLayout.setComponentAlignment(btn, Alignment.TOP_LEFT);

        
        btn = new Button("Close");
        
        btn.setStyleName(ChameleonTheme.BUTTON_SMALL);
        btn.setWidth(FOOTER_BUTTONS_WIDTH);
        btn.addListener(new Button.ClickListener() {

            public void buttonClick(ClickEvent event) {
                wm.getWindow().removeWindow(activeWindow);
            }
        });
        hLayout.addComponent(btn);
        hLayout.setComponentAlignment(btn, Alignment.TOP_RIGHT);

        return hLayout;
    }
}
