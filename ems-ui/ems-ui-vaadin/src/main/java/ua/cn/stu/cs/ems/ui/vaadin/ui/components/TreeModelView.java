package ua.cn.stu.cs.ems.ui.vaadin.ui.components;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vaadin.data.Item;
import java.util.List;
import java.util.Set;

import ua.cn.stu.cs.ems.ui.vaadin.WidgetManager;
import ua.cn.stu.cs.ems.ui.vaadin.dto.AggregateVariables;
import ua.cn.stu.cs.ems.ui.vaadin.dto.Variable;
import ua.cn.stu.cs.ems.ui.vaadin.ui.components.MainContent.Position;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.EntityType;

import com.vaadin.data.Property;
import com.vaadin.event.Action;
import com.vaadin.event.ShortcutAction;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Tree;
import java.util.HashSet;
import java.util.LinkedList;
import ua.cn.stu.cs.ems.ui.vaadin.ui.input.tree.ModelTree;

public class TreeModelView extends Panel implements RegisteredComponent {

    final static Logger logger = LoggerFactory.getLogger(TreeModelView.class);
    
    public static final String CID = "tree_model_view";
    private static final String caption = "Model components";
    private static final String DOT = "∙ ";
    private String modelName = "Model";
    private ModelTree tree;
    private WidgetManager wm;
    private List<String> workspaces = new LinkedList<String>();

    public TreeModelView() {
        setSizeFull();
        setImmediate(true);
        setStyleName("TreeModelView_style");
    }
    
    public String getTabCaption() {
        return caption;
    }

    public void init(WidgetManager wm) {
        this.wm = wm;
        tree = new ModelTree(modelName);
        tree.setMultiSelect(true);
        tree.setImmediate(true);
        TreeHandler treeHandler = new TreeHandler(tree);
        tree.addListener(treeHandler);
        
        CssLayout layout = new CssLayout();
        layout.addComponent(tree);
        setContent(layout);
        
        tree.addActionHandler(treeHandler);
    }
    
    public void createWorkspace(String name) {
        if (!workspaces.isEmpty())
            tree.saveInCache();
        tree.clearNodes();
        tree.renameItem(tree.getRoot(), name);
        workspaces.add(name);
    }
    
    public void removeWorkspace(String name, String restore) {
        if (workspaces.isEmpty()) {
            return;
        }
        if (getRoot().equals(name)) {
            if (restore != null) {
                tree.restoreFromCache(restore);
            }
        }
        else {
            tree.clearCache(name);
        }
        workspaces.remove(name);
    }
    
    public void switchWorkspace(String name) {
        tree.saveInCache();
        tree.restoreFromCache(name);
    }
    
    public void renameRoot(String name) {
        tree.renameItem(tree.getRoot(), name);
    }

    // TODO different settings in menu
    public void registerMenu() {
    }

    /**
     * Initializes the object tree and expand them. Creates a root with the
     * model name, and leaves to group items into categories.
     */

    /**
     * Addes new item to object tree.
     *
     * @param id         New item.
     * @param entityType Type of new entity. Used for group items into categories.
     */
    public void register(String id, EntityType entityType) {
        tree.register(id, entityType);
    }

    public void removeItem(String id) {
        tree.removeItem(id);
        tree.removeItem(DOT + id);
    }

    public void removeAllItems() {
        tree.clear();
    }
    
    public void addItem(String parent, String id) {
        tree.addItem(parent, id);
    }
    
    public String getRoot() {
        return tree.getRoot();
    }
    
    /**
     * Rename item in the tree (only in last level).
     * 
     * @param oldName
     * @param newName
     */
    public void renameItem(String oldName, String newName) {
        tree.renameItem(oldName, newName);
        tree.renameItem(DOT + oldName, DOT + newName);
    }
    
    public void pointPlace(String id, boolean point) {
        Item item ;
        if (point) {
            item = tree.getItem(id);
        }
        else {
            item = tree.getItem(DOT + id);
        }
        
        if (item == null) {
            return;
        }
        else {
            String oldname = point ? id : DOT + id;
            String newName = point ? DOT + id : id;
            tree.renameItem(oldname, newName);
            tree.setChildrenAllowed(newName, false);
        }
    }

    public String getCid() {
        return CID;
    }

    private class TreeHandler implements Property.ValueChangeListener, Action.Handler {

        private final Tree tree;
        private final Action deleteAction = new ShortcutAction("Delete", ShortcutAction.KeyCode.DELETE, null);
        private final Action variablesAction = new ShortcutAction("Edit variables");
        private final Action functionsAction = new ShortcutAction("Functions editor");

        private TreeHandler(Tree tree) {
            this.tree = tree;
        }

        public Action[] getActions(Object o, Object o1) {
            Object parent = tree.getParent(o);
            List<Action> actions = new LinkedList<Action>();
            if (ModelTree.PLACES.equals(parent)) {
                actions.add(deleteAction);
                actions.add(variablesAction);
            }
            else if (ModelTree.TRANSITIONS.equals(parent)) {
                actions.add(deleteAction);
                actions.add(functionsAction);
            }
            else if (!ModelTree.VARIABLES.equals(parent)) {
                actions.add(deleteAction);
            }
            return actions.toArray(new Action[actions.size()]);
        }

        public void handleAction(Action action, Object o, Object o1) {
            MainContent.WsRec ws = wm.getMainContent().getActiveWorkspace();
            if (ws == null) {
                return;
            }
            if (deleteAction == action) {
                ws.getJessCanvas().delete(getSelectedItems());
            }
            else if (variablesAction == action || functionsAction == action) {
                String[] sel = getSelectedItems();
                if (sel != null && sel.length > 0) {
                    String id = sel[0];
                    ws.getDispatcher().propertiesRequested(id);
                }
            }
        }

        public void valueChange(Property.ValueChangeEvent event) {
            MainContent.WsRec ws = wm.getMainContent().getActiveWorkspace();
            if (ws != null) {
                ws.getJessCanvas().select(getSelectedItems());
            }
        }

        @SuppressWarnings("unchecked")
        private String[] getSelectedItems() {
            Set<String> values = (Set<String>) tree.getValue();
            Set<String> rValues = new HashSet<String>();
            for (String value: values) {
                if (value.startsWith(DOT)) {
                    value = value.substring(DOT.length());
                }
                rValues.add(value);
            }
            return (rValues.toArray(new String[rValues.size()]));
        }
    }

    /**
     * Removes all variables in the tree and adds new.
     * @param variables List of new fariables.
     */
    public void initVariablesInTree() {
        List<Variable> variables = AggregateVariables.readFrom(
                wm.getMainContent().getActiveWorkspace().getDispatcher().getRootAggregate()).getVariables();
        if (null == variables) {
            throw new NullPointerException("Patameter variables can't be equals to null.");
        }
        /* Receiving all variables in tree and remove variables, which has been removed. */
        tree.clearNode(ModelTree.VARIABLES);
        /* Add all new variables to the tree. */
        for (Variable variable : variables) {
            tree.addItem(ModelTree.VARIABLES, variable.getName());
        }
    }

    public Position getDefaultPosition() {
        return Position.LEFT;
    }
}