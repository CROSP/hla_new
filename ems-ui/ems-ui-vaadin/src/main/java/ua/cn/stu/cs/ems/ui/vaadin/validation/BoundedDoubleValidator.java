package ua.cn.stu.cs.ems.ui.vaadin.validation;

/**
 * @author n0weak
 */
public class BoundedDoubleValidator extends AbstractValidator {

    private final double from;
    private final double to;

    BoundedDoubleValidator(double from, double to) {
        super("Only double values from the [" + from + "," + to + "] range are accepted");
        this.from = from;
        this.to = to;
    }

    public boolean isValid(Object o) {
        Double d;
        try {
            d = Double.parseDouble(String.valueOf(o));
        } catch (Exception e) {
            d = null;
        }
        return d != null && d >= from && d <= to;
    }
}
