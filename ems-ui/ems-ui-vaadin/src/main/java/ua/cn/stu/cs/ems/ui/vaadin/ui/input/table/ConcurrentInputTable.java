package ua.cn.stu.cs.ems.ui.vaadin.ui.input.table;

/**
 * Editor table which must be used to work on the limited data with unique valued fields.
 * Values must be exposed via {@link Valuable#getValue()}.
 * Must be used together with {@link ConcurrentFieldCreator}
 *
 * @author n0weak
 */
public abstract class ConcurrentInputTable<T extends Valuable<T>> extends CachingInputTable<T> {

    public ConcurrentInputTable(Class<T> clazz, String caption) {
        super(clazz, caption);
    }

    @Override
    @SuppressWarnings("unchecked")
    public boolean removeItem(Object itemId) {
        CachingFieldCreator<?> fieldCreator = getFieldCreator();
        if (fieldCreator instanceof ConcurrentFieldCreator<?>) {
            ((ConcurrentFieldCreator) fieldCreator).updateContainers(null, ((T) itemId).getValue());
        }
        return super.removeItem(itemId);
    }
}
