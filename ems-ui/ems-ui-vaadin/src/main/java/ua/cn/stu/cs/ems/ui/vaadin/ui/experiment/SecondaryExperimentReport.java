package ua.cn.stu.cs.ems.ui.vaadin.ui.experiment;

import com.vaadin.data.Property;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import ua.cn.stu.cs.ems.core.aggregates.AggregateChild;
import ua.cn.stu.cs.ems.core.aggregates.AggregateVariable;
import ua.cn.stu.cs.ems.core.experiment.statistics.StatisticsParameters;
import ua.cn.stu.cs.ems.core.experiment.statistics.StatisticsResult;
import ua.cn.stu.cs.ems.core.utils.Pair;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * @author n0weak
 */
class SecondaryExperimentReport extends ExperimentReport {
// ------------------------------ FIELDS ------------------------------

    private final Pair<AggregateVariable, Double>[][] factorValues;
    private final Collection<String> factorNames;
    private final Collection<ExperimentSettings.FactorEntry> factorEntries;
    private ComboBox factorCB;
    private SecondaryFactorsControl factorsControl;

// --------------------------- CONSTRUCTORS ---------------------------
    public SecondaryExperimentReport(Map<AggregateChild, StatisticsResult[]> results,
                                     Pair<AggregateVariable, Double>[][] factorValues,
                                     Collection<ExperimentSettings.FactorEntry> factorsEntries) {
        super(results);

        if (factorValues.length == 0 || factorValues[0].length == 0 || results.isEmpty()) {
            throw new IllegalArgumentException("Report data must be fully initialized");
        }

        this.factorValues = factorValues;
        this.factorEntries = factorsEntries;

        this.factorNames = new ArrayList<String>(factorValues[0].length);
        for (Pair<AggregateVariable, Double> pair : factorValues[0]) {
            this.factorNames.add(pair.getFirst().getName());
        }

        paintContent();
    }

    @Override
    protected void paintContent() {
        super.paintContent();
        factorCB.setValue(factorNames.iterator().next());
    }

// -------------------------- OTHER METHODS --------------------------
    @Override
    protected Panel createControlsPanel() {
        Panel p = super.createControlsPanel();
        VerticalLayout layout = (VerticalLayout) p.getContent();

        factorCB = new ComboBox("Factor");
        factorCB.setContainerDataSource(new BeanItemContainer<String>(String.class, factorNames));
        factorCB.setImmediate(true);
        layout.addComponent(factorCB);
        layout.setComponentAlignment(factorCB, Alignment.MIDDLE_CENTER);

        if (factorEntries.size() > 1) {
            factorsControl = new SecondaryFactorsControl(factorEntries, controlsValueChangeListener);
            layout.addComponent(factorsControl);
            layout.setComponentAlignment(factorsControl, Alignment.MIDDLE_CENTER);

            factorCB.addListener(new Property.ValueChangeListener() {

                public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
                    Object value = valueChangeEvent.getProperty().getValue();
                    if (null != value) {
                        factorsControl.hide(value.toString());
                    }
                }
            });
        }

        factorCB.addListener(controlsValueChangeListener);

        return p;
    }

    @Override
    protected void drawCharts(Panel mainContent, AggregateChild child) {
        Collection<Integer> resultIndices = null != factorsControl ? getResultIndices(factorsControl.getValue()) : null;
        String varName = factorCB.getValue().toString();
        double[] varValues = getVarValues(varName, resultIndices);

        for (StatisticsParameters param : StatisticsParameters.getApplicableParameters(child)) {
            mainContent.addComponent(createChartTypeSelect(param));
            mainContent.addComponent(drawChart(varValues, getStatistic(child, param, resultIndices), param.toString(),
                                               varName, getChartType(param)));
        }
    }

    /**
     * The goal of this method is to define entities from the results array which correspond to the main factor and the secondary factors, chosen by the user.
     *
     * @param secondaryFactors map which contains name-value pairs representing secondary factors, chosen by the user.
     * @return indices of corresponding results
     */
    private Collection<Integer> getResultIndices(Map<String, Double> secondaryFactors) {
        Collection<Integer> result = new ArrayList<Integer>();
        for (Integer i = 0; i < factorValues.length; i++) {
            Pair<AggregateVariable, Double>[] pairs = factorValues[i];
            if (containsStamp(pairs, secondaryFactors)) {
                result.add(i);
            }
        }
        return result;
    }

    private boolean containsStamp(Pair<AggregateVariable, Double>[] pairs, Map<String, Double> secondaryFactors) {
        Map<String, Double> factorsStamp = new HashMap<String, Double>(secondaryFactors);
        for (Pair<AggregateVariable, Double> pair : pairs) {
            if (pair.getSecond().equals(factorsStamp.get(pair.getFirst().getName()))) {
                factorsStamp.remove(pair.getFirst().getName());
            }
        }
        return factorsStamp.isEmpty();
    }

    private double[] getVarValues(String name, Collection<Integer> resultIndices) {
        double[] result = new double[null != resultIndices ? resultIndices.size() : factorValues.length];
        int resultIdx = 0;
        for (Integer i = 0; i < factorValues.length; i++) {
            if (null == resultIndices || resultIndices.contains(i)) {
                Pair<AggregateVariable, Double>[] pairs = factorValues[i];
                result[resultIdx] = getVarValue(name, pairs);
                resultIdx++;
            }
        }
        return result;
    }

    private double getVarValue(String name, Pair<AggregateVariable, Double>[] pairs) {
        for (Pair<AggregateVariable, Double> pair : pairs) {
            if (pair.getFirst().getName().equals(name)) {
                return pair.getSecond();
            }
        }
        throw new IllegalArgumentException("Failed to find value for " + name);
    }

    private double[] getStatistic(AggregateChild entity, StatisticsParameters parameter,
                                  Collection<Integer> resultIndices) {
        StatisticsResult[] srs = results.get(entity);
        double[] result = new double[null != resultIndices ? resultIndices.size() : srs.length];
        int resultIdx = 0;
        for (Integer i = 0; i < srs.length; i++) {
            if (null == resultIndices || resultIndices.contains(i)) {
                StatisticsResult sr = srs[i];
                result[resultIdx] = sr.getResultValue(parameter);
                resultIdx++;
            }
        }
        return result;
    }

    @Override
    protected boolean validateAdditionalControls() {
        return null != factorCB.getValue() && factorNames.contains(factorCB.getValue().toString());
    }
}
