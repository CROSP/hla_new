package ua.cn.stu.cs.ems.ui.vaadin.ui.experiment.report;

import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Window;

/**
 * @author n0weak
 */
public class GraphicStatisticsResultWindow extends Window {

    private static final String CAPTION = "Report";
    private Panel mainContent;

    public GraphicStatisticsResultWindow(GraphicResult graphicResult, GraphicResultOutputer outputer) {
        super(CAPTION);

        setResizable(false);

        HorizontalLayout content = new HorizontalLayout();
        setContent(content);
        mainContent = new Panel();
        mainContent.setHeight("575px");
        mainContent.setWidth("640px");
        mainContent.setScrollable(true);
        content.addComponent(mainContent);

        Component graphic = outputer.createOutput(graphicResult);
        mainContent.addComponent(graphic);

        this.setPositionX(350);
        this.setPositionY(60);
    }

    public GraphicStatisticsResultWindow(HistoResult histoResult, GraphicResultOutputer outputer) {
        super(CAPTION);

        setResizable(false);

        HorizontalLayout content = new HorizontalLayout();
        setContent(content);
        mainContent = new Panel();
        mainContent.setHeight("575px");
        mainContent.setWidth("640px");
        mainContent.setScrollable(true);
        content.addComponent(mainContent);

        Component histo = outputer.createOutput(histoResult);
        mainContent.addComponent(histo);

        this.setPositionX(350);
        this.setPositionY(60);
    }
}
