package ua.cn.stu.cs.ems.ui.vaadin.ui.experiment;

import com.vaadin.ui.Window;
import ua.cn.stu.cs.ems.core.utils.ArrayUtils;
import ua.cn.stu.cs.ems.ui.vaadin.ExperimentManager;
import ua.cn.stu.cs.ems.ui.vaadin.ui.experiment.report.GraphicResult;
import ua.cn.stu.cs.ems.ui.vaadin.ui.experiment.report.GraphicStatisticsResultWindow;
import ua.cn.stu.cs.ems.ui.vaadin.ui.experiment.report.SeveralGraphicsOutputer;

/**
 * @author n0weak
 */
public class ReportFactory {

    public static Window createExperimentReport(ExperimentManager manager) {

        //ToDo Dummy

//        switch (manager.getExperimentType()) {
//            case COUNT_DOWN:
//            case MUL_AUTOSTOP:
//            case SINGLE_AUTOSTOP:
        GraphicResult gr = new GraphicResult(new double[]{1, 2, 3}, "XAxe");
        gr.addYValues(ArrayUtils.createBoxedArray(new double[]{10, 200, 30}), "YAxe1", "g1");
        gr.addYValues(ArrayUtils.createBoxedArray(new double[]{30, 100, 20}), "YAxe2", "g2");
        gr.addYValues(ArrayUtils.createBoxedArray(new double[]{300, 50, 100}), "YAxe2", "g3");

        return new GraphicStatisticsResultWindow(gr, new SeveralGraphicsOutputer());
//            case PRIMARY:
//                return new PrimaryExperimentReport(manager.getExperimentResults(), manager.getStatisticsDumpTimes());
//            default:
//                throw new UnsupportedOperationException("Experiment type is not supported: "
//                                                        + manager.getExperimentType());
//        }
    }
}
