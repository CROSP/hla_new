package ua.cn.stu.cs.ems.ui.vaadin.validation;

import com.vaadin.data.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Method;
import java.util.Collection;

/**
 * @author n0weak
 */
public class UniquenessValidator<T> implements Validator {

    final static Logger logger = LoggerFactory.getLogger(UniquenessValidator.class);
    private static final String MSG = "Value is not unique!";
    private final Method method;
    private final DataSource<T> dataSource;

    public UniquenessValidator(Class<T> clazz, String getterName, Collection<T> items) {
        this(clazz, getterName, new DefaultDataSource<T>(items));
    }

    public UniquenessValidator(Class<T> clazz, String getterName, DataSource<T> dataSource) {
        try {
            this.method = clazz.getMethod(getterName);
            method.setAccessible(true);
        } catch (NoSuchMethodException e) {
            throw new IllegalArgumentException("Method " + getterName + " was not found in class " + clazz);
        }
        this.dataSource = dataSource;
    }

    public void validate(Object o) throws InvalidValueException {
        if (!isValid(o)) {
            throw new InvalidValueException(MSG);
        }
    }

    public boolean isValid(Object o) {
        try {
            int cnt = 0;

            Collection<T> items = dataSource.getItems();
            for (T item : items) {
                if (method.invoke(item).equals(o)) {
                    cnt++;
                }
            }

            return cnt < 2;

        } catch (Exception e) {
            logger.error("failed to validate uniqueness", e);
            return false;
        }
    }

    public interface DataSource<T> {

        Collection<T> getItems();
    }

    public static class DefaultDataSource<T> implements DataSource<T> {

        private final Collection<T> items;

        public DefaultDataSource(Collection<T> items) {
            this.items = items;
        }

        public Collection<T> getItems() {
            return items;
        }
    }
}
