package ua.cn.stu.cs.ems.ui.vaadin.validation;

import com.vaadin.data.Validator;

/**
 * @author n0weak
 */
public class PositiveIntegerValidator implements Validator {

    private static final String MSG = "Positive integer expected";

    public void validate(Object o) throws InvalidValueException {
        if (!isValid(o)) {
            throw new InvalidValueException(MSG);
        }
    }

    public boolean isValid(Object o) {
        boolean result = false;
        if (null != o) {
            try {
                Integer val = Integer.parseInt(o.toString());
                if (val >= 0) {
                    result = true;
                }
            } catch (Exception e) {
                result = false;
            }
        }
        return result;
    }
}
