package ua.cn.stu.cs.ems.ui.vaadin.ui;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.VerticalLayout;
import org.vaadin.spacewrapper.SpaceWrapper;
import ua.cn.stu.cs.ems.ui.vaadin.PropertiesListener;
import ua.cn.stu.cs.ems.ui.vaadin.dto.GlobalSettings;

/**
 * @author n0weak
 */
public class AnimationSettingsWindow extends AbstractSettingsWindow {

    static final String CAPTION = "Animation Mode Settings";
    private final static String COMMIT_BTN_NAME = "Run";
    private static final String WIDTH = "300px";
    private final PropertiesListener<GlobalSettings> propertiesListener;
    private final GlobalSettings settings;
    private CheckBox showDialogCB;
    private AnimationSettingsForm form;

    public AnimationSettingsWindow(GlobalSettings settings, final PropertiesListener<GlobalSettings> propertiesListener) {
        super(CAPTION, WIDTH);
        this.propertiesListener = propertiesListener;
        this.settings = settings;
        paint();
    }

    @Override
    protected String getCommitButtonName() {
        return COMMIT_BTN_NAME;
    }

    protected void paint() {
        VerticalLayout layout = (VerticalLayout) getContent();

        form = new AnimationSettingsForm(settings.getAnimationSettings());
        layout.addComponent(form);
        paintFooter(layout);

        showDialogCB = new CheckBox("Show this dialog before each run");
        showDialogCB.setValue(settings.getUISettings().isAnimationSetupDialogShown());

        SpaceWrapper sw = new SpaceWrapper(showDialogCB);
        sw.setMarginTop(10);
        layout.addComponent(sw);
        layout.setComponentAlignment(sw, Alignment.TOP_LEFT);
    }

    @Override
    protected boolean apply() {
        if (form.isValid() && showDialogCB.isValid()) {
            form.commit();
            settings.getUISettings().setAnimationSetupDialogShown(showDialogCB.booleanValue());
            propertiesListener.propertiesSet(settings);
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    protected void discard() {
        form.discard();
        showDialogCB.setValue(settings.getUISettings().isAnimationSetupDialogShown());
    }
}
