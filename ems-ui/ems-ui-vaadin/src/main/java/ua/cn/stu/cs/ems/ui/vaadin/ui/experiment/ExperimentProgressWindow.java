package ua.cn.stu.cs.ems.ui.vaadin.ui.experiment;

import com.vaadin.addon.chameleon.ChameleonTheme;
import com.vaadin.ui.*;
import com.vaadin.ui.Button.ClickEvent;
import org.vaadin.artur.icepush.ICEPush;
import ua.cn.stu.cs.ems.core.experiment.ExperimentReport;
import ua.cn.stu.cs.ems.ui.vaadin.Dispatcher;
import ua.cn.stu.cs.ems.ui.vaadin.ExperimentManager;

/**
 * @author n0weak
 */
public class ExperimentProgressWindow extends Window implements Dispatcher.ExperimentProgressListener {

    private static final int POLLING_INTERVAL = 1000;
    /**
     * Defines period of {@link #currentProgress} updates.
     * Too intense updates lead to progress indication failure - its value freezes.
     */
    private static final int MODEL_TIME_UPD_PERIOD = POLLING_INTERVAL / 2;
    private static final String START_CAPTION = "Experiment is running...";
    private static final String FINISH_CAPTION = "Experiment finished";
    private final ProgressIndicator currentProgress;
    private ProgressIndicator factorProgress;
    private ProgressIndicator totalProgress;
    private ExperimentManager manager;
    private final Button stopBtn;
    private final Button reportBtn;
    private final Button exitButton;
    private final double modelingTime;
    private final long factorRuns;
    private final long experimentRuns;
    private final long totalRuns;
    private final ICEPush pusher;
    private long lastUpdate;
    private int madeRuns;

    public ExperimentProgressWindow(final ExperimentManager manager, ICEPush pusher,
            final ExperimentWindowListener listener) {
        super(START_CAPTION);
        this.modelingTime = manager.getModelingTime();
        this.factorRuns = manager.getFactorRuns();
        this.experimentRuns = manager.getExperimentRuns();
        this.totalRuns = factorRuns * experimentRuns;
        this.pusher = pusher;
        this.manager = manager;

        manager.setExperimentListener(this);

        setSizeUndefined();
        getContent().setSizeUndefined();
        setResizable(false);
        setClosable(false);
        setModal(true);

        if (manager.isSecondaryStatisticsEnabled() || experimentRuns > 1) {
            totalProgress = createProgressIndicator();
            addComponent(totalProgress);
            setTotalProgress(0);
        }

        currentProgress = createProgressIndicator();
        addComponent(currentProgress);
        setCurrentProgress(0);

        if (manager.isSecondaryStatisticsEnabled()) {
            factorProgress = createProgressIndicator();
            addComponent(factorProgress);
            setFactorProgress(0);
        }

        HorizontalLayout h_layout = new HorizontalLayout();
        h_layout.setWidth("100%");

        reportBtn = new Button("Report");
        reportBtn.setStyleName(ChameleonTheme.BUTTON_SMALL);
        reportBtn.setEnabled(false);
        reportBtn.addListener(new Button.ClickListener() {

            public void buttonClick(Button.ClickEvent clickEvent) {
                listener.showReport(manager.getExperimentReport());
                ExperimentProgressWindow.this.getParent().removeWindow(ExperimentProgressWindow.this);
            }
        });
        h_layout.addComponent(reportBtn);
        h_layout.setComponentAlignment(this.reportBtn, Alignment.MIDDLE_LEFT);

        stopBtn = new Button("Stop");
        stopBtn.setStyleName(ChameleonTheme.BUTTON_SMALL);
        stopBtn.addListener(new Button.ClickListener() {

            public void buttonClick(Button.ClickEvent clickEvent) {
                listener.stopExperiment();
                ExperimentProgressWindow.this.getParent().removeWindow(ExperimentProgressWindow.this);
            }
        });
        h_layout.addComponent(stopBtn);
        h_layout.setComponentAlignment(this.stopBtn, Alignment.MIDDLE_CENTER);

        this.exitButton = new Button("Exit", new Button.ClickListener() {

            public void buttonClick(ClickEvent event) {
                ExperimentProgressWindow.this.getParent().removeWindow(ExperimentProgressWindow.this);
            }
        });
        this.exitButton.setStyleName(ChameleonTheme.BUTTON_SMALL);
        this.exitButton.setEnabled(false);
        h_layout.addComponent(this.exitButton);
        h_layout.setComponentAlignment(this.exitButton, Alignment.MIDDLE_RIGHT);

        this.addComponent(h_layout);
    }

    private void setCurrentProgress(double time) {
        if (System.currentTimeMillis() - lastUpdate > MODEL_TIME_UPD_PERIOD || experimentFinished()) {
            lastUpdate = System.currentTimeMillis();
            float progress = (float) (time / modelingTime);
            currentProgress.setValue(progress);
            currentProgress.setCaption("Model time: " + time + "/" + modelingTime);
        }
    }

    private void setFactorProgress(long madeRuns) {
        if (this.manager.isSecondaryStatisticsEnabled()) {
            long madeFactorRuns;
            if (!experimentFinished()) {
                madeFactorRuns = madeRuns / experimentRuns;
                factorProgress.setValue(madeFactorRuns / (float) factorRuns);
            } else {
                madeFactorRuns = factorRuns;
                factorProgress.setValue(1);
            }
            factorProgress.setCaption("Factor runs: " + madeFactorRuns + "/" + factorRuns);
        }
    }

    private void setTotalProgress(long madeRuns) {
        if (this.manager.isSecondaryStatisticsEnabled()) {
            long madeExperimentRuns = madeRuns;
            if (!experimentFinished()) {
                totalProgress.setValue(madeRuns / (float) totalRuns);
            } else {
                madeExperimentRuns = totalRuns;
                totalProgress.setValue(1);
            }
            totalProgress.setCaption("Total runs " + madeExperimentRuns + "/" + totalRuns);
        } else if (this.manager.getExperimentRuns() > 1) {
            float progress = (float) (madeRuns / (float) experimentRuns);
            if (madeRuns < experimentRuns) {
                totalProgress.setValue(progress);
            } else {
                totalProgress.setValue(1);
            }
            totalProgress.setCaption("Total runs " + madeRuns + "/" + experimentRuns);
            totalProgress.requestRepaint();
        }
    }

    public void modelTimeChanged(double time) {
        synchronized (getApplication()) {
            setCurrentProgress(time);
        }
    }

    public void modelStarted() {
        synchronized (getApplication()) {
            setCurrentProgress(0d);
            stopBtn.setEnabled(true);
            reportBtn.setEnabled(false);
            exitButton.setEnabled(false);
        }
    }

    public void modelFinished() {
        synchronized (getApplication()) {
            madeRuns++;
            setFactorProgress(madeRuns);
            setTotalProgress(madeRuns);
            setCurrentProgress(modelingTime);
            if (experimentFinished()) {
                setCaption(FINISH_CAPTION);
                stopBtn.setEnabled(false);
                reportBtn.setEnabled(true);
                this.exitButton.setEnabled(true);
                pusher.push();
            }
        }
    }

    private boolean experimentFinished() {
        return madeRuns >= totalRuns;
    }

    private ProgressIndicator createProgressIndicator() {
        ProgressIndicator progress = new ProgressIndicator();
        progress.addStyleName(ChameleonTheme.PROGRESS_INDICATOR_BIG);
        progress.setPollingInterval(POLLING_INTERVAL);
        progress.setValue(0f);

        progress.setWidth("350px");
        progress.setHeight("25px");
        return progress;
    }

    public interface ExperimentWindowListener {

        void stopExperiment();

        void showReport(ExperimentReport experimentReport);
    }
}