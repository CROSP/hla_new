package ua.cn.stu.cs.ems.ui.vaadin.ui.input.table;

/**
 * @author n0weak
 */
public interface Valuable<T> extends Editable<T> {

    String getValue();
}
