package ua.cn.stu.cs.ems.ui.vaadin.ui.input.table;

import com.vaadin.data.Property;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.AbstractField;
import com.vaadin.ui.ComboBox;
import org.apache.commons.lang.StringUtils;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/**
 * Field creator which must be used to work on the limited data with unique valued fields.
 * Values must be exposed via {@link Valuable#getValue()}.
 * Must be used together with {@link ConcurrentInputTable}
 *
 * @author n0weak
 */
public abstract class ConcurrentFieldCreator<T extends Valuable<T>> extends CachingFieldCreator<T> {

    private Map<T, String> savedData = new HashMap<T, String>();
    private final String field;

    protected ConcurrentFieldCreator(String field) {
        super(field);
        this.field = field;
    }

    public void process(AbstractField field, final T item) {
        field.addListener(new Property.ValueChangeListener() {

            public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
                String prevVal = savedData.get(item);
                if (StringUtils.isNotBlank(item.getValue()) && (prevVal == null || !prevVal.equals(item.getValue()))) {
                    updateContainers(item.getValue(), prevVal);
                    savedData.put(item, item.getValue());
                }
            }
        });
    }

    public void updateContainers(String val, String prevVal) {
        for (AbstractField f : getCache(field).values()) {
            if (null == val || !val.equals(String.valueOf(f.getValue()))) {
                ComboBox cb = (ComboBox) f;
                cb.getContainerDataSource().removeItem(val);
                if (null != prevVal) {
                    cb.getContainerDataSource().addItem(prevVal);
                }
            }
        }
    }

    public BeanItemContainer<String> initContainer(Object currentId, Collection<String> entities,
                                                   Collection<T> innerList) {
        Collection<String> availableEntities = new HashSet<String>(entities);
        for (T entry : innerList) {
            if (null != entry.getValue() && (null == currentId || !currentId.toString().equals(entry.getValue()))) {
                availableEntities.remove(entry.getValue());
            }
        }
        return new BeanItemContainer<String>(String.class, availableEntities);
    }
}
