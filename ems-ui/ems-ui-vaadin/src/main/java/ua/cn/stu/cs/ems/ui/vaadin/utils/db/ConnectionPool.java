package ua.cn.stu.cs.ems.ui.vaadin.utils.db;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.*;
import java.sql.*;
import java.util.*;
import java.io.*;
import ua.cn.stu.cs.ems.ui.vaadin.utils.PropertiesReader;
import ua.cn.stu.cs.ems.ui.vaadin.utils.PropertiesReader.Property;

public class ConnectionPool implements ConnectionPoolDataSource, ConnectionEventListener {

    final static Logger logger = LoggerFactory.getLogger(ConnectionPool.class);
    protected static ConnectionPool poolSingleton = null;
    private Vector pooledConnections = null;
    private PrintWriter logWriter = new PrintWriter(System.out);
    private int initConnectionCount;
    private String driver;
    private String url;
    private String user;
    private String password;
    private String charSet;
    private Properties properties;

    public static synchronized ConnectionPool getConnectionPool()
            throws SQLException {
        if (poolSingleton != null) {
            return poolSingleton;
        }

        String _driver = PropertiesReader.read(Property.DB_CONNECTION_DRIVER);
        String _url = PropertiesReader.read(Property.DB_CONNECTION_HOST);
        String _user = PropertiesReader.read(Property.DB_CONNECTION_USERNAME);
        String _password = PropertiesReader.read(Property.DB_CONNECTION_PASSWORD);
        String _charSet = PropertiesReader.read(Property.DB_CONNECTION_CHARSET);

        int _initConnectionCount = Integer.parseInt(PropertiesReader.read(Property.DB_CONNECTION_COUNT));

        try {
            poolSingleton = new ConnectionPool(_driver, _url, _user, _password, _charSet, _initConnectionCount);
        } catch (ClassNotFoundException _e) {
            logger.debug("Exception:" + _e);
            return null;
        }
        return poolSingleton;
    }

    /**
     * Paramentrized Constructor.
     * 
     * @param _driver
     *            JDBC Driver Name
     * @param _url
     *            Database URL
     * @param _user
     *            Database User Name
     * @param _password
     *            Database User Password
     * @param _initConnectionCount
     *            Init connection count
     * @exception SQLException
     * @exception ClassNotFoundException
     */
    protected ConnectionPool(String _driver, String _url, String _user,
            String _password, String _charSet, int _initConnectionCount)
            throws SQLException, ClassNotFoundException {
        driver = _driver;
        url = _url;
        user = _user;
        password = _password;
        charSet = _charSet;
        initConnectionCount = _initConnectionCount;
        logger.debug("Initializing Pool : " + this);

        properties = new Properties();
        properties.put("user", _user);
        properties.put("password", _password);
        properties.put("charSet", _charSet);

        pooledConnections = new Vector();

        Class.forName(driver);

        synchronized (pooledConnections) {
            for (int i = 0; i < initConnectionCount; i++) {
                createPooledConnection();
            }
        }
    }

    /**
     * Creates new PooledConnectionImpl and adds it to pool.
     * 
     * @return new PooleConnectionImpl
     * @exception SQLException
     */
    private PooledConnectionImpl createPooledConnection() throws SQLException {
        // PooledConnectionImpl pooledConnection = new
        // PooledConnectionImpl(DriverManager.getConnection(url,user,password));
        PooledConnectionImpl pooledConnection = new PooledConnectionImpl(DriverManager.getConnection(url, properties));
        pooledConnection.addConnectionEventListener(this);
        pooledConnections.add(pooledConnection);
        logger.debug("Open New Pooled Connection : " + pooledConnection);
        return pooledConnection;
    }

    /**
     * Stub. Not Implemented. Bean 'set' method for loginTimeout property.
     * 
     * @param _timeout
     * @exception SQLException
     */
    public void setLoginTimeout(int _timeout) throws SQLException {
        throw new SQLException(
                "ConnectionPool::setLoginTimeout(...) - function not implemented.");
    }

    /**
     * Stub. Not Implemented. Bean 'get' method for loginTimeout property.
     * 
     * @return loginTimeout property value;
     * @exception SQLException
     */
    public int getLoginTimeout() throws SQLException {
        throw new SQLException("ConnectionPool::getLoginTimeout() - function not implemented.");
    }

    /**
     * Gets PooledConnection from pool. If no free connections avalable new will
     * be created.
     * 
     * @return PooledConnection
     * @exception SQLException
     */
    public PooledConnection getPooledConnection() throws SQLException {
        logger.debug("Getting Pooled Connection.");
        synchronized (pooledConnections) {
            int size = pooledConnections.size();
            PooledConnectionImpl conn = null;

            for (int i = 0; i < size; i++) {
                if (!((PooledConnectionImpl) pooledConnections.get(i)).getBusy()) {
                    conn = (PooledConnectionImpl) pooledConnections.get(i);
                    if (conn.isClosed()) {
                        conn.setConnection(DriverManager.getConnection(url, user, password));
                        logger.debug("Closed Connection " + conn + " Found. Reconnecting...");
                    }
                }
            }

            if (conn == null) {
                conn = createPooledConnection();
            }
            conn.setBusy(true);
            logger.debug("Returning PooledConnection : " + conn);
            logger.debug("Pool Size is " + pooledConnections.size() + ".");
            return (PooledConnection) conn;
        }
    }

    /**
     * Stub. Not Implemented. Gets PooledConnection from pool.
     * 
     * @param _user
     *            Database User Name
     * @param _password
     *            Database User Password
     * @exception SQLException
     */
    public PooledConnection getPooledConnection(String _user, String _password)
            throws SQLException {
        throw new SQLException("ConnectionPool::getPooledConnection(...) - function not implemented.");
    }

    /**
     * On connection close event handler. Implementation of
     * ConnectioEventListener interface. Releases connection and returns it to
     * pool.
     * 
     * @param _event
     *            connection event object.
     */
    public void connectionClosed(ConnectionEvent _event) {
        PooledConnectionImpl conn = (PooledConnectionImpl) _event.getSource();
        conn.setBusy(false);
        logger.debug("DEBUG:ConnectionPool:Closing PooledConnection : " + conn);
    }

    /**
     * Stub. Not Implemented. On connection error event handler. Implementation
     * of ConnectioEventListener interface.
     * 
     * @param _event
     *            connection event object.
     */
    public void connectionErrorOccurred(ConnectionEvent _event) {
        logWriter.println("connectionErrorOccurred(...) - function not implemented.");
    }

    /**
     * Finalizer. Closes all opened connections when object collected by GC.
     */
    public void finalize() {
        logger.debug("Finalization " + this);
        logger.debug("Closing  "
                + pooledConnections.size() + " PooledConnections");
        for (int i = 0; i < pooledConnections.size(); i++) {
            try {
                ((PooledConnection) pooledConnections.remove(i)).getConnection().close();
            } catch (Exception _exception) {
                logWriter.println("ConnectionPool::finalize() we can't even die normally: " + _exception);
                logger.debug("Exception:" + _exception);
            }
        }
    }

    public PrintWriter getLogWriter() throws SQLException {
        return logWriter;
    }

    public void setLogWriter(PrintWriter _logWriter) throws SQLException {
       logWriter = _logWriter;
    }
    
    public java.util.logging.Logger getParentLogger() throws SQLFeatureNotSupportedException {
        throw new SQLFeatureNotSupportedException("Not supported");
    }
}