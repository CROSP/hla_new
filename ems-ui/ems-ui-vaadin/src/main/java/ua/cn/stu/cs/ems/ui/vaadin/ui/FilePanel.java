package ua.cn.stu.cs.ems.ui.vaadin.ui;

import com.vaadin.data.Property;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Tree;
import ua.cn.stu.cs.ems.ui.vaadin.FileManager;
import ua.cn.stu.cs.ems.ui.vaadin.SavesManager;

/**
 * @author n0weak
 */
public class FilePanel extends Panel {
    
    private FileManager.FileType mode = null;

    public FilePanel(String[] modelsName, String[] aggTypesNames, SelectionListener listener) {
        HorizontalLayout layout = new HorizontalLayout();
        setContent(layout);
        setScrollable(true);

        final Tree modelsTree = createTree(layout);
        final Tree aggTypesTree = createTree(layout);

        new TreeManager(listener, modelsTree, aggTypesTree, null);

        fillTree(modelsTree, modelsName);
        fillTree(aggTypesTree, aggTypesNames);
    }
    
    public FilePanel(String[] names, FileManager.FileType namesType, SelectionListener listener) {
        mode = namesType;
        HorizontalLayout layout = new HorizontalLayout();
        setContent(layout);
        setScrollable(true);

        final Tree modelsTree = createTree(layout);
        final Tree aggTypesTree = createTree(layout);

        new TreeManager(listener, modelsTree, aggTypesTree, namesType);

        for (int i=0;i<names.length;i++) {
            fillTree(i%2 == 0 ? modelsTree : aggTypesTree, new String[] {names[i]});
        }
    }
    
    private void fillTree(Tree tree, String[] values) {
        for (int i = 0; i < values.length; i++) {
            tree.addItem(values[i]);
            tree.setChildrenAllowed(values[i], false);
        }        
    }

    @Override
    public void setWidth(String width) {
        super.setWidth(width);
        getContent().setWidth(width);
    }

    private Tree createTree(HorizontalLayout layout) {
        Tree tree = new Tree();
        tree.setMultiSelect(false);
        tree.setImmediate(true);
        tree.setNullSelectionAllowed(true);
        layout.addComponent(tree);
        layout.setExpandRatio(tree, 0.5f);
        return tree;
    }

    public interface SelectionListener {

        void fileSelected(String fileName, FileManager.FileType mode);
    }

    private static class TreeManager implements Property.ValueChangeListener {

        private final Tree modelsTree;
        private final Tree aggTypesTree;
        private final FileManager.FileType mode;
        private final SelectionListener listener;

        private TreeManager(SelectionListener listener, Tree modelsTree, Tree aggTypesTree, FileManager.FileType mode) {
            this.modelsTree = modelsTree;
            this.aggTypesTree = aggTypesTree;
            modelsTree.addListener(this);
            aggTypesTree.addListener(this);
            this.listener = listener;
            this.mode = mode;
        }

        public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
            Object value = valueChangeEvent.getProperty().getValue();
            FileManager.FileType ft = null;
            if (value != null) {
                if (value == modelsTree.getValue()) {
                    aggTypesTree.setValue(null);
                    ft = (mode == null ? FileManager.FileType.MODEL : mode);
                }
                if (value == aggTypesTree.getValue()) {
                    modelsTree.setValue(null);
                    ft = (mode == null ? FileManager.FileType.AGGREGATE_TYPE : mode);
                }
                listener.fileSelected(value.toString(), ft);
            }
        }
    }
}
