package ua.cn.stu.cs.ems.ui.vaadin.ui.experiment;

import com.vaadin.addon.chameleon.ChameleonTheme;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Validator;
import com.vaadin.data.Validator.InvalidValueException;
import com.vaadin.data.util.BeanItem;
import com.vaadin.event.Action;
import com.vaadin.event.ShortcutAction;
import com.vaadin.terminal.Sizeable;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.*;
import java.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.cn.stu.cs.ems.core.experiment.statistics.StatisticsParameters;
import ua.cn.stu.cs.ems.ui.vaadin.EntityClass;
import ua.cn.stu.cs.ems.ui.vaadin.Informant;
import ua.cn.stu.cs.ems.ui.vaadin.PropertiesListener;
import ua.cn.stu.cs.ems.ui.vaadin.WidgetManager;
import ua.cn.stu.cs.ems.ui.vaadin.ui.input.tree.ModelTree;
import ua.cn.stu.cs.ems.ui.vaadin.validation.PositiveIntegerValidator;
import ua.cn.stu.cs.ems.ui.vaadin.validation.ValidatorFactory;

/**
 * Draw window for editing experiment's settings befor running it.
 *
 * @author AlexanderBartash@mail.ru
 * @version 1.0
 */
public class ExperimentSettingsWindow extends Window {

    final static Logger logger = LoggerFactory.getLogger(ExperimentSettingsWindow.class);
    private static final String WINDOW_CAPTION = "Experiment settings";
    private static final String WINDOW_WIDTH = "467px";
    private static final String EMPTY_FIELD_NOTIFY = "The field may not be empty";
    private static final String FOOTER_BUTTONS_WIDTH = "70px";
    /**
     * We need save link to this text field, because it is set to required as
     * default. But if user turns on autostop it field must be set as
     * non-required.
     */
    private TextField numberOfRuns;
    /*
     * Saving links to this field allow getting value of this field in inner
     * classes during validation entered values.
     */
    private TextField modelingTime;
    /**
     * Contain all setting from this window.
     */
    private ExperimentSettings experimentSettings;
    /*
     * BeanItems of ExperimentSettings. Use it for setting property data source
     * in components.
     */
    private BeanItem<ExperimentSettings> experimentSettingsBean;
    private BeanItem<ExperimentSettings.PrimaryStatisticsSettings> primaryStatisticsSettingsBean;
    private BeanItem<ExperimentSettings.SecondaryStatisticsSettins> secondaryStatisticsSettinsBean;
    private BeanItem<ExperimentSettings.AutostopSettings> autostopSettingsBean;
    /**
     * Used for receiving list of aggregates variables for FactorEditor.
     */
    private Informant dispatcher;
    /**
     * Used for accesssing for other form components
     */
    private WidgetManager wm;
    /*
     * Components, what must be commited when button "Save" was clicked.
     */
    private List<AbstractField> toCommit = new LinkedList<AbstractField>();
    private StatisticSubjectsTable statisticSubjectsTable;
    private FactorsEditor factorsEditor;
    /*
     * This listener must be notified about result commiting setting, when
     * button "Run" is cliked. Execution of this listener will start the
     * experiment.
     */
    private PropertiesListener<ExperimentSettings> settingsListener;
    /*
     * Saving links to this fields allow getting value of this field in inner
     * classes during validation entered values.
     */
    private TextField statisticsStart;
    private TextField statisticsPeriod;
    private TextField response;
    private TextField autostopResponse;
    private ComboBox responseType;
    private ComboBox autostopResponseType;
//    private CheckBox collectOnEvent;
//    private CheckBox collectWithPeriod;
    private ComboBox selectedAggregateTree;
    private ModelTree tree;
    private boolean debug;
    private HashMap<String, HashMap<EntityClass, List<String>>> selectiveTreeValues =
            new HashMap<String, HashMap<EntityClass, List<String>>>();

    /**
     * Creates window, which containes inside experiment's settings.
     *
     * @param settings Current experiment settings. Window will be initialise
     * according to it.
     * @param dispatcher Current instance of Dispatcher class.
     * @param listener Listener for handling final confirmation of experiment
     * settings.
     * @throws NullPointerException If one from parameters is equal to null.
     *
     */
    public ExperimentSettingsWindow(ExperimentSettings settings, Informant dispatcher,
            PropertiesListener<ExperimentSettings> listener,
            boolean debug,
            WidgetManager wm) throws NullPointerException {
        if ((null == settings) || (null == dispatcher) || (null == listener)) {
            throw new NullPointerException("Parameters can't be equals to null! settings = " + settings
                    + "; dispatcher = " + dispatcher + "; listener = " + listener);
        }
        this.dispatcher = dispatcher;
        this.settingsListener = listener;
        this.experimentSettings = settings;
        this.wm = wm;
        this.debug = debug;

        /*
         * Create a bean, which allows different components to store their
         * states into the this.experimentSettings.
         */
        this.experimentSettingsBean = new BeanItem<ExperimentSettings>(this.experimentSettings);
        this.primaryStatisticsSettingsBean = new BeanItem<ExperimentSettings.PrimaryStatisticsSettings>(
                this.experimentSettings.getPrimaryStatisticSettings());
        this.secondaryStatisticsSettinsBean = new BeanItem<ExperimentSettings.SecondaryStatisticsSettins>(
                this.experimentSettings.getSecondaryStatisticsSettins());
        this.autostopSettingsBean = new BeanItem<ExperimentSettings.AutostopSettings>(
                this.experimentSettings.getAutostopSettings());

        this.setModal(true);
        this.setCaption(ExperimentSettingsWindow.WINDOW_CAPTION);
        this.setWidth(ExperimentSettingsWindow.WINDOW_WIDTH);
        this.setHeight(Sizeable.SIZE_UNDEFINED, Sizeable.UNITS_PIXELS);
        this.setResizable(false);
        this.setScrollable(false);

        VerticalLayout mainLayout = (VerticalLayout) this.getContent();
        mainLayout.setMargin(true);
        mainLayout.addComponent(this.createHead());

        Panel p;
        HorizontalLayout details = new HorizontalLayout();
        details.setWidth("600px");
        details.setHeight(Sizeable.SIZE_UNDEFINED, Sizeable.UNITS_PIXELS);
        details.setMargin(true, false, false, false);
        p = createTreePanel();
        details.addComponent(p);

        Layout l = createAdditionalSettings();
        details.addComponent(l);
        details.setExpandRatio(l, 1.0f);
        l.setWidth("425px");

        p = new Panel();
        CssLayout css = new CssLayout();
        p.setContent(css);
        details.setMargin(false);
        css.addComponent(details);

        p.setWidth("100%");
        p.setHeight((getWindow().getWidth() - 150) + "px");
        css.setWidth("590px");
        css.setStyleName("experimentDetails");
        css.setMargin(false);

        mainLayout.addComponent(p);
        mainLayout.addComponent(this.createFooter());
        this.setWidth("635px");
    }

    /**
     * Create layout with head of settings window.
     *
     * @return Layout with head of settings window.
     */
    private VerticalLayout createHead() {
        VerticalLayout vLayout = new VerticalLayout();
        vLayout.setWidth("100%");
        vLayout.setHeight(Sizeable.SIZE_UNDEFINED, Sizeable.UNITS_PIXELS);

        FormLayout fLayout = new FormLayout();
        fLayout.setMargin(false);
        fLayout.setWidth("100%");
        fLayout.setHeight("80px");

        TextField txtField = new TextField("Name:");
        this.toCommit.add(txtField);
        txtField.setWidth("100%");
        txtField.setRequired(true);
        txtField.setImmediate(true); /*
         * We want to validate an input when the field lose the focus.
         */
        txtField.setInputPrompt("Enter experiment name");
        txtField.setRequiredError(ExperimentSettingsWindow.EMPTY_FIELD_NOTIFY);
        /*
         * Connect field to the appropriate fild in this.experimentSettings
         * through the experimentSettingsBean class.
         */
        txtField.setPropertyDataSource(this.experimentSettingsBean.getItemProperty("modelName"));
        txtField.setValue(dispatcher.getModelName());
        fLayout.addComponent(txtField);

        this.modelingTime = new TextField("Modeling time:");
        this.toCommit.add(this.modelingTime);
        this.modelingTime.setWidth("100%");
        this.modelingTime.setRequired(true);
        this.modelingTime.setImmediate(true); /*
         * We want to validate an input when the field lose the focus.
         */
        this.modelingTime.setRequiredError(ExperimentSettingsWindow.EMPTY_FIELD_NOTIFY);
        this.modelingTime.setPropertyDataSource(this.experimentSettingsBean.getItemProperty("modelingTime"));
        this.modelingTime.addValidator(ValidatorFactory.getPositiveDoubleValidator());
        /*
         * Validation of the fields "Statistics Period" and "Statistics start"
         * depends from the value of the field "Modeling time". Because we must
         * notify them, if value has been changed.
         */
        this.modelingTime.addListener(new Property.ValueChangeListener() {

            public void valueChange(ValueChangeEvent event) {
                try {
                    ExperimentSettingsWindow.this.statisticsStart.requestRepaint();
                    ExperimentSettingsWindow.this.statisticsPeriod.requestRepaint();
                } catch (Exception ex) {
                }
            }
        });
        fLayout.addComponent(this.modelingTime);

        /*
         * We save link to whis text field for AutostopSettingsDrawer. If user
         * turns on autostop, this field must be set as non-required. And that
         * method must have link to instance of this text field. Sending link in
         * paramener don't comfortably in this case.
         */
        this.numberOfRuns = new TextField("Number of runs:");
        this.toCommit.add(this.numberOfRuns);
        this.numberOfRuns.setWidth("100%");
        this.numberOfRuns.setRequired(true);
        this.numberOfRuns.setImmediate(true); /*
         * We want to validate an input when the field lose the focus.
         */
        this.numberOfRuns.setRequiredError(ExperimentSettingsWindow.EMPTY_FIELD_NOTIFY);
        this.numberOfRuns.setPropertyDataSource(this.experimentSettingsBean.getItemProperty("numberOfRuns"));
        this.numberOfRuns.addValidator(new PositiveIntegerValidator());
        if (debug) {
            this.numberOfRuns.addListener(new Property.ValueChangeListener() {

                public void valueChange(ValueChangeEvent event) {
                    numberOfRuns.setValue("1");
                }
            });
        }

        fLayout.addComponent(this.numberOfRuns);

        vLayout.addComponent(fLayout);
        return vLayout;
    }

    private Layout createAdditionalSettings() {
        FormLayout layout = new FormLayout();
        layout.setMargin(false);
        layout.setHeight(Sizeable.SIZE_UNDEFINED, Sizeable.UNITS_PIXELS);
        layout.addComponent(new PrimaryStatisticsSettingsDrawer());
        layout.addComponent(new SecodaryStatisticsSettingsDrawer());
        layout.addComponent(new AutostopSettingsDrawer());
        return layout;
    }

    /**
     * Create layout with footer of settings window, which contain three
     * buttons: Discard, Save and Run.
     *
     * @return Layout with footer of window.
     */
    private Layout createFooter() {
        HorizontalLayout hLayout = new HorizontalLayout();
        hLayout.setWidth("100%");
        hLayout.setMargin(true, false, false, false);

        Button btn = new Button("Discard");
        btn.setStyleName(ChameleonTheme.BUTTON_SMALL);
        btn.setWidth(ExperimentSettingsWindow.FOOTER_BUTTONS_WIDTH);
        hLayout.addComponent(btn);
        hLayout.setComponentAlignment(btn, Alignment.MIDDLE_LEFT);

        btn = new Button("Save");
        btn.setStyleName(ChameleonTheme.BUTTON_SMALL);
        btn.setWidth(ExperimentSettingsWindow.FOOTER_BUTTONS_WIDTH);
        btn.addListener(new Button.ClickListener() {

            public void buttonClick(ClickEvent event) {
                ExperimentSettingsWindow.this.save();
            }
        });
        hLayout.addComponent(btn);
        hLayout.setComponentAlignment(btn, Alignment.MIDDLE_CENTER);

        if (!debug) {
            btn = new Button("Run");
        } else {
            btn = new Button("Debug");
        }
        btn.setStyleName(ChameleonTheme.BUTTON_SMALL);
        btn.setWidth(ExperimentSettingsWindow.FOOTER_BUTTONS_WIDTH);
        btn.addListener(new Button.ClickListener() {

            public void buttonClick(ClickEvent event) {
                if (!ExperimentSettingsWindow.this.save()) {
                    return;
                }
                ExperimentSettingsWindow.this.settingsListener.propertiesSet(
                        ExperimentSettingsWindow.this.experimentSettings);
                ExperimentSettingsWindow.this.getParent().removeWindow(ExperimentSettingsWindow.this);
            }
        });
        hLayout.addComponent(btn);
        hLayout.setComponentAlignment(btn, Alignment.BOTTOM_RIGHT);

        return hLayout;
    }

    /**
     * Validate the values of all fields, needed in validation. And if all
     * values is valid, commit the changes.
     */
    private boolean save() {
        for (AbstractField component : ExperimentSettingsWindow.this.toCommit) {
            if (component.isEnabled()) {
                if (!component.isValid()) {
                    return false;
                }
            }
        }
        if (!ExperimentSettingsWindow.this.experimentSettings.getPrimaryStatisticSettings().isEnabled()
                && !ExperimentSettingsWindow.this.experimentSettings.getSecondaryStatisticsSettins().isEnabled()) {
            return false;
        }
        if (ExperimentSettingsWindow.this.experimentSettings.getSecondaryStatisticsSettins().isEnabled()) {
            if (ExperimentSettingsWindow.this.experimentSettings.getSecondaryStatisticsSettins().getResponse().isEmpty()) {
                return false;
            }
            ExperimentSettingsWindow.this.experimentSettings.getSecondaryStatisticsSettins().setFactors(
                    factorsEditor.getSelectedFactors());
        }

        if (ExperimentSettingsWindow.this.statisticSubjectsTable.isEnabled()) {
            if (!ExperimentSettingsWindow.this.statisticSubjectsTable.isValid()) {
                return false;
            }
            ExperimentSettingsWindow.this.statisticSubjectsTable.commit();
            ExperimentSettingsWindow.this.experimentSettings.getPrimaryStatisticSettings().setSubjectEntities(
                    ExperimentSettingsWindow.this.statisticSubjectsTable.getSelectedEntities());
        }
        for (AbstractField component : ExperimentSettingsWindow.this.toCommit) {
            if (component.isEnabled()) {
                component.commit();
            }
        }
        return true;
    }

    // -----------------------------    INNER CLASSES    -----------------------------
    /**
     * This class used for disabling\enabling components, attaching to the check
     * box's listener.
     */
    public class DisablerCheckBox extends CheckBox {

        /**
         * List of components, what must be disabling\enabling.
         */
        private ArrayList<Component> toDisable = new ArrayList<Component>();

        /**
         * Create check box, what can disabling or enabling other component.
         *
         * @param caption Caption of check box.
         */
        DisablerCheckBox(String caption) {
            super(caption);
            this.setImmediate(true);
            this.addListener(Button.ClickEvent.class, DisablerCheckBox.this, "buttonClick");
        }

        /**
         * Click event of main component, what controll enabling and disabling.
         *
         * @param event Instance of event.
         */
        public void buttonClick(ClickEvent event) {
            boolean enabled = this.booleanValue();
            for (Component component : toDisable) {
                component.setEnabled(enabled);
            }
        }

        /**
         * Register component in listener.
         *
         * @param component Component what must be disabling or enabling, when
         * check box state was changed.
         * @throws NullPointerException if {@code component == null}.
         */
        void register(Component component) throws NullPointerException {
            if (null == component) {
                throw new NullPointerException("Parameter can't be equal to null!");
            }
            this.toDisable.add(component);
            /*
             * We must update state of new component, because their
             * enabled\disabled state may differ with current CheckBox state.
             */
            component.setEnabled(this.booleanValue());
        }

        /**
         * Update enabled\disabled state of all connecting components.
         */
        public void updateState() {
            boolean enabled = this.booleanValue();
            for (Component component : toDisable) {
                component.setEnabled(enabled);
            }
        }

        @Override
        public void setPropertyDataSource(Property newDataSource) {
            super.setPropertyDataSource(newDataSource);
            this.updateState();
        }
    }

    /**
     * Layout with components fo editing primary statistic settings.
     */
    private class PrimaryStatisticsSettingsDrawer extends Panel {

        /**
         * Create layout with primary statistic's settings.
         */
        PrimaryStatisticsSettingsDrawer() {
            this.setWidth("379px");
            this.setHeight("220px");

            /*
             * New layout used for grouping components. It is allow to disable
             * them together by disabling this layout.
             */
            VerticalLayout vLayout = new VerticalLayout();
            vLayout.setMargin(false);

            DisablerCheckBox checkBox = new DisablerCheckBox("Primary statistics");
            ExperimentSettingsWindow.this.toCommit.add(checkBox);

            /*
             * Register vLayout as component, what must be disabled if check box
             * is unchecked.
             */
            checkBox.register(vLayout);
            checkBox.setPropertyDataSource(ExperimentSettingsWindow.this.primaryStatisticsSettingsBean.getItemProperty("enabled"));
            this.addComponent(checkBox);

            /*
             * Must be after adding the check box.
             */
            this.addComponent(vLayout);

            /*
             * Form layout used for grouping text fields and locating their
             * captions at the left side of field.
             */
            FormLayout fLayout = new FormLayout();
            fLayout.setWidth("100%");
            fLayout.setHeight(Sizeable.SIZE_UNDEFINED, Sizeable.UNITS_PIXELS);
            vLayout.addComponent(fLayout);

//            collectOnEvent = new CheckBox("Collect on event");
//            collectWithPeriod = new CheckBox("Collect on period:");
//
//            collectOnEvent.setImmediate(true);
//            ExperimentSettingsWindow.this.collectOnEvent.setPropertyDataSource(
//                    ExperimentSettingsWindow.this.primaryStatisticsSettingsBean.getItemProperty("collectOnEvent"));
//
//            collectWithPeriod.setImmediate(true);
//            ExperimentSettingsWindow.this.collectWithPeriod.setPropertyDataSource(
//                    ExperimentSettingsWindow.this.primaryStatisticsSettingsBean.getItemProperty("collectWithPeriod"));
//
//            ExperimentSettingsWindow.this.statisticsPeriod = new TextField();
            ExperimentSettingsWindow.this.statisticsPeriod = new TextField("Collect on period:");
            ExperimentSettingsWindow.this.toCommit.add(ExperimentSettingsWindow.this.statisticsPeriod);
            ExperimentSettingsWindow.this.statisticsPeriod.setWidth("100%");
            ExperimentSettingsWindow.this.statisticsPeriod.setRequired(false);
            /*
             * We want to validate an input when the field lose the focus.
             */
            ExperimentSettingsWindow.this.statisticsPeriod.setImmediate(true);
            ExperimentSettingsWindow.this.statisticsPeriod.setPropertyDataSource(
                    ExperimentSettingsWindow.this.primaryStatisticsSettingsBean.getItemProperty("statisticsPeriod"));
            ExperimentSettingsWindow.this.statisticsPeriod.addValidator(ValidatorFactory.getPositiveDoubleValidator());
            /*
             * This validator check what the value is less then (modelingTime -
             * statisticsStart) / 2).
             */
            ExperimentSettingsWindow.this.statisticsPeriod.addValidator(new Validator() {

                public void validate(Object value) throws InvalidValueException {
                    if (!this.isValid(value)) {
                        throw new InvalidValueException("Too much value!");
                    }
                }

                public boolean isValid(Object value) {
                    if ((null != value) && (null != ExperimentSettingsWindow.this.statisticsStart)
                            && (null != ExperimentSettingsWindow.this.modelingTime)) {
                        Object oStatisticsStart = ExperimentSettingsWindow.this.statisticsStart.getValue();
                        Object oModelingTime = ExperimentSettingsWindow.this.modelingTime.getValue();
                        if ((null != oStatisticsStart) && (null != oModelingTime)) {
                            double lStatisticsStart = 0;
                            double lModelingTime = 0;
                            double lStatisticsPeriod = 0;
                            try {
                                lStatisticsStart = Double.parseDouble(oStatisticsStart.toString());
                                lStatisticsPeriod = Double.parseDouble(value.toString());
                                lModelingTime = Double.parseDouble(oModelingTime.toString());
                            } catch (NumberFormatException numberFormatException) {
                                return true; /*
                                 * Invalid format. Exception must be handle by
                                 * another type of validator.
                                 */
                            }
                            return (((lModelingTime - lStatisticsStart) / 2) >= lStatisticsPeriod);
                        }
                    }
                    return true; /*
                     * Empty field. Exception must be handle by another type of
                     * validator.
                     */
                }
            });

//             collectOnEvent.addListener(new Property.ValueChangeListener() {
//
//                public void valueChange(ValueChangeEvent event) {
//                    if (ExperimentSettingsWindow.this.collectOnEvent.booleanValue()) {
//                        ExperimentSettingsWindow.this.collectWithPeriod.setValue(false);
//                    } else {
//                        ExperimentSettingsWindow.this.collectWithPeriod.setValue(true);
//                    }
//                }
//            });
//            collectWithPeriod.addListener(new Property.ValueChangeListener() {
//
//                public void valueChange(ValueChangeEvent event) {
//                    if (ExperimentSettingsWindow.this.collectWithPeriod.booleanValue()) {
//                        ExperimentSettingsWindow.this.collectOnEvent.setValue(false);
//                        ExperimentSettingsWindow.this.statisticsPeriod.setEnabled(true);
//                    } else {
//                        ExperimentSettingsWindow.this.collectOnEvent.setValue(true);
//                        ExperimentSettingsWindow.this.statisticsPeriod.setEnabled(false);
//                    }
//                }
//            });
//
//            ExperimentSettingsWindow.this.toCommit.add(collectOnEvent);
//            ExperimentSettingsWindow.this.toCommit.add(collectWithPeriod);
//
//            HorizontalLayout hCollectLayout = new HorizontalLayout();
            FormLayout hCollectLayout = new FormLayout();
            hCollectLayout.setWidth("100%");
            hCollectLayout.setHeight(Sizeable.SIZE_UNDEFINED, Sizeable.UNITS_PIXELS);
            hCollectLayout.setMargin(true, true, false, true);
//            collectWithPeriod.setValue(true);
//            collectWithPeriod.setValue(false);
//            hCollectLayout.addComponent(collectOnEvent);
//            hCollectLayout.addComponent(collectWithPeriod);
            hCollectLayout.addComponent(statisticsPeriod);
//            hCollectLayout.setExpandRatio(collectOnEvent, 1.0f);
//            hCollectLayout.setExpandRatio(collectWithPeriod, 1.0f);
//            hCollectLayout.setExpandRatio(statisticsPeriod, 3.0f);
            hCollectLayout.setExpandRatio(statisticsPeriod, 1.0f);

            vLayout.addComponent(hCollectLayout);

            Collection<String> places = dispatcher.listAll(EntityClass.PLACE);
            Collection<String> queues = dispatcher.listAll(EntityClass.QUEUE);
            Collection<String> transitions = dispatcher.listAll(EntityClass.TRANSITION);
            /*
             * We must clear list of selected entities because it may be cause
             * of problem in initialization of StatisticSubjectsTable.
             */
            ExperimentSettingsWindow.this.experimentSettings.getPrimaryStatisticSettings().getSubjectEntities().clear();
            ExperimentSettingsWindow.this.statisticSubjectsTable = new StatisticSubjectsTable("100%", "100px",
                    "Collect statistics for:",
                    places, queues, transitions);
            statisticSubjectsTable.setColumnWidth(StatisticSubjectsTable.COLUMN_IDS[1], 35);

            vLayout.addComponent(ExperimentSettingsWindow.this.statisticSubjectsTable);
            vLayout.setExpandRatio(statisticSubjectsTable, 1.0f);

            HorizontalLayout hLayout = new HorizontalLayout();
            hLayout.setWidth("100%");
            hLayout.setHeight(Sizeable.SIZE_UNDEFINED, Sizeable.UNITS_PIXELS);
            vLayout.addComponent(hLayout);

            Button btn = null;

            btn = new Button("all places");
            btn.setStyleName(ChameleonTheme.BUTTON_LINK);
//            btn.setEnabled(!places.isEmpty());
            btn.addListener(new Button.ClickListener() {

                public void buttonClick(ClickEvent event) {
                    Collection list = tree.getChildren(ModelTree.PLACES);
                    Collection<String> l2 = new ArrayList<String>();

                    String prefix = "";
                    if (!dispatcher.getModelName().equals(selectedAggregateTree.getValue())) {
                        prefix = selectedAggregateTree.getValue() + ".";
                        prefix = prefix.substring(prefix.indexOf(".") + 1);
                    }

                    for (Object value : list) {
                        l2.add(prefix + value);
                    }
                    ExperimentSettingsWindow.this.statisticSubjectsTable.selectAll(l2);
                }
            });
            hLayout.addComponent(btn);
            hLayout.setComponentAlignment(btn, Alignment.MIDDLE_LEFT);

            btn = new Button("all queues");
            btn.setStyleName(ChameleonTheme.BUTTON_LINK);
            hLayout.addComponent(btn);
//            btn.setEnabled(!queues.isEmpty());
            btn.addListener(new Button.ClickListener() {

                public void buttonClick(ClickEvent event) {
                    Collection list = tree.getChildren(ModelTree.QUEUES);
                    Collection<String> l2 = new ArrayList<String>();
                    String prefix = "";
                    if (!dispatcher.getModelName().equals(selectedAggregateTree.getValue())) {
                        prefix = selectedAggregateTree.getValue() + ".";
                        prefix = prefix.substring(prefix.indexOf(".") + 1);
                    }

                    for (Object value : list) {
                        l2.add(prefix + value);
                    }
                    ExperimentSettingsWindow.this.statisticSubjectsTable.selectAll(l2);
                }
            });
            hLayout.addComponent(btn);
            hLayout.setComponentAlignment(btn, Alignment.MIDDLE_CENTER);

            btn = new Button("all transitions");
            btn.setStyleName(ChameleonTheme.BUTTON_LINK);
            hLayout.addComponent(btn);
//            btn.setEnabled(!transitions.isEmpty());
            btn.addListener(new Button.ClickListener() {

                public void buttonClick(ClickEvent event) {
                    Collection list = tree.getChildren(ModelTree.TRANSITIONS);
                    Collection<String> l2 = new ArrayList<String>();
                    String prefix = "";
                    if (!dispatcher.getModelName().equals(selectedAggregateTree.getValue())) {
                        prefix = selectedAggregateTree.getValue() + ".";
                        prefix = prefix.substring(prefix.indexOf(".") + 1);
                    }

                    for (Object value : list) {
                        l2.add(prefix + value);
                    }
                    ExperimentSettingsWindow.this.statisticSubjectsTable.selectAll(l2);
                }
            });
            hLayout.addComponent(btn);
            hLayout.setComponentAlignment(btn, Alignment.MIDDLE_RIGHT);
        }
    }

    /**
     * Layout with components fo editing secondary statistic settings.
     */
    private class SecodaryStatisticsSettingsDrawer extends Panel {

        /**
         * Create layout with secondary statistic's settings.
         */
        SecodaryStatisticsSettingsDrawer() {
            this.setWidth("379px");
            this.setHeight("150px");
//            this.setMargin(true);

            DisablerCheckBox checkBox = new DisablerCheckBox("Secondary statistics");
            ExperimentSettingsWindow.this.toCommit.add(checkBox);
            checkBox.setPropertyDataSource(ExperimentSettingsWindow.this.secondaryStatisticsSettinsBean.getItemProperty("enabled"));
            this.addComponent(checkBox);

            VerticalLayout vLayout = new VerticalLayout();
            vLayout.setWidth("100%");
            vLayout.setHeight(Sizeable.SIZE_UNDEFINED, Sizeable.UNITS_PIXELS);
            this.addComponent(vLayout);

            /*
             * We must clear list of selected factors because it may be cause of
             * problem in initialization of FactorsEditor.
             */

            ExperimentSettingsWindow.this.experimentSettings.getSecondaryStatisticsSettins().getFactors().clear();
            factorsEditor = new FactorsEditor("Factors:",
                    dispatcher.listAllVariables(),
                    ExperimentSettingsWindow.this.experimentSettings.getSecondaryStatisticsSettins().getFactors());
            factorsEditor.setColumnWidth(StatisticSubjectsTable.COLUMN_IDS[1], 35);
            ExperimentSettingsWindow.this.toCommit.add(factorsEditor);
            factorsEditor.setWidth("100%");
            factorsEditor.setHeight("50px");
            vLayout.addComponent(factorsEditor);

            response = new TextField("Response:");
            ExperimentSettingsWindow.this.experimentSettings.getSecondaryStatisticsSettins().setResponse("");
            toCommit.add(response);
            response.setWidth("100%");
            response.setRequired(true);
            response.setImmediate(true); /*
             * We want to validate an input when the field lose the focus.
             */
            response.setRequiredError("Value cant be empty or not variable");
            response.setPropertyDataSource(secondaryStatisticsSettinsBean.getItemProperty("response"));

            HorizontalLayout hl = new HorizontalLayout();
            FormLayout fLayout = new FormLayout();
            fLayout.setMargin(true, false, false, false);
            fLayout.setWidth("100%");

            fLayout.addComponent(response);
            hl.addComponent(fLayout);
            fLayout.setWidth("353px");

            responseType = new ComboBox();
            ExperimentSettingsWindow.this.toCommit.add(responseType);
            responseType.setWidth("100%");
            responseType.setPropertyDataSource(secondaryStatisticsSettinsBean.getItemProperty("responseType"));
            responseType.addValidator(new Validator() {

                public void validate(Object value) throws InvalidValueException {
                    if (!isValid(value)) {
                        throw new InvalidValueException("Value can be empty");
                    }
                }

                public boolean isValid(Object value) {
                    return null != value;
                }
            });
            responseType.setRequired(false);
            fLayout = new FormLayout();
            fLayout.setMargin(true, false, false, false);
            fLayout.setWidth("100px");
            fLayout.addComponent(responseType);
            fLayout.setExpandRatio(responseType, 1.0f);
            hl.addComponent(fLayout);
            fLayout.setVisible(false);
            hl.setExpandRatio(fLayout, 1.0f);

            fLayout = new FormLayout();
            fLayout.setMargin(true, false, false, false);
            fLayout.setWidth("100%");
            hl.addComponent(fLayout);
            hl.setExpandRatio(fLayout, 1.0f);

            vLayout.addComponent(hl);

            this.addComponent(vLayout);
            checkBox.register(vLayout);
        }
    }

    /**
     * Layout with components fo editing autostop settings.
     */
    private class AutostopSettingsDrawer extends Panel {

        /**
         * Create layout with autostop's settings.
         */
        AutostopSettingsDrawer() {
            this.setWidth("379px");
            this.setHeight(Sizeable.SIZE_UNDEFINED, Sizeable.UNITS_PIXELS);

            final DisablerCheckBox checkBox = new DisablerCheckBox("Autostop switch on");
            ExperimentSettingsWindow.this.toCommit.add(checkBox);

            /*
             * If user turn on autostop we must set field "Number of runs" as
             * non-required.
             */
            checkBox.addListener(new Property.ValueChangeListener() {

                public void valueChange(ValueChangeEvent event) {
                    if (null != ExperimentSettingsWindow.this.numberOfRuns) {
                        ExperimentSettingsWindow.this.numberOfRuns.setEnabled(!checkBox.booleanValue());
                    }
                }
            });

            checkBox.setPropertyDataSource(
                    ExperimentSettingsWindow.this.autostopSettingsBean.getItemProperty("enabled"));
            this.addComponent(checkBox);

            /*
             * New layout used for grouping components. It is allow to disable
             * them together by disabling this layout.
             */
            FormLayout fLayout = new FormLayout();

            fLayout.setWidth("100%");
            fLayout.setHeight(Sizeable.SIZE_UNDEFINED, Sizeable.UNITS_PIXELS);
            fLayout.setEnabled(false);
            this.addComponent(fLayout);

            autostopResponse = new TextField("Object:");
            ExperimentSettingsWindow.this.toCommit.add(autostopResponse);
            autostopResponse.setRequired(true);
            autostopResponse.setRequiredError("The field may not be empty");
            autostopResponse.setImmediate(true); /*
             * We want to validate an input when the field lose the focus.
             */
            autostopResponse.setWidth("100%");
            autostopResponse.setPropertyDataSource(ExperimentSettingsWindow.this.autostopSettingsBean.getItemProperty(
                    "response"));
            autostopResponse.setValue("");
            fLayout.addComponent(autostopResponse);

            autostopResponseType = new ComboBox("Parameter:");
            ExperimentSettingsWindow.this.toCommit.add(autostopResponseType);
            autostopResponseType.setRequired(true);
            autostopResponseType.setRequiredError(ExperimentSettingsWindow.EMPTY_FIELD_NOTIFY);
            autostopResponseType.setWidth("100%");
            autostopResponseType.setInputPrompt("Select the parameter");
            //ToDo init a contain of the combobox
            autostopResponseType.setPropertyDataSource(ExperimentSettingsWindow.this.autostopSettingsBean.getItemProperty(
                    "parameter"));
            autostopResponseType.setValue(null);
            autostopResponseType.setEnabled(false);
            fLayout.addComponent(autostopResponseType);

            TextField txtField = new TextField("Confidence interval:");
            ExperimentSettingsWindow.this.toCommit.add(txtField);
            txtField.setRequired(true);
            txtField.setRequiredError("The field may not be empty");
            txtField.setImmediate(true); /*
             * We want to validate an input when the field lose the focus.
             */
            txtField.setWidth("100%");
//            BeanItemContainer<String> b = new BeanItemContainer<String>(String.class);
            txtField.setPropertyDataSource(ExperimentSettingsWindow.this.autostopSettingsBean.getItemProperty(
                    "confidenceInterval"));
            fLayout.addComponent(txtField);

            ComboBox comboBox = new ComboBox("Confidence probability:");
            ExperimentSettingsWindow.this.toCommit.add(comboBox);
            comboBox.setInputPrompt("Select the probability");
            int cnt =
                    ExperimentSettingsWindow.this.experimentSettings.getAutostopSettings().ALLOWED_PROBABILITIES.length;
            for (int i = 0; i < cnt; i++) {
                comboBox.addItem(
                        ExperimentSettingsWindow.this.experimentSettings.getAutostopSettings().ALLOWED_PROBABILITIES[i]);
            }
            comboBox.setRequired(true);
            comboBox.setRequiredError(ExperimentSettingsWindow.EMPTY_FIELD_NOTIFY);
            comboBox.setWidth("100%");
            comboBox.setPropertyDataSource(ExperimentSettingsWindow.this.autostopSettingsBean.getItemProperty(
                    "confidenceProbability"));
            fLayout.addComponent(comboBox);
            /*
             * Register fLayout as component, what must be disabled if check box
             * is unchecked.
             */
            checkBox.register(fLayout);
        }
    }

    private Panel createTreePanel() {
        this.selectedAggregateTree = new ComboBox();

        tree = new ModelTree(dispatcher.getModelName());
        tree.setStyleName("v-experiment-settings-tree");
        tree.setMultiSelect(true);

        fillModelTree(tree);
        TreeHandler th = new TreeHandler(tree);
        tree.addListener(th);

        selectedAggregateTree.addListener(new Property.ValueChangeListener() {

            public void valueChange(ValueChangeEvent event) {
                String val = selectedAggregateTree.getValue().toString();
                logger.debug("switch tree: " + val);
                switchTree(val);
            }
        });

        Panel p = new Panel();
        p.setWidth("200px");
        p.setHeight("100%");
        CssLayout l = new CssLayout();
        l.setWidth("100%");
        l.addComponent(selectedAggregateTree);
        selectedAggregateTree.setWidth("100%");
        selectedAggregateTree.setNewItemsAllowed(false);
        selectedAggregateTree.setNullSelectionAllowed(false);
        selectedAggregateTree.setValue(selectedAggregateTree.getItemIds().toArray()[0]);
        selectedAggregateTree.setImmediate(true);
        l.addComponent(tree);
        p.setContent(l);

        p.requestRepaintAll();
        tree.addActionHandler(th);
        return p;
    }

    private StatisticsParameters[] getStatisticParameters(String parent) {
        StatisticsParameters[] params = null;
        if (ModelTree.PLACES.equals(parent)) {
            params = new StatisticsParameters[]{
                StatisticsParameters.AVERAGE_OCCUPIED_TIME,
                StatisticsParameters.OCCUPIED_COEFFICIENT,
                StatisticsParameters.NUMBER_OF_PASSED_TOKENS};
        } else if (ModelTree.QUEUES.equals(parent)) {
            params = new StatisticsParameters[]{
                StatisticsParameters.AVERAGE_QUEUE_LENGTH,
                StatisticsParameters.NUMBER_OF_PASSED_TOKENS,
                StatisticsParameters.AVERAGE_TIME_IN_QUEUE};
        } else if (ModelTree.TRANSITIONS.equals(parent)) {
            params = new StatisticsParameters[]{
                StatisticsParameters.AVERAGE_TIME_IN_DELAY,
                StatisticsParameters.OCCUPIED_COEFFICIENT,
                StatisticsParameters.NUMBER_OF_PASSED_TOKENS};
        }
        return params;
    }

    private void refreshRespondType(String parent, AbstractField response, ComboBox responseType, boolean fixSize) {
        if (ModelTree.VARIABLES.equals(parent)) {
            responseType.setValue(null);
            responseType.setRequired(false);
            responseType.setImmediate(false);
            responseType.setEnabled(false);
            if (fixSize) {
                responseType.getParent().setVisible(false);
                response.getParent().setWidth("353px");
            }
        } else {
            responseType.setEnabled(true);
            responseType.removeAllItems();
            ExperimentSettingsWindow.this.experimentSettings.getSecondaryStatisticsSettins().setResponseType(null);
            StatisticsParameters[] params = getStatisticParameters(parent);
            if (params != null) {
                for (StatisticsParameters sp : params) {
                    responseType.addItem(sp);
                }
            }
            if (fixSize) {
                response.getParent().setWidth("253px");
                responseType.getParent().setVisible(true);
            }
            responseType.setRequired(true);
            responseType.setImmediate(true);
        }
        responseType.requestRepaint();
        response.requestRepaint();
    }

    private class TreeHandler implements Property.ValueChangeListener, Action.Handler {

        private final Tree tree;
        private final Action sActionResponse = new ShortcutAction("Set as reponse");
        private final Action sActionAutostopObject = new ShortcutAction("Set as autostop parameter");
        private final Action sActionToFactors = new ShortcutAction("Add to Factors");
        private final Action sActionToPrimary = new ShortcutAction("Primary Statistic");
        private Logger logger = LoggerFactory.getLogger(TreeHandler.class);

        private TreeHandler(Tree tree) {
            this.tree = tree;
        }

        public Action[] getActions(Object obj, Object o1) {

            Object parent = tree.getParent(obj);

            List<Action> actions = new LinkedList<Action>();
            if (parent != null) {
                if (ModelTree.QUEUES.equals(parent) || ModelTree.PLACES.equals(parent)
                        || ModelTree.TRANSITIONS.equals(parent)) {
                    actions.add(sActionToPrimary);
                    actions.add(sActionResponse);
                    actions.add(sActionAutostopObject);
                } else if (ModelTree.VARIABLES.equals(parent)) {
                    actions.add(sActionToFactors);
                    actions.add(sActionResponse);
                    actions.add(sActionAutostopObject);
                }
            }
            return actions.toArray(new Action[actions.size()]);
        }

        @Override
        public void handleAction(Action action, Object o, Object o1) {
            if (sActionResponse == action || sActionAutostopObject == action) {
                logger.debug("  set as response:");

                if (!ExperimentSettingsWindow.this.experimentSettings.getSecondaryStatisticsSettins().isEnabled()
                        && !ExperimentSettingsWindow.this.experimentSettings.getAutostopSettings().isEnabled()) {
                    return;
                }
                for (Object id : getSelectedItems()) {
                    Object parent = tree.getParent(id);
                    if (parent != null) {
                        String parentValue = parent.toString();
                        if (ModelTree.VARIABLES.equals(parentValue)
                                || ModelTree.PLACES.equals(parentValue)
                                || ModelTree.QUEUES.equals(parentValue)
                                || ModelTree.TRANSITIONS.equals(parentValue)) {
                            String prefix = "";
                            if (!dispatcher.getModelName().equals(selectedAggregateTree.getValue())) {
                                prefix = selectedAggregateTree.getValue() + ".";
                                prefix = prefix.substring(prefix.indexOf('.') + 1);
                            }
                            if (sActionResponse == action) {
                                if (ExperimentSettingsWindow.this.experimentSettings.getSecondaryStatisticsSettins().isEnabled()) {
                                    ExperimentSettingsWindow.this.response.setValue(prefix + id.toString());
                                    refreshRespondType(parentValue, ExperimentSettingsWindow.this.response, ExperimentSettingsWindow.this.responseType, true);
                                }
                            } else {
                                if (ExperimentSettingsWindow.this.experimentSettings.getAutostopSettings().isEnabled()) {
                                    ExperimentSettingsWindow.this.autostopResponse.setValue(prefix + id.toString());
                                    refreshRespondType(parentValue, ExperimentSettingsWindow.this.autostopResponse, ExperimentSettingsWindow.this.autostopResponseType, false);
                                }
                            }
                        }
                    }
                }
                return;
            }
            if (sActionToPrimary == action) {
                String prefix = "";
                if (!dispatcher.getModelName().equals(selectedAggregateTree.getValue())) {
                    prefix = selectedAggregateTree.getValue().toString();
                    prefix = prefix.substring(prefix.indexOf('.') + 1);
                    if (!"".equals(prefix)) {
                        prefix += ".";
                    }
                }
                for (String id : getSelectedItems()) {
                    if (ExperimentSettingsWindow.this.experimentSettings.getPrimaryStatisticSettings().isEnabled()) {
                        String nodeValue = id.toString();
                        if (ModelTree.PLACES.equals(nodeValue)
                                || ModelTree.QUEUES.equals(nodeValue)
                                || ModelTree.TRANSITIONS.equals(nodeValue)) {
                            Collection list = tree.getChildren(nodeValue);
                            Collection<String> l2 = new ArrayList<String>();
                            for (Object value : list) {
                                l2.add(selectedAggregateTree.getValue() + "." + value);
                            }
                            ExperimentSettingsWindow.this.statisticSubjectsTable.selectAll(l2);
                        } else {
                            String parent = (String) tree.getParent(id);
                            String parentValue = parent != null ? parent.toString() : null;
                            if (parentValue != null && (ModelTree.PLACES.equals(parentValue)
                                    || ModelTree.QUEUES.equals(parentValue)
                                    || ModelTree.TRANSITIONS.equals(parentValue)
                                    || ModelTree.AGGREGATES.equals(parentValue))) {
                                ExperimentSettingsWindow.this.statisticSubjectsTable.selectItem(
                                        prefix + id.toString());
                            }
                        }
                    }
                }
            }
            if (sActionToFactors == action) {
                for (String id : getSelectedItems()) {
                    if (ExperimentSettingsWindow.this.experimentSettings.getSecondaryStatisticsSettins().isEnabled()) {
                        if (tree.getParent(id) != null && ModelTree.VARIABLES.equals(tree.getParent(id).toString())) {
                            String prefix = "";
                            if (!dispatcher.getModelName().equals(selectedAggregateTree.getValue())) {
                                prefix = selectedAggregateTree.getValue() + ".";
                                prefix = prefix.substring(prefix.indexOf('.') + 1);
                            }
                            ExperimentSettingsWindow.this.factorsEditor.selectItem(prefix
                                    + id.toString(), dispatcher.getVariableValue(prefix + id.toString()));
                        }
                    }
                }
            }
        }

        public void valueChange(Property.ValueChangeEvent event) {
            logger.debug("value change selected: " + getSelectedItems().length);
        }

        @SuppressWarnings("unchecked")
        private String[] getSelectedItems() {
            logger.debug("    get selected items");
            Set<String> values = (Set<String>) tree.getValue();
            logger.debug("    get value=" + values);
            return (values.toArray(new String[values.size()]));
        }
    }

    private List<String> getChildList(String path) {
        List<String> list = new ArrayList<String>();

        List<String> childs = dispatcher.listAll(path, EntityClass.AGGREGATE);
        String prefix = path != null ? path + "." : "";
        for (String child : childs) {
            list.add(prefix + child);
            list.addAll(getChildList(prefix + child));
        }

        return list;
    }

    private void generateModelTreeValues(ModelTree tree, String caption, String path) {
        List<String> list;

        HashMap<EntityClass, List<String>> values = new HashMap<EntityClass, List<String>>();

        selectedAggregateTree.addItem(caption);
        list = dispatcher.listAll(path, EntityClass.PLACE);

        if (!list.isEmpty()) {
            values.put(EntityClass.PLACE, list);
        }

        list = dispatcher.listAll(path, EntityClass.QUEUE);
        if (!list.isEmpty()) {
            values.put(EntityClass.QUEUE, list);
        }

        list = dispatcher.listAll(path, EntityClass.TRANSITION);
        if (!list.isEmpty()) {
            values.put(EntityClass.TRANSITION, list);
        }

        list = dispatcher.listAllVariables(path);
        if (!list.isEmpty()) {
            values.put(EntityClass.VARIABLE, list);
        }

        selectiveTreeValues.put(caption, values);
    }

    private void fillModelTree(ModelTree tree) {
        generateModelTreeValues(tree, dispatcher.getModelName(), null);

        List<String> childs = getChildList(null);
        for (String child : childs) {
            generateModelTreeValues(tree, dispatcher.getModelName() + "." + child, child);
        }
    }

    private void switchTree(String cache) {
        String root = cache.lastIndexOf('.') > -1
                ? cache.substring(cache.lastIndexOf('.') + 1) : cache;

        HashMap<EntityClass, List<String>> values = selectiveTreeValues.get(cache);

        tree.clear();
        tree.renameItem(tree.getRoot(), root);

        if (values.containsKey(EntityClass.PLACE)) {
            List<String> ids = values.get(EntityClass.PLACE);
            for (String id : ids) {
                tree.addItem(ModelTree.PLACES, id);
            }
        }

        if (values.containsKey(EntityClass.QUEUE)) {
            List<String> ids = values.get(EntityClass.QUEUE);
            for (String id : ids) {
                tree.addItem(ModelTree.QUEUES, id);
            }
        }

        if (values.containsKey(EntityClass.TRANSITION)) {
            List<String> ids = values.get(EntityClass.TRANSITION);
            for (String id : ids) {
                tree.addItem(ModelTree.TRANSITIONS, id);
            }
        }

        if (values.containsKey(EntityClass.VARIABLE)) {
            List<String> ids = values.get(EntityClass.VARIABLE);
            for (String id : ids) {
                tree.addItem(ModelTree.VARIABLES, id);
            }
        }

        tree.removeEmptyNodes();
    }
}
