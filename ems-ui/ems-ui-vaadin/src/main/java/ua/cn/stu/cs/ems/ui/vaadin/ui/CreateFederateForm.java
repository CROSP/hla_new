package ua.cn.stu.cs.ems.ui.vaadin.ui;

import com.vaadin.data.Item;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.TextField;

/**
 *
 * @author leonid
 */
public class CreateFederateForm extends AbstractForm {
    
    private static final String WIDTH = "400px";
    private static final String CAPTION = "Create federate";

    public CreateFederateForm() {
        super();
        setVisibleItemProperties(new Object[]{"federation", "name", "timeManagment"});
    }

    private static class CreateFederateSettingsFiledFactory extends DefaultFieldFactory {

        @Override
        public Field createField(Item item, Object propertyId, Component uiContext) {
            Field f = super.createField(item, propertyId, uiContext);
            if ("federate".equals(propertyId)) {
                CheckBox cb = (CheckBox) f;
                cb.setCaption("Choose federation");
            }
            if ("name".equals(propertyId)) {
                TextField tf = (TextField) f;
                tf.setCaption("Name");
            }
            return f;
        }
    }
}
