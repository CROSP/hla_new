package ua.cn.stu.cs.ems.ui.vaadin.ui.modules;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.cn.stu.cs.ems.core.experiment.ExperimentReport;
import ua.cn.stu.cs.ems.ui.vaadin.ExperimentManager;
import ua.cn.stu.cs.ems.ui.vaadin.PropertiesListener;
import ua.cn.stu.cs.ems.ui.vaadin.WidgetManager;
import ua.cn.stu.cs.ems.ui.vaadin.ui.components.ActionHandler;
import ua.cn.stu.cs.ems.ui.vaadin.ui.experiment.DistributedExperimentSettingsWindow;
import ua.cn.stu.cs.ems.ui.vaadin.ui.experiment.ExperimentProgressWindow;
import ua.cn.stu.cs.ems.ui.vaadin.ui.experiment.ExperimentSettings;
import ua.cn.stu.cs.ems.ui.vaadin.ui.experiment.report.ExperimentReportWindow;
import ua.cn.stu.cs.ems.ui.vaadin.ui.hla.*;

/**
 *
 * @author leonid
 */
public class HlaManagmentModule implements EmsModule {

    final static Logger logger = LoggerFactory.getLogger(HlaManagmentModule.class);
    private static final String MENU_PATH = "HLA->";
    private WidgetManager wm;
    private HlaDispatcher dispatcher;

    public HlaManagmentModule() {
        logger.debug("RTI_HOME=" + System.getProperty("rti.home"));
    }

    public void init(WidgetManager wm) {
        this.wm = wm;
        dispatcher = new HlaDispatcher(wm);
        registerMenu();
    }

    private void registerMenu() {
        wm.getDefaultMenu().setItem(MENU_PATH + "Federation->Create", new ActionHandler() {

            public void handle() {
                wm.getWindow().addWindow(new ActionsFederationWindow("Create Federation", "400px",
                        "Create", new PropertiesListener<String>() {

                    public void propertiesSet(String props) {
                        dispatcher.createFederation(props);
                    }
                }));
            }
        });

//        wm.getDefaultMenu().setItem(MENU_PATH + "Federation->Destroy", new ActionHandler() {
//
//            public void handle() {
//                wm.getWindow().addWindow(new ActionsFederationWindow("Destroy Federation", "400px",
//                        "Destroy", dispatcher.listFederations(), new PropertiesListener<String>() {
//
//                    public void propertiesSet(String props) {
//                        dispatcher.destroy(props);
//                    }
//                }));
//            }
//        });

//        wm.getDefaultMenu().setItem(MENU_PATH + "Federation->Run", new ActionHandler() {
//
//            public void handle() {
//                wm.getWindow().addWindow(new ActionsFederationWindow("Run Federation", "400px",
//                        "Run", dispatcher.listFederations(), new PropertiesListener<String>() {
//
//                    public void propertiesSet(String props) {
//                        dispatcher.start(props);
//                    }
//                }));
//            }
//        });

//        wm.getDefaultMenu().setItem(MENU_PATH + "Federation->Stop", new ActionHandler() {
//
//            public void handle() {
//                wm.getWindow().addWindow(new ActionsFederationWindow("Stop Federation", "400px",
//                        "Stop", dispatcher.listFederations(), new PropertiesListener<String>() {
//
//                    public void propertiesSet(String props) {
//                        dispatcher.stop(props);
//                    }
//                }));
//            }
//        });

        wm.getDefaultMenu().setItem(MENU_PATH + "Federate->Create", new ActionHandler() {

            public void handle() {
                wm.getWindow().addWindow(new CreateFederateWindow(
                        "Create Federate", "700px", wm, dispatcher));
            }
        });

        wm.getDefaultMenu().setItem("Model->Start Distributed Experiment", new ActionHandler() {

            public void handle() {
                wm.getWindow().addWindow(new ChooseFederationWindow(
                        "Choose Federation", "400px", wm, new PropertiesListener() {

                    public void propertiesSet(Object props) {
                        wm.getWindow().addWindow(new DistributedExperimentSettingsWindow(
                                wm.getExperimentSettings(), dispatcher, new PropertiesListener<ExperimentSettings>() {

                            public void propertiesSet(ExperimentSettings settings) {
                                ExperimentManager experimentManager = dispatcher.createExperimentManager(settings);
                                HlaExperimentProgressWindow experimentProgressWindow =
                                        new HlaExperimentProgressWindow(
                                        experimentManager,
                                        wm.getIcePusher(),
                                        new HlaExperimentWindowListener(experimentManager));
                                wm.getWindow().addWindow(experimentProgressWindow);
                                experimentManager.start();
                            }
                        }, false, wm, (String) props));
                    }
                }));
            }
        });
    }

    private class HlaExperimentWindowListener implements ExperimentProgressWindow.ExperimentWindowListener {

        private final ExperimentManager experimentManager;

        private HlaExperimentWindowListener(ExperimentManager experimentManager) {
            this.experimentManager = experimentManager;
        }

        public void stopExperiment() {
            experimentManager.stop();
        }

        public void showReport(ExperimentReport experimentReport) {
            wm.getWindow().addWindow(new ExperimentReportWindow(experimentReport, wm.getExperimentSettings()));
        }
    }
}
