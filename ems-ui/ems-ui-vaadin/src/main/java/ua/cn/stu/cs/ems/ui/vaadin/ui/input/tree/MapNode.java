
package ua.cn.stu.cs.ems.ui.vaadin.ui.input.tree;

import java.util.*;
import java.util.Map.Entry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 *
 * @author slava
 */

public class MapNode {
    	private Object data;
        private MapNode parent = null;
	private Map<Object,MapNode> mapSon = null;
        final static Logger logger = LoggerFactory.getLogger(MapNode.class);

   
    public MapNode(Object data) {
    	this.data = data;
    	this.mapSon = new LinkedHashMap<Object,MapNode>();
    }
    
    
    public MapNode() {
    	this.data = null;
    	this.mapSon = null;
    }
    
    public MapNode addSonToFirst(Object data) {
        MapNode newSon = new MapNode(data);
        mapSon.put(data,newSon);
        mapSon.get(data).setParent(this);
        return mapSon.get(data);
    }

    public void setParent(MapNode parent) {
        this.parent = parent;
    }
    
    public MapNode getParent() {
        return parent;
    }
	
    public Enumeration<MapNode> children() {
            return new Enumeration<MapNode> () {
        Set<Entry<Object,MapNode>> sons = mapSon.entrySet();
                    Iterator<Entry<Object, MapNode>> iter = sons.iterator();
                     @Override
                    public boolean hasMoreElements() {
                            return iter.hasNext();
                    }
                    @Override
                    public MapNode nextElement() {
                            Entry<Object,MapNode> newEl = iter.next();
                            return newEl.getValue();
                    }				
       };
    }

    public MapNode cutNodeMain(int pos) {
    MapNode cutNode = new MapNode();
    cutNode = (MapNode) getChildAt(pos-1);
    mapSon.remove(cutNode.data);
            return cutNode;
    }

    public boolean deleteElement(int pos) {
    MapNode cutNode = new MapNode();
    cutNode = (MapNode)getChildAt(pos-1);
    mapSon.remove(cutNode.data);

    return true;		
    }

    public Object getData() {
            return data;
    }

    public void setData(Object data) {
            this.data = data;
    }

    @Override
    public String toString() {
            return data.toString();
    }

    public int getChildCount() {
            int count = 0;
            Enumeration<MapNode> c =  children();
            while (c.hasMoreElements()) {
                    c.nextElement();
                    count++;
            }
            return count;
    }


    public boolean isLeaf() {
            return getChildCount() == 0;
    }

    public MapNode getChildAt(int index) {
            Enumeration<MapNode> c = children();
            MapNode abstractNode = null;
            int count = 0;
            while (c.hasMoreElements()) {
                    abstractNode = (MapNode) c.nextElement();
                    if (index == count)
                            return abstractNode;
                    count++;
            }
            return null;
    }

    public int getIndexOfChild(MapNode child) {
            Enumeration<MapNode> c = children();				
            MapNode abstractNode = null;
            int count = 0;
            while (c.hasMoreElements()) {
                    abstractNode = (MapNode) c.nextElement();
                    count++;
                    if (abstractNode.toString().compareTo(child.toString()) == 0)
                            return count;
            }
            return 0;
    }	

    public MapNode FindChild(Object child) {
            Enumeration<MapNode> c = children();
            MapNode abstractNode = null;

            while (c.hasMoreElements()) {
                    abstractNode = (MapNode) c.nextElement();
                    if (abstractNode.toString().compareTo(child.toString()) == 0)
                            return abstractNode;
            }
            return null;
    }

    public boolean delSon(Object data) {
        MapNode delEl = FindChild(data);	
            int pos = getIndexOfChild(delEl);
            return deleteElement(pos);
    }

    public MapNode cutNode(Object data) {
            MapNode delEl = FindChild(data);	
            int pos = getIndexOfChild(delEl);
            return cutNodeMain(pos);


    }

}

