package ua.cn.stu.cs.ems.ui.vaadin.ui.input.table;

import com.vaadin.ui.Field;
import ua.cn.stu.cs.ems.core.utils.Pair;

/**
 * @author n0weak
 */
public interface FieldCreator<T> {

    /**
     * Creates field suitable for supplied parameters.
     *
     * @param item       item which property is being processed
     * @param propertyId property of the field being created
     * @return pair which contains created field and boolean flag which indicates whether field needs additional initialization
     */
    Pair<Field, Boolean> createField(T item, Object propertyId);
}
