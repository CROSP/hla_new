package ua.cn.stu.cs.ems.ui.vaadin.ui.components;

import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Panel;

public class StatusBar extends Panel {
	
	public StatusBar() {
		super();
		setStyleName("status_bar");
		setWidth("100%");
		setHeight("26px");
		setScrollable(false);
		setContent(new CssLayout());
	}

}
