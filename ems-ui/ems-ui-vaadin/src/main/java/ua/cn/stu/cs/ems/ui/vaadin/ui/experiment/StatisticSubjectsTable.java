package ua.cn.stu.cs.ems.ui.vaadin.ui.experiment;

import com.vaadin.data.Validator;
import com.vaadin.data.Validator.InvalidValueException;
import com.vaadin.ui.AbstractField;
import com.vaadin.ui.TextField;
import ua.cn.stu.cs.ems.ui.vaadin.EntityClass;
import ua.cn.stu.cs.ems.ui.vaadin.ui.input.table.CachingFieldCreator;
import ua.cn.stu.cs.ems.ui.vaadin.ui.input.table.ConcurrentFieldCreator;
import ua.cn.stu.cs.ems.ui.vaadin.ui.input.table.Valuable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import ua.cn.stu.cs.ems.ui.vaadin.ui.input.table.CachingInputTable;

/**
 * @author n0weak
 */
class StatisticSubjectsTable extends CachingInputTable<StatisticSubjectsTable.SubjectEntry> {

    public static final String[] COLUMN_IDS = {"id", ""};
    private final Collection<SubjectEntry> subjectEntries = new ArrayList<SubjectEntry>();
    private final Set<String> entities;
    private final Collection<String> places;
    private final Collection<String> queues;
    private final Collection<String> transitions;
    private final SubjectEntryFieldCreator fieldCreator = new SubjectEntryFieldCreator();

    public StatisticSubjectsTable(String width, String height, String caption, 
            Collection<String> places, Collection<String> queues, 
            Collection<String> transitions) {

        super(SubjectEntry.class, caption);
        setWidth(width);
        setHeight(height);
        expandAllowed = false;
        
        this.transitions = transitions;
        this.places = places;
        this.queues = queues;
        Set<String> entities = new HashSet<String>(places.size() + queues.size() + transitions.size());
        entities.addAll(places);
        entities.addAll(queues);
        entities.addAll(transitions);
        this.entities = entities;

        initContainer(subjectEntries);
        initFactory(fieldCreator);
        setVisibleColumns(COLUMN_IDS[0], COLUMN_IDS[1]);

        addValidator(new Validator() {
            public void validate(Object o) throws InvalidValueException {
                if (!isValid(o)) {
                    throw new InvalidValueException("At least one entity must be chosen!");
                }
            }

            public boolean isValid(Object o) {
                return getInnerList().size() > 1; //the list always contains at least one empty stub item
            }
        });
    }

    public void selectAll(EntityClass entityClass) {
        switch (entityClass) {
            case QUEUE:
                selectAll(queues);
                break;
            case PLACE:
                selectAll(places);
                break;
            case TRANSITION:
                selectAll(transitions);
                break;
        }
    }


    public Set<String> getSelectedEntities() {
        Set<String> set = new HashSet<String>();
        for (SubjectEntry entry : subjectEntries) {
            set.add(entry.getId());
        }
        return set;
    }

    @Override
    public CachingFieldCreator<SubjectEntry> getFieldCreator() {
        return fieldCreator;
    }


    public void selectAll(Collection<String> data) {
        for (String entity : data) {
            this.selectItem(entity);
        }
    }
    
    public void selectItem(String id) {
        for (SubjectEntry sel: getInnerList()) {
            if (sel != null && sel.getId() != null && sel.getId().equals(id)) {
                return; // we dont need identical IDs
            };
        }
        expandAllowed = true;
        AbstractField f = getFieldCreator().getField(COLUMN_IDS[0], getLast());
        if (null != f) {
            f.setValue(id);
        } else {
            // throw exception!!!!
        }
    }

    @SuppressWarnings("unused")
    public static class SubjectEntry implements Valuable<SubjectEntry> {
        private String id;

        public SubjectEntry() {
            id = "";
        }

        public SubjectEntry(String id) {
            this.id = id;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public SubjectEntry copy() {
            return new SubjectEntry(getId());
        }

        public String getValue() {
            return id;
        }
    }

    private class SubjectEntryFieldCreator extends ConcurrentFieldCreator<SubjectEntry> {

        public SubjectEntryFieldCreator() {
            super(COLUMN_IDS[0]);
        }

        public AbstractField createNewField(final SubjectEntry item, Object propertyId) {
//            AbstractField f = new ComboBox();
            AbstractField f = new TextField();
            f.setImmediate(true);
            f.setWidth("100%");
            if (COLUMN_IDS[0].equals(propertyId)) {
//                f.addValidator(new Validator() {
//
//                    public void validate(Object value) throws InvalidValueException {
//                        if (!isValid(value)) {
//                            throw new InvalidValueException("Invalid value: not an entity or has been dublicate");
//                        }
//                    }
//
//                    public boolean isValid(Object value) {
//                        if ("".equals(value)) {
//                            return true;
//                        }
//                        if (!entities.contains(value)) {
//                            return false;
//                        }
//                        int count = 0;
//                        for (SubjectEntry sel: getInnerList()) {
//                            if (sel != null && sel.getId() != null && sel.getId().equals(value)) {
//                                count++; // we dont need identical IDs
//                            };
//                        }
//                        return count < 2;
//                    }
//                });
            }
            return f;
        }
    }

}


