package ua.cn.stu.cs.ems.ui.vaadin.ui.components;

import ua.cn.stu.cs.ems.ui.vaadin.WidgetManager;
import ua.cn.stu.cs.ems.ui.vaadin.ui.components.MainContent.Position;

import com.vaadin.addon.chameleon.ChameleonTheme;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.NativeButton;
import com.vaadin.ui.Panel;
import java.util.Collection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.cn.stu.cs.ems.core.aggregates.Aggregate;
import ua.cn.stu.cs.ems.core.utils.JessUtils;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.EntityType;

public class AgragatesPanel extends Panel implements RegisteredComponent {

    final static Logger logger = LoggerFactory.getLogger(AgragatesPanel.class);
    public static final String CID = "agregates_panel";
    private static final String caption = "Aggregate";
    private WidgetManager wm;

    public AgragatesPanel() {
        super();
        setStyleName(ChameleonTheme.ACCORDION_OPAQUE);
        addStyleName("InstrumentsPanel_style");
        CssLayout layout = new CssLayout();
        layout.setStyleName("sidebar-menu");
        layout.addStyleName("layout_InstrumentsPanel");
    }

    public String getTabCaption() {
        return caption;
    }

    private Button createModeSwitcher(final String aggId) {
        Button b = new NativeButton(aggId);
        b.addListener(new Button.ClickListener() {

            private final String id = aggId;

            public void buttonClick(Button.ClickEvent clickEvent) {
                MainContent.WsRec activeWs = wm.getMainContent().getActiveWorkspace();
                if (activeWs == null) {
                    return;
                }
                Aggregate ad = wm.getSaveManager().getAggregateType(id).createInstance(id);
                Collection<String> inputs = JessUtils.getAggregateInputs(ad);
                Collection<String> outputs = JessUtils.getAggregateOutputs(ad);
                
                activeWs.getUidGenerator().registerCustomId(id);
                EntityType type = EntityType.AGGREGATE;
                type.setEntityConnectorsNames(inputs.toArray(new String[inputs.size()]), outputs.toArray(new String[outputs.size()]));
                activeWs.getJessCanvas().toggleCreationMode(type,
                        activeWs.getUidGenerator().nextCustomId(id));
            }
        });
        wm.getModeSwitchers().addButton(b);
        return b;
    }

    public void init(WidgetManager wm) {
        this.wm = wm;

        CssLayout layout = new CssLayout();
        layout.setStyleName("sidebar-menu");
        layout.addStyleName("layout_InstrumentsPanel");

        Collection<String> list = wm.getSaveManager().listAllAggregateTypes();
        if (list != null) {
            for (String modelName : list) {
                Button b = createModeSwitcher(modelName);
                if (null != b) {
                    layout.addComponent(b);
                }
            }
        }

        setContent(layout);
    }

    public void refresh() {
        init(wm);
    }

    public void registerMenu() {
    }

    public Position getDefaultPosition() {
        return Position.RIGHT;
    }

    public String getCid() {
        return CID;
    }
}