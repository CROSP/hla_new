package ua.cn.stu.cs.ems.ui.vaadin.ui.experiment.report;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.terminal.UserError;
import com.vaadin.ui.Layout.MarginInfo;
import com.vaadin.ui.*;
import java.util.*;
import ua.cn.stu.cs.ems.core.experiment.ExperimentReport;
import ua.cn.stu.cs.ems.core.experiment.ReportNormilizer;
import ua.cn.stu.cs.ems.ui.vaadin.ui.experiment.ExperimentSettings;

/**
 *
 * @author proger
 */
public class ExperimentReportWindow extends Window {

    private static final String CAPTION = "Experiment report";
    private static final String STATISTICS_TAB_CAPTION = "Statistics";
    private static final String STATISTICS_STORE_TAB_CAPTION = "Save";
    private static final String PRIMARY_STATISTICS_PANEL_CAPTION = "Primary statistics";
    private static final String SECONDARY_STATISTICS_PANEL_CAPTION = "Secondary statistics";
    private ExperimentReport normalizedReport;
    private ExperimentReport initialReport;
    private TabSheet tabSheet;
    private Panel primaryStatisticsPanel;
    private Panel secondaryStatisticsPanel;
    private VerticalLayout primaryStatisticsSelectsLayout;
    private VerticalLayout secondaryStatisticsSelectsLayout;
    private Select primaryStatisticsComponentName;
    private Select primaryStatisticsFactorValue;
    private Select primaryStatisticsOutputType;
    private Button showPrimaryStatisticsButton;
    private HorizontalLayout primaryStatisticsTypeLayout;
    private Select secondaryStatisticsOutputType;
    private Button showSecondaryStatisticsButton;
    private Button closeButton;
    private ExperimentSettings settings;

    public ExperimentReportWindow(ExperimentReport theExperimentReport, ExperimentSettings expSettings) {
        super(CAPTION);

        initialReport = theExperimentReport;
        normalizedReport = ReportNormilizer.normilizeReport(theExperimentReport);
        settings = expSettings;

        setModal(true);
        setSizeUndefined();
        setResizable(false);

        VerticalLayout layout = new VerticalLayout();
        setContent(layout);


        layout.setSpacing(true);
        MarginInfo margin = new MarginInfo(true);

        layout.setMargin(margin);

        layout.addComponent(createTabSheet());
        layout.addComponent(createCloseButton());


        layout.setComponentAlignment(tabSheet, Alignment.MIDDLE_CENTER);
        layout.setComponentAlignment(closeButton, Alignment.BOTTOM_LEFT);
        layout.setSizeUndefined();
    }

    private TabSheet createTabSheet() {
        Component statisticsTab = createStatisticsTabContent();

        VerticalLayout savingTab = new VerticalLayout();
        savingTab.addComponent(new Label("Save statistics"));

        tabSheet = new TabSheet();
        tabSheet.addTab(statisticsTab, STATISTICS_TAB_CAPTION, null);
        tabSheet.addTab(savingTab, STATISTICS_STORE_TAB_CAPTION, null);

        tabSheet.setWidth("500px");
        tabSheet.setHeight("450px");

        return tabSheet;
    }

    private Component createStatisticsTabContent() {
        VerticalLayout vLayout = new VerticalLayout();
        vLayout.setSpacing(true);

        Component primaryStatistics = creatPrimaryStatisticsPanel();
        Component secondaryStatistics = createSecondaryStatisticsPanel();

        vLayout.addComponent(primaryStatistics);
        vLayout.addComponent(secondaryStatistics);

        return vLayout;
    }

    private Component creatPrimaryStatisticsPanel() {
        primaryStatisticsPanel = new Panel(PRIMARY_STATISTICS_PANEL_CAPTION);

        primaryStatisticsSelectsLayout = new VerticalLayout();

        primaryStatisticsComponentName = new Select("Component name", normalizedReport.getStatisticsObjectsNames());
        primaryStatisticsComponentName.setWidth("300px");
        primaryStatisticsComponentName.setRequired(true);

        primaryStatisticsFactorValue = new Select("Factor value", normalizedReport.getFactorValues());
        primaryStatisticsFactorValue.setRequired(true);

        primaryStatisticsOutputType = new Select("Output type", getOutputTypes());
//        primaryStatisticsOutputType.addItem(OutputType.VARIABLES_TABLE);
        primaryStatisticsOutputType.setRequired(true);
        primaryStatisticsOutputType.setImmediate(true);


        primaryStatisticsOutputType.addListener(new ValueChangeListener() {

            //private boolean inserted = false;

            public void valueChange(ValueChangeEvent event) {
                if (primaryStatisticsOutputType.getValue() == OutputType.GRAPH) {
//                    if (!inserted) {
//                        insertItems(primaryStatisticsComponentName, initialReport.getVariablesState().getChildNames());
//                        inserted = true;
//                    }
                    if (factorsValuesWereSet()) {
                        attachFactorsSelect();
                    }
                    attachPrimaryStatisticsComponentName();
                } else if (primaryStatisticsOutputType.getValue() == OutputType.HISTO) {
//                    if (inserted) {
//                        removeItems(primaryStatisticsComponentName, initialReport.getVariablesState().getChildNames());
//                        inserted = false;
//                    }
                    if (factorsValuesWereSet()) {
                        attachFactorsSelect();
                    }
                    attachPrimaryStatisticsComponentName();
                } else {
                    detachFactorsSelect();
                    detachPrimaryStatisticsComponentName();
                }
            }
        });

        primaryStatisticsTypeLayout = new HorizontalLayout();
        primaryStatisticsTypeLayout.addComponent(primaryStatisticsOutputType);

        primaryStatisticsSelectsLayout.addComponent(primaryStatisticsOutputType);
        primaryStatisticsSelectsLayout.addComponent(primaryStatisticsTypeLayout);

        showPrimaryStatisticsButton = new Button("Show", this, "showPrimaryStatistics");

        primaryStatisticsPanel.addComponent(primaryStatisticsSelectsLayout);
        primaryStatisticsPanel.addComponent(showPrimaryStatisticsButton);

        if (!normalizedReport.isPrimaryStatisticsCollected()) {
            primaryStatisticsPanel.setEnabled(false);
        }

        return primaryStatisticsPanel;
    }

    private void insertItems(Select dst, Collection<String> values) {
        for (String item : values) {
            dst.addItem(item);
        }
    }

    private void removeItems(Select dst, Collection<String> values) {
        for (String item : values) {
            dst.removeItem(item);
        }
    }

    private void attachFactorsSelect() {
        primaryStatisticsSelectsLayout.addComponent(primaryStatisticsFactorValue);
    }

    private void detachFactorsSelect() {
        if (primaryStatisticsFactorValue != null) {
            primaryStatisticsSelectsLayout.removeComponent(primaryStatisticsFactorValue);
        }
    }

    private void attachPrimaryStatisticsComponentName() {
        primaryStatisticsSelectsLayout.addComponent(primaryStatisticsComponentName);
    }

    private void detachPrimaryStatisticsComponentName() {
        if (primaryStatisticsComponentName != null) {
            primaryStatisticsSelectsLayout.removeComponent(primaryStatisticsComponentName);
        }
    }

    private Component createSecondaryStatisticsPanel() {
        secondaryStatisticsPanel = new Panel(SECONDARY_STATISTICS_PANEL_CAPTION);

        secondaryStatisticsOutputType = new Select("Output type", getOutputTypes());
        secondaryStatisticsOutputType.setRequired(true);
        secondaryStatisticsOutputType.setImmediate(true);

        showSecondaryStatisticsButton = new Button("Show", this, "showSecondaryStatistics");

        secondaryStatisticsSelectsLayout = new VerticalLayout();
        secondaryStatisticsSelectsLayout.addComponent(secondaryStatisticsOutputType);

        secondaryStatisticsPanel.addComponent(secondaryStatisticsSelectsLayout);
        secondaryStatisticsPanel.addComponent(showSecondaryStatisticsButton);

        if (!initialReport.isSecondaryStatisticsCollected()) {
            secondaryStatisticsPanel.setEnabled(false);
        }

        return secondaryStatisticsPanel;
    }

    private Button createCloseButton() {
        closeButton = new Button("Close", this, "closeButtonClick");

        return closeButton;
    }

    public void showPrimaryStatistics(Button.ClickEvent event) {
        if (primaryStatisticsParametersAreValid()) {
            if (primaryStatisticsOutputType.getValue() == OutputType.GRAPH
                    || primaryStatisticsOutputType.getValue() == OutputType.HISTO) {
                String objectName = (String) primaryStatisticsComponentName.getValue();
                Double factorValue = (Double) primaryStatisticsFactorValue.getValue();

                GraphicResult graphicResult = null;
                HistoResult histoResult = null;

//                if (primaryStatisticsOutputType.getValue() == OutputType.GRAPH
//                        && initialReport.getVariablesState().getChildNames().contains(objectName)) {
//                    //graphicResult = ReportConverter.primaryVariableStaticticstoGrapthResult(initialReport, objectName);
//                } else 
                if (factorsValuesWereSet()) {
                    if (primaryStatisticsOutputType.getValue() == OutputType.GRAPH) {
                        graphicResult =
                                ReportConverter.primaryStatisticsToGraphResult(normalizedReport, objectName, factorValue);
                    } else {
                        histoResult =
                                ReportConverter.primaryStatisticsToHistoResult(normalizedReport, objectName, factorValue);
                    }
                } else {
                    if (primaryStatisticsOutputType.getValue() == OutputType.GRAPH) {
                        graphicResult =
                                ReportConverter.primaryStatisticsToGraphResult(normalizedReport, objectName);

                    } else {
                        histoResult =
                                ReportConverter.primaryStatisticsToHistoResult(normalizedReport, objectName);
                    }
                }

                GraphicResultOutputer outputer = new SeveralGraphicsOutputer();
                GraphicStatisticsResultWindow statisticsResult;

                if (graphicResult != null) {
                    statisticsResult = new GraphicStatisticsResultWindow(graphicResult, outputer);
                } else {
                    statisticsResult = new GraphicStatisticsResultWindow(histoResult, outputer);
                }

                showOutputWindow(statisticsResult);
            } else if (primaryStatisticsOutputType.getValue() == OutputType.TABLE) {
                HashMap<String, TableResult> tableResult = ReportConverter.primaryStatisticsToTableResult(initialReport);
                // ArrayList<Table> tableVariables = ReportConverter.primaryStatisticToVariablesTables(initialReport);
                if (tableResult != null) {
                    TableStatisticsResultWindow statisticsResult = new TableStatisticsResultWindow(tableResult, null, settings);
                    showOutputWindow(statisticsResult);
                }
            }
        }
    }

    public void showSecondaryStatistics(Button.ClickEvent event) {
        if (secondaryStatisticsParametersAreValid()) {
            if (secondaryStatisticsOutputType.getValue() == OutputType.GRAPH) {
                GraphicResult graphicResult = ReportConverter.secondaryStatisticsToGraphicResult(initialReport);
                NameGraphicOutputerPair pair = new NameGraphicOutputerPair("Graph", new SeveralGraphicsOutputer());
                GraphicResultOutputer outputer = pair.getGraphicResultOutputer();

                GraphicStatisticsResultWindow statisticsResult = new GraphicStatisticsResultWindow(graphicResult, outputer);
                showOutputWindow(statisticsResult);
            } else if (secondaryStatisticsOutputType.getValue() == OutputType.HISTO) {
                HistoResult histoResult = ReportConverter.secondaryStatisticsToHistoResult(initialReport);
                NameGraphicOutputerPair pair = new NameGraphicOutputerPair("Histo", new SeveralGraphicsOutputer());
                GraphicResultOutputer outputer = pair.getGraphicResultOutputer();

                GraphicStatisticsResultWindow statisticsResult = new GraphicStatisticsResultWindow(histoResult, outputer);
                showOutputWindow(statisticsResult);
            } else if (secondaryStatisticsOutputType.getValue() == OutputType.TABLE) {
                TableResult tableResult = ReportConverter.secondaryStatisticsToTableResult(initialReport);
                if (tableResult != null) {
                    TableStatisticsResultWindow statisticsResult = new TableStatisticsResultWindow(tableResult, settings);
                    showOutputWindow(statisticsResult);
                }
            }
        }
    }

    private boolean primaryStatisticsParametersAreValid() {
        return statisticsPanelIsValid(primaryStatisticsPanel);
    }

    private boolean secondaryStatisticsParametersAreValid() {
        return statisticsPanelIsValid(secondaryStatisticsPanel);
    }

    private boolean statisticsPanelIsValid(Panel panel) {
        Iterator<Component> iter = panel.getComponentIterator();

        while (iter.hasNext()) {
            Component component = iter.next();

            if (component instanceof Select) {
                Select select = (Select) component;

                if (select.isRequired() && select.getValue() == null) {
                    select.setComponentError(new UserError("Must be set"));
                    return false;
                } else {
                    select.setComponentError(null);
                }
            }
        }

        return true;
    }

    public void closeButtonClick(Button.ClickEvent event) {
        getParent().removeWindow(this);
    }

    private void showOutputWindow(Window window) {
        getParent().addWindow(window);
        window.setVisible(true);
    }

    private Collection<?> getOutputTypes() {
        return new ArrayList<OutputType>() {

            {
                add(OutputType.GRAPH);
                add(OutputType.HISTO);
                add(OutputType.TABLE);
            }
        };
    }

    private boolean factorsValuesWereSet() {
        return !(initialReport.getFactorValues().isEmpty());
    }

    enum OutputType {

        TABLE("Table"),
        GRAPH("Graph"),
        HISTO("Histogram");
//        VARIABLES_TABLE("Variables table");
        private String name;

        private OutputType(String theName) {
            name = theName;
        }

        @Override
        public final String toString() {
            return name;
        }
    }

    private class NameGraphicOutputerPair {

        private String name;
        private GraphicResultOutputer graphicResultOutputer;

        public NameGraphicOutputerPair(String theName, GraphicResultOutputer theGraphicResultOutputer) {
            name = theName;
            graphicResultOutputer = theGraphicResultOutputer;
        }

        public String getName() {
            return name;
        }

        public GraphicResultOutputer getGraphicResultOutputer() {
            return graphicResultOutputer;
        }

        @Override
        public String toString() {
            return name;
        }
    }
}