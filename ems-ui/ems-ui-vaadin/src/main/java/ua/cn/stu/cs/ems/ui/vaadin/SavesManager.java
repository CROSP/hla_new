package ua.cn.stu.cs.ems.ui.vaadin;

import java.io.ByteArrayInputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.cn.stu.cs.ems.core.aggregates.AggregateDefinition;
import ua.cn.stu.cs.ems.core.aggregates.AggregateRegistry;

import java.io.File;
import java.io.InputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import org.jdom.Document;
import org.jdom.input.SAXBuilder;
import ua.cn.stu.cs.ems.core.aggregates.Aggregate;
import ua.cn.stu.cs.ems.core.aggregates.AggregateDefinitionReference;
import ua.cn.stu.cs.ems.core.utils.JessUtils;
import ua.cn.stu.cs.ems.core.xml.AggregateBuilder;
import ua.cn.stu.cs.ems.core.xml.AggregateTypesReader;
import ua.cn.stu.cs.ems.ui.vaadin.ui.OpenDialog;
import ua.cn.stu.cs.ems.ui.vaadin.ui.SaveDialog.SaveRecord;
import ua.cn.stu.cs.ems.ui.vaadin.ui.components.MainContent.WorkspaceType;
import ua.cn.stu.cs.ems.ui.vaadin.ui.components.MainContent.WsRec;
import ua.cn.stu.cs.ems.ui.vaadin.ui.components.TreeModelView;
import ua.cn.stu.cs.ems.ui.vaadin.utils.PropertiesReader;
import ua.cn.stu.cs.ems.ui.vaadin.utils.db.AggregateEntity;

/**
 * @author n0weak
 */
public class SavesManager {
// ------------------------------ FIELDS ------------------------------

    final static Logger logger = LoggerFactory.getLogger(SavesManager.class);
    private final UIManager uiManager;
    private final ErrorRenderer errorRenderer;
    private final FileManager fileManager;
    private final WidgetManager wm;

// --------------------------- CONSTRUCTORS ---------------------------
    public SavesManager(UIManager uiManager, ErrorRenderer errorRenderer,
            FileManager fileManager, WidgetManager wm) {
        this.uiManager = uiManager;
        this.errorRenderer = errorRenderer;
        this.fileManager = fileManager;
        this.wm = wm;
    }

// --------------------- GETTER / SETTER METHODS ---------------------
// ------------------------ INTERFACE METHODS ------------------------
// -------------------------- OTHER METHODS --------------------------
    public void load() {
        uiManager.showOpenDialog(listAllModels(), listAllAggregateTypes(),
                new PropertiesListener<OpenDialog.OpenRecord>() {

                    public void propertiesSet(OpenDialog.OpenRecord props) {
                        doLoad(props);
                    }
                });
    }

    private void doLoad(OpenDialog.OpenRecord data) {
        AggregateRegistry ar = getAggregateRegestryCopy();
        AggregateDefinition ad =
                data.getFileType() == FileManager.FileType.MODEL
                ? getModel(data.getFileName(), ar) : ar.getAggregateDefinition(data.getFileName());

        wm.getMainContent().createNewWorkspace(ad.getName(),
                data.getFileType() == FileManager.FileType.MODEL
                ? WorkspaceType.MODEL : WorkspaceType.AGG_TYPE);

        WsRec ws = wm.getMainContent().getActiveWorkspace();
        ws.setRegistry(ar);
        ws.getDispatcher().getLoader().onLoad(ad);
        ws.setSaveNeeded(false);
    }

    public void onSave(final WsRec workspace, final PropertiesListener afterHandler) {
        logger.debug("on save: save need? " + workspace.isSaveNeeded() + " childs save need? " + workspace.isChildsSaveNeed());
        if (workspace.isSaveNeeded() || workspace.isChildsSaveNeed()) {
            if (workspace.getWsType() == WorkspaceType.MODEL) {
                if (aggregateExist(workspace.getName(), AggregatesManager.AggregateType.MODEL)) { // EXIST
                    saveModel(workspace.getDispatcher().getModelName(),
                            workspace.getDispatcher().getRootAggregate());
                    workspace.resetSaveNeed();

                    if (afterHandler != null) {
                        afterHandler.propertiesSet(null);
                    }
                } // HAS FILE
                else { // not exist
                    uiManager.showSaveDialog(workspace.getName(),
                            FileManager.FileType.MODEL, new PropertiesListener<SaveRecord>() {

                        public void propertiesSet(SaveRecord props) {
                            saveModel(props.getFileName(),
                                    workspace.getDispatcher().getRootAggregate());
//                            workspace.setFile(processingFile);
                            workspace.resetSaveNeed();
                            if (afterHandler != null) {
                                afterHandler.propertiesSet(null);
                            }
                        }
                    });
                }
            } // IF WORKSPACE == Model
            else {
                if (workspace.isParent()) {
                    saveAggregateType(workspace.getDispatcher().getModelName(),
                            workspace.getDispatcher().getRootAggregate());
                    workspace.resetSaveNeed();
                    if (afterHandler != null) {
                        afterHandler.propertiesSet(null);
                    }
                } // isParent
                else {
                    WsRec parent = workspace;
                    do {
                        parent = parent.getParent();
                    } while (parent.getParent() != null);

                    if (parent.getWsType() == WorkspaceType.MODEL) {
                        onSave(parent, afterHandler);
                        return;
                    } else {
                        saveAggregateType(workspace.getDispatcher().getModelName(),
                                workspace.getDispatcher().getRootAggregate());
                        if (afterHandler != null) {
                            afterHandler.propertiesSet(null);
                        }
                    }
                }
            }
        } else {
            if (afterHandler != null) {
                afterHandler.propertiesSet(null);
            }
        }
    }

    public void onSaveAs(final WsRec workspace) {
        if (workspace.getWsType() == WorkspaceType.MODEL) {
            uiManager.showSaveDialog(workspace.getName(),
                    FileManager.FileType.MODEL, new PropertiesListener<SaveRecord>() {

                public void propertiesSet(SaveRecord props) {
                    saveModel(props.getFileName(), workspace.getDispatcher().getRootAggregate());
//                    workspace.setFile(processingFile);

                    TreeModelView tmv = (TreeModelView) wm.getRegisteredComponent(TreeModelView.CID);

                    workspace.getJessCanvas().clear();
                    workspace.setRegistry(getAggregateRegestryCopy());
                    workspace.getDispatcher().getLoader().onLoad(getModel(props.getFileName(), workspace.getRegistry()));
                    workspace.setName(workspace.getDispatcher().getModelName());
                    workspace.resetSaveNeed();
                    wm.getMainContent().renameWorkspace(workspace, tmv.getRoot());
                    tmv.renameRoot(workspace.getWsName());
                }
            });
        } else if (workspace.getWsType() == WorkspaceType.AGG_TYPE) {
            uiManager.showSaveDialog(workspace.getName(),
                    FileManager.FileType.AGGREGATE_TYPE, new PropertiesListener<SaveRecord>() {

                public void propertiesSet(SaveRecord props) {
                    saveAggregateType(props.getFileName(), workspace.getDispatcher().getRootAggregate());

                    if (!workspace.isParent()) {
                        workspace.getParent().setSaveNeeded(true);
                        workspace.getParent().removeChildWorkspace(workspace);
                    }

                    AggregateRegistry ar = getAggregateRegestryCopy();
                    workspace.setRegistry(ar);
                    for (WsRec ws : workspace.getChilds()) {
                        if (workspace.getDispatcher().getRootAggregate().getAggregate(ws.getName()) == null) {
                            ws.setSaveNeeded(false);
                            wm.getMainContent().closeWorkspace(ws.getWsName());
                        }
                    }
                    TreeModelView tmv = (TreeModelView) wm.getRegisteredComponent(TreeModelView.CID);

                    // reloading childs
                    for (WsRec ws : workspace.getChildsList()) {
                        wm.getMainContent().activateWorkspace(ws.getWsName());
                        tmv.removeAllItems();
                        ws.getDispatcher().getLoader().onLoad(workspace.getRegistry().getAggregateDefinition(ws.getDispatcher().getModelName()));
                    }

                    wm.getMainContent().activateWorkspace(workspace.getWsName());
                }
            });
        }
    }

    public AggregateDefinition getModel(String name, AggregateRegistry registry) {
        AggregateDefinition agg = null;
        try {
            String data_table_cname = PropertiesReader.read(PropertiesReader.Property.DB_TABLE_C_NAME);
            AggregatesManager am = AggregatesManager.getFactory();
            List<AggregateEntity> ls = am.list(AggregatesManager.AggregateType.MODEL, data_table_cname + "='" + name + "'");
            AggregateBuilder ab = new AggregateBuilder();
            Document dom = new SAXBuilder().build(new StringReader(ls.get(0).getData()));
            return ab.buildAggregateDefinition(dom, registry);
        } catch (Exception e) {
            String msg = "Failed to load the model: " + name;
            logger.error(msg, e);
        }
        return agg;
    }

    private boolean saveModel(String name, AggregateDefinition aggregate) {
        try {
            return saveAggregate(name, aggregate, AggregatesManager.AggregateType.MODEL);
        } catch (Exception e) {
            String msg = "Failed to save the model";
            logger.error(msg, e);
            errorRenderer.renderError("Error", msg);
            return false;
        }
    }

    private boolean saveAggregateType(String name, AggregateDefinition aggregate) {
        try {
            return saveAggregate(name, aggregate, AggregatesManager.AggregateType.TYPE);
        } catch (Exception e) {
            String msg = "Failed to save the aggregate type";
            logger.error(msg, e);
            errorRenderer.renderError("Error", msg);
            return false;
        }
    }
    
    private boolean saveAggregate(String name, AggregateDefinition aggregate, AggregatesManager.AggregateType type) {
        try {
            logger.debug("saving aggregate " + name);
            aggregate.changeName(name);
            AggregatesManager am = AggregatesManager.getFactory();
            logger.debug("am->save");
            long id = -1;
            
            List<AggregateEntity> list = am.get(name, type);
            logger.debug("list=" + list);
            if (list != null && !list.isEmpty()) {
                id = list.get(0).getId();
            }

            am.save(new AggregateEntity(id, name, JessUtils.aggregateToString(aggregate), 
                    type.name()));
            
            for (Aggregate a: aggregate.getChildAggregates()) {
                AggregateDefinition child = ((AggregateDefinitionReference)a).getAggregateDefinition();
                saveAggregate(child.getName(), child, AggregatesManager.AggregateType.TYPE);
            }
            
            return true;
        } catch (Exception e) {
            String msg = "Failed to save the model";
            logger.error(msg, e);
            errorRenderer.renderError("Error", msg);
            return false;
        }
    }
    
    private boolean aggregateExist(String name, AggregatesManager.AggregateType type) {
        try {
            return AggregatesManager.getFactory().exist(name, type);
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    public AggregateRegistry getAggregateRegestryCopy() {
        List<AggregateEntity> db = AggregatesManager.getFactory().list(AggregatesManager.AggregateType.TYPE);
        
        HashMap<String, InputStream> sources = new HashMap<String, InputStream>();
        for (AggregateEntity ae: db) {
            String name = ae.getName();
            InputStream is = new ByteArrayInputStream(ae.getData().getBytes());
            sources.put(name, is);
        }
        
        AggregateTypesReader atr = new AggregateTypesReader();
        try {
            return atr.readAggregateTypes(sources);
        } catch (Exception ex) {
            return null;
        }
    }

    public List<String> listAllAggregateTypes() {
        List<String> result = new ArrayList<String>();

        AggregateRegistry ar = getAggregateRegestryCopy();
        for (AggregateDefinition ad : ar.getAggregateDefinitions()) {
            result.add(ad.getName());
        }

        return result;
    }

    public Collection<String> listAllModels() {
        return AggregatesManager.getFactory().listNames(AggregatesManager.AggregateType.MODEL);
    }

    public AggregateDefinition getAggregateType(AggregateRegistry ar, String name) {
        AggregateDefinition ad = ar.getAggregateDefinition(name);
        return ad;
    }

    public AggregateDefinition getAggregateType(String aggId) {
        return getAggregateType(getAggregateRegestryCopy(), aggId);
    }

// -------------------------- INNER CLASSES --------------------------
    public interface LoadListener {

        void onLoad(AggregateDefinition aggregate);
    }
}
