package ua.cn.stu.cs.ems.ui.vaadin.ui;

import com.vaadin.data.Item;
import com.vaadin.ui.Form;
import com.vaadin.ui.FormFieldFactory;
import com.vaadin.ui.VerticalLayout;

/**
 * @author n0weak
 */
public class AbstractForm extends Form {

    public AbstractForm() {
        setLayout(new VerticalLayout());
        setSizeUndefined();
        getLayout().setSizeUndefined();
    }

    public AbstractForm(Item dataSource, FormFieldFactory fieldFactory) {
        this();
        init(dataSource, fieldFactory);
    }

    public AbstractForm(String width, Item dataSource, FormFieldFactory fieldFactory) {
        init(dataSource, fieldFactory);
        setWidth(width);
    }

    protected void init(Item dataSource, FormFieldFactory fieldFactory) {
        setWriteThrough(false);
        setInvalidCommitted(false);
        setFormFieldFactory(fieldFactory);
        setItemDataSource(dataSource);
    }
}
