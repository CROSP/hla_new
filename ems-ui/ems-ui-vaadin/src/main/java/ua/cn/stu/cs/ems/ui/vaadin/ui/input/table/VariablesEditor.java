package ua.cn.stu.cs.ems.ui.vaadin.ui.input.table;

import com.vaadin.data.Property;
import com.vaadin.data.Validator;
import com.vaadin.ui.AbstractField;
import com.vaadin.ui.Component;
import com.vaadin.ui.TextField;
import ua.cn.stu.cs.ems.ui.vaadin.dto.Variable;
import ua.cn.stu.cs.ems.ui.vaadin.validation.UniquenessValidator;
import ua.cn.stu.cs.ems.ui.vaadin.validation.ValidatorFactory;

import java.util.Collection;

/**
 * @author n0weak
 */
public class VariablesEditor extends CachingInputTable<Variable> {

    private static final String WIDTH = "450px";
    private final CachingFieldCreator<Variable> fieldCreator;

    public VariablesEditor(String caption, Collection<Variable> attributes) {
        super(Variable.class, caption);
        initContainer(attributes);
        fieldCreator = new TokenAttributesFieldCreator();
        initFactory(fieldCreator);
        setWidth(WIDTH);
        setRequiredFields("name");
    }

    @Override
    public CachingFieldCreator<Variable> getFieldCreator() {
        return fieldCreator;
    }

    private class TokenAttributesFieldCreator extends CachingFieldCreator<Variable> implements
            UniquenessValidator.DataSource<AbstractField> {

        private final Validator nameValidator = new UniquenessValidator<AbstractField>(AbstractField.class, "getValue",
                                                                                       this);

        public TokenAttributesFieldCreator() {
            super("name");
        }

        public AbstractField createNewField(final Variable item, Object propertyId) {
            final AbstractField tf = new TextField();
            tf.setInvalidCommitted(true);  //don't want to lose invalid but yet valuable changes during table repaints

            if (propertyId.equals("name")) {
                tf.addListener(new Property.ValueChangeListener() {

                    public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
                        //repainting all components to be revalidated
                        for (Component component : getCache("name").values()) {
                            if (!component.equals(tf)) {
                                component.requestRepaint();
                            }
                        }
                    }
                });

                tf.addValidator(nameValidator);
                if (!isLast(item)) {
                    ValidatorFactory.addRequiredValidator(tf);
                }

            }
            else {
                if (!isLast(item)) {
                    tf.focus();
                }
            }

            return tf;

        }

        public Collection<AbstractField> getItems() {
            return getCache("name").values();
        }
    }
    
    @Override
    public boolean removeAllItems() {
        while (getItemIds().size() > 1) {
            remove(getInnerList().iterator().next());
        }

        return true;
    }
}
