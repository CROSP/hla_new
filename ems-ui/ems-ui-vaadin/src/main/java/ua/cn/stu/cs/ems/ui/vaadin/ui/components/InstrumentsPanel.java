package ua.cn.stu.cs.ems.ui.vaadin.ui.components;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.BaseTheme;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.cn.stu.cs.ems.core.utils.Pair;
import ua.cn.stu.cs.ems.ui.vaadin.WidgetManager;
import ua.cn.stu.cs.ems.ui.vaadin.ui.components.MainContent.Position;
import ua.cn.stu.cs.ems.ui.vaadin.utils.UIUtils;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.DrawingMode;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.EntityType;

public class InstrumentsPanel extends Panel implements RegisteredComponent {

    final static Logger logger = LoggerFactory.getLogger(InstrumentsPanel.class);
    public static final String CID = "instruments_panel";
    private static final String caption = "Components";
    private static String ICONS_PATH = "icons/instruments_panel/";
    private WidgetManager wm;

    public InstrumentsPanel() {
        super();
        setStyleName("InstrumentsPanel_style");

        CssLayout layout = new CssLayout();
        layout.setStyleName("sidebar-menu");
        layout.addStyleName("layout_InstrumentsPanel");
    }

    public String getTabCaption() {
        return caption;
    }

    private void toogleCreationMode(final EntityType entityType) {
        MainContent.WsRec activeWs = wm.getMainContent().getActiveWorkspace();
        if (activeWs != null) {
//            logger.debug("    switching to switch type: " + entityType + " in=" + entityType.getCountInputs() + " out=" + entityType.getCountOutputs());
            activeWs.getJessCanvas().toggleCreationMode(entityType, activeWs.getUidGenerator().currentId(entityType));
        }
    }

    private Button createModeSwitcher(final String title, final EntityType entityType, String icon) {
        Button b = new NativeButton(title);

        b.addListener(new Button.ClickListener() {

            private final EntityType type = entityType;

            public void buttonClick(Button.ClickEvent clickEvent) {
                InstrumentsPanel.this.toogleCreationMode(type);
            }
        });

        if (icon != null) {
            b.setIcon(UIUtils.getThemeResource(ICONS_PATH + icon));
        }

        wm.getModeSwitchers().addButton(b);
        return b;
    }

    private Pair<Button, PopupView> createSpecialModeSwitcher(final String title, 
            final EntityType entityType, String icon, String iconFormat,
            final boolean outputs, int[] ios) {
        Button btn = new NativeButton(title);
        HorizontalLayout vl = new HorizontalLayout();

        final PopupView pv = new PopupView("", vl);
        pv.setHideOnMouseOut(true);

        for (int i: ios) {
            
            Button b = new Button();
            final int cnt = i;
            b.addListener(new Button.ClickListener() {

                private final EntityType type = EntityType.valueOf(entityType.name());
                private final int in = outputs ? 1 : cnt;
                private final int out = outputs ? cnt : 1;

                public void buttonClick(ClickEvent event) {
                    type.setEntityConnectorsCount(in, out);
                    logger.debug("try to switch type: " + type + " in=" + type.getCountInputs() + " out=" + type.getCountOutputs());
                    toogleCreationMode(this.type);
                    pv.setPopupVisible(false);
                }
            });
            String bIcon = String.format(iconFormat, i);
            b.setIcon(UIUtils.getThemeResource(ICONS_PATH + bIcon));
            b.setStyleName(BaseTheme.BUTTON_LINK);
            b.setWidth("24px");
            b.setHeight("24px");
            vl.addComponent(b);
        }

        btn.addListener(new Button.ClickListener() {

            private final EntityType type = EntityType.valueOf(entityType.name());
            private final int in = outputs ? 1 : 2;
            private final int out = outputs ? 2 : 1;

            public void buttonClick(ClickEvent event) {
                type.setEntityConnectorsCount(in, out);
                InstrumentsPanel.this.toogleCreationMode(type);
                pv.setPopupVisible(true);
            }
        });

        btn.setIcon(UIUtils.getThemeResource(ICONS_PATH + icon));
        wm.getModeSwitchers().addButton(btn);
        return new Pair<Button, PopupView>(btn, pv);
    }

    private Button createModeSwitcher(final String title, final DrawingMode drawingMode, String icon) {
        Button b = new NativeButton(title);
        b.setIcon(UIUtils.getThemeResource(ICONS_PATH + icon));
        return wm.createModeSwitcher(b, drawingMode);
    }

    public void init(WidgetManager wm) {
        this.wm = wm;

        CssLayout layout = new CssLayout();
        layout.setStyleName("sidebar-menu");
        layout.addStyleName("layout_InstrumentsPanel");

        EntityType type;

        type = EntityType.TTRANSITION;
        type.setEntityConnectorsCount(1, 1);
        layout.addComponent(createModeSwitcher("T-Transition", type, "ttransition.png"));


        Pair<Button, PopupView> special = createSpecialModeSwitcher(
                "F-Transition", EntityType.FTRANSITION, "ftransition.png", "ftransition_%d.png", true, new int[] {2,3,4,5,8});
        layout.addComponent(special.getFirst());
        layout.addComponent(special.getSecond());

        special = createSpecialModeSwitcher(
                "J-Transition", EntityType.JTRANSITION, "jtransition.png", "jtransition_%d.png", false, new int[] {2,3,4,5,8});
        layout.addComponent(special.getFirst());
        layout.addComponent(special.getSecond());

        special = createSpecialModeSwitcher(
                "Y-Transition", EntityType.YTRANSITION, "ytransition.png", "ytransition_%d.png", false, new int[] {2,3,4,5,8});
        layout.addComponent(special.getFirst());
        layout.addComponent(special.getSecond());

        special = createSpecialModeSwitcher(
                "X-Transition", EntityType.XTRANSITION, "xtransition.png", "xtransition_%d.png", true, new int[] {2,3,4,5,8});
        layout.addComponent(special.getFirst());
        layout.addComponent(special.getSecond());

        type = EntityType.FIFO;
        type.setEntityConnectorsCount(1, 2);
        layout.addComponent(createModeSwitcher("FIFO", type, "FIFO.png"));

        type = EntityType.PFIFO;
        type.setEntityConnectorsCount(1, 1);
        layout.addComponent(createModeSwitcher("Priority FIFO", type, "pfifo.png"));

        type = EntityType.LIFO;
        type.setEntityConnectorsCount(1, 1);
        layout.addComponent(createModeSwitcher("LIFO", EntityType.LIFO, "lifo.png"));

        type = EntityType.PFIFO;
        type.setEntityConnectorsCount(1, 1);
        layout.addComponent(createModeSwitcher("Priority LIFO", EntityType.PLIFO, "plifo.png"));

        type = EntityType.PLACE;
        type.setEntityConnectorsCount(1, 1);
        layout.addComponent(createModeSwitcher("Place", type, "place.png"));

        type = EntityType.INPUT;
        type.setEntityConnectorsCount(0, 1);
        layout.addComponent(createModeSwitcher("Input", type, "input.png"));

        type = EntityType.OUTPUT;
        type.setEntityConnectorsCount(1, 0);
        layout.addComponent(createModeSwitcher("Output", type, "output.png"));

        layout.addComponent(createModeSwitcher("Connector", DrawingMode.CONNECT, "connector.png"));
        layout.addComponent(createModeSwitcher("Mark", DrawingMode.MARK, "mark.png"));

        setContent(layout);
    }

    public void registerMenu() {
    }

    public Position getDefaultPosition() {
        return Position.RIGHT;
    }

    public String getCid() {
        return CID;
    }
}
