package ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.drawer;

import org.junit.Ignore;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.drawer.Drawer;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.drawer.TransitionDrawer;
import org.junit.Before;
import org.junit.Test;
import org.vaadin.gwtgraphics.client.Group;
import org.vaadin.gwtgraphics.client.VectorObject;
import org.vaadin.gwtgraphics.client.shape.Path;
import org.vaadin.gwtgraphics.client.shape.Text;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.Coordinates;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.Scaler;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.graphics.PreciseText;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.test.TestUtils;

import static org.junit.Assert.assertEquals;
import static ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.Scaler.scale;
import static ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.test.TestUtils.validatePath;

/**
 * @author n0weak
 */
public class TransitionDrawerUnitTest {

    @Before
    public void before() {
        TestUtils.mockAll();
    }

    @Test
    public void check_full_f_transition_drawing() {
        Coordinates c = new Coordinates(0, 0);
        int expectedHeight = 4 * Scaler.scale(TransitionDrawer.DEFAULT_VSPACING); //gap-stick-gap-stick-gap-stick-gap
        int expectedWidth = Scaler.scale(Drawer.DEFAULT_WIDTH);
        Coordinates expectedTopLeft = new Coordinates(c.x - expectedWidth / 2, c.y - expectedHeight / 2);

        TransitionDrawer.FDrawer drawer = new TransitionDrawer.FDrawer("", c.x, c.y, 1, 2);
        drawer.setGroup(TestUtils.createMockedGroup(drawer.getGroup()));

        Text text = get(drawer, PreciseText.class);
        Path path = get(drawer, Path.class);

        assertEquals(expectedHeight, drawer.getBaseHeight());
        assertEquals(expectedHeight, TestUtils.getPathHeight(path));
        assertEquals(expectedWidth, drawer.getWidth());
        assertEquals(expectedTopLeft, drawer.getTopLeft());
        assertEquals("Text must be painted right under the entity", drawer.getTopLeft().y + expectedHeight, text.getY());
        assertEquals("The center of the text must be right under the entity", drawer.getCenter().x - text.getTextWidth()
                                                                                                     / 2, text.getX());

        drawer.addStick();
        path = get(drawer, Path.class);

        Coordinates inStick = new Coordinates(drawer.getCenter().x, drawer.getTopLeft().y
                                                                    + Scaler.scale(
                TransitionDrawer.FlexibleTransitionDrawer.STARTING_HEIGHT) / 2);
        Coordinates outStick0 = new Coordinates(drawer.getCenter().x + Scaler.scale(Drawer.STICK_SIZE), drawer.
                getTopLeft().y + Scaler.scale(TransitionDrawer.DEFAULT_VSPACING));
        Coordinates outStick1 = new Coordinates(drawer.getCenter().x + Scaler.scale(Drawer.STICK_SIZE), drawer.
                getTopLeft().y + Scaler.scale(TransitionDrawer.DEFAULT_VSPACING) * 2);
        Coordinates outStick2 = new Coordinates(drawer.getCenter().x + Scaler.scale(Drawer.STICK_SIZE), drawer.
                getTopLeft().y + Scaler.scale(TransitionDrawer.DEFAULT_VSPACING) * 3);

        assertEquals("Input connector must be planted right into the core stick",
                     inStick, drawer.getInputCoordinates(0));
        assertEquals("Output connector must start from the output stick of the component, not from the core one",
                     outStick0, drawer.getOutputCoordinates(0));
        assertEquals(outStick1, drawer.getOutputCoordinates(1));
        assertEquals(outStick2, drawer.getOutputCoordinates(2));

        int vGap = scale(TransitionDrawer.DEFAULT_VSPACING);
        int hGap = scale(Drawer.STICK_SIZE);
        validatePath(path,
                     new Coordinates(c.x, expectedTopLeft.y), // .
                     new Coordinates(0, vGap), // |
                     new Coordinates(hGap, 0), new Coordinates(-hGap, 0), // .--
                     new Coordinates(0, vGap), // |
                     new Coordinates(-hGap, 0), new Coordinates(hGap, 0), new Coordinates(hGap, 0), new Coordinates(
                -hGap, 0), //--.--
                     new Coordinates(0, vGap), // |
                     new Coordinates(hGap, 0), new Coordinates(-hGap, 0), // .--
                     new Coordinates(0, vGap) // |
                );
    }

    @Ignore
    // TODO This test falls. Need to be fixed
    public void check_scaled_f_transition_drawing() {
        try {
            Scaler.setScale(0.4);
            check_full_f_transition_drawing();
            Scaler.setScale(1.3);
            check_full_f_transition_drawing();
        } finally {
            Scaler.setScale(1);
        }
    }

    @SuppressWarnings("unchecked")
    private <T> T get(Drawer drawer, Class<T> clazz) {
        T result = null;
        Group group = (Group) drawer.getVectorObject();
        for (int i = 0; i < group.getVectorObjectCount(); i++) {
            VectorObject vo = group.getVectorObject(i);
            if (clazz.equals(vo.getClass())) {
                result = (T) vo;
                break;
            }
        }
        return result;
    }
}
