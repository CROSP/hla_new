package ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.event;

import org.junit.Test;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.Coordinates;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.EntityType;

import static org.junit.Assert.assertEquals;

/**
 * @author n0weak
 */
public class EntityEventUnitTest {

    @Test
    public void simpleDeserialization() {
        String s = "aa" + Stringalizable.DELIM + "PLACE" + 
                Stringalizable.DELIM + "1" + Stringalizable.DELIM + "1" + 
                Stringalizable.DELIM + "11" + Stringalizable.DELIM + "12";

        EntityEvent entityEvent = EntityEvent.deserialize(s);

        assertEquals("aa", entityEvent.getEntityDetails().getId());
        assertEquals(EntityType.PLACE, entityEvent.getEntityDetails().getEntityType());
        assertEquals(11, entityEvent.getEntityDetails().getCenterCoords().x);
        assertEquals(12, entityEvent.getEntityDetails().getCenterCoords().y);
    }

    @Test
    public void simpleSerialization() {
        String s = "id" + Stringalizable.DELIM + "PFIFO" + Stringalizable.DELIM + 
                "1" + Stringalizable.DELIM + "1" + Stringalizable.DELIM + 
                "11" + Stringalizable.DELIM + "12";

        EntityType type = EntityType.PFIFO;
        type.setEntityConnectorsCount(1, 1);
        EntityEvent entityEvent = new EntityEvent(new EntityDetails("id", type, new Coordinates(11, 12)));
        
        assertEquals(s, entityEvent.serialize());
    }

    @Test
    public void simpleSerializationAggregateTypeWithDifferentConnectors() {
        String s = "id" + Stringalizable.DELIM + "AGGREGATE" + Stringalizable.DELIM + 
                "2" + Stringalizable.DELIM + "0" + Stringalizable.DELIM + 
                "In1" + Stringalizable.DELIM + "In2" + Stringalizable.DELIM + 
                "11" + Stringalizable.DELIM + "12";

        
        
        EntityType type = EntityType.AGGREGATE;
        type.setEntityConnectorsNames(new String[] {"In1", "In2"}, null);
        EntityEvent entityEvent = new EntityEvent(new EntityDetails("id", type, new Coordinates(11, 12)));
        
        assertEquals(s, entityEvent.serialize());
        assertEquals("In2", entityEvent.getEntityDetails().getEntityType().getInputNames()[1]);
        assertEquals(2, entityEvent.getEntityDetails().getEntityType().getCountInputs());
        assertEquals(0, entityEvent.getEntityDetails().getEntityType().getCountOutputs());
        
        
        s = "id" + Stringalizable.DELIM + "AGGREGATE" + Stringalizable.DELIM + 
                "0" + Stringalizable.DELIM + "2" + Stringalizable.DELIM + 
                "Out1" + Stringalizable.DELIM + "Out2" + Stringalizable.DELIM + 
                "11" + Stringalizable.DELIM + "12";
        
        type = EntityType.AGGREGATE;
        type.setEntityConnectorsNames(null, new String[] {"Out1", "Out2"});
        entityEvent = new EntityEvent(new EntityDetails("id", type, new Coordinates(11, 12)));
        assertEquals(s, entityEvent.serialize());
        assertEquals("Out2", entityEvent.getEntityDetails().getEntityType().getOutputNames()[1]);
        assertEquals(0, entityEvent.getEntityDetails().getEntityType().getCountInputs());
        assertEquals(2, entityEvent.getEntityDetails().getEntityType().getCountOutputs());
    }
    
    @Test
    public void simpleDeserializationAggregateTypeWithDifferentConnectors() {
        String s = "id" + Stringalizable.DELIM + "AGGREGATE" + Stringalizable.DELIM + 
                "2" + Stringalizable.DELIM + "0" + Stringalizable.DELIM + 
                "In1" + Stringalizable.DELIM + "In2" + Stringalizable.DELIM + 
                "11" + Stringalizable.DELIM + "12";

        
        
        EntityEvent entityEvent = EntityEvent.deserialize(s);
        
        assertEquals(s, entityEvent.serialize());
        assertEquals("In2", entityEvent.getEntityDetails().getEntityType().getInputNames()[1]);
        assertEquals(2, entityEvent.getEntityDetails().getEntityType().getCountInputs());
        assertEquals(0, entityEvent.getEntityDetails().getEntityType().getCountOutputs());
        
        
        s = "id" + Stringalizable.DELIM + "AGGREGATE" + Stringalizable.DELIM + 
                "0" + Stringalizable.DELIM + "2" + Stringalizable.DELIM + 
                "Out1" + Stringalizable.DELIM + "Out2" + Stringalizable.DELIM + 
                "11" + Stringalizable.DELIM + "12";
        
        entityEvent = EntityEvent.deserialize(s);
        assertEquals(s, entityEvent.serialize());
        assertEquals("Out2", entityEvent.getEntityDetails().getEntityType().getOutputNames()[1]);
        assertEquals(0, entityEvent.getEntityDetails().getEntityType().getCountInputs());
        assertEquals(2, entityEvent.getEntityDetails().getEntityType().getCountOutputs());
    }
}
