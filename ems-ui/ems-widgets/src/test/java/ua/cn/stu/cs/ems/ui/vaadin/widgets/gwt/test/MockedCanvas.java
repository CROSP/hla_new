package ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.test;

import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.IJessCanvas;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.ActiveEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * @author n0weak
 */
public class MockedCanvas implements IJessCanvas {

    private static final int CANVAS_WIDTH = 600;
    private static final int CANVAS_HEIGHT = 600;
    private List<ActiveEntity> entities = new ArrayList<ActiveEntity>();

    public int getWidth() {
        return CANVAS_WIDTH;
    }

    public int getHeight() {
        return CANVAS_HEIGHT;
    }

    public List<ActiveEntity> getEntitiesList() {
        return entities;
    }

    public void setEntities(List<ActiveEntity> entities) {
        this.entities = entities;
    }

    public List<ActiveEntity> getEntities() {
        return entities;
    }
}
