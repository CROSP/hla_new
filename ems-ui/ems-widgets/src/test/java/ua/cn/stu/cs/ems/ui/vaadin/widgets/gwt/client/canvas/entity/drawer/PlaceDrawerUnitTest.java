package ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.drawer;

import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.drawer.PlaceDrawer;
import org.junit.Before;
import org.junit.Test;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.test.TestUtils;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @author n0weak
 */
public class PlaceDrawerUnitTest {

    private static final int RADIUS = PlaceDrawer.DEFAULT_RADIUS;
    private static final int X = 100;
    private static final int Y = 100;
    private PlaceDrawer placeDrawer;

    @Before
    public void before() {
        TestUtils.mockAll();
        placeDrawer = new PlaceDrawer("", X, Y);
    }

    @Test
    public void boundary_v_intersection_must_be_detected() {
        assertTrue(placeDrawer.intersectsV(X - RADIUS, Y, Y));
        assertTrue(placeDrawer.intersectsV(X + RADIUS, Y, Y));
        assertTrue(placeDrawer.intersectsV(X + RADIUS, Y - RADIUS, Y));
        assertTrue(placeDrawer.intersectsV(X + RADIUS, Y, Y + RADIUS));
    }

    @Test
    public void inner_v_intersection_must_be_detected() {
        assertTrue(placeDrawer.intersectsV(X, Y, Y));
    }

    @Test
    public void simple_v_intersection_must_be_deteted() {
        assertTrue(placeDrawer.intersectsV(X - RADIUS / 2, Y - RADIUS - 20, Y + RADIUS + 20));
        assertTrue(placeDrawer.intersectsV(X - RADIUS / 2, Y + RADIUS + 20, Y - RADIUS - 20));
    }

    @Test
    public void no_v_intersection() {
        assertFalse(placeDrawer.intersectsV(X - RADIUS * 2, Y - RADIUS - 20, Y + RADIUS + 20));
        assertFalse(placeDrawer.intersectsV(X + RADIUS * 2, Y - RADIUS - 20, Y + RADIUS + 20));
        assertFalse(placeDrawer.intersectsV(X, Y - RADIUS - 20, Y - RADIUS - 10));
        assertFalse(placeDrawer.intersectsV(X, Y + RADIUS + 10, Y + RADIUS + 10));
    }
}
