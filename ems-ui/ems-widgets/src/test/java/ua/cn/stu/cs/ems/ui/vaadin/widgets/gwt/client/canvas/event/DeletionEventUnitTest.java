package ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.event;

import org.junit.Test;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.Coordinates;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.EntityType;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * @author n0weak
 */
public class DeletionEventUnitTest {

    @Test
    public void simpleDeserialization() {
        String s = "a1" + Stringalizable.DELIM + "PLACE" + Stringalizable.DELIM 
                   + "1" + Stringalizable.DELIM + "1" + Stringalizable.DELIM 
                   + "11" + Stringalizable.DELIM + "12" + Stringalizable.DELIM
                   + "a2" + Stringalizable.DELIM + "PFIFO" + Stringalizable.DELIM + "1" + Stringalizable.DELIM + "1" + Stringalizable.DELIM 
                   + "21" + Stringalizable.DELIM + "22" + Stringalizable.DELIM 
                   + "a3" + Stringalizable.DELIM + "PLIFO" + Stringalizable.DELIM + "1" + Stringalizable.DELIM + "1" + Stringalizable.DELIM
                   + "31" + Stringalizable.DELIM + "32" + Stringalizable.DELIM;
                   
        DeletionEvent event = DeletionEvent.deserialize(s);

        assertEquals(3, event.getDeletedEntities().size());
        assertEquals(EntityType.PLACE, event.getDeletedEntities().get(0).getEntityType());
        assertEquals("a1", event.getDeletedEntities().get(0).getId());
        assertEquals(EntityType.PFIFO, event.getDeletedEntities().get(1).getEntityType());
        assertEquals("a2", event.getDeletedEntities().get(1).getId());
        assertEquals(EntityType.PLIFO, event.getDeletedEntities().get(2).getEntityType());
        assertEquals("a3", event.getDeletedEntities().get(2).getId());
    }

    @Test
    public void simpleSerialization() {
        String s = "id" + Stringalizable.DELIM + "FIFO" + Stringalizable.DELIM + "1" + Stringalizable.DELIM + 1
                + Stringalizable.DELIM + "11" + Stringalizable.DELIM + "12" + Stringalizable.DELIM
                + "id2" + Stringalizable.DELIM + "PLIFO" + Stringalizable.DELIM + "1" + Stringalizable.DELIM + "1" + Stringalizable.DELIM + "21" 
                + Stringalizable.DELIM + "22" + Stringalizable.DELIM;
        
        List<EntityDetails> list = new ArrayList<EntityDetails>();

        EntityType type = EntityType.FIFO;
        type.setEntityConnectorsCount(1, 1);
        list.add(new EntityDetails("id", EntityType.FIFO, new Coordinates(11, 12)));

        type = EntityType.PLIFO;
        type.setEntityConnectorsCount(1, 1);
        list.add(new EntityDetails("id2", type, new Coordinates(21, 22)));
        
        DeletionEvent event = new DeletionEvent(list);
        
        assertEquals(s, event.serialize());
    }
}
