package ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.test;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.GWTBridge;
import com.google.gwt.dom.client.Element;
import org.junit.Ignore;
import org.mockito.Mockito;
import org.vaadin.gwtgraphics.client.Group;
import org.vaadin.gwtgraphics.client.VectorObject;
import org.vaadin.gwtgraphics.client.impl.SVGImpl;
import org.vaadin.gwtgraphics.client.shape.Path;
import org.vaadin.gwtgraphics.client.shape.path.MoveTo;
import org.vaadin.gwtgraphics.client.shape.path.PathStep;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.Coordinates;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.graphics.CustomSVGImpl;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.graphics.PreciseText;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.*;

import static org.junit.Assert.assertEquals;

/**
 * @author n0weak
 */
@SuppressWarnings("unused")
@Ignore
public class TestUtils {

    private static boolean mocked;

    public static void mockAll() {
        if (!mocked) {
            setGwtBridge(new MockedGwtBridge(getGwtBridge()));
            mockSvgImpl();
            mocked = true;
        }
    }

    public static GWTBridge getGwtBridge() {
        Class<GWT> gwtClass = GWT.class;

        try {
            Field bridgeField = gwtClass.getDeclaredField("sGWTBridge");
            bridgeField.setAccessible(true);
            return (GWTBridge) bridgeField.get(GWT.class);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void mockSvgImpl() {
        try {
            Field svgImplField = VectorObject.class.getDeclaredField("impl");
            svgImplField.setAccessible(true);

            Field modifiersField = Field.class.getDeclaredField("modifiers");
            modifiersField.setAccessible(true);
            modifiersField.setInt(svgImplField, svgImplField.getModifiers() & ~Modifier.FINAL);

            svgImplField.set(VectorObject.class, new MockedSvgImpl());

            //mocking custom impl of the PreciseText class
            svgImplField = PreciseText.class.getDeclaredField("impl");
            svgImplField.setAccessible(true);
            svgImplField.set(PreciseText.class, Mockito.mock(CustomSVGImpl.class));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void setGwtBridge(GWTBridge bridge) {
        Class<GWT> gwtClass = GWT.class;
        Class<?>[] paramTypes = new Class[]{GWTBridge.class};
        Method setBridgeMethod;
        try {
            setBridgeMethod = gwtClass.getDeclaredMethod("setBridge", paramTypes);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
        setBridgeMethod.setAccessible(true);
        try {
            setBridgeMethod.invoke(gwtClass, bridge);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    public static void printSteps(Path path) {
        for (int i = 0; i < path.getStepCount(); i++) {
            MoveTo step = (MoveTo) path.getStep(i);
            System.out.println("Step #" + i + ": x - " + step.getX() + ", y - " + step.getY());
        }
    }

    /**
     * Method that evaluates height of the path, form top to bottom, starting form the first step.
     *
     * @param path path which height to be evaluated. Path is assumed to be started at its top point.
     * @return height of the path
     */
    public static int getPathHeight(Path path) {
        int height = 0;
        int result = 0;
        for (int i = 1; i < path.getStepCount(); i++) {
            MoveTo step = (MoveTo) path.getStep(i);
            height += step.getY();
            if (height > result) {
                result = height;
            }
        }
        return result;
    }

    public static void assertPathNotContains(Path path, Coordinates... coords) {
        List<Coordinates> list = new ArrayList<Coordinates>(Arrays.asList(coords));
        int baseX = path.getX();
        int baseY = path.getY();

        int i = 1; //first step is counted in baseX and baseY, skipping
        for (; i < path.getStepCount(); i++) {
            MoveTo step = (MoveTo) path.getStep(i);
            baseX += step.getX();
            baseY += step.getY();
            Coordinates c = new Coordinates(baseX, baseY);
            if (list.contains(c)) {
                throw new AssertionError("Path must not contain " + c);
            }
        }
    }

    public static void assertPathContains(Path path, Coordinates... coords) {
        List<Coordinates> list = new ArrayList<Coordinates>(Arrays.asList(coords));
        int baseX = path.getX();
        int baseY = path.getY();

        int i = 1; //first step is counted in baseX and baseY, skipping
        for (; i < path.getStepCount(); i++) {
            MoveTo step = (MoveTo) path.getStep(i);
            baseX += step.getX();
            baseY += step.getY();
            list.remove(new Coordinates(baseX, baseY));
        }
        assertEquals("Path must contain all coordinates specified", 0, list.size());
    }

    public static void validatePath(Path path, Coordinates... coords) {
        for (int i = 0; i < path.getStepCount(); i++) {
            MoveTo step = (MoveTo) path.getStep(i);
            Coordinates expected = coords[i];
            assertEquals("#" + i + ": checking x", expected.x, step.getX());
            assertEquals("#" + i + ": checking y", expected.y, step.getY());

        }
        assertEquals(path.getStepCount(), coords.length);
    }

    public static void printSteps(List<Path> paths) {
        for (int i = 0; i < paths.size(); i++) {
            System.out.println("Path #" + i);
            Path path = paths.get(i);
            for (int j = 0; j < path.getStepCount(); j++) {
                MoveTo step = (MoveTo) path.getStep(j);
                System.out.println("Step #" + j + ": x - " + step.getX() + ", y - " + step.getY());
            }

        }
    }

    public static Group createMockedGroup(Group group) {
        return new MockedGroup(group);
    }

    private static class MockedGwtBridge extends GWTBridge {

        private final GWTBridge realBridge;

        private MockedGwtBridge(GWTBridge realBridge) {
            this.realBridge = realBridge;
        }

        @Override
        @SuppressWarnings("unchecked")
        public <T> T create(Class<?> aClass) {
            return (T) Mockito.mock(aClass);
        }

        @Override
        public String getThreadUniqueID() {
            return realBridge.getThreadUniqueID();
        }

        @Override
        public String getVersion() {
            return realBridge.getVersion();
        }

        @Override
        public boolean isClient() {
            return realBridge.isClient();
        }

        @Override
        public void log(String s, Throwable throwable) {
        }
    }

    private static class MockedSvgImpl extends SVGImpl {

        private SVGImpl mockedSvgImpl = Mockito.mock(SVGImpl.class);
        private Map<Integer, MockedElement> elements = new HashMap<Integer, MockedElement>();
        private int currentId = 0;

        @Override
        public Element createElement(Class<? extends VectorObject> type) {
            MockedElement element = new MockedElement(++currentId);
            elements.put(element.getElementId(), element);
            return element;
        }

        @Override
        public void drawPath(Element element, List<PathStep> steps) {
            mockedSvgImpl.drawPath(element, steps);
        }

        @Override
        public void setX(Element element, int x, boolean attached) {
            getMockedElement(element).setX(x);
        }

        @Override
        public void setY(Element element, int y, boolean attached) {
            getMockedElement(element).setY(y);
        }

        @Override
        public void setCircleRadius(Element element, int radius) {
            getMockedElement(element).setRadius(radius);
        }

        @Override
        public void setEllipseRadiusX(Element element, int radiusX) {
            mockedSvgImpl.setEllipseRadiusX(element, radiusX);
        }

        @Override
        public void setEllipseRadiusY(Element element, int radiusY) {
            mockedSvgImpl.setEllipseRadiusY(element, radiusY);
        }

        @Override
        public void setFillColor(Element element, String color) {
            mockedSvgImpl.setFillColor(element, color);
        }

        @Override
        public void setFillOpacity(Element element, double opacity) {
            mockedSvgImpl.setFillOpacity(element, opacity);
        }

        @Override
        public void setHeight(Element element, int height) {
            mockedSvgImpl.setHeight(element, height);
        }

        @Override
        public void setImageHref(Element element, String src) {
            mockedSvgImpl.setImageHref(element, src);
        }

        @Override
        public void setLineX2(Element element, int x2) {
            getMockedElement(element).setX2(x2);
        }

        @Override
        public void setLineY2(Element element, int y2) {
            getMockedElement(element).setY2(y2);
        }

        @Override
        public void setRectangleRoundedCorners(Element element, int radius) {
            mockedSvgImpl.setRectangleRoundedCorners(element, radius);
        }

        @Override
        public void setRotation(Element element, int degree, boolean attached) {
            mockedSvgImpl.setRotation(element, degree, attached);
        }

        @Override
        public void setStrokeColor(Element element, String color) {
            mockedSvgImpl.setStrokeColor(element, color);
        }

        @Override
        public void setStrokeOpacity(Element element, double opacity) {
            mockedSvgImpl.setStrokeOpacity(element, opacity);
        }

        @Override
        public void setStrokeWidth(Element element, int width, boolean attached) {
            mockedSvgImpl.setStrokeWidth(element, width, attached);
        }

        @Override
        public void setStyleName(Element element, String name) {
            mockedSvgImpl.setStyleName(element, name);
        }

        @Override
        public void setText(Element element, String text, boolean attached) {
            mockedSvgImpl.setText(element, text, attached);
        }

        @Override
        public void setTextFontFamily(Element element, String family, boolean attached) {
            mockedSvgImpl.setTextFontFamily(element, family, attached);
        }

        @Override
        public void setTextFontSize(Element element, int size, boolean attached) {
            mockedSvgImpl.setTextFontSize(element, size, attached);
        }

        @Override
        public void setWidth(Element element, int width) {
            mockedSvgImpl.setWidth(element, width);
        }

        @Override
        public int getCircleRadius(Element element) {
            return getMockedElement(element).getRadius();
        }

        @Override
        public int getEllipseRadiusX(Element element) {
            return mockedSvgImpl.getEllipseRadiusX(element);
        }

        @Override
        public int getEllipseRadiusY(Element element) {
            return mockedSvgImpl.getEllipseRadiusY(element);
        }

        @Override
        public String getFillColor(Element element) {
            return mockedSvgImpl.getFillColor(element);
        }

        @Override
        public double getFillOpacity(Element element) {
            return mockedSvgImpl.getFillOpacity(element);
        }

        @Override
        public int getHeight(Element element) {
            return mockedSvgImpl.getHeight(element);
        }

        @Override
        public String getImageHref(Element element) {
            return mockedSvgImpl.getImageHref(element);
        }

        @Override
        public int getLineX2(Element element) {
            return getMockedElement(element).getX2();
        }

        @Override
        public int getLineY2(Element element) {
            return getMockedElement(element).getY2();
        }

        @Override
        public int getRectangleRoundedCorners(Element element) {
            return mockedSvgImpl.getRectangleRoundedCorners(element);
        }

        @Override
        public String getRendererString() {
            return mockedSvgImpl.getRendererString();
        }

        @Override
        public int getRotation(Element element) {
            return mockedSvgImpl.getRotation(element);
        }

        @Override
        public String getStrokeColor(Element element) {
            return mockedSvgImpl.getStrokeColor(element);
        }

        @Override
        public double getStrokeOpacity(Element element) {
            return mockedSvgImpl.getStrokeOpacity(element);
        }

        @Override
        public int getStrokeWidth(Element element) {
            return mockedSvgImpl.getStrokeWidth(element);
        }

        @Override
        public String getStyleSuffix() {
            return mockedSvgImpl.getStyleSuffix();
        }

        @Override
        public String getText(Element element) {
            return mockedSvgImpl.getText(element);
        }

        @Override
        public String getTextFontFamily(Element element) {
            return mockedSvgImpl.getTextFontFamily(element);
        }

        @Override
        public int getTextFontSize(Element element) {
            return mockedSvgImpl.getTextFontSize(element);
        }

        @Override
        public int getTextHeight(Element element) {
            return mockedSvgImpl.getTextHeight(element);
        }

        @Override
        public int getTextWidth(Element element) {
            return mockedSvgImpl.getTextWidth(element);
        }

        @Override
        public int getWidth(Element element) {
            return mockedSvgImpl.getWidth(element);
        }

        @Override
        public int getX(Element element) {
            return getMockedElement(element).getX();
        }

        @Override
        public int getY(Element element) {
            return getMockedElement(element).getY();
        }

        @Override
        public void insert(Element root, Element element, int beforeIndex, boolean attached) {
            mockedSvgImpl.insert(root, element, beforeIndex, attached);
        }

        @Override
        public int measureTextSize(Element element, boolean measureWidth) {
            return 0;
        }

        @Override
        public void onAttach(Element element, boolean attached) {
            mockedSvgImpl.onAttach(element, attached);
        }

        @Override
        public void remove(Element root, Element element) {
            mockedSvgImpl.remove(root, element);
        }

        @Override
        public void add(Element root, Element element, boolean attached) {
            mockedSvgImpl.add(root, element, attached);
        }

        @Override
        public void bringToFront(Element root, Element element) {
            mockedSvgImpl.bringToFront(root, element);
        }

        @Override
        public void clear(Element root) {
            mockedSvgImpl.clear(root);
        }

        @Override
        public Element createDrawingArea(Element container, int width, int height) {
            return mockedSvgImpl.createDrawingArea(container, width, height);
        }

        private MockedElement getMockedElement(Element element) {
            if (element instanceof MockedElement) {
                return elements.get(((MockedElement) element).getElementId());
            }
            else {
                throw new IllegalArgumentException("MockedSvgImpl serves only for MockedElements!");
            }
        }

        private static class MockedElement extends com.google.gwt.user.client.Element {

            private final int id;
            private int x;
            private int y;
            private int radius;
            private int y2;
            private int x2;

            private MockedElement(int id) {
                this.id = id;
            }

            public int getElementId() {
                return id;
            }

            public int getRadius() {
                return radius;
            }

            public void setRadius(int radius) {
                this.radius = radius;
            }

            public int getX() {
                return x;
            }

            public void setX(int x) {
                this.x = x;
            }

            public int getY() {
                return y;
            }

            public void setY(int y) {
                this.y = y;
            }

            public int getX2() {
                return x2;
            }

            public void setX2(int x2) {
                this.x2 = x2;
            }

            public int getY2() {
                return y2;
            }

            public void setY2(int y2) {
                this.y2 = y2;
            }
        }
    }

    /**
     * Group which clear method can be safely called in unit tests.
     */
    private static class MockedGroup extends Group {

        private Group group;

        private MockedGroup(Group group) {
            this.group = group;
        }

        @Override
        public VectorObject add(VectorObject vo) {
            return group.add(vo);
        }

        @Override
        public VectorObject bringToFront(VectorObject vo) {
            return group.bringToFront(vo);
        }

        @Override
        public void clear() {
            group = new Group();
        }

        @Override
        public VectorObject getVectorObject(int index) {
            return group.getVectorObject(index);
        }

        @Override
        public int getVectorObjectCount() {
            return group.getVectorObjectCount();
        }

        @Override
        public VectorObject insert(VectorObject vo, int beforeIndex) {
            return group.insert(vo, beforeIndex);
        }
    }
}
