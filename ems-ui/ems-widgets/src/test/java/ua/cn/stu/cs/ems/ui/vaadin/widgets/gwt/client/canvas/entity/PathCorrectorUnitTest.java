package ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity;

import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.EntityType;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.ActiveEntity;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.PathCorrector;
import org.junit.Before;
import org.junit.Test;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.Coordinates;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.Grid;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.drawer.PlaceDrawer;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.test.MockedCanvas;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.test.TestUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

/**
 * @author n0weak
 */
public class PathCorrectorUnitTest {

    private PathCorrector pathCorrector;
    private MockedCanvas mockedCanvas;

    @Before
    public void setUp() {
        TestUtils.mockAll();
    }

    public PathCorrectorUnitTest() {
        Grid.setAdjustEnabled(false);
        mockedCanvas = new MockedCanvas();
        pathCorrector = new PathCorrector(mockedCanvas);
        pathCorrector.setEnabled(true);
        PathCorrector.setHorizontalFirst(true);
    }

    @Test
    public void simple_y_correction() {
        List<ActiveEntity> candidates = new ArrayList<ActiveEntity>();
        int x = 50;
        int y = 50;
        ActiveEntity place = EntityType.PLACE.createEntity("", x, y);
        candidates.add(place);


        Coordinates begin = new Coordinates(x, y - PlaceDrawer.DEFAULT_RADIUS - 20);
        Coordinates end = new Coordinates(x, y + PlaceDrawer.DEFAULT_RADIUS + 20);

        assertEquals("place must detect vertical intersection with the line drown", true, place.intersectsV(begin.x,
                                                                                                            begin.y,
                                                                                                            end.y));

        List<Coordinates> coords = new ArrayList<Coordinates>();
        coords.add(begin);

        boolean topDown = true;
        boolean left = true;

        Coordinates last;
        do {
            last = coords.get(coords.size() - 1);
        } while (!pathCorrector.correctY(coords, candidates, last.x, last.y, end.y, topDown, left));


        int newY = y - pathCorrector.getGap() - place.getHeight() / 2;
        int newX = x - pathCorrector.getGap() - place.getWidth() / 2;

        assertEquals("corrector should have added 3 new coordinates to avoid intersection", 4, coords.size());
        assertEquals(begin, coords.get(0));
        assertEquals("stop before reaching the entity", new Coordinates(x, newY), coords.get(1));
        assertEquals("bypass entity to the left", new Coordinates(newX, newY), coords.get(2));
        assertEquals("and reach end.y!", new Coordinates(newX, end.y), coords.get(3));
    }

    @Test
    public void simple_x_correction() {

        List<ActiveEntity> candidates = new ArrayList<ActiveEntity>();
        int x = 180;
        int y = 180;
        ActiveEntity place = EntityType.PLACE.createEntity("", x, y);
        candidates.add(place);


        Coordinates begin = new Coordinates(x - PlaceDrawer.DEFAULT_RADIUS - 20, y);
        Coordinates end = new Coordinates(x + PlaceDrawer.DEFAULT_RADIUS + 20, y);

        assertEquals("place must detect horizontal intersection with the line drown", true, place.intersectsH(begin.y,
                                                                                                              begin.x,
                                                                                                              end.x));

        List<Coordinates> coords = new ArrayList<Coordinates>();
        coords.add(begin);

        boolean leftToRight = true;
        boolean up = true;


        Coordinates last;
        do {
            last = coords.get(coords.size() - 1);
        } while (!pathCorrector.correctX(coords, candidates, last.y, last.x, end.x, leftToRight, up));


        int newY = y - pathCorrector.getGap() - place.getHeight() / 2;
        int newX = x - pathCorrector.getGap() - place.getWidth() / 2;

        assertEquals("corrector should have added 3 new coordinates to avoid intersection", 4, coords.size());
        assertEquals(begin, coords.get(0));
        assertEquals("stop before reaching the entity", new Coordinates(newX, y), coords.get(1));
        assertEquals("bypass entity to the up..", new Coordinates(newX, newY), coords.get(2));
        assertEquals("and reach end.x!", new Coordinates(end.x, newY), coords.get(3));
    }

    @Test
    public void x_correction_with_end_right_under_the_entity() {
        List<ActiveEntity> candidates = new ArrayList<ActiveEntity>();
        int x = 80;
        int y = 80;
        ActiveEntity place = EntityType.PLACE.createEntity("", x, y);
        candidates.add(place);

        //blocking the second path (v+h)
        candidates.add(EntityType.PLACE.createEntity("", 0, 120));

        Coordinates begin = new Coordinates(20, 80);
        Coordinates end = new Coordinates(80, 120);

        mockedCanvas.setEntities(candidates);
        List<Coordinates> coords = pathCorrector.correct(begin, end);

        assertEquals("corrector should have added 2 new coordinates to avoid intersection + begin + end", 4,
                     coords.size());
        assertEquals(begin, coords.get(0));
        int newX = x - place.getWidth() / 2 - pathCorrector.getGap();
        int newY = begin.y;
        assertEquals(new Coordinates(newX, newY), coords.get(1));
        newY = end.y;
        assertEquals(new Coordinates(newX, newY), coords.get(2));
        assertEquals(end, coords.get(3));
    }

    @Test
    public void x_correction_with_end_right_over_the_entity() {
        List<ActiveEntity> candidates = new ArrayList<ActiveEntity>();
        int x = 80;
        int y = 80;
        ActiveEntity place = EntityType.PLACE.createEntity("", x, y);
        candidates.add(place);

        Coordinates begin = new Coordinates(x - PlaceDrawer.DEFAULT_RADIUS - 10, y);
        Coordinates end = new Coordinates(x, y);

        mockedCanvas.setEntities(candidates);
        List<Coordinates> coords = pathCorrector.correct(begin, end);

        assertEquals("corrector should have added no coordinates to avoid intersection", 2, coords.size());
        assertEquals(begin, coords.get(0));
        assertEquals(end, coords.get(1));
    }

    @Test
    public void detect_that_no_correction_is_needed_by_drawing_horizontal_and_then_vertical_lines() {
        List<ActiveEntity> candidates = new ArrayList<ActiveEntity>();
        candidates.add(EntityType.PLACE.createEntity("", 100, 100));
        candidates.add(EntityType.PLACE.createEntity("", 180, 180));

        Coordinates begin = new Coordinates(100, 40);
        Coordinates end = new Coordinates(280, 180);

        mockedCanvas.setEntities(candidates);
        List<Coordinates> coords = pathCorrector.correct(begin, end);


        assertEquals("corrector should have added 1 new coordinate to avoid intersection + begin + end", 3,
                     coords.size());
        assertEquals(begin, coords.get(0));
        assertEquals("corrector should have used horizontal and then vertical line", new Coordinates(280, 40), coords.
                get(1));
        assertEquals(end, coords.get(2));
    }

    @Test
    public void detect_that_no_correction_is_needed_by_drawing_vertical_and_then_horizontal_lines() {
        List<ActiveEntity> candidates = new ArrayList<ActiveEntity>();
        candidates.add(EntityType.PLACE.createEntity("", 160, 40));
        candidates.add(EntityType.PLACE.createEntity("", 280, 100));

        Coordinates begin = new Coordinates(100, 40);
        Coordinates end = new Coordinates(280, 180);

        mockedCanvas.setEntities(candidates);
        List<Coordinates> coords = pathCorrector.correct(begin, end);

        assertEquals("corrector should have added 1 new coordinate to avoid intersection + begin + end", 3,
                     coords.size());
        assertEquals(begin, coords.get(0));
        assertEquals("corrector should have used vertical and then horizontal line", new Coordinates(100, 180), coords.
                get(1));
        assertEquals(end, coords.get(2));
    }

    @Test
    public void simple_correction() {
        List<ActiveEntity> candidates = new ArrayList<ActiveEntity>();

        int x1 = 160;
        int y1 = 40;
        int x2 = 280;
        int y2 = 100;

        ActiveEntity place = EntityType.PLACE.createEntity("", x1, y1);

        candidates.add(place);
        candidates.add(EntityType.PLACE.createEntity("", x2, y2));

        //entities which block another path (vertical+horizontal)
        candidates.add(EntityType.PLACE.createEntity("", 70, 100));
        candidates.add(EntityType.PLACE.createEntity("", 180, 180));


        Coordinates begin = new Coordinates(70, 40);
        Coordinates end = new Coordinates(280, 180);

        mockedCanvas.setEntities(candidates);
        List<Coordinates> coords = pathCorrector.correct(begin, end);

        int gap = pathCorrector.getGap();
        int width = place.getWidth();
        int height = place.getHeight();

        assertEquals("corrector should have added 5 new coordinates to avoid intersection + begin + end", 7,
                     coords.size());
        assertEquals(begin, coords.get(0));
        int newX = x1 - gap - width / 2;
        int newY = y1;
        assertEquals(new Coordinates(newX, newY), coords.get(1));
        newY = y1 + gap + height / 2;
        assertEquals(new Coordinates(newX, newY), coords.get(2));
        newX = x2 - gap - width / 2;
        assertEquals(new Coordinates(newX, newY), coords.get(3));
        newY = y2 + gap + height / 2;
        assertEquals(new Coordinates(newX, newY), coords.get(4));
        newX = end.x;
        assertEquals(new Coordinates(newX, newY), coords.get(5));
        assertEquals(end, coords.get(6));
    }

    @Test
    public void get_candidates_only_from_the_valid_range() {

        List<ActiveEntity> entities = new ArrayList<ActiveEntity>();
        ActiveEntity place1 = EntityType.PLACE.createEntity("", 50, 50);
        entities.add(place1);
        ActiveEntity place2 =
                     EntityType.PLACE.createEntity("", 50 + PathCorrector.ZONE_GAP, 50 + PathCorrector.ZONE_GAP);
        entities.add(place2);

        Coordinates begin = new Coordinates(50, 50 - PlaceDrawer.DEFAULT_RADIUS - 20);
        Coordinates end = new Coordinates(50, 50 + PlaceDrawer.DEFAULT_RADIUS + 20);

        assertEquals("place1 must detect vertical intersection with the line drown", true, place1.intersectsV(begin.x,
                                                                                                              begin.y,
                                                                                                              end.y));
        assertEquals("place2 must not detect vertical intersection with the line drown", false,
                     place2.intersectsV(begin.x, begin.y, end.y));

        List<ActiveEntity> candidates = pathCorrector.getCandidates(begin, end, entities, new ArrayList<ActiveEntity>());

        assertEquals(1, candidates.size());
        assertEquals(place1, candidates.get(0));
    }

    @Test
    public void perpendicular_intersection_to_be_detected() {
        Coordinates intersection = pathCorrector.closestIntersection(new Coordinates(1, 4), new Coordinates(6, 4),
                                                                     new Coordinates(3, 1), new Coordinates(3, 7));
        assertEquals(new Coordinates(3, 4), intersection);
    }

    @Test
    public void collinear_left_to_right_intersection() {
        Coordinates intersection = pathCorrector.closestIntersection(new Coordinates(1, 4), new Coordinates(6, 4),
                                                                     new Coordinates(3, 4), new Coordinates(8, 4));
        assertEquals(new Coordinates(6, 4), intersection);

        intersection = pathCorrector.closestIntersection(new Coordinates(0, 0), new Coordinates(4, 4),
                                                         new Coordinates(2, 2), new Coordinates(6, 6));
        assertEquals(new Coordinates(4, 4), intersection);

        intersection = pathCorrector.closestIntersection(new Coordinates(2, 5), new Coordinates(6, 15), new Coordinates(
                4, 10), new Coordinates(8, 20));
        assertEquals(new Coordinates(6, 15), intersection);
    }

    @Test
    public void collinear_right_to_left_intersection() {
        Coordinates intersection = pathCorrector.closestIntersection(new Coordinates(1, 4), new Coordinates(6, 4),
                                                                     new Coordinates(8, 4), new Coordinates(3, 4));
        assertEquals(new Coordinates(3, 4), intersection);
        intersection = pathCorrector.closestIntersection(new Coordinates(2, 5), new Coordinates(6, 15), new Coordinates(
                8, 20), new Coordinates(4, 10));
        assertEquals(new Coordinates(4, 10), intersection);
    }

    @Test
    public void no_intersection_to_be_detected() {
        Coordinates intersection = pathCorrector.closestIntersection(new Coordinates(1, 4), new Coordinates(6, 4),
                                                                     new Coordinates(7, 1), new Coordinates(7, 7));
        assertEquals(null, intersection);
        intersection = pathCorrector.closestIntersection(new Coordinates(1, 4), new Coordinates(6, 4),
                                                         new Coordinates(8, 4), new Coordinates(10, 4));
        assertEquals(null, intersection);
        intersection = pathCorrector.closestIntersection(new Coordinates(14, 4), new Coordinates(6, 4), new Coordinates(
                1, 4), new Coordinates(3, 4));
        assertEquals(null, intersection);
    }

    @Test
    public void remove_no_cycles() {
        List<Coordinates> coords = new ArrayList<Coordinates>(Arrays.asList(
                new Coordinates(1, 2), new Coordinates(5, 2), new Coordinates(5, 4), new Coordinates(9, 4),
                new Coordinates(9, 1), new Coordinates(13, 1), new Coordinates(13, 10)));

        List<Coordinates> uncycledCoords = new ArrayList<Coordinates>(coords);
        pathCorrector.removeCycles(uncycledCoords);

        assertArrayEquals(coords.toArray(), uncycledCoords.toArray());
    }

    @Test
    public void remove_cycle_with_perpendicular_intersection() {
        List<Coordinates> coords = new ArrayList<Coordinates>(Arrays.asList(
                new Coordinates(1, 2), new Coordinates(5, 2), new Coordinates(5, 4), new Coordinates(9, 4),
                new Coordinates(9, 1), new Coordinates(3, 1), new Coordinates(3, 10),
                new Coordinates(6, 10)));

        coords = pathCorrector.removeCycles(coords);

        assertEquals(4, coords.size());
        assertEquals(new Coordinates(1, 2), coords.get(0));
        assertEquals(new Coordinates(3, 2), coords.get(1));
        assertEquals(new Coordinates(3, 10), coords.get(2));
        assertEquals(new Coordinates(6, 10), coords.get(3));
    }

    @Test
    public void remove_cycle_with_collinear_left_to_right_intersection() {
        List<Coordinates> coords = new ArrayList<Coordinates>(Arrays.asList(
                new Coordinates(1, 2), new Coordinates(5, 2), new Coordinates(5, 4), new Coordinates(9, 4),
                new Coordinates(9, 1), new Coordinates(3, 1), new Coordinates(3, 2),
                new Coordinates(7, 2)));


        coords = pathCorrector.removeCycles(coords);

        assertEquals(3, coords.size());
        assertEquals(new Coordinates(1, 2), coords.get(0));
        assertEquals(new Coordinates(3, 2), coords.get(1));
        assertEquals(new Coordinates(7, 2), coords.get(2));
    }

    @Test
    public void remove_cycle_with_collinear_right_to_left_intersection() {
        List<Coordinates> coords = new ArrayList<Coordinates>(Arrays.asList(
                new Coordinates(1, 2), new Coordinates(5, 2), new Coordinates(5, 4), new Coordinates(9, 4),
                new Coordinates(9, 2), new Coordinates(3, 2)));


        coords = pathCorrector.removeCycles(coords);

        assertEquals(2, coords.size());
        assertEquals(new Coordinates(1, 2), coords.get(0));
        assertEquals(new Coordinates(3, 2), coords.get(1));
    }
}
