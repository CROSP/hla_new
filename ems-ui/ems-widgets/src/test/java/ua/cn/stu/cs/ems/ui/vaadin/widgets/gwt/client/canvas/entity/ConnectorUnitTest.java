package ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity;

import org.junit.Before;
import org.junit.Test;
import org.vaadin.gwtgraphics.client.Group;
import org.vaadin.gwtgraphics.client.Line;
import org.vaadin.gwtgraphics.client.VectorObject;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.Coordinates;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.Grid;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.test.MockedCanvas;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.test.TestUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * @author n0weak
 */
public class ConnectorUnitTest {

    private Connector connector;
    private MockedGroup group;
    private MockedCanvas mockedCanvas;

    @Before
    public void setUp() {
        TestUtils.mockAll();
        group = new MockedGroup();

        Grid.setAdjustEnabled(false);
        mockedCanvas = new MockedCanvas();
        PathCorrector pathCorrector = new PathCorrector(mockedCanvas);
        pathCorrector.setEnabled(true);
        Connector.setPathCorrector(pathCorrector);
        PathCorrector.setHorizontalFirst(true);
    }

    @Test
    public void test_detach_without_fixes() {
        Coordinates begin = new Coordinates(10, 20);
        Coordinates end = new Coordinates(100, 100);

        ActiveEntity tail = EntityType.PLACE.createEntity("", begin.x, begin.y);
        ActiveEntity head = EntityType.JTRANSITION.createEntity("", end.x, end.y);
        initConnector(tail, begin);

        connector.drawPathTo(end);
        connector.attachHead(head, end);
        connector.detachHead();


        checkFixedParts(begin);
        checkPendingPart();
        checkTailCoords(begin);
    }

    @Test
    public void test_detach_with_fixes() {
        Coordinates begin = new Coordinates(10, 20);
        Coordinates end = new Coordinates(100, 100);
        Coordinates middle = new Coordinates(end.x, begin.y);

        ActiveEntity tail = EntityType.PLACE.createEntity("", begin.x, begin.y);
        ActiveEntity head = EntityType.JTRANSITION.createEntity("", end.x, end.y);
        initConnector(tail, begin);
        connector.drawPathTo(middle);
        connector.fixHead();
        connector.drawPathTo(end);
        connector.attachHead(head, new Coordinates(end.x, end.y));
        connector.detachHead();

        checkFixedParts(begin, middle);
        checkPendingPart();
//        checkDrownPaths(2);
        checkTailCoords(begin);
    }

    @Test
    public void check_state_after_tail_attaching() {
        Coordinates begin = new Coordinates(10, 20);
        ActiveEntity tail = EntityType.PLACE.createEntity("", begin.x, begin.y);
        initConnector(tail, begin);

        checkTailCoords(begin);
        checkFixedParts(begin);
        checkPendingPart();
        checkDrownPaths(0);
    }

    @Test
    public void check_state_after_path_drawing() {
        Coordinates begin = new Coordinates(10, 20);
        Coordinates end = new Coordinates(100, 100);
        Coordinates middle = new Coordinates(end.x, begin.y);

        ActiveEntity tail = EntityType.PLACE.createEntity("", begin.x, begin.y);
        initConnector(tail, begin);
        connector.drawPathTo(end);

        checkFixedParts(begin);
        checkPendingPart(middle, end);
        checkDrownPaths(2);
        checkTailCoords(begin);
    }

    @Test
    public void check_state_after_head_fixing() {
        Coordinates begin = new Coordinates(10, 20);
        Coordinates end = new Coordinates(100, 100);
        Coordinates middle = new Coordinates(end.x, begin.y);

        ActiveEntity tail = EntityType.PLACE.createEntity("", begin.x, begin.y);
        initConnector(tail, begin);
        connector.drawPathTo(end);
        connector.fixHead();

        checkFixedParts(begin, middle, end);
        checkPendingPart();
        checkDrownPaths(2);
        checkTailCoords(begin);
    }

    @Test
    public void check_state_after_tail_moving() {
        Coordinates begin = new Coordinates(10, 20);
        Coordinates end = new Coordinates(100, 100);
        Coordinates newBegin = new Coordinates(0, begin.y);
        Coordinates newMiddle = new Coordinates(end.x, newBegin.y);

        ActiveEntity tail = EntityType.PLACE.createEntity("", begin.x, begin.y);
        initConnector(tail, begin);
        connector.drawPathTo(end);
        connector.fixHead();
        connector.moveTail(newBegin);

        checkFixedParts(newBegin, newMiddle, end);
        checkPendingPart();
        checkDrownPaths(2);
        checkTailCoords(newBegin);
    }

    @Test
    public void test_repaint_after_scale_change() {
        Scaler.setScale(1);
        Coordinates begin = new Coordinates(12, 25);
        Coordinates end1 = new Coordinates(123, 156);
        Coordinates middle = new Coordinates(end1.x, begin.y);
        Coordinates end2 = new Coordinates(288, 293);
        Coordinates middle2 = new Coordinates(end2.x, end1.y);

        ActiveEntity tail = EntityType.PLACE.createEntity("", begin.x, begin.y);
        initConnector(tail, begin);
        connector.drawPathTo(end1);
        connector.fixHead();
        connector.drawPathTo(end2);

        checkPath(Scaler.scale(begin), Scaler.scale(middle), Scaler.scale(end1), Scaler.scale(middle2), Scaler.scale(
                end2));
        try {
            Scaler.setScale(0.6);
            connector.repaint();

            checkPath(Scaler.scale(begin), Scaler.scale(middle), Scaler.scale(end1), Scaler.scale(middle2),
                      Scaler.scale(end2));

            Scaler.setScale(0.2);
            connector.repaint();

            checkPath(Scaler.scale(begin), Scaler.scale(middle), Scaler.scale(end1), Scaler.scale(middle2),
                      Scaler.scale(end2));

            Scaler.setScale(1);
            connector.repaint();

            checkPath(Scaler.scale(begin), Scaler.scale(middle), Scaler.scale(end1), Scaler.scale(middle2),
                      Scaler.scale(end2));
        } finally {
            Scaler.setScale(1);
        }

    }

    /*
     * NOT SUPPORTED RIGHT TO LEFT CONNECTION
     */
//    @Test
//    public void right_to_left_connection_with_correction() {
//        // FIX this test to anable compiling with new version
//        Coordinates begin = new Coordinates(200, 60);
//        Coordinates middle = new Coordinates(140, begin.y);
//        Coordinates end = new Coordinates(20, begin.y);
//
//        ActiveEntity place = EntityType.PLACE.createEntity("", begin.x, begin.y);
//        ActiveEntity transition = EntityType.TTRANSITION.createEntity("", end.x, end.y);
//
//        List<ActiveEntity> candidates = new ArrayList<ActiveEntity>(Arrays.asList(place, transition));
//        mockedCanvas.getEntities().addAll(candidates);
//
//        initConnector(place, begin, false);
//        connector.drawPathTo(middle);
//        connector.fixHead();
//        transition.connectInput(connector, end);
//        connector.fixHead();
//        int gap = Scaler.scale(Grid.getStep());
//
//        Coordinates placeOut = place.getOutputCoordinates(0);
//        Coordinates transitionIn = transition.getInputCoordinates(0);
//
//        Coordinates c2 = new Coordinates(placeOut.x + gap, placeOut.y);
//        Coordinates c3 = new Coordinates(c2.x, place.getTopLeft().y - gap);
//        Coordinates c4 = new Coordinates(middle.x, c3.y);
//
//        Coordinates c6 = new Coordinates(transition.getTopLeft().x + transition.getWidth() + gap, middle.y);
//        Coordinates c7 = new Coordinates(c6.x, transition.getTopLeft().y - gap);
//        Coordinates c8 = new Coordinates(transitionIn.x - Connector.ARROW_LENGTH, c7.y);
//        Coordinates c9 = new Coordinates(c8.x, transitionIn.y);
//
//        checkPendingPart();
//        checkFixedParts(placeOut, c2, c3, c4, middle, c6, c7, c8, c9);
//        checkDrownPaths(9); //8 lines of path + arrow
//    }

    private void initConnector(ActiveEntity entity, Coordinates c) {
        initConnector(entity, c, true);
    }

    private void initConnector(ActiveEntity entity, Coordinates c, boolean force) {
        connector = entity.connectOutput(c);
        connector.setGroup(group);
        if (force) {
            connector.setTailCoords(c);
        }
    }

    private void checkTailCoords(Coordinates c) {
        assertEquals(c, connector.getTailCoords());
    }

    private void checkDrownPaths(int count) {
//        assertEquals(count, group.getChildCount());
    }

    private void checkPendingPart(Coordinates... allSteps) {
        if (allSteps.length > 0) {
            checkSteps(connector.getPendingCoords(), allSteps);
        }
        else {
            assertEquals(0, connector.getPendingCoords().size());
        }
    }

    private void checkFixedParts(Coordinates... allSteps) {
        checkSteps(connector.getCoordinates(), allSteps);
    }

    private void checkPath(Coordinates... steps) {
        List<Line> path = connector.getPath();
        assertEquals(steps.length - 1, path.size());
        for (int j = 0; j < steps.length - 1; j++) {
            Coordinates a = steps[j];
            Coordinates b = steps[j + 1];
            Line l = path.get(j);
            assertEquals(a.x, l.getX1());
            assertEquals(a.y, l.getY1());
            assertEquals(b.x, l.getX2());
            assertEquals(b.y, l.getY2());
        }
    }

    private void checkSteps(List<Coordinates> path, Coordinates... allSteps) {
        assertEquals(path.size(), allSteps.length);
        for (int i = 0; i < allSteps.length; i++) {
            Coordinates expectedStep = allSteps[i];
            Coordinates actualStep = path.get(i);
            assertEquals(expectedStep.x, actualStep.x);
            assertEquals(expectedStep.y, actualStep.y);
        }
    }

    private static class MockedGroup extends Group {

        private int childCount;

        @Override
        public VectorObject add(VectorObject vo) {
            childCount++;
            return vo;
        }

        @Override
        public VectorObject remove(VectorObject vo) {
            childCount--;
            return vo;
        }

        public int getChildCount() {
            return childCount;
        }
    }
}
