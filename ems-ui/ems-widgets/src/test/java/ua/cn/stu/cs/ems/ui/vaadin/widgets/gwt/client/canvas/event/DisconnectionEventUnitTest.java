package ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.event;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * @author n0weak
 */
public class DisconnectionEventUnitTest {

    @Test
    public void simpleDeserialization() {
        String s = "p1" + Stringalizable.DELIM + "t1" + Stringalizable.DELIM + "INPUT" + Stringalizable.DELIM + Stringalizable.DELIM
                + "p2" + Stringalizable.DELIM + "t2" + Stringalizable.DELIM + "OUTPUT" + Stringalizable.DELIM + Stringalizable.DELIM
                + "p3" + Stringalizable.DELIM + "t3" + Stringalizable.DELIM + "INPUT" + Stringalizable.DELIM;

        DisconnectionEvent event = DisconnectionEvent.deserialize(s);

        assertEquals(3, event.getDetails().size());
        assertEquals("t1", event.getDetails().get(0).getTransitionId());
        assertEquals("p1", event.getDetails().get(0).getPlaceId());
        assertEquals(DisconnectionEvent.DisconnectionDetails.ConnectionType.INPUT, event.getDetails().get(0).
                getConnectionType());
        assertEquals("t2", event.getDetails().get(1).getTransitionId());
        assertEquals("p2", event.getDetails().get(1).getPlaceId());
        assertEquals(DisconnectionEvent.DisconnectionDetails.ConnectionType.OUTPUT, event.getDetails().get(1).
                getConnectionType());
        assertEquals("t3", event.getDetails().get(2).getTransitionId());
        assertEquals("p3", event.getDetails().get(2).getPlaceId());
        assertEquals(DisconnectionEvent.DisconnectionDetails.ConnectionType.INPUT, event.getDetails().get(2).
                getConnectionType());
    }

    @Test
    public void simpleSerialization() {
        String s = "ppp1" + Stringalizable.DELIM + "ttt1" + Stringalizable.DELIM + "INPUT" + Stringalizable.DELIM + Stringalizable.DELIM
                + "ppp2" + Stringalizable.DELIM + "ttt2" + Stringalizable.DELIM + "OUTPUT" + Stringalizable.DELIM + Stringalizable.DELIM;

        List<DisconnectionEvent.DisconnectionDetails> list = new ArrayList<DisconnectionEvent.DisconnectionDetails>();
        list.add(
                new DisconnectionEvent.DisconnectionDetails("ppp1", "ttt1",
                        DisconnectionEvent.DisconnectionDetails.ConnectionType.INPUT, ""));
        list.add(
                new DisconnectionEvent.DisconnectionDetails("ppp2", "ttt2",
                        DisconnectionEvent.DisconnectionDetails.ConnectionType.OUTPUT, ""));

        DisconnectionEvent event = new DisconnectionEvent(list);

        assertEquals(s, event.serialize());
    }
}
