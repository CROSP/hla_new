package ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.event;

import org.junit.Test;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.Coordinates;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.EntityType;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

/**
 * @author n0weak
 */
public class ConnectionEventUnitTest {

    @Test
    public void simpleDeserialization() {
        String s = "a1" + Stringalizable.DELIM
                + "PLACE" + Stringalizable.DELIM + "1" + Stringalizable.DELIM + "1" + Stringalizable.DELIM
                + "11" + Stringalizable.DELIM + "12" + Stringalizable.DELIM
                + "O0" + Stringalizable.DELIM
                + "a2" + Stringalizable.DELIM
                + "PFIFO" + Stringalizable.DELIM + "1" + Stringalizable.DELIM + "1" + Stringalizable.DELIM
                + "21" + Stringalizable.DELIM + "22" + Stringalizable.DELIM
                + "I0" + Stringalizable.DELIM
                + "1" + Stringalizable.DELIM + "2" + Stringalizable.DELIM
                + "3" + Stringalizable.DELIM + "4" + Stringalizable.DELIM;

        ConnectionEvent event = ConnectionEvent.deserialize(s);

        assertEquals(EntityType.PLACE, event.getFrom().getEntityType());
        assertEquals("a1", event.getFrom().getId());
        assertEquals(11, event.getFrom().getCenterCoords().x);
        assertEquals(12, event.getFrom().getCenterCoords().y);
        assertEquals(EntityType.PFIFO, event.getTo().getEntityType());
        assertEquals("a2", event.getTo().getId());
        assertEquals(21, event.getTo().getCenterCoords().x);
        assertEquals(22, event.getTo().getCenterCoords().y);

        assertEquals(2, event.getFixedCoords().size());
        assertEquals(new Coordinates(1, 2), event.getFixedCoords().toArray(
                new Coordinates[event.getFixedCoords().size()])[0]);
        assertEquals(new Coordinates(3, 4), event.getFixedCoords().toArray(
                new Coordinates[event.getFixedCoords().size()])[1]);
    }

    @Test
    public void simpleSerialization() {
        String s = "id" + Stringalizable.DELIM 
                + "FIFO" + Stringalizable.DELIM + "1" + Stringalizable.DELIM + "1" + Stringalizable.DELIM
                + "11" + Stringalizable.DELIM + "12" + Stringalizable.DELIM
                + "O0" + Stringalizable.DELIM
                + "id2" + Stringalizable.DELIM 
                + "PLIFO" + Stringalizable.DELIM + "1" + Stringalizable.DELIM + "1" + Stringalizable.DELIM
                + "21" + Stringalizable.DELIM + "22" + Stringalizable.DELIM
                + "I0" + Stringalizable.DELIM
                + "1" + Stringalizable.DELIM + "2" + Stringalizable.DELIM 
                + "3" + Stringalizable.DELIM + "4";

        EntityType type = EntityType.FIFO;
        type.setEntityConnectorsCount(1, 1);
        EntityDetails ed1 = new EntityDetails("id", type, new Coordinates(11, 12));

        type = EntityType.PLIFO;
        type.setEntityConnectorsCount(1, 1);
        EntityDetails ed2 = new EntityDetails("id2", type, new Coordinates(21, 22));
        
        ConnectionEvent event = new ConnectionEvent(ed1, ed2, "O0", "I0",
                Arrays.asList(new Coordinates(1, 2), new Coordinates(3, 4)));

        assertEquals(s, event.serialize());
    }
}
