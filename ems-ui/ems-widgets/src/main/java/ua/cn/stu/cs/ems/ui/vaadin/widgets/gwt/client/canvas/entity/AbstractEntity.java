package ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity;

import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.DoubleClickHandler;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverHandler;
import org.vaadin.gwtgraphics.client.DrawingArea;
import org.vaadin.gwtgraphics.client.VectorObject;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.Coordinates;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.drawer.Drawer;

import java.util.*;

/**
 * Basic implementation of  {@link CanvasEntity} interface.
 *
 * @author n0weak
 */
abstract class AbstractEntity<DRAWER extends Drawer> implements ActiveEntity {
// ------------------------------ FIELDS ------------------------------

    protected Set<Connector> inputConnectors = new HashSet<Connector>();
    protected Set<Connector> outputConnectors = new HashSet<Connector>();
    protected DRAWER drawer;
    private String id;
    private boolean enabled = true;
    private boolean selected;
    private final String typeRepresentation;

// --------------------------- CONSTRUCTORS ---------------------------
    /**
     * @param id     identifier of this entity
     * @param drawer {@link Drawer} which is able to paint this entity.
     */
    protected AbstractEntity(EntityType type, String id, DRAWER drawer) {
        this.id = id;
        this.drawer = drawer;
        this.typeRepresentation = type.serialize();
    }

// --------------------- GETTER / SETTER METHODS ---------------------
    public String getId() {
        return id;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public EntityType getType() {
        return EntityType.deserialize(typeRepresentation);
    }

//--------------------------------------------------------------------
    private boolean isConnected(Integer connectorId, boolean output) {
        for (Connector c : output ? outputConnectors : inputConnectors) {
            if ((!output && c.getInputNum() != null && c.getInputNum().equals(connectorId))
                    || (output && c.getOutputNum() != null && c.getOutputNum().equals(connectorId))) {
                return true;
            }
        }
        return false;
    }

    /*
     * searching a free input or isOutput
     */
    private int findFreeConnector(int connectorId, boolean isOutput) {
        if (connectorId < 0) {
            connectorId = 0;
        }

        int connects = 0;
        while (connects < getDrawer().getSticksCount(isOutput)) {
            if (isConnected(connectorId, isOutput)) {
                connects++;
            } else {
                return connectorId;
            }
        }

        return -1;
    }

    private Connector connectOutputConnector(int id) {
        Coordinates coordinates = getOutputCoordinates(id);
        Connector connector = new Connector(this, coordinates);
        connector.setOutputNum(id);

        outputConnectors.add(connector);
        return connector;
    }

    public void connectInputConnector(Connector connector, int id) {
        Coordinates coordinates = getInputCoordinates(id);
        connector.attachHead(this, coordinates);
        connector.setInputNum(id);
        inputConnectors.add(connector);
    }

// ------------------------ INTERFACE METHODS ------------------------
// --------------------- Interface ActiveEntity ---------------------
    public void move(Coordinates mousePos) {
        drawer.move(mousePos);
        fix();
    }

    public Connector connectOutput(String outputName) {
        for (int i = 0; i < getDrawer().getSticksCount(true); i++) {
            if (getDrawer().getOutputName(i).equals(outputName)) {
                if (!isConnected(i, true)) {
                    return connectOutputConnector(i);
                }
                else {
                    return null;
                }
            }
        }
        return null;
    }

    public Connector connectOutput(Coordinates mousePos) {
        int outId = findFreeConnector(getDrawer().getOutputId(mousePos), true);
        if (outId < 0) {
            return null;
        }

        return connectOutputConnector(outId);
    }

    public void connectInput(Connector connector, String inputName) {
        for (int i = 0; i < getDrawer().getSticksCount(false); i++) {
            if (getDrawer().getInputName(i).equals(inputName)) {
                if (!isConnected(i, false)) {
                    connectInputConnector(connector, i);
                }
                return;
            }
        }
    }
    
    public void connectInput(Connector connector, Coordinates mousePos) {
        if (!inputConnectors.contains(connector)) {
            int inId = findFreeConnector(getDrawer().getInputId(mousePos), false);

            if (inId < 0) {
                return;
            }
            connectInputConnector(connector, inId);
        }
    }

    public void disconnect(Connector connector) {
        outputConnectors.remove(connector);
        if (inputConnectors.remove(connector)) {
            connector.detachHead();
            connector.setInputNum(null);
        }
    }

    public void fix() {
        int i = 0;
        for (Connector connector : outputConnectors) {
            Coordinates outputCoordinates = getOutputCoordinates(connector.getOutputNum());
            connector.moveTail(outputCoordinates);
            i++;
        }
        i = 0;
        for (Connector connector : inputConnectors) {
            Coordinates inputCoordinates = getInputCoordinates(connector.getInputNum());
            connector.moveHead(inputCoordinates);
            connector.fixHead();
            i++;
        }
    }

    public void setEnabled(boolean enabled) {
        if (enabled != isEnabled()) {
            drawer.setEnabled(enabled);
            this.enabled = enabled;
        }
    }

    public void addDoubleClickHandler(DoubleClickHandler handler) {
        getVectorObject().addDoubleClickHandler(handler);
    }

    public void addClickHandler(ClickHandler handler) {
        getVectorObject().addClickHandler(handler);
    }

    public void addMouseOutHandler(MouseOutHandler handler) {
        getVectorObject().addMouseOutHandler(handler);
    }

    public void addMouseOverHandler(MouseOverHandler handler) {
        getVectorObject().addMouseOverHandler(handler);
    }

    public void setId(String id) {
        this.id = id;
        drawer.setId(id);
    }

    /*
     * count of connectors that connected!!!
     */
    public int getConnectorsCount(boolean output) {
        if (output) {
            return outputConnectors.size();
        } else {
            return inputConnectors.size();
        }
    }

    public Collection<Connector> getOutputConnectors() {
        return outputConnectors;
    }

    public Collection<Connector> getInputConnectors() {
        return inputConnectors;
    }

    public Drawer getDrawer() {
        return drawer;
    }

    /**
     * Defines coordinates of input position by connector's index.
     *
     * @param connectorIdx index of the connector.
     * @return coordinates of the input position.
     */
    public Coordinates getInputCoordinates(int connectorIdx) {
        return drawer.getInputCoordinates(connectorIdx);
    }

    /**
     * Defines coordinates of output position by connector's index.
     *
     * @param connectorIdx index of the connector.
     * @return coordinates of the output position.
     */
    public Coordinates getOutputCoordinates(int connectorIdx) {
        return drawer.getOutputCoordinates(connectorIdx);
    }

// --------------------- Interface CanvasEntity ---------------------
    public void delete(DrawingArea canvas) {
        List<Connector> connectors = new ArrayList<Connector>(outputConnectors);
        connectors.addAll(inputConnectors);

        for (Connector c : connectors) {
            c.delete(canvas);
        }

        canvas.remove(this.getVectorObject());
    }

// --------------------- Interface Drawable ---------------------
    public VectorObject getVectorObject() {
        return drawer.getVectorObject();
    }

// --------------------- Interface Informative ---------------------
    public Coordinates getTopLeft() {
        return drawer.getTopLeft();
    }

    public int getWidth() {
        return drawer.getWidth();
    }

    public int getHeight() {
        return drawer.getHeight();
    }

    public boolean intersectsH(int y, int x1, int x2) {
        return drawer.intersectsH(y, x1, x2);
    }

    public boolean intersectsV(int x, int y1, int y2) {
        return drawer.intersectsV(x, y1, y2);
    }

    public int getX() {
        return drawer.getX();
    }

    public int getY() {
        return drawer.getY();
    }

    public Coordinates getCenter() {
        return drawer.getCenter();
    }

// --------------------- Interface Repaintable ---------------------
    public void repaint() {
        drawer.repaint();
    }

// --------------------- Interface Selectable ---------------------
    public boolean isContainedIn(int x1, int y1, int x2, int y2) {
        return drawer.isContainedIn(x1, y1, x2, y2);
    }

    public void setSelected(boolean selected) {
        if (selected != this.selected) {
            drawer.setSelected(selected);
            this.selected = selected;
        }
    }
}
