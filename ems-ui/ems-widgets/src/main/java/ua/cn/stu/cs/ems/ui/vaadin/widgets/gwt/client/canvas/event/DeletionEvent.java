package ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.event;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

/**
 * @author n0weak
 */
public class DeletionEvent implements Stringalizable {
// ------------------------------ FIELDS ------------------------------

    private final List<EntityDetails> deletedEntities;

// -------------------------- STATIC METHODS --------------------------
    /**
     * Constructs entity from its serialized representation. Consumes all the input, trying to convert it into the list of {@link EntityDetails}.
     *
     * @param serializedString serialized representation of the entity.
     * @return deserialized entity if process was successful.
     * @throws IllegalArgumentException if deserialization failed
     * @see #serialize()
     */
    public static DeletionEvent deserialize(String serializedString) {
        try {
            return deserialize(EventUtils.tokenize(serializedString));
        } catch (Exception e) {
            throw new IllegalArgumentException("Cannot deserialize DeletionEvent from supplied data: "
                                               + serializedString, e);
        }
    }

    static DeletionEvent deserialize(Queue<String> tokens) {
        List<EntityDetails> list = new ArrayList<EntityDetails>();
        while (!tokens.isEmpty()) {
            list.add(EntityDetails.deserialize(tokens));
        }
        return new DeletionEvent(list);
    }

// --------------------------- CONSTRUCTORS ---------------------------
    /**
     * Constructs event instance without making safe copy of the list passed.
     *
     * @param deletedEntities list that contains information about deleted entities
     */
    public DeletionEvent(List<EntityDetails> deletedEntities) {
        this.deletedEntities = deletedEntities;
    }

// --------------------- GETTER / SETTER METHODS ---------------------
    public List<EntityDetails> getDeletedEntities() {
        return deletedEntities;
    }

// ------------------------ INTERFACE METHODS ------------------------
// --------------------- Interface Stringalizable ---------------------
    public String serialize() {
        StringBuilder sb = new StringBuilder();
        for (EntityDetails details : deletedEntities) {
            sb.append(details.serialize()).append(DELIM);
        }
        return sb.toString();
    }
}
