package ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity;

import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.Coordinates;

import java.util.ArrayList;
import java.util.List;

/**
 * Abstract class which facilitates implementation of scalable repainting. Contains current scale value which is common for all the entities on the canvas.
 * Provides some useful scaling methods.
 *
 * @author n0weak
 */
public class Scaler {

    private static double scale = 1;

    /**
     * @return value of the scale which is common for all the entities on the canvas.
     */
    public static double getScale() {
        return scale;
    }

    /**
     * Sets common scale value.
     *
     * @param scale common scale value to be set.
     */
    public static void setScale(double scale) {
        Scaler.scale = scale;
    }

    /**
     * 'Unscales' the value, i.e. evaluates original representation of the specified value assuming that it was previously recounted with the current scale.
     *
     * @param val previously scaled value.
     * @return evaluated unscaled value.
     */
    public static int unscale(int val) {
        return (int) (val / getScale());
    }

    /**
     * Recounts specified value with the current scale.
     *
     * @param val value to be recounted.
     * @return recounted value.
     */
    public static int scale(int val) {
        return (int) Math.ceil(val * getScale());
    }

    /**
     * 'Unscales' x and y fields of coordinates, i.e. evaluates original representation of the specified coordinates assuming that it was previously recounted with the current scale.
     *
     * @param c previously scaled coordinates.
     * @return evaluated unscaled coordinates.
     */
    public static Coordinates unscale(Coordinates c) {
        return new Coordinates(unscale(c.x), unscale(c.y));
    }

    /**
     * Recounts x and y fields of the specified coordinates with the current scale.
     *
     * @param c coordinates to be recounted.
     * @return recounted coordinates.
     */
    public static Coordinates scale(Coordinates c) {
        return new Coordinates(scale(c.x), scale(c.y));
    }

    public static List<Coordinates> unscale(List<Coordinates> coords) {
        List<Coordinates> result = new ArrayList<Coordinates>(coords.size());
        for (Coordinates c : coords) {
            result.add(unscale(c));
        }
        return result;
    }

    private Scaler() {
    }
}
