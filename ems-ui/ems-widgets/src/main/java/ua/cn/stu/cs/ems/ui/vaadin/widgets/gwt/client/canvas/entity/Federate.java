package ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity;

import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.drawer.FederateDrawer;

/**
 * @author Leonid
 */
class Federate extends AbstractEntity<FederateDrawer> implements ActiveEntity {
    
    Federate(EntityType type, String id, FederateDrawer drawer) {
        super(type, id, drawer);
    }
    
    public boolean acceptsConnection(ActiveEntity entity) {
        return false;
    }

    public boolean acceptsConnection() {
        return false;
    }
}
