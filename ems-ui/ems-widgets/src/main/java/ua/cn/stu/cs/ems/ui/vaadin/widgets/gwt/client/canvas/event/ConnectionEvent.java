package ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.event;

import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.Coordinates;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.Connector;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Queue;

/**
 * @author n0weak
 */
public class ConnectionEvent implements Stringalizable {
// ------------------------------ FIELDS ------------------------------

    private final EntityDetails from;
    private final EntityDetails to;
    private final String outputName;
    private final String inputName;
    private final Collection<Coordinates> fixedCoords;

// -------------------------- STATIC METHODS --------------------------
    /**
     * Constructs entity from its serialized representation. Consumes all the input, trying to convert it into the list of connector's coordinates;
     *
     * @param serializedString serialized representation of the entity.
     * @return deserialized entity if process was successful.
     * @throws IllegalArgumentException if deserialization failed
     * @see #serialize()
     */
    public static ConnectionEvent deserialize(String serializedString) {
        try {
            return deserialize(EventUtils.tokenize(serializedString));
        } catch (Exception e) {
            throw new IllegalArgumentException("Cannot deserialize ConnectionEvent from supplied data: "
                                               + serializedString, e);
        }
    }

    static ConnectionEvent deserialize(Queue<String> tokens) {
        EntityDetails from = EntityDetails.deserialize(tokens);
        String outputName = tokens.poll();
        EntityDetails to = EntityDetails.deserialize(tokens);
        String inputName = tokens.poll();
        Collection<Coordinates> fixedCoords = new ArrayList<Coordinates>();
        while (!tokens.isEmpty()) {
            fixedCoords.add(new Coordinates(Integer.valueOf(tokens.poll()), Integer.valueOf(tokens.poll())));
        }
        return new ConnectionEvent(from, to, outputName, inputName, fixedCoords);
    }

// --------------------------- CONSTRUCTORS ---------------------------
    public ConnectionEvent(EntityDetails from, EntityDetails to, 
            String outputName, String inputName, 
            Collection<Coordinates> fixedCoords) {
        this.from = from;
        this.to = to;
        this.outputName = outputName;
        this.inputName = inputName;
        this.fixedCoords = fixedCoords;
    }
    
    public ConnectionEvent(Connector connector) {
        this.from = new EntityDetails(connector.getTailEntity());
        this.to = new EntityDetails(connector.getHeadEntity());
        this.outputName = connector.getOutputName();
        this.inputName = connector.getInputName();
        this.fixedCoords = connector.getPersistentCoordinates();
    }

// --------------------- GETTER / SETTER METHODS ---------------------
    public EntityDetails getFrom() {
        return from;
    }

    public EntityDetails getTo() {
        return to;
    }

    public Collection<Coordinates> getFixedCoords() {
        return fixedCoords;
    }

    public String getInputName() {
        return inputName;
    }
    
    public String getOutputName() {
        return outputName;
    }
    // ------------------------ INTERFACE METHODS ------------------------
// --------------------- Interface Stringalizable ---------------------
    public String serialize() {
        StringBuilder sb = new StringBuilder().append(from.serialize()).append(DELIM).
                append(outputName).append(DELIM).
                append(to.serialize()).append(DELIM).
                append(inputName);
        for (Coordinates c : fixedCoords) {
            sb.append(DELIM).append(c.x).append(DELIM).append(c.y);
        }
        return sb.toString();
    }
}
