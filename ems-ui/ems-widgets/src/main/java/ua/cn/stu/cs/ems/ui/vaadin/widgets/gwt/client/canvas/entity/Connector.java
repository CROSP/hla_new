package ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity;

import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import org.vaadin.gwtgraphics.client.DrawingArea;
import org.vaadin.gwtgraphics.client.Group;
import org.vaadin.gwtgraphics.client.Line;
import org.vaadin.gwtgraphics.client.VectorObject;
import org.vaadin.gwtgraphics.client.shape.Path;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.Coordinates;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.Grid;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.drawer.Drawer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.Scaler.scale;
import static ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.Scaler.unscale;

/**
 * @author n0weak
 */
public class Connector implements CanvasEntity {
// ------------------------------ FIELDS ------------------------------

    static final int ARROW_LENGTH = 20;
    static boolean ARROW_PAINTED = false;
    /**
     * Path corrector which is used during the drawing process. If null then no correction is performed.
     */
    private static PathCorrector pathCorrector;
    /**
     * Groups all the elements that represent connector's view
     */
    private Group group = new Group();
    /**
     * Part of the view, represents arrow on the head of the connector.
     */
    private Path arrow;
    /**
     * Entity that is attach to connector's head. Note that connector's head is attached to entity's input (tail).
     */
    private ActiveEntity headEntity;
    /**
     * Entity that is attach to connector's tail. Note that connector's head is attached to entity's output (head).
     */
    private final ActiveEntity tailEntity;
    
    /**
     * Name of head entity's input that connected
     */
    private String inputName;
    
    /**
     * Name of tail entity's input that connected
     */
    private final String outputName;
    
    private Integer inputNum;
    
    private Integer outputNum;
    
    /**
     * Graphical representation of this connector.
     * Usage of {@link Path} element leads to some problems with selection (folded path responds on all clicks in a big enough area around it),
     * so the list of {@link Line} is used instead.
     */
    private List<Line> path;
    /**
     * Each entry of this list represents coordinates belonging to different steps of the fixed part of this connector.
     * All coordinates are unscaled, hence must be scaled before drawing.
     */
    private List<Coordinates> coordinates = new ArrayList<Coordinates>();
    /**
     * Coordinates representing varying part of this connector.
     * All coordinates are unscaled, hence must be scaled before drawing.
     */
    private List<Coordinates> pendingCoords = new ArrayList<Coordinates>();

// -------------------------- STATIC METHODS --------------------------
    public static void setPathCorrector(PathCorrector pathCorrector) {
        Connector.pathCorrector = pathCorrector;
    }

// --------------------------- CONSTRUCTORS ---------------------------
    /**
     * Creates connector with specified tail tailEntity
     *
     * @param tailEntity tail of this connector will e attached to output place of this entity.
     * @param position   of the output place to attach connector's head entity.
     */
    public Connector(ActiveEntity tailEntity, Coordinates position) {
        if (null == tailEntity || null == position) {
            throw new IllegalArgumentException("Both tailEntity and position parameters are not nullable!");
        }
        
        this.tailEntity = tailEntity;
        this.outputName = tailEntity.getDrawer().getOutputName(tailEntity.getDrawer().getOutputId(position));
        this.coordinates.add(unscale(position));
    }

// --------------------- GETTER / SETTER METHODS ---------------------
    public List<Coordinates> getCoordinates() {
        return coordinates;
    }

    public List<Coordinates> getPersistentCoordinates() {
        List<Coordinates> persistentCoords = new ArrayList<Coordinates>(coordinates);
        if (!persistentCoords.isEmpty()) {
            persistentCoords.remove(0);   //first coordinate is generated from position of the tail entity
        }
        if (!persistentCoords.isEmpty()) {
            persistentCoords.remove(persistentCoords.size() - 1);  //last coordinate is generated from position of the head entity
        }
        return persistentCoords;
    }

    /**
     * @return entity that is attached to the head of this connector
     */
    public ActiveEntity getHeadEntity() {
        return headEntity;
    }

    public List<Line> getPath() {
        return path;
    }

    public List<Coordinates> getPendingCoords() {
        return pendingCoords;
    }

    public Coordinates getTailCoords() {
        return coordinates.isEmpty() ? null : coordinates.get(0);
    }

    public void setTailCoords(Coordinates tailCoords) {
        if (null == tailCoords) {
            throw new IllegalArgumentException("tail coordinates cannot be null!");
        }
        if (coordinates.isEmpty()) {
            coordinates.add(tailCoords);
        }
        else {
            coordinates.set(0, tailCoords);
        }
    }

    /**
     * @return entity that is attached to the tail of this connector
     */
    public ActiveEntity getTailEntity() {
        return tailEntity;
    }

    void setGroup(Group group) {
        this.group = group;
    }

// ------------------------ INTERFACE METHODS ------------------------
// --------------------- Interface CanvasEntity ---------------------
    /**
     * Disconnects itself from tail & head entities using {@link #disconnect()} method and removes itself from the canvas.
     *
     * @param canvas canvas that contains entities.
     */
    public void delete(DrawingArea canvas) {
        disconnect();
        canvas.remove(getVectorObject());
    }

// --------------------- Interface Drawable ---------------------
    public VectorObject getVectorObject() {
        return group;
    }

// --------------------- Interface Repaintable ---------------------
    public void repaint() {
        if (!coordinates.isEmpty()) {
            redrawPath(coordinates, pendingCoords);
        }

        if (null != arrow) {
            group.remove(arrow);
            int arrowLength = getArrowLength();
            Coordinates lastCoords = scale(getLastCoords());
            arrow = createArrow(lastCoords.x + arrowLength, lastCoords.y, arrowLength);
            group.add(arrow);
        }
    }

// --------------------- Interface Selectable ---------------------
    public boolean isContainedIn(int x1, int y1, int x2, int y2) {
        return null != tailEntity && null != headEntity && tailEntity.isContainedIn(x1, y1, x2, y2) && headEntity.
                isContainedIn(x1, y1, x2, y2);
    }

    public void setSelected(boolean selected) {
        if (selected) {
            if (null != path) {
                for (Line l : path) {
                    l.setStrokeColor(Drawer.COLOR_SELECTED);
                }
            }
            if (null != arrow) {
                arrow.setStrokeColor(Drawer.COLOR_SELECTED);
                arrow.setFillColor(Drawer.COLOR_SELECTED);
            }
        }
        else {
            if (null != path) {
                for (Line l : path) {
                    l.setStrokeColor(Drawer.COLOR_DEFAULT);
                }
            }
            if (null != arrow) {
                arrow.setStrokeColor(Drawer.COLOR_DEFAULT);
                arrow.setFillColor(Drawer.COLOR_DEFAULT);
            }
        }
    }

// -------------------------- OTHER METHODS --------------------------
    public HandlerRegistration addClickHandler(ClickHandler handler) {
        return group.addClickHandler(handler);
    }

    /**
     * Disconnects this connector from its tail and head entities.
     */
    public void disconnect() {
        if (null != tailEntity) {
            tailEntity.disconnect(this);
        }
        if (null != headEntity) {
            headEntity.disconnect(this);
        }
    }

    public void fix(Coordinates coordinates) {
        drawPathTo(coordinates);
        fixHead();
    }

    public void drawPathTo(Coordinates coordinates) {
        Coordinates lastCoords = getLastCoords();
        pendingCoords = getCorrectedCoords(lastCoords, unscale(coordinates));

        pendingCoords.remove(0); //first coordinate of that lst was obtained via getLastCoords() call, hence it is 
                                 //already saved
        redrawPath(this.coordinates, pendingCoords);
    }

    private Coordinates getLastCoords() {
        if (coordinates.isEmpty()) {
            return null;
        }

        return coordinates.get(coordinates.size() - 1);
    }

    /**
     * Uses pathCorrector to remove intersections where possible
     *
     * @param begin first coordinate of the path, must be unscaled
     * @param end   last coordinate of the path, must be unscaled
     * @return unscaled corrected coordinates
     */
    private List<Coordinates> getCorrectedCoords(Coordinates begin, Coordinates end) {
        if (null != pathCorrector) {
            //do not have to bypass tail entity if drawing left-to-tight lines
            List<ActiveEntity> skippedEntities = begin.x < end.x && null != tailEntity ? Arrays.asList(tailEntity)
                                                 : new ArrayList<ActiveEntity>(0);
            return unscale(pathCorrector.correct(scale(begin), scale(end), skippedEntities, tailEntity));
        }
        else {
            return new ArrayList<Coordinates>(Arrays.asList(begin, end));
        }
    }

    private void redrawPath(List<Coordinates>... coordsLists) {
        if (null != path) {
            for (Line l : path) {
                group.remove(l);
            }
        }
        path = drawPath(coordsLists);

        for (Line l : path) {
            group.add(l);
        }
    }

    private List<Line> drawPath(List<Coordinates>... coordsLists) {
        List<Coordinates> allCoords = new ArrayList<Coordinates>();
        for (List<Coordinates> coords : coordsLists) {
            allCoords.addAll(coords);
        }
        return draw(allCoords);
    }

    private List<Line> draw(List<Coordinates> coords) {
        if (coords.isEmpty()) {
            throw new IllegalArgumentException("Coordinates list must not be empty!");
        }

        coords = removeCycles(coords);

        Coordinates from = coords.get(0);
        //first coordinate is not adjusted to the grid by x as far as connection stick may have whatever
        //size programmer wants
        from = new Coordinates(scale(from.x), Grid.adjust(scale(from.y)));

        List<Line> path = new ArrayList<Line>();

        for (int i = 1; i < coords.size(); i++) {
            Coordinates to = Grid.adjust(scale(coords.get(i)));
            if (from.equals(to)) {
                continue;
            }

            Coordinates middle = PathCorrector.getMiddlePoint(from, to);
            if (null != middle) {
                path.add(new Line(from.x, from.y, middle.x, middle.y));
                path.add(new Line(middle.x, middle.y, to.x, to.y));
            }
            else {
                path.add(new Line(from.x, from.y, to.x, to.y));
            }

            from = to;
        }

        return path;
    }

    private List<Coordinates> removeCycles(List<Coordinates> coords) {
        if (null != pathCorrector) {
            return pathCorrector.removeCycles(coords);
        }
        else {
            return coords;
        }
    }

    /**
     * Fixes head of this connector in the specified position.
     * 'Fixing' means that previous varying steps of this connector are saved and evaluation of the next varying steps starts from the specified position.
     * Varying steps - unsaved parts of the connector that are changing while user moves the mouse.
     */
    public void fixHead() {
        if (!pendingCoords.isEmpty()) {
            coordinates.addAll(pendingCoords);
            coordinates = removeCycles(coordinates);
            pendingCoords.clear();
        }
    }

    /**
     * Moves connector's head to the specified coordinates.
     *
     * @param coordinates coordinates to move the head to.
     */
    public void moveHead(Coordinates coordinates) {
        if (null == this.headEntity) {
            throw new IllegalStateException("This operation is not supported until head entity is attached!");
        }

        ActiveEntity headEntity = this.headEntity;

        detachHead();
        attachHead(headEntity, coordinates);
    }

    /**
     * Detaches head from the {@link #headEntity}
     */
    public void detachHead() {
        if (null != arrow) {
            group.remove(arrow);
            arrow = null;
        }

        if (!pendingCoords.isEmpty()) {
            pendingCoords.clear();
        }
        else if (!coordinates.isEmpty()) {
            coordinates.remove(coordinates.size() - 1);
        }

        this.headEntity = null;
    }

    /**
     * Attaches head of this connector to the input of the specified entity in the specified position.
     *
     * @param entity   head of this connector will e attached to output place of this entity.
     * @param position position of the input place to attach connector's head to.
     */
    public void attachHead(ActiveEntity entity, Coordinates position) {
        if (null != entity) {
            this.headEntity = entity;
            this.inputName = entity.getDrawer().getInputName(entity.getDrawer().getInputId(position));
            int arrowLength = getArrowLength();
            Coordinates coords = new Coordinates(position.x - arrowLength, position.y);
            drawPathTo(coords);

            if (null != arrow) {
                group.remove(arrow);
            }

            arrow = createArrow(position.x, position.y, arrowLength);
            group.add(arrow);
        }
    }

    static int getArrowLength() {
        return scale(ARROW_LENGTH);
    }

    private Path createArrow(int x2, int y2, int arrowLength) {
        Path arrowHead = new Path(x2 - arrowLength, y2);
        arrowHead.lineRelativelyTo(arrowLength, 0);
        if (ARROW_PAINTED) {
            arrowHead.lineRelativelyTo(-arrowLength / 2, arrowLength / 4);
            arrowHead.lineRelativelyTo(0, -arrowLength / 2);
            arrowHead.lineRelativelyTo(arrowLength / 2, arrowLength / 4);
            arrowHead.close();
            arrowHead.setFillColor("back");
        }
        return arrowHead;
    }

    /**
     * Moves connector's tail to the specified coordinates.
     *
     * @param coordinates coordinates to move the tail to.
     */
    public void moveTail(Coordinates coordinates) {
        if (!this.coordinates.isEmpty()) {
            coordinates = unscale(coordinates);
            this.coordinates.remove(0);
            List<Coordinates> coords = getCorrectedCoords(coordinates, this.coordinates.get(0));
            coords.remove(coords.size() - 1);
            this.coordinates.addAll(0, coords);
            this.coordinates = removeCycles(this.coordinates);

            redrawPath(this.coordinates);
        }
    }
    
    /*
     * Methods for working with Input and Output data
     */
    public String getInputName() {
        return inputName;
    }
    
    public String getOutputName() {
        return outputName;
    }
    
    public void setInputNum(Integer num) {
        this.inputNum = num;
    }
    
    public Integer getInputNum() {
        return inputNum;
    }

    public void setOutputNum(Integer num) {
        this.outputNum = num;
    }
    
    public Integer getOutputNum() {
        return outputNum;
    }
}
