package ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.event;

import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.ActiveEntity;

import java.util.Queue;

/**
 * @author n0weak
 */
public class EntityEvent implements Stringalizable {
// ------------------------------ FIELDS ------------------------------

    private final EntityDetails entityDetails;

// -------------------------- STATIC METHODS --------------------------
    /**
     * Constructs entity from its serialized representation.
     *
     * @param serializedString serialized representation of the entity.
     * @return deserialized entity if process was successful.
     * @throws IllegalArgumentException if deserialization failed
     * @see #serialize()
     */
    public static EntityEvent deserialize(String serializedString) {
        try {
            return deserialize(EventUtils.tokenize(serializedString));
        } catch (Exception e) {
            throw new IllegalArgumentException("Cannot deserialize EntityEvent from supplied data: " + serializedString,
                                               e);
        }
    }

    static EntityEvent deserialize(Queue<String> tokens) {
        return new EntityEvent(EntityDetails.deserialize(tokens));
    }

// --------------------------- CONSTRUCTORS ---------------------------
    public EntityEvent(EntityDetails entityDetails) {
        this.entityDetails = entityDetails;
    }

    public EntityEvent(ActiveEntity entity) {
        entityDetails = new EntityDetails(entity);
    }

// --------------------- GETTER / SETTER METHODS ---------------------
    public EntityDetails getEntityDetails() {
        return entityDetails;
    }

// ------------------------ INTERFACE METHODS ------------------------
// --------------------- Interface Stringalizable ---------------------
    public String serialize() {
        return entityDetails.serialize();
    }
}
