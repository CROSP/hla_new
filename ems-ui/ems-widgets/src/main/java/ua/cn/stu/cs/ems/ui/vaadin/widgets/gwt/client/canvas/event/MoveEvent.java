package ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.event;

import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.ActiveEntity;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.Connector;

import java.util.HashSet;
import java.util.Queue;
import java.util.Set;

/**
 * @author n0weak
 */
public class MoveEvent implements Stringalizable {
// ------------------------------ FIELDS ------------------------------

    private final EntityEvent entityEvent;
    private final Set<ConnectionEvent> connections;

// -------------------------- STATIC METHODS --------------------------
    /**
     * Constructs entity from its serialized representation.
     * <p/>
     * Representation must contain serialized {@link EntityEvent} and the set of {@link ConnectionEvent}, delimited by 2 x {@link Stringalizable#DELIM}
     *
     * @param serializedString serialized representation of the entity.
     * @return deserialized entity if process was successful.
     * @throws IllegalArgumentException if deserialization failed
     * @see #serialize()
     */
    public static MoveEvent deserialize(String serializedString) {
        try {
            return deserialize(EventUtils.tokenize(serializedString, 2));
        } catch (Exception e) {
            throw new IllegalArgumentException("Cannot deserialize MoveEvent from supplied data: " + serializedString, e);
        }
    }

    static MoveEvent deserialize(Queue<String> tokens) {
        EntityEvent entityEvent = EntityEvent.deserialize(tokens.poll());
        Set<ConnectionEvent> connections = new HashSet<ConnectionEvent>();
        while (!tokens.isEmpty()) {
            connections.add(ConnectionEvent.deserialize(tokens.poll()));
        }
        return new MoveEvent(entityEvent, connections);
    }

// --------------------------- CONSTRUCTORS ---------------------------
    public MoveEvent(EntityEvent entityEvent, Set<ConnectionEvent> connections) {
        this.connections = connections;
        this.entityEvent = entityEvent;
    }

    public MoveEvent(ActiveEntity entity) {
        entityEvent = new EntityEvent(entity);
        connections = new HashSet<ConnectionEvent>();
        for (Connector connector : entity.getInputConnectors()) {
            connections.add(new ConnectionEvent(connector));
        }
        for (Connector connector : entity.getOutputConnectors()) {
            connections.add(new ConnectionEvent(connector));
        }
    }

// --------------------- GETTER / SETTER METHODS ---------------------
    public EntityDetails getEntityDetails() {
        return entityEvent.getEntityDetails();
    }

    public Set<ConnectionEvent> getConnections() {
        return connections;
    }

    // ------------------------ INTERFACE METHODS ------------------------
// --------------------- Interface Stringalizable ---------------------
    public String serialize() {
        StringBuilder sb = new StringBuilder().append(entityEvent.serialize()).append(DELIM).append(DELIM);
        for (ConnectionEvent event : connections) {
            sb.append(event.serialize()).append(DELIM).append(DELIM);
        }
        return sb.toString();
    }
}
