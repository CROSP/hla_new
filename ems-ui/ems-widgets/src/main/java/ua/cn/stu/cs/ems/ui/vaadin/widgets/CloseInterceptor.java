package ua.cn.stu.cs.ems.ui.vaadin.widgets;

import com.vaadin.terminal.PaintException;
import com.vaadin.terminal.PaintTarget;
import com.vaadin.ui.AbstractComponent;
import com.vaadin.ui.ClientWidget;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.VCloseInterceptor;

/**
 * @author n0weak
 */
@ClientWidget(VCloseInterceptor.class)
public class CloseInterceptor extends AbstractComponent {

    private String text;

    public CloseInterceptor(String text) {
        this.text = text;
    }

    @Override
    public void paintContent(PaintTarget target) throws PaintException {
        super.paintContent(target);
        target.addVariable(this, "text", text);
    }

    public void setText(String text) {
        this.text = text;
    }
}
