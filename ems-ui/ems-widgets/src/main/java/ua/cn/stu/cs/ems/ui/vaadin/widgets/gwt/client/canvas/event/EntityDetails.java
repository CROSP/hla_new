package ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.event;

import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.Coordinates;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.ActiveEntity;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.EntityType;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.Scaler;

import java.util.Queue;

/**
 * @author n0weak
 */
public class EntityDetails implements Stringalizable {
// ------------------------------ FIELDS ------------------------------

    private final String id;
//    private final EntityType entityType;
    private final String entityTypeRepresentation;
    private final Coordinates centerCoords;

// -------------------------- STATIC METHODS --------------------------
    /**
     * Deserializes instance of this class from the queue of strings.
     *
     * @param tokens queue of strings, representing serialized state of this instance. Must contain at least 2 elements. First string in the queue is treated as identifier of the entity,
     *               second - as its type. Both used tokens will be polled from the queue.
     * @return deserialized entity if process was successful.
     * @throws IllegalArgumentException if deserialization failed
     */
    static EntityDetails deserialize(Queue<String> tokens) {
        try {
            String id = tokens.poll();
            EntityType type = EntityType.deserialize(tokens);
            EntityDetails details = new EntityDetails(id, type,
                                     new Coordinates(Integer.parseInt(tokens.poll()), Integer.parseInt(tokens.poll())));
            
            return details;
        } catch (Exception e) {
            throw new IllegalArgumentException("Cannot deserialize EntityDetails from supplied data", e);
        }
    }

// --------------------------- CONSTRUCTORS ---------------------------
    public EntityDetails(String id, EntityType entityType, Coordinates centerCoords) {
//        this.entityType = EntityType.deserialize(entityType.serialize());
        this.entityTypeRepresentation = entityType.serialize();
        this.id = id;
        this.centerCoords = centerCoords;
    }

    public EntityDetails(ActiveEntity entity) {
//        this.entityType = EntityType.deserialize(entity.getType().serialize());
        this.entityTypeRepresentation = entity.getType().serialize();
        this.id = entity.getId();
        this.centerCoords = Scaler.unscale(entity.getCenter());
    }

// --------------------- GETTER / SETTER METHODS ---------------------
    public EntityType getEntityType() {
//        return entityType;
        return EntityType.deserialize(entityTypeRepresentation);
    }

    public String getId() {
        return id;
    }

    public Coordinates getCenterCoords() {
        return centerCoords;
    }

    // ------------------------ INTERFACE METHODS ------------------------
// --------------------- Interface Stringalizable ---------------------
    public String serialize() {
        return new StringBuilder().append(id).append(DELIM).
//                append(entityType.serialize()).append(DELIM).append(centerCoords.x).
                append(entityTypeRepresentation).append(DELIM).append(centerCoords.x).
                append(DELIM).append(centerCoords.y).toString();
    }
}
