package ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity;

/**
 * Contract for all the entities that support selection.
 *
 * @author n0weak
 */
public interface Selectable {

    /**
     * Defines whether the entity is contained in rectangle with specified coordinates.
     *
     * @param x1 top-left x coordinate of the rectangle, inclusive.
     * @param y1 top-left y coordinate of the rectangle, inclusive.
     * @param x2 bottom-right x coordinate of the rectangle, inclusive.
     * @param y2 bottom-right y coordinate of the rectangle, inclusive.
     * @return true if entity is contained in rectangle with specified coordinates, false if not.
     */
    boolean isContainedIn(int x1, int y1, int x2, int y2);

    /**
     * Toggles selection mode for this entity.
     *
     * @param selected true to select the entity, false to deselect
     */
    void setSelected(boolean selected);
}
