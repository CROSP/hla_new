package ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity;

import org.vaadin.gwtgraphics.client.DrawingArea;

/**
 * @author n0weak
 */
public interface CanvasEntity extends Drawable, Selectable, Repaintable {

    /**
     * Performs all the cleaning needed for deletion and removes entity from the canvas.
     *
     * @param canvas canvas that contains entities.
     */
    void delete(DrawingArea canvas);
}
