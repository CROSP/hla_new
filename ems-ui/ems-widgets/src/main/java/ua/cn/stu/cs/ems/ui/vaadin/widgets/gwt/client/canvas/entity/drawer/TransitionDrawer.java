package ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.drawer;

import org.vaadin.gwtgraphics.client.shape.Path;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.GwtJessCanvas;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.Coordinates;

import static ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.Scaler.scale;

/**
 * @author n0weak
 */
public abstract class TransitionDrawer extends AbstractDrawer {

    static final int DEFAULT_VSPACING = DEFAULT_HEIGHT / 2;
    protected Path path;
    protected int inputSticks = 0;
    protected int outputSticks = 0;

    /**
     * @param id string identifier of this component
     * @param x  x coordinate of the center of the component
     * @param y  y coordinate of the center of the component
     */
    TransitionDrawer(String id, int x, int y, int countInputs, int countOutputs) {
        super(id, x, y);
        this.inputSticks = countInputs;
        this.outputSticks = countOutputs;
    }

    public int getWidth() {
        return scale(DEFAULT_WIDTH);
    }

    public void moveComponent(int x, int y) {
        path.setX(x);
        path.setY(y);
    }

    public Coordinates getTopLeft() {
        return new Coordinates(path.getX() - getWidth() / 2, path.getY());
    }

    public Coordinates getCenter() {
        return new Coordinates(path.getX(), path.getY() + getBaseHeight() / 2);
    }

    public Coordinates getOutputCoordinates(int connectorIdx) {
        Coordinates center = getCenter();
        return new Coordinates(center.x + scale(STICK_SIZE), center.y);
    }

    public Coordinates getInputCoordinates(int connectorIdx) {
        Coordinates center = getCenter();
        return new Coordinates(center.x, center.y);
    }

    public boolean isContainedIn(int x1, int y1, int x2, int y2) {
        Coordinates topLeft = getTopLeft();
        return topLeft.x >= x1 && topLeft.x + getWidth() <= x2 && topLeft.y >= y1 && topLeft.y + getBaseHeight() <= y2;
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
    }

    @Override
    public void setSelected(boolean selected) {
        super.setSelected(selected);
    }

    @Override
    protected void paintComponent(String id, int x, int y) {
        path = new Path(x, y);
        draw(path);
        group.add(path);

        super.paintComponent(id, x, y);
    }

    /**
     * Draws connectors sticks and the main line into the passed path entity which represents current transition.
     * After the drawing path will stand in {@link #getVerticalSpacing()} under the last stick.
     * If both in and out parameters are 0 or less, no drawing will be made.
     *
     * @param path path entity which represents current transition
     * @param out  number of connectors which are coming out
     * @param in   number of connectors which are coming in
     */
    protected void drawConnectors(Path path, int out, int in) {
        if (in <= 0 && out <= 0) {
            return;
        }

        int hGap = getVerticalSpacing();
        int stickSize = scale(STICK_SIZE);

        int max = Math.max(in, out);
        int min = Math.min(in, out);

        boolean sameParity = in % 2 == out % 2;
        int diff = max - min;
        int handicap = (int) Math.ceil(diff / (double) 2);
        boolean extrovert = out > in;

        int i = 0;
        for (; i < handicap; i++) {
            path.lineRelativelyTo(0, hGap);
            drawStick(path, stickSize, extrovert);
        }

        for (; i < min + handicap; i++) {
            path.lineRelativelyTo(0, hGap);
            drawStick(path, stickSize, !extrovert);

            if (!sameParity) {
                path.lineRelativelyTo(0, hGap);
            }
            drawStick(path, stickSize, extrovert);
        }

        for (; i < max; i++) {
            path.lineRelativelyTo(0, hGap);
            drawStick(path, stickSize, extrovert);
        }
        path.lineRelativelyTo(0, hGap);
    }

    /**
     * Draws the stick into the specified path.
     * After the drawing path will be returned to the beginning coordinates.
     *
     * @param path              path to draw the stick onto
     * @param isOutputConnector true if output connector must be drawn, false if input one
     * @param stickSize         size of the stick to be drawn
     */
    protected void drawStick(Path path, int stickSize, boolean isOutputConnector) {
        path.lineRelativelyTo(isOutputConnector ? stickSize : -stickSize, 0);
        path.lineRelativelyTo(isOutputConnector ? -stickSize : stickSize, 0);
    }

    /**
     * Provides ability to the subclasses to draw type-related icons. Generic icon is an empty rectangle.
     *
     * @param leftOriented indicates whether icon should be drown on the left side of the component.
     */
    protected void drawGenericIcon(boolean leftOriented) {
        int direction = leftOriented ? 1 : -1;
        int height = scale(17);
        int width = scale(8);
        int stick = scale(5);
        int topGap = scale(8);
        int iconGap = scale(3);

        path.lineRelativelyTo(0, topGap);
        path.lineRelativelyTo(direction * -stick, 0);

        //drawing rectangle
        path.lineRelativelyTo(0, -(height - iconGap));
        path.lineRelativelyTo(direction * -width, 0);
        path.lineRelativelyTo(0, height);
        path.lineRelativelyTo(direction * width, 0);
        path.lineRelativelyTo(0, -iconGap);
        path.lineRelativelyTo(direction * stick, 0);
        path.lineRelativelyTo(0, -topGap);
    }

    protected int getVerticalSpacing() {
        return scale(DEFAULT_VSPACING);
    }

    public int getSticksCount(boolean output) {
        return output ? outputSticks : inputSticks;
    }

    /**
     * Method to be implemented by the subclasses which perform actual drawing
     *
     * @param path initialized path entity that represents current transition
     */
    protected abstract void draw(Path path);

    /**
     * Represents transitions which support unlimited dynamic component attachments and resizing.
     */
    public static abstract class FlexibleTransitionDrawer extends TransitionDrawer {

        /**
         * Height for transitions with 2 inputs/outputs and 1 output/input respectively.
         */
        static final int STARTING_HEIGHT = DEFAULT_VSPACING * 4;
        private static final int INITIAL_STICKS_CNT = 2;
        /**
         * Number of output connectors
         */
        protected Integer flexibleSticksCount = INITIAL_STICKS_CNT;
        /**
         * Unscaled height of the component. Must be scaled before the usage.
         */
        protected Integer height = STARTING_HEIGHT;

        FlexibleTransitionDrawer(String id, int x, int y, int countInputs, int countOutputs) {
            super(id, x, y, countInputs, countOutputs);
        }

        /**
         * Method used to evaluate y coordinate of the connection stick.
         * It is assumed that user has flexible input/output connection places and fixed to 1 amount of output/input connection places.
         *
         * @param totalCnt     total amount of input/output connectors of this transition
         * @param connectorIdx index of the connector to get stick coordinate for
         * @return y coordinate of the stick
         */
        protected int getStickY(int connectorIdx, int totalCnt) {
            return totalCnt % 2 > 0 || connectorIdx < totalCnt / 2 ? (connectorIdx + 1) * getVerticalSpacing()
                    : (connectorIdx
                    + 2) * getVerticalSpacing();
        }

        @Override
        public int getBaseHeight() {
            if (null == height) {  //setting the value manually if called from the superclass during it's initialization when this.height is not yet initialized.
                height = STARTING_HEIGHT;
            }
            return scale(height);
        }

//        public void removeStick() {
//            if (flexibleSticksCount == INITIAL_STICKS_CNT) {
//                throw new IllegalStateException("Transition must have at least " + INITIAL_STICKS_CNT
//                                                + " connection sticks");
//            }
//            else {
//                flexibleSticksCount--;
//                if (flexibleSticksCount % 2 == 1) {
//                    height -= DEFAULT_VSPACING * 2;
//                }
//                repaint();
//            }
//        }
        
        public void addStick() {
            flexibleSticksCount++;
            if (flexibleSticksCount % 2 == 0) {
                height += DEFAULT_VSPACING * 2;
            }
            repaint();
        }

        public Integer getFlexibleSticksCount() {
            if (null == flexibleSticksCount) {
                flexibleSticksCount = INITIAL_STICKS_CNT;
            }
            return flexibleSticksCount;
        }
    }

    public static class TDrawer extends TransitionDrawer {

        public TDrawer(String id, int x, int y) {
            super(id, x, y, 1, 1);
        }

        @Override
        protected void draw(Path path) {
            drawConnectors(path, 1, 1);
        }
    }

    public static class FDrawer extends FlexibleTransitionDrawer {

        public FDrawer(String id, int x, int y, int countInputs, int countOutputs) {
            super(id, x, y, countInputs, countOutputs);
            for (int i=getFlexibleSticksCount();i<countOutputs;i++) {
                addStick();
            }
        }

        @Override
        protected void draw(Path path) {
            drawConnectors(path, getFlexibleSticksCount(), 1);
        }

        @Override
        public Coordinates getOutputCoordinates(int connectorIdx) {
            return new Coordinates(getCenter().x + scale(STICK_SIZE), getTopLeft().y
                    + getStickY(connectorIdx, getFlexibleSticksCount()));
        }
    }

    public static class XDrawer extends FDrawer {

        public XDrawer(String id, int x, int y, int countInputs, int countOutputs) {
            super(id, x, y, countInputs, countOutputs);
        }

        @Override
        protected void draw(Path path) {
            drawGenericIcon(true);
            super.draw(path);
        }
    }

    public static class JDrawer extends FlexibleTransitionDrawer {

        public JDrawer(String id, int x, int y, int countInputs, int countOutputs) {
            super(id, x, y, countInputs, countOutputs);
            for (int i=getFlexibleSticksCount();i<countInputs;i++) {
                addStick();
            }
        }

        @Override
        protected void draw(Path path) {
            drawConnectors(path, 1, getFlexibleSticksCount());
        }

        @Override
        public Coordinates getInputCoordinates(int connectorIdx) {
            return new Coordinates(getCenter().x, getTopLeft().y + getStickY(connectorIdx, getFlexibleSticksCount()));
        }
    }

    public static class YDrawer extends JDrawer {

        public YDrawer(String id, int x, int y, int countInputs, int countOutputs) {
            super(id, x, y, countInputs, countOutputs);
        }

        @Override
        protected void draw(Path path) {
            drawGenericIcon(true);
            super.draw(path);
        }
    }

    private abstract static class QueueDrawer extends TransitionDrawer {

        private static final int DEFAULT_GAP = 5;

        protected static int getDefaultGap() {
            return scale(DEFAULT_GAP);
        }

        public QueueDrawer(String id, int x, int y) {
            super(id, x, y, 1, 1);
            adjustText();
        }

        protected void drawRect(Path path, OrnamentDrawer ornamentDrawer) {
            int h = getBaseHeight();
            int w = getWidth();
            //drawing rectangle with connectors
            drawConnectors(path, 0, 1);
            //going up to the top-left corner
            path.lineRelativelyTo(0, -h);
            path.lineRelativelyTo(w, 0);
            //drawing ornament from the top-left corner of main rectangle
            ornamentDrawer.draw(path, w, h);
            drawConnectors(path, 1, 0);
            path.lineRelativelyTo(-w, 0);
        }

        @Override
        public Coordinates getCenter() {
            return new Coordinates(path.getX() + getWidth() / 2, path.getY() + getBaseHeight() / 2);
        }

        @Override
        public Coordinates getOutputCoordinates(int connectorIdx) {
            return new Coordinates(path.getX() + getWidth() + scale(STICK_SIZE), getCenter().y);
        }

        @Override
        public Coordinates getInputCoordinates(int connectorIdx) {
            return new Coordinates(path.getX(), getCenter().y);
        }

        protected interface OrnamentDrawer {

            /**
             * Draw queue-specific ornament in the main rectangle.
             *
             * @param path   path witch represents main rectangle of queues icon. Stands in its top-left corner.
             * @param width  width of the main rectangle
             * @param height height  of the main rectangle
             */
            void draw(Path path, int width, int height);
        }
    }

    public static class FifoDrawer extends QueueDrawer {

        private static final OrnamentDrawer ORNAMENT_DRAWER = new OrnamentDrawer() {

            public void draw(Path path, int width, int height) {
                int gap = getDefaultGap();
                path.lineRelativelyTo(-gap, 0);
                path.lineRelativelyTo(0, height);
                path.lineRelativelyTo(0, -height);
                path.lineRelativelyTo(gap, 0);
            }
        };

        public FifoDrawer(String id, int x, int y) {
            super(id, x, y);
        }

        @Override
        protected void draw(Path path) {
            drawRect(path, ORNAMENT_DRAWER);
        }
    }

    public static class PFifoDrawer extends QueueDrawer {

        private static final OrnamentDrawer ORNAMENT_DRAWER = new OrnamentDrawer() {

            public void draw(Path path, int width, int height) {
                path.lineRelativelyTo(-width, height);
                path.lineRelativelyTo(width, -height);
            }
        };

        public PFifoDrawer(String id, int x, int y) {
            super(id, x, y);
        }

        @Override
        protected void draw(Path path) {
            drawRect(path, ORNAMENT_DRAWER);
        }
    }

    public static class LifoDrawer extends QueueDrawer {

        private static final OrnamentDrawer ORNAMENT_DRAWER = new OrnamentDrawer() {

            public void draw(Path path, int width, int height) {
                int gap = getDefaultGap();
                path.lineRelativelyTo(-(width - gap), 0);
                path.lineRelativelyTo(0, height);
                path.lineRelativelyTo(0, -height);
                path.lineRelativelyTo(width - gap, 0);
            }
        };

        public LifoDrawer(String id, int x, int y) {
            super(id, x, y);
        }

        @Override
        protected void draw(Path path) {
            drawRect(path, ORNAMENT_DRAWER);
        }
    }

    public static class PLifoDrawer extends QueueDrawer {

        private static final OrnamentDrawer ORNAMENT_DRAWER = new OrnamentDrawer() {

            public void draw(Path path, int width, int height) {
                path.lineRelativelyTo(-width, 0);
                path.lineRelativelyTo(width, height);
                path.lineRelativelyTo(-width, -height);
                path.lineRelativelyTo(width, 0);
            }
        };

        public PLifoDrawer(String id, int x, int y) {
            super(id, x, y);
        }

        @Override
        protected void draw(Path path) {
            drawRect(path, ORNAMENT_DRAWER);
        }
    }
}
