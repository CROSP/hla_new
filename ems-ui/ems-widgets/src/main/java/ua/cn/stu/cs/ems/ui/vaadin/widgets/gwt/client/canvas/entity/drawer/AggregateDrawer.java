package ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.drawer;

import java.util.ArrayList;
import java.util.List;
import org.vaadin.gwtgraphics.client.shape.Path;
import org.vaadin.gwtgraphics.client.shape.Rectangle;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.Coordinates;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.graphics.PreciseText;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.Scaler;

/**
 *
 * @author Alexander Bartash
 */
public class AggregateDrawer extends AbstractDrawer {

    /**Stored aggregate's rectangle's VectorObject. Used for move and repaint component.*/
    private Rectangle rectangle;
    /**Rectangle's height defines by count of the IO sticks.*/
    private static final int DEFAULT_RECTANGLE_WIDTH = 200;
    private static final int DEFAULT_RECTANGLE_HEIGHT = 40;
    /**Free vertical space between IO sticks.*/
    private static final int DEFAULT_STICK_SPACE = 20;
    /**Offset of the stick's name from the left\right side of the rectangle.*/
    private static final int DEFAULT_STICK_NAME_OFFSET = 10;
    private static final int MAX_STICK_NAME_LENGTH = 10;
    /**Stored IO stiks VectorObject. Used for move and repaint component.*/
    private List<Stick> inputSticks;
    private List<Stick> outputSticks;

    public int getSticksCount(boolean output) {
        return output ? getOutputSticks().size() : getInputSticks().size();
    }
    /**
     * Used for grupping stick's descriptions. Such as path and name.
     * @see Path
     * @see PreciseText
     */
    private class Stick {

        private Path path;
        /**This name will be draw inside the rectangle near this stick.*/
        private PreciseText name;
        private static final String DEFAULT_TEXT_FONT = "Arial";
        private static final String DEFAULT_TEXT_STROKE_COLOUR = "Gray";
        private static final String DEFAULT_TEXT_FILL_COLOUR = "Gray";
        private static final int DEFAULT_STICK_NAME_FONT_SIZE = 13;
        private String fullName;

        Stick() {
        }

        Stick(Path path, PreciseText name, String fullName) {
            this.path = path;
            this.name = name;
            this.fullName = fullName;
        }

        public PreciseText getName() {
            return name;
        }
        
        public String getFullName() {
            return fullName;
        }

        public Path getPath() {
            return path;
        }

        public final void setName(PreciseText name) {
            this.name = name;
        }

        public void setPath(Path path) {
            this.path = path;
        }

        /**
         * Equals objects only by {@link #name}.
         * @param obj Object for comparing.
         * @return {@code true/false}.
         */
        @Override
        public boolean equals(Object obj) {
            return obj instanceof Stick ? ((Stick) obj).getFullName().equals(this.fullName) : false;
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 59 * hash + (this.path != null ? this.path.hashCode() : 0);
            hash = 59 * hash + (this.name != null ? this.name.hashCode() : 0);
            hash = 59 * hash + (this.fullName != null ? this.fullName.hashCode() : 0);
            return hash;
        }
    }

    public AggregateDrawer(String id, int x, int y, String[] inputNames, String[] outputNames) {
        super(id, x, y);
        if (inputNames != null && inputNames.length > 0) {
            for (String name: inputNames) {
                addInputStick(name);
            }
        }
        if (outputNames != null && outputNames.length > 0) {
            for (String name: outputNames) {
                addOutputStick(name);
            }
        }
        repaint();
    }
        
    /**DON'T TOUCH!!!
     * We must use lazy initialization for this get-methods, because some 
     * methods, what using this attributes, can ivoke from parent class 
     * {@link AbstractDrawer}, when attributes {@link #inputSticks}, 
     * {@link #outputSticks} and {@link #rectangle} equals {@code null}.
     */
    public List<Stick> getInputSticks() {
        if (null == this.inputSticks) {
            this.inputSticks = new ArrayList<Stick>();
        }
        return inputSticks;
    }

    public List<Stick> getOutputSticks() {
        if (null == this.outputSticks) {
            this.outputSticks = new ArrayList<Stick>();
        }
        return outputSticks;
    }

    private Rectangle getRectangle() {
        if (null == this.rectangle) {
            this.rectangle = new Rectangle(40, 40,
                    Scaler.scale(AggregateDrawer.DEFAULT_RECTANGLE_WIDTH),
                    Scaler.scale(AggregateDrawer.DEFAULT_RECTANGLE_HEIGHT));
        }
        return rectangle;
    }

    //---------- Interface Drawer ----------
    /**
     * Defines coordinates of output position by connector's index.
     * @param connectorIdx Index of the output connector.
     * @return Coordinates of the output position.
     * @throws IndexOutOfBoundsException If {@code connectorIdx} &gt count of 
     * output sticks or &lt 0;
     */
    public Coordinates getOutputCoordinates(int connectorIdx) {
        Path path = this.getOutputSticks().get(connectorIdx).getPath();
        return new Coordinates(path.getX() + Scaler.scale(Drawer.STICK_SIZE), path.getY());
    }

    /**
     * Defines coordinates of input position by the connector's index.
     * @param connectorIdx Index of the input connector.
     * @return Coordinates of the input position.
     * @throws IndexOutOfBoundsException If {@code connectorIdx} &gt count of 
     * input sticks or &lt 0;
     */
    public Coordinates getInputCoordinates(int connectorIdx) {
        Path path = this.getInputSticks().get(connectorIdx).getPath();
        return new Coordinates(path.getX(), path.getY());
    }

    //---------- Interface Informative ----------
    /**
     * Returns scaled coordinates of top left corner of the rectangle.
     * Inputs and outputs sticks doesn't takes into account.
     * @return Scaled coordinates of top left corner of the rectangle.
     * @see Scaler#scale(int) 
     */
    @Override
    public Coordinates getTopLeft() {
        return new Coordinates(
                this.rectangle.getX() - 
                (getInputSticks().isEmpty() ? 0 : Scaler.scale(STICK_SIZE)), 
                getRectangle().getY());
    }

    /**
     * Returns scaled coodinates of center of the rectangle.
     * @return Scaled coodinates of center of the rectangle.
     * @see Scaler#scale(int) 
     */
    @Override
    public Coordinates getCenter() {
        return new Coordinates(getRectangle().getX() + getRectangle().getWidth()/2, 
                getRectangle().getY() + getHeight()/2);
    }

    /**
     * Returns scaled width of the rectangle. Inputs and outputs sticks with 
     * takes into account.
     * @return Scaled width of the rectangle.
     * @see Scaler#scale(int) 
     */
    @Override
    public int getWidth() {
        return getRectangle().getWidth() + 
                (getInputSticks().isEmpty() ? 0 : Scaler.scale(STICK_SIZE)) +
                (getOutputSticks().isEmpty() ? 0 : Scaler.scale(STICK_SIZE));
    }

    //---------- AbstractDrawer ----------
    /**
     * Returns scaled height of the rectangle + his name at the foot.
     * @return scaled height of the rectangle + his name at the foot.
     * @see Scaler#scale(int) 
     */
    @Override
    public int getHeight() {
        return getRectangle().getHeight() + text.getTextHeight();
    }

    /**
     * Updates aggregates size and position, using current scale and coordinates.
     * @param x scaled and adjusted x coordinate of top left corner of the rectangle.
     * @param y scaled and adjusted y coordinate of top left corner of the rectangle.
     * @see Scaler#scale(int) 
     */
    private void update(int x, int y) {
        //calculate where is maximum count of sticks: input or output.
        int cnt = this.getInputSticks().size();
        if (this.getOutputSticks().size() > cnt) {
            cnt = this.getOutputSticks().size();
        }
        cnt++; //cnt may be equal to 0
        //update rectangles size and position
        Rectangle rec = this.getRectangle();
                
        rec.setX(x);
        rec.setY(y);
        rec.setWidth(Scaler.scale(AggregateDrawer.DEFAULT_RECTANGLE_WIDTH));
        rec.setHeight(Scaler.scale(AggregateDrawer.DEFAULT_STICK_SPACE) * cnt);
        //update input sticks position
        Stick stick = null;
        PreciseText name = null;
        Path path = null;
        List<Stick> lst = this.getInputSticks();
        cnt = lst.size();
        for (int i = 0; i < cnt; i++) {
            stick = lst.get(i);
            path = stick.getPath();
            path.setX(x);
            path.setY(y + (i + 1) * Scaler.scale(AggregateDrawer.DEFAULT_STICK_SPACE));
            path.removeStep(1);
            path.lineRelativelyTo(-Scaler.scale(Drawer.STICK_SIZE), 0);
            name = stick.getName();
            name.setFontSize(Scaler.scale(Stick.DEFAULT_STICK_NAME_FONT_SIZE));
            name.setX(x + Scaler.scale(AggregateDrawer.DEFAULT_STICK_NAME_OFFSET));
            name.setY(y + (i + 1) * Scaler.scale(AggregateDrawer.DEFAULT_STICK_SPACE) + name.getTextHeight() / 2);
        }
        //update output sticks position
        lst = this.getOutputSticks();
        cnt = lst.size();
        for (int i = 0; i < cnt; i++) {
            stick = lst.get(i);
            path = stick.getPath();
            path.setX(x + Scaler.scale(AggregateDrawer.DEFAULT_RECTANGLE_WIDTH));
            path.setY(y + (i + 1) * Scaler.scale(AggregateDrawer.DEFAULT_STICK_SPACE));
            path.removeStep(1);
            path.lineRelativelyTo(Scaler.scale(Drawer.STICK_SIZE), 0);
            name = stick.getName();
            name.setFontSize(Scaler.scale(Stick.DEFAULT_STICK_NAME_FONT_SIZE));
            name.setX(x + Scaler.scale(AggregateDrawer.DEFAULT_RECTANGLE_WIDTH)
                    - Scaler.scale(AggregateDrawer.DEFAULT_STICK_NAME_OFFSET)
                    - name.getTextWidth());
            name.setY(y + (i + 1) * Scaler.scale(AggregateDrawer.DEFAULT_STICK_SPACE) + name.getTextHeight() / 2);
        }
    }

    /**
     * Move the component to specified coordinates.
     * @param x scaled and adjusted x coordinate of top left corner of the rectangle.
     * @param y scaled and adjusted y coordinate of top left corner of the rectangle.
     */
    @Override
    protected void moveComponent(int x, int y) {
        /*Befor call this method value (this.getBaseHeight() / 2) substruct from
         * Y-coordinare. Because we must corrent them.*/
//        this.update(x - Scaler.scale(DEFAULT_RECTANGLE_WIDTH/2) , y);
        
        
        int delta = 0;
        int in = getSticksCount(false);
        int out = getSticksCount(true);
        delta -= getBaseHeight()/2;
        int max = Math.max(in, out);
        if (max > 1) {
            delta *= (int) (max / 2);
        }
        else if (max == 1) {
            delta *= 0;
        }
        
        this.update(x-Scaler.scale(DEFAULT_RECTANGLE_WIDTH/2), y + delta);

    }

    /**
     * @param id string identifier of this component.
     * @param x scaled and adjusted x coordinate of the center of the component.
     * @param y scaled and adjusted y coordinate of the top of the component.
     * @param If parameter id == null;
     */
    @Override
    protected void paintComponent(String id, int x, int y) throws NullPointerException {
        if (null == id) {
            throw new NullPointerException("Parameter id == null.");
        }
        
        int delta = 0;
        int in = getSticksCount(false);
        int out = getSticksCount(true);
        delta -= getBaseHeight()/2;
        int max = Math.max(in, out);
        if (max > 1) {
            delta *= (int) (max / 2);
        }
        else if (max == 1) {
            delta *= 0;
        }
        
        this.update(x-Scaler.scale(DEFAULT_RECTANGLE_WIDTH/2), y + delta);
        this.group.add(this.getRectangle());
        for (Stick stick : this.getInputSticks()) {
            this.group.add(stick.getPath());
            this.group.add(stick.getName());
        }
        for (Stick stick : this.getOutputSticks()) {
            this.group.add(stick.getPath());
            this.group.add(stick.getName());
        }
        
        super.paintComponent(id, x, y + delta);
    }
    //---------- END AbstractDrawer ----------

    private String smallName(String name) {
        String newName = "";
        
        if (name.length() > MAX_STICK_NAME_LENGTH) {
            for (int i=0;i<MAX_STICK_NAME_LENGTH/2-1;i++) {
                newName += name.charAt(i);
            }
            newName += "..";
            for (int i=name.length()-1-MAX_STICK_NAME_LENGTH/2-1;i<name.length();i++) {
                newName += name.charAt(i);
            }
        }
        else {
            newName = name;
        }
        
        return newName;
    }
    
    /**
     * Adds new input stick to the aggregate.
     * @param name Name of stick.
     * @throws NullPointerException If parameter name == null.
     */
    public void addInputStick(String name) throws NullPointerException {
        if (null == name) {
            throw new NullPointerException("Parameter name == null.");
        }
        String fullName = name;
        name = smallName(name);
        Path path = null;
        PreciseText txt = null;
        Rectangle rect = this.getRectangle();
        path = new Path(rect.getX(),
                rect.getY() + Scaler.scale(AggregateDrawer.DEFAULT_STICK_SPACE) * (this.getInputSticks().size() + 1));
        path.lineRelativelyTo(-Scaler.scale(Drawer.STICK_SIZE), 0);
        txt = new PreciseText(
                rect.getX() + Scaler.scale(AggregateDrawer.DEFAULT_STICK_NAME_OFFSET),
                rect.getY() + (this.getInputSticks().size() + 1) * Scaler.scale(AggregateDrawer.DEFAULT_STICK_SPACE),
                name);
        txt.setFontFamily(Stick.DEFAULT_TEXT_FONT);
        txt.setStrokeColor(Stick.DEFAULT_TEXT_STROKE_COLOUR);
        txt.setFillColor(Stick.DEFAULT_TEXT_FILL_COLOUR);
        txt.setFontSize(Scaler.scale(Stick.DEFAULT_STICK_NAME_FONT_SIZE));
        txt.setStrokeWidth(0.5);
        //Must be after formatting text. Because it's size depends on formatting.
        txt.setY(txt.getY() + txt.getTextHeight() / 2);
        this.getInputSticks().add(new Stick(path, txt, fullName));
    }

    /**
     * Adds new output stick to the aggregate.
     * @param name Name of stick.
     * @throws NullPointerException If parameter name == null.
     */
    public void addOutputStick(String name) throws NullPointerException {
        if (null == name) {
            throw new NullPointerException("Parameter name == null.");
        }
        String fullName = name;
        name = smallName(name);
        Path path = null;
        PreciseText txt = null;
        Rectangle rect = this.getRectangle();
        path = new Path(rect.getX() + Scaler.scale(AggregateDrawer.DEFAULT_RECTANGLE_WIDTH),
                rect.getY() + Scaler.scale(AggregateDrawer.DEFAULT_STICK_SPACE) * (this.getOutputSticks().size() + 1));
        path.lineRelativelyTo(Scaler.scale(Drawer.STICK_SIZE), 0);
        txt = new PreciseText(
                rect.getX() + Scaler.scale(AggregateDrawer.DEFAULT_RECTANGLE_WIDTH) - Scaler.scale(AggregateDrawer.DEFAULT_STICK_NAME_OFFSET),
                rect.getY() + (this.getOutputSticks().size() + 1) * Scaler.scale(AggregateDrawer.DEFAULT_STICK_SPACE),
                name);
        txt.setFontFamily(Stick.DEFAULT_TEXT_FONT);
        txt.setStrokeColor(Stick.DEFAULT_TEXT_STROKE_COLOUR);
        txt.setFillColor(Stick.DEFAULT_TEXT_FILL_COLOUR);
        txt.setFontSize(Scaler.scale(Stick.DEFAULT_STICK_NAME_FONT_SIZE));
        txt.setStrokeWidth(0.5);
        //Must be after formatting text. Because it's size depends on formatting.
        txt.setX(txt.getX() - txt.getTextWidth());
        txt.setY(txt.getY() + txt.getTextHeight() / 2);
        this.getOutputSticks().add(new Stick(path, txt, fullName));
    }
    
    @Override
    public String getInputName(int i) {
        if (i < 0 || i >= getSticksCount(false)) {
            return null;
        }
        return getInputSticks().get(i).getFullName();
    }
    
    @Override
    public String getOutputName(int i) {
        if (i < 0 || i >= getSticksCount(true)) {
            return null;
        }
        return getOutputSticks().get(i).getFullName();
    }
}