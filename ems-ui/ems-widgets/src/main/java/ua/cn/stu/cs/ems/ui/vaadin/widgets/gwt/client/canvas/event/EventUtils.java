package ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.event;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

/**
 * @author n0weak
 */
public class EventUtils {

    /**
     * @param serializedString string to be tokenized
     * @param level nesting level, starts from 1. Tokens on the level 1 are delimited by {@link Stringalizable#DELIM},
     *              on level 2 - {@link Stringalizable#DELIM}+{@link Stringalizable#DELIM} etc
     * @return
     */
    public static Queue<String> tokenize(String serializedString, int level) {
        if (level < 1) {
            throw new IllegalArgumentException("Level must be >=1");
        }
        StringBuilder delim = new StringBuilder();
        for (int i = 0; i < level; i++) {
            delim.append(Stringalizable.DELIM);
        }
        return new LinkedList<String>(Arrays.asList(serializedString.split(delim.toString())));
    }

    public static Queue<String> tokenize(String serializedString) {
        return tokenize(serializedString, 1);
    }
}
