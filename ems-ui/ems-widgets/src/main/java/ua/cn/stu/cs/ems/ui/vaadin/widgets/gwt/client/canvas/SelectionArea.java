package ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas;

import org.vaadin.gwtgraphics.client.VectorObject;
import org.vaadin.gwtgraphics.client.shape.Rectangle;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.Drawable;

/**
 * @author n0weak
 */
public class SelectionArea implements Drawable {

    private static final String COLOR = "grey";
    private Rectangle rectangle;
    private final int baseX;
    private final int baseY;

    public SelectionArea(int x, int y) {
        baseX = x;
        baseY = y;
        rectangle = new Rectangle(x, y, 0, 0);
        rectangle.setFillOpacity(0);
        rectangle.setStrokeColor(COLOR);
    }

    public VectorObject getVectorObject() {
        return rectangle;
    }

    public void move(int x, int y) {

        int width = x - baseX;
        int height = y - baseY;

        if (width < 0) {
            rectangle.setX(x);
        }
        else {
            rectangle.setX(baseX);
        }

        if (height < 0) {
            rectangle.setY(y);
        }
        else {
            rectangle.setY(baseY);
        }

        rectangle.setWidth(Math.abs(width));
        rectangle.setHeight(Math.abs(height));
    }

    public int getX1() {
        return rectangle.getX();
    }

    public int getY1() {
        return rectangle.getY();
    }

    public int getX2() {
        return rectangle.getX() + rectangle.getWidth();
    }

    public int getY2() {
        return rectangle.getY() + rectangle.getHeight();
    }
}
