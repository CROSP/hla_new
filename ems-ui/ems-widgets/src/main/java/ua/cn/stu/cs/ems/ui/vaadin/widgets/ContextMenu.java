package ua.cn.stu.cs.ems.ui.vaadin.widgets;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.VContextMenu;

import com.vaadin.terminal.PaintException;
import com.vaadin.terminal.PaintTarget;
import com.vaadin.ui.AbstractComponent;
import com.vaadin.ui.ClientWidget;
import com.vaadin.ui.Component;

/**
 * ContextMenu is a popup menu that can be placed to an arbitrary location
 * inside Vaadin application's bounds.
 * 
 * Items can be added to ContextMenu by calling ContextMenu.addItem(String).
 * Returned value represents ContextMenuItem that is attached to added item. If
 * ContextMenu.ClickListener is attached to component,
 * ContextMenu.ClickListener.getClickedItem() can be used to retrieve the
 * clicked item.
 * 
 * It is also possible to set ContextMenuItem disabled or invisible using
 * ContextMenuItem.setEnabled(boolean) and ContextMenuItem.setVisible(boolean)
 * methods.
 * 
 * @author Peter Lehto / IT Mill Oy Ltd
 */

/**
 * BROKEN MULTILEVEL MENU, simple (1-level menu works ok)
 * 
 */
@ClientWidget(VContextMenu.class)
public class ContextMenu extends AbstractComponent {

    private static final long serialVersionUID = 1729861951779648682L;
    private boolean visible;
    private int itemIndex;
    private int x;
    private int y;
    private final Map<String, List<String>> hierarchy;
    private final Map<String, ContextMenuItem> items;
    private final Set<String> hiddenItems;
    private final Set<String> disabledItems;

    /**
     * Creates new empty context menu
     */
    public ContextMenu() {
        setVisible(false);
        this.itemIndex = 0;

        items = new HashMap<String, ContextMenuItem>();
        hierarchy = new HashMap<String, List<String>>();
        hiddenItems = new HashSet<String>();
        disabledItems = new HashSet<String>();

        hierarchy.put("root", new ArrayList<String>());
    }

    @Override
    public void paintContent(PaintTarget target) throws PaintException {
        super.paintContent(target);

        Map<String, String> itemNames = new HashMap<String, String>();

        for (String itemId : items.keySet()) {
            itemNames.put(itemId, items.get(itemId).getName());
        }

        target.addAttribute("itemNames", itemNames);
        target.addAttribute("hierarchy", hierarchy.keySet().toArray());
        target.addAttribute("disabled", disabledItems.toArray());

        for (String rootId : hierarchy.keySet()) {
            List<String> itemsToAdd = new ArrayList<String>();

            for (Object itemId : hierarchy.get(rootId).toArray()) {
                if (!hiddenItems.contains(itemId)) {
                    itemsToAdd.add(itemId.toString());
                }
            }

            target.addAttribute(rootId, itemsToAdd.toArray());
        }

        if (visible) {
            target.addAttribute("show", true);
            target.addAttribute("left", x);
            target.addAttribute("top", y);
        } else {
            target.addAttribute("show", false);
        }
    }

    @Override
    public void changeVariables(Object source, Map<String, Object> variables) {
        super.changeVariables(source, variables);

        if (variables.containsKey("clickedItem")) {
            fireClick(items.get(variables.get("clickedItem")));
        }
    }

    /**
     * Pops up context menu in given coordinates
     * 
     * @param left
     *            - pixels from application's left border
     * @param top
     *            - pixels from application's top border
     */
    public void show(int left, int top) {
        setVisible(true);

        this.visible = true;
        this.x = left;
        this.y = top;

        requestRepaint();
    }

    /**
     * Hides context menu
     */
    public void hide() {
        visible = false;

        requestRepaint();
    }

    /**
     * Adds new item to context menu using given name. Returns corresponding
     * ContextMenuItem object that can be used for click event handling and
     * settings added item visible or enabled.
     * 
     * @param name
     * @return ContextMenuItem
     */
    public ContextMenuItem addItem(String name) {
        String itemKey = Integer.toString(itemIndex++);
        ContextMenuItem item = new ContextMenuItem(itemKey, name);

        items.put(itemKey, item);
        hierarchy.get("root").add(itemKey);

        requestRepaint();

        return item;
    }

    /**
     * ContextMenu's ClickListener that is called when an item from context menu
     * is clicked.
     * 
     * @author Peter Lehto / IT Mill Oy Ltd
     */
    public interface ClickListener extends Serializable {

        /**
         * Called when ContextMenuItem is clicked from ContextMenu
         * 
         * @param event
         */
        public void contextItemClick(ClickEvent event);
    }

    /**
     * Adds the context item click listener.
     * 
     * @param listener
     *            the Listener to be added.
     */
    public void addListener(ClickListener listener) {
        addListener(ClickEvent.class, listener, BUTTON_CLICK_METHOD);
    }

    /**
     * Removes the context item click listener.
     * 
     * @param listener
     *            the Listener to be removed.
     */
    public void removeListener(ClickListener listener) {
        removeListener(ClickEvent.class, listener, BUTTON_CLICK_METHOD);
    }

    protected void fireClick(ContextMenuItem contextMenuItem) {
        fireEvent(new ContextMenu.ClickEvent(this, contextMenuItem));
    }
    private static final Method BUTTON_CLICK_METHOD;

    static {
        try {
            BUTTON_CLICK_METHOD = ClickListener.class.getDeclaredMethod(
                    "contextItemClick", new Class[]{ClickEvent.class});
        } catch (final java.lang.NoSuchMethodException e) {
            throw new java.lang.RuntimeException(
                    "Internal error finding methods in ContextMenu");
        }
    }

    /**
     * ClickEvent is fired when context item is clicked from ContextMenu
     * 
     * @author Peter Lehto / IT Mill Oy Ltd
     */
    public class ClickEvent extends Component.Event {

        private static final long serialVersionUID = -7705638357488426038L;
        private final ContextMenuItem clickedItem;

        /**
         * New instance of context item click event.
         * 
         * @param source
         *            the Source of the event.
         */
        public ClickEvent(Component source, ContextMenuItem clickedItem) {
            super(source);

            this.clickedItem = clickedItem;
        }

        /**
         * Gets the ContextMenu where the event occurred.
         * 
         * @return the Source of the event.
         */
        public ContextMenu getContextMenu() {
            return (ContextMenu) getSource();
        }

        public ContextMenuItem getClickedItem() {
            return clickedItem;
        }
    }

    /**
     * ContextMenuItem is POJO that can be used to access items in ContextMenu.
     * ContextMenuItem has methods for hiding and disabling
     * 
     * @author Peter Lehto / IT Mill Oy Ltd
     */
    public class ContextMenuItem implements Serializable {

        private static final long serialVersionUID = 3828334687114823216L;
        private final String name;
        private boolean enabled;
        private boolean visible;
        private final String index;

        private ContextMenuItem(String index, String name) {
            this.index = index;
            this.name = name;

            visible = true;
            enabled = true;
        }

        public String getName() {
            return name;
        }

        String getIndex() {
            return index;
        }

        /**
         * Enables or disables this menu item
         * 
         * @param enabled
         */
        public void setEnabled(boolean enabled) {
            this.enabled = enabled;

            if (enabled) {
                disabledItems.remove(index);
            } else {
                disabledItems.add(index);
            }

            requestRepaint();
        }

        /**
         * @return true if menu item is enabled, false otherwise
         */
        public boolean isEnabled() {
            return enabled;
        }

        /**
         * Sets visible or hides this menu item
         * 
         * @param visible
         */
        public void setVisible(boolean visible) {
            this.visible = visible;

            if (visible) {
                hiddenItems.remove(index);
            } else {
                hiddenItems.add(index);
            }

            requestRepaint();
        }

        /**
         * @return true if menu item is visible, false otherwise
         */
        public boolean isVisible() {
            return visible;
        }

        /**
         * Adds new item to context menu as this menu's sub menu. Returns
         * corresponding ContextMenuItem object that can be used for click event
         * handling and settings added item visible or enabled.
         * 
         * @param name
         * @return ContextMenuItem
         */
        public ContextMenuItem addItem(String name) {
            String itemKey = Integer.toString(itemIndex++);

            ContextMenuItem item = new ContextMenuItem(itemKey, name);

            if (!hierarchy.containsKey(index)) {
                hierarchy.put(index, new ArrayList<String>());
            }

            hierarchy.get(index).add(itemKey);

            items.put(itemKey, item);

            requestRepaint();

            return item;
        }
    }
}