package ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.drawer;

import org.vaadin.gwtgraphics.client.shape.Circle;
import org.vaadin.gwtgraphics.client.shape.Path;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.Coordinates;

import static ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.Scaler.scale;

/**
 * @author n0weak
 */
public class PlaceDrawer extends AbstractDrawer {

    public static final int DEFAULT_RADIUS = DEFAULT_HEIGHT / 2;
    public static final int MARK_RADIUS = 3;
    private Circle circle;
    private Circle mark;
    private Path inStick;
    private Path outStick;

    /**
     * @param id string identifier of this component
     * @param x  x coordinate of the center of the component
     * @param y  y coordinate of the center of the component
     */
    public PlaceDrawer(String id, int x, int y) {
        super(id, x, y);
    }

    @Override
    public void moveComponent(int x, int y) {
        circle.setX(x);
        circle.setY(y + circle.getRadius());

        inStick.setX(circle.getX() - circle.getRadius());
        outStick.setX(circle.getX() + circle.getRadius());
        inStick.setY(circle.getY());
        outStick.setY(circle.getY());

        if (null != mark) {
            mark.setX(circle.getX());
            mark.setY(circle.getY());
        }
    }

    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        if (null != mark) {
            if (!enabled) {
                mark.setFillColor(COLOR_DISABLED);
            }
            else {
                mark.setFillColor(COLOR_DEFAULT);
            }
        }
    }

    public void setSelected(boolean selected) {
        super.setSelected(selected);
        if (null != mark) {
            if (selected) {
                mark.setFillColor(COLOR_SELECTED);
            }
            else {
                mark.setFillColor(COLOR_DEFAULT);
            }
        }
    }

    public Coordinates getCenter() {
        return new Coordinates(circle.getX(), circle.getY());
    }

    public void mark() {
        if (null == mark) {
            mark = new Circle(circle.getX(), circle.getY(), scale(MARK_RADIUS));
            mark.setFillColor(circle.getStrokeColor());
            mark.setStrokeColor(circle.getStrokeColor());
            group.add(mark);
        }
    }

    public void unmark() {
        if (null != mark) {
            group.remove(mark);
            mark = null;
        }
    }

    public Coordinates getOutputCoordinates(int connectorIdx) {
        return new Coordinates(circle.getX() + circle.getRadius() + scale(STICK_SIZE), circle.getY());
    }

    public Coordinates getInputCoordinates(int connectorIdx) {
        return new Coordinates(circle.getX() - circle.getRadius(), circle.getY());
    }

    public boolean isContainedIn(int x1, int y1, int x2, int y2) {
        int radius = circle.getRadius();
        int x = circle.getX();
        int y = circle.getY();
        return x - radius >= x1 && x + radius <= x2 && y - radius >= y1 && y + radius <= y2;
    }

    @Override
    protected void paintComponent(String id, int x, int y) {
        circle = new Circle(x, y + scale(DEFAULT_RADIUS), scale(DEFAULT_RADIUS));
        circle.setFillOpacity(0);
        group.add(circle);

        inStick = new Path(circle.getX() - circle.getRadius(), circle.getY());
        inStick.lineRelativelyTo(-scale(STICK_SIZE), 0);
        group.add(inStick);

        outStick = new Path(circle.getX() + circle.getRadius(), circle.getY());
        outStick.lineRelativelyTo(scale(STICK_SIZE), 0);
        group.add(outStick);

        if (null != mark) {
            mark = null;
            mark();
        }

        super.paintComponent(id, x, y);
    }

    public Coordinates getTopLeft() {
        return new Coordinates(circle.getX() - circle.getRadius() - scale(STICK_SIZE), circle.getY()
                                                                                       - circle.getRadius());
    }

    public int getWidth() {
        return circle.getRadius() * 2 + scale(STICK_SIZE * 2);
    }

    public int getSticksCount(boolean output) {
        return 1;
    }

    @Override
    public int getOutputId(Coordinates mousePos) {
        return 0;
    }

    @Override
    public int getInputId(Coordinates mousePos) {
        return 0;
    }
}
