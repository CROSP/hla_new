package ua.cn.stu.cs.ems.ui.vaadin.widgets;


import com.vaadin.terminal.PaintException;
import com.vaadin.terminal.PaintTarget;
import com.vaadin.ui.AbstractComponent;
import com.vaadin.ui.ClientWidget;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.GwtJessCanvas;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.VJessCanvas;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.DrawingMode;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.EntityType;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.PathCorrector;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.event.*;

import java.util.*;

import static ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.VJessCanvas.*;

@ClientWidget(VJessCanvas.class)
public class JessCanvas extends AbstractComponent {

    final static Logger logger = LoggerFactory.getLogger(JessCanvas.class);
// ------------------------------ FIELDS ------------------------------
    public static final int SCALE_MIN = GwtJessCanvas.SCALE_MIN;
    public static final int SCALE_MAX = GwtJessCanvas.SCALE_MAX;
    public static final int SCALE_STEP = GwtJessCanvas.SCALE_STEP;
    /**
     * Canvas is drawn improperly when scale is set to this value.
     */
    public static final int UNWANTED_SCALE = 110;
    private Set<String> updates = new HashSet<String>();
    private DrawingMode drawingMode = DrawingMode.SELECT;
    private EntityType entityType = EntityType.PLACE;
    private String entityId = "";
    private CanvasListener canvasListener;
    private int scale = 100;
    private boolean gridAdjustEnabled = true;
    private boolean gridPainted = true;
    private boolean pathCorrectionEnabled = true;
    private boolean horizontalLinesDrawnFirst = PathCorrector.isHorizontalFirst();
    private boolean previewDrawn = false;
    private int baseWidth;
    private int baseHeight;
    private int scaledWidth;
    private int scaledHeight;
    private Set<String> markedEntities = new HashSet<String>();
    private Set<String> unmarkedEntities = new HashSet<String>();
    private String[] selectedEntities;
    private String[] deletedEntities;
    private List<String> updatedNames = new ArrayList<String>();
    private List<String> createdEntities = new ArrayList<String>();
    private List<String> connectedEntities = new ArrayList<String>();

// --------------------- GETTER / SETTER METHODS ---------------------
    public int getBaseHeight() {
        return baseHeight;
    }

    public int getBaseWidth() {
        return baseWidth;
    }

    public DrawingMode getDrawingMode() {
        return drawingMode;
    }

    public boolean isGridAdjustEnabled() {
        return gridAdjustEnabled;
    }

    public boolean isGridPainted() {
        return gridPainted;
    }

    public boolean isHorizontalLinesDrawnFirst() {
        return horizontalLinesDrawnFirst;
    }

    public boolean isPathCorrectionEnabled() {
        return pathCorrectionEnabled;
    }

    public void setCanvasListener(CanvasListener canvasListener) {
        this.canvasListener = canvasListener;
    }

// ------------------------ INTERFACE METHODS ------------------------
// --------------------- Interface Component ---------------------
    /**
     * Selection, deletion and mode switching is blocked and canvas is switch to the {@link DrawingMode#BLOCKED}
     * mode when component is in readOnly state.
     *
     * @param readOnly defines whether component must be switched to readOnly state
     */
    @Override
    public void setReadOnly(boolean readOnly) {
        updates.add(VJessCanvas.READ_ONLY);
        super.setReadOnly(readOnly);
        requestRepaint();
    }

// --------------------- Interface Sizeable ---------------------
    /**
     * @return actual width of the canvas, which is evaluated as base width * scale
     */
    @Override
    public float getWidth() {
        return scaledWidth;
    }

    @Override
    @Deprecated
    @SuppressWarnings("deprecation")
    public void setWidth(float width) {
        throw new UnsupportedOperationException("Please, use #setWidth(int)!");
    }

    /**
     * @return actual height of the canvas, which is evaluated as base height * scale
     */
    @Override
    public float getHeight() {
        return scaledHeight;
    }

    @Override
    @Deprecated
    @SuppressWarnings("deprecation")
    public void setHeight(float height) {
        throw new UnsupportedOperationException("Please, use #setHeight(int)!");
    }

    @Override
    @Deprecated
    public void setHeight(String height) {
        throw new UnsupportedOperationException("Please, use #setHeight(int)!");
    }

    @Override
    @Deprecated
    public void setWidth(float width, int unit) {
        throw new UnsupportedOperationException("Please, use #setWidth(int)!");
    }

    @Override
    @Deprecated
    public void setHeight(float height, int unit) {
        throw new UnsupportedOperationException("Please, use #setHeight(int)!");
    }

    @Override
    @Deprecated
    public void setWidth(String width) {
        throw new UnsupportedOperationException("Please, use #setWidth(int)!");
    }

// --------------------- Interface VariableOwner ---------------------
    /**
     * Deserialize changes received from client.
     * See chapter 10.6.3. Client Server Deserialization in the Book of Vaadin.
     */
    @Override
    public void changeVariables(Object source, Map<String, Object> variables) {
        if (variables.containsKey(WIDTH)) {
            scaledWidth = (Integer) variables.get(WIDTH);
        }

        if (variables.containsKey(HEIGHT)) {
            scaledHeight = (Integer) variables.get(HEIGHT);
        }

        if (variables.containsKey(CREATION_EVENT)) {
            setEntityId(canvasListener.entityCreated(CreationEvent.deserialize((String) variables.get(
                    VJessCanvas.CREATION_EVENT))));
        }

        if (variables.containsKey(CONNECTION_EVENT)) {
            canvasListener.entitiesConnected(ConnectionEvent.deserialize((String) variables.get(
                    VJessCanvas.CONNECTION_EVENT)));
        }

        if (variables.containsKey(POSITION_STATE_EVENT)) {
            canvasListener.positionStateChanged((String) variables.get(VJessCanvas.POSITION_STATE_EVENT));
        }

        if (variables.containsKey(DELETION_EVENT)) {
            canvasListener.entitiesDeleted(DeletionEvent.deserialize((String) variables.get(VJessCanvas.DELETION_EVENT)));
        }

        if (variables.containsKey(DISCONNECTION_EVENT)) {
            canvasListener.entitiesDisconnected(DisconnectionEvent.deserialize((String) variables.get(
                    VJessCanvas.DISCONNECTION_EVENT)));
        }

        if (variables.containsKey(DOUBLE_CLICK_EVENT) && !isReadOnly()) {
            canvasListener.entityDoubleClicked(EntityEvent.deserialize((String) variables.get(DOUBLE_CLICK_EVENT)));
        }
        if (variables.containsKey(MOVE_EVENT) && !isReadOnly()) {
            canvasListener.entityMoved(MoveEvent.deserialize((String) variables.get(MOVE_EVENT)));
        }
        if (variables.containsKey(RESIZE_EVENT) && !isReadOnly()) {
            ResizeEvent event = ResizeEvent.deserialize((String) variables.get(RESIZE_EVENT));
            canvasListener.canvasResized(event);
            this.baseHeight = event.getBaseHeight();
            this.baseWidth = event.getBaseWidth();
        }
        if (variables.containsKey(DEBUG)) {
            logger.debug("GWT DEBUG: " + variables.get(DEBUG));
        }
    }

// -------------------------- OTHER METHODS --------------------------
    /**
     * Delete last registered updates in queue.
     */
    public void clear() {
        updates.add(CLEAR_REQUEST);
        requestRepaint();
    }

    public void setPreviewDrawn(boolean previewDrawn) {
        updates.add(PREVIEW_DRAWN);
        this.previewDrawn = previewDrawn;
        requestRepaint();
    }

    public void connect(Collection<ConnectionEvent> events) {
        for (ConnectionEvent event : events) {
            connectedEntities.add(event.serialize());
        }
        requestRepaint();
    }

    public void create(Collection<CreationEvent> events) {
        DrawingMode mode = getDrawingMode();
        for (CreationEvent event : events) {
            createdEntities.add(event.serialize());
        }
        setDrawingMode(mode);
    }

    public void delete(String... ids) {
        updates.add(DELETED_ENTITIES);
        deletedEntities = ids;
        requestRepaint();
    }

    public void mark(String id) {
        unmarkedEntities.remove(id);
        markedEntities.add(id);
        requestRepaint();
    }

    /**
     * Paint (serialize) the component for the client.
     */
    @Override
    public void paintContent(PaintTarget target) throws PaintException {
        super.paintContent(target);

        if (!isReadOnly()) {
            if (updates.contains(DRAWING_MODE)) {
                target.addVariable(this, DRAWING_MODE, drawingMode.toString());
                updates.remove(DRAWING_MODE);
            }
            if (updates.contains(ENTITY_TYPE)) {
                target.addVariable(this, ENTITY_TYPE, entityType.serialize());
                updates.remove(ENTITY_TYPE);
            }

            if (updates.contains(ENTITY_ID)) {
                target.addVariable(this, ENTITY_ID, entityId);
                updates.remove(ENTITY_ID);
            }

            if (null != selectedEntities) {
                target.addVariable(this, SELECTED_ENTITIES, selectedEntities);
                selectedEntities = null;
            }
            if (null != deletedEntities) {
                target.addVariable(this, DELETED_ENTITIES, deletedEntities);
                deletedEntities = null;
            }
            if (!updatedNames.isEmpty()) {
                target.addVariable(this, UPDATED_NAMES, updatedNames.toArray(new String[updatedNames.size()]));
                updatedNames.clear();
            }
            if (!createdEntities.isEmpty()) {
                target.addVariable(this, CREATED_ENTITIES, createdEntities.toArray(new String[createdEntities.size()]));
                createdEntities.clear();
            }
            if (!connectedEntities.isEmpty()) {
                target.addVariable(this, CONNECTED_ENTITIES,
                        connectedEntities.toArray(new String[connectedEntities.size()]));
                connectedEntities.clear();
            }
            if (updates.contains(CLEAR_REQUEST)) {
                target.addVariable(this, CLEAR_REQUEST, "");
                updates.remove(CLEAR_REQUEST);
            }
        }

        if (updates.contains(PREVIEW_DRAWN)) {
            target.addVariable(this, PREVIEW_DRAWN, previewDrawn);
            updates.remove(PREVIEW_DRAWN);
        }

        if (updates.contains(GRID_ADJUST_ENABLED)) {
            target.addVariable(this, GRID_ADJUST_ENABLED, gridAdjustEnabled);
            updates.remove(GRID_ADJUST_ENABLED);
        }

        if (updates.contains(GRID_PAINTED)) {
            target.addVariable(this, GRID_PAINTED, gridPainted);
            updates.remove(GRID_PAINTED);
        }

        if (updates.contains(PATH_CORRECTION_ENABLED)) {
            target.addVariable(this, PATH_CORRECTION_ENABLED, pathCorrectionEnabled);
            updates.remove(PATH_CORRECTION_ENABLED);
        }
        if (updates.contains(SCALE)) {
            target.addVariable(this, SCALE, scale);
            updates.remove(SCALE);
        }

        if (updates.contains(VJessCanvas.READ_ONLY)) {
            target.addVariable(this, VJessCanvas.READ_ONLY, isReadOnly());
            updates.remove(READ_ONLY);
        }

        if (updates.contains(HEIGHT)) {
            target.addVariable(this, HEIGHT, baseHeight);
            updates.remove(HEIGHT);
        }
        if (updates.contains(WIDTH)) {
            target.addVariable(this, WIDTH, baseWidth);
            updates.remove(WIDTH);
        }
        if (updates.contains(HORIZ_LINES_DRAWN_FIRST)) {
            target.addVariable(this, HORIZ_LINES_DRAWN_FIRST, horizontalLinesDrawnFirst);
            updates.remove(HORIZ_LINES_DRAWN_FIRST);
        }
        if (!unmarkedEntities.isEmpty()) {
            target.addVariable(this, UNMARKED_ENTITIES, unmarkedEntities.toArray(new String[unmarkedEntities.size()]));
            unmarkedEntities.clear();
        }
        if (!markedEntities.isEmpty()) {
            target.addVariable(this, MARKED_ENTITIES, markedEntities.toArray(new String[markedEntities.size()]));
            markedEntities.clear();
        }
    }

    public void scale(int scale) {
        updates.add(SCALE);
        this.scale = scale;
        requestRepaint();
    }

    /**
     * Selects specified entities. All previously made selection will be cleared.
     *
     * @param ids identifiers of the entities to be selected.
     * @see GwtJessCanvas#select(String...)
     */
    public void select(String... ids) {
        updates.add(SELECTED_ENTITIES);
        selectedEntities = ids;
        requestRepaint();
    }

    public void setGridAdjustEnabled(boolean gridAdjustEnabled) {
        updates.add(GRID_ADJUST_ENABLED);
        this.gridAdjustEnabled = gridAdjustEnabled;
        requestRepaint();
    }

    public void setGridPainted(boolean gridPainted) {
        updates.add(GRID_PAINTED);
        this.gridPainted = gridPainted;
        requestRepaint();
    }

    /**
     * Adapts height of the canvas to the value specified. If current scale is != 1, then actual height of the canvas will be recounted using scale value.
     *
     * @param height desirable height of the canvas with scale = 1
     */
    public void setHeight(int height) {
        updates.add(HEIGHT);
        baseHeight = height;
        requestRepaint();
    }

    public void setHorizontalLinesDrawnFirst(boolean horizontalLinesDrawnFirst) {
        updates.add(HORIZ_LINES_DRAWN_FIRST);
        this.horizontalLinesDrawnFirst = horizontalLinesDrawnFirst;
        requestRepaint();
    }

    public void setPathCorrectionEnabled(boolean pathCorrectionEnabled) {
        updates.add(PATH_CORRECTION_ENABLED);
        this.pathCorrectionEnabled = pathCorrectionEnabled;
        requestRepaint();
    }

    /**
     * Adapts width of the canvas to the value specified. If current scale is != 1, then actual width of the canvas will be recounted using scale value.
     *
     * @param width desirable width of the canvas with scale = 1
     */
    public void setWidth(int width) {
        updates.add(WIDTH);
        baseWidth = width;
        requestRepaint();
    }

    public void toggleCreationMode(EntityType entityType, String entityId) {
        updates.add(ENTITY_TYPE);
        this.entityType = entityType;
        requestRepaint();
        setEntityId(entityId);
        setDrawingMode(DrawingMode.CREATE);
        requestRepaint();
    }

    public void setEntityId(String entityId) {
        logger.debug("setting entity id=" + entityId);
        updates.add(ENTITY_ID);
        this.entityId = entityId;
        requestRepaint();
    }

    public void setDrawingMode(DrawingMode mode) {
        updates.add(DRAWING_MODE);
        this.drawingMode = mode;
        requestRepaint();
    }

    public void unmark(String id) {
        markedEntities.remove(id);
        unmarkedEntities.add(id);
        requestRepaint();
    }

    public void updateName(String oldName, String newName) {
        updatedNames.add(oldName);
        updatedNames.add(newName);
        requestRepaint();
    }

// -------------------------- INNER CLASSES --------------------------
    public interface CanvasListener {

        /**
         * Happens when new entity is added to the canvas. If entity is added close to the borders of the canvas then canvas expands.
         * Check {@link ua.cn.stu.cs.jess.ui.vaadin.widgets.JessCanvas#getWidth()} and {@link ua.cn.stu.cs.jess.ui.vaadin.widgets.JessCanvas#getHeight()} to get new values.
         *
         * @param event object which represents creation event
         * @return identifier to be used during next element creation
         */
        String entityCreated(CreationEvent event);

        void entitiesConnected(ConnectionEvent event);

        void positionStateChanged(String positionId);

        void entitiesDeleted(DeletionEvent event);

        void entitiesDisconnected(DisconnectionEvent event);

        void entityDoubleClicked(EntityEvent event);

        void entityMoved(MoveEvent event);

        void canvasResized(ResizeEvent event);
    }
}
