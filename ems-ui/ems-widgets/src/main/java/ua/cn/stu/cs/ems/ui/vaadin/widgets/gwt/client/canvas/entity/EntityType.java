package ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity;

import java.util.Collections;
import java.util.Queue;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.drawer.AggregateDrawer;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.drawer.FederateDrawer;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.drawer.InputDrawer;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.drawer.OutputDrawer;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.drawer.PlaceDrawer;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.drawer.TransitionDrawer;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.event.EventUtils;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.event.Stringalizable;

/**
 * @author n0weak
 */
public enum EntityType {

    PLACE {

        public ActiveEntity createEntity(String id, int x, int y) {
            return new Place(this, id, new PlaceDrawer(id, x, y));
        }
    },
    INPUT {

        public ActiveEntity createEntity(String id, int x, int y) {
            return new Input(this, id, new InputDrawer(id, x, y));
        }
    },
    OUTPUT {

        public ActiveEntity createEntity(String id, int x, int y) {
            return new Output(this, id, new OutputDrawer(id, x, y));
        }
    },
    TTRANSITION {

        public ActiveEntity createEntity(String id, int x, int y) {
            return new TTransition(this, id, new TransitionDrawer.TDrawer(id, x, y));
        }
    },
    XTRANSITION {

        public ActiveEntity createEntity(String id, int x, int y) {
            return new XTransition(this, id, new TransitionDrawer.XDrawer(id, x, y, getCountInputs(), getCountOutputs()));
        }
    },
    YTRANSITION {

        public ActiveEntity createEntity(String id, int x, int y) {
            return new YTransition(this, id, new TransitionDrawer.YDrawer(id, x, y, getCountInputs(), getCountOutputs()));
        }
    },
    FTRANSITION {

        public ActiveEntity createEntity(String id, int x, int y) {
            return new FTransition(this, id, new TransitionDrawer.FDrawer(id, x, y, getCountInputs(), getCountOutputs()));
        }
    },
    JTRANSITION {

        public ActiveEntity createEntity(String id, int x, int y) {
            return new JTransition(this, id, new TransitionDrawer.JDrawer(id, x, y, getCountInputs(), getCountOutputs()));
        }
    },
    FIFO {

        public ActiveEntity createEntity(String id, int x, int y) {
            return new Fifo(this, id, new TransitionDrawer.FifoDrawer(id, x, y));
        }
    },
    PFIFO {

        public ActiveEntity createEntity(String id, int x, int y) {
            return new PFifo(this, id, new TransitionDrawer.PFifoDrawer(id, x, y));
        }
    },
    LIFO {

        public ActiveEntity createEntity(String id, int x, int y) {
            return new Lifo(this, id, new TransitionDrawer.LifoDrawer(id, x, y));
        }
    },
    PLIFO {

        public ActiveEntity createEntity(String id, int x, int y) {
            return new PLifo(this, id, new TransitionDrawer.PLifoDrawer(id, x, y));
        }
    },
    AGGREGATE {

        public ActiveEntity createEntity(String id, int x, int y) {
            return new Aggregate(this, id, new AggregateDrawer(id, x, y, getInputNames(), getOutputNames()));
        }
    },
    FEDERATE {

        public ActiveEntity createEntity(String id, int x, int y) {
            return new Federate(this, id, new FederateDrawer(id, x, y, getInputNames(), getOutputNames()));
        }
    };
    protected int countInputs = 0;
    protected int countOutputs = 0;
    protected String[] inputNames = null;
    protected String[] outputNames = null;

    public void setEntityConnectorsCount(int countInputs, int countOutputs) {
        setEntityConnectorsNames(null, null);
        this.countInputs = countInputs;
        this.countOutputs = countOutputs;
    }

    public void setEntityConnectorsNames(String[] inputNames, String[] outputNames) {
        this.inputNames = inputNames;
        this.outputNames = outputNames;
        
        if (inputNames != null) {
            countInputs = inputNames.length;
        }
        else {
            countInputs = 0;
        }
        if (outputNames != null) {
            countOutputs = outputNames.length;
        }
        else {
            countOutputs = 0;
        }
    }

    public int getCountInputs() {
        return countInputs;
    }

    public int getCountOutputs() {
        return countOutputs;
    }

    public String[] getInputNames() {
        return inputNames;
    }

    public String[] getOutputNames() {
        return outputNames;
    }

    public String serialize() {
        StringBuilder sb = new StringBuilder();

        sb.append(this.toString()).append(Stringalizable.DELIM);
        sb.append(getCountInputs()).append(Stringalizable.DELIM).append(getCountOutputs());
        if (this == AGGREGATE) {
            if (inputNames != null)
                for (int i = 0; i < getCountInputs(); i++) {
                    sb.append(Stringalizable.DELIM).append(inputNames[i]);
                }
            if (outputNames != null)
                for (int i = 0; i < getCountOutputs(); i++) {
                    sb.append(Stringalizable.DELIM).append(outputNames[i]);
                }
        }
        return sb.toString();
    }

    public static EntityType deserialize(String tokenizedString) {
        return EntityType.deserialize(EventUtils.tokenize(tokenizedString));
    }

    public static EntityType deserialize(Queue<String> tokens) {
        EntityType type = EntityType.valueOf(tokens.poll());
        type.setEntityConnectorsCount(Integer.valueOf(tokens.poll()), Integer.valueOf(tokens.poll()));

        if (type == AGGREGATE) {
            String[] namesIn = null;
            String[] namesOut = null;
            if (type.getCountInputs() > 0) {
                namesIn = new String[type.getCountInputs()];
                for (int i = 0; i < type.getCountInputs(); i++) {
                    namesIn[i] = tokens.poll();
                }
            }
            if (type.getCountOutputs() > 0) {
                namesOut = new String[type.getCountOutputs()];
                for (int i = 0; i < type.getCountOutputs(); i++) {
                    namesOut[i] = tokens.poll();
                }
            }
            type.setEntityConnectorsNames(namesIn, namesOut);
        }
        return type;
    }

    public abstract ActiveEntity createEntity(String id, int x, int y);
}
