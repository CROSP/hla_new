package ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.event;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

/**
 * @author n0weak
 */
public class DisconnectionEvent implements Stringalizable {
// ------------------------------ FIELDS ------------------------------

    private final List<DisconnectionDetails> details;

// -------------------------- STATIC METHODS --------------------------
    /**
     * Constructs entity from its serialized representation. Consumes all the input, trying to convert it into the list of {@link DisconnectionDetails}.
     *
     * @param serializedString serialized representation of the entity.
     * @return deserialized entity if process was successful.
     * @throws IllegalArgumentException if deserialization failed
     * @see #serialize()
     */
    public static DisconnectionEvent deserialize(String serializedString) {
        try {
            return deserialize(EventUtils.tokenize(serializedString));
        } catch (Exception e) {
            throw new IllegalArgumentException("Cannot deserialize DeletionEvent from supplied data: "
                                               + serializedString, e);
        }
    }

    static DisconnectionEvent deserialize(Queue<String> tokens) {
        List<DisconnectionDetails> list = new ArrayList<DisconnectionDetails>();
        while (!tokens.isEmpty()) {
            list.add(DisconnectionDetails.deserialize(tokens));
        }
        return new DisconnectionEvent(list);
    }

// --------------------------- CONSTRUCTORS ---------------------------
    public DisconnectionEvent(List<DisconnectionDetails> details) {
        this.details = details;
    }

// --------------------- GETTER / SETTER METHODS ---------------------
    public List<DisconnectionDetails> getDetails() {
        return details;
    }

// ------------------------ INTERFACE METHODS ------------------------
// --------------------- Interface Stringalizable ---------------------
    public String serialize() {
        StringBuilder sb = new StringBuilder();
        for (DisconnectionDetails detail : details) {
            sb.append(detail.serialize()).append(DELIM);
        }
        return sb.toString();
    }

// -------------------------- INNER CLASSES --------------------------
    public static class DisconnectionDetails implements Stringalizable {

        /**
         * Deserializes instance of this class from the queue of strings.
         *
         * @param tokens queue of strings, representing serialized state of this instance. Must contain at least 3 elements. 1st string in the queue is treated as {@link #placeId},
         *               2nd - as {@link #transitionId}, 3d - {@link #connectionType}. Used tokens will be polled from the queue.
         * @return deserialized entity if process was successful.
         * @throws IllegalArgumentException if deserialization failed
         */
        static DisconnectionDetails deserialize(Queue<String> tokens) {
            try {
                return new DisconnectionDetails(tokens.poll(), tokens.poll(), 
                        ConnectionType.valueOf(tokens.poll()), tokens.poll());
            } catch (Exception e) {
                throw new IllegalArgumentException("Cannot deserialize DisconnectionDetails from supplied data", e);
            }
        }
        private final String placeId;
        private final String transitionId;
        private final ConnectionType connectionType;
        private final String transitionIOName;

        public DisconnectionDetails(String placeId, String transitionId, ConnectionType connectionType,
                String transitionIOName) {
            this.placeId = placeId;
            this.connectionType = connectionType;
            this.transitionId = transitionId;
            this.transitionIOName = transitionIOName;
        }

        public String getPlaceId() {
            return placeId;
        }

        public ConnectionType getConnectionType() {
            return connectionType;
        }

        public String getTransitionId() {
            return transitionId;
        }
        
        public String getTransitionIOName() {
            return transitionIOName;
        }

        public String serialize() {
            return new StringBuilder().append(placeId).append(DELIM).append(transitionId).append(DELIM).
                    append(connectionType).append(DELIM).append(transitionIOName).toString();
        }

        public enum ConnectionType {

            INPUT, OUTPUT
        }
    }
}
