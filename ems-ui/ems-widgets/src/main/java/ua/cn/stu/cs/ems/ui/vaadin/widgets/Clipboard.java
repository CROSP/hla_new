package ua.cn.stu.cs.ems.ui.vaadin.widgets;

import com.vaadin.terminal.PaintException;
import com.vaadin.terminal.PaintTarget;
import com.vaadin.ui.AbstractComponent;

/**
 *
 * @author slava
 */
@com.vaadin.ui.ClientWidget(ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.VClipboard.class)
public class Clipboard extends AbstractComponent {

    private String clipboardText = "";

    @Override
    public void paintContent(PaintTarget target) throws PaintException {
        super.paintContent(target);
        target.addAttribute("clipboardText", clipboardText);
    }

    public void setClipboardText(String clipboardText) {
        this.clipboardText = clipboardText;
        requestRepaint();
    }
}
