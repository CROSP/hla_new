/* 
 * Copyright 2010 IT Mill Ltd.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client;

import com.google.gwt.dom.client.Document;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FocusPanel;
import org.vaadin.gwtgraphics.client.DrawingArea;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.*;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.*;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.event.*;

import java.util.*;

public class GwtJessCanvas extends Composite implements IJessCanvas {
// ------------------------------ FIELDS ------------------------------

    public static final int SCALE_MIN = 20;
    public static final int SCALE_MAX = 150;
    public static final int SCALE_STEP = 10;
    private double scale = 100;
    /**
     * Each time entity is created at location with x >= {@link #getWidth()}  - GROWING_GAP} or y >=  {@link #getHeight()}  - GROWING_GAP
     * canvas expands its width abd height respectively on {@link #GROWING_STEP}
     */
    private final static int GROWING_GAP = 150;
    /**
     * Each time entity is created at location with x >= {@link #getWidth()}  - {@link #GROWING_GAP} or y >=  {@link #getHeight()}  -  {@link #GROWING_GAP}
     * canvas expands its width abd height respectively on GROWING_STEP
     */
    private final static int GROWING_STEP = 150;
    private DrawingArea canvas;
    /**
     * Already created shape which is now moved by user. Applicable to the {@link DrawingMode#MOVE}
     */
    private ActiveEntity movingShape;
    /**
     * Potential candidate for creation, preview of the entity. Applicable to the {@link DrawingMode#CREATE}
     */
    private ActiveEntity newbornShape;
    private DrawingMode drawingMode = DrawingMode.SELECT;
    private String entityTypeRepresentation;
    private Connector currentConnector;
    private CanvasEventHandler eventHandler;
    private String entityId;
    private Set<Connector> connectors = new HashSet<Connector>();
    private Map<String, ActiveEntity> entities = new HashMap<String, ActiveEntity>();
    private Set<CanvasEntity> selectedEntities = new HashSet<CanvasEntity>();
    private List<JessCanvasListener> listeners = new ArrayList<JessCanvasListener>();
    private DrawingMode suspendedMode;
    private boolean readOnly;
    private int baseWidth;
    private int baseHeight;
    private boolean previewDrawn;
    private final FocusPanel panel;
    private final PathCorrector pathCorrector;

// --------------------------- CONSTRUCTORS ---------------------------
    public GwtJessCanvas() {
        EntityType entityType = EntityType.PLACE;
        entityType.setEntityConnectorsCount(1, 1);
        entityTypeRepresentation = entityType.serialize();
        
        panel = new FocusPanel();

        canvas = new DrawingArea(0, 0);
        CanvasHandler canvasHandler = new CanvasHandler();
        canvas.addClickHandler(canvasHandler);
        canvas.addMouseMoveHandler(canvasHandler);
        canvas.addMouseDownHandler(canvasHandler);
        canvas.addMouseUpHandler(canvasHandler);
        panel.addKeyUpHandler(canvasHandler);
        panel.addKeyDownHandler(canvasHandler);
        addListener(canvasHandler);
        panel.add(canvas);

        Grid.setAdjustEnabled(true);

        pathCorrector = new PathCorrector(this);
        Connector.setPathCorrector(pathCorrector);

        panel.setStyleName(Grid.getGridStyleName());

        initWidget(panel);
    }

    private void addListener(JessCanvasListener listener) {
        listeners.add(listener);
    }

    public void setPreviewDrawn(boolean previewDrawn) {
        if (this.previewDrawn != previewDrawn && null != newbornShape) {
            if (previewDrawn) {
                canvas.add(newbornShape.getVectorObject());
            } else {
                canvas.remove(newbornShape.getVectorObject());
            }
        }
        this.previewDrawn = previewDrawn;
    }

    // --------------------- GETTER / SETTER METHODS ---------------------
    public DrawingMode getDrawingMode() {
        return drawingMode;
    }

    public String getEntityId() {
        return entityId;
    }

    public EntityType getEntityType() {
        return EntityType.deserialize(entityTypeRepresentation);
    }

    public boolean isReadOnly() {
        return readOnly;
    }

    public void setReadOnly(boolean readOnly) {
        if (readOnly) {
            if (null == suspendedMode) {
                suspendedMode = drawingMode;
                setDrawingMode(DrawingMode.BLOCKED);
            }
        } else if (null != suspendedMode) {
            setDrawingMode(suspendedMode);
            suspendedMode = null;
        }
        this.readOnly = readOnly;
    }
    
    public void debug(String msg) {
        eventHandler.debug(msg);
    }

    void setEventHandler(CanvasEventHandler eventHandler) {
        this.eventHandler = eventHandler;
    }

// ------------------------ INTERFACE METHODS ------------------------
// --------------------- Interface IJessCanvas ---------------------
    /**
     * @return actual width of the canvas, which is base width * scale
     */
    public int getWidth() {
        return canvas.getWidth();
    }

    /**
     * @return actual height of the canvas, which is base height * scale
     */
    public int getHeight() {
        return canvas.getHeight();
    }

    public Collection<ActiveEntity> getEntitiesList() {
        return entities.values();
    }

// -------------------------- OTHER METHODS --------------------------
    void clear() {
        canvas.clear();
        entities.clear();
        connectors.clear();
        selectedEntities.clear();
        currentConnector = null;
        movingShape = null;
        if (null != newbornShape) {
            canvas.add(newbornShape.getVectorObject());
        }
    }

    void connect(ConnectionEvent event) {
        ActiveEntity from = entities.get(event.getFrom().getId());
        ActiveEntity to = entities.get(event.getTo().getId());

        if (null != from && null != to) {
            Connector connector = from.connectOutput(event.getOutputName());
            if (connector == null) {
                return; // cant connect - output not found or already connected
            }

            for (Coordinates c : event.getFixedCoords()) {
                connector.fix(Grid.adjust(Scaler.scale(c)));
            }

            to.connectInput(connector, event.getInputName());
            to.fix();
            connectors.add(connector);
            connector.addClickHandler(new Selector(connector));
            canvas.add(connector.getVectorObject());
            if (DrawingMode.CONNECT == drawingMode) {
                reenable();
            }
        }
    }

    private void reenable() {
        for (ActiveEntity entity : entities.values()) {
            entity.setEnabled(entity.acceptsConnection());
        }
    }

    void create(CreationEvent event) {
        if (null != entities.get(event.getEntityDetails().getId())) {
            return; // already exist
        }

        boolean canvasResized = false;
        ActiveEntity entity = event.create();
        canvas.add(entity.getVectorObject());

        int x = event.getEntityDetails().getCenterCoords().x;
        int y = event.getEntityDetails().getCenterCoords().y;
        
        x += entity.getWidth() / 2;
        y += entity.getHeight() / 2;
        
        canvasResized |= tryResize(x, y);

        registerEntity(entity, true);

        if (canvasResized) {
            fireResizeEvent();
        }
    }

    void create(Collection<CreationEvent> events) {
        for (CreationEvent event : events) {
            create(event);
        }
    }

    private boolean tryResize(int x, int y) {
        boolean needsUpdate = false;
        if (x > Scaler.scale(baseWidth - GROWING_GAP)) {
            setWidth(x + GROWING_STEP);
            needsUpdate = true;
        }

        if (y > Scaler.scale(baseHeight) - GROWING_GAP) {
            setHeight(y + GROWING_STEP);
            needsUpdate = true;
        }

        return needsUpdate;
    }

    private void registerEntity(final ActiveEntity entity, boolean silent) {
        EntityHandler handler = new EntityHandler(entity);

        entity.addDoubleClickHandler(handler);
        entity.addClickHandler(handler);
        entity.addMouseOverHandler(handler);
        entity.addMouseOutHandler(handler);

        if (entity instanceof Markable) {
            entity.addClickHandler(new Marker(entity));
        }

        entities.put(entity.getId(), entity);
        if (!silent) {
            eventHandler.entityCreated(new CreationEvent(new EntityDetails(entity)));
        }
    }

    private void fireResizeEvent() {
        eventHandler.canvasResized(new ResizeEvent(
                Scaler.scale(baseWidth), Scaler.scale(baseHeight), 
                baseWidth, baseHeight));
    }

    public void delete(String... ids) {
        List<CanvasEntity> list = new ArrayList<CanvasEntity>(ids.length);
        for (String id : ids) {
            CanvasEntity entity = entities.get(id);
            if (null != entity) {
                list.add(entity);
            }
        }

        delete(list);
    }

    private void delete(Collection<CanvasEntity> list) {
        List<EntityDetails> deletedEntities = new ArrayList<EntityDetails>();
        List<DisconnectionEvent.DisconnectionDetails> disconnectedEntities =
                new ArrayList<DisconnectionEvent.DisconnectionDetails>();

        for (CanvasEntity ce : list) {
            if (ce instanceof ActiveEntity) {
                ActiveEntity ae = (ActiveEntity) ce;
                entities.remove(ae.getId());
                deletedEntities.add(new EntityDetails(ae.getId(), ae.getType(), Scaler.unscale(ae.getCenter())));
            } else if (ce instanceof Connector) {
                Connector c = (Connector) ce;
                connectors.remove(c);
                if (null != c.getHeadEntity() && null != c.getTailEntity() && !list.contains(c.getHeadEntity())
                        && !list.contains(c.getTailEntity())) { //no need to report about disconnection if one of the entities is being deleted too
                    try {
                        String placeId;
                        String transitionId;
                        DisconnectionEvent.DisconnectionDetails.ConnectionType placeType;
                        String transitionIOName = "";

                        if (c.getHeadEntity().getType() == EntityType.PLACE) {
                            placeId = c.getHeadEntity().getId();
                            transitionId = c.getTailEntity().getId();
                            placeType = DisconnectionEvent.DisconnectionDetails.ConnectionType.OUTPUT;
                            transitionIOName = c.getOutputName();
                        } else {
                            placeId = c.getTailEntity().getId();
                            transitionId = c.getHeadEntity().getId();
                            placeType = DisconnectionEvent.DisconnectionDetails.ConnectionType.INPUT;
                            transitionIOName = c.getInputName();
                        }

                        disconnectedEntities.add(new DisconnectionEvent.DisconnectionDetails(
                                placeId, transitionId, placeType, transitionIOName));
                    } catch (Exception e) {
                    }
                }
            }
            ce.delete(canvas);
        }

        if (!deletedEntities.isEmpty()) {
            eventHandler.entitiesDeleted(new DeletionEvent(deletedEntities), disconnectedEntities.isEmpty());
        }
        if (!disconnectedEntities.isEmpty()) {
            eventHandler.entitiesDisconnected(new DisconnectionEvent(disconnectedEntities), true);
        }
    }

    public void mark(String id) {
        ActiveEntity entity = entities.get(id);
        if (null != entity && entity instanceof Markable) {
            ((Markable) entity).mark();
        }
    }

    private void registerEntity(final ActiveEntity entity) {
        registerEntity(entity, false);
    }
    
    private void requestRepaintAll() {
        for (ActiveEntity entity : entities.values()) {
            entity.repaint();
        }
        for (Connector connector : connectors) {
            connector.repaint();
        }
        if (null != currentConnector) {
            currentConnector.repaint();
        }
        if (null != newbornShape) {
            newbornShape.repaint();
        }

        panel.setStyleName(Grid.getGridStyleName());        
    }

    public void scale(double scale) {
        if (scale < SCALE_MIN || scale > SCALE_MAX) {
            return;
        }

        Scaler.setScale(scale / 100);
        this.scale = Scaler.getScale();
        Grid.setStep(Scaler.scale(Grid.DEFAULT_GRID_STEP));

        //updating height & width
        setHeight(baseHeight);
        setWidth(baseWidth);

        requestRepaintAll();
    }

    public void setHeight(int height) {
        baseHeight = height;
        int scaledHeight = Scaler.scale(baseHeight);
        canvas.setHeight(scaledHeight);
        panel.setHeight(scaledHeight + "px");
    }

    public void setWidth(int width) {
        baseWidth = width;
        int scaledWidth = Scaler.scale(baseWidth);
        canvas.setWidth(scaledWidth);
        panel.setWidth(scaledWidth + "px");
    }

    /**
     * Selects specified entities. All previously made selection will be cleared.
     *
     * @param ids identifiers of the entities to be selected.
     */
    public void select(String... ids) {
        deselectAll();
        for (String id : ids) {
            CanvasEntity ce = entities.get(id);
            if (null != ce) {
                select(ce);
            }
        }
    }

    private void deselectAll() {
        for (Selectable entity : selectedEntities) {
            entity.setSelected(false);
        }
        selectedEntities.clear();
    }

    private void selectAll(int x1, int y1, int x2, int y2) {
        for (ActiveEntity entity : entities.values()) {
            if (entity.isContainedIn(x1, y1, x2, y2)) {
                select(entity);
            }
        }

        for (Connector entity : connectors) {
            if (entity.isContainedIn(x1, y1, x2, y2)) {
                select(entity);
            }
        }
    }

    private void select(CanvasEntity entity) {
        entity.setSelected(true);
        selectedEntities.add(entity);
    }

    public void setGridAdjustEnabled(boolean enabled) {
        Grid.setAdjustEnabled(enabled);
    }

    public void setGridPainted(boolean painted) {
        Grid.setPainted(painted);
        panel.setStyleName(Grid.getGridStyleName());
    }

    public void setPathCorrectionEnabled(boolean enabled) {
        if (null != currentConnector) {
            removeConnector(currentConnector);
            currentConnector = null;
        }

        pathCorrector.setEnabled(enabled);
    }

    private void removeConnector(Connector connector) {
        connector.disconnect();
        canvas.remove(connector.getVectorObject());
    }

    public void toggleCreationMode(EntityType entityType, String entityId) {
        this.entityTypeRepresentation = entityType.serialize();
        this.entityId = entityId;
        setDrawingMode(DrawingMode.CREATE);

        if (null != newbornShape) {
            canvas.remove(newbornShape.getVectorObject());
        }

        newbornShape = getEntityType().createEntity(entityId, -1000, -1000);

        if (previewDrawn) {
            canvas.add(newbornShape.getVectorObject());
        }
    }

    public void setDrawingMode(DrawingMode drawingMode) {
        for (JessCanvasListener listener : listeners) {
            listener.modeChanged(this.drawingMode, drawingMode);
        }

        this.drawingMode = drawingMode != null ? drawingMode : DrawingMode.SELECT;
    }

    public void unmark(String id) {
        ActiveEntity entity = entities.get(id);
        if (null != entity && entity instanceof Markable) {
            ((Markable) entity).unmark();
        }
    }

    public void updateName(String oldName, String newName) {
        ActiveEntity entity = entities.remove(oldName);
        if (null != entity) {
            entity.setId(newName);
            entities.put(newName, entity);
        }
    }

// -------------------------- INNER CLASSES --------------------------
    /**
     * Listener which receives JessCanvas inner events
     */
    private interface JessCanvasListener {

        /**
         * Notifies the listener about drawing mode toggling. Listener must clean all the stuff left from the previous mode.
         *
         * @param previousMode mode that was active
         * @param currentMode  mode that is toggled to be acrive
         */
        void modeChanged(DrawingMode previousMode, DrawingMode currentMode);
    }

    /**
     * Class that handles all meaningful events which can happen on the canvas.
     */
    private class CanvasHandler implements ClickHandler, MouseMoveHandler, MouseDownHandler, MouseUpHandler,
            KeyDownHandler, JessCanvasListener, MouseOverHandler, KeyUpHandler {

        private SelectionArea selectionArea;

        public void onClick(ClickEvent event) {
            switch (drawingMode) {
                case SELECT:
                    select();   //user can insensibly release mouse button somewhere out of the canvas borders.  if so, selection is performed by click instead of mouse up.
                    break;
                case CREATE:
                    if (null != entityId) {
                        int x = Grid.adjust(event.getX() + newbornShape.getWidth()/2);
                        int y = Grid.adjust(event.getY() + newbornShape.getHeight()/2);
                        if (tryResize(x, y)) {
                            fireResizeEvent();
                        }
                        if (!previewDrawn) {
                            canvas.add(newbornShape.getVectorObject());
                        }
                        registerEntity(newbornShape);
                        newbornShape = null;
                        entityId = null;
                    }
                    break;
                case CONNECT:
                    if (null != currentConnector) {
                        currentConnector.fix(Grid.adjust(new Coordinates(event.getX(), event.getY())));
                    }
                    break;
                case MOVE:
                    if (null != movingShape) {
                        movingShape.fix();
                        eventHandler.entityMoved(new MoveEvent(movingShape));
                        movingShape = null;
                    }
                    break;
                default:
                    break;
            }
        }

        public void onMouseMove(MouseMoveEvent event) {
            switch (drawingMode) {
                case CREATE:
                    if (null != newbornShape) {
                        newbornShape.move(Grid.adjust(new Coordinates(event.getX(), event.getY())));
                    }
                    break;
                case MOVE:
                    if (null != movingShape) {
                        movingShape.move(Grid.adjust(new Coordinates(event.getX(), event.getY())));
                        if (tryResize(movingShape.getTopLeft().x + movingShape.getWidth(), 
                            movingShape.getTopLeft().y + movingShape.getHeight())) {
                            fireResizeEvent();
                        }
                    }
                    break;
                case CONNECT:
                    if (null != currentConnector && null == currentConnector.getHeadEntity()) {
                        Coordinates coords = Grid.isAdjustEnabled() ? 
                                Grid.adjust(new Coordinates(event.getX(), event.getY())) : 
                                new Coordinates(event.getX() - 1, event.getY() - 1);
                        currentConnector.drawPathTo(coords);
                    }
                    break;
                case SELECT:
                    if (null != selectionArea) {
                        selectionArea.move(event.getX(), event.getY());
                    }
                    break;
            }
        }

        public void onMouseDown(MouseDownEvent event) {
            if (DrawingMode.SELECT == drawingMode && null == selectionArea) {
                selectionArea = new SelectionArea(event.getX(), event.getY());
                canvas.add(selectionArea.getVectorObject());
            }
        }

        public void onMouseUp(MouseUpEvent mouseUpEvent) {
            select();
        }

        public void modeChanged(DrawingMode previousMode, DrawingMode currentMode) {
            if (null != selectionArea) {
                canvas.remove(selectionArea.getVectorObject());
                selectionArea = null;
            }

            if (DrawingMode.CREATE == currentMode && (null == getEntityType() || null == entityId)) {
                throw new IllegalArgumentException(
                        "You have to initialize entity type and id to enable creation mode, use #toggleCreationMode(EntityType, String)");
            }

            if (DrawingMode.CONNECT == currentMode && currentMode != previousMode) {
                reenable();
            }

            if (DrawingMode.CONNECT != currentMode && DrawingMode.CONNECT == previousMode) {
                for (ActiveEntity entity : entities.values()) {
                    if (!entity.isEnabled()) {
                        entity.setEnabled(true);
                    }
                }
            }

            if (DrawingMode.SELECT != currentMode && DrawingMode.SELECT == previousMode) {
                deselectAll();
            }

            if (DrawingMode.MOVE != currentMode) {
                movingShape = null;
            }

            if (DrawingMode.CREATE != currentMode && null != newbornShape) {
                canvas.remove(newbornShape.getVectorObject());
                newbornShape = null;
            }


            if (DrawingMode.CONNECT != currentMode && null != currentConnector) {
                removeConnector(currentConnector);
                currentConnector = null;
            }
        }

        private void select() {
            if (null != selectionArea) {
                deselectAll();
                selectAll(selectionArea.getX1(), selectionArea.getY1(), selectionArea.getX2(), selectionArea.getY2());
                canvas.remove(selectionArea.getVectorObject());
                selectionArea = null;
            }
        }

        public void onKeyDown(KeyDownEvent event) {
            switch (drawingMode) {
                case SELECT:
                    if (event.getNativeKeyCode() == KeyCodes.KEY_DELETE) {
                        deleteSelected();
                    }
                    break;
                case CONNECT:
                    if (event.getNativeKeyCode() == KeyCodes.KEY_ESCAPE && null != currentConnector) {
                        removeConnector(currentConnector);
                        currentConnector = null;
                        reenable();
                    }
                case MOVE:
                    if (event.getNativeKeyCode() == KeyCodes.KEY_CTRL) {
                        pathCorrector.toggle();
                    }
                    break;
            }
        }

        private void deleteSelected() {
            delete(selectedEntities);
            selectedEntities.clear();
        }

        public void onMouseOver(MouseOverEvent mouseOverEvent) {
        }

        public void onKeyUp(KeyUpEvent event) {
            switch (drawingMode) {
                case CONNECT:
                case MOVE:
                    if (event.getNativeKeyCode() == KeyCodes.KEY_CTRL) {
                        pathCorrector.untoggle();
                    }
                    break;
            }
        }
    }

    private class EntityHandler implements ClickHandler, DoubleClickHandler, MouseOverHandler, MouseOutHandler {

        private ActiveEntity entity;

        private EntityHandler(ActiveEntity entity) {
            this.entity = entity;
        }

        public void onMouseOut(MouseOutEvent mouseOutEvent) {
            if (entity.isEnabled() && null != currentConnector && entity == currentConnector.getHeadEntity()) {
                entity.disconnect(currentConnector);
            }
        }

        public void onMouseOver(MouseOverEvent mouseOverEvent) {
            if (entity.isEnabled() && null != currentConnector && entity != currentConnector.getTailEntity()) {
                int y = mouseOverEvent.getNativeEvent().getClientY()- canvas.getAbsoluteTop() + Document.get().getScrollTop();
                int x = mouseOverEvent.getNativeEvent().getClientX()- canvas.getAbsoluteLeft() + Document.get().getScrollLeft();

                entity.connectInput(currentConnector, new Coordinates(x, y));
            }
        }

        public void onDoubleClick(DoubleClickEvent doubleClickEvent) {
            switch (drawingMode) {
                case MOVE:
                    movingShape = entity;
                    break;
                case SELECT:
                    eventHandler.entityDoubleClicked(new EntityEvent(entity));
                    break;
            }
        }

        public void onClick(ClickEvent event) {
            if (!entity.isEnabled()) {
                return;
            }

            switch (drawingMode) {
                case CONNECT:
                    if (null == currentConnector) {
                        int y = event.getNativeEvent().getClientY()- canvas.getAbsoluteTop() + Document.get().getScrollTop();
                        int x = event.getNativeEvent().getClientX()- canvas.getAbsoluteLeft() + Document.get().getScrollLeft();
                        currentConnector = entity.connectOutput(new Coordinates(x, y));
                        currentConnector.addClickHandler(new Selector(currentConnector));
                        canvas.add(currentConnector.getVectorObject());
                        for (ActiveEntity e : entities.values()) {
                            if (e != entity) {
                                e.setEnabled(e.acceptsConnection(entity));
                            }
                        }
                    } else {
                        if (entity == currentConnector.getHeadEntity()) {
                            entity.fix();
                            eventHandler.entitiesConnected(new ConnectionEvent(currentConnector));
                            connectors.add(currentConnector);
                            currentConnector = null;
                            reenable();
                        }
                    }
                    event.stopPropagation();
                    break;
                case SELECT:
                    deselectAll();
                    select(entity);
                    event.stopPropagation();
                    break;
            }
        }
    }

    private class Marker implements ClickHandler {

        private ActiveEntity entity;

        private Marker(ActiveEntity entity) {
            if (!(entity instanceof Markable)) {
                throw new IllegalArgumentException("Marker supports only Markable implementers!");
            }

            this.entity = entity;
        }

        public void onClick(ClickEvent event) {
            if (DrawingMode.MARK == drawingMode) {
                Markable markable = (Markable) entity;
                if (markable.isMarked()) {
                    markable.unmark();
                } else {
                    markable.mark();
                }
                eventHandler.positionStateChanged(entity.getId());
            }
        }
    }

    private class Selector implements ClickHandler {

        private CanvasEntity entity;

        private Selector(CanvasEntity entity) {
            this.entity = entity;
        }

        public void onClick(ClickEvent event) {
            if (DrawingMode.SELECT == drawingMode) {
                deselectAll();
                select(entity);
            }
        }
    }
}
