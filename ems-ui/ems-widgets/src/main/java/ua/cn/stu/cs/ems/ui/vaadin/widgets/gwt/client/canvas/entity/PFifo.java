package ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity;

import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.drawer.TransitionDrawer;

/**
 * @author n0weak
 */
class PFifo extends TTransition {

    PFifo(EntityType type, String id, TransitionDrawer drawer) {
        super(type, id, drawer);
    }
}
