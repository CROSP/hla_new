package ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.graphics;

import org.vaadin.gwtgraphics.client.impl.util.SVGUtil;

/**
 * @author n0weak
 */
public class CustomSVGImpl {

    public void setStrokeWidth(com.google.gwt.user.client.Element element, double width) {
        SVGUtil.setAttributeNS(element, "stroke-width", String.valueOf(width));
    }
}
