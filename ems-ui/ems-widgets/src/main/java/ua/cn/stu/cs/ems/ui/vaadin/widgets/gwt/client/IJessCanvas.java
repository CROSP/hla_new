package ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client;

import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.ActiveEntity;

import java.util.Collection;

/**
 * @author n0weak
 */
public interface IJessCanvas {

    int getWidth();

    int getHeight();

    Collection<ActiveEntity> getEntitiesList();
}
