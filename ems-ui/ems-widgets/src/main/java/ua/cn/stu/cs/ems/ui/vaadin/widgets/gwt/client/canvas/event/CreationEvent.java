package ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.event;

import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.Grid;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.ActiveEntity;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.Scaler;

import java.util.Queue;

/**
 * @author n0weak
 */
public class CreationEvent implements Stringalizable {
// ------------------------------ FIELDS ------------------------------

    private final EntityDetails entityDetails;

// -------------------------- STATIC METHODS --------------------------
    /**
     * Constructs entity from its serialized representation.
     *
     * @param serializedString serialized representation of the entity.
     * @return deserialized entity if process was successful.
     * @throws IllegalArgumentException if deserialization failed
     * @see #serialize()
     */
    public static CreationEvent deserialize(String serializedString) {
        try {
            return deserialize(EventUtils.tokenize(serializedString));
        } catch (Exception e) {
            throw new IllegalArgumentException("Cannot deserialize CreationEvent from supplied data: "
                                               + serializedString, e);
        }
    }

    static CreationEvent deserialize(Queue<String> tokens) {
        return new CreationEvent(EntityDetails.deserialize(tokens));
    }

// --------------------------- CONSTRUCTORS ---------------------------
    public CreationEvent(EntityDetails entityDetails) {
        this.entityDetails = entityDetails;
        this.entityDetails.getEntityType().setEntityConnectorsCount(
                entityDetails.getEntityType().getCountInputs(), entityDetails.getEntityType().getCountOutputs());
        this.entityDetails.getEntityType().setEntityConnectorsNames(
                entityDetails.getEntityType().getInputNames(), entityDetails.getEntityType().getOutputNames());
    }

// --------------------- GETTER / SETTER METHODS ---------------------
    public EntityDetails getEntityDetails() {
        return entityDetails;
    }

// ------------------------ INTERFACE METHODS ------------------------
// --------------------- Interface Stringalizable ---------------------
    public String serialize() {
        return entityDetails.serialize();
    }

// -------------------------- OTHER METHODS --------------------------
    public ActiveEntity create() {
        ActiveEntity entity = entityDetails.getEntityType().createEntity(entityDetails.getId(), 
                Grid.adjust(Scaler.scale(entityDetails.getCenterCoords().x)), 
                Grid.adjust(Scaler.scale(entityDetails.getCenterCoords().y)));
        return entity;
    }
}
