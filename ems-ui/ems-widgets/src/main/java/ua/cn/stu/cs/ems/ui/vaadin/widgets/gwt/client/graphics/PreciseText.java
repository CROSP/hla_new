package ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.graphics;

import com.google.gwt.core.client.GWT;
import org.vaadin.gwtgraphics.client.shape.Text;

/**
 * @author n0weak
 */
public class PreciseText extends Text {

    private static CustomSVGImpl impl = GWT.create(CustomSVGImpl.class);

    public PreciseText(int x, int y, String text) {
        super(x, y, text);
    }

    public void setStrokeWidth(double width) {
        impl.setStrokeWidth(getElement(), width);
    }
}
