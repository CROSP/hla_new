package ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.event;

/**
 * Contract to be implemented by classes that are used as data objects during Vaadin communications
 * and are convertible into/from string representation.
 * <p/>
 * Serialization is made with {@link #serialize()} call.
 * <p/>
 * Each class must provide its own method for deserialization.
 *
 * @author n0weak
 */
public interface Stringalizable {

    /**
     * String that must be used as a delimiter between serializing attributes
     */
    static final String DELIM = "\n";

    /**
     * Serializes this entity to the string representation, using following formula: field1.serialize() + {@link #DELIM} + field2.serialize() + {@link #DELIM} + .. [+ {@link #DELIM}] ([] means that it is optional).
     * serialize() method of inner fields must act according to this formula. Non-complex inner fields should be be serialized with {@link Object#toString()}  call.
     *
     * @return serialized representation of this entity
     */
    public String serialize();
}
