package ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity;

import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.drawer.AggregateDrawer;

/**
 * @author n0weak
 */
class Aggregate extends AbstractEntity<AggregateDrawer> implements ActiveEntity {
    
    Aggregate(EntityType type, String id, AggregateDrawer drawer) {
        super(type, id, drawer);
    }
    
    public boolean acceptsConnection(ActiveEntity entity) {
        AggregateDrawer ad = (AggregateDrawer) getDrawer();
        return ad.getInputSticks().size() > getConnectorsCount(false) && 
                (EntityType.PLACE == entity.getType() || 
                EntityType.AGGREGATE == entity.getType());
    }

    public boolean acceptsConnection() {
        AggregateDrawer ad = (AggregateDrawer) getDrawer();
        return ad.getOutputSticks().size() > outputConnectors.size();
    }
}
