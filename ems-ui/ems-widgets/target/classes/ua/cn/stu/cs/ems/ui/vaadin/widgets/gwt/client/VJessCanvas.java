/* 
 * Copyright 2010 IT Mill Ltd.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client;

import com.vaadin.terminal.gwt.client.ApplicationConnection;
import com.vaadin.terminal.gwt.client.Paintable;
import com.vaadin.terminal.gwt.client.UIDL;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.CanvasEventHandler;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.DrawingMode;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.EntityType;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.PathCorrector;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.event.*;

public class VJessCanvas extends GwtJessCanvas implements Paintable, CanvasEventHandler {

    public static final String READ_ONLY = "readOnly";
    public static final String ENTITY_ID = "entityId";
    public static final String ENTITY_TYPE = "entityType";
    public static final String DRAWING_MODE = "drawingMode";
    public static final String SELECTED_ENTITIES = "selectedEntities";
    public static final String DELETED_ENTITIES = "deletedEntities";
    public static final String GRID_ADJUST_ENABLED = "gridAdjustEnabled";
    public static final String GRID_PAINTED = "gridPainted";
    public static final String MARKED_ENTITIES = "markedEntities";
    public static final String UNMARKED_ENTITIES = "unmarkedEntities";
    public static final String SCALE = "scale";
    public static final String HEIGHT = "height";
    public static final String WIDTH = "width";
    public static final String PATH_CORRECTION_ENABLED = "pathCorrectionEnabled";
    public static final String PREVIEW_DRAWN = "previewDrawn";
    public static final String CREATION_EVENT = "creationEvent";
    public static final String CONNECTION_EVENT = "connectionEvent";
    public static final String POSITION_STATE_EVENT = "positionStateChangedEvent";
    public static final String DELETION_EVENT = "deletionEvent";
    public static final String DISCONNECTION_EVENT = "disconnectionEvent";
    public static final String DOUBLE_CLICK_EVENT = "doubleClickEvent";
    public static final String MOVE_EVENT = "moveEvent";
    public static final String RESIZE_EVENT = "resizeEvent";
    public static final String HORIZ_LINES_DRAWN_FIRST = "drawHorizFirst";
    public static final String UPDATED_NAMES = "updatedNames";
    public static final String CREATED_ENTITIES = "createdEntities";
    public static final String CONNECTED_ENTITIES = "connectedEntities";
    public static final String CLEAR_REQUEST = "clearRequest";
    public static final String DEBUG = "debugRequest";
    /**
     * Component identifier in UIDL communications.
     */
    private String uidlId;
    /**
     * Reference to the server connection object.
     */
    private ApplicationConnection client;

    public VJessCanvas() {
        setEventHandler(this);
    }

    /**
     * This method must be implemented to update the client-side component from
     * UIDL data received from server.
     * <p/>
     * This method is called when the page is loaded for the first time, and
     * every time UI changes in the component are received from the server.
     */
    public void updateFromUIDL(UIDL uidl, ApplicationConnection client) {
        // This call should be made first. Ensure correct implementation,
        // and let the containing layout manage caption, etc.
        if (client.updateComponent(this, uidl, true)) {
            return;
        }

        // Save reference to server connection object to be able to send
        // user interaction later
        this.client = client;

        // Save the UIDL identifier for the component
        uidlId = uidl.getId();

        if (uidl.hasVariable(READ_ONLY)) {
            setReadOnly(uidl.getBooleanVariable(READ_ONLY));
        }

        if (!isReadOnly()) {
            if (uidl.hasVariable(CLEAR_REQUEST)) {
                clear();
            }

            //DrawingMode.Create, entityId & entityType must be updated only via toggleCreationMode(args) method
            if (uidl.hasVariable(DRAWING_MODE)) {
                DrawingMode drawingMode = DrawingMode.valueOf(uidl.getStringVariable(DRAWING_MODE));

                if (DrawingMode.CREATE == drawingMode) {
                    updateCreationMode(uidl);
                }
                else {
                    setDrawingMode(drawingMode);
                }

            }
            else {
                if ((uidl.hasVariable(ENTITY_ID) || uidl.hasVariable(ENTITY_TYPE)) 
                        && (DrawingMode.CREATE == getDrawingMode())) {
                    updateCreationMode(uidl);
                }
            }

            if (uidl.hasVariable(SELECTED_ENTITIES)) {
                select(uidl.getStringArrayVariable(SELECTED_ENTITIES));
            }

            if (uidl.hasVariable(DELETED_ENTITIES)) {
                delete(uidl.getStringArrayVariable(DELETED_ENTITIES));
            }

            if (uidl.hasVariable(UPDATED_NAMES)) {
                String[] data = uidl.getStringArrayVariable(UPDATED_NAMES);
                if (data.length % 2 == 0) { //if length is not even then data is corrupted
                    for (int i = 0; i < data.length - 1; i += 2) {
                        updateName(data[i], data[i + 1]);
                    }
                }
            }

            if (uidl.hasVariable(CREATED_ENTITIES)) {
                String[] events = uidl.getStringArrayVariable(CREATED_ENTITIES);
                for (String event : events) {
                    create(CreationEvent.deserialize(event));
                }
            }

            if (uidl.hasVariable(CONNECTED_ENTITIES)) {
                String[] events = uidl.getStringArrayVariable(CONNECTED_ENTITIES);
                for (String event : events) {
                    connect(ConnectionEvent.deserialize(event));
                }
            }
        }

        if (uidl.hasVariable(WIDTH)) {
            setWidth(uidl.getIntVariable(WIDTH));
        }
        if (uidl.hasVariable(HEIGHT)) {
            setHeight(uidl.getIntVariable(HEIGHT));
        }

        if (uidl.hasVariable(SCALE)) {
            scale(uidl.getDoubleVariable(SCALE));
        }


        if (uidl.hasVariable(GRID_PAINTED)) {
            setGridPainted(uidl.getBooleanVariable(GRID_PAINTED));
        }

        if (uidl.hasVariable(GRID_ADJUST_ENABLED)) {
            setGridAdjustEnabled(uidl.getBooleanVariable(GRID_ADJUST_ENABLED));
        }

        if (uidl.hasVariable(PATH_CORRECTION_ENABLED)) {
            setPathCorrectionEnabled(uidl.getBooleanVariable(PATH_CORRECTION_ENABLED));
        }

        if (uidl.hasVariable(HORIZ_LINES_DRAWN_FIRST)) {
            PathCorrector.setHorizontalFirst(uidl.getBooleanVariable(HORIZ_LINES_DRAWN_FIRST));
        }

        if (uidl.hasVariable(UNMARKED_ENTITIES)) {
            String[] entities = uidl.getStringArrayVariable(UNMARKED_ENTITIES);
            for (String entity : entities) {
                unmark(entity);
            }
        }

        if (uidl.hasVariable(MARKED_ENTITIES)) {
            String[] entities = uidl.getStringArrayVariable(MARKED_ENTITIES);
            for (String entity : entities) {
                mark(entity);
            }
        }

        if (uidl.hasVariable(PREVIEW_DRAWN)) {
            setPreviewDrawn(uidl.getBooleanVariable(PREVIEW_DRAWN));
        }
    }

    private void updateCreationMode(UIDL uidl) {
        String serialized = uidl.hasVariable(ENTITY_TYPE) ? uidl.getStringVariable(ENTITY_TYPE) : null;
        
        EntityType entityType = serialized != null ? EntityType.deserialize(serialized) : getEntityType();
        String entityId = uidl.hasVariable(ENTITY_ID) ? uidl.getStringVariable(ENTITY_ID) : getEntityId();
        
        if (null != entityType && null != entityId) {
            toggleCreationMode(entityType, entityId);
        }
    }

    @Override
    public void setHeight(int height) {
        super.setHeight(height);
        client.updateVariable(uidlId, HEIGHT, getHeight(), true);
    }

    @Override
    public void setWidth(int width) {
        super.setWidth(width);
        client.updateVariable(uidlId, WIDTH, getWidth(), true);
    }

    public void entityCreated(CreationEvent event) {
        client.updateVariable(uidlId, CREATION_EVENT, event.serialize(), true);
    }

    public void entitiesConnected(ConnectionEvent event) {
        client.updateVariable(uidlId, CONNECTION_EVENT, event.serialize(), true);
    }

    public void positionStateChanged(String positionId) {
        client.updateVariable(uidlId, POSITION_STATE_EVENT, positionId, true);
    }

    public void entitiesDeleted(DeletionEvent event, boolean immediate) {
        client.updateVariable(uidlId, DELETION_EVENT, event.serialize(), immediate);
    }

    public void entitiesDisconnected(DisconnectionEvent event, boolean immediate) {
        client.updateVariable(uidlId, DISCONNECTION_EVENT, event.serialize(), immediate);
    }

    public void entityDoubleClicked(EntityEvent event) {
        client.updateVariable(uidlId, DOUBLE_CLICK_EVENT, event.serialize(), true);
    }

    public void entityMoved(MoveEvent event) {
        client.updateVariable(uidlId, MOVE_EVENT, event.serialize(), true);
    }

    public void canvasResized(ResizeEvent event) {
        client.updateVariable(uidlId, RESIZE_EVENT, event.serialize(), true);
    }
    
    public void debug(String msg) {
        client.updateVariable(uidlId, DEBUG, msg, true);        
    }
}
