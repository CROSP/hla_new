package ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;

/**
 * @author n0weak
 */
public class GwtCloseInterceptor extends Composite {

    private String text = "";

    public GwtCloseInterceptor() {
        inject(this);
        initWidget(new HorizontalPanel());
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        if (text != null) {
            this.text = text;
        }
    }

    public native void inject(GwtCloseInterceptor ci) /*-{
    window.onbeforeunload = function() {
    return ci.@ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.GwtCloseInterceptor::getText()();
    };
    window.onunload = function() {
    return ci.@ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.GwtCloseInterceptor::getText()();
    };
    }-*/;
}
