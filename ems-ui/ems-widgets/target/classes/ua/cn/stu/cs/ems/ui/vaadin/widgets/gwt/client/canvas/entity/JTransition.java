package ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity;

import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.drawer.TransitionDrawer;

/**
 * @author n0weak
 */
class JTransition extends AbstractTransition {
// --------------------------- CONSTRUCTORS ---------------------------

    JTransition(EntityType type, String id, TransitionDrawer drawer) {
        super(type, id, drawer);
    }

// ------------------------ INTERFACE METHODS ------------------------
// --------------------- Interface ActiveEntity ---------------------

    public boolean acceptsConnection(ActiveEntity entity) {
        return EntityType.PLACE == entity.getType() &&
                getDrawer().getSticksCount(false) > getConnectorsCount(false);
    }

    public boolean acceptsConnection() {
        return outputConnectors.isEmpty();
    }
}
