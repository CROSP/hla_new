package ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.vaadin.terminal.gwt.client.ValueMap;
import com.vaadin.terminal.gwt.client.ui.VOverlay;

/**
 * Menu is a visible item of ContextMenu component. For every child menu new
 * instance of Menu class will be instantiated.
 * 
 * @author Peter Lehto / IT Mill Oy Ltd
 */
class VMenu extends VOverlay {

    public static final String CLASSNAME = "v-ctxmenu";
    private final VContextMenu app;
    private VMenu parent;
    private int items;
    private final FlowPanel root;
    private Timer openTimer;
    private boolean immediateOpen;
    private final List<VMenu> children;
    private final int width;

    public VMenu(VContextMenu app, String[] menuIds, String[] hierarchy,
            String[] disabled, ValueMap idsToNames) {
        super(true, false, true);

        this.app = app;

        root = new FlowPanel();
        root.setStyleName(CLASSNAME);

        children = new ArrayList<VMenu>();

        add(root);

        int width = 0;
        boolean hasSubitems = false;
        for (int i = 0; i < menuIds.length; i++) {
            boolean hasSubmenu = false;
            boolean isEnabled = true;

            for (String hierarchyItem : hierarchy) {
                if (hierarchyItem.equals(menuIds[i])) {
                    hasSubmenu = true;
                }
            }

            for (String disabledItem : disabled) {
                if (disabledItem.equals(menuIds[i])) {
                    isEnabled = false;
                }
            }

            items += 1;
            String name = idsToNames.getString(menuIds[i]);
            root.add(new VMenuItem(this, menuIds[i], name, isEnabled, hasSubmenu));
            
            hasSubitems |= hasSubmenu;
            width = name.length() > width ? name.length() : width;
        }
        width = width*8 + 10;
        width += hasSubitems ? 10 : 0 ;
        setWidth(width + "px");
        this.width = width;
    }
    
    public int getWidth() {
        return width;
    }

    public void setParent(VMenu menu) {
        this.parent = menu;
        menu.addChild(this);
    }

    private void addChild(VMenu menu) {
        children.add(menu);
    }

    @Override
    public void hide() {
        super.hide();

        for (VMenu child : children) {
            child.hide();
        }
    }

    public boolean hasParent() {
        return parent != null;
    }

    public VMenu getParentMenu() {
        return parent;
    }

    /**
     * @return number of visible items in this menu
     */
    public int getNumberOfItems() {
        return items;
    }

    /**
     * ContextMenuItem is a clickable label that represents one option in a menu
     * component.
     * 
     * @author Peter Lehto / IT Mill Oy Ltd
     */
    class VMenuItem extends Label {

        private final String key;
        private final boolean enabled;
        private final VMenu owner;
        private final boolean hasSubmenu;

        public VMenuItem(VMenu owner, String key, String name, boolean enabled,
                boolean hasSubmenu) {
            this.owner = owner;
            this.key = key;
            this.enabled = enabled;
            this.hasSubmenu = hasSubmenu;

            sinkEvents(Event.ONCLICK);
            sinkEvents(Event.MOUSEEVENTS);
            setStyleName("ctxmenu-menu-item");

            if (hasSubmenu) {
                addStyleName("ctxmenu-submenu");
            }

            if (!enabled) {
                addStyleName("v-disabled");
            }

            setText(name);
        }

        @Override
        public void onBrowserEvent(Event event) {
            event.preventDefault();

            switch (event.getTypeInt()) {
                case Event.ONCLICK: {
                    if (enabled) {
                        if (openTimer != null) {
                            openTimer.cancel();
                            immediateOpen = false;
                        }
                        app.itemClicked(this);
                    }
                    break;
                }
                case Event.ONMOUSEOVER: {
                    // automatically open submenus after one second of hovering
                    if (enabled && hasSubmenu && immediateOpen == false) {
                        openTimer = new Timer() {

                            @Override
                            public void run() {
                                app.itemClicked(VMenuItem.this);
                                immediateOpen = true;
                            }
                        };
                        openTimer.schedule(1000);
                    }

                    // open other submenus from same menu immediately when hovered
                    // if user has already hovered another item in the same menu
                    if (enabled && immediateOpen && hasSubmenu) {
                        app.itemClicked(this);
                    }

                    break;
                }
                case Event.ONMOUSEOUT: {
                    if (openTimer != null) {
                        openTimer.cancel();
                    }
                    break;
                }
            }
        }

        /**
         * @return id of this menu item
         */
        public String getId() {
            return key;
        }

        /**
         * @return Menu to which this menu item belongs to
         */
        public VMenu getOwnerMenu() {
            return owner;
        }
    }
}