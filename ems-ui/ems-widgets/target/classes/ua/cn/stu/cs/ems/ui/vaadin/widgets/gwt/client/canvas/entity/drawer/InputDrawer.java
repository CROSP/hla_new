package ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.drawer;

import org.vaadin.gwtgraphics.client.shape.Path;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.Coordinates;

import static ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.Scaler.scale;

/**
 * @author n0weak
 */
public class InputDrawer extends AbstractDrawer {

    private Path outStick;
    private Path triangle;

    /**
     * @param id string identifier of this component
     * @param x  x coordinate of the center of the component
     * @param y  y coordinate of the center of the component
     */
    public InputDrawer(String id, int x, int y) {
        super(id, x, y);
    }

    @Override
    public void moveComponent(int x, int y) {
        triangle.setX(x - scale(DEFAULT_WIDTH / 2));
        triangle.setY(y);

        outStick.setX(x + scale(DEFAULT_WIDTH / 2));
        outStick.setY(y + scale(DEFAULT_HEIGHT / 2));
    }

    public Coordinates getCenter() {
        return new Coordinates(triangle.getX() + scale(DEFAULT_WIDTH / 2), triangle.getY() + scale(DEFAULT_HEIGHT / 2));
    }

    public Coordinates getOutputCoordinates(int connectorIdx) {
        return new Coordinates(triangle.getX() + getWidth(), triangle.getY() + scale(DEFAULT_HEIGHT / 2));
    }

    public Coordinates getInputCoordinates(int connectorIdx) {
        throw new UnsupportedOperationException("Input component has no input stick!");
    }

    @Override
    protected void paintComponent(String id, int x, int y) {
        triangle = new Path(x - scale(DEFAULT_WIDTH / 2), y);
        triangle.lineRelativelyTo(0, scale(DEFAULT_HEIGHT));
        triangle.lineRelativelyTo(scale(DEFAULT_WIDTH), -scale(DEFAULT_HEIGHT / 2));
        triangle.close();
        group.add(triangle);

        outStick = new Path(x + scale(DEFAULT_WIDTH / 2), y + scale(DEFAULT_HEIGHT / 2));
        outStick.lineRelativelyTo(scale(STICK_SIZE), 0);
        group.add(outStick);

        super.paintComponent(id, x, y);
    }

    public Coordinates getTopLeft() {
        return new Coordinates(triangle.getX(), triangle.getY());
    }

    public int getWidth() {
        return scale(DEFAULT_WIDTH) + scale(STICK_SIZE);
    }

    public int getSticksCount(boolean output) {
        return output ? 1 : 0;
    }

    @Override
    public int getOutputId(Coordinates mousePos) {
        return 0;
    }

    @Override
    public int getInputId(Coordinates mousePos) {
        return -1;
    }
}
