package ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity;

/**
 * @author n0weak
 */
public interface Markable {

    void mark();

    void unmark();

    boolean isMarked();
}
