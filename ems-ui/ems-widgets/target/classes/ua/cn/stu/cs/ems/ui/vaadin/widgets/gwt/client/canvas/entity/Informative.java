package ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity;

import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.Coordinates;

/**
 * @author n0weak
 */
public interface Informative {

    Coordinates getTopLeft();

    int getWidth();

    int getHeight();

    /**
     * Defines whether entity intersects with the straight horizontal line. Line is defined by two coordinates passed as parameters.
     *
     * @param y  y coordinate of the line
     * @param x1 most-left or most-right x coordinate of the line
     * @param x2 most-left or most-right (different from x1) x coordinate of the line
     * @return true if entity intersects with the line, false if not
     */
    boolean intersectsH(int y, int x1, int x2);

    /**
     * Defines whether entity intersects with the straight vertical line. Line is defined by two coordinates passed as parameters.
     *
     * @param x  x coordinate of the line
     * @param y1 most-top or most-bottom y coordinate of the line
     * @param y2 most-top or most-bottom (different from y1) y coordinate of the line
     * @return true if entity intersects with the line, false if not
     */
    boolean intersectsV(int x, int y1, int y2);

    /**
     * @return unscaled x coordinate of the center of the component
     */
    int getX();

    /**
     * @return unscaled y coordinate of the top of the component
     */
    int getY();

    /**
     * @return scaled coordinates of the center of the entity
     */
    Coordinates getCenter();
}
