package ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.drawer;

import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.Coordinates;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.Drawable;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.Informative;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.Repaintable;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.Selectable;

/**
 * Basic contract for classes that are responsible for the drawing (painting) of {@link ua.cn.stu.cs.jess.ui.vaadin.widgets.gwt.client.canvas.entity.CanvasEntity} entities.
 *
 * @author n0weak
 */
public interface Drawer extends Drawable, Selectable, Repaintable, Informative {

    static final int DEFAULT_HEIGHT = 40;
    static final int DEFAULT_WIDTH = 20;
    static final int DEFAULT_FONT_SIZE = 13;
    static final int STICK_SIZE = 10;
    static final int SELECTIVE_SPACE = 10;
    static final String COLOR_DISABLED = "grey";
    static final String COLOR_DEFAULT = "black";
    static final String COLOR_SELECTED = "blue";
    static final String DEFAULT_FONT = "Arial";

    /**
     * Moves drawn entity to position defined by mouse coordinates specified.
     *
     * @param mousePos coordinates that define cursor position, pointing at the center of the entity.
     */
    void move(Coordinates mousePos);

    /**
     * Toggles enabled/disabled mode for this entity.
     *
     * @param enabled true to enable the entity, false to disable
     */
    void setEnabled(boolean enabled);

    /**
     * Defines coordinates of output position by connector's index.
     *
     * @param connectorIdx index of the connector.
     * @return coordinates of the output position.
     */
    Coordinates getOutputCoordinates(int connectorIdx);

    int getOutputId(Coordinates mousePos);

    /**
     * Defines coordinates of input position by connector's index.
     *
     * @param connectorIdx index of the connector.
     * @return coordinates of the input position.
     */
    Coordinates getInputCoordinates(int connectorIdx);
    int getInputId(Coordinates mousePos);

    /**
     * Updates value on the id label.
     *
     * @param id id to be set
     */
    void setId(String id);
    
    String getInputName(int i);
    String getOutputName(int i);

    int getSticksCount(boolean output);
}
