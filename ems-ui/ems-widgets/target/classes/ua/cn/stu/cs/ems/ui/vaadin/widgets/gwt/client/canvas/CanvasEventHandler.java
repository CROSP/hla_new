package ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas;

import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.event.*;

/**
 * @author n0weak
 */
public interface CanvasEventHandler {

    void entityCreated(CreationEvent event);

    void entitiesConnected(ConnectionEvent event);

    void positionStateChanged(String positionId);

    void entitiesDeleted(DeletionEvent event, boolean immediate);

    void entitiesDisconnected(DisconnectionEvent event, boolean immediate);

    void entityDoubleClicked(EntityEvent event);

    void entityMoved(MoveEvent event);

    void canvasResized(ResizeEvent event);
    
    void debug(String msg);
}
