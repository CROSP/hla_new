package ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity;

import org.vaadin.gwtgraphics.client.VectorObject;

/**
 * Contract for all the entities that can be drown on {@link org.vaadin.gwtgraphics.client.DrawingArea} from GWT Graphics library
 *
 * @author n0weak
 */
public interface Drawable {

    /**
     * @return GWT Graphics' {@link VectorObject} which represents drown entity.
     */
    public VectorObject getVectorObject();
}
