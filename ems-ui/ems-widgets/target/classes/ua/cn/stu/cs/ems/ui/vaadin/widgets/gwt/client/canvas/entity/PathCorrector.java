package ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity;

import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.IJessCanvas;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.Coordinates;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.Grid;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static java.lang.Math.max;
import static java.lang.Math.min;

/**
 * Used to correct connector's path to avoid intersections with entities on the canvas when possible.
 *
 * @author n0weak
 * @version 0.1-alpha
 */
public class PathCorrector {
// ------------------------------ FIELDS ------------------------------

    static final int ZONE_GAP = 400;
    /**
     * If true, connector will draw horizontal line before vertical to reach the destination point.
     */
    static boolean horizontalFirst = false;
    private static final int MAX_ITERATIONS = 20;
    private final IJessCanvas canvas;
    private boolean enabled = false;
    private Boolean toggledValue;

// -------------------------- STATIC METHODS --------------------------
    public static Coordinates getMiddlePoint(Coordinates from, Coordinates to) {
        if (from.x != to.x && from.y != to.y) {
            return horizontalFirst ? new Coordinates(to.x, from.y) : new Coordinates(from.x, to.y);
        }
        else {
            return null;
        }
    }

    public static void setHorizontalFirst(boolean horizontalFirst) {
        PathCorrector.horizontalFirst = horizontalFirst;
    }

    public static boolean isHorizontalFirst() {
        return horizontalFirst;
    }

// --------------------------- CONSTRUCTORS ---------------------------
    public PathCorrector(IJessCanvas canvas) {
        this.canvas = canvas;
    }

// --------------------- GETTER / SETTER METHODS ---------------------
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
        toggledValue = null;
    }

// -------------------------- OTHER METHODS --------------------------
    public List<Coordinates> correct(Coordinates begin, Coordinates end) {
        return correct(begin, end, new ArrayList<ActiveEntity>(0), null);
    }

    /**
     * Removes intersections with existing entities where possible
     *
     * @param begin           first coordinate of the path, must be scaled
     * @param end             last coordinate of the path, must be scaled
     * @param skippedEntities entities that must be excluded from intersections checks
     * @param sourceEntity    entity which is the source of the connection, intersections with its last output stick won't be detected. Parameter is nullable.
     * @return corrected coordinates, scaled
     */
    public List<Coordinates> correct(Coordinates begin, Coordinates end, List<ActiveEntity> skippedEntities,
                                     ActiveEntity sourceEntity) {
        if (!enabled) {
            return new ArrayList<Coordinates>(Arrays.asList(begin, end));
        }

        List<Coordinates> coords = new ArrayList<Coordinates>();

        coords.add(begin);

        List<ActiveEntity> candidates = getCandidates(begin, end, canvas.getEntitiesList(), skippedEntities);


        boolean horizFirst = horizontalFirst;

        List<ActiveEntity> hIntersctors = horizFirst ? getHorizontalIntersectors(candidates, begin.y, begin.x, end.x)
                                          : getHorizontalIntersectors(candidates, end.y, begin.x, end.x);
        List<ActiveEntity> vIntersectors = horizFirst ? getVerticalIntersectors(candidates, end.x, begin.y, end.y)
                                           : getVerticalIntersectors(candidates, begin.x, begin.y, end.y);

        if (!hIntersctors.isEmpty() || !vIntersectors.isEmpty()) {
            //trying to bypass intersection by going the other way
            hIntersctors = !horizFirst ? getHorizontalIntersectors(candidates, begin.y, begin.x, end.x)
                           : getHorizontalIntersectors(candidates, end.y, begin.x, end.x);
            vIntersectors = !horizFirst ? getVerticalIntersectors(candidates, end.x, begin.y, end.y)
                            : getVerticalIntersectors(candidates, begin.x, begin.y, end.y);
            if (hIntersctors.isEmpty() && vIntersectors.isEmpty()) {
                horizFirst = !horizFirst;
            }
        }

        //intersections with the last output stick of the source entity must not be detected
        if (null != sourceEntity && sourceEntity.getOutputCoordinates(sourceEntity.getConnectorsCount(true) - 1).x
                                    == begin.x
            && !sourceEntity.intersectsH(horizFirst ? begin.y : end.y, begin.x + 1, end.x)) {
            candidates.remove(sourceEntity);
        }

        if (horizFirst) {
            correctX(candidates, coords, coords.get(coords.size() - 1), end);
            correctY(candidates, coords, coords.get(coords.size() - 1), end);
        }
        else {
            correctY(candidates, coords, coords.get(coords.size() - 1), end);
            correctX(candidates, coords, coords.get(coords.size() - 1), end);
        }

        if (!coords.get(coords.size() - 1).equals(end)) {
            coords.add(end);
        }

        return coords;
    }

    List<ActiveEntity> getCandidates(Coordinates begin, Coordinates end, Collection<ActiveEntity> entities,
                                     List<ActiveEntity> skippedEntities) {
        int zoneGap = Scaler.scale(ZONE_GAP);

        int left = Math.min(begin.x, end.x) - zoneGap;
        int right = Math.max(end.x, begin.x) + zoneGap;
        int top = Math.min(begin.y, end.y) - zoneGap;
        int bottom = Math.max(begin.y, end.y) + zoneGap;

        List<ActiveEntity> candidates = new ArrayList<ActiveEntity>();


        for (ActiveEntity entity : entities) {
            //adding entities only from the range defined with ZONE_GAP, begin and end coordinates.
            //also, if user points directly at the entity - then no correction is needed.
            if (entity.isContainedIn(left, top, right, bottom)
                && !(entity.intersectsV(end.x, end.y, end.y) && entity.intersectsH(end.y, end.x, begin.x))) {
                candidates.add(entity);
            }
        }

        candidates.removeAll(skippedEntities);

        return candidates;
    }

    private List<ActiveEntity> getHorizontalIntersectors(List<ActiveEntity> candidates, int y, int x1, int x2) {
        List<ActiveEntity> intersectors = new ArrayList<ActiveEntity>();

        for (ActiveEntity entity : candidates) {
            if (entity.intersectsH(y, x1, x2)) {
                intersectors.add(entity);
            }
        }
        return intersectors;
    }

    private List<ActiveEntity> getVerticalIntersectors(List<ActiveEntity> candidates, int x, int y1, int y2) {
        List<ActiveEntity> intersectors = new ArrayList<ActiveEntity>();

        for (ActiveEntity entity : candidates) {
            if (entity.intersectsV(x, y1, y2)) {
                intersectors.add(entity);
            }
        }
        return intersectors;
    }

    private void correctY(List<ActiveEntity> candidates, List<Coordinates> coords, Coordinates begin, Coordinates end) {
        boolean done = false;
        List<Coordinates> pendingCoords = new ArrayList<Coordinates>();
        if (begin.y != end.y) {
            boolean topDown = begin.y < end.y;
            boolean left = begin.x >= end.x;       //moving closer to the end.x during our corrections

            int i = 0;
            Coordinates last = coords.get(coords.size() - 1);
            do {
                if (i >= MAX_ITERATIONS) {
                    break;
                }
                i++;

                done = correctY(pendingCoords, candidates, last.x, last.y, end.y, topDown, left);
                if (!done) {
                    last = pendingCoords.get(pendingCoords.size() - 1);
                }
            } while (!done);
        }
        if (done) {
            coords.addAll(pendingCoords);
        }
    }

    private void correctX(List<ActiveEntity> candidates, List<Coordinates> coords, Coordinates begin, Coordinates end) {
        boolean done = false;
        List<Coordinates> pendingCoords = new ArrayList<Coordinates>();
        if (begin.x != end.x) {
            boolean leftToRight = begin.x < end.x;
            boolean up = end.y <= begin.y;    //moving closer to the end.y during our corrections

            int i = 0;
            Coordinates last = coords.get(coords.size() - 1);
            do {
                if (i >= MAX_ITERATIONS) {
                    break;
                }
                i++;

                done = correctX(pendingCoords, candidates, last.y, last.x, end.x, leftToRight, up);
                if (!done) {
                    last = pendingCoords.get(pendingCoords.size() - 1);
                }
            } while (!done);
        }

        if (done) {
            coords.addAll(pendingCoords);
        }
    }

    boolean correctX(List<Coordinates> coords, List<ActiveEntity> candidates, int y, int x1, int x2, boolean leftToRight,
                     boolean up) {
        boolean done = false;

        if ((leftToRight && x1 >= x2) || (!leftToRight && x1 <= x2)) {
            done = true;
        }
        else {
            List<ActiveEntity> intersectors = getHorizontalIntersectors(candidates, y, x1, x2);

            if (!intersectors.isEmpty()) {
                ActiveEntity closestIntersector = intersectors.get(0);

                for (int i = 1; i < intersectors.size(); i++) {
                    ActiveEntity intersector = intersectors.get(i);
                    if ((leftToRight && closestIntersector.getTopLeft().x > intersector.getTopLeft().x)
                        || (!leftToRight && closestIntersector.getTopLeft().x < intersector.getTopLeft().x)) {
                        closestIntersector = intersector;
                    }
                }

                Coordinates topLeft = closestIntersector.getTopLeft();

                int newY = up ? topLeft.y - getGap() : topLeft.y + closestIntersector.getHeight() + getGap();
                int newX = leftToRight ? topLeft.x - getGap() : topLeft.x + closestIntersector.getWidth() + getGap();

                if (y > canvas.getHeight() || y < 0) {
                    done = true;
                }
                else {
                    coords.add(new Coordinates(newX, y));
                    coords.add(new Coordinates(newX, newY));
                }
            }
            else {
                coords.add(new Coordinates(x2, y));
                done = true;
            }
        }

        return done;
    }

    int getGap() {
        return Scaler.scale(Grid.getStep());
    }

    boolean correctY(List<Coordinates> coords, List<ActiveEntity> candidates, int x, int y1, int y2, boolean topDown,
                     boolean left) {
        boolean done = false;


        if ((topDown && y1 >= y2) || (!topDown && y1 <= y2)) {
            done = true;
        }
        else {
            List<ActiveEntity> intersectors = getVerticalIntersectors(candidates, x, y1, y2);

            if (!intersectors.isEmpty()) {
                ActiveEntity closestIntersector = intersectors.get(0);

                for (int i = 1; i < intersectors.size(); i++) {
                    ActiveEntity intersector = intersectors.get(i);
                    if ((topDown && closestIntersector.getTopLeft().y > intersector.getTopLeft().y)
                        || (!topDown && closestIntersector.getTopLeft().y < intersector.getTopLeft().y)) {
                        closestIntersector = intersector;
                    }
                }

                Coordinates topLeft = closestIntersector.getTopLeft();

                int newY = topDown ? topLeft.y - getGap() : topLeft.y + closestIntersector.getHeight() + getGap();
                int newX = left ? topLeft.x - getGap() : topLeft.x + closestIntersector.getWidth() + getGap();

                if (x > canvas.getWidth() || x < 0) {
                    done = true;
                }
                else {
                    coords.add(new Coordinates(x, newY));
                    coords.add(new Coordinates(newX, newY));
                }
            }
            else {
                coords.add(new Coordinates(x, y2));
                done = true;
            }
        }

        return done;
    }

    /**
     * Removes cycles from the path specified by the list of coordinates.
     *
     * @param c coordinates of the path to be corrected
     * @return new list, containing corrected coordinates, if corrector is enabled or the same instance of the passed list if corrector is disabled.
     */
    public List<Coordinates> removeCycles(List<Coordinates> c) {
        if (!enabled) {
            return c;
        }
        List<Coordinates> coords = new ArrayList<Coordinates>(c);
        for (int i = 1; i < coords.size() - 1; i++) {
            Coordinates b = coords.get(i);
            Coordinates e = coords.get(i + 1);

            if (e.equals(b)) {
                coords.remove(i);
                i--;
                continue;
            }

            for (int j = 1; j < i; j++) {
                Coordinates prevB = coords.get(j - 1);
                Coordinates prevE = coords.get(j);

                Coordinates intersection = closestIntersection(prevB, prevE, b, e);

                if (null != intersection) {
                    for (int k = j; k < i + 1; k++) {
                        coords.remove(j);
                    }
                    if (!intersection.equals(e)) {
                        coords.add(j, intersection);
                    }
                    i = j;
                    break;
                }
            }
        }
        return coords;
    }

    /**
     * Let A,B,C,D be 2-space position vectors.  Then the directed line
     * segments AB & CD are given by:
     * <p/>
     * AB=A+r(B-A), r in [0,1]
     * CD=C+s(D-C), s in [0,1]
     * <p/>
     * If AB & CD intersect, then
     * <p/>
     * A+r(B-A)=C+s(D-C), or
     * <p/>
     * Ax+r(Bx-Ax)=Cx+s(Dx-Cx)
     * Ay+r(By-Ay)=Cy+s(Dy-Cy)  for some r,s in [0,1]
     * <p/>
     * Solving the above for r and s yields
     * <p/>
     * (Ay-Cy)(Dx-Cx)-(Ax-Cx)(Dy-Cy)
     * r = -----------------------------  (eqn 1)
     * (Bx-Ax)(Dy-Cy)-(By-Ay)(Dx-Cx)
     * <p/>
     * (Ay-Cy)(Bx-Ax)-(Ax-Cx)(By-Ay)
     * s = -----------------------------  (eqn 2)
     * (Bx-Ax)(Dy-Cy)-(By-Ay)(Dx-Cx)
     * <p/>
     * Let P be the position vector of the intersection point, then
     * <p/>
     * P=A+r(B-A) or
     * <p/>
     * Px=Ax+r(Bx-Ax)
     * Py=Ay+r(By-Ay)
     * <p/>
     * By examining the values of r & s, you can also determine some
     * other limiting conditions:
     * <p/>
     * If 0<=r<=1 & 0<=s<=1, intersection exists
     * r<0 or r>1 or s<0 or s>1 line segments do not intersect
     * <p/>
     * If the denominator in eqn 1 is zero, AB & CD are parallel
     * If the numerator in eqn 1 is also zero, AB & CD are collinear.
     * <p/>
     * If they are collinear, then the segments may be projected to the x-
     * or y-axis, and overlap of the projected intervals checked.
     * <p/>
     * If the intersection point of the 2 lines are needed (lines in this
     * context mean infinite lines) regardless whether the two line
     * segments intersect, then
     * <p/>
     * If r>1, P is located on extension of AB
     * If r<0, P is located on extension of BA
     * If s>1, P is located on extension of CD
     * If s<0, P is located on extension of DC
     * <p/>
     * Algorithm taken from http://www.faqs.org/faqs/graphics/algorithms-faq/
     *
     * @param a A position vector
     * @param b B position vector
     * @param c C position vector
     * @param d D position vector
     * @return coordinates of intersection point, closest to D, or null if segments do not intersect.
     */
    Coordinates closestIntersection(Coordinates a, Coordinates b, Coordinates c, Coordinates d) {
        Coordinates result = null;

        double rNum = (a.y - c.y) * (d.x - c.x) - (a.x - c.x) * (d.y - c.y);
        double rDenum = (b.x - a.x) * (d.y - c.y) - (b.y - a.y) * (d.x - c.x);

        if (0 == rNum || 0 == rDenum) {
            if (0 == rNum && 0 == rDenum) {
                //segments are collinear
                if (isIntersecting(a.y, b.y, c.y, d.y) && isIntersecting(a.x, b.x, c.x, d.x)) {
                    int x = Math.min(b.x, d.x);
                    int y = Math.min(b.y, d.y);
                    result = new Coordinates(x, y);
                }
            }
        }
        else {
            double r = rNum / rDenum;
            double s = ((a.y - c.y) * (b.x - a.x) - (a.x - c.x) * (b.y - a.y)) / (double) ((b.x - a.x) * (d.y - c.y)
                                                                                           - (b.y - a.y) * (d.x - c.x));

            if (0 <= r && r <= 1 && 0 <= s && s <= 1) {
                int x = (int) (a.x + r * (b.x - a.x));
                int y = (int) (a.y + r * (b.y - a.y));
                result = new Coordinates(x, y);
            }
        }

        return result;
    }

    private boolean isIntersecting(int a, int b, int c, int d) {
        int minCD = min(c, d);
        int minAB = min(a, b);
        return (minAB <= minCD && minCD <= max(a, b)) || (minCD < minAB && minAB < max(c, d));
    }

    public void toggle() {
        if (null == toggledValue) {
            toggledValue = enabled;
            enabled = !enabled;
        }
    }

    public void untoggle() {
        if (null != toggledValue) {
            enabled = toggledValue;
            toggledValue = null;
        }
    }
}
