package ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.drawer;

import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.Scaler;
import org.vaadin.gwtgraphics.client.Group;
import org.vaadin.gwtgraphics.client.Shape;
import org.vaadin.gwtgraphics.client.VectorObject;
import org.vaadin.gwtgraphics.client.shape.Rectangle;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.Coordinates;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.Grid;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.graphics.PreciseText;

import static ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.Scaler.scale;
import static ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.Scaler.unscale;

/**
 * @author n0weak
 */
abstract class AbstractDrawer implements Drawer {
    
    protected final static String INPUT_MASK = "I";
    protected final static String OUTPUT_MASK = "O";
// ------------------------------ FIELDS ------------------------------

    protected PreciseText text;
    protected Group group;
    /**
     * Spacer used for better mouse events catching
     */
    protected Rectangle spacer;
    /**
     * unscaled x coordinate of the center of the component
     */
    protected int baseX;
    /**
     * unscaled y coordinate of the top of the component
     */
    protected int baseY;

// --------------------------- CONSTRUCTORS ---------------------------
    /**
     * @param id string identifier of this component
     * @param x  x coordinate of the center of the component
     * @param y  y coordinate of the center of the component
     */
    protected AbstractDrawer(String id, int x, int y) {
        y -= getBaseHeight() / 2;
        baseY = unscale(y);
        baseX = unscale(x);
        y = Grid.adjust(y);
        x = Grid.adjust(x);
        group = new Group();

        paintComponent(id, x, y);
    }

    /**
     * @return scaled height of the component without the text label
     */
    protected int getBaseHeight() {
        return scale(DEFAULT_HEIGHT);
    }

    /**
     * @param id string identifier of this component
     * @param x  scaled and adjusted x coordinate of the center of the component
     * @param y  scaled and adjusted y coordinate of the top of the component
     */
    protected void paintComponent(String id, int x, int y) {
        text = new PreciseText(x, y, id);
        text.setFontFamily(DEFAULT_FONT);
        text.setStrokeWidth(0.5);
        text.setFontSize(scale(DEFAULT_FONT_SIZE));
        text.setStrokeColor("black");
        text.setFillColor("black");
        adjustText();
        group.add(text);

        int componentWidth = getWidth();

        spacer = new Rectangle(x - componentWidth / 2, y, componentWidth, getHeight());
        spacer.setStrokeColor("white");
//        spacer.setStrokeColor("yellow");
        spacer.setFillOpacity(0);
        group.insert(spacer, 0);
    }

    /**
     * Moves component's text to the footer. Uses {@link #getTopLeft()}, {@link #getHeight()}, {@link #getWidth()} to evaluate needed coordinates
     */
    protected void adjustText() {
        text.setY(getTopLeft().y + getHeight());
        text.setX(getCenter().x - text.getTextWidth() / 2);
    }

    public int getHeight() {
        return getBaseHeight() + text.getTextHeight();
    }

// --------------------- GETTER / SETTER METHODS ---------------------
    Group getGroup() {
        return group;
    }

    void setGroup(Group group) {
        this.group = group;
    }

// ------------------------ INTERFACE METHODS ------------------------
// --------------------- Interface Drawable ---------------------
    public VectorObject getVectorObject() {
        return group;
    }

// --------------------- Interface Drawer ---------------------
    public final void move(Coordinates mousePos) {
        int y = mousePos.y - getBaseHeight() / 2;
        baseX = unscale(mousePos.x);
        baseY = unscale(y);
        int x = Grid.adjust(mousePos.x);
        y = Grid.adjust(y);

        spacer.setX(x - getWidth() / 2);
        spacer.setY(y);

        moveComponent(x, y);
        adjustText();
    }

    public void setEnabled(boolean enabled) {
        String color = COLOR_DEFAULT;
        if (!enabled) {
            color = COLOR_DISABLED;
        }
        for (int i = 0; i < group.getVectorObjectCount(); i++) {
            VectorObject vo = group.getVectorObject(i);
            if (vo instanceof Shape) {
                ((Shape) vo).setStrokeColor(color);
            }
        }
        spacer.setStrokeColor("white");
    }

    public void setId(String name) {
        text.setText(name);
        adjustText();
    }

// --------------------- Interface Informative ---------------------
    public boolean intersectsH(int y, int x1, int x2) {
        Coordinates topLeft = getTopLeft();
        int w = getWidth();
        int h = getHeight();

        return y >= topLeft.y && y <= topLeft.y + h && ((x1 < topLeft.x + w && x2 > topLeft.x) || (x2 < topLeft.x + w
                                                                                                   && x1 > topLeft.x));
    }

    public boolean intersectsV(int x, int y1, int y2) {
        Coordinates topLeft = getTopLeft();
        int w = getWidth();
        int h = getHeight();

        return x >= topLeft.x && x <= topLeft.x + w && ((y1 < topLeft.y + h && y2 > topLeft.y) || (y2 < topLeft.y + h
                                                                                                   && y1 > topLeft.y));
    }

    public int getX() {
        return baseX;
    }

    public int getY() {
        return baseY;
    }

// --------------------- Interface Repaintable ---------------------
    public void repaint() {
        group.clear();
        paintComponent(text.getText(), Grid.adjust(scale(baseX)), Grid.adjust(scale(baseY)));
    }

// --------------------- Interface Selectable ---------------------
    public boolean isContainedIn(int x1, int y1, int x2, int y2) {
        int width = getWidth();
        int height = getHeight();
        Coordinates topLeft = getTopLeft();
        int x = topLeft.x;
        int y = topLeft.y;
        return x >= x1 && x + width <= x2 && y >= y1 && y + height <= y2;
    }

    public void setSelected(boolean selected) {
        String color = COLOR_DEFAULT;
        if (selected) {
            color = COLOR_SELECTED;
        }
        for (int i = 0; i < group.getVectorObjectCount(); i++) {
            VectorObject vo = group.getVectorObject(i);
            if (vo instanceof Shape) {
                ((Shape) vo).setStrokeColor(color);
            }
        }
        spacer.setStrokeColor("white");
    }

    public String getInputName(int i) {
        if (i >= 0 || i < getSticksCount(false)) {
            return INPUT_MASK + i;
        }
        return null;
    }

    public String getOutputName(int i) {
        if (i >= 0 || i < getSticksCount(true)) {
            return OUTPUT_MASK + i;
        }
        return null;
    }
    
    public int getOutputId(Coordinates mousePos) {
        for (int i=0;i<getSticksCount(true);i++) {
            int delta = mousePos.y - getOutputCoordinates(i).y;
            delta = delta < 0 ? -delta : delta;
            if (delta < Scaler.scale(SELECTIVE_SPACE)) {
                return i;
            }
        }
        return -1;
    }
    
    public int getInputId(Coordinates mousePos) {
        for (int i=0;i<getSticksCount(false);i++) {
            int delta = mousePos.y - getInputCoordinates(i).y;
            delta = delta < 0 ? -delta : delta;
            if (delta < Scaler.scale(SELECTIVE_SPACE)) {
                return i;
            }
        }
        return -1;
    }

// -------------------------- OTHER METHODS --------------------------
    /**
     * Move the component to specified coordinates
     *
     * @param x scaled and adjusted x coordinate of the center of the component
     * @param y scaled and adjusted y coordinate of the top of the component
     */
    protected abstract void moveComponent(int x, int y);
}
