package ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas;

/**
 * @author n0weak
 */
public class Grid {

    public static final int DEFAULT_GRID_STEP = 20;
    private static final String STYLE_BASENAME = "grid";
    private static boolean painted = true;
    private static boolean adjustEnabled = true;
    private static int step = DEFAULT_GRID_STEP;

    public static void setStep(int step) {
        Grid.step = step;
    }

    public static int getStep() {
        return step;
    }

    public static boolean isPainted() {
        return painted;
    }

    public static void setPainted(boolean painted) {
        Grid.painted = painted;
    }

    public static boolean isAdjustEnabled() {
        return adjustEnabled;
    }

    public static void setAdjustEnabled(boolean adjustEnabled) {
        Grid.adjustEnabled = adjustEnabled;
    }

    public static int adjust(int coord) {
        if (adjustEnabled) {
            int remainder = coord % step;
            return remainder > step / 2 ? coord + step - remainder : coord - remainder;
        }
        else {
            return coord;
        }
    }

    public static Coordinates adjust(Coordinates coords) {
        return adjustEnabled ? new Coordinates(adjust(coords.x), adjust(coords.y)) : coords;
    }

    public static String getGridStyleName() {
        return isPainted() ? (STYLE_BASENAME + step) : "";
    }

    private Grid() {
    }
}
