package ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.event;

import java.util.Queue;

/**
 * @author n0weak
 */
public class ResizeEvent implements Stringalizable {
// ------------------------------ FIELDS ------------------------------

    /**
     * Scaled width
     */
    private final int width;
    /**
     * Scaled height
     */
    private final int height;
    /**
     * Unscaled height
     */
    private final int baseHeight;
    /**
     * Unscaled width
     */
    private final int baseWidth;

// -------------------------- STATIC METHODS --------------------------
    /**
     * Constructs entity from its serialized representation.
     *
     * @param serializedString serialized representation of the entity.
     * @return deserialized entity if process was successful.
     * @throws IllegalArgumentException if deserialization failed
     * @see #serialize()
     */
    public static ResizeEvent deserialize(String serializedString) {
        try {
            return deserialize(EventUtils.tokenize(serializedString));
        } catch (Exception e) {
            throw new IllegalArgumentException("Cannot deserialize CreationEvent from supplied data: "
                                               + serializedString, e);
        }
    }

    static ResizeEvent deserialize(Queue<String> tokens) {
        return new ResizeEvent(Integer.parseInt(tokens.poll()), Integer.parseInt(tokens.poll()),
                               Integer.parseInt(tokens.poll()), Integer.parseInt(tokens.poll()));
    }

// --------------------------- CONSTRUCTORS ---------------------------
    public ResizeEvent(int width, int height, int baseWidth, int baseHeight) {
        this.baseHeight = baseHeight;
        this.baseWidth = baseWidth;
        this.height = height;
        this.width = width;
    }

    /**
     * @return unscaled height
     */
    public int getBaseHeight() {
        return baseHeight;
    }

    /**
     * @return unscaled width
     */
    public int getBaseWidth() {
        return baseWidth;
    }

    /**
     * @return scaled height
     */
    public int getHeight() {
        return height;
    }

    /**
     * @return scaled width
     */
    public int getWidth() {
        return width;
    }

// ------------------------ INTERFACE METHODS ------------------------
// --------------------- Interface Stringalizable ---------------------
    public String serialize() {
        return new StringBuilder().append(width).append(DELIM).append(height).append(DELIM).append(baseWidth).append(
                DELIM).append(baseHeight).toString();
    }
}
