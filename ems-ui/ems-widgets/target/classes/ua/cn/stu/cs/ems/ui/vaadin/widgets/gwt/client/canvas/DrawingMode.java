package ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas;

/**
 * @author n0weak
 */
public enum DrawingMode {

    CREATE, CONNECT, MOVE, SELECT, MARK, BLOCKED
}
