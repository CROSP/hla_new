package ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client;

import com.vaadin.terminal.gwt.client.ApplicationConnection;
import com.vaadin.terminal.gwt.client.Paintable;
import com.vaadin.terminal.gwt.client.UIDL;

/**
 * @author n0weak
 */
public class VCloseInterceptor extends GwtCloseInterceptor implements Paintable {

    public void updateFromUIDL(UIDL uidl, ApplicationConnection client) {
        if (client.updateComponent(this, uidl, true)) {
            return;
        }
        if (uidl.hasVariable("text")) {
            setText(uidl.getStringVariable("text"));
        }
    }
}
