package ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client;

import java.util.HashMap;
import java.util.Map;

import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.VMenu.VMenuItem;

import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Widget;
import com.vaadin.terminal.gwt.client.ApplicationConnection;
import com.vaadin.terminal.gwt.client.Paintable;
import com.vaadin.terminal.gwt.client.UIDL;
import com.vaadin.terminal.gwt.client.ValueMap;

/**
 * Client side implementation for ContextMenu component
 * 
 * @author Peter Lehto / IT Mill Oy Ltd
 */
public class VContextMenu extends Widget implements Paintable {

    protected String paintableId;
    protected ApplicationConnection client;
    private final Map<String, VMenu> availableMenus;
    private int rootX;
    private int rootY;
    private boolean openToRight;

    public VContextMenu() {
        Element element = DOM.createDiv();
        setElement(element);

        availableMenus = new HashMap<String, VMenu>();
    }

    public void updateFromUIDL(UIDL uidl, ApplicationConnection client) {

        if (client.updateComponent(this, uidl, true)) {
            return;
        }

        this.client = client;
        paintableId = uidl.getId();

        availableMenus.clear();

        String[] hierarchy = uidl.getStringArrayAttribute("hierarchy");
        String[] disabledItems = uidl.getStringArrayAttribute("disabled");
        ValueMap itemNames = uidl.getMapAttribute("itemNames");

        // build available menus
        for (String menuId : hierarchy) {
            VMenu contextMenu = new VMenu(this, uidl.getStringArrayAttribute(menuId), hierarchy, disabledItems,
                    itemNames);

            availableMenus.put(menuId, contextMenu);
        }

        // build parent hierarchy for menus
        for (String hierarchyItem : hierarchy) {
            for (String menuItemId : uidl.getStringArrayAttribute(hierarchyItem)) {
                if (availableMenus.containsKey(menuItemId)) {
                    availableMenus.get(menuItemId).setParent(
                            availableMenus.get(hierarchyItem));
                }
            }
        }

        // Show root menu
        if (uidl.hasAttribute("show")) {
            if (uidl.getBooleanAttribute("show") == true) {
                openToRight = false;

                rootX = uidl.getIntAttribute("left");

                if (rootX + 200 > Window.getClientWidth()) {
                    rootX = Window.getClientWidth() - 200;
                }

                rootY = uidl.getIntAttribute("top");
                positionAndShowMenu(availableMenus.get("root"), rootX, rootY);
            }
        }
    }

    private void positionAndShowMenu(VMenu menu, int left, int top) {
        int windowWidth = Window.getClientWidth();

        if (menu.hasParent()) {
            if (left > windowWidth - 200 || openToRight) {
                left -= 400;
                // if menu is opened leftwards, open submenus leftwards too
                openToRight = true;
            }
        }

        int menuHeight = menu.getNumberOfItems() * 20;

        if (top + menuHeight > Window.getClientHeight()) {
            top -= menuHeight;
        }

        menu.setPopupPosition(left, top);
        menu.show();
    }

    /**
     * Called when menu item is clicked. If clicked menu item has a sub menu it
     * will be opened.
     * 
     * @param parent
     * @param id
     * @param top
     */
    public void itemClicked(VMenuItem item) {

        if (availableMenus.containsKey(item.getId())) {
            VMenu openedMenu = availableMenus.get(item.getId());

            // if menu is already open, ignore it
            if (!openedMenu.isShowing()) {

                // tell server that submenu was opened
                client.updateVariable(paintableId, "clickedItem", item.getId(),
                        true);

                if (openedMenu.hasParent()) {
                    // Hide possibly open sibling menus
                    for (VMenu menu : availableMenus.values()) {
                        if (menu.hasParent()) {
                            if (openedMenu.getParentMenu().equals(
                                    menu.getParentMenu())) {
                                menu.hide();
                            }
                        }
                    }

                    // show opened menu
                    positionAndShowMenu(openedMenu, 
                            openedMenu.getParentMenu().getAbsoluteLeft() + openedMenu.getWidth(), 
                            item.getAbsoluteTop());
                }
            }
        } else {
            // final selection, notify server and close all menus
            client.updateVariable(paintableId, "clickedItem", item.getId(),
                    true);

            for (VMenu menu : availableMenus.values()) {
                menu.hide();
            }

            item.getOwnerMenu().hide();
        }
    }
}