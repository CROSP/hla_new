package ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity;

import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.drawer.OutputDrawer;

/**
 * @author n0weak
 */
class Output extends AbstractEntity<OutputDrawer> implements ActiveEntity {

    Output(EntityType type, String id, OutputDrawer drawer) {
        super(type, id, drawer);
    }

    public boolean acceptsConnection(ActiveEntity entity) {
        return inputConnectors.isEmpty() && entity.getType() == EntityType.PLACE;
    }

    public boolean acceptsConnection() {
        return false;
    }
}
