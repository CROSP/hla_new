package ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity;

import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.drawer.PlaceDrawer;

/**
 * @author n0weak
 */
class Place extends AbstractEntity<PlaceDrawer> implements ActiveEntity, Markable {

    private boolean marked;

    Place(EntityType type, String id, PlaceDrawer drawer) {
        super(type, id, drawer);
    }

    public boolean acceptsConnection(ActiveEntity entity) {
        return EntityType.PLACE != entity.getType() && inputConnectors.isEmpty();
    }

    public boolean acceptsConnection() {
        return outputConnectors.isEmpty();
    }

    public void mark() {
        if (!marked) {
            drawer.mark();
            marked = true;
        }
    }

    public void unmark() {
        if (marked) {
            drawer.unmark();
            marked = false;
        }
    }

    public boolean isMarked() {
        return marked;
    }
}
