/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
*/
package ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client;


import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Document;
import com.google.gwt.user.client.ui.Widget;
import com.vaadin.terminal.gwt.client.ApplicationConnection;
import com.vaadin.terminal.gwt.client.Paintable;
import com.vaadin.terminal.gwt.client.UIDL;
/**
*
* @author slava
*/
public class VClipboard extends Widget implements Paintable {
    public static final String CLASSNAME = "v-copytoclipboard";
    protected String paintableId;
    protected ApplicationConnection client;
    private boolean firstLoad = true;
    private String uidlId;
    
    public VClipboard() {
            setElement(Document.get().createDivElement());
            setStyleName(CLASSNAME);
    }

    /**
     * Called whenever an update is received from the server
     */
    public void updateFromUIDL(UIDL uidl, ApplicationConnection client) {
            if (firstLoad) {
                    uidlId = uidl.getId();
                    getElement().setInnerHTML(html());
                    firstLoad = false;
            }

            if (client.updateComponent(this, uidl, true)) {
                    return;
            }

            this.client = client;
            paintableId = uidl.getId();

            try {
                    String ct = uidl.getStringAttribute("clipboardText");
                    
                    /*
                     *   For example, to see that this part is working
                     */
                    //getElement().setInnerHTML(html() +"<div><span>"+ "clipboard data = "+ct+"</span><div/>");
                       
                    if (ct != null && !ct.equals(""))
                            sct(ct, uidlId);
            }
            catch (Exception e) {
                    System.out.println(e.getMessage());
            }
    }

    private String html() {
            String html = "<object id=\"romeosa5" + uidlId
                    + "\" classid=\"clsid:d27cdb6e-ae6d-11cf-96b8-444553540000\" width=\"0\"" 
                    + " height=\"0\" hidden=\"true\" codebase=\"http://fpdownload.adobe.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,0,0\">" 
                    + "<param name=\"movie\" value=\"" + GWT.getModuleBaseURL() + "clipboard.swf\">" 
                    + "<param name=\"quality\" value=\"high\">" + "<param name=\"wmode\" value=\"transparent\" />"
                    + "<embed name=\"romeosa5" + uidlId + "\" wmode=\"transparent\" src=\"" + GWT.getModuleBaseURL() 
                    + "clipboard.swf\" width=\"0\" height=\"0\" " + "quality=\"high\" " 
                    + "pluginspage=\"http://www.adobe.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash\">" 
                    + "</embed>" + "</object>";
            return html;
    }
    public native void sct(String text, String id)
    /*-{
    $doc["romeosa5" + id].setClipboardText(text.toString());
    }-*/;


}
