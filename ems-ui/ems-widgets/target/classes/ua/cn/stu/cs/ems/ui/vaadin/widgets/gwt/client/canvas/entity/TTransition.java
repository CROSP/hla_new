package ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity;

import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.drawer.TransitionDrawer;

/**
 * @author n0weak
 */
class TTransition extends AbstractTransition {

    TTransition(EntityType type, String id, TransitionDrawer drawer) {
        super(type, id, drawer);
    }

    public boolean acceptsConnection(ActiveEntity entity) {
        return inputConnectors.isEmpty() && EntityType.PLACE == entity.getType();
    }

    public boolean acceptsConnection() {
        return outputConnectors.isEmpty();
    }
}
