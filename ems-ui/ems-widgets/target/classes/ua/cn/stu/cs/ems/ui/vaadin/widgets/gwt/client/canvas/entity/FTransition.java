package ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity;

import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.drawer.TransitionDrawer;

/**
 * @author n0weak
 */
class FTransition extends AbstractTransition {    
// --------------------------- CONSTRUCTORS ---------------------------

    FTransition(EntityType type, String id, TransitionDrawer drawer) {
        super(type, id, drawer);
    }

// ------------------------ INTERFACE METHODS ------------------------
// --------------------- Interface ActiveEntity ---------------------

    public boolean acceptsConnection(ActiveEntity entity) {
        return EntityType.PLACE == entity.getType() && inputConnectors.isEmpty();
    }

    public boolean acceptsConnection() {
        return getDrawer().getSticksCount(true) > getConnectorsCount(true);
    }
}
