package ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity;

/**
 * Contract for all the entities that can be repainted. Repainting is currently used for the scaling.
 *
 * @author n0weak
 */
public interface Repaintable {

    /**
     * Repaints this entity.
     */
    void repaint();
}
