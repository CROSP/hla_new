package ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity;

import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.DoubleClickHandler;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverHandler;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.Coordinates;

import java.util.Collection;
import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.drawer.Drawer;

/**
 * Interface to be implemented by every meaningful active entity: place, transition, aggregate.
 *
 * @author n0weak
 */
public interface ActiveEntity extends CanvasEntity, Informative {

    void move(Coordinates mousePos);

    /**
     * Creates and returns connector attached to the output of this entity
     *
     * @return newly created connector attached to the output of this entity
     */
    Connector connectOutput(Coordinates mousePos);

    Connector connectOutput(String outputName);
    /**
     * Connects specified {@link Connector} to the input position of this entity.
     *
     * @param connector connector instance to be attached
     * @param mousePos  coordinates of mouse position, can be used to define desirable connection coordinates
     */
    void connectInput(Connector connector, Coordinates mousePos);

    void connectInput(Connector connector, String inputName);
    /**
     * Disconnects this entity from connector passed.
     *
     * @param connector connector to be detached.
     */
    void disconnect(Connector connector);

    /**
     * Fixes position of this entity on the canvas.
     */
    void fix();

    /**
     * @return identifier of the entity. Every entity must have its own unique identifier.
     */
    String getId();
    
    /**
     * Initializes id of this entity and updates the label.
     *
     * @param id id to be set
     */
    void setId(String id);

    /**
     * @return type of this entity.
     */
    EntityType getType();

    /**
     * -     * to tail
     * Defines whether passed entity can be connected to input position of this one.
     * <p/>
     * -     * @param entity
     * -     * @return
     *
     * @param entity candidate for connection.
     * @return true if this entity accepts connection of the candidate, false if not.
     */
    boolean acceptsConnection(ActiveEntity entity);

    /**
     * -     * to head
     * Defines whether this entity accepts any connection to its output position.
     * <p/>
     * -     * @return
     *
     * @return true if this entity accepts connections to output position, false if not.
     */
    boolean acceptsConnection();

    /**
     * Toggles enabled/disabled mode for this entity.
     *
     * @param enabled true to enable the entity, false to disable
     */
    void setEnabled(boolean enabled);

    /**
     * @return true if this entity is enabled, false otherwise
     */
    boolean isEnabled();

    /**
     * Adds GWT double click handler for this entity.
     *
     * @param handler handler to be added.
     */
    void addDoubleClickHandler(DoubleClickHandler handler);

    /**
     * Adds GWT click handler for this entity.
     *
     * @param handler handler to be added.
     */
    void addClickHandler(ClickHandler handler);

    /**
     * Adds GWT mouse out handler for this entity.
     *
     * @param handler handler to be added.
     */
    void addMouseOutHandler(MouseOutHandler handler);

    /**
     * Adds GWT mouse over handler for this entity.
     *
     * @param handler handler to be added.
     */
    void addMouseOverHandler(MouseOverHandler handler);

    /**
     * Returns amount of connectors attached to this entity
     *
     * @param output output connectors are counted if true, input otherwise
     * @return amount of connectors attached to this entity
     */
    int getConnectorsCount(boolean output);

    /**
     * Defines coordinates of input position by connector's index.
     *
     * @param connectorIdx index of the connector.
     * @return coordinates of the input position.
     */
    Coordinates getInputCoordinates(int connectorIdx);
    
    /**
     * Defines coordinates of output position by connector's index.
     *
     * @param connectorIdx index of the connector.
     * @return coordinates of the output position.
     */
    Coordinates getOutputCoordinates(int connectorIdx);

    Collection<Connector> getInputConnectors();

    Collection<Connector> getOutputConnectors();
    
    /**
     * Using for customization some entities
     * 
     * @return drawer 
     */
    Drawer getDrawer();    
}