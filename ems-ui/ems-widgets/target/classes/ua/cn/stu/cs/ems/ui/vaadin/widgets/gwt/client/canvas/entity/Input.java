package ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity;

import ua.cn.stu.cs.ems.ui.vaadin.widgets.gwt.client.canvas.entity.drawer.InputDrawer;

/**
 * @author n0weak
 */
class Input extends AbstractEntity<InputDrawer> implements ActiveEntity {

    Input(EntityType type, String id, InputDrawer drawer) {
        super(type, id, drawer);
    }

    public boolean acceptsConnection(ActiveEntity entity) {
        return false;
    }

    public boolean acceptsConnection() {
        return outputConnectors.isEmpty();
    }
}
