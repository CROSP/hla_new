package com.vaadin.terminal.gwt.client.ui.richtextarea;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class VRichTextToolbar_Images_default_StaticClientBundleGenerator implements com.vaadin.terminal.gwt.client.ui.richtextarea.VRichTextToolbar.Images {
  public com.google.gwt.resources.client.ImageResource bold() {
    return bold;
  }
  public com.google.gwt.resources.client.ImageResource createLink() {
    return createLink;
  }
  public com.google.gwt.resources.client.ImageResource hr() {
    return hr;
  }
  public com.google.gwt.resources.client.ImageResource indent() {
    return indent;
  }
  public com.google.gwt.resources.client.ImageResource insertImage() {
    return insertImage;
  }
  public com.google.gwt.resources.client.ImageResource italic() {
    return italic;
  }
  public com.google.gwt.resources.client.ImageResource justifyCenter() {
    return justifyCenter;
  }
  public com.google.gwt.resources.client.ImageResource justifyLeft() {
    return justifyLeft;
  }
  public com.google.gwt.resources.client.ImageResource justifyRight() {
    return justifyRight;
  }
  public com.google.gwt.resources.client.ImageResource ol() {
    return ol;
  }
  public com.google.gwt.resources.client.ImageResource outdent() {
    return outdent;
  }
  public com.google.gwt.resources.client.ImageResource removeFormat() {
    return removeFormat;
  }
  public com.google.gwt.resources.client.ImageResource removeLink() {
    return removeLink;
  }
  public com.google.gwt.resources.client.ImageResource strikeThrough() {
    return strikeThrough;
  }
  public com.google.gwt.resources.client.ImageResource subscript() {
    return subscript;
  }
  public com.google.gwt.resources.client.ImageResource superscript() {
    return superscript;
  }
  public com.google.gwt.resources.client.ImageResource ul() {
    return ul;
  }
  public com.google.gwt.resources.client.ImageResource underline() {
    return underline;
  }
  private void _init0() {
    bold = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
    "bold",
    bundledImage_None,
    340, 0, 20, 20, false, false
  );
    createLink = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
    "createLink",
    bundledImage_None,
    320, 0, 20, 20, false, false
  );
    hr = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
    "hr",
    bundledImage_None,
    300, 0, 20, 20, false, false
  );
    indent = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
    "indent",
    bundledImage_None,
    280, 0, 20, 20, false, false
  );
    insertImage = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
    "insertImage",
    bundledImage_None,
    260, 0, 20, 20, false, false
  );
    italic = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
    "italic",
    bundledImage_None,
    240, 0, 20, 20, false, false
  );
    justifyCenter = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
    "justifyCenter",
    bundledImage_None,
    220, 0, 20, 20, false, false
  );
    justifyLeft = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
    "justifyLeft",
    bundledImage_None,
    200, 0, 20, 20, false, false
  );
    justifyRight = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
    "justifyRight",
    bundledImage_None,
    180, 0, 20, 20, false, false
  );
    ol = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
    "ol",
    bundledImage_None,
    160, 0, 20, 20, false, false
  );
    outdent = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
    "outdent",
    bundledImage_None,
    140, 0, 20, 20, false, false
  );
    removeFormat = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
    "removeFormat",
    bundledImage_None,
    120, 0, 20, 20, false, false
  );
    removeLink = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
    "removeLink",
    bundledImage_None,
    100, 0, 20, 20, false, false
  );
    strikeThrough = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
    "strikeThrough",
    bundledImage_None,
    80, 0, 20, 20, false, false
  );
    subscript = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
    "subscript",
    bundledImage_None,
    60, 0, 20, 20, false, false
  );
    superscript = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
    "superscript",
    bundledImage_None,
    40, 0, 20, 20, false, false
  );
    ul = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
    "ul",
    bundledImage_None,
    20, 0, 20, 20, false, false
  );
    underline = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
    "underline",
    bundledImage_None,
    0, 0, 20, 20, false, false
  );
  }
  
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static final java.lang.String bundledImage_None = GWT.getModuleBaseURL() + "DE709C1CAB0F65BCC5BEF11EDA73A925.cache.png";
  private static com.google.gwt.resources.client.ImageResource bold;
  private static com.google.gwt.resources.client.ImageResource createLink;
  private static com.google.gwt.resources.client.ImageResource hr;
  private static com.google.gwt.resources.client.ImageResource indent;
  private static com.google.gwt.resources.client.ImageResource insertImage;
  private static com.google.gwt.resources.client.ImageResource italic;
  private static com.google.gwt.resources.client.ImageResource justifyCenter;
  private static com.google.gwt.resources.client.ImageResource justifyLeft;
  private static com.google.gwt.resources.client.ImageResource justifyRight;
  private static com.google.gwt.resources.client.ImageResource ol;
  private static com.google.gwt.resources.client.ImageResource outdent;
  private static com.google.gwt.resources.client.ImageResource removeFormat;
  private static com.google.gwt.resources.client.ImageResource removeLink;
  private static com.google.gwt.resources.client.ImageResource strikeThrough;
  private static com.google.gwt.resources.client.ImageResource subscript;
  private static com.google.gwt.resources.client.ImageResource superscript;
  private static com.google.gwt.resources.client.ImageResource ul;
  private static com.google.gwt.resources.client.ImageResource underline;
  
  static {
    new VRichTextToolbar_Images_default_StaticClientBundleGenerator()._init0();
  }
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      bold(), 
      createLink(), 
      hr(), 
      indent(), 
      insertImage(), 
      italic(), 
      justifyCenter(), 
      justifyLeft(), 
      justifyRight(), 
      ol(), 
      outdent(), 
      removeFormat(), 
      removeLink(), 
      strikeThrough(), 
      subscript(), 
      superscript(), 
      ul(), 
      underline(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("bold", bold());
        resourceMap.put("createLink", createLink());
        resourceMap.put("hr", hr());
        resourceMap.put("indent", indent());
        resourceMap.put("insertImage", insertImage());
        resourceMap.put("italic", italic());
        resourceMap.put("justifyCenter", justifyCenter());
        resourceMap.put("justifyLeft", justifyLeft());
        resourceMap.put("justifyRight", justifyRight());
        resourceMap.put("ol", ol());
        resourceMap.put("outdent", outdent());
        resourceMap.put("removeFormat", removeFormat());
        resourceMap.put("removeLink", removeLink());
        resourceMap.put("strikeThrough", strikeThrough());
        resourceMap.put("subscript", subscript());
        resourceMap.put("superscript", superscript());
        resourceMap.put("ul", ul());
        resourceMap.put("underline", underline());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'bold': return this.@com.vaadin.terminal.gwt.client.ui.richtextarea.VRichTextToolbar.Images::bold()();
      case 'createLink': return this.@com.vaadin.terminal.gwt.client.ui.richtextarea.VRichTextToolbar.Images::createLink()();
      case 'hr': return this.@com.vaadin.terminal.gwt.client.ui.richtextarea.VRichTextToolbar.Images::hr()();
      case 'indent': return this.@com.vaadin.terminal.gwt.client.ui.richtextarea.VRichTextToolbar.Images::indent()();
      case 'insertImage': return this.@com.vaadin.terminal.gwt.client.ui.richtextarea.VRichTextToolbar.Images::insertImage()();
      case 'italic': return this.@com.vaadin.terminal.gwt.client.ui.richtextarea.VRichTextToolbar.Images::italic()();
      case 'justifyCenter': return this.@com.vaadin.terminal.gwt.client.ui.richtextarea.VRichTextToolbar.Images::justifyCenter()();
      case 'justifyLeft': return this.@com.vaadin.terminal.gwt.client.ui.richtextarea.VRichTextToolbar.Images::justifyLeft()();
      case 'justifyRight': return this.@com.vaadin.terminal.gwt.client.ui.richtextarea.VRichTextToolbar.Images::justifyRight()();
      case 'ol': return this.@com.vaadin.terminal.gwt.client.ui.richtextarea.VRichTextToolbar.Images::ol()();
      case 'outdent': return this.@com.vaadin.terminal.gwt.client.ui.richtextarea.VRichTextToolbar.Images::outdent()();
      case 'removeFormat': return this.@com.vaadin.terminal.gwt.client.ui.richtextarea.VRichTextToolbar.Images::removeFormat()();
      case 'removeLink': return this.@com.vaadin.terminal.gwt.client.ui.richtextarea.VRichTextToolbar.Images::removeLink()();
      case 'strikeThrough': return this.@com.vaadin.terminal.gwt.client.ui.richtextarea.VRichTextToolbar.Images::strikeThrough()();
      case 'subscript': return this.@com.vaadin.terminal.gwt.client.ui.richtextarea.VRichTextToolbar.Images::subscript()();
      case 'superscript': return this.@com.vaadin.terminal.gwt.client.ui.richtextarea.VRichTextToolbar.Images::superscript()();
      case 'ul': return this.@com.vaadin.terminal.gwt.client.ui.richtextarea.VRichTextToolbar.Images::ul()();
      case 'underline': return this.@com.vaadin.terminal.gwt.client.ui.richtextarea.VRichTextToolbar.Images::underline()();
    }
    return null;
  }-*/;
}
