package ua.cn.stu.cs.ems.dbus;


import org.junit.Before;
import org.junit.Test;
import ua.cn.stu.cs.ems.dbus.DBus.Message;
import static org.junit.Assert.*;

/**
 *
 * @author leonid
 */
public class DBusOneServiceTest {

    private Thread app;

    public DBusOneServiceTest() {
    }

    @Before
    public void setUpVariables() {
        app = Thread.currentThread();
    }

    @Test
    public void testRegisterAndUnregisterApp() {
        DBus.registerAppKey(app);
        assertTrue(DBus.isRegistered(app));

        DBus.unregister(app);
        assertFalse(DBus.isRegistered(app));
    }

    @Test
    public void testRegisterAppAndService() {
        DBus.registerAppKey(app);

        TestService ts = new TestService(app);
        DBus.registerService(app, ts);
        assertTrue(DBus.isRegistered(app, ts));

        DBus.Service serv = DBus.find(app, ts.getInfo().getIface());
        assertNotNull(serv);
        assertEquals(ts, serv);
    }

    private class TestService extends ADBusService {

        private final Info INFO = new Info() {

            public String getName() {
                return "D-Bus Test Service";
            }

            public String getIface() {
                return "org.ems.dbus.Test";
            }

            public String getDescription() {
                return "Test service";
            }
        };

        public TestService(Thread app) {
            super(app);
        }

        public void receive(Message msg) {
            System.out.println("Recived messege: " + msg.toString()
                    + " data: " + msg.getData()
                    + " type: " + msg.getMessageType());
        }

        public Info getInfo() {
            return INFO;
        }
    }
}
