package ua.cn.stu.cs.ems.dbus;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ua.cn.stu.cs.ems.dbus.services.DBusSystemService;

/**
 *
 * @author leonid
 */
// TODO using object for key of app (not Thread)
public class DBus {

    final static Logger logger = LoggerFactory.getLogger(DBus.class);
    private final static DBusSystemService dbss = new DBusSystemService(Thread.currentThread());

    public synchronized static void send(Message msg) {
        dbss.send(msg);
    }

    public static synchronized Service find(Object appKey, String serviceName) {
        return dbss.find(appKey, serviceName);
    }

    public static synchronized void registerService(Object app, Service service) {
        dbss.register(app, service);
    }

    public static synchronized void registerAppKey(Object app) {
        dbss.registerAppKey(app);
        registerService(app, dbss);
    }

    public static synchronized void unregister(Object app) {
        dbss.unregister(app);
    }

    public static boolean isRegistered(Object app) {
        return dbss.isRegistered(app);
    }

    public static boolean isRegistered(Thread app, Service service) {
        return dbss.isRegistered(app, service);
    }

    public static void stop() {
        dbss.forceStop();
    }
    /*
     * Interfaces used as components in system
     */

    public static interface Service {

        public static interface Info {

            public String getName();

            public String getIface();

            public String getDescription();
        }

        void receive(Message msg);

        Info getInfo();
    }

    public static interface Message<T> extends Sender, HasDescription {

        enum MessageType {

            ACTION, RESPONSE, DATA, ERROR;
        }

        MessageType getMessageType();

        void setMessageType(MessageType mt);

        T getData();

        T[] getDataArray();

        boolean isSingleData();
    }

    public static interface Sender {

        Object getAppKey();

        String getSource();

        String getTarget();
    }

    public static interface App {

        void register();

        void unregister();
    }

    public static interface HasDescription {

        String getDescription();

        void setDescription(String data);
    }
}
