package ua.cn.stu.cs.ems.dbus;

/**
 *
 * @author leonid
 */
public interface Message<T> {
    
    T getData();
    
}
