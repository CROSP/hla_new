package ua.cn.stu.cs.ems.dbus;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import ua.cn.stu.cs.ems.dbus.DBus.Message;
import ua.cn.stu.cs.ems.dbus.DBus.Service;
import ua.cn.stu.cs.ems.dbus.messages.ExceptionMessage;

/**
 *
 * @author leonid
 */
public class DBusExecutor {
    
    final static Logger logger = LoggerFactory.getLogger(DBusExecutor.class);
    
    private List<Executor> executions;
    private static Queue<Message> messages = new LinkedList<Message>();
    
    public DBusExecutor(int cores) {
        init(cores);
    }
    
    private class Executor extends Thread {
        
        @Override
        public void run() {
            Message m;
            while (null != (m = DBusExecutor.this.peakMessage())) {
                Service srv = DBus.find(m.getAppKey(), m.getTarget());
                logger.debug("found service: " + srv + ". Sending message from " + m.getSource());
                if (srv != null) {
                    try {
                        srv.receive(m);
                    } catch (final Exception e) {
                        if (m.getSource() != null) {
                            DBus.send(new ExceptionMessage(m) {{
                                setMessageType(MessageType.ERROR);
                                setDescription("Internal error: " + e.getMessage());
                            }});
                        }
                    }
                }
                else {
                    if (m.getSource() != null) {
//                        logger.debug("calback");
                        DBus.send(new ExceptionMessage(m) {{
                            setMessageType(MessageType.ERROR);
                            setDescription("Target not found");
                        }});
                    }
                }
            }
        }
    }
    
    private synchronized Queue<Message> getMessageQueue() {
        return messages;
    }
    
    // run 1 thread (from pool) per message
    public synchronized void fireIncomingMessage(Message m) {
        getMessageQueue().add(m);
        
        for (Executor e: executions) {
            if (!e.isAlive()) {
                logger.debug("starting thread " + e.getName());
                e.start();
                break;
            }
        }
    }
    
    private synchronized Message peakMessage() {
        return getMessageQueue().poll();
    }
    
    public void forceStop() {
        Queue<Message> backup = new LinkedList<Message>();
        Message m;
        while (null != (m = peakMessage())) {
            backup.add(m);
        }
        
        for (Executor e: executions) {
            if (e.isAlive()) {
                try {
                    e.join();
                } catch (InterruptedException ie) {
                }
            }
        }
        
        while (null != (m = backup.peek())) {
            getMessageQueue().add(m);
        }
    }
    
    public void init(int cores) {
        logger.debug("initing D-BUS service executor");
        executions = new ArrayList<Executor>(cores);
        for (int i=0;i<cores;i++) {
            Executor e = new Executor();
            e.setName("executor@" + (i+1));
            executions.add(e);
        }
    }
}
