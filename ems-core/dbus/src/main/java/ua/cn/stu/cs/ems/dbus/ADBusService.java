package ua.cn.stu.cs.ems.dbus;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author leonid
 */
public abstract class ADBusService implements DBus.Service {
    
    private Thread app;
    private String id;
    private final List<String> aliases = new ArrayList<String>();
    
    public ADBusService(Thread app) {
        this.app = app;
    }

    public boolean setAlias(String alias) {
        if (null == DBus.find(app, alias)) {
            aliases.add(alias);
            return true;
        }
        return false;
    }

    public boolean unsetAlias(String alias) {
        return aliases.remove(alias);
    }
    
    @Override
    public String toString() {
        return "Service " + getInfo().getIface();
    }
}
