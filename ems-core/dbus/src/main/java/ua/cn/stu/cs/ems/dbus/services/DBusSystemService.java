package ua.cn.stu.cs.ems.dbus.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ua.cn.stu.cs.ems.dbus.ADBusService;
import ua.cn.stu.cs.ems.dbus.DBus.Message;
import ua.cn.stu.cs.ems.dbus.DBus.Service;
import ua.cn.stu.cs.ems.dbus.DBusExecutor;

/**
 *
 * @author leonid
 */
public class DBusSystemService extends ADBusService {
    
    final static Logger logger = LoggerFactory.getLogger(DBusSystemService.class);
    public static final Info INFO = new Info() {

        public String getName() {
            return "D-Bus Default System Service (core)";
        }

        public String getIface() {
            return "org.ems.dbus.Manager";
        }

        public String getDescription() {
            return "Core service that allow using some controlling functions "
                    + "for system";
        }
    };
    
    private static final int PROCESSOR_CORES = 1;
    private DBusExecutor dbe;
    private HashMap<Object, ArrayList<Service>> services = 
            new HashMap<Object, ArrayList<Service>>();

    public DBusSystemService(Thread app) {
        super(app);
        dbe = new DBusExecutor(PROCESSOR_CORES);
    }

    public void receive(Message msg) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
    /************************* getter and setters *****************************/
    private synchronized List<Service> services(Object appKey) {
        return services.get(appKey);
    }
    
    /***************** Methods for controlling system *************************/
    public synchronized void send(Message msg) {
        dbe.fireIncomingMessage(msg);
    }
    
    public synchronized Service find(Object appKey, String serviceName) {
        for (Service s: services(appKey)) {
            if (s.getInfo().getIface().equals(serviceName)) {
                return s;
            }
        }
        return null;
    }
    
    public synchronized void register(Object app, Service service) {
        if (find(app, service.getInfo().getIface()) == null) {
            services(app).add(service);
        }
    }

    public synchronized void registerAppKey(Object app) {
        if (!isRegistered(app)) {
            services.put(app, new ArrayList<Service>());
        }
    }
    
    public synchronized void unregister(Object app) {
        services.remove(app);
    }
    
    public boolean isRegistered(Object app) {
        return services(app) != null;
    }
    
    public boolean isRegistered(Object app, Service service) {
        return services(app) != null && services(app).contains(service);
    }

    public void forceStop() {
        dbe.forceStop();
    }

    public Info getInfo() {
        return INFO;
    }
    
    private void executeAction() {
        
    }
}
