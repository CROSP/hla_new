package ua.cn.stu.cs.ems.dbus.messages;

import ua.cn.stu.cs.ems.dbus.DBus.Message;
import ua.cn.stu.cs.ems.dbus.DBusMessage;

/**
 *
 * @author leonid
 */
public class ExceptionMessage extends DBusMessage<Message> {
    
    public ExceptionMessage(Message m) {
        super(m.getAppKey(), null, m.getSource(), m);
    }
}
