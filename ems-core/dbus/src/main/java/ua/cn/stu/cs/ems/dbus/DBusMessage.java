package ua.cn.stu.cs.ems.dbus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import ua.cn.stu.cs.ems.dbus.DBus.Message.MessageType;

/**
 *
 * @author leonid
 */
public class DBusMessage<T> implements DBus.Message<T> {
    
    private Object appKey;
    private String source;
    private String target;
    private List<T> data;
    private MessageType msgType;
    private String desc; // description
    
    public DBusMessage(Object appKey, String source, String target, 
            T... values) {
        this.appKey = appKey;
        this.source = source;
        this.target = target;
        data = new ArrayList<T>(values.length);
        data.addAll(Arrays.asList(values));
        msgType = MessageType.DATA;
    }
    
    public boolean isSingleData() {
        return data.size() == 1;
    }
    
    public T getData() {
        return data.isEmpty() ? null : data.get(0);
    }
    
    public T[] getDataArray() {
        T[] values = (T[])Collections.unmodifiableList(data).toArray();
        return values;
    }
    
    public String getSource() {
        return source;
    }
    
    public String getTarget() {
        return target;
    }

    public Object getAppKey() {
        return appKey;
    }

    public MessageType getMessageType() {
        return msgType;
    }

    public void setMessageType(MessageType mt) {
        msgType = mt;
    }
    
    @Override
    public String toString() {
        return "Message from " + getSource() + " to " + getTarget();
    }

    public String getDescription() {
        return this.desc;
    }

    public void setDescription(String data) {
        this.desc = data;
    }
}
