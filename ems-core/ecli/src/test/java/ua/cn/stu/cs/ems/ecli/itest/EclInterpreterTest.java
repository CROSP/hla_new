package ua.cn.stu.cs.ems.ecli.itest;

import org.junit.Assert;
import org.junit.Test;
import ua.cn.stu.cs.ems.ecli.*;
import ua.cn.stu.cs.ems.ecli.errors.ErrorCode;
import ua.cn.stu.cs.ems.ecli.errors.InterpretationException;
import ua.cn.stu.cs.ems.ecli.errors.ParsingError;
import ua.cn.stu.cs.ems.ecli.errors.ValidationException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.*;

/**
 * @author n0weak
 */
public class EclInterpreterTest {

    private EclInterpreter interpreter = new EclInterpreter(false, true);

    @Test
    public void testReturn() throws IOException, ValidationException {
        checkReturn("RETURN 2;", 2);
        checkReturn("RETURN;", null);
        checkReturn("RETURN 2/0;", Value.UNDEFINED, false);
        checkReturn("RETURN 2; RETURN 3;", 2, false);
    }

    @Test
    public void testArythm() throws IOException, ValidationException {
        checkReturn("RETURN 3/2;", 1.5);
        checkReturn("RETURN 4/1.5;", 2.666666666666667);
        checkReturn("RETURN 134-3/2*45+12*(9-3);", 138.5);
        //testing + and - signs interpretation
        checkReturn("RETURN -2 + -45;", -47);
    }

    @Test
    public void testSimpleVariableUsage() throws IOException, ValidationException {
        checkReturn("VAR X; VAR Y; X=2; Y=4; RETURN Y*X;", 8);
    }

    @Test
    public void testUndefinedVariableReporting() throws IOException {
        testErrorReporting("VAR X;\nY=2+2;", 2, 0, 2, 5, ErrorCode.UNDEFINED_VAR);
        testErrorReporting("X=X+2;", 1, 0, 1, 5, ErrorCode.UNDEFINED_VAR);
        testErrorReporting("X=2+X;", 1, 0, 1, 5, ErrorCode.UNDEFINED_VAR);
    }

    @Test
    public void testUnreachableStatementValidation() throws IOException {
        ValidationReport report = validate("RETURN 2;\nVAR X=2;\nX=X+1;");
        assertEquals(2, report.getParsingErrors().size());
        ParsingError parsingError = report.getParsingErrors().get(0);
        assertEquals(ErrorCode.UNREACHABLE_STMT, parsingError.getErrorCode());
        parsingError = report.getParsingErrors().get(1);
        assertEquals(ErrorCode.UNREACHABLE_STMT, parsingError.getErrorCode());
    }

    @Test
    public void testRedefinedVarValidation() throws IOException {
        testErrorReporting("VAR X = 10; \nVAR X = \n20;", 2, 0, 3, 2, ErrorCode.REDEFINED_VAR);
    }

    @Test
    public void testInvalidStatementValidation() throws IOException {
        testErrorReporting("22+33;", 1, 0, 1, 5, ErrorCode.INVALID_STMT);
        testErrorReporting("SIN(33);", 1, 0, 1, 7, ErrorCode.INVALID_STMT);
        testErrorReporting("VAR X = 10;\n(X);", 2, 0, 2, 3, ErrorCode.INVALID_STMT);
    }

    @Test
    public void testErrorsValidation() throws IOException {
        testErrorReporting("RETURN 22+44", 0, -1, 0, -1, ErrorCode.UNEXPECTED_EOF);
        testErrorReporting("\nVAR X;\n  X=2", 0, -1, 0, -1, ErrorCode.UNEXPECTED_EOF);
        testErrorReporting("VAR Y = 22 33; ", 1, 11, 1, 12, ErrorCode.MISMATCHED_TOKEN);
    }

    @Test
    public void testErrorCounting() throws IOException {
        assertErrorsFound("VAR X = 22 33; RETURN 3/2", 2);
        assertErrorsFound("VAR X = 22 33;\nVAR X = \n20;\nY = 2;\nRETURN 3/2 X=X+2;", 5);
        assertErrorsFound("fdsfdsfsdf VAR X =2; VAR X;", 3);
    }

    @Test
    public void testComplicatedValidations() throws IOException {
        testValidationErrorReporting("VAR Y = 22 33; VAR X = 10; \nVAR X = \n20;\nZ = 2;\nRETURN 3/2 X=X+2;", new ParsingError[]{
                    new ParsingError(1, 11, 1, 12, ErrorCode.MISMATCHED_TOKEN, ""),
                    new ParsingError(5, 11, 5, 11, ErrorCode.MISMATCHED_TOKEN, ""),
                    new ParsingError(2, 0, 3, 2, ErrorCode.REDEFINED_VAR, ""),
                    new ParsingError(4, 0, 4, 5, ErrorCode.UNDEFINED_VAR, ""),
                    new ParsingError(5, 11, 5, 16, ErrorCode.UNREACHABLE_STMT, "")
                });
    }

    @Test
    public void testHasReturn() throws IOException {
        ValidationReport report = validate("RETURN 2;");
        assertTrue(report.returnsValue());
        report = validate("VAR X = 2;");
        assertFalse(report.returnsValue());
        report = validate("IF (2==2) RETURN 2;");
        assertFalse(report.returnsValue());
        report = validate("IF (2==2) RETURN 2; ELSE RETURN;");
        assertFalse(report.returnsValue());
        report = validate("IF (2==2) RETURN 2; ELSE RETURN -2;");
        assertTrue(report.returnsValue());
    }
    //TODO uncomment :)
    @Test
    public void testInvalidInputValidation() throws IOException {
   /*     testErrorReporting("fdsfdsfsdf", 0, -1, 0, -1, ErrorCode.UNEXPECTED_EOF);
        testErrorReporting("RETURN 2@;", 1, 8, 1, 8, ErrorCode.INVALID_INPUT);
        testErrorReporting("RETURN 2;$", 1, 9, 1, 9, ErrorCode.INVALID_INPUT);
        testErrorReporting("=1", 1, 0, 1, 0, ErrorCode.INVALID_INPUT);
        testErrorReporting("", 0, -1, 0, -1, ErrorCode.INVALID_INPUT);*/
    }

    @Test
    public void testAllowedVarNames() throws IOException {
        assertErrorsFound("VAR A1 = 22; RETURN A1;", 0);
        assertErrorsFound("VAR aA1 = 22; RETURN aA1;", 0);
        assertErrorsFound("VAR _A1 = 22; RETURN _A1;", 0);
        assertErrorsFound("VAR ZERO_VAL = 0; RETURN ZERO_VAL;", 0);
        testErrorReporting("VAR 11BAD_VAR = 0;", 1, 4, 1, 12, ErrorCode.INVALID_INPUT);
    }

    @Test
    public void testArithmeticErrorsValidation() throws IOException {
        testWarningsReporting("RETURN 2/0;", 1, 0, 1, 10, ErrorCode.DIV_BY_0);
        testWarningsReporting("RETURN 2/(2-2);", 1, 0, 1, 14, ErrorCode.DIV_BY_0);
        testWarningsReporting("VAR X = 22; X = X/2; X = X-11; RETURN 2/X;", 1, 31, 1, 41, ErrorCode.DIV_BY_0);
        testWarningsReporting("VAR X = 22;\nX = X/0;\nVAR Y = 12-11;\nRETURN 2/(Y-1);", new ParsingError[]{
                    new ParsingError(2, 0, 2, 7, ErrorCode.DIV_BY_0, ""),
                    new ParsingError(4, 0, 4, 14, ErrorCode.DIV_BY_0, "")
                });
        //all related to 'X' calculations have to be stopped after the first /0
        testWarningsReporting("VAR X = 22;\nX = X/0;\nRETURN 2/(X-1);", new ParsingError[]{
                    new ParsingError(2, 0, 2, 7, ErrorCode.DIV_BY_0, ""),});
    }

    @Test
    public void testCalculations() throws IOException {
        checkReturn("VAR VERY_BIG_VAL = 9999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999;"
                + "VERY_BIG_VAL = VERY_BIG_VAL*10; RETURN VERY_BIG_VAL;",
                "99999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999990");
        checkReturn("VAR VERY_BIG_VAL = 9999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999;"
                + "VERY_BIG_VAL = SIN(VERY_BIG_VAL); RETURN VERY_BIG_VAL;",
                Value.UNDEFINED, false);
        checkReturn("VAR VERY_BIG_VAL = 9999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999;"
                + "VERY_BIG_VAL = COS(VERY_BIG_VAL); RETURN VERY_BIG_VAL;",
                Value.UNDEFINED, false);
        checkReturn("VAR VERY_BIG_VAL = 9999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999;"
                + "VERY_BIG_VAL = TAN(VERY_BIG_VAL); RETURN VERY_BIG_VAL;",
                Value.UNDEFINED, false);
        checkReturn("VAR VERY_BIG_VAL = 9999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999;"
                + "VERY_BIG_VAL = COT(VERY_BIG_VAL); RETURN VERY_BIG_VAL;",
                Value.UNDEFINED, false);

    }

    @Test
    public void testUndefinedValuesProcessing() throws IOException {
        //interpreter returns Value.UNDEFINED when undefined value is returned
        checkReturn("VAR VERY_BIG_VAL = 9999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999;"
                + "VERY_BIG_VAL = COS(VERY_BIG_VAL); RETURN VERY_BIG_VAL;",
                Value.UNDEFINED, false);
        checkReturn("RETURN 2/0;", Value.UNDEFINED, false);
        //interpreter returns actual value without any errors when undefined value is overwritten
        checkReturn("VAR VERY_BIG_VAL = 9999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999;"
                + "VERY_BIG_VAL = COS(VERY_BIG_VAL); VERY_BIG_VAL = 22; RETURN VERY_BIG_VAL;",
                22, false);


        //validator must produce errors in such cases
        testWarningsReporting("VAR VERY_BIG_VAL = 9999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999;"
                + "\nVERY_BIG_VAL = SIN(VERY_BIG_VAL);\nRETURN VERY_BIG_VAL;", new ParsingError[]{
                    new ParsingError(2, 0, 2, 32, ErrorCode.UNDEFINED_VALUE, ""),});
        testWarningsReporting("VAR VERY_BIG_VAL = 9999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999;"
                + "\nVERY_BIG_VAL = COS(VERY_BIG_VAL); \nVERY_BIG_VAL = 22;\n RETURN VERY_BIG_VAL;", new ParsingError[]{
                    new ParsingError(2, 0, 2, 32, ErrorCode.UNDEFINED_VALUE, "")
                });

        assertErrorsFound("VAR VERY_BIG_VAL = 9999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999;"
                + "\nVAR UNDEF_VAL = COS(VERY_BIG_VAL); \nVERY_BIG_VAL = UNDEF_VAL;\n RETURN UNDEF_VAL*VERY_BIG_VAL;", 2);

        testWarningsReporting("VAR X;\nRETURN X;", 2, 0, 2, 8, ErrorCode.UNDEFINED_VALUE);
        testWarningsReporting("VAR X;\nVAR Z = 2 + X;\n Z=0; RETURN Z;", 2, 0, 2, 13, ErrorCode.UNDEFINED_VALUE);

        ModelAccessor modelAccessor = createModelAccessor();
        testWarningsReporting("P['place1'].T['markAttr2'] = 15/0; \nRETURN P['place1'].T['markAttr2'];", modelAccessor, new ParsingError[]{
                    new ParsingError(1, 0, 1, 33, ErrorCode.DIV_BY_0, ""),});
        assertEquals(Value.UNDEFINED, modelAccessor.getTokenAt("place1").getAttribute("markAttr2"));

        testWarningsReporting("A['agg1'].V['nestedVar45'] = 2/0; \nRETURN A['agg1'].V['nestedVar45'];", modelAccessor, new ParsingError[]{
                    new ParsingError(1, 0, 1, 32, ErrorCode.DIV_BY_0, ""),});
        assertEquals(Value.UNDEFINED, modelAccessor.getAggregate("agg1").getVariable("nestedVar45"));
    }

    @Test
    public void testStringProcessing() throws IOException {
        checkReturn("RETURN 2+'34';", "234");
        checkReturn("RETURN 2+\"34\";", "234");
        checkReturn("RETURN 'RESULT: '+3/0;", "RESULT: UNDEFINED", false);
        checkReturn("RETURN '2\\\\34\\\\s\\\\n';", "2\\34\\s\\n");
        checkReturn("RETURN \"2\\\\34\\\\s\\\\n\";", "2\\34\\s\\n");
        checkReturn("RETURN '\"QUOTED_VAL\"';", "\"QUOTED_VAL\"");
        checkReturn("RETURN \"'QUOTED_VAL'\";", "'QUOTED_VAL'");
        checkReturn("RETURN '\\\"QUOTED_VAL\\\"';", "\"QUOTED_VAL\"");
        checkReturn("RETURN \"\\\'QUOTED_VAL\\\'\";", "\'QUOTED_VAL\'");
        checkReturn("RETURN '\\\'QUOTED_VAL\\\'';", "\'QUOTED_VAL\'");
        checkReturn("RETURN \"\\\"QUOTED_VAL\\\"\";", "\"QUOTED_VAL\"");
        checkReturn("RETURN \"\";", "");
        checkReturn("RETURN '';", "");
        testInterpretationErrorReporting("RETURN '\\\n';", 1, 9, 1, 9, ErrorCode.INVALID_INPUT);
        testInterpretationErrorReporting("RETURN \"\\\n\";", 1, 9, 1, 9, ErrorCode.INVALID_INPUT);
    }

    @Test
    public void testComparison() throws IOException {
        checkReturn("VAR X = 2 > 0; RETURN X;", true);
        checkReturn("RETURN 2 > 2.0;", false);
        checkReturn("RETURN 2 >= 2.0;", true);
        checkReturn("RETURN 2 <= 2.0;", true);
        checkReturn("RETURN 2 < 2.0;", false);
        checkReturn("RETURN 2 == 2.0;", true);
        checkReturn("RETURN 2 != 2.0;", false);
        checkReturn("RETURN 2 == '2.0';", false);
        checkReturn("RETURN 2 != '2.0';", true);

        checkReturn("RETURN TRUE > FALSE;", true);
        checkReturn("RETURN TRUE > 1;", true);
        checkReturn("RETURN FALSE > 1;", true);
        checkReturn("RETURN TRUE < 'TRUF';", true);
        checkReturn("RETURN 2/0 == 2/0;", true, false);
        checkReturn("RETURN 2/0 != 2/0;", false, false);
        checkReturn("RETURN 2/0 > 2;", Value.UNDEFINED, false);

        checkReturn("VAR X = 'abcd'; RETURN 'abaa' < X;", true);
        checkReturn("VAR X = 3/0; RETURN X > 'abaa';", Value.UNDEFINED, false);
    }

    @Test
    public void testInversions() throws IOException {
        checkReturn("RETURN -2;", -2);
        checkReturn("RETURN !2;", -2);
        checkReturn("RETURN -(!(-(-2)));", 2);
        checkReturn("VAR X = 2.055; RETURN !X;", -2.055);
        checkReturn("RETURN !2 < 1;", true);
        checkReturn("RETURN -(2 > 1.5);", false);
        checkReturn("RETURN -'string';", "string");
        checkReturn("RETURN !'string';", "string");
        checkReturn("RETURN -(2/0);", Value.UNDEFINED, false);
    }

    @Test
    public void testLogical() throws IOException {
        checkReturn("RETURN 2 > -2 && 10 <= 10;", true);
        checkReturn("RETURN 2 > -2 || 10 <= 10;", true);
        checkReturn("RETURN 2 > -2 && 10 >= 100;", false);
        checkReturn("RETURN 2 > -2 || 10 >= 100;", true);
        checkReturn("RETURN !(2 > -2 || 10 <= 100) && 2>-100;", false);
        checkReturn("RETURN -(2 > -2 && 10 <= 100) || 2>-100;", true);

        checkReturn("RETURN 2 > -2 || 10;", Value.UNDEFINED, false);
        checkReturn("RETURN 'string' || 10;", Value.UNDEFINED, false);
        checkReturn("RETURN 222.06 && 10;", Value.UNDEFINED, false);
    }

    @Test
    public void testIfStmt() throws IOException {
        checkReturn("VAR X = 0; IF (2==2) X= -92; ELSE X= 92; RETURN X;", -92);
        checkReturn("VAR X = 0; IF (2!=2) X= -92; ELSE X= 92; RETURN X;", 92);
        checkReturn("VAR X = 0; IF (2!=2) X= -92; RETURN X;", 0);
        checkReturn("VAR X = 0; IF (2==2) X= -92; RETURN X;", -92);

        checkReturn("VAR X = 0; IF ('string' && 2) X= -92; ELSE X= 92; RETURN X;", 92);
        checkReturn("VAR X = 0; IF ('aa' < 'bb' && 2!=2) X= -92; ELSE X= 92; RETURN X;", 92);

        checkReturn("VAR X = 0; IF ('aa' < 'bb' && 2!=2) {X= -92;} ELSE { X= 92; } RETURN X;", 92);
        checkReturn("VAR X = 0; IF (2==2) {X= -92; RETURN X; } ELSE { RETURN 21; } RETURN -100;", -92, false);

        checkReturn("IF (2!=2) IF (2==2) RETURN -1; ELSE RETURN -2;", null);
        checkReturn("IF (2==2) IF (2!=2) RETURN -1; ELSE RETURN -2;", -2);
        checkReturn("IF (2==2) IF (2!=2) RETURN -1; ELSE RETURN -2; ELSE RETURN -3;", -2);
        checkReturn("IF (2!=2) IF (2!=2) RETURN -1; ELSE RETURN -2; ELSE RETURN -3;", -3);
        checkReturn("IF (2==2) {IF (2!=2) RETURN -1;} ELSE RETURN -2;", null);

        String redefinedVarWithUnreachableCodeStmt = "VAR X = 0; \nIF (2!=2) \n{VAR X= -92; RETURN X; \nRETURN -100;} \nELSE \n{VAR X = 21;  RETURN X; \nRETURN -100;} \nRETURN -100;";
        testInterpretationErrorReporting(redefinedVarWithUnreachableCodeStmt, 6, 1, 6, 11, ErrorCode.REDEFINED_VAR);
        testValidationErrorReporting(redefinedVarWithUnreachableCodeStmt, new ParsingError[]{
                    new ParsingError(3, 1, 3, 11, ErrorCode.REDEFINED_VAR, ""),
                    new ParsingError(4, 0, 4, 11, ErrorCode.UNREACHABLE_STMT, ""),
                    new ParsingError(6, 1, 6, 11, ErrorCode.REDEFINED_VAR, ""),
                    new ParsingError(7, 0, 7, 11, ErrorCode.UNREACHABLE_STMT, ""),
                    new ParsingError(8, 0, 8, 11, ErrorCode.UNREACHABLE_STMT, ""),});

        testValidationErrorReporting("IF (2!=2) IF (2==2) RETURN -1; ELSE RETURN -100; ELSE RETURN -2; \nRETURN -140;",
                2, 0, 2, 11, ErrorCode.UNREACHABLE_STMT);

        String complicatedIfStmt = "IF (2==2) { IF (2!=2) RETURN -1; RETURN -2;} ELSE RETURN -3; \nRETURN -100;";
        checkReturn(complicatedIfStmt, -2, false);
        testValidationErrorReporting(complicatedIfStmt, 2, 0, 2, 11, ErrorCode.UNREACHABLE_STMT);
        testErrorReporting("IF (2!=2) {VAR X= -92;} ELSE {VAR X = 21;}  \nRETURN X;", 2, 0, 2, 8, ErrorCode.UNDEFINED_VAR);

        checkReturn("VAR X = 2; IF (X) RETURN 1; ELSE RETURN -1;", 1);
        checkReturn("VAR X = 2; IF (X/0) RETURN 1; ELSE RETURN -1;", -1, false);
        checkReturn("IF (TRUE) RETURN 1; ELSE RETURN -1;", 1);
        checkReturn("IF (FALSE) RETURN 1; ELSE RETURN -1;", -1);
        checkReturn("VAR X = 2; IF (X==TRUE) RETURN 1; ELSE RETURN -1;", -1);
        checkReturn("VAR X = 2; IF (X/0==FALSE) RETURN 1; ELSE RETURN -1;", -1, false);

        checkReturn("IF ('aa') RETURN 2; ELSE RETURN -2;", 2);
        checkReturn("IF (2/0) RETURN 2; ELSE RETURN -2;", -2, false);
    }

    @Test
    public void testRandom() throws IOException {
        Value val = interpret("RETURN POISSON(2.5);").getResult();
        Assert.assertEquals(ValType.INTEGER, val.getType());
        checkReturn("RETURN POISSON('string');", Value.UNDEFINED, false);
        testWarningsReporting("RETURN POISSON('string');", 1, 0, 1, 24, ErrorCode.UNDEFINED_VALUE);
        checkReturn("RETURN POISSON(-2);", Value.UNDEFINED, false);
        testWarningsReporting("RETURN POISSON(-2);", 1, 0, 1, 18, ErrorCode.UNDEFINED_VALUE);

        val = interpret("RETURN UNIFORM(2, 10.9);").getResult();
        assertEquals(ValType.REAL, val.getType());
        checkReturn("RETURN UNIFORM('string', 2);", Value.UNDEFINED, false);
        testWarningsReporting("RETURN UNIFORM('string', 2);", 1, 0, 1, 27, ErrorCode.UNDEFINED_VALUE);
        checkReturn("RETURN UNIFORM(3, 2);", Value.UNDEFINED, false);
        testWarningsReporting("RETURN UNIFORM(3, 2);", 1, 0, 1, 20, ErrorCode.UNDEFINED_VALUE);

        val = interpret("RETURN EXPONENTIAL(10.9);").getResult();
        assertEquals(ValType.REAL, val.getType());
        checkReturn("RETURN EXPONENTIAL('string');", Value.UNDEFINED, false);
        testWarningsReporting("RETURN EXPONENTIAL('string');", 1, 0, 1, 28, ErrorCode.UNDEFINED_VALUE);
       
        val = interpret("RETURN NORMAL(2, 10.9);").getResult();
        assertEquals(ValType.REAL, val.getType());
        checkReturn("RETURN NORMAL('string', 2);", Value.UNDEFINED, false);
        testWarningsReporting("RETURN NORMAL('string', 2);", 1, 0, 1, 26, ErrorCode.UNDEFINED_VALUE);
        checkReturn("RETURN NORMAL(3, 0);", Value.UNDEFINED, false);
        testWarningsReporting("RETURN NORMAL(3, 0);", 1, 0, 1, 19, ErrorCode.UNDEFINED_VALUE);

        val = interpret("RETURN BINOMIAL(2, 0.4);").getResult();
        assertEquals(ValType.INTEGER, val.getType());
        val = interpret("RETURN BINOMIAL(0, 0.4);").getResult();
        assertEquals(ValType.INTEGER, val.getType());
        checkReturn("RETURN BINOMIAL(-2, 0.4);", Value.UNDEFINED, false);
        testWarningsReporting("RETURN BINOMIAL(-2, 0.4);", 1, 0, 1, 24, ErrorCode.UNDEFINED_VALUE);
        checkReturn("RETURN BINOMIAL('string', 0.2);", Value.UNDEFINED, false);
        testWarningsReporting("RETURN BINOMIAL('string', 0.2);", 1, 0, 1, 30, ErrorCode.UNDEFINED_VALUE);
        checkReturn("RETURN BINOMIAL(3.2, 0.4);", Value.UNDEFINED, false);
        testWarningsReporting("RETURN BINOMIAL(3.3, 0.4);", 1, 0, 1, 25, ErrorCode.UNDEFINED_VALUE);
        checkReturn("RETURN BINOMIAL(3.2, 4);", Value.UNDEFINED, false);
        testWarningsReporting("RETURN BINOMIAL(3.3, 4);", 1, 0, 1, 23, ErrorCode.UNDEFINED_VALUE);
        checkReturn("RETURN BINOMIAL(3.2, 0);", Value.UNDEFINED, false);
        testWarningsReporting("RETURN BINOMIAL(3.3, 0);", 1, 0, 1, 23, ErrorCode.UNDEFINED_VALUE);

        Value val1 = interpret("SEED(10000); RETURN NORMAL(1,10000000);").getResult();
        Value val2 = interpret("SEED(60000); RETURN NORMAL(1,10000000);").getResult();
        assertFalse(val1.equals(val2));
        val1 = interpret("SEED(100);RETURN NORMAL(1,10000000);").getResult();
        val2 = interpret("SEED(100);RETURN NORMAL(1,10000000);").getResult();
        assertEquals(val1, val2);
    }

    @Test(expected = InterpretationException.class)
    public void testModelAccessingWithoutAccessor() throws Exception {
        //no errors during validation
        testValidationErrorReporting("V[1000] = 145; RETURN V[1000];", new ParsingError[]{});
        //InterpretationException is thrown during interpretation
        interpret("VAR X= V[1000]; RETURN X;");
    }

    @Test
    public void testModelAccessing() throws Exception {
        ModelAccessor modelAccessor = createModelAccessor();
        checkReturn("RETURN V[1];", 145.3, modelAccessor);
        checkReturn("V[1] = 136; RETURN V[1];", 136, modelAccessor);
        assertEquals(new Value(136), modelAccessor.getVariable(1));

        checkReturn("RETURN V['var1'];", 22, modelAccessor);
        checkReturn("V['var1'] = 23; RETURN V['var1'];", 23L, modelAccessor);
        assertEquals(new Value(23), modelAccessor.getVariable("var1"));


        testErrorReporting("RETURN V[1000];", modelAccessor, 1, 0, 1, 14, ErrorCode.INVALID_REFERENCE);
        //dynamic definition of aggregate variables was not allowed for the root aggregate.
        testErrorReporting("V[1000] = 1;", modelAccessor, 1, 0, 1, 11, ErrorCode.INVALID_REFERENCE);
        assertEquals(null, modelAccessor.getVariable(1000));

        checkReturn("RETURN P['place1'].T['markAttr1'];", 100, modelAccessor);
        checkReturn("P['place1'].T['markAttr1'] = 200; RETURN P['place1'].T['markAttr1'];", 200L, modelAccessor);
        assertEquals(new Value(200), modelAccessor.getTokenAt("place1").getAttribute("markAttr1"));

        checkReturn("P['place1'].T['markAttr1'] = 'aa'; RETURN P['place1'].T['markAttr1'];", "aa", modelAccessor);
        assertEquals(new Value("aa"), modelAccessor.getTokenAt("place1").getAttribute("markAttr1"));

        testErrorReporting("RETURN P['place1'].\nT['markAttr2'];", modelAccessor, 1, 0, 2, 14, ErrorCode.INVALID_REFERENCE);
        //dynamic definition of mark's attributes was allowed by the StubMark class.
        checkReturn("P['place1'].T['markAttr2'] = 15;RETURN P['place1'].T['markAttr2'];", 15L, modelAccessor);
        assertEquals(new Value(15), modelAccessor.getTokenAt("place1").getAttribute("markAttr2"));

        testErrorReporting("RETURN P['place2'].\nT['markAttr4'];", modelAccessor, 1, 0, 2, 14, ErrorCode.INVALID_REFERENCE);
        //dynamic definition of mark's attributes was allowed by the StubMark class.
        checkReturn("P['place2'].T['markAttr4'] = 2.5; RETURN P['place2'].T['markAttr4'];", new BigDecimal(2.5), modelAccessor);
        assertEquals(new Value("2.5"), modelAccessor.getTokenAt("place2").getAttribute("markAttr4"));

        checkReturn("RETURN P['place3'].\nT['markAttr1'];", Value.UNDEFINED, false, modelAccessor);
        checkReturn("P['place3'].T['markAttr1'] = 23; RETURN P['place3'].T['markAttr1'];", Value.UNDEFINED, false, modelAccessor);
        //during validation all existing places must be marked to validate mark's attributes
        testValidationErrorReporting("RETURN P['place3'].\nT['markAttr1'];", modelAccessor, 1, 0, 2, 14, ErrorCode.INVALID_REFERENCE);
        testValidationErrorReporting("P['place3'].T['markAttr1'] = 23;", modelAccessor, 1, 0, 1, 31, ErrorCode.INVALID_REFERENCE);
        //interpreter produces undefined place warnings in such cases
        testInterpretationWarningsReporting("RETURN P['place3'].\nT['markAttr1'];", modelAccessor, 1, 0, 2, 14, ErrorCode.UNMARKED_PLACE);
        testInterpretationWarningsReporting("P['place3'].T['markAttr1'] = 23;", modelAccessor, 1, 0, 1, 31, ErrorCode.UNMARKED_PLACE);

        testErrorReporting("RETURN P['place45'].\nT['markAttr1'];", modelAccessor, 1, 0, 2, 14, ErrorCode.INVALID_REFERENCE);
        testErrorReporting("P['place45'].T['markAttr1'] = 23;", modelAccessor, 1, 0, 1, 32, ErrorCode.INVALID_REFERENCE);

        checkReturn("RETURN P['place1'].T;", true, modelAccessor);
        checkReturn("RETURN P['place2'].T;", true, modelAccessor);
        checkReturn("RETURN P['place3'].T;", false, modelAccessor);

        checkReturn("RETURN A['agg1'].V['nestedVar1'];", 500, modelAccessor);
        checkReturn("A['agg1'].V['nestedVar1'] = 13.2;RETURN A['agg1'].V['nestedVar1'];", 13.2, modelAccessor);
        assertEquals(new Value("13.2"), modelAccessor.getAggregate("agg1").getVariable("nestedVar1"));

        testErrorReporting("RETURN A['agg1'].V['nestedVar45'];", modelAccessor, 1, 0, 1, 33, ErrorCode.INVALID_REFERENCE);
        //dynamic definition of aggregate variables was allowed for the nested aggregates.
        checkReturn("A['agg1'].V['nestedVar45'] = TRUE; RETURN A['agg1'].V['nestedVar45'];", true, modelAccessor);
        assertEquals(Value.TRUE, modelAccessor.getAggregate("agg1").getVariable("nestedVar45"));

        testErrorReporting("RETURN A['agg45'].V['nestedVar1'];", modelAccessor, 1, 0, 1, 33, ErrorCode.INVALID_REFERENCE);
        testErrorReporting("A['agg45'].V['nestedVar1'] = 12;", modelAccessor, 1, 0, 1, 31, ErrorCode.INVALID_REFERENCE);

        checkReturn("RETURN A['agg1'].A['agg2'].P['p1'].T;", true, modelAccessor);
        checkReturn("RETURN A['agg1'].A['agg2'].P['p2'].T;", false, modelAccessor);
        checkReturn("RETURN A['agg1'].A['agg2'].P['p1'].T['a1'];", -100, modelAccessor);
        checkReturn("RETURN A['agg1'].A['agg2'].V['v1'];", 888, modelAccessor);
        testErrorReporting("RETURN A['agg1'].A['agg2'].P['p43'].T;", modelAccessor, 1, 0, 1, 37, ErrorCode.INVALID_REFERENCE);
        testErrorReporting("RETURN A['agg1'].A['agg2'].V[43];", modelAccessor, 1, 0, 1, 32, ErrorCode.INVALID_REFERENCE);
        testErrorReporting("RETURN A['agg1'].A['agg2'].P['p1'].T['a43'];", modelAccessor, 1, 0, 1, 43, ErrorCode.INVALID_REFERENCE);

        checkReturn("RETURN TIME;", 120, modelAccessor);
    }

    @Test
    public void testValuesRecognition() throws Exception {
        checkReturn("RETURN 1.5E-1;", 0.15);
        checkReturn("RETURN 1.5E1;", 15);
        checkReturn("RETURN 15E1;", new BigDecimal("1.5E+2"));
        checkReturn("RETURN 15E+1;", new BigDecimal("1.5E+2"));
        checkReturn("RETURN 15E0;", 15);
        checkReturn("RETURN -1.5E-1;", -0.15);
        checkReturn("RETURN -1.5E1;", -15);
        checkReturn("RETURN -15E1;", new BigDecimal("-0.015E+4"));
        checkReturn("RETURN -15E0;", -15);
        checkReturn("RETURN -15E+1;", new BigDecimal("-1.5E+2"));
    }

    @Test
    public void testCaseSensivity() throws Exception {
        interpreter = new EclInterpreter(false, false);
        checkReturn("VaR x = 10; ReTuRn x;", 10);

        interpreter = new EclInterpreter(false, true);
        testErrorReporting("VAR X = 10; ReTuRn;", 1, 12, 1, 18, ErrorCode.INVALID_STMT);
    }

    private ModelAccessor createModelAccessor() {
        Map<String, Object> variables = new HashMap<String, Object>();
        Set<String> places = new HashSet<String>();
        Map<String, EcliToken> marks = new HashMap<String, EcliToken>();
        Map<String, ModelAccessor> aggregates = new HashMap<String, ModelAccessor>();
        variables.put("1", 145.3);
        variables.put("var1", 22);
        places.add("place1");
        places.add("place2");
        places.add("place3");
        Map<String, Object> markAttributes = new HashMap<String, Object>();
        markAttributes.put("markAttr1", 100);
        EcliToken mark = new StubModelAccessor.StubMark(markAttributes);
        marks.put("place1", mark);
        marks.put("place2", new StubModelAccessor.StubMark());
        StubModelAccessor nestedAggregate = new StubModelAccessor(true);
        nestedAggregate.getVariables().put("nestedVar1", 500);
        aggregates.put("agg1", nestedAggregate);
        StubModelAccessor nestedAggregate2 = new StubModelAccessor(true);
        nestedAggregate2.getPlaces().add("p1");
        nestedAggregate2.getPlaces().add("p2");
        Map<String, Object> markAttributes2 = new HashMap<String, Object>();
        markAttributes2.put("a1", -100);
        EcliToken mark2 = new StubModelAccessor.StubMark(markAttributes2);
        nestedAggregate2.getMarks().put("p1", mark2);
        nestedAggregate2.getVariables().put("v1", 888);
        nestedAggregate.getAggregates().put("agg2", nestedAggregate2);
        return new StubModelAccessor(variables, places, marks, aggregates);

    }

    @Test
    public void testPow() throws Exception {
        checkReturn("RETURN 2^3*2;", 16);
        checkReturn("RETURN 2^1*2;", 4);
        checkReturn("RETURN 2^0*2;", 2);
        checkReturn("RETURN 0.5^2;", 0.25);
        checkReturn("RETURN 0.5^(-2);", 4);
        checkReturn("RETURN 0.5^-2;", 4);
        checkReturn("RETURN -2^2;", 4);
        checkReturn("RETURN -(2^2);", -4);
        checkReturn("RETURN -2^-2;", 0.25);
        checkReturn("RETURN --(-2^-2);", 0.25);
        checkReturn("RETURN 2^0.5*2;", new BigDecimal("2.8284271247461902"));
        checkReturn("RETURN 2^'aa';", Value.UNDEFINED, false);
        checkReturn("RETURN 'aa'^2;", Value.UNDEFINED, false);
        checkReturn("RETURN TRUE^2;", Value.UNDEFINED, false);
    }

    @Test
    public void testLn() throws Exception {
        checkReturn("RETURN LN(2*2);", new BigDecimal("1.3862943611198906"));
        checkReturn("RETURN LN(0.5);", new BigDecimal("-0.6931471805599453"));
        checkReturn("RETURN LN(-9);", Value.UNDEFINED, false);
        checkReturn("RETURN LN(0);", Value.UNDEFINED, false);
        checkReturn("RETURN LN('aa');", Value.UNDEFINED, false);
        checkReturn("RETURN LN(TRUE);", Value.UNDEFINED, false);
    }

    @Test
    public void testAbs() throws Exception {
        checkReturn("RETURN ABS(2);", 2);
        checkReturn("RETURN ABS(-0.5);", 0.5);
        checkReturn("RETURN ABS(FALSE);", Value.UNDEFINED, false);
        checkReturn("RETURN ABS('aa');", Value.UNDEFINED, false);
    }

    @Test
    public void testSqrt() throws Exception {
        checkReturn("RETURN SQRT(2*2);", 2);
        checkReturn("RETURN SQRT(0.5);", new BigDecimal("0.7071067811865476"));
        checkReturn("RETURN SQRT(0);", 0);
        checkReturn("RETURN SQRT(-9);", Value.UNDEFINED, false);
        checkReturn("RETURN SQRT('aa');", Value.UNDEFINED, false);
        checkReturn("RETURN SQRT(TRUE);", Value.UNDEFINED, false);
    }

    @Test
    public void testExp() throws Exception {
        checkReturn("RETURN E;", new BigDecimal("2.718281828459045"));
        checkReturn("RETURN E^-2;", new BigDecimal("0.1353352832366127"));
        checkReturn("RETURN E^2;", new BigDecimal("7.3890560989306495"));
    }

    @Test
    public void testSign() throws Exception {
        checkReturn("RETURN SIGN(2.5);", 1);
        checkReturn("RETURN SIGN(0);", 0);
        checkReturn("RETURN SIGN(-10);", -1);
        checkReturn("RETURN SIGN(FALSE);", Value.UNDEFINED, false);
        checkReturn("RETURN SIGN('aa');", Value.UNDEFINED, false);
    }

    @Test
    public void testEntier() throws Exception {
        checkReturn("RETURN ENTIER(2);", 2);
        checkReturn("RETURN ENTIER(0);", 0);
        checkReturn("RETURN ENTIER(-10);", -10);
        checkReturn("RETURN ENTIER(2.1);", 2);
        checkReturn("RETURN ENTIER(2.6);", 2);
        checkReturn("RETURN ENTIER(2.0);", 2);
        checkReturn("RETURN ENTIER(-10.1);", -11);
        checkReturn("RETURN ENTIER(-10.9);", -11);
        checkReturn("RETURN ENTIER(-10.0);", -10);
        checkReturn("RETURN ENTIER(FALSE);", Value.UNDEFINED, false);
        checkReturn("RETURN ENTIER('aa');", Value.UNDEFINED, false);
    }

    @Test
    public void testAtan() throws Exception {
        checkReturn("RETURN ATAN(2.5);", new BigDecimal("1.1902899496825317"));
        checkReturn("RETURN ATAN(0);", 0);
        checkReturn("RETURN ATAN(-10);", new BigDecimal("-1.4711276743037347"));
        checkReturn("RETURN ATAN(FALSE);", Value.UNDEFINED, false);
        checkReturn("RETURN ATAN('aa');", Value.UNDEFINED, false);
    }

    @Test
    public void testComment() throws Exception {
        checkReturn("//RETURN 2; RETURN -100; \nRETURN 6;", 6);
        checkReturn("RETURN \"//NOT_A_COMMENT\";", "//NOT_A_COMMENT");
        checkReturn("RETURN '//NOT_A_COMMENT';", "//NOT_A_COMMENT");
        checkReturn("//RETURN '//NOT_A_COMMENT'\nRETURN 1;", 1);
    }

    @Test
    public void testTypeConverstions() throws Exception {
        Value val = interpret("RETURN 2+2;").getResult();
        assertEquals(ValType.INTEGER, val.getType());
        assertEquals(4L, val.getValue());
        val = interpret("RETURN 2+2.2;").getResult();
        assertEquals(ValType.REAL, val.getType());
        assertEquals(new BigDecimal("4.2"), val.getValue());
        val = interpret("RETURN 2+'2.2';").getResult();
        assertEquals(ValType.STRING, val.getType());
        assertEquals("22.2", val.getValue());
        val = interpret("RETURN 2+TRUE;").getResult();
        assertEquals(ValType.UNDEFINED, val.getType());
        assertEquals(Value.UNDEFINED, val);
        val = interpret("RETURN TRUE+'2.2';").getResult();
        assertEquals(ValType.STRING, val.getType());
        assertEquals("TRUE2.2", val.getValue());
    }

    @Test
    public void testConcat() throws Exception {
        checkReturn("RETURN 2 + '45';", "245");
        checkReturn("RETURN 2/0 + '45';", "UNDEFINED45", false);
        checkReturn("RETURN TRUE + '45';", "TRUE45");
    }

    @Test
    public void testCurrentTokenUsage() throws Exception {
        StubModelAccessor modelAccessor = new StubModelAccessor(false);

        StubModelAccessor.StubMark mark = new StubModelAccessor.StubMark();
        mark.setAttribute("test", new Value(9));
        modelAccessor.setCurrentToken(mark);

        checkReturn("RETURN T['test'];", new Value(9), modelAccessor);
        checkReturn("T['test'] = 125; RETURN T['test'];", new Value(125), modelAccessor);
        assertEquals(new Value(125), mark.getAttribute("test"));


        modelAccessor.setCurrentToken(null);
        testInterpretationErrorReporting("RETURN T['test'];", modelAccessor, 1, 0, 1, 16, ErrorCode.INVALID_REFERENCE);
    }

    @Test
    public void testCurrentAggregateUsage() throws Exception {
        StubModelAccessor modelAccessor = new StubModelAccessor(false);
        StubModelAccessor currentAgg = new StubModelAccessor(false);
        currentAgg.getVariables().put("test", new Value(777));
        modelAccessor.setCurrentAgregate(currentAgg);
        modelAccessor.getVariables().put("test", new Value(888));

        checkReturn("RETURN V['test'];", new Value(777), modelAccessor);
        checkReturn("V['test'] = 125; RETURN V['test'];", new Value(125), modelAccessor);
        assertEquals(new Value(125), currentAgg.getVariable("test"));


        modelAccessor.setCurrentAgregate(null);
        testErrorReporting("RETURN V['test'];", modelAccessor, 1, 0, 1, 16, ErrorCode.INVALID_REFERENCE);
    }

    @Test
    public void testRootAggregateUsage() throws Exception {
        StubModelAccessor modelAccessor = new StubModelAccessor(false);
        StubModelAccessor currentAgg = new StubModelAccessor(false);
        currentAgg.getVariables().put("test", new Value(777));
        modelAccessor.setCurrentAgregate(currentAgg);
        modelAccessor.getVariables().put("test", new Value(888));

        checkReturn("RETURN ROOT.V['test'];", new Value(888), modelAccessor);
        checkReturn("ROOT.V['test'] = 125; RETURN ROOT.V['test'];", new Value(125), modelAccessor);
        assertEquals(new Value(125), modelAccessor.getVariable("test"));
    }

    private void assertErrorsFound(String input, int amount) throws IOException {

        ValidationReport report = validate(input);
        if (report.getParsingErrors().size() != amount) {
            System.out.println(report.getParsingErrors());
        }
        assertEquals(amount, report.getParsingErrors().size());
    }

    private void testErrorReporting(String input, ModelAccessor modelAccessor, int startLine, int startPos, int endLine, int endPos, ErrorCode errorCode) throws IOException {
        testValidationErrorReporting(input, modelAccessor, startLine, startPos, endLine, endPos, errorCode);
        testInterpretationErrorReporting(input, modelAccessor, startLine, startPos, endLine, endPos, errorCode);
    }

    private void testErrorReporting(String input, int startLine, int startPos, int endLine, int endPos, ErrorCode errorCode) throws IOException {
        testValidationErrorReporting(input, startLine, startPos, endLine, endPos, errorCode);
        testInterpretationErrorReporting(input, startLine, startPos, endLine, endPos, errorCode);
    }

    private void testWarningsReporting(String input, int startLine, int startPos, int endLine, int endPos, ErrorCode errorCode) throws IOException {
        testValidationErrorReporting(input, null, new ParsingError[]{new ParsingError(startLine, startPos, endLine, endPos, errorCode, "")});
        testInterpretationWarningsReporting(input, null, new ParsingError[]{new ParsingError(startLine, startPos, endLine, endPos, errorCode, "")});
    }

    private void testValidationErrorReporting(String input, int startLine, int startPos, int endLine, int endPos, ErrorCode errorCode) throws IOException {
        testValidationErrorReporting(input, null, new ParsingError[]{new ParsingError(startLine, startPos, endLine, endPos, errorCode, "")});
    }

    private void testValidationErrorReporting(String input, ModelAccessor modelAccessor, int startLine, int startPos, int endLine, int endPos, ErrorCode errorCode) throws IOException {
        testValidationErrorReporting(input, modelAccessor, new ParsingError[]{new ParsingError(startLine, startPos, endLine, endPos, errorCode, "")});
    }

    private void testInterpretationWarningsReporting(String input, ModelAccessor modelAccessor, int startLine, int startPos, int endLine, int endPos, ErrorCode errorCode) throws IOException {
        testInterpretationWarningsReporting(input, modelAccessor, new ParsingError[]{new ParsingError(startLine, startPos, endLine, endPos, errorCode, "")});
    }

    private void testWarningsReporting(String input, ModelAccessor modelAccessor, ParsingError[] expectedErrors) throws IOException {
        testValidationErrorReporting(input, modelAccessor, expectedErrors);
        testInterpretationWarningsReporting(input, modelAccessor, expectedErrors);
    }

    private void testWarningsReporting(String input, ParsingError[] expectedErrors) throws IOException {
        testValidationErrorReporting(input, null, expectedErrors);
        testInterpretationWarningsReporting(input, null, expectedErrors);
    }

    private void testValidationErrorReporting(String input, ParsingError[] expectedErrors) throws IOException {
        testValidationErrorReporting(input, null, expectedErrors);
    }

    private void testInterpretationWarningsReporting(String input, ModelAccessor modelAccessor, ParsingError[] expectedErrors) throws IOException {
        InterpretationReport report = interpret(input, modelAccessor);
        if (expectedErrors.length != report.getWarnings().size()) {
            System.out.println("Received errors: " + report.getWarnings());
        }
        assertEquals("Checking amount of errors found:", expectedErrors.length, report.getWarnings().size());
        for (int i = 0; i < expectedErrors.length; i++) {
            ParsingError peActual = report.getWarnings().get(i);
            ParsingError peExp = expectedErrors[i];
            assertEquals("Comparing start lines of warn#" + i, peExp.getStartLine(), peActual.getStartLine());
            assertEquals("Comparing start positions of warn#" + i, peExp.getStartPos(), peActual.getStartPos());
            assertEquals("Comparing end lines of warn#" + i, peExp.getEndLine(), peActual.getEndLine());
            assertEquals("Comparing end positions of warn#" + i, peExp.getEndPos(), peActual.getEndPos());
            assertEquals("Comparing error codes of warn#" + i, peExp.getErrorCode(), peActual.getErrorCode());
        }
    }

    private void testValidationErrorReporting(String input, ModelAccessor modelAccessor, ParsingError[] expectedErrors) throws IOException {
        ValidationReport report = validate(input, modelAccessor);
        if (expectedErrors.length != report.getParsingErrors().size()) {
            System.out.println("Received errors: " + report.getParsingErrors());
        }
        assertEquals("Checking amount of errors found:", expectedErrors.length, report.getParsingErrors().size());
        for (int i = 0; i < expectedErrors.length; i++) {
            ParsingError peActual = report.getParsingErrors().get(i);
            ParsingError peExp = expectedErrors[i];
            assertEquals("Comparing start lines of err#" + i, peExp.getStartLine(), peActual.getStartLine());
            assertEquals("Comparing start positions of err#" + i, peExp.getStartPos(), peActual.getStartPos());
            assertEquals("Comparing end lines of err#" + i, peExp.getEndLine(), peActual.getEndLine());
            assertEquals("Comparing end positions of err#" + i, peExp.getEndPos(), peActual.getEndPos());
            assertEquals("Comparing error codes of err#" + i, peExp.getErrorCode(), peActual.getErrorCode());
        }
    }

    private void testInterpretationErrorReporting(String input, int startLine, int startPos, int endLine, int endPos, ErrorCode errorCode) throws IOException {
        testInterpretationErrorReporting(input, null, startLine, startPos, endLine, endPos, errorCode);
    }

    private void testInterpretationErrorReporting(String input, ModelAccessor modelAccessor, int startLine, int startPos, int endLine, int endPos, ErrorCode errorCode) throws IOException {
        boolean catched = false;
        try {
            interpret(input, modelAccessor);
        } catch (ValidationException e) {
            ParsingError pe = e.getParsingError();
            assertEquals(startLine, pe.getStartLine());
            assertEquals(startPos, pe.getStartPos());
            assertEquals(endLine, pe.getEndLine());
            assertEquals(endPos, pe.getEndPos());
            assertEquals(errorCode, pe.getErrorCode());
            catched = true;
        }
        assertTrue(catched);
    }

    private void checkReturn(String input, Object expected) throws IOException, ValidationException {
        checkReturn(input, expected, true, null);
    }

    private void checkReturn(String input, Object expected, boolean validate) throws IOException, ValidationException {
        checkReturn(input, expected, validate, null);
    }

    private void checkReturn(String input, Object expected, ModelAccessor modelAccessor) throws IOException, ValidationException {
        checkReturn(input, expected, true, modelAccessor);
    }

    private void checkReturn(String input, Object expected, boolean validate, ModelAccessor modelAccessor) throws IOException, ValidationException {
        if (validate) {
            ValidationReport report = validate(input, modelAccessor);
            if (report.getParsingErrors().size() > 0) {
                System.out.println(report.getParsingErrors());
            }
            assertEquals(0, report.getParsingErrors().size());
        }
        InterpretationReport report = interpret(input, modelAccessor);
        if (validate) {
            assertEquals(0, report.getWarnings().size());
        }
        if (null != expected) {
            assertEquals(expected.toString(), report.getResult().getValue().toString());
        } else {
            assertEquals(null, report.getResult());
        }
    }

    private InterpretationReport interpret(String input) throws IOException {
        return interpret(input, null);
    }

    private InterpretationReport interpret(String input, ModelAccessor modelAccessor) throws IOException {
        return interpreter.interpret(new ByteArrayInputStream(input.getBytes("UTF8")), modelAccessor);
    }

    private ValidationReport validate(String input, ModelAccessor modelAccessor) throws IOException {
        return interpreter.validate(new ByteArrayInputStream(input.getBytes("UTF8")), modelAccessor);
    }

    private ValidationReport validate(String input) throws IOException {
        return interpreter.validate(new ByteArrayInputStream(input.getBytes("UTF8")));
    }

    private static class StubModelAccessor implements ModelAccessor {

        private Map<String, Object> variables = new HashMap<String, Object>();
        private Set<String> places = new HashSet<String>();
        private Map<String, EcliToken> marks = new HashMap<String, EcliToken>();
        private Map<String, ModelAccessor> aggregates = new HashMap<String, ModelAccessor>();
        private boolean definitionAllowed;
        private EcliToken currentToken;
        private EcliAggregate currentAggregate = this;

        private StubModelAccessor(boolean definitionAllowed) {
            this.definitionAllowed = definitionAllowed;
        }

        private StubModelAccessor(Map<String, Object> variables, Set<String> places, Map<String, EcliToken> marks, Map<String, ModelAccessor> aggregates) {
            this.variables = variables;
            this.places = places;
            this.marks = marks;
            this.aggregates = aggregates;
        }

        public Map<String, Object> getVariables() {
            return variables;
        }

        public Set<String> getPlaces() {
            return places;
        }

        public Map<String, EcliToken> getMarks() {
            return marks;
        }

        public Map<String, ModelAccessor> getAggregates() {
            return aggregates;
        }

        public boolean containsVar(Object name) {
            return variables.containsKey(name.toString());
        }

        public boolean containsPlace(Object place) {
            return places.contains(place.toString());
        }

        public boolean isPlaceMarked(Object place) {
            return places.contains(place.toString()) && marks.containsKey(place.toString());
        }

        public boolean containsAggregate(Object aggregate) {
            return aggregates.containsKey(aggregate.toString());
        }

        public Object getVariable(Object name) {
            return variables.get(name.toString());
        }

        public boolean setVariable(Object key, Value value) {
            if (definitionAllowed || containsVar(key)) {
                variables.put(key.toString(), value);
                return true;
            }
            return false;
        }

        public EcliToken getTokenAt(Object place) {
            EcliToken mark = marks.get(place.toString());
            if (null == mark) {
                mark = new StubMark();
                marks.put(place.toString(), mark);
            }
            return mark;
        }

        public ModelAccessor getAggregate(Object aggregate) {
            return aggregates.get(aggregate.toString());
        }

        public double getTime() {
            return 120;
        }

        public EcliToken getCurrentToken() {
            return currentToken;
        }

        public EcliAggregate getCurrentAggregate() {
            return currentAggregate;
        }

        public EcliAggregate getRootAggregate() {
            return this;
        }

        public void setCurrentToken(EcliToken currentToken) {
            this.currentToken = currentToken;
        }

        public void setCurrentAgregate(EcliAggregate currentAggregate) {
            this.currentAggregate = currentAggregate;
        }

        private static class StubMark implements EcliToken {

            private Map<String, Object> attributes = new HashMap<String, Object>();

            private StubMark() {
            }

            private StubMark(Map<String, Object> attributes) {
                this.attributes = attributes;
            }

            public Object getAttribute(Object key) {
                return attributes.get(key.toString());
            }

            public boolean containsAttribute(Object key) {
                return attributes.containsKey(key.toString());
            }

            public boolean setAttribute(Object key, Value value) {
                attributes.put(key.toString(), value);
                return true;
            }
        }
    }
}
