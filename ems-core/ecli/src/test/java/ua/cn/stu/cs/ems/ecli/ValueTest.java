package ua.cn.stu.cs.ems.ecli;

import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

/**
 * @author n0weak
 */
public class ValueTest {

    @Test
    public void testAdditionAndTypeConversion() {
        Value var1 = new Value(2);
        Assert.assertEquals(ValType.INTEGER, var1.getType());
        Value var2 = new Value(2.5);
        assertEquals(ValType.REAL, var2.getType());
        var1 = Calculator.add(var1, var2);
        assertEquals(ValType.REAL, var1.getType());
        assertEquals("4.5", var1.getValue().toString());
        var2.setValue("string!");
        var1 = Calculator.add(var1, var2);
        assertEquals(ValType.STRING, var1.getType());
        assertEquals("4.5string!", var1.getValue());
        var1.setValue(55);
        assertEquals(ValType.INTEGER, var1.getType());
        var2 = Calculator.add(var2, var1);
        assertEquals(ValType.STRING, var2.getType());
        assertEquals("string!55", var2.getValue());
        var2.setValue(3.4);
        assertEquals(ValType.REAL, var2.getType());
        var1.setValue("55.0");
        assertEquals(ValType.INTEGER, var1.getType());
    }

    @Test
    public void testTypeConversion() {
        Value var1 = new Value();
        var1.setValue("2");
        assertEquals(ValType.INTEGER, var1.getType());
        var1.setValue("2.2");
        assertEquals(ValType.REAL, var1.getType());
        var1.setValue("2.2a");
        assertEquals(ValType.STRING, var1.getType());
        var1.setValue(Integer.MAX_VALUE + 1l);
        assertEquals(ValType.INTEGER, var1.getType());
        var1.setValue(new BigDecimal(Long.MAX_VALUE).add(BigDecimal.ONE).toString());
        assertEquals(ValType.REAL, var1.getType());
    }

    @Test
    public void testUndefinedValues() {
        Value var1 = new Value();
        assertEquals(Value.UNDEFINED, var1);
        Value var2 = new Value(2);
        assertFalse(var2.equals(var1));
        var2 = Calculator.add(var1, var2);
        assertEquals(Value.UNDEFINED, var2);
        var2 = new Value(2);
        var2 = Calculator.multiply(var2, null);
        assertEquals(Value.UNDEFINED, var2);
    }

    @Test
    public void testNumberConversions() {
        Value var1 = new Value("9999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999");
        assertNull(var1.doubleValue());
        assertNull(var1.longValue());
        var1.setValue(9999999999999999999d);
        assertEquals(new Double(9999999999999999999d), var1.doubleValue());
        assertNull(var1.longValue());
        var1.setValue(999999999);
        assertEquals(new Double(999999999d), var1.doubleValue());
        assertEquals("999999999", var1.longValue().toString());
    }

    @Test
    public void testEquality() {
        assertTrue(new Value("2").equals(new Value(2)));
        assertTrue(new Value("2.0").equals(new Value(2)));
        assertFalse("string value initialized with 'true' must not be equal to bool value initialized with java's boolean true"
                , new Value("true").equals(new Value(true)));
    }

    @Test
    public void testMatrioshkaHandling() {
        Value val1 = new Value(new Value(22));
        assertEquals(ValType.INTEGER, val1.getType());
        assertEquals(22L, val1.getValue());
        assertTrue(val1.getValue() instanceof Long);
    }
}
