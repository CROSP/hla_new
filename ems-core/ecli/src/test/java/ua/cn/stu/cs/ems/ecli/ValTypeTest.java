package ua.cn.stu.cs.ems.ecli;

import ua.cn.stu.cs.ems.ecli.Value;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static ua.cn.stu.cs.ems.ecli.ValType.*;

/**
 * @author n0weak
 */
public class ValTypeTest {

    @Test
    public void testCommonTypesDefinition() {
        Value a = new Value("2", INTEGER);
        Value b = new Value("2.2", REAL);
        assertEquals("common type for integer and real must be real", REAL, getCommonType(a, b));

        a = new Value("2", STRING);
        b = new Value("2.2", REAL);
        assertEquals("common type for string and real must be string", STRING, getCommonType(a, b));

        a = new Value("2", INTEGER);
        b = new Value("2.2", STRING);
        assertEquals("common type for integer and string must be string", STRING, ValType.getCommonType(a, b));

        a = new Value("2", INTEGER);
        b = new Value(true, BOOL);
        assertEquals("common type for integer and bool must be string", STRING, ValType.getCommonType(a, b));

        a = new Value("2", REAL);
        b = new Value(true, BOOL);
        assertEquals("common type for real and bool must be string", STRING, ValType.getCommonType(a, b));

        a = new Value("2", STRING);
        b = new Value(true, BOOL);
        assertEquals("common type for string and bool must be string", STRING, ValType.getCommonType(a, b));

        a = new Value(true, BOOL);
        b = new Value(true, BOOL);
        assertEquals("common type for bool and bool must be bool", BOOL, ValType.getCommonType(a, b));

        a = new Value(null, UNDEFINED);
        b = new Value(true, BOOL);
        assertEquals("common type for undefined and bool must be string", STRING, ValType.getCommonType(a, b));

        a = new Value(null, UNDEFINED);
        b = new Value("2.2", STRING);
        assertEquals("common type for undefined and string must be string", STRING, ValType.getCommonType(a, b));

        a = new Value(null, UNDEFINED);
        b = new Value("2.2", REAL);
        assertEquals("common type for undefined and real must be string", STRING, ValType.getCommonType(a, b));

        a = new Value(null, UNDEFINED);
        b = new Value("2", INTEGER);
        assertEquals("common type for undefined and integer must be string", STRING, ValType.getCommonType(a, b));
    }
}
