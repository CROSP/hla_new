package ua.cn.stu.cs.ems.ecli;

import ua.cn.stu.cs.ems.ecli.Calculator;
import ua.cn.stu.cs.ems.ecli.Value;
import ua.cn.stu.cs.ems.ecli.ValType;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author n0weak
 */
public class CalculatorTest {

    @Test
    public void testAdd() {
        Value a = new Value(2, ValType.STRING);
        Value b = new Value(3);
        Value c = Calculator.add(a, b);
        assertEquals("addition of integer with explicitly defined string must return string", ValType.STRING, c.getType());
        assertEquals("addition of integer with explicitly defined string must return string", "23", c.getValue());

        a = new Value("2");
        b = new Value(3);
        c = Calculator.add(a, b);
        assertEquals("addition of integer with dynamically defined integer must return integer", ValType.INTEGER, c.getType());
        assertEquals("addition of integer with dynamically defined integer must return integer", 5l, c.getValue());

        a = new Value(2.5);
        b = new Value(3);
        c = Calculator.add(a, b);
        assertEquals("addition of integer with real must return real", ValType.REAL, c.getType());
        assertEquals("addition of integer with real must return real", new BigDecimal(5.5), c.getValue());

        assertEquals("addition of with null must return undefined value", Value.UNDEFINED, Calculator.add(null, new Value(2)));
    }

    @Test
    public void testConcat() {
        Value a = new Value(2);
        Value b = new Value(3);
        Value c = Calculator.concat(a, b);
        assertEquals("concatenation of two integer values must result in pre-defined string", ValType.STRING, c.getType());
        assertEquals("concatenation of two integer values must result in pre-defined string", "23", c.getValue());
        assertTrue("concatenation of two integer values must result in pre-defined string", c.isTypePredefined());

        a = new Value(2.5);
        b = new Value(3);
        c = Calculator.concat(a, b);
        assertEquals("concatenation of integer and double values must result in pre-defined string", ValType.STRING, c.getType());
        assertEquals("concatenation of integer and double values must result in pre-defined string", "2.53", c.getValue());
        assertTrue("concatenation of integer and double values must result in pre-defined string", c.isTypePredefined());

        a = Value.UNDEFINED;
        b = new Value(3);
        c = Calculator.concat(a, b);
        assertEquals("concatenation with undefined in pre-defined string with 'UNDEFINED' representation of undef value",
                ValType.STRING, c.getType());
        assertEquals("concatenation with undefined in pre-defined string with 'UNDEFINED' representation of undef value",
                "UNDEFINED3", c.getValue());
        assertTrue("concatenation with undefined in pre-defined string with 'UNDEFINED' representation of undef value",
                c.isTypePredefined());
    }

    @Test
    public void testComparison() {
        Value a = new Value(2.0003);
        Value b = new Value(2);
        assertEquals(Value.TRUE, Calculator.ne(a, b));
        assertEquals(Value.FALSE, Calculator.eq(a, b));
        assertEquals(Value.TRUE, Calculator.gt(a, b));
        assertEquals(Value.FALSE, Calculator.lt(a, b));
        assertEquals(Value.FALSE, Calculator.le(a, b));
        assertEquals(Value.TRUE, Calculator.ge(a, b));

        a = new Value("2");
        b = new Value(2);
        assertEquals(Value.FALSE, Calculator.ne(a, b));
        assertEquals(Value.TRUE, Calculator.eq(a, b));
        assertEquals(Value.FALSE, Calculator.gt(a, b));
        assertEquals(Value.FALSE, Calculator.lt(a, b));
        assertEquals(Value.TRUE, Calculator.le(a, b));
        assertEquals(Value.TRUE, Calculator.ge(a, b));


        a = new Value("2");
        b = Value.UNDEFINED;

        assertEquals(Value.UNDEFINED, Calculator.ne(a, b));
        assertEquals(Value.UNDEFINED, Calculator.eq(a, b));
        assertEquals(Value.UNDEFINED, Calculator.gt(a, b));
        assertEquals(Value.UNDEFINED, Calculator.lt(a, b));
        assertEquals(Value.UNDEFINED, Calculator.le(a, b));
        assertEquals(Value.UNDEFINED, Calculator.ge(a, b));

        a = Value.UNDEFINED;
        b = Value.UNDEFINED;

        assertEquals(Value.FALSE, Calculator.ne(a, b));
        assertEquals(Value.TRUE, Calculator.eq(a, b));
        assertEquals(Value.FALSE, Calculator.gt(a, b));
        assertEquals(Value.FALSE, Calculator.lt(a, b));
        assertEquals(Value.TRUE, Calculator.le(a, b));
        assertEquals(Value.TRUE, Calculator.ge(a, b));

        a = Value.TRUE;
        b = Value.FALSE;

        assertEquals(Value.TRUE, Calculator.ne(a, b));
        assertEquals(Value.FALSE, Calculator.eq(a, b));
        assertEquals(Value.TRUE, Calculator.gt(a, b));
        assertEquals(Value.FALSE, Calculator.lt(a, b));
        assertEquals(Value.FALSE, Calculator.le(a, b));
        assertEquals(Value.TRUE, Calculator.ge(a, b));

        a = new Value(22);
        b = new Value("22bb");

        assertEquals(Value.TRUE, Calculator.ne(a, b));
        assertEquals(Value.FALSE, Calculator.eq(a, b));
        assertEquals(Value.FALSE, Calculator.gt(a, b));
        assertEquals(Value.TRUE, Calculator.lt(a, b));
        assertEquals(Value.TRUE, Calculator.le(a, b));
        assertEquals(Value.FALSE, Calculator.ge(a, b));
    }

    @Test
    public void testInvert() {
        assertEquals(new Value(-2), Calculator.invert(new Value(2)));
        assertEquals(new Value(2.0), Calculator.invert(new Value(-2.0)));
        assertEquals(Value.FALSE, Calculator.invert(Value.TRUE));
        assertEquals(Value.TRUE, Calculator.invert(Value.FALSE));
        assertEquals(new Value("string"), Calculator.invert(new Value("string")));
        assertEquals(Value.UNDEFINED, Calculator.invert(Value.UNDEFINED));
    }

    @Test
    public void testAnd() {
        assertEquals(Value.TRUE, Calculator.and(Value.TRUE, Value.TRUE));
        assertEquals(Value.FALSE, Calculator.and(Value.TRUE, Value.FALSE));
        assertEquals(Value.FALSE, Calculator.and(Value.FALSE, Value.FALSE));
        assertEquals(Value.UNDEFINED, Calculator.and(Value.TRUE, new Value(2)));
    }

    @Test
    public void testOr() {
        assertEquals(Value.TRUE, Calculator.or(Value.TRUE, Value.TRUE));
        assertEquals(Value.TRUE, Calculator.or(Value.TRUE, Value.FALSE));
        assertEquals(Value.FALSE, Calculator.or(Value.FALSE, Value.FALSE));
        assertEquals(Value.UNDEFINED, Calculator.or(Value.TRUE, new Value(2)));
    }

    @Test
    public void testDivAndMul() {
        double d = 1/3;
        assertEquals(0.0, d*3);
        Value val = Calculator.divide(new Value(1), new Value(3));
        assertEquals("0.9999999999999999", Calculator.multiply(val, new Value(3)).toString());
    }

}
