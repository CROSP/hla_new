grammar ECL;

options {output=AST;} // build trees

tokens {
	VAR_DECL;
	IF_STMT;
	IF_BEGIN;
	IF_END;
	STMT_BEGIN;
	STMT_END;
	INVERT;
	BLOCK;
	IS_MARKED;
	CURRENT_MARK;
}

@header{
    	package ua.cn.stu.cs.ems.ecli.impl;
		import ua.cn.stu.cs.ems.ecli.errors.*;
}


@lexer::header{
    	package ua.cn.stu.cs.ems.ecli.impl;
		import ua.cn.stu.cs.ems.ecli.errors.*;
}

@members{
	private List<RecognitionErrorListener> errorListeners = new ArrayList<RecognitionErrorListener>();

	public void addErrorListener(RecognitionErrorListener listener) {
	    errorListeners.add(listener);
    }

    public void removeErrorListener(RecognitionErrorListener listener) {
	    errorListeners.remove(listener);
    }

    public void fireRecognitionError(RecognitionException e) {
        for (RecognitionErrorListener listener : errorListeners) {
            listener.errorHappened(e, this);
        }
    }

	public void displayRecognitionError(String[] tokenNames, RecognitionException e) {
		fireRecognitionError(correctException(e));
	}


	/**
	 *	Method that makes exception processing in order to insert some specific data or substitute
	 *  default exceptions on more suitable ones;
	 **/
	private RecognitionException correctException(RecognitionException e) {
		//if error is caused be unexpected EOF, then we have to substitute  MismatchedTokenException
		//with UnexpectedEOFException
		if (e instanceof MismatchedTokenException) {
			if (e.token.getType()==Token.EOF) {
				e = new UnexpectedEOFException((MismatchedTokenException) e);
			}
		}
		return e;
	}
}

@lexer::members{
	private List<RecognitionErrorListener> errorListeners = new ArrayList<RecognitionErrorListener>();

	public void addErrorListener(RecognitionErrorListener listener) {
	    errorListeners.add(listener);
    }

    public void removeErrorListener(RecognitionErrorListener listener) {
	    errorListeners.remove(listener);
    }

    public void fireRecognitionError(RecognitionException e) {
        for (RecognitionErrorListener listener : errorListeners) {
            listener.errorHappened(e, this);
        }
    }

	public void displayRecognitionError(String[] tokenNames, RecognitionException e) {
		fireRecognitionError(e);	
	}

}

@rulecatch {
	catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
	}
}

/*----------------
* PARSER RULES
*----------------*/

ecl			:	statement+
			;

statement   :   stmt end=';' -> (STMT_BEGIN[$start] stmt STMT_END[$end])?
			|	if_stmt		 -> (IF_BEGIN if_stmt IF_END)?
            ;

stmt		: 	(assign) => assign
			|	standalone_expr //will be forbidden by semantic checks performed by tree walkers
			|	declar
			|   ret
			|	seed_stmt
			;
			
seed_stmt	:	SEED '(' expr ')' -> ^(SEED expr)
			;
			 
standalone_expr 
			:	expr
			;

declar		:	VAR id	-> ^(VAR_DECL id)
		    |	VAR id ASSIGN expr -> ^(VAR_DECL id expr)
		    ;

ret			:   RETURN expr -> ^(RETURN expr)
			|	RETURN
            ;
			
if_stmt	
options {backtrack=true;}
			:	IF '(' expr ')' if_block ELSE if_block -> ^(IF_STMT expr if_block if_block) 
			|	IF '(' expr ')' if_block ->  ^(IF_STMT expr if_block)
			;
			
if_block	: statement 
			| statement_block
			;
			
statement_block
			:	'{' statement* '}' -> ^(BLOCK statement*)
			;

assign		:	id ASSIGN expr -> ^(ASSIGN id expr)
			|	sys_id ASSIGN expr -> ^(ASSIGN sys_id expr)
			;

expr		:	prec5
			;
			
prec5		:	prec4 (
				(LAND^ | LOR^)
				prec4
				)?
			;
			
prec4		:	prec3 (
				( GT^ |	LT^ |	LE^	|	GE^ |	EQ^ 	|	NE^	)
				prec3
				)?
			;

prec3 		: 	prec2
			|	rand
			|	ENTIER '(' prec2 ')' -> ^(ENTIER prec2)
			|	SIGN '(' prec2 ')' -> ^(SIGN prec2)
			|	SQRT '(' prec2 ')' -> ^(SQRT prec2)
			|	ABS	'('	prec2 ')' -> ^(ABS prec2)
			|	LN	'('	prec2 ')' -> ^(LN prec2)
			|	SIN '(' prec2 ')' -> ^(SIN prec2)
			|	COS '(' prec2 ')' -> ^(COS prec2)
			|	TAN '(' prec2 ')' -> ^(TAN prec2)
			|	ATAN '(' prec2 ')' -> ^(ATAN prec2)
			|	COT '(' prec2 ')' -> ^(COT prec2)
			;

prec2		:	prec1 (PLUS^ prec1 | MINUS^ prec1)*
			;

prec1		:	pow (MULT^ pow | DIV^ pow)*
			;

pow			:	prec0 (POW^ pow)?
			;			
			
prec0		:	atom
			|	(MINUS | NOT) prec0 -> ^(INVERT prec0)
			;

			
atom		:	INT
			|	REAL
			|	id
			| 	'(' expr ')' -> expr
			|	STR
			|   TRUE
			|   FALSE
			|	EXP
			|	sys_atom
			;
			
id			:	ID
			|	MALFORMED_ID
			;
			
rand		:	UNIFORM '(' expr ',' expr ')' -> ^(UNIFORM expr expr)	
			|	POISSON '(' expr ')' -> ^(POISSON expr)
			|	EXPONENTIAL '(' expr ')' -> ^(EXPONENTIAL expr)
			|	NORMAL '(' expr ',' expr ')' -> ^(NORMAL expr expr)
			|	BINOMIAL '(' expr ',' expr ')' -> ^(BINOMIAL expr expr)
			;
			
sys_atom	:	(sys_id) => sys_id
			|	aggregate? PLACE '[' expr ']' '.' MARK  -> ^(IS_MARKED aggregate? expr)
			|	TIME
			;
			
sys_id		:	aggregate? (
					SYS_VAR '[' expr ']' -> ^(SYS_VAR aggregate? expr)
					|	
					PLACE '[' expr ']' '.' MARK '[' expr ']' -> ^(MARK aggregate? expr expr)
				)
			|	MARK '[' expr ']' -> ^(CURRENT_MARK expr)
			;
			
			
			
aggregate	:	non_root_agg+
			|	ROOT_AGG '.'! non_root_agg*
			;
			
non_root_agg	
			:	AGGREGATE '['! expr ']'! '.'!
			;
			
/*----------------
* LEXER RULES
*----------------*/

COMMENT	:	'//' ( ~('\r' | '\n') )* '\n'? {$channel = HIDDEN;}
		;

fragment LETTER
		:	('a'..'z'|'A'..'Z')+;
fragment DIGIT
		:	'0'..'9';
fragment
ESC
    :   '\\'
        (       'n'    {setText("\n");}
        |       'r'    {setText("\r");}
        |       't'    {setText("\t");}
        |       'b'    {setText("\b");}
        |       'f'    {setText("\f");}
        |       '"'    {setText("\"");}
        |       '\''   {setText("\'");}
        |       '/'    {setText("/");}
        |       '\\'   {setText("\\");}
        )
    ;

		
STR          
@init{StringBuilder lBuf = new StringBuilder();}
		:   
           '"'
           (
				escaped=ESC {lBuf.append(getText());} 
				| 
				normal=~('\\'|'"'|'\n'|'\r') {lBuf.appendCodePoint(normal);}
			)* 
           '"'    
           {setText(lBuf.toString());}
		|   
           '\''
			(
				escaped=ESC {lBuf.append(getText());} 
				| 
				normal=~('\\'|'\''|'\n'|'\r') {lBuf.appendCodePoint(normal);} 
			)* 
           '\''    
           {setText(lBuf.toString());}
		;

ENTIER	:	'ENTIER';		
SIGN	:	'SIGN';
EXP		:	'E';		
SQRT	:	'SQRT';		
ABS		:	'ABS';
LN		:	'LN';
POW		:	'^';		
TIME	:	'TIME';
ROOT_AGG
		:	'ROOT';
AGGREGATE
		:	'A';
PLACE	:	'P';
MARK	:	'T';
SYS_VAR
		:	'V';
POISSON	:	'POISSON';
UNIFORM	:	'UNIFORM';
EXPONENTIAL
		:	'EXPONENTIAL';
NORMAL	:	'NORMAL';
BINOMIAL
		:	'BINOMIAL';
SEED	:	'SEED';
TRUE    :   'TRUE';
FALSE   :   'FALSE';
IF		: 	'IF';
ELSE	:	'ELSE';
LOR		:	'||';
LAND	:	'&&';
NOT		:	'!';
EQ		:	'==';
NE		:	'!=';
GE		:	'>=';
LE		:	'<=';
GT		:	'>';
LT		:	'<';		
VAR		:	'VAR'; 
SIN		:	'SIN';
COS		:	'COS';
ATAN		
		:	'ATAN';
TAN		:	'TAN';
COT		:	'COT';
PLUS 	: 	'+';
MINUS	: 	'-';
MULT	: 	'*';
DIV		: 	'/';
ASSIGN	:	 '=';
RETURN	:	'RETURN';
INT		:	DIGIT+;
REAL	:	DIGIT+ '.' DIGIT+ | DIGIT+ 'E' ('-' | '+')? DIGIT+ | DIGIT+ '.' DIGIT+ 'E' ('-' | '+')? DIGIT+;
ID 		:	(LETTER | '_') (LETTER | DIGIT | '_')*;
MALFORMED_ID 	//will be forbidden by semantic checks performed by tree walkers, grammar-based prohibition makes error generation too noisy
		: (LETTER | DIGIT | '_') (LETTER | DIGIT | '_')*;
WS 		:	(' '|'\t'|'\n'|'\r')+	{$channel = HIDDEN;};