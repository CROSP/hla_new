tree grammar EclTreeWalker;

options {
		tokenVocab=ECL; // import tokens from ECL.g
		ASTLabelType=CommonTree;
}

@header{
    	package ua.cn.stu.cs.ems.ecli.impl;

	    import java.lang.Math;
	    import ua.cn.stu.cs.ems.ecli.*;
	    import ua.cn.stu.cs.ems.ecli.errors.*;
	    import java.util.List;
	    import java.util.ArrayList;
	    import java.util.Set;
	    import java.util.HashSet;
	    import java.lang.StringBuilder;
}

@members{
	private Heap heap = new Heap();
	private EclRandomData randomData;
	private ModelAccessor modelAccessor;
	private List<RecognitionErrorListener> warningListeners = new ArrayList<RecognitionErrorListener>();
	
	/**
	 * Indicates whether current token is contained inside assign statement.
	 */
	private boolean inAssign = false;
	
	/**
     * list of ids that have already appeared in errors reporting, no need to report about 'em again
     */	 
	private Set<Object> reportedIds = new HashSet<Object>();  
	
	/**
	 * Indicates whether walker must validate input or interpret it.
	 */
	private boolean validating = false;

	/**
	 * Represents number of current token in current statement, counting starts from 1.
	 */
	private int tokenNo = 0;
	
	/** 
	 * Interpretation result, expression from 'RETURN' statement 
	 * @see ua.cn.stu.cs.jess.ecli.InterpretationReport#getResult() 
	 */
	private Value retVal;
	
	/** Defines whether interpreter must make evaluation, i.e. execute rule actions. Used by if-the-else statement during interpretation. **/
	private boolean evaluate = true;
	
	/**
     * @see ua.cn.stu.cs.jess.ecli.ValidationReport#returnsValue()
     */
	private boolean returnsValue;

	/*
	 * Indicates whether statements that are being currently processed are specified after 'RETURN' statement,
	 * hence they are unreachable.
	 * Used during validation.
	 */
    private boolean afterReturn;
	
	public EclTreeWalker(TreeNodeStream input, ModelAccessor modelAccessor, EclRandomData randomData) {
		this(input);
		this.modelAccessor = modelAccessor;
		this.randomData = randomData;
	}
	

	public Value getRetVal() {
		return retVal;
	}

	/**
     * @see ua.cn.stu.cs.jess.ecli.ValidationReport#returnsValue()
     */
	public boolean returnsValue() {
		return returnsValue;
	}

	public void addWarningListener(RecognitionErrorListener listener) {
	    warningListeners.add(listener);
    }

    public void warningErrorListener(RecognitionErrorListener listener) {
	    warningListeners.remove(listener);
    }

    public void warn(RecognitionException e) {
        for (RecognitionErrorListener listener : warningListeners) {
            listener.errorHappened(e, this);
        }
    }


	public void displayRecognitionError(String[] tokenNames, RecognitionException e){
	    if(validating) {
		    warn(e);
		} else {
		    super.displayRecognitionError(tokenNames, e);
		}
	}

	public Object recoverFromMismatchedSet(IntStream input, RecognitionException e, BitSet follow) 
					throws RecognitionException{
        if (validating) {
            return super.recoverFromMismatchedSet(input, e, follow);
        } else {
            throw e;
        }
	}
	
	public Object match(IntStream input, int ttype, BitSet follow) throws RecognitionException {
		tokenNo++;
		return super.match(input, ttype, follow);		
	}

	protected Object recoverFromMismatchedToken(IntStream input, int ttype, BitSet follow)
					throws RecognitionException {
        if (validating) {
            return super.recoverFromMismatchedToken(input, ttype, follow);
        } else {
            throw new MismatchedTokenException(ttype, input);
        }
	}
	
	private void reportUndefinedRef(OuterRef.RefDecorator ref) throws ECLSemanticException {
		switch (ref.getType()) {
			case VAR:
				handleError(new ECLSemanticException("MissingVariable", ErrorCode.INVALID_REFERENCE, ref.getKey().toString()));
			case ATTR:
				handleError(new ECLSemanticException("MissingAttribute", ErrorCode.INVALID_REFERENCE, ref.getKey().toString()));
		}
	}

	private void handleError(ECLSemanticException e) throws ECLSemanticException{
        if (validating || ErrorCode.Severity.WARNING == e.getErrorCode().getSeverity()) {
            $statement::warnings.add(e);
        } else {
            throw e;
        }
	}
	
}


@rulecatch {
			catch (RecognitionException re) {
			    if (validating) {
                    reportError(re);
                    recover(input,re);
			    } else {
				    throw re;
				}
			} catch (InterpretationException ie) {
				throw ie;
			} catch (Exception e) {
				throw new InterpretationException("Interpretation failed due to inner error. May be a bug, please report", e);
			}	
}

/*----------------
*      RULES
*----------------*/

interpret
@init {
	validating = false;	
}
@after {
	if (null != retVal) {
		returnsValue = true;
	}
}
            :  ecl;

validate
@init {
	validating = true;
}
            :  ecl;

ecl			:	(statement {
						if (!validating) {
							if ($statement.ret) { 
								return;
							}
						}	
                    }
			    )*
			;

statement returns[boolean ret]
scope {
	Integer startLine;
	Integer startPos;
	List<ECLSemanticException> warnings;
	boolean reportUndefined;    //indicates whether 'undefined value' errors should be reported for this stmt
}
@init {
    $statement::warnings = new ArrayList<ECLSemanticException>();
    $statement::reportUndefined = true;
}
 	        :	STMT_BEGIN {
					tokenNo = 0;
 	                if (!validating) {
                        if (!evaluate) {consumeUntil(input, STMT_END); input.consume(); return false;} //skipping current statement
                        $statement::startLine = $STMT_BEGIN.line;
                        $statement::startPos = $STMT_BEGIN.getCharPositionInLine();
					}
				} 
				stmt STMT_END {
				    $ret = $stmt.ret;
				    if (validating) {
                        if (afterReturn) {
                            reportError(new ECLSemanticException(ErrorCode.UNREACHABLE_STMT, $STMT_BEGIN, $STMT_END));
                        } else if ($stmt.ret) {
                            afterReturn = true;
                            returnsValue = true;
                        }
                    }
				    for (ECLSemanticException e: $statement::warnings) {
                        if (!e.isFullyInitialized()) {
                            e.line = $STMT_BEGIN.line;
                            e.charPositionInLine = $STMT_BEGIN.getCharPositionInLine();
                            e.endLine = $STMT_END.line;
                            e.endPos = $STMT_END.getCharPositionInLine();
                            e.setFullyInitialized(true);
                        }
                        if (validating) {
                            reportError(e);
                        } else {
                            warn(e);
                        }
                    }
				}
			|	IF_BEGIN {
			        if (!validating) {
					    if (!evaluate) {
							consumeUntil(input, IF_END); input.consume(); return false; //skipping current statement
						} 
					}
				}
				if_stmt {
				    if (!validating) {  
				        if ($if_stmt.ret) {
							return true;
						}
				    }
				}
				IF_END {$ret = $if_stmt.ret;}
 	        ;
catch [RecognitionException re] {
				//need to initialize end line and position of the error
				if (re instanceof ECLSemanticException && !((ECLSemanticException)re).isFullyInitialized() ) {	
					ECLSemanticException ese = (ECLSemanticException) re;
					//we've already passed syntax validation phase, hence there is definitely STMT_END token somewhere
					//it will indicate the end of out statement
					consumeUntil(input, STMT_END);
					CommonTree endTree = (CommonTree) getCurrentInputSymbol(input);
					ese.line = $statement::startLine;
					ese.charPositionInLine = $statement::startPos;
					ese.endLine = endTree.getLine();
					ese.endPos = endTree.getCharPositionInLine();
					ese.setFullyInitialized(true);
                } 
					throw re;
			}
catch [InterpretationException ie] {
	throw ie;
}
catch [Exception e] {
	throw new InterpretationException("Interpretation failed due to inner error. May be a bug, please report", e);
}

stmt returns[boolean ret]
			:	standalone_expr
			|	assign
			|	declar
			|	seed_stmt
			|   r=ret 	{$ret = $r.ret;}
			;
			
seed_stmt	:	^(SEED a=expr)	{
                    if (!validating) {
                        randomData.reSeed($a.value);
                    }
                }
			;
			
standalone_expr
			:	expr	{
							handleError(new ECLSemanticException(ErrorCode.INVALID_STMT));
						}
			;
		
declar		:	^(VAR_DECL id expr?)	{
                                if (!$id.isMalformed) {
                                    if (heap.containsVar($id.text)) {
                                        handleError(new ECLSemanticException(ErrorCode.REDEFINED_VAR, $id.text));
                                    } else {
                                        heap.putVar($id.text, $expr.value != null? $expr.value : Value.UNDEFINED);
										if ($statement_block.size() > 0) {
											$statement_block::localVars.add($id.text);
										}
                                    }
                                }
                            }
		    ;


ret returns[boolean ret]
			:   ^(RETURN expr)	{
					$ret = true;
                    if (!validating) {
                        this.retVal = $expr.value;
                    }
                }
			|	RETURN	{
			        if (validating) {
                        $ret = false;
                    } else {
						$ret = true;
			            this.retVal = null;
			        }
			    }
            ;

if_stmt returns[boolean ret]	
@after {evaluate = true;}
			:	^(IF_STMT expr {
			            if (!validating) {
                            if (($expr.value.getType() == ValType.BOOL && !$expr.value.boolValue()) ||
                                    (!$expr.value.isDefined())) {
                                evaluate = false;
                            }
					    }
					}
					a=if_block {
					    if (validating) {
					        afterReturn = false;
					    } else {
					        if ($a.ret) return true;
					        evaluate = !evaluate;
                        }
					}
					(b=if_block {
					        if (!validating && $b.ret) {
					            return true;
					        }
					    }
					)?
						
				) {
                        if (validating) {
                            if (!$a.ret || !$b.ret) { //If both parts of if-else statement contain return statement,
                                                       //then all others are definitely placed after return, hence are unreachable.
                                                       //Otherwise other statements may be reachable.
                                afterReturn = false;
                                returnsValue = false;
                            } else {
                                afterReturn = true;
                                $ret = true;
                            }
                        } else {
                            $ret = false;
                        }
				}
			;	

if_block returns[boolean ret]
			: statement 	{$ret = $statement.ret;}
			| statement_block {$ret = $statement_block.ret;}
			;			


statement_block returns[boolean ret]
scope {
	List<String> localVars; //contains names of all the variables which were defined in this block
}
@init {
	$statement_block::localVars = new ArrayList<String>();
}
@after {
	for (String name: $statement_block::localVars) {
		heap.removeVar(name);
	}
}
			:	 ^(BLOCK (statement {
			                    if ($statement.ret) {
			                        if (validating) {
			                            $ret = true;
                                    } else {
                                        return true;
                                    }
                                }
			                }
			            )*
			        )
			;


			
assign				
@init {
	inAssign = true;
}
@after {
	inAssign = false;
}	
			:	^(ASSIGN id expr) {
								if (heap.containsVar($id.text)) {
								    if (!$id.isMalformed) {
                                        heap.putVar($id.text, $expr.value);
                                        if (Value.UNDEFINED.equals($expr.value)) {  //error was already reported for expr if its value is undefined, no need to report again for the var
                                            reportedIds.add($id.text);
                                        } else {
                                            reportedIds.remove($id.text);
                                        }
								    }
								} else {
									handleError(new ECLSemanticException(ErrorCode.UNDEFINED_VAR, $id.text));
								}
							}
			|	^(ASSIGN sys_id expr) {
			                    if (null == modelAccessor) {
								    return;
							    }
								if (null != $sys_id.ref) {
									if (!$sys_id.ref.setValue($expr.value)) {
										reportUndefinedRef($sys_id.ref);
									}
									if (Value.UNDEFINED.equals($expr.value)) {  //error was already reported for expr if its value is undefined, no need to report again for the ref
                                        reportedIds.add($sys_id.ref);
								    } else {
								        reportedIds.remove($sys_id.ref);
								    }
								}
							}
			;

		
expr returns[Value value]
@after {
    if (validating && null == $value) { //means that calculations were not performed
		$value = Value.UNDEFINED;
	} else if (Value.UNDEFINED.equals($value) && $statement::reportUndefined && 0 == $statement::warnings.size()) {  //'undefined value' error must be reported only if there are no other errors found on that line
        handleError(new ECLSemanticException(ErrorCode.UNDEFINED_VALUE));
        $statement::reportUndefined = false;
    }
}
			:	^(LAND a=expr b=expr) {$value = Calculator.and($a.value, $b.value);}
			|	^(LOR a=expr b=expr) {$value = Calculator.or($a.value, $b.value);}
			|	^(GT a=expr b=expr) {$value = Calculator.gt($a.value, $b.value);}
			|	^(LT a=expr b=expr) {$value = Calculator.lt($a.value, $b.value);}
			|	^(LE a=expr b=expr) {$value = Calculator.le($a.value, $b.value);}
			|	^(GE a=expr b=expr) {$value = Calculator.ge($a.value, $b.value);}
			|	^(EQ a=expr b=expr) {$value = Calculator.eq($a.value, $b.value);}
			|	^(NE a=expr b=expr) {$value = Calculator.ne($a.value, $b.value);}
			|	^(SIN a=expr) 	{$value = Calculator.sin($a.value);} 	
			|	^(COS a=expr)	{$value = Calculator.cos($a.value);}
			|	^(TAN a=expr)	{$value = Calculator.tan($a.value);}
			|	^(ATAN a=expr)	{$value = Calculator.atan($a.value);}
			|	^(COT a=expr)	{$value = Calculator.cot($a.value);}
			|	^(LN a=expr)	{$value = Calculator.ln($a.value);}
			|	^(ABS a=expr)	{$value = Calculator.abs($a.value);}
			|	^(ENTIER a=expr)	{$value = Calculator.entier($a.value);}
			|	^(SIGN a=expr)	{$value = Calculator.sign($a.value);}
			|	^(SQRT a=expr)	{$value = Calculator.sqrt($a.value);}
			|	^(PLUS a=expr b=expr)		{$value = Calculator.add($a.value, $b.value);} 
			| 	^(MINUS a=expr b=expr)	{$value = Calculator.sub($a.value, $b.value);} 	
			|	^(MULT a=expr b=expr)	{$value = Calculator.multiply($a.value, $b.value);} 
			| 	^(DIV a=expr b=expr)	{
			        if (((Double)0d).equals($b.value.doubleValue())) {
                        handleError(new ECLSemanticException(ErrorCode.DIV_BY_0));
                    }
			        $value = Calculator.divide($a.value, $b.value);
			    }
			|	^(INVERT a=expr)	{$value = Calculator.invert($a.value);} 
			|	^(POW a=expr b=expr) {$value = Calculator.pow($a.value, $b.value);}
			|	INT 	{$value = new Value($INT.text);} 
			|	REAL	{$value = new Value($REAL.text);}
			|	EXP		{$value = new Value(Math.exp(1));}
			|	id		{
			        if (!$id.isMalformed) {
						if (inAssign || tokenNo > 1) {
							if (heap.containsVar($id.text)) {
								$value = heap.getVar($id.text);
								if (Value.UNDEFINED.equals($value)) {
									if (reportedIds.contains($id.text)) {   //error must be reported only once per variable
										$statement::reportUndefined = false;
									} else {
										reportedIds.add($id.text);
									}
								}
							} else {
								handleError(new ECLSemanticException(ErrorCode.UNDEFINED_VAR, $id.text));
							}
						} else {
							handleError(new ECLSemanticException(ErrorCode.INVALID_STMT, $id.text));
						}
					}
			    }
			|	STR		{ $value = new Value($STR.text, ValType.STRING); }
			|   TRUE    { $value = new Value(true, ValType.BOOL); }
			|   FALSE   { $value = new Value(false, ValType.BOOL); }
			|	rand 	{
			         $value = $rand.value;
			         if (validating) {
			            if (Value.UNDEFINED.equals($rand.value) && 0 == $statement::warnings.size()) {
						    handleError(new ECLSemanticException(ErrorCode.UNDEFINED_VALUE));
                        }
                        $value = null; //random values must not be taken into account during the validation stage.
                    }
			    }
			|	sys_atom {$value = $sys_atom.value;}
			;
			
id returns[String text, boolean isMalformed]
			:	ID {$text = $ID.text;}
			|	MALFORMED_ID {
						$isMalformed = true;
						ECLSemanticException e = new ECLSemanticException(ErrorCode.INVALID_INPUT, $MALFORMED_ID.getLine(),
								$MALFORMED_ID.getCharPositionInLine(), $MALFORMED_ID.getLine(), $MALFORMED_ID.getCharPositionInLine() +
								($MALFORMED_ID.getText() == null ? 0 : $MALFORMED_ID.getText().length()-1));
						e.token = $MALFORMED_ID.getToken();
						e.setFullyInitialized(true);
						handleError(e);
						}
			;


rand returns[Value value]
			:	^(UNIFORM a=expr b=expr) 	{$value = randomData.nextUniform($a.value, $b.value);}
			|	^(POISSON a=expr) 		 	{$value = randomData.nextPoisson($a.value);}
			|	^(EXPONENTIAL a=expr)	 	{$value = randomData.nextExponential($a.value);}
			|	^(NORMAL a=expr b=expr)		{$value = randomData.nextNormal($a.value, $b.value);}
			|	^(BINOMIAL a=expr b=expr)	{$value = randomData.nextBinomial($a.value, $b.value);}		
			;	

sys_atom returns [Value value]
scope {
	EcliAggregate agg;
}	
@init {
	if (!validating && null == this.modelAccessor) {
		throw new InterpretationException("Model accessor is required for proper interpretation, null value was supplied!");
	}
	$sys_atom::agg = modelAccessor;
}
			:	^(IS_MARKED aggregate expr) { 
					$sys_atom::agg = $aggregate.agg;
					if (null != $sys_atom::agg) {
                        if (!$sys_atom::agg.containsPlace($expr.value.getValue())) {
                            handleError(new ECLSemanticException("MissingPlace", ErrorCode.INVALID_REFERENCE, $expr.value.toString()));
                        }
                        $value = new Value($sys_atom::agg.isPlaceMarked($expr.value.getValue()));
                    }
				}
			|	sys_id { 
					if (null == $sys_id.ref) {
						$value = validating? null : Value.UNDEFINED;
					} else {
						if (!$sys_id.ref.isDefined()) {
							reportUndefinedRef($sys_id.ref);
						} else {
							$value = new Value($sys_id.ref.getValue());
							if (Value.UNDEFINED.equals($value)) {
                                if (reportedIds.contains($sys_id.ref)) {   //error must be reported only once per ref
                                    $statement::reportUndefined = false;
                                } else {
                                    reportedIds.add($sys_id.ref);
                                }
                            }
						}
					}
				}
			|	TIME {
					if (null != modelAccessor) {
						$value = new Value(modelAccessor.getTime());
					}
				}
			;

	
/* returns outer ref decorator or null if the ref cannot be reached */	
sys_id returns[OuterRef.RefDecorator ref]
scope {
	EcliAggregate agg;
}
@init {
	if (null == this.modelAccessor) {
		throw new InterpretationException("Model accessor is required for proper interpretation, null value was supplied!");
	}
	$sys_id::agg = modelAccessor.getCurrentAggregate();
}
			:	^(SYS_VAR aggregate expr)	{
						$sys_id::agg = $aggregate.agg;
						if (null != $sys_id::agg) {
						    $ref = OuterRef.createVarDecorator($sys_id::agg, $expr.value.getValue(), $aggregate.uid.append('\0').append($expr.value).toString());
						}
					}	
			|	^(MARK aggregate p=expr a=expr){
						$sys_id::agg = $aggregate.agg;
						if (null != $sys_id::agg) {
                            if (!$sys_id::agg.containsPlace($p.value.getValue())) {
                                 handleError(new ECLSemanticException("MissingPlace", ErrorCode.INVALID_REFERENCE, $p.value.toString()));
                            } else if ($sys_id::agg.isPlaceMarked($p.value.getValue())) {
                                    $ref = OuterRef.createAttrDecorator($sys_id::agg.getTokenAt($p.value.getValue()), $a.value.getValue(),
                                            $aggregate.uid.append('\0').append($p.value).append('\0').append($a.value).toString());
                            } else {
								if (validating) {
									handleError(new ECLSemanticException("MissingAttribute", ErrorCode.INVALID_REFERENCE, $a.value.toString()));
								} else {
									handleError(new ECLSemanticException(ErrorCode.UNMARKED_PLACE, $p.value.getValue().toString()));
                                }
								$ref = null;
                            }
                        }
					}	
			|	^(CURRENT_MARK a=expr) {
							EcliToken currentToken = modelAccessor.getCurrentToken();
							if (null != currentToken) {
								$ref = OuterRef.createAttrDecorator(currentToken, $a.value.getValue(),
                                        new StringBuilder().append('\t').append("root").append('\t').append($a.value).toString());
                           
							} else if (!validating) {
								handleError(new ECLSemanticException("CurrentTokenUndefined", ErrorCode.INVALID_REFERENCE));
							}
						}
			;
			
/*
    returns: agg - corresponding aggregate or null if the aggregate cannot be reached
             uid - unique aggregate identifier, used for outer reference comparing
*/
aggregate returns[EcliAggregate agg, StringBuilder uid]
@init {
	$agg = null;
	$uid = new StringBuilder();
}
			: (ROOT_AGG	{
					$agg = modelAccessor.getRootAggregate();
					if ($agg == null) {
						handleError(new ECLSemanticException("RootUndefined", ErrorCode.INVALID_REFERENCE));
					}
					$uid.append('\t').append("rootagg"); //using '/t' as a delimiter to avoid ambiguity with root and non-root aggregate refs (ROOT != a['rootagg'])
				}
				)?
				{
					if ($agg == null && ($agg = modelAccessor.getCurrentAggregate()) == null) {
							handleError(new ECLSemanticException("CurrentAggregateUndefined", ErrorCode.INVALID_REFERENCE));
					}
				}
				(AGGREGATE expr {
						if (!$agg.containsAggregate($expr.value.getValue())) {
							handleError(new ECLSemanticException("MissingAggregate", ErrorCode.INVALID_REFERENCE, $expr.value.toString()));
                            $agg = null;
						} else {
						    $uid.append('\0').append($expr.value);  //using '/0' as a delimiter to avoid ambiguity (a['1'].a['134'] != a['11'].a['34'])
							$agg = $agg.getAggregate($expr.value.getValue());
						}
					}
				)*
			;