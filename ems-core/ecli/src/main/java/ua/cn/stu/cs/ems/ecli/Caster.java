package ua.cn.stu.cs.ems.ecli;

/**
 * @author n0weak
 */
interface Caster {

    /**
     * Method that determines whether the object can be casted to the type that is represented by the caster
     *
     * @param obj object to be casted
     * @return true if object can be casted or false of not
     */
    boolean isCastable(Object obj);

    /**
     * Method that casts passed object to the  type that is represented by the caster
     *
     * @param obj object to be casted
     * @return casted value of the passed object or null if object cannot be casted
     */
    Object cast(Object obj);
}
