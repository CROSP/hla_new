package ua.cn.stu.cs.ems.ecli.errors;

/**
 * @author n0weak
 */
public class InterpretationException extends RuntimeException {
    public InterpretationException() {
        super();
    }

    public InterpretationException(String message) {
        super(message);
    }

    public InterpretationException(String message, Throwable cause) {
        super(message, cause);
    }
}
