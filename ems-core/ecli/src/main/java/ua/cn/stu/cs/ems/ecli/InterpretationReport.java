package ua.cn.stu.cs.ems.ecli;

import ua.cn.stu.cs.ems.ecli.errors.ParsingError;

import java.util.ArrayList;
import java.util.List;

/**
 * @author n0weak
 */
public class InterpretationReport {

    private List<ParsingError> warnings;
    private Value result;

    public InterpretationReport(List<ParsingError> warnings, Value result) {
        this.warnings = new ArrayList<ParsingError>(warnings);
        this.result = result;
    }

    /**
     * @return list of errors with {@link ua.cn.stu.cs.jess.ecli.errors.ErrorCode.Severity#WARNING} severity which happened during interpretation process.
     *         Such errors don't fail the whole processing but can lead to {@link Value#UNDEFINED} values.
     */
    public List<ParsingError> getWarnings() {
        return warnings;
    }

    /**
     * @return interpretation result, i.e. value that is specified in 'RETURN' statement in the sources.
     *         If there is no return statement or an empty one ('RETURN;') found, then null is returned.
     */
    public Value getResult() {
        return result;
    }

}
