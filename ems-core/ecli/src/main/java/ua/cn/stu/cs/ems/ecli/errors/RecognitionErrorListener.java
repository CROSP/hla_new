package ua.cn.stu.cs.ems.ecli.errors;

import org.antlr.runtime.BaseRecognizer;
import org.antlr.runtime.RecognitionException;

/**
 * @author n0weak
 */
public interface RecognitionErrorListener {

   void errorHappened(RecognitionException e, BaseRecognizer recognizer);
}
