// $ANTLR 3.5.2 ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g 2016-04-20 18:12:58

    	package ua.cn.stu.cs.ems.ecli.impl;
		import ua.cn.stu.cs.ems.ecli.errors.*;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

import org.antlr.runtime.tree.*;


@SuppressWarnings("all")
public class ECLParser extends Parser {
	public static final String[] tokenNames = new String[] {
		"<invalid>", "<EOR>", "<DOWN>", "<UP>", "ABS", "AGGREGATE", "ASSIGN", 
		"ATAN", "BINOMIAL", "BLOCK", "COMMENT", "COS", "COT", "CURRENT_MARK", 
		"DIGIT", "DIV", "ELSE", "ENTIER", "EQ", "ESC", "EXP", "EXPONENTIAL", "FALSE", 
		"GE", "GT", "ID", "IF", "IF_BEGIN", "IF_END", "IF_STMT", "INT", "INVERT", 
		"IS_MARKED", "LAND", "LE", "LETTER", "LN", "LOR", "LT", "MALFORMED_ID", 
		"MARK", "MINUS", "MULT", "NE", "NORMAL", "NOT", "PLACE", "PLUS", "POISSON", 
		"POW", "REAL", "RETURN", "ROOT_AGG", "SEED", "SIGN", "SIN", "SQRT", "STMT_BEGIN", 
		"STMT_END", "STR", "SYS_VAR", "TAN", "TIME", "TRUE", "UNIFORM", "VAR", 
		"VAR_DECL", "WS", "'('", "')'", "','", "'.'", "';'", "'['", "']'", "'{'", 
		"'}'"
	};
	public static final int EOF=-1;
	public static final int T__68=68;
	public static final int T__69=69;
	public static final int T__70=70;
	public static final int T__71=71;
	public static final int T__72=72;
	public static final int T__73=73;
	public static final int T__74=74;
	public static final int T__75=75;
	public static final int T__76=76;
	public static final int ABS=4;
	public static final int AGGREGATE=5;
	public static final int ASSIGN=6;
	public static final int ATAN=7;
	public static final int BINOMIAL=8;
	public static final int BLOCK=9;
	public static final int COMMENT=10;
	public static final int COS=11;
	public static final int COT=12;
	public static final int CURRENT_MARK=13;
	public static final int DIGIT=14;
	public static final int DIV=15;
	public static final int ELSE=16;
	public static final int ENTIER=17;
	public static final int EQ=18;
	public static final int ESC=19;
	public static final int EXP=20;
	public static final int EXPONENTIAL=21;
	public static final int FALSE=22;
	public static final int GE=23;
	public static final int GT=24;
	public static final int ID=25;
	public static final int IF=26;
	public static final int IF_BEGIN=27;
	public static final int IF_END=28;
	public static final int IF_STMT=29;
	public static final int INT=30;
	public static final int INVERT=31;
	public static final int IS_MARKED=32;
	public static final int LAND=33;
	public static final int LE=34;
	public static final int LETTER=35;
	public static final int LN=36;
	public static final int LOR=37;
	public static final int LT=38;
	public static final int MALFORMED_ID=39;
	public static final int MARK=40;
	public static final int MINUS=41;
	public static final int MULT=42;
	public static final int NE=43;
	public static final int NORMAL=44;
	public static final int NOT=45;
	public static final int PLACE=46;
	public static final int PLUS=47;
	public static final int POISSON=48;
	public static final int POW=49;
	public static final int REAL=50;
	public static final int RETURN=51;
	public static final int ROOT_AGG=52;
	public static final int SEED=53;
	public static final int SIGN=54;
	public static final int SIN=55;
	public static final int SQRT=56;
	public static final int STMT_BEGIN=57;
	public static final int STMT_END=58;
	public static final int STR=59;
	public static final int SYS_VAR=60;
	public static final int TAN=61;
	public static final int TIME=62;
	public static final int TRUE=63;
	public static final int UNIFORM=64;
	public static final int VAR=65;
	public static final int VAR_DECL=66;
	public static final int WS=67;

	// delegates
	public Parser[] getDelegates() {
		return new Parser[] {};
	}

	// delegators


	public ECLParser(TokenStream input) {
		this(input, new RecognizerSharedState());
	}
	public ECLParser(TokenStream input, RecognizerSharedState state) {
		super(input, state);
	}

	protected TreeAdaptor adaptor = new CommonTreeAdaptor();

	public void setTreeAdaptor(TreeAdaptor adaptor) {
		this.adaptor = adaptor;
	}
	public TreeAdaptor getTreeAdaptor() {
		return adaptor;
	}
	@Override public String[] getTokenNames() { return ECLParser.tokenNames; }
	@Override public String getGrammarFileName() { return "ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g"; }


		private List<RecognitionErrorListener> errorListeners = new ArrayList<RecognitionErrorListener>();

		public void addErrorListener(RecognitionErrorListener listener) {
		    errorListeners.add(listener);
	    }

	    public void removeErrorListener(RecognitionErrorListener listener) {
		    errorListeners.remove(listener);
	    }

	    public void fireRecognitionError(RecognitionException e) {
	        for (RecognitionErrorListener listener : errorListeners) {
	            listener.errorHappened(e, this);
	        }
	    }

		public void displayRecognitionError(String[] tokenNames, RecognitionException e) {
			fireRecognitionError(correctException(e));
		}


		/**
		 *	Method that makes exception processing in order to insert some specific data or substitute
		 *  default exceptions on more suitable ones;
		 **/
		private RecognitionException correctException(RecognitionException e) {
			//if error is caused be unexpected EOF, then we have to substitute  MismatchedTokenException
			//with UnexpectedEOFException
			if (e instanceof MismatchedTokenException) {
				if (e.token.getType()==Token.EOF) {
					e = new UnexpectedEOFException((MismatchedTokenException) e);
				}
			}
			return e;
		}


	public static class ecl_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "ecl"
	// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:101:1: ecl : ( statement )+ ;
	public final ECLParser.ecl_return ecl() throws RecognitionException {
		ECLParser.ecl_return retval = new ECLParser.ecl_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope statement1 =null;


		try {
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:101:7: ( ( statement )+ )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:101:9: ( statement )+
			{
			root_0 = (Object)adaptor.nil();


			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:101:9: ( statement )+
			int cnt1=0;
			loop1:
			while (true) {
				int alt1=2;
				int LA1_0 = input.LA(1);
				if ( ((LA1_0 >= ABS && LA1_0 <= AGGREGATE)||(LA1_0 >= ATAN && LA1_0 <= BINOMIAL)||(LA1_0 >= COS && LA1_0 <= COT)||LA1_0==ENTIER||(LA1_0 >= EXP && LA1_0 <= FALSE)||(LA1_0 >= ID && LA1_0 <= IF)||LA1_0==INT||LA1_0==LN||(LA1_0 >= MALFORMED_ID && LA1_0 <= MINUS)||(LA1_0 >= NORMAL && LA1_0 <= PLACE)||LA1_0==POISSON||(LA1_0 >= REAL && LA1_0 <= SQRT)||(LA1_0 >= STR && LA1_0 <= VAR)||LA1_0==68) ) {
					alt1=1;
				}

				switch (alt1) {
				case 1 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:101:9: statement
					{
					pushFollow(FOLLOW_statement_in_ecl102);
					statement1=statement();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, statement1.getTree());

					}
					break;

				default :
					if ( cnt1 >= 1 ) break loop1;
					if (state.backtracking>0) {state.failed=true; return retval;}
					EarlyExitException eee = new EarlyExitException(1, input);
					throw eee;
				}
				cnt1++;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}

			catch (RecognitionException re) {
					reportError(re);
					recover(input,re);
			}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "ecl"


	public static class statement_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "statement"
	// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:104:1: statement : ( stmt end= ';' -> ( STMT_BEGIN[$start] stmt STMT_END[$end] )? | if_stmt -> ( IF_BEGIN if_stmt IF_END )? );
	public final ECLParser.statement_return statement() throws RecognitionException {
		ECLParser.statement_return retval = new ECLParser.statement_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token end=null;
		ParserRuleReturnScope stmt2 =null;
		ParserRuleReturnScope if_stmt3 =null;

		Object end_tree=null;
		RewriteRuleTokenStream stream_72=new RewriteRuleTokenStream(adaptor,"token 72");
		RewriteRuleSubtreeStream stream_stmt=new RewriteRuleSubtreeStream(adaptor,"rule stmt");
		RewriteRuleSubtreeStream stream_if_stmt=new RewriteRuleSubtreeStream(adaptor,"rule if_stmt");

		try {
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:104:13: ( stmt end= ';' -> ( STMT_BEGIN[$start] stmt STMT_END[$end] )? | if_stmt -> ( IF_BEGIN if_stmt IF_END )? )
			int alt2=2;
			int LA2_0 = input.LA(1);
			if ( ((LA2_0 >= ABS && LA2_0 <= AGGREGATE)||(LA2_0 >= ATAN && LA2_0 <= BINOMIAL)||(LA2_0 >= COS && LA2_0 <= COT)||LA2_0==ENTIER||(LA2_0 >= EXP && LA2_0 <= FALSE)||LA2_0==ID||LA2_0==INT||LA2_0==LN||(LA2_0 >= MALFORMED_ID && LA2_0 <= MINUS)||(LA2_0 >= NORMAL && LA2_0 <= PLACE)||LA2_0==POISSON||(LA2_0 >= REAL && LA2_0 <= SQRT)||(LA2_0 >= STR && LA2_0 <= VAR)||LA2_0==68) ) {
				alt2=1;
			}
			else if ( (LA2_0==IF) ) {
				alt2=2;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 2, 0, input);
				throw nvae;
			}

			switch (alt2) {
				case 1 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:104:17: stmt end= ';'
					{
					pushFollow(FOLLOW_stmt_in_statement119);
					stmt2=stmt();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_stmt.add(stmt2.getTree());
					end=(Token)match(input,72,FOLLOW_72_in_statement123); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_72.add(end);

					// AST REWRITE
					// elements: stmt
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 104:30: -> ( STMT_BEGIN[$start] stmt STMT_END[$end] )?
					{
						// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:104:33: ( STMT_BEGIN[$start] stmt STMT_END[$end] )?
						if ( stream_stmt.hasNext() ) {
							adaptor.addChild(root_0, (Object)adaptor.create(STMT_BEGIN, (retval.start)));
							adaptor.addChild(root_0, stream_stmt.nextTree());
							adaptor.addChild(root_0, (Object)adaptor.create(STMT_END, end));
						}
						stream_stmt.reset();

					}


					retval.tree = root_0;
					}

					}
					break;
				case 2 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:105:6: if_stmt
					{
					pushFollow(FOLLOW_if_stmt_in_statement143);
					if_stmt3=if_stmt();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_if_stmt.add(if_stmt3.getTree());
					// AST REWRITE
					// elements: if_stmt
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 105:16: -> ( IF_BEGIN if_stmt IF_END )?
					{
						// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:105:19: ( IF_BEGIN if_stmt IF_END )?
						if ( stream_if_stmt.hasNext() ) {
							adaptor.addChild(root_0, (Object)adaptor.create(IF_BEGIN, "IF_BEGIN"));
							adaptor.addChild(root_0, stream_if_stmt.nextTree());
							adaptor.addChild(root_0, (Object)adaptor.create(IF_END, "IF_END"));
						}
						stream_if_stmt.reset();

					}


					retval.tree = root_0;
					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}

			catch (RecognitionException re) {
					reportError(re);
					recover(input,re);
			}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "statement"


	public static class stmt_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "stmt"
	// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:108:1: stmt : ( ( assign )=> assign | standalone_expr | declar | ret | seed_stmt );
	public final ECLParser.stmt_return stmt() throws RecognitionException {
		ECLParser.stmt_return retval = new ECLParser.stmt_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope assign4 =null;
		ParserRuleReturnScope standalone_expr5 =null;
		ParserRuleReturnScope declar6 =null;
		ParserRuleReturnScope ret7 =null;
		ParserRuleReturnScope seed_stmt8 =null;


		try {
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:108:7: ( ( assign )=> assign | standalone_expr | declar | ret | seed_stmt )
			int alt3=5;
			switch ( input.LA(1) ) {
			case ID:
			case MALFORMED_ID:
				{
				int LA3_1 = input.LA(2);
				if ( (synpred1_ECL()) ) {
					alt3=1;
				}
				else if ( (true) ) {
					alt3=2;
				}

				}
				break;
			case AGGREGATE:
				{
				int LA3_2 = input.LA(2);
				if ( (synpred1_ECL()) ) {
					alt3=1;
				}
				else if ( (true) ) {
					alt3=2;
				}

				}
				break;
			case ROOT_AGG:
				{
				int LA3_3 = input.LA(2);
				if ( (synpred1_ECL()) ) {
					alt3=1;
				}
				else if ( (true) ) {
					alt3=2;
				}

				}
				break;
			case SYS_VAR:
				{
				int LA3_4 = input.LA(2);
				if ( (synpred1_ECL()) ) {
					alt3=1;
				}
				else if ( (true) ) {
					alt3=2;
				}

				}
				break;
			case PLACE:
				{
				int LA3_5 = input.LA(2);
				if ( (synpred1_ECL()) ) {
					alt3=1;
				}
				else if ( (true) ) {
					alt3=2;
				}

				}
				break;
			case MARK:
				{
				int LA3_6 = input.LA(2);
				if ( (synpred1_ECL()) ) {
					alt3=1;
				}
				else if ( (true) ) {
					alt3=2;
				}

				}
				break;
			case ABS:
			case ATAN:
			case BINOMIAL:
			case COS:
			case COT:
			case ENTIER:
			case EXP:
			case EXPONENTIAL:
			case FALSE:
			case INT:
			case LN:
			case MINUS:
			case NORMAL:
			case NOT:
			case POISSON:
			case REAL:
			case SIGN:
			case SIN:
			case SQRT:
			case STR:
			case TAN:
			case TIME:
			case TRUE:
			case UNIFORM:
			case 68:
				{
				alt3=2;
				}
				break;
			case VAR:
				{
				alt3=3;
				}
				break;
			case RETURN:
				{
				alt3=4;
				}
				break;
			case SEED:
				{
				alt3=5;
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 3, 0, input);
				throw nvae;
			}
			switch (alt3) {
				case 1 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:108:10: ( assign )=> assign
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_assign_in_stmt185);
					assign4=assign();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, assign4.getTree());

					}
					break;
				case 2 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:109:6: standalone_expr
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_standalone_expr_in_stmt192);
					standalone_expr5=standalone_expr();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, standalone_expr5.getTree());

					}
					break;
				case 3 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:110:6: declar
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_declar_in_stmt200);
					declar6=declar();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, declar6.getTree());

					}
					break;
				case 4 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:111:8: ret
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_ret_in_stmt209);
					ret7=ret();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, ret7.getTree());

					}
					break;
				case 5 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:112:6: seed_stmt
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_seed_stmt_in_stmt216);
					seed_stmt8=seed_stmt();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, seed_stmt8.getTree());

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}

			catch (RecognitionException re) {
					reportError(re);
					recover(input,re);
			}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "stmt"


	public static class seed_stmt_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "seed_stmt"
	// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:115:1: seed_stmt : SEED '(' expr ')' -> ^( SEED expr ) ;
	public final ECLParser.seed_stmt_return seed_stmt() throws RecognitionException {
		ECLParser.seed_stmt_return retval = new ECLParser.seed_stmt_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token SEED9=null;
		Token char_literal10=null;
		Token char_literal12=null;
		ParserRuleReturnScope expr11 =null;

		Object SEED9_tree=null;
		Object char_literal10_tree=null;
		Object char_literal12_tree=null;
		RewriteRuleTokenStream stream_69=new RewriteRuleTokenStream(adaptor,"token 69");
		RewriteRuleTokenStream stream_68=new RewriteRuleTokenStream(adaptor,"token 68");
		RewriteRuleTokenStream stream_SEED=new RewriteRuleTokenStream(adaptor,"token SEED");
		RewriteRuleSubtreeStream stream_expr=new RewriteRuleSubtreeStream(adaptor,"rule expr");

		try {
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:115:11: ( SEED '(' expr ')' -> ^( SEED expr ) )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:115:13: SEED '(' expr ')'
			{
			SEED9=(Token)match(input,SEED,FOLLOW_SEED_in_seed_stmt231); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_SEED.add(SEED9);

			char_literal10=(Token)match(input,68,FOLLOW_68_in_seed_stmt233); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_68.add(char_literal10);

			pushFollow(FOLLOW_expr_in_seed_stmt235);
			expr11=expr();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_expr.add(expr11.getTree());
			char_literal12=(Token)match(input,69,FOLLOW_69_in_seed_stmt237); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_69.add(char_literal12);

			// AST REWRITE
			// elements: expr, SEED
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 115:31: -> ^( SEED expr )
			{
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:115:34: ^( SEED expr )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot(stream_SEED.nextNode(), root_1);
				adaptor.addChild(root_1, stream_expr.nextTree());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}

			catch (RecognitionException re) {
					reportError(re);
					recover(input,re);
			}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "seed_stmt"


	public static class standalone_expr_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "standalone_expr"
	// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:118:1: standalone_expr : expr ;
	public final ECLParser.standalone_expr_return standalone_expr() throws RecognitionException {
		ECLParser.standalone_expr_return retval = new ECLParser.standalone_expr_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope expr13 =null;


		try {
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:119:4: ( expr )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:119:6: expr
			{
			root_0 = (Object)adaptor.nil();


			pushFollow(FOLLOW_expr_in_standalone_expr265);
			expr13=expr();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, expr13.getTree());

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}

			catch (RecognitionException re) {
					reportError(re);
					recover(input,re);
			}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "standalone_expr"


	public static class declar_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "declar"
	// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:122:1: declar : ( VAR id -> ^( VAR_DECL id ) | VAR id ASSIGN expr -> ^( VAR_DECL id expr ) );
	public final ECLParser.declar_return declar() throws RecognitionException {
		ECLParser.declar_return retval = new ECLParser.declar_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token VAR14=null;
		Token VAR16=null;
		Token ASSIGN18=null;
		ParserRuleReturnScope id15 =null;
		ParserRuleReturnScope id17 =null;
		ParserRuleReturnScope expr19 =null;

		Object VAR14_tree=null;
		Object VAR16_tree=null;
		Object ASSIGN18_tree=null;
		RewriteRuleTokenStream stream_VAR=new RewriteRuleTokenStream(adaptor,"token VAR");
		RewriteRuleTokenStream stream_ASSIGN=new RewriteRuleTokenStream(adaptor,"token ASSIGN");
		RewriteRuleSubtreeStream stream_id=new RewriteRuleSubtreeStream(adaptor,"rule id");
		RewriteRuleSubtreeStream stream_expr=new RewriteRuleSubtreeStream(adaptor,"rule expr");

		try {
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:122:9: ( VAR id -> ^( VAR_DECL id ) | VAR id ASSIGN expr -> ^( VAR_DECL id expr ) )
			int alt4=2;
			int LA4_0 = input.LA(1);
			if ( (LA4_0==VAR) ) {
				int LA4_1 = input.LA(2);
				if ( (LA4_1==ID||LA4_1==MALFORMED_ID) ) {
					int LA4_2 = input.LA(3);
					if ( (LA4_2==72) ) {
						alt4=1;
					}
					else if ( (LA4_2==ASSIGN) ) {
						alt4=2;
					}

					else {
						if (state.backtracking>0) {state.failed=true; return retval;}
						int nvaeMark = input.mark();
						try {
							for (int nvaeConsume = 0; nvaeConsume < 3 - 1; nvaeConsume++) {
								input.consume();
							}
							NoViableAltException nvae =
								new NoViableAltException("", 4, 2, input);
							throw nvae;
						} finally {
							input.rewind(nvaeMark);
						}
					}

				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 4, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 4, 0, input);
				throw nvae;
			}

			switch (alt4) {
				case 1 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:122:11: VAR id
					{
					VAR14=(Token)match(input,VAR,FOLLOW_VAR_in_declar278); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_VAR.add(VAR14);

					pushFollow(FOLLOW_id_in_declar280);
					id15=id();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_id.add(id15.getTree());
					// AST REWRITE
					// elements: id
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 122:18: -> ^( VAR_DECL id )
					{
						// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:122:21: ^( VAR_DECL id )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(VAR_DECL, "VAR_DECL"), root_1);
						adaptor.addChild(root_1, stream_id.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 2 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:123:9: VAR id ASSIGN expr
					{
					VAR16=(Token)match(input,VAR,FOLLOW_VAR_in_declar298); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_VAR.add(VAR16);

					pushFollow(FOLLOW_id_in_declar300);
					id17=id();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_id.add(id17.getTree());
					ASSIGN18=(Token)match(input,ASSIGN,FOLLOW_ASSIGN_in_declar302); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_ASSIGN.add(ASSIGN18);

					pushFollow(FOLLOW_expr_in_declar304);
					expr19=expr();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_expr.add(expr19.getTree());
					// AST REWRITE
					// elements: expr, id
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 123:28: -> ^( VAR_DECL id expr )
					{
						// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:123:31: ^( VAR_DECL id expr )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(VAR_DECL, "VAR_DECL"), root_1);
						adaptor.addChild(root_1, stream_id.nextTree());
						adaptor.addChild(root_1, stream_expr.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}

			catch (RecognitionException re) {
					reportError(re);
					recover(input,re);
			}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "declar"


	public static class ret_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "ret"
	// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:126:1: ret : ( RETURN expr -> ^( RETURN expr ) | RETURN );
	public final ECLParser.ret_return ret() throws RecognitionException {
		ECLParser.ret_return retval = new ECLParser.ret_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token RETURN20=null;
		Token RETURN22=null;
		ParserRuleReturnScope expr21 =null;

		Object RETURN20_tree=null;
		Object RETURN22_tree=null;
		RewriteRuleTokenStream stream_RETURN=new RewriteRuleTokenStream(adaptor,"token RETURN");
		RewriteRuleSubtreeStream stream_expr=new RewriteRuleSubtreeStream(adaptor,"rule expr");

		try {
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:126:7: ( RETURN expr -> ^( RETURN expr ) | RETURN )
			int alt5=2;
			int LA5_0 = input.LA(1);
			if ( (LA5_0==RETURN) ) {
				int LA5_1 = input.LA(2);
				if ( ((LA5_1 >= ABS && LA5_1 <= AGGREGATE)||(LA5_1 >= ATAN && LA5_1 <= BINOMIAL)||(LA5_1 >= COS && LA5_1 <= COT)||LA5_1==ENTIER||(LA5_1 >= EXP && LA5_1 <= FALSE)||LA5_1==ID||LA5_1==INT||LA5_1==LN||(LA5_1 >= MALFORMED_ID && LA5_1 <= MINUS)||(LA5_1 >= NORMAL && LA5_1 <= PLACE)||LA5_1==POISSON||LA5_1==REAL||LA5_1==ROOT_AGG||(LA5_1 >= SIGN && LA5_1 <= SQRT)||(LA5_1 >= STR && LA5_1 <= UNIFORM)||LA5_1==68) ) {
					alt5=1;
				}
				else if ( (LA5_1==72) ) {
					alt5=2;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 5, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 5, 0, input);
				throw nvae;
			}

			switch (alt5) {
				case 1 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:126:11: RETURN expr
					{
					RETURN20=(Token)match(input,RETURN,FOLLOW_RETURN_in_ret333); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RETURN.add(RETURN20);

					pushFollow(FOLLOW_expr_in_ret335);
					expr21=expr();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_expr.add(expr21.getTree());
					// AST REWRITE
					// elements: RETURN, expr
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 126:23: -> ^( RETURN expr )
					{
						// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:126:26: ^( RETURN expr )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot(stream_RETURN.nextNode(), root_1);
						adaptor.addChild(root_1, stream_expr.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 2 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:127:6: RETURN
					{
					root_0 = (Object)adaptor.nil();


					RETURN22=(Token)match(input,RETURN,FOLLOW_RETURN_in_ret350); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					RETURN22_tree = (Object)adaptor.create(RETURN22);
					adaptor.addChild(root_0, RETURN22_tree);
					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}

			catch (RecognitionException re) {
					reportError(re);
					recover(input,re);
			}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "ret"


	public static class if_stmt_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "if_stmt"
	// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:130:1: if_stmt options {backtrack=true; } : ( IF '(' expr ')' if_block ELSE if_block -> ^( IF_STMT expr if_block if_block ) | IF '(' expr ')' if_block -> ^( IF_STMT expr if_block ) );
	public final ECLParser.if_stmt_return if_stmt() throws RecognitionException {
		ECLParser.if_stmt_return retval = new ECLParser.if_stmt_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token IF23=null;
		Token char_literal24=null;
		Token char_literal26=null;
		Token ELSE28=null;
		Token IF30=null;
		Token char_literal31=null;
		Token char_literal33=null;
		ParserRuleReturnScope expr25 =null;
		ParserRuleReturnScope if_block27 =null;
		ParserRuleReturnScope if_block29 =null;
		ParserRuleReturnScope expr32 =null;
		ParserRuleReturnScope if_block34 =null;

		Object IF23_tree=null;
		Object char_literal24_tree=null;
		Object char_literal26_tree=null;
		Object ELSE28_tree=null;
		Object IF30_tree=null;
		Object char_literal31_tree=null;
		Object char_literal33_tree=null;
		RewriteRuleTokenStream stream_69=new RewriteRuleTokenStream(adaptor,"token 69");
		RewriteRuleTokenStream stream_68=new RewriteRuleTokenStream(adaptor,"token 68");
		RewriteRuleTokenStream stream_IF=new RewriteRuleTokenStream(adaptor,"token IF");
		RewriteRuleTokenStream stream_ELSE=new RewriteRuleTokenStream(adaptor,"token ELSE");
		RewriteRuleSubtreeStream stream_if_block=new RewriteRuleSubtreeStream(adaptor,"rule if_block");
		RewriteRuleSubtreeStream stream_expr=new RewriteRuleSubtreeStream(adaptor,"rule expr");

		try {
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:132:4: ( IF '(' expr ')' if_block ELSE if_block -> ^( IF_STMT expr if_block if_block ) | IF '(' expr ')' if_block -> ^( IF_STMT expr if_block ) )
			int alt6=2;
			int LA6_0 = input.LA(1);
			if ( (LA6_0==IF) ) {
				int LA6_1 = input.LA(2);
				if ( (synpred2_ECL()) ) {
					alt6=1;
				}
				else if ( (true) ) {
					alt6=2;
				}

			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 6, 0, input);
				throw nvae;
			}

			switch (alt6) {
				case 1 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:132:6: IF '(' expr ')' if_block ELSE if_block
					{
					IF23=(Token)match(input,IF,FOLLOW_IF_in_if_stmt385); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_IF.add(IF23);

					char_literal24=(Token)match(input,68,FOLLOW_68_in_if_stmt387); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_68.add(char_literal24);

					pushFollow(FOLLOW_expr_in_if_stmt389);
					expr25=expr();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_expr.add(expr25.getTree());
					char_literal26=(Token)match(input,69,FOLLOW_69_in_if_stmt391); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_69.add(char_literal26);

					pushFollow(FOLLOW_if_block_in_if_stmt393);
					if_block27=if_block();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_if_block.add(if_block27.getTree());
					ELSE28=(Token)match(input,ELSE,FOLLOW_ELSE_in_if_stmt395); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_ELSE.add(ELSE28);

					pushFollow(FOLLOW_if_block_in_if_stmt397);
					if_block29=if_block();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_if_block.add(if_block29.getTree());
					// AST REWRITE
					// elements: if_block, expr, if_block
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 132:45: -> ^( IF_STMT expr if_block if_block )
					{
						// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:132:48: ^( IF_STMT expr if_block if_block )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(IF_STMT, "IF_STMT"), root_1);
						adaptor.addChild(root_1, stream_expr.nextTree());
						adaptor.addChild(root_1, stream_if_block.nextTree());
						adaptor.addChild(root_1, stream_if_block.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 2 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:133:6: IF '(' expr ')' if_block
					{
					IF30=(Token)match(input,IF,FOLLOW_IF_in_if_stmt417); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_IF.add(IF30);

					char_literal31=(Token)match(input,68,FOLLOW_68_in_if_stmt419); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_68.add(char_literal31);

					pushFollow(FOLLOW_expr_in_if_stmt421);
					expr32=expr();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_expr.add(expr32.getTree());
					char_literal33=(Token)match(input,69,FOLLOW_69_in_if_stmt423); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_69.add(char_literal33);

					pushFollow(FOLLOW_if_block_in_if_stmt425);
					if_block34=if_block();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_if_block.add(if_block34.getTree());
					// AST REWRITE
					// elements: if_block, expr
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 133:31: -> ^( IF_STMT expr if_block )
					{
						// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:133:35: ^( IF_STMT expr if_block )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(IF_STMT, "IF_STMT"), root_1);
						adaptor.addChild(root_1, stream_expr.nextTree());
						adaptor.addChild(root_1, stream_if_block.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}

			catch (RecognitionException re) {
					reportError(re);
					recover(input,re);
			}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "if_stmt"


	public static class if_block_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "if_block"
	// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:136:1: if_block : ( statement | statement_block );
	public final ECLParser.if_block_return if_block() throws RecognitionException {
		ECLParser.if_block_return retval = new ECLParser.if_block_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope statement35 =null;
		ParserRuleReturnScope statement_block36 =null;


		try {
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:136:10: ( statement | statement_block )
			int alt7=2;
			int LA7_0 = input.LA(1);
			if ( ((LA7_0 >= ABS && LA7_0 <= AGGREGATE)||(LA7_0 >= ATAN && LA7_0 <= BINOMIAL)||(LA7_0 >= COS && LA7_0 <= COT)||LA7_0==ENTIER||(LA7_0 >= EXP && LA7_0 <= FALSE)||(LA7_0 >= ID && LA7_0 <= IF)||LA7_0==INT||LA7_0==LN||(LA7_0 >= MALFORMED_ID && LA7_0 <= MINUS)||(LA7_0 >= NORMAL && LA7_0 <= PLACE)||LA7_0==POISSON||(LA7_0 >= REAL && LA7_0 <= SQRT)||(LA7_0 >= STR && LA7_0 <= VAR)||LA7_0==68) ) {
				alt7=1;
			}
			else if ( (LA7_0==75) ) {
				alt7=2;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 7, 0, input);
				throw nvae;
			}

			switch (alt7) {
				case 1 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:136:12: statement
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_statement_in_if_block451);
					statement35=statement();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, statement35.getTree());

					}
					break;
				case 2 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:137:6: statement_block
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_statement_block_in_if_block459);
					statement_block36=statement_block();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, statement_block36.getTree());

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}

			catch (RecognitionException re) {
					reportError(re);
					recover(input,re);
			}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "if_block"


	public static class statement_block_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "statement_block"
	// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:140:1: statement_block : '{' ( statement )* '}' -> ^( BLOCK ( statement )* ) ;
	public final ECLParser.statement_block_return statement_block() throws RecognitionException {
		ECLParser.statement_block_return retval = new ECLParser.statement_block_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token char_literal37=null;
		Token char_literal39=null;
		ParserRuleReturnScope statement38 =null;

		Object char_literal37_tree=null;
		Object char_literal39_tree=null;
		RewriteRuleTokenStream stream_75=new RewriteRuleTokenStream(adaptor,"token 75");
		RewriteRuleTokenStream stream_76=new RewriteRuleTokenStream(adaptor,"token 76");
		RewriteRuleSubtreeStream stream_statement=new RewriteRuleSubtreeStream(adaptor,"rule statement");

		try {
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:141:4: ( '{' ( statement )* '}' -> ^( BLOCK ( statement )* ) )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:141:6: '{' ( statement )* '}'
			{
			char_literal37=(Token)match(input,75,FOLLOW_75_in_statement_block477); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_75.add(char_literal37);

			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:141:10: ( statement )*
			loop8:
			while (true) {
				int alt8=2;
				int LA8_0 = input.LA(1);
				if ( ((LA8_0 >= ABS && LA8_0 <= AGGREGATE)||(LA8_0 >= ATAN && LA8_0 <= BINOMIAL)||(LA8_0 >= COS && LA8_0 <= COT)||LA8_0==ENTIER||(LA8_0 >= EXP && LA8_0 <= FALSE)||(LA8_0 >= ID && LA8_0 <= IF)||LA8_0==INT||LA8_0==LN||(LA8_0 >= MALFORMED_ID && LA8_0 <= MINUS)||(LA8_0 >= NORMAL && LA8_0 <= PLACE)||LA8_0==POISSON||(LA8_0 >= REAL && LA8_0 <= SQRT)||(LA8_0 >= STR && LA8_0 <= VAR)||LA8_0==68) ) {
					alt8=1;
				}

				switch (alt8) {
				case 1 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:141:10: statement
					{
					pushFollow(FOLLOW_statement_in_statement_block479);
					statement38=statement();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_statement.add(statement38.getTree());
					}
					break;

				default :
					break loop8;
				}
			}

			char_literal39=(Token)match(input,76,FOLLOW_76_in_statement_block482); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_76.add(char_literal39);

			// AST REWRITE
			// elements: statement
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 141:25: -> ^( BLOCK ( statement )* )
			{
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:141:28: ^( BLOCK ( statement )* )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(BLOCK, "BLOCK"), root_1);
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:141:36: ( statement )*
				while ( stream_statement.hasNext() ) {
					adaptor.addChild(root_1, stream_statement.nextTree());
				}
				stream_statement.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}

			catch (RecognitionException re) {
					reportError(re);
					recover(input,re);
			}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "statement_block"


	public static class assign_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "assign"
	// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:144:1: assign : ( id ASSIGN expr -> ^( ASSIGN id expr ) | sys_id ASSIGN expr -> ^( ASSIGN sys_id expr ) );
	public final ECLParser.assign_return assign() throws RecognitionException {
		ECLParser.assign_return retval = new ECLParser.assign_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token ASSIGN41=null;
		Token ASSIGN44=null;
		ParserRuleReturnScope id40 =null;
		ParserRuleReturnScope expr42 =null;
		ParserRuleReturnScope sys_id43 =null;
		ParserRuleReturnScope expr45 =null;

		Object ASSIGN41_tree=null;
		Object ASSIGN44_tree=null;
		RewriteRuleTokenStream stream_ASSIGN=new RewriteRuleTokenStream(adaptor,"token ASSIGN");
		RewriteRuleSubtreeStream stream_id=new RewriteRuleSubtreeStream(adaptor,"rule id");
		RewriteRuleSubtreeStream stream_sys_id=new RewriteRuleSubtreeStream(adaptor,"rule sys_id");
		RewriteRuleSubtreeStream stream_expr=new RewriteRuleSubtreeStream(adaptor,"rule expr");

		try {
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:144:9: ( id ASSIGN expr -> ^( ASSIGN id expr ) | sys_id ASSIGN expr -> ^( ASSIGN sys_id expr ) )
			int alt9=2;
			int LA9_0 = input.LA(1);
			if ( (LA9_0==ID||LA9_0==MALFORMED_ID) ) {
				alt9=1;
			}
			else if ( (LA9_0==AGGREGATE||LA9_0==MARK||LA9_0==PLACE||LA9_0==ROOT_AGG||LA9_0==SYS_VAR) ) {
				alt9=2;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 9, 0, input);
				throw nvae;
			}

			switch (alt9) {
				case 1 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:144:11: id ASSIGN expr
					{
					pushFollow(FOLLOW_id_in_assign504);
					id40=id();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_id.add(id40.getTree());
					ASSIGN41=(Token)match(input,ASSIGN,FOLLOW_ASSIGN_in_assign506); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_ASSIGN.add(ASSIGN41);

					pushFollow(FOLLOW_expr_in_assign508);
					expr42=expr();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_expr.add(expr42.getTree());
					// AST REWRITE
					// elements: id, expr, ASSIGN
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 144:26: -> ^( ASSIGN id expr )
					{
						// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:144:29: ^( ASSIGN id expr )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot(stream_ASSIGN.nextNode(), root_1);
						adaptor.addChild(root_1, stream_id.nextTree());
						adaptor.addChild(root_1, stream_expr.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 2 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:145:6: sys_id ASSIGN expr
					{
					pushFollow(FOLLOW_sys_id_in_assign525);
					sys_id43=sys_id();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_sys_id.add(sys_id43.getTree());
					ASSIGN44=(Token)match(input,ASSIGN,FOLLOW_ASSIGN_in_assign527); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_ASSIGN.add(ASSIGN44);

					pushFollow(FOLLOW_expr_in_assign529);
					expr45=expr();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_expr.add(expr45.getTree());
					// AST REWRITE
					// elements: ASSIGN, sys_id, expr
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 145:25: -> ^( ASSIGN sys_id expr )
					{
						// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:145:28: ^( ASSIGN sys_id expr )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot(stream_ASSIGN.nextNode(), root_1);
						adaptor.addChild(root_1, stream_sys_id.nextTree());
						adaptor.addChild(root_1, stream_expr.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}

			catch (RecognitionException re) {
					reportError(re);
					recover(input,re);
			}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "assign"


	public static class expr_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "expr"
	// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:148:1: expr : prec5 ;
	public final ECLParser.expr_return expr() throws RecognitionException {
		ECLParser.expr_return retval = new ECLParser.expr_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope prec546 =null;


		try {
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:148:7: ( prec5 )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:148:9: prec5
			{
			root_0 = (Object)adaptor.nil();


			pushFollow(FOLLOW_prec5_in_expr552);
			prec546=prec5();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, prec546.getTree());

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}

			catch (RecognitionException re) {
					reportError(re);
					recover(input,re);
			}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "expr"


	public static class prec5_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "prec5"
	// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:151:1: prec5 : prec4 ( ( LAND ^| LOR ^) prec4 )? ;
	public final ECLParser.prec5_return prec5() throws RecognitionException {
		ECLParser.prec5_return retval = new ECLParser.prec5_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token LAND48=null;
		Token LOR49=null;
		ParserRuleReturnScope prec447 =null;
		ParserRuleReturnScope prec450 =null;

		Object LAND48_tree=null;
		Object LOR49_tree=null;

		try {
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:151:8: ( prec4 ( ( LAND ^| LOR ^) prec4 )? )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:151:10: prec4 ( ( LAND ^| LOR ^) prec4 )?
			{
			root_0 = (Object)adaptor.nil();


			pushFollow(FOLLOW_prec4_in_prec5568);
			prec447=prec4();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, prec447.getTree());

			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:151:16: ( ( LAND ^| LOR ^) prec4 )?
			int alt11=2;
			int LA11_0 = input.LA(1);
			if ( (LA11_0==LAND||LA11_0==LOR) ) {
				alt11=1;
			}
			switch (alt11) {
				case 1 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:152:5: ( LAND ^| LOR ^) prec4
					{
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:152:5: ( LAND ^| LOR ^)
					int alt10=2;
					int LA10_0 = input.LA(1);
					if ( (LA10_0==LAND) ) {
						alt10=1;
					}
					else if ( (LA10_0==LOR) ) {
						alt10=2;
					}

					else {
						if (state.backtracking>0) {state.failed=true; return retval;}
						NoViableAltException nvae =
							new NoViableAltException("", 10, 0, input);
						throw nvae;
					}

					switch (alt10) {
						case 1 :
							// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:152:6: LAND ^
							{
							LAND48=(Token)match(input,LAND,FOLLOW_LAND_in_prec5577); if (state.failed) return retval;
							if ( state.backtracking==0 ) {
							LAND48_tree = (Object)adaptor.create(LAND48);
							root_0 = (Object)adaptor.becomeRoot(LAND48_tree, root_0);
							}

							}
							break;
						case 2 :
							// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:152:14: LOR ^
							{
							LOR49=(Token)match(input,LOR,FOLLOW_LOR_in_prec5582); if (state.failed) return retval;
							if ( state.backtracking==0 ) {
							LOR49_tree = (Object)adaptor.create(LOR49);
							root_0 = (Object)adaptor.becomeRoot(LOR49_tree, root_0);
							}

							}
							break;

					}

					pushFollow(FOLLOW_prec4_in_prec5590);
					prec450=prec4();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, prec450.getTree());

					}
					break;

			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}

			catch (RecognitionException re) {
					reportError(re);
					recover(input,re);
			}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "prec5"


	public static class prec4_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "prec4"
	// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:157:1: prec4 : prec3 ( ( GT ^| LT ^| LE ^| GE ^| EQ ^| NE ^) prec3 )? ;
	public final ECLParser.prec4_return prec4() throws RecognitionException {
		ECLParser.prec4_return retval = new ECLParser.prec4_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token GT52=null;
		Token LT53=null;
		Token LE54=null;
		Token GE55=null;
		Token EQ56=null;
		Token NE57=null;
		ParserRuleReturnScope prec351 =null;
		ParserRuleReturnScope prec358 =null;

		Object GT52_tree=null;
		Object LT53_tree=null;
		Object LE54_tree=null;
		Object GE55_tree=null;
		Object EQ56_tree=null;
		Object NE57_tree=null;

		try {
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:157:8: ( prec3 ( ( GT ^| LT ^| LE ^| GE ^| EQ ^| NE ^) prec3 )? )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:157:10: prec3 ( ( GT ^| LT ^| LE ^| GE ^| EQ ^| NE ^) prec3 )?
			{
			root_0 = (Object)adaptor.nil();


			pushFollow(FOLLOW_prec3_in_prec4613);
			prec351=prec3();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, prec351.getTree());

			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:157:16: ( ( GT ^| LT ^| LE ^| GE ^| EQ ^| NE ^) prec3 )?
			int alt13=2;
			int LA13_0 = input.LA(1);
			if ( (LA13_0==EQ||(LA13_0 >= GE && LA13_0 <= GT)||LA13_0==LE||LA13_0==LT||LA13_0==NE) ) {
				alt13=1;
			}
			switch (alt13) {
				case 1 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:158:5: ( GT ^| LT ^| LE ^| GE ^| EQ ^| NE ^) prec3
					{
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:158:5: ( GT ^| LT ^| LE ^| GE ^| EQ ^| NE ^)
					int alt12=6;
					switch ( input.LA(1) ) {
					case GT:
						{
						alt12=1;
						}
						break;
					case LT:
						{
						alt12=2;
						}
						break;
					case LE:
						{
						alt12=3;
						}
						break;
					case GE:
						{
						alt12=4;
						}
						break;
					case EQ:
						{
						alt12=5;
						}
						break;
					case NE:
						{
						alt12=6;
						}
						break;
					default:
						if (state.backtracking>0) {state.failed=true; return retval;}
						NoViableAltException nvae =
							new NoViableAltException("", 12, 0, input);
						throw nvae;
					}
					switch (alt12) {
						case 1 :
							// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:158:7: GT ^
							{
							GT52=(Token)match(input,GT,FOLLOW_GT_in_prec4623); if (state.failed) return retval;
							if ( state.backtracking==0 ) {
							GT52_tree = (Object)adaptor.create(GT52);
							root_0 = (Object)adaptor.becomeRoot(GT52_tree, root_0);
							}

							}
							break;
						case 2 :
							// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:158:13: LT ^
							{
							LT53=(Token)match(input,LT,FOLLOW_LT_in_prec4628); if (state.failed) return retval;
							if ( state.backtracking==0 ) {
							LT53_tree = (Object)adaptor.create(LT53);
							root_0 = (Object)adaptor.becomeRoot(LT53_tree, root_0);
							}

							}
							break;
						case 3 :
							// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:158:19: LE ^
							{
							LE54=(Token)match(input,LE,FOLLOW_LE_in_prec4633); if (state.failed) return retval;
							if ( state.backtracking==0 ) {
							LE54_tree = (Object)adaptor.create(LE54);
							root_0 = (Object)adaptor.becomeRoot(LE54_tree, root_0);
							}

							}
							break;
						case 4 :
							// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:158:25: GE ^
							{
							GE55=(Token)match(input,GE,FOLLOW_GE_in_prec4638); if (state.failed) return retval;
							if ( state.backtracking==0 ) {
							GE55_tree = (Object)adaptor.create(GE55);
							root_0 = (Object)adaptor.becomeRoot(GE55_tree, root_0);
							}

							}
							break;
						case 5 :
							// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:158:31: EQ ^
							{
							EQ56=(Token)match(input,EQ,FOLLOW_EQ_in_prec4643); if (state.failed) return retval;
							if ( state.backtracking==0 ) {
							EQ56_tree = (Object)adaptor.create(EQ56);
							root_0 = (Object)adaptor.becomeRoot(EQ56_tree, root_0);
							}

							}
							break;
						case 6 :
							// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:158:38: NE ^
							{
							NE57=(Token)match(input,NE,FOLLOW_NE_in_prec4649); if (state.failed) return retval;
							if ( state.backtracking==0 ) {
							NE57_tree = (Object)adaptor.create(NE57);
							root_0 = (Object)adaptor.becomeRoot(NE57_tree, root_0);
							}

							}
							break;

					}

					pushFollow(FOLLOW_prec3_in_prec4658);
					prec358=prec3();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, prec358.getTree());

					}
					break;

			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}

			catch (RecognitionException re) {
					reportError(re);
					recover(input,re);
			}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "prec4"


	public static class prec3_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "prec3"
	// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:163:1: prec3 : ( prec2 | rand | ENTIER '(' prec2 ')' -> ^( ENTIER prec2 ) | SIGN '(' prec2 ')' -> ^( SIGN prec2 ) | SQRT '(' prec2 ')' -> ^( SQRT prec2 ) | ABS '(' prec2 ')' -> ^( ABS prec2 ) | LN '(' prec2 ')' -> ^( LN prec2 ) | SIN '(' prec2 ')' -> ^( SIN prec2 ) | COS '(' prec2 ')' -> ^( COS prec2 ) | TAN '(' prec2 ')' -> ^( TAN prec2 ) | ATAN '(' prec2 ')' -> ^( ATAN prec2 ) | COT '(' prec2 ')' -> ^( COT prec2 ) );
	public final ECLParser.prec3_return prec3() throws RecognitionException {
		ECLParser.prec3_return retval = new ECLParser.prec3_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token ENTIER61=null;
		Token char_literal62=null;
		Token char_literal64=null;
		Token SIGN65=null;
		Token char_literal66=null;
		Token char_literal68=null;
		Token SQRT69=null;
		Token char_literal70=null;
		Token char_literal72=null;
		Token ABS73=null;
		Token char_literal74=null;
		Token char_literal76=null;
		Token LN77=null;
		Token char_literal78=null;
		Token char_literal80=null;
		Token SIN81=null;
		Token char_literal82=null;
		Token char_literal84=null;
		Token COS85=null;
		Token char_literal86=null;
		Token char_literal88=null;
		Token TAN89=null;
		Token char_literal90=null;
		Token char_literal92=null;
		Token ATAN93=null;
		Token char_literal94=null;
		Token char_literal96=null;
		Token COT97=null;
		Token char_literal98=null;
		Token char_literal100=null;
		ParserRuleReturnScope prec259 =null;
		ParserRuleReturnScope rand60 =null;
		ParserRuleReturnScope prec263 =null;
		ParserRuleReturnScope prec267 =null;
		ParserRuleReturnScope prec271 =null;
		ParserRuleReturnScope prec275 =null;
		ParserRuleReturnScope prec279 =null;
		ParserRuleReturnScope prec283 =null;
		ParserRuleReturnScope prec287 =null;
		ParserRuleReturnScope prec291 =null;
		ParserRuleReturnScope prec295 =null;
		ParserRuleReturnScope prec299 =null;

		Object ENTIER61_tree=null;
		Object char_literal62_tree=null;
		Object char_literal64_tree=null;
		Object SIGN65_tree=null;
		Object char_literal66_tree=null;
		Object char_literal68_tree=null;
		Object SQRT69_tree=null;
		Object char_literal70_tree=null;
		Object char_literal72_tree=null;
		Object ABS73_tree=null;
		Object char_literal74_tree=null;
		Object char_literal76_tree=null;
		Object LN77_tree=null;
		Object char_literal78_tree=null;
		Object char_literal80_tree=null;
		Object SIN81_tree=null;
		Object char_literal82_tree=null;
		Object char_literal84_tree=null;
		Object COS85_tree=null;
		Object char_literal86_tree=null;
		Object char_literal88_tree=null;
		Object TAN89_tree=null;
		Object char_literal90_tree=null;
		Object char_literal92_tree=null;
		Object ATAN93_tree=null;
		Object char_literal94_tree=null;
		Object char_literal96_tree=null;
		Object COT97_tree=null;
		Object char_literal98_tree=null;
		Object char_literal100_tree=null;
		RewriteRuleTokenStream stream_ABS=new RewriteRuleTokenStream(adaptor,"token ABS");
		RewriteRuleTokenStream stream_SIGN=new RewriteRuleTokenStream(adaptor,"token SIGN");
		RewriteRuleTokenStream stream_69=new RewriteRuleTokenStream(adaptor,"token 69");
		RewriteRuleTokenStream stream_68=new RewriteRuleTokenStream(adaptor,"token 68");
		RewriteRuleTokenStream stream_COT=new RewriteRuleTokenStream(adaptor,"token COT");
		RewriteRuleTokenStream stream_ATAN=new RewriteRuleTokenStream(adaptor,"token ATAN");
		RewriteRuleTokenStream stream_SIN=new RewriteRuleTokenStream(adaptor,"token SIN");
		RewriteRuleTokenStream stream_SQRT=new RewriteRuleTokenStream(adaptor,"token SQRT");
		RewriteRuleTokenStream stream_LN=new RewriteRuleTokenStream(adaptor,"token LN");
		RewriteRuleTokenStream stream_COS=new RewriteRuleTokenStream(adaptor,"token COS");
		RewriteRuleTokenStream stream_ENTIER=new RewriteRuleTokenStream(adaptor,"token ENTIER");
		RewriteRuleTokenStream stream_TAN=new RewriteRuleTokenStream(adaptor,"token TAN");
		RewriteRuleSubtreeStream stream_prec2=new RewriteRuleSubtreeStream(adaptor,"rule prec2");

		try {
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:163:9: ( prec2 | rand | ENTIER '(' prec2 ')' -> ^( ENTIER prec2 ) | SIGN '(' prec2 ')' -> ^( SIGN prec2 ) | SQRT '(' prec2 ')' -> ^( SQRT prec2 ) | ABS '(' prec2 ')' -> ^( ABS prec2 ) | LN '(' prec2 ')' -> ^( LN prec2 ) | SIN '(' prec2 ')' -> ^( SIN prec2 ) | COS '(' prec2 ')' -> ^( COS prec2 ) | TAN '(' prec2 ')' -> ^( TAN prec2 ) | ATAN '(' prec2 ')' -> ^( ATAN prec2 ) | COT '(' prec2 ')' -> ^( COT prec2 ) )
			int alt14=12;
			switch ( input.LA(1) ) {
			case AGGREGATE:
			case EXP:
			case FALSE:
			case ID:
			case INT:
			case MALFORMED_ID:
			case MARK:
			case MINUS:
			case NOT:
			case PLACE:
			case REAL:
			case ROOT_AGG:
			case STR:
			case SYS_VAR:
			case TIME:
			case TRUE:
			case 68:
				{
				alt14=1;
				}
				break;
			case BINOMIAL:
			case EXPONENTIAL:
			case NORMAL:
			case POISSON:
			case UNIFORM:
				{
				alt14=2;
				}
				break;
			case ENTIER:
				{
				alt14=3;
				}
				break;
			case SIGN:
				{
				alt14=4;
				}
				break;
			case SQRT:
				{
				alt14=5;
				}
				break;
			case ABS:
				{
				alt14=6;
				}
				break;
			case LN:
				{
				alt14=7;
				}
				break;
			case SIN:
				{
				alt14=8;
				}
				break;
			case COS:
				{
				alt14=9;
				}
				break;
			case TAN:
				{
				alt14=10;
				}
				break;
			case ATAN:
				{
				alt14=11;
				}
				break;
			case COT:
				{
				alt14=12;
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 14, 0, input);
				throw nvae;
			}
			switch (alt14) {
				case 1 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:163:12: prec2
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_prec2_in_prec3680);
					prec259=prec2();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, prec259.getTree());

					}
					break;
				case 2 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:164:6: rand
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_rand_in_prec3687);
					rand60=rand();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, rand60.getTree());

					}
					break;
				case 3 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:165:6: ENTIER '(' prec2 ')'
					{
					ENTIER61=(Token)match(input,ENTIER,FOLLOW_ENTIER_in_prec3694); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_ENTIER.add(ENTIER61);

					char_literal62=(Token)match(input,68,FOLLOW_68_in_prec3696); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_68.add(char_literal62);

					pushFollow(FOLLOW_prec2_in_prec3698);
					prec263=prec2();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_prec2.add(prec263.getTree());
					char_literal64=(Token)match(input,69,FOLLOW_69_in_prec3700); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_69.add(char_literal64);

					// AST REWRITE
					// elements: prec2, ENTIER
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 165:27: -> ^( ENTIER prec2 )
					{
						// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:165:30: ^( ENTIER prec2 )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot(stream_ENTIER.nextNode(), root_1);
						adaptor.addChild(root_1, stream_prec2.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 4 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:166:6: SIGN '(' prec2 ')'
					{
					SIGN65=(Token)match(input,SIGN,FOLLOW_SIGN_in_prec3715); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_SIGN.add(SIGN65);

					char_literal66=(Token)match(input,68,FOLLOW_68_in_prec3717); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_68.add(char_literal66);

					pushFollow(FOLLOW_prec2_in_prec3719);
					prec267=prec2();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_prec2.add(prec267.getTree());
					char_literal68=(Token)match(input,69,FOLLOW_69_in_prec3721); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_69.add(char_literal68);

					// AST REWRITE
					// elements: SIGN, prec2
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 166:25: -> ^( SIGN prec2 )
					{
						// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:166:28: ^( SIGN prec2 )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot(stream_SIGN.nextNode(), root_1);
						adaptor.addChild(root_1, stream_prec2.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 5 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:167:6: SQRT '(' prec2 ')'
					{
					SQRT69=(Token)match(input,SQRT,FOLLOW_SQRT_in_prec3736); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_SQRT.add(SQRT69);

					char_literal70=(Token)match(input,68,FOLLOW_68_in_prec3738); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_68.add(char_literal70);

					pushFollow(FOLLOW_prec2_in_prec3740);
					prec271=prec2();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_prec2.add(prec271.getTree());
					char_literal72=(Token)match(input,69,FOLLOW_69_in_prec3742); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_69.add(char_literal72);

					// AST REWRITE
					// elements: prec2, SQRT
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 167:25: -> ^( SQRT prec2 )
					{
						// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:167:28: ^( SQRT prec2 )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot(stream_SQRT.nextNode(), root_1);
						adaptor.addChild(root_1, stream_prec2.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 6 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:168:6: ABS '(' prec2 ')'
					{
					ABS73=(Token)match(input,ABS,FOLLOW_ABS_in_prec3757); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_ABS.add(ABS73);

					char_literal74=(Token)match(input,68,FOLLOW_68_in_prec3759); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_68.add(char_literal74);

					pushFollow(FOLLOW_prec2_in_prec3761);
					prec275=prec2();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_prec2.add(prec275.getTree());
					char_literal76=(Token)match(input,69,FOLLOW_69_in_prec3763); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_69.add(char_literal76);

					// AST REWRITE
					// elements: prec2, ABS
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 168:24: -> ^( ABS prec2 )
					{
						// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:168:27: ^( ABS prec2 )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot(stream_ABS.nextNode(), root_1);
						adaptor.addChild(root_1, stream_prec2.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 7 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:169:6: LN '(' prec2 ')'
					{
					LN77=(Token)match(input,LN,FOLLOW_LN_in_prec3778); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LN.add(LN77);

					char_literal78=(Token)match(input,68,FOLLOW_68_in_prec3780); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_68.add(char_literal78);

					pushFollow(FOLLOW_prec2_in_prec3782);
					prec279=prec2();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_prec2.add(prec279.getTree());
					char_literal80=(Token)match(input,69,FOLLOW_69_in_prec3784); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_69.add(char_literal80);

					// AST REWRITE
					// elements: prec2, LN
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 169:23: -> ^( LN prec2 )
					{
						// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:169:26: ^( LN prec2 )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot(stream_LN.nextNode(), root_1);
						adaptor.addChild(root_1, stream_prec2.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 8 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:170:6: SIN '(' prec2 ')'
					{
					SIN81=(Token)match(input,SIN,FOLLOW_SIN_in_prec3799); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_SIN.add(SIN81);

					char_literal82=(Token)match(input,68,FOLLOW_68_in_prec3801); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_68.add(char_literal82);

					pushFollow(FOLLOW_prec2_in_prec3803);
					prec283=prec2();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_prec2.add(prec283.getTree());
					char_literal84=(Token)match(input,69,FOLLOW_69_in_prec3805); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_69.add(char_literal84);

					// AST REWRITE
					// elements: prec2, SIN
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 170:24: -> ^( SIN prec2 )
					{
						// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:170:27: ^( SIN prec2 )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot(stream_SIN.nextNode(), root_1);
						adaptor.addChild(root_1, stream_prec2.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 9 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:171:6: COS '(' prec2 ')'
					{
					COS85=(Token)match(input,COS,FOLLOW_COS_in_prec3820); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_COS.add(COS85);

					char_literal86=(Token)match(input,68,FOLLOW_68_in_prec3822); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_68.add(char_literal86);

					pushFollow(FOLLOW_prec2_in_prec3824);
					prec287=prec2();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_prec2.add(prec287.getTree());
					char_literal88=(Token)match(input,69,FOLLOW_69_in_prec3826); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_69.add(char_literal88);

					// AST REWRITE
					// elements: prec2, COS
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 171:24: -> ^( COS prec2 )
					{
						// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:171:27: ^( COS prec2 )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot(stream_COS.nextNode(), root_1);
						adaptor.addChild(root_1, stream_prec2.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 10 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:172:6: TAN '(' prec2 ')'
					{
					TAN89=(Token)match(input,TAN,FOLLOW_TAN_in_prec3841); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_TAN.add(TAN89);

					char_literal90=(Token)match(input,68,FOLLOW_68_in_prec3843); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_68.add(char_literal90);

					pushFollow(FOLLOW_prec2_in_prec3845);
					prec291=prec2();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_prec2.add(prec291.getTree());
					char_literal92=(Token)match(input,69,FOLLOW_69_in_prec3847); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_69.add(char_literal92);

					// AST REWRITE
					// elements: TAN, prec2
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 172:24: -> ^( TAN prec2 )
					{
						// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:172:27: ^( TAN prec2 )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot(stream_TAN.nextNode(), root_1);
						adaptor.addChild(root_1, stream_prec2.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 11 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:173:6: ATAN '(' prec2 ')'
					{
					ATAN93=(Token)match(input,ATAN,FOLLOW_ATAN_in_prec3862); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_ATAN.add(ATAN93);

					char_literal94=(Token)match(input,68,FOLLOW_68_in_prec3864); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_68.add(char_literal94);

					pushFollow(FOLLOW_prec2_in_prec3866);
					prec295=prec2();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_prec2.add(prec295.getTree());
					char_literal96=(Token)match(input,69,FOLLOW_69_in_prec3868); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_69.add(char_literal96);

					// AST REWRITE
					// elements: ATAN, prec2
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 173:25: -> ^( ATAN prec2 )
					{
						// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:173:28: ^( ATAN prec2 )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot(stream_ATAN.nextNode(), root_1);
						adaptor.addChild(root_1, stream_prec2.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 12 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:174:6: COT '(' prec2 ')'
					{
					COT97=(Token)match(input,COT,FOLLOW_COT_in_prec3883); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_COT.add(COT97);

					char_literal98=(Token)match(input,68,FOLLOW_68_in_prec3885); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_68.add(char_literal98);

					pushFollow(FOLLOW_prec2_in_prec3887);
					prec299=prec2();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_prec2.add(prec299.getTree());
					char_literal100=(Token)match(input,69,FOLLOW_69_in_prec3889); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_69.add(char_literal100);

					// AST REWRITE
					// elements: prec2, COT
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 174:24: -> ^( COT prec2 )
					{
						// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:174:27: ^( COT prec2 )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot(stream_COT.nextNode(), root_1);
						adaptor.addChild(root_1, stream_prec2.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}

			catch (RecognitionException re) {
					reportError(re);
					recover(input,re);
			}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "prec3"


	public static class prec2_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "prec2"
	// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:177:1: prec2 : prec1 ( PLUS ^ prec1 | MINUS ^ prec1 )* ;
	public final ECLParser.prec2_return prec2() throws RecognitionException {
		ECLParser.prec2_return retval = new ECLParser.prec2_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token PLUS102=null;
		Token MINUS104=null;
		ParserRuleReturnScope prec1101 =null;
		ParserRuleReturnScope prec1103 =null;
		ParserRuleReturnScope prec1105 =null;

		Object PLUS102_tree=null;
		Object MINUS104_tree=null;

		try {
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:177:8: ( prec1 ( PLUS ^ prec1 | MINUS ^ prec1 )* )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:177:10: prec1 ( PLUS ^ prec1 | MINUS ^ prec1 )*
			{
			root_0 = (Object)adaptor.nil();


			pushFollow(FOLLOW_prec1_in_prec2910);
			prec1101=prec1();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, prec1101.getTree());

			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:177:16: ( PLUS ^ prec1 | MINUS ^ prec1 )*
			loop15:
			while (true) {
				int alt15=3;
				int LA15_0 = input.LA(1);
				if ( (LA15_0==PLUS) ) {
					alt15=1;
				}
				else if ( (LA15_0==MINUS) ) {
					alt15=2;
				}

				switch (alt15) {
				case 1 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:177:17: PLUS ^ prec1
					{
					PLUS102=(Token)match(input,PLUS,FOLLOW_PLUS_in_prec2913); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					PLUS102_tree = (Object)adaptor.create(PLUS102);
					root_0 = (Object)adaptor.becomeRoot(PLUS102_tree, root_0);
					}

					pushFollow(FOLLOW_prec1_in_prec2916);
					prec1103=prec1();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, prec1103.getTree());

					}
					break;
				case 2 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:177:31: MINUS ^ prec1
					{
					MINUS104=(Token)match(input,MINUS,FOLLOW_MINUS_in_prec2920); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					MINUS104_tree = (Object)adaptor.create(MINUS104);
					root_0 = (Object)adaptor.becomeRoot(MINUS104_tree, root_0);
					}

					pushFollow(FOLLOW_prec1_in_prec2923);
					prec1105=prec1();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, prec1105.getTree());

					}
					break;

				default :
					break loop15;
				}
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}

			catch (RecognitionException re) {
					reportError(re);
					recover(input,re);
			}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "prec2"


	public static class prec1_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "prec1"
	// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:180:1: prec1 : pow ( MULT ^ pow | DIV ^ pow )* ;
	public final ECLParser.prec1_return prec1() throws RecognitionException {
		ECLParser.prec1_return retval = new ECLParser.prec1_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token MULT107=null;
		Token DIV109=null;
		ParserRuleReturnScope pow106 =null;
		ParserRuleReturnScope pow108 =null;
		ParserRuleReturnScope pow110 =null;

		Object MULT107_tree=null;
		Object DIV109_tree=null;

		try {
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:180:8: ( pow ( MULT ^ pow | DIV ^ pow )* )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:180:10: pow ( MULT ^ pow | DIV ^ pow )*
			{
			root_0 = (Object)adaptor.nil();


			pushFollow(FOLLOW_pow_in_prec1938);
			pow106=pow();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, pow106.getTree());

			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:180:14: ( MULT ^ pow | DIV ^ pow )*
			loop16:
			while (true) {
				int alt16=3;
				int LA16_0 = input.LA(1);
				if ( (LA16_0==MULT) ) {
					alt16=1;
				}
				else if ( (LA16_0==DIV) ) {
					alt16=2;
				}

				switch (alt16) {
				case 1 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:180:15: MULT ^ pow
					{
					MULT107=(Token)match(input,MULT,FOLLOW_MULT_in_prec1941); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					MULT107_tree = (Object)adaptor.create(MULT107);
					root_0 = (Object)adaptor.becomeRoot(MULT107_tree, root_0);
					}

					pushFollow(FOLLOW_pow_in_prec1944);
					pow108=pow();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, pow108.getTree());

					}
					break;
				case 2 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:180:27: DIV ^ pow
					{
					DIV109=(Token)match(input,DIV,FOLLOW_DIV_in_prec1948); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					DIV109_tree = (Object)adaptor.create(DIV109);
					root_0 = (Object)adaptor.becomeRoot(DIV109_tree, root_0);
					}

					pushFollow(FOLLOW_pow_in_prec1951);
					pow110=pow();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, pow110.getTree());

					}
					break;

				default :
					break loop16;
				}
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}

			catch (RecognitionException re) {
					reportError(re);
					recover(input,re);
			}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "prec1"


	public static class pow_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "pow"
	// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:183:1: pow : prec0 ( POW ^ pow )? ;
	public final ECLParser.pow_return pow() throws RecognitionException {
		ECLParser.pow_return retval = new ECLParser.pow_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token POW112=null;
		ParserRuleReturnScope prec0111 =null;
		ParserRuleReturnScope pow113 =null;

		Object POW112_tree=null;

		try {
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:183:7: ( prec0 ( POW ^ pow )? )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:183:9: prec0 ( POW ^ pow )?
			{
			root_0 = (Object)adaptor.nil();


			pushFollow(FOLLOW_prec0_in_pow967);
			prec0111=prec0();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, prec0111.getTree());

			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:183:15: ( POW ^ pow )?
			int alt17=2;
			int LA17_0 = input.LA(1);
			if ( (LA17_0==POW) ) {
				alt17=1;
			}
			switch (alt17) {
				case 1 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:183:16: POW ^ pow
					{
					POW112=(Token)match(input,POW,FOLLOW_POW_in_pow970); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					POW112_tree = (Object)adaptor.create(POW112);
					root_0 = (Object)adaptor.becomeRoot(POW112_tree, root_0);
					}

					pushFollow(FOLLOW_pow_in_pow973);
					pow113=pow();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, pow113.getTree());

					}
					break;

			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}

			catch (RecognitionException re) {
					reportError(re);
					recover(input,re);
			}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "pow"


	public static class prec0_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "prec0"
	// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:186:1: prec0 : ( atom | ( MINUS | NOT ) prec0 -> ^( INVERT prec0 ) );
	public final ECLParser.prec0_return prec0() throws RecognitionException {
		ECLParser.prec0_return retval = new ECLParser.prec0_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token MINUS115=null;
		Token NOT116=null;
		ParserRuleReturnScope atom114 =null;
		ParserRuleReturnScope prec0117 =null;

		Object MINUS115_tree=null;
		Object NOT116_tree=null;
		RewriteRuleTokenStream stream_NOT=new RewriteRuleTokenStream(adaptor,"token NOT");
		RewriteRuleTokenStream stream_MINUS=new RewriteRuleTokenStream(adaptor,"token MINUS");
		RewriteRuleSubtreeStream stream_prec0=new RewriteRuleSubtreeStream(adaptor,"rule prec0");

		try {
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:186:8: ( atom | ( MINUS | NOT ) prec0 -> ^( INVERT prec0 ) )
			int alt19=2;
			int LA19_0 = input.LA(1);
			if ( (LA19_0==AGGREGATE||LA19_0==EXP||LA19_0==FALSE||LA19_0==ID||LA19_0==INT||(LA19_0 >= MALFORMED_ID && LA19_0 <= MARK)||LA19_0==PLACE||LA19_0==REAL||LA19_0==ROOT_AGG||(LA19_0 >= STR && LA19_0 <= SYS_VAR)||(LA19_0 >= TIME && LA19_0 <= TRUE)||LA19_0==68) ) {
				alt19=1;
			}
			else if ( (LA19_0==MINUS||LA19_0==NOT) ) {
				alt19=2;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 19, 0, input);
				throw nvae;
			}

			switch (alt19) {
				case 1 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:186:10: atom
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_atom_in_prec0994);
					atom114=atom();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, atom114.getTree());

					}
					break;
				case 2 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:187:6: ( MINUS | NOT ) prec0
					{
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:187:6: ( MINUS | NOT )
					int alt18=2;
					int LA18_0 = input.LA(1);
					if ( (LA18_0==MINUS) ) {
						alt18=1;
					}
					else if ( (LA18_0==NOT) ) {
						alt18=2;
					}

					else {
						if (state.backtracking>0) {state.failed=true; return retval;}
						NoViableAltException nvae =
							new NoViableAltException("", 18, 0, input);
						throw nvae;
					}

					switch (alt18) {
						case 1 :
							// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:187:7: MINUS
							{
							MINUS115=(Token)match(input,MINUS,FOLLOW_MINUS_in_prec01002); if (state.failed) return retval; 
							if ( state.backtracking==0 ) stream_MINUS.add(MINUS115);

							}
							break;
						case 2 :
							// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:187:15: NOT
							{
							NOT116=(Token)match(input,NOT,FOLLOW_NOT_in_prec01006); if (state.failed) return retval; 
							if ( state.backtracking==0 ) stream_NOT.add(NOT116);

							}
							break;

					}

					pushFollow(FOLLOW_prec0_in_prec01009);
					prec0117=prec0();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_prec0.add(prec0117.getTree());
					// AST REWRITE
					// elements: prec0
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 187:26: -> ^( INVERT prec0 )
					{
						// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:187:29: ^( INVERT prec0 )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(INVERT, "INVERT"), root_1);
						adaptor.addChild(root_1, stream_prec0.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}

			catch (RecognitionException re) {
					reportError(re);
					recover(input,re);
			}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "prec0"


	public static class atom_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "atom"
	// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:191:1: atom : ( INT | REAL | id | '(' expr ')' -> expr | STR | TRUE | FALSE | EXP | sys_atom );
	public final ECLParser.atom_return atom() throws RecognitionException {
		ECLParser.atom_return retval = new ECLParser.atom_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token INT118=null;
		Token REAL119=null;
		Token char_literal121=null;
		Token char_literal123=null;
		Token STR124=null;
		Token TRUE125=null;
		Token FALSE126=null;
		Token EXP127=null;
		ParserRuleReturnScope id120 =null;
		ParserRuleReturnScope expr122 =null;
		ParserRuleReturnScope sys_atom128 =null;

		Object INT118_tree=null;
		Object REAL119_tree=null;
		Object char_literal121_tree=null;
		Object char_literal123_tree=null;
		Object STR124_tree=null;
		Object TRUE125_tree=null;
		Object FALSE126_tree=null;
		Object EXP127_tree=null;
		RewriteRuleTokenStream stream_69=new RewriteRuleTokenStream(adaptor,"token 69");
		RewriteRuleTokenStream stream_68=new RewriteRuleTokenStream(adaptor,"token 68");
		RewriteRuleSubtreeStream stream_expr=new RewriteRuleSubtreeStream(adaptor,"rule expr");

		try {
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:191:7: ( INT | REAL | id | '(' expr ')' -> expr | STR | TRUE | FALSE | EXP | sys_atom )
			int alt20=9;
			switch ( input.LA(1) ) {
			case INT:
				{
				alt20=1;
				}
				break;
			case REAL:
				{
				alt20=2;
				}
				break;
			case ID:
			case MALFORMED_ID:
				{
				alt20=3;
				}
				break;
			case 68:
				{
				alt20=4;
				}
				break;
			case STR:
				{
				alt20=5;
				}
				break;
			case TRUE:
				{
				alt20=6;
				}
				break;
			case FALSE:
				{
				alt20=7;
				}
				break;
			case EXP:
				{
				alt20=8;
				}
				break;
			case AGGREGATE:
			case MARK:
			case PLACE:
			case ROOT_AGG:
			case SYS_VAR:
			case TIME:
				{
				alt20=9;
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 20, 0, input);
				throw nvae;
			}
			switch (alt20) {
				case 1 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:191:9: INT
					{
					root_0 = (Object)adaptor.nil();


					INT118=(Token)match(input,INT,FOLLOW_INT_in_atom1034); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					INT118_tree = (Object)adaptor.create(INT118);
					adaptor.addChild(root_0, INT118_tree);
					}

					}
					break;
				case 2 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:192:6: REAL
					{
					root_0 = (Object)adaptor.nil();


					REAL119=(Token)match(input,REAL,FOLLOW_REAL_in_atom1041); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					REAL119_tree = (Object)adaptor.create(REAL119);
					adaptor.addChild(root_0, REAL119_tree);
					}

					}
					break;
				case 3 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:193:6: id
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_id_in_atom1048);
					id120=id();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, id120.getTree());

					}
					break;
				case 4 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:194:7: '(' expr ')'
					{
					char_literal121=(Token)match(input,68,FOLLOW_68_in_atom1056); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_68.add(char_literal121);

					pushFollow(FOLLOW_expr_in_atom1058);
					expr122=expr();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_expr.add(expr122.getTree());
					char_literal123=(Token)match(input,69,FOLLOW_69_in_atom1060); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_69.add(char_literal123);

					// AST REWRITE
					// elements: expr
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 194:20: -> expr
					{
						adaptor.addChild(root_0, stream_expr.nextTree());
					}


					retval.tree = root_0;
					}

					}
					break;
				case 5 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:195:6: STR
					{
					root_0 = (Object)adaptor.nil();


					STR124=(Token)match(input,STR,FOLLOW_STR_in_atom1071); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					STR124_tree = (Object)adaptor.create(STR124);
					adaptor.addChild(root_0, STR124_tree);
					}

					}
					break;
				case 6 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:196:8: TRUE
					{
					root_0 = (Object)adaptor.nil();


					TRUE125=(Token)match(input,TRUE,FOLLOW_TRUE_in_atom1080); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					TRUE125_tree = (Object)adaptor.create(TRUE125);
					adaptor.addChild(root_0, TRUE125_tree);
					}

					}
					break;
				case 7 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:197:8: FALSE
					{
					root_0 = (Object)adaptor.nil();


					FALSE126=(Token)match(input,FALSE,FOLLOW_FALSE_in_atom1089); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					FALSE126_tree = (Object)adaptor.create(FALSE126);
					adaptor.addChild(root_0, FALSE126_tree);
					}

					}
					break;
				case 8 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:198:6: EXP
					{
					root_0 = (Object)adaptor.nil();


					EXP127=(Token)match(input,EXP,FOLLOW_EXP_in_atom1096); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					EXP127_tree = (Object)adaptor.create(EXP127);
					adaptor.addChild(root_0, EXP127_tree);
					}

					}
					break;
				case 9 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:199:6: sys_atom
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_sys_atom_in_atom1103);
					sys_atom128=sys_atom();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, sys_atom128.getTree());

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}

			catch (RecognitionException re) {
					reportError(re);
					recover(input,re);
			}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "atom"


	public static class id_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "id"
	// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:202:1: id : ( ID | MALFORMED_ID );
	public final ECLParser.id_return id() throws RecognitionException {
		ECLParser.id_return retval = new ECLParser.id_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token set129=null;

		Object set129_tree=null;

		try {
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:202:6: ( ID | MALFORMED_ID )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:
			{
			root_0 = (Object)adaptor.nil();


			set129=input.LT(1);
			if ( input.LA(1)==ID||input.LA(1)==MALFORMED_ID ) {
				input.consume();
				if ( state.backtracking==0 ) adaptor.addChild(root_0, (Object)adaptor.create(set129));
				state.errorRecovery=false;
				state.failed=false;
			}
			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				MismatchedSetException mse = new MismatchedSetException(null,input);
				throw mse;
			}
			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}

			catch (RecognitionException re) {
					reportError(re);
					recover(input,re);
			}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "id"


	public static class rand_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "rand"
	// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:206:1: rand : ( UNIFORM '(' expr ',' expr ')' -> ^( UNIFORM expr expr ) | POISSON '(' expr ')' -> ^( POISSON expr ) | EXPONENTIAL '(' expr ')' -> ^( EXPONENTIAL expr ) | NORMAL '(' expr ',' expr ')' -> ^( NORMAL expr expr ) | BINOMIAL '(' expr ',' expr ')' -> ^( BINOMIAL expr expr ) );
	public final ECLParser.rand_return rand() throws RecognitionException {
		ECLParser.rand_return retval = new ECLParser.rand_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token UNIFORM130=null;
		Token char_literal131=null;
		Token char_literal133=null;
		Token char_literal135=null;
		Token POISSON136=null;
		Token char_literal137=null;
		Token char_literal139=null;
		Token EXPONENTIAL140=null;
		Token char_literal141=null;
		Token char_literal143=null;
		Token NORMAL144=null;
		Token char_literal145=null;
		Token char_literal147=null;
		Token char_literal149=null;
		Token BINOMIAL150=null;
		Token char_literal151=null;
		Token char_literal153=null;
		Token char_literal155=null;
		ParserRuleReturnScope expr132 =null;
		ParserRuleReturnScope expr134 =null;
		ParserRuleReturnScope expr138 =null;
		ParserRuleReturnScope expr142 =null;
		ParserRuleReturnScope expr146 =null;
		ParserRuleReturnScope expr148 =null;
		ParserRuleReturnScope expr152 =null;
		ParserRuleReturnScope expr154 =null;

		Object UNIFORM130_tree=null;
		Object char_literal131_tree=null;
		Object char_literal133_tree=null;
		Object char_literal135_tree=null;
		Object POISSON136_tree=null;
		Object char_literal137_tree=null;
		Object char_literal139_tree=null;
		Object EXPONENTIAL140_tree=null;
		Object char_literal141_tree=null;
		Object char_literal143_tree=null;
		Object NORMAL144_tree=null;
		Object char_literal145_tree=null;
		Object char_literal147_tree=null;
		Object char_literal149_tree=null;
		Object BINOMIAL150_tree=null;
		Object char_literal151_tree=null;
		Object char_literal153_tree=null;
		Object char_literal155_tree=null;
		RewriteRuleTokenStream stream_69=new RewriteRuleTokenStream(adaptor,"token 69");
		RewriteRuleTokenStream stream_68=new RewriteRuleTokenStream(adaptor,"token 68");
		RewriteRuleTokenStream stream_EXPONENTIAL=new RewriteRuleTokenStream(adaptor,"token EXPONENTIAL");
		RewriteRuleTokenStream stream_UNIFORM=new RewriteRuleTokenStream(adaptor,"token UNIFORM");
		RewriteRuleTokenStream stream_NORMAL=new RewriteRuleTokenStream(adaptor,"token NORMAL");
		RewriteRuleTokenStream stream_BINOMIAL=new RewriteRuleTokenStream(adaptor,"token BINOMIAL");
		RewriteRuleTokenStream stream_70=new RewriteRuleTokenStream(adaptor,"token 70");
		RewriteRuleTokenStream stream_POISSON=new RewriteRuleTokenStream(adaptor,"token POISSON");
		RewriteRuleSubtreeStream stream_expr=new RewriteRuleSubtreeStream(adaptor,"rule expr");

		try {
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:206:7: ( UNIFORM '(' expr ',' expr ')' -> ^( UNIFORM expr expr ) | POISSON '(' expr ')' -> ^( POISSON expr ) | EXPONENTIAL '(' expr ')' -> ^( EXPONENTIAL expr ) | NORMAL '(' expr ',' expr ')' -> ^( NORMAL expr expr ) | BINOMIAL '(' expr ',' expr ')' -> ^( BINOMIAL expr expr ) )
			int alt21=5;
			switch ( input.LA(1) ) {
			case UNIFORM:
				{
				alt21=1;
				}
				break;
			case POISSON:
				{
				alt21=2;
				}
				break;
			case EXPONENTIAL:
				{
				alt21=3;
				}
				break;
			case NORMAL:
				{
				alt21=4;
				}
				break;
			case BINOMIAL:
				{
				alt21=5;
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 21, 0, input);
				throw nvae;
			}
			switch (alt21) {
				case 1 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:206:9: UNIFORM '(' expr ',' expr ')'
					{
					UNIFORM130=(Token)match(input,UNIFORM,FOLLOW_UNIFORM_in_rand1143); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_UNIFORM.add(UNIFORM130);

					char_literal131=(Token)match(input,68,FOLLOW_68_in_rand1145); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_68.add(char_literal131);

					pushFollow(FOLLOW_expr_in_rand1147);
					expr132=expr();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_expr.add(expr132.getTree());
					char_literal133=(Token)match(input,70,FOLLOW_70_in_rand1149); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_70.add(char_literal133);

					pushFollow(FOLLOW_expr_in_rand1151);
					expr134=expr();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_expr.add(expr134.getTree());
					char_literal135=(Token)match(input,69,FOLLOW_69_in_rand1153); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_69.add(char_literal135);

					// AST REWRITE
					// elements: UNIFORM, expr, expr
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 206:39: -> ^( UNIFORM expr expr )
					{
						// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:206:42: ^( UNIFORM expr expr )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot(stream_UNIFORM.nextNode(), root_1);
						adaptor.addChild(root_1, stream_expr.nextTree());
						adaptor.addChild(root_1, stream_expr.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 2 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:207:6: POISSON '(' expr ')'
					{
					POISSON136=(Token)match(input,POISSON,FOLLOW_POISSON_in_rand1171); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_POISSON.add(POISSON136);

					char_literal137=(Token)match(input,68,FOLLOW_68_in_rand1173); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_68.add(char_literal137);

					pushFollow(FOLLOW_expr_in_rand1175);
					expr138=expr();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_expr.add(expr138.getTree());
					char_literal139=(Token)match(input,69,FOLLOW_69_in_rand1177); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_69.add(char_literal139);

					// AST REWRITE
					// elements: expr, POISSON
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 207:27: -> ^( POISSON expr )
					{
						// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:207:30: ^( POISSON expr )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot(stream_POISSON.nextNode(), root_1);
						adaptor.addChild(root_1, stream_expr.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 3 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:208:6: EXPONENTIAL '(' expr ')'
					{
					EXPONENTIAL140=(Token)match(input,EXPONENTIAL,FOLLOW_EXPONENTIAL_in_rand1192); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_EXPONENTIAL.add(EXPONENTIAL140);

					char_literal141=(Token)match(input,68,FOLLOW_68_in_rand1194); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_68.add(char_literal141);

					pushFollow(FOLLOW_expr_in_rand1196);
					expr142=expr();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_expr.add(expr142.getTree());
					char_literal143=(Token)match(input,69,FOLLOW_69_in_rand1198); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_69.add(char_literal143);

					// AST REWRITE
					// elements: EXPONENTIAL, expr
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 208:31: -> ^( EXPONENTIAL expr )
					{
						// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:208:34: ^( EXPONENTIAL expr )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot(stream_EXPONENTIAL.nextNode(), root_1);
						adaptor.addChild(root_1, stream_expr.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 4 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:209:6: NORMAL '(' expr ',' expr ')'
					{
					NORMAL144=(Token)match(input,NORMAL,FOLLOW_NORMAL_in_rand1213); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_NORMAL.add(NORMAL144);

					char_literal145=(Token)match(input,68,FOLLOW_68_in_rand1215); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_68.add(char_literal145);

					pushFollow(FOLLOW_expr_in_rand1217);
					expr146=expr();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_expr.add(expr146.getTree());
					char_literal147=(Token)match(input,70,FOLLOW_70_in_rand1219); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_70.add(char_literal147);

					pushFollow(FOLLOW_expr_in_rand1221);
					expr148=expr();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_expr.add(expr148.getTree());
					char_literal149=(Token)match(input,69,FOLLOW_69_in_rand1223); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_69.add(char_literal149);

					// AST REWRITE
					// elements: NORMAL, expr, expr
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 209:35: -> ^( NORMAL expr expr )
					{
						// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:209:38: ^( NORMAL expr expr )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot(stream_NORMAL.nextNode(), root_1);
						adaptor.addChild(root_1, stream_expr.nextTree());
						adaptor.addChild(root_1, stream_expr.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 5 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:210:6: BINOMIAL '(' expr ',' expr ')'
					{
					BINOMIAL150=(Token)match(input,BINOMIAL,FOLLOW_BINOMIAL_in_rand1240); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_BINOMIAL.add(BINOMIAL150);

					char_literal151=(Token)match(input,68,FOLLOW_68_in_rand1242); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_68.add(char_literal151);

					pushFollow(FOLLOW_expr_in_rand1244);
					expr152=expr();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_expr.add(expr152.getTree());
					char_literal153=(Token)match(input,70,FOLLOW_70_in_rand1246); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_70.add(char_literal153);

					pushFollow(FOLLOW_expr_in_rand1248);
					expr154=expr();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_expr.add(expr154.getTree());
					char_literal155=(Token)match(input,69,FOLLOW_69_in_rand1250); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_69.add(char_literal155);

					// AST REWRITE
					// elements: expr, expr, BINOMIAL
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 210:37: -> ^( BINOMIAL expr expr )
					{
						// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:210:40: ^( BINOMIAL expr expr )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot(stream_BINOMIAL.nextNode(), root_1);
						adaptor.addChild(root_1, stream_expr.nextTree());
						adaptor.addChild(root_1, stream_expr.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}

			catch (RecognitionException re) {
					reportError(re);
					recover(input,re);
			}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "rand"


	public static class sys_atom_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "sys_atom"
	// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:213:1: sys_atom : ( ( sys_id )=> sys_id | ( aggregate )? PLACE '[' expr ']' '.' MARK -> ^( IS_MARKED ( aggregate )? expr ) | TIME );
	public final ECLParser.sys_atom_return sys_atom() throws RecognitionException {
		ECLParser.sys_atom_return retval = new ECLParser.sys_atom_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token PLACE158=null;
		Token char_literal159=null;
		Token char_literal161=null;
		Token char_literal162=null;
		Token MARK163=null;
		Token TIME164=null;
		ParserRuleReturnScope sys_id156 =null;
		ParserRuleReturnScope aggregate157 =null;
		ParserRuleReturnScope expr160 =null;

		Object PLACE158_tree=null;
		Object char_literal159_tree=null;
		Object char_literal161_tree=null;
		Object char_literal162_tree=null;
		Object MARK163_tree=null;
		Object TIME164_tree=null;
		RewriteRuleTokenStream stream_PLACE=new RewriteRuleTokenStream(adaptor,"token PLACE");
		RewriteRuleTokenStream stream_MARK=new RewriteRuleTokenStream(adaptor,"token MARK");
		RewriteRuleTokenStream stream_71=new RewriteRuleTokenStream(adaptor,"token 71");
		RewriteRuleTokenStream stream_73=new RewriteRuleTokenStream(adaptor,"token 73");
		RewriteRuleTokenStream stream_74=new RewriteRuleTokenStream(adaptor,"token 74");
		RewriteRuleSubtreeStream stream_aggregate=new RewriteRuleSubtreeStream(adaptor,"rule aggregate");
		RewriteRuleSubtreeStream stream_expr=new RewriteRuleSubtreeStream(adaptor,"rule expr");

		try {
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:213:10: ( ( sys_id )=> sys_id | ( aggregate )? PLACE '[' expr ']' '.' MARK -> ^( IS_MARKED ( aggregate )? expr ) | TIME )
			int alt23=3;
			int LA23_0 = input.LA(1);
			if ( (LA23_0==AGGREGATE) ) {
				int LA23_1 = input.LA(2);
				if ( (synpred3_ECL()) ) {
					alt23=1;
				}
				else if ( (true) ) {
					alt23=2;
				}

			}
			else if ( (LA23_0==ROOT_AGG) ) {
				int LA23_2 = input.LA(2);
				if ( (synpred3_ECL()) ) {
					alt23=1;
				}
				else if ( (true) ) {
					alt23=2;
				}

			}
			else if ( (LA23_0==SYS_VAR) && (synpred3_ECL())) {
				alt23=1;
			}
			else if ( (LA23_0==PLACE) ) {
				int LA23_4 = input.LA(2);
				if ( (synpred3_ECL()) ) {
					alt23=1;
				}
				else if ( (true) ) {
					alt23=2;
				}

			}
			else if ( (LA23_0==MARK) && (synpred3_ECL())) {
				alt23=1;
			}
			else if ( (LA23_0==TIME) ) {
				alt23=3;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 23, 0, input);
				throw nvae;
			}

			switch (alt23) {
				case 1 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:213:12: ( sys_id )=> sys_id
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_sys_id_in_sys_atom1281);
					sys_id156=sys_id();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, sys_id156.getTree());

					}
					break;
				case 2 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:214:6: ( aggregate )? PLACE '[' expr ']' '.' MARK
					{
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:214:6: ( aggregate )?
					int alt22=2;
					int LA22_0 = input.LA(1);
					if ( (LA22_0==AGGREGATE||LA22_0==ROOT_AGG) ) {
						alt22=1;
					}
					switch (alt22) {
						case 1 :
							// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:214:6: aggregate
							{
							pushFollow(FOLLOW_aggregate_in_sys_atom1288);
							aggregate157=aggregate();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) stream_aggregate.add(aggregate157.getTree());
							}
							break;

					}

					PLACE158=(Token)match(input,PLACE,FOLLOW_PLACE_in_sys_atom1291); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_PLACE.add(PLACE158);

					char_literal159=(Token)match(input,73,FOLLOW_73_in_sys_atom1293); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_73.add(char_literal159);

					pushFollow(FOLLOW_expr_in_sys_atom1295);
					expr160=expr();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_expr.add(expr160.getTree());
					char_literal161=(Token)match(input,74,FOLLOW_74_in_sys_atom1297); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_74.add(char_literal161);

					char_literal162=(Token)match(input,71,FOLLOW_71_in_sys_atom1299); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_71.add(char_literal162);

					MARK163=(Token)match(input,MARK,FOLLOW_MARK_in_sys_atom1301); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_MARK.add(MARK163);

					// AST REWRITE
					// elements: aggregate, expr
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 214:46: -> ^( IS_MARKED ( aggregate )? expr )
					{
						// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:214:49: ^( IS_MARKED ( aggregate )? expr )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(IS_MARKED, "IS_MARKED"), root_1);
						// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:214:61: ( aggregate )?
						if ( stream_aggregate.hasNext() ) {
							adaptor.addChild(root_1, stream_aggregate.nextTree());
						}
						stream_aggregate.reset();

						adaptor.addChild(root_1, stream_expr.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 3 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:215:6: TIME
					{
					root_0 = (Object)adaptor.nil();


					TIME164=(Token)match(input,TIME,FOLLOW_TIME_in_sys_atom1320); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					TIME164_tree = (Object)adaptor.create(TIME164);
					adaptor.addChild(root_0, TIME164_tree);
					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}

			catch (RecognitionException re) {
					reportError(re);
					recover(input,re);
			}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "sys_atom"


	public static class sys_id_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "sys_id"
	// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:218:1: sys_id : ( ( aggregate )? ( SYS_VAR '[' expr ']' -> ^( SYS_VAR ( aggregate )? expr ) | PLACE '[' expr ']' '.' MARK '[' expr ']' -> ^( MARK ( aggregate )? expr expr ) ) | MARK '[' expr ']' -> ^( CURRENT_MARK expr ) );
	public final ECLParser.sys_id_return sys_id() throws RecognitionException {
		ECLParser.sys_id_return retval = new ECLParser.sys_id_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token SYS_VAR166=null;
		Token char_literal167=null;
		Token char_literal169=null;
		Token PLACE170=null;
		Token char_literal171=null;
		Token char_literal173=null;
		Token char_literal174=null;
		Token MARK175=null;
		Token char_literal176=null;
		Token char_literal178=null;
		Token MARK179=null;
		Token char_literal180=null;
		Token char_literal182=null;
		ParserRuleReturnScope aggregate165 =null;
		ParserRuleReturnScope expr168 =null;
		ParserRuleReturnScope expr172 =null;
		ParserRuleReturnScope expr177 =null;
		ParserRuleReturnScope expr181 =null;

		Object SYS_VAR166_tree=null;
		Object char_literal167_tree=null;
		Object char_literal169_tree=null;
		Object PLACE170_tree=null;
		Object char_literal171_tree=null;
		Object char_literal173_tree=null;
		Object char_literal174_tree=null;
		Object MARK175_tree=null;
		Object char_literal176_tree=null;
		Object char_literal178_tree=null;
		Object MARK179_tree=null;
		Object char_literal180_tree=null;
		Object char_literal182_tree=null;
		RewriteRuleTokenStream stream_PLACE=new RewriteRuleTokenStream(adaptor,"token PLACE");
		RewriteRuleTokenStream stream_SYS_VAR=new RewriteRuleTokenStream(adaptor,"token SYS_VAR");
		RewriteRuleTokenStream stream_MARK=new RewriteRuleTokenStream(adaptor,"token MARK");
		RewriteRuleTokenStream stream_71=new RewriteRuleTokenStream(adaptor,"token 71");
		RewriteRuleTokenStream stream_73=new RewriteRuleTokenStream(adaptor,"token 73");
		RewriteRuleTokenStream stream_74=new RewriteRuleTokenStream(adaptor,"token 74");
		RewriteRuleSubtreeStream stream_aggregate=new RewriteRuleSubtreeStream(adaptor,"rule aggregate");
		RewriteRuleSubtreeStream stream_expr=new RewriteRuleSubtreeStream(adaptor,"rule expr");

		try {
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:218:9: ( ( aggregate )? ( SYS_VAR '[' expr ']' -> ^( SYS_VAR ( aggregate )? expr ) | PLACE '[' expr ']' '.' MARK '[' expr ']' -> ^( MARK ( aggregate )? expr expr ) ) | MARK '[' expr ']' -> ^( CURRENT_MARK expr ) )
			int alt26=2;
			int LA26_0 = input.LA(1);
			if ( (LA26_0==AGGREGATE||LA26_0==PLACE||LA26_0==ROOT_AGG||LA26_0==SYS_VAR) ) {
				alt26=1;
			}
			else if ( (LA26_0==MARK) ) {
				alt26=2;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 26, 0, input);
				throw nvae;
			}

			switch (alt26) {
				case 1 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:218:11: ( aggregate )? ( SYS_VAR '[' expr ']' -> ^( SYS_VAR ( aggregate )? expr ) | PLACE '[' expr ']' '.' MARK '[' expr ']' -> ^( MARK ( aggregate )? expr expr ) )
					{
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:218:11: ( aggregate )?
					int alt24=2;
					int LA24_0 = input.LA(1);
					if ( (LA24_0==AGGREGATE||LA24_0==ROOT_AGG) ) {
						alt24=1;
					}
					switch (alt24) {
						case 1 :
							// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:218:11: aggregate
							{
							pushFollow(FOLLOW_aggregate_in_sys_id1336);
							aggregate165=aggregate();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) stream_aggregate.add(aggregate165.getTree());
							}
							break;

					}

					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:218:22: ( SYS_VAR '[' expr ']' -> ^( SYS_VAR ( aggregate )? expr ) | PLACE '[' expr ']' '.' MARK '[' expr ']' -> ^( MARK ( aggregate )? expr expr ) )
					int alt25=2;
					int LA25_0 = input.LA(1);
					if ( (LA25_0==SYS_VAR) ) {
						alt25=1;
					}
					else if ( (LA25_0==PLACE) ) {
						alt25=2;
					}

					else {
						if (state.backtracking>0) {state.failed=true; return retval;}
						NoViableAltException nvae =
							new NoViableAltException("", 25, 0, input);
						throw nvae;
					}

					switch (alt25) {
						case 1 :
							// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:219:6: SYS_VAR '[' expr ']'
							{
							SYS_VAR166=(Token)match(input,SYS_VAR,FOLLOW_SYS_VAR_in_sys_id1346); if (state.failed) return retval; 
							if ( state.backtracking==0 ) stream_SYS_VAR.add(SYS_VAR166);

							char_literal167=(Token)match(input,73,FOLLOW_73_in_sys_id1348); if (state.failed) return retval; 
							if ( state.backtracking==0 ) stream_73.add(char_literal167);

							pushFollow(FOLLOW_expr_in_sys_id1350);
							expr168=expr();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) stream_expr.add(expr168.getTree());
							char_literal169=(Token)match(input,74,FOLLOW_74_in_sys_id1352); if (state.failed) return retval; 
							if ( state.backtracking==0 ) stream_74.add(char_literal169);

							// AST REWRITE
							// elements: expr, aggregate, SYS_VAR
							// token labels: 
							// rule labels: retval
							// token list labels: 
							// rule list labels: 
							// wildcard labels: 
							if ( state.backtracking==0 ) {
							retval.tree = root_0;
							RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

							root_0 = (Object)adaptor.nil();
							// 219:27: -> ^( SYS_VAR ( aggregate )? expr )
							{
								// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:219:30: ^( SYS_VAR ( aggregate )? expr )
								{
								Object root_1 = (Object)adaptor.nil();
								root_1 = (Object)adaptor.becomeRoot(stream_SYS_VAR.nextNode(), root_1);
								// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:219:40: ( aggregate )?
								if ( stream_aggregate.hasNext() ) {
									adaptor.addChild(root_1, stream_aggregate.nextTree());
								}
								stream_aggregate.reset();

								adaptor.addChild(root_1, stream_expr.nextTree());
								adaptor.addChild(root_0, root_1);
								}

							}


							retval.tree = root_0;
							}

							}
							break;
						case 2 :
							// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:221:6: PLACE '[' expr ']' '.' MARK '[' expr ']'
							{
							PLACE170=(Token)match(input,PLACE,FOLLOW_PLACE_in_sys_id1378); if (state.failed) return retval; 
							if ( state.backtracking==0 ) stream_PLACE.add(PLACE170);

							char_literal171=(Token)match(input,73,FOLLOW_73_in_sys_id1380); if (state.failed) return retval; 
							if ( state.backtracking==0 ) stream_73.add(char_literal171);

							pushFollow(FOLLOW_expr_in_sys_id1382);
							expr172=expr();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) stream_expr.add(expr172.getTree());
							char_literal173=(Token)match(input,74,FOLLOW_74_in_sys_id1384); if (state.failed) return retval; 
							if ( state.backtracking==0 ) stream_74.add(char_literal173);

							char_literal174=(Token)match(input,71,FOLLOW_71_in_sys_id1386); if (state.failed) return retval; 
							if ( state.backtracking==0 ) stream_71.add(char_literal174);

							MARK175=(Token)match(input,MARK,FOLLOW_MARK_in_sys_id1388); if (state.failed) return retval; 
							if ( state.backtracking==0 ) stream_MARK.add(MARK175);

							char_literal176=(Token)match(input,73,FOLLOW_73_in_sys_id1390); if (state.failed) return retval; 
							if ( state.backtracking==0 ) stream_73.add(char_literal176);

							pushFollow(FOLLOW_expr_in_sys_id1392);
							expr177=expr();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) stream_expr.add(expr177.getTree());
							char_literal178=(Token)match(input,74,FOLLOW_74_in_sys_id1394); if (state.failed) return retval; 
							if ( state.backtracking==0 ) stream_74.add(char_literal178);

							// AST REWRITE
							// elements: expr, expr, MARK, aggregate
							// token labels: 
							// rule labels: retval
							// token list labels: 
							// rule list labels: 
							// wildcard labels: 
							if ( state.backtracking==0 ) {
							retval.tree = root_0;
							RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

							root_0 = (Object)adaptor.nil();
							// 221:47: -> ^( MARK ( aggregate )? expr expr )
							{
								// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:221:50: ^( MARK ( aggregate )? expr expr )
								{
								Object root_1 = (Object)adaptor.nil();
								root_1 = (Object)adaptor.becomeRoot(stream_MARK.nextNode(), root_1);
								// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:221:57: ( aggregate )?
								if ( stream_aggregate.hasNext() ) {
									adaptor.addChild(root_1, stream_aggregate.nextTree());
								}
								stream_aggregate.reset();

								adaptor.addChild(root_1, stream_expr.nextTree());
								adaptor.addChild(root_1, stream_expr.nextTree());
								adaptor.addChild(root_0, root_1);
								}

							}


							retval.tree = root_0;
							}

							}
							break;

					}

					}
					break;
				case 2 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:223:6: MARK '[' expr ']'
					{
					MARK179=(Token)match(input,MARK,FOLLOW_MARK_in_sys_id1420); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_MARK.add(MARK179);

					char_literal180=(Token)match(input,73,FOLLOW_73_in_sys_id1422); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_73.add(char_literal180);

					pushFollow(FOLLOW_expr_in_sys_id1424);
					expr181=expr();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_expr.add(expr181.getTree());
					char_literal182=(Token)match(input,74,FOLLOW_74_in_sys_id1426); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_74.add(char_literal182);

					// AST REWRITE
					// elements: expr
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 223:24: -> ^( CURRENT_MARK expr )
					{
						// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:223:27: ^( CURRENT_MARK expr )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(CURRENT_MARK, "CURRENT_MARK"), root_1);
						adaptor.addChild(root_1, stream_expr.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}

			catch (RecognitionException re) {
					reportError(re);
					recover(input,re);
			}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "sys_id"


	public static class aggregate_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "aggregate"
	// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:228:1: aggregate : ( ( non_root_agg )+ | ROOT_AGG '.' ! ( non_root_agg )* );
	public final ECLParser.aggregate_return aggregate() throws RecognitionException {
		ECLParser.aggregate_return retval = new ECLParser.aggregate_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token ROOT_AGG184=null;
		Token char_literal185=null;
		ParserRuleReturnScope non_root_agg183 =null;
		ParserRuleReturnScope non_root_agg186 =null;

		Object ROOT_AGG184_tree=null;
		Object char_literal185_tree=null;

		try {
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:228:11: ( ( non_root_agg )+ | ROOT_AGG '.' ! ( non_root_agg )* )
			int alt29=2;
			int LA29_0 = input.LA(1);
			if ( (LA29_0==AGGREGATE) ) {
				alt29=1;
			}
			else if ( (LA29_0==ROOT_AGG) ) {
				alt29=2;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 29, 0, input);
				throw nvae;
			}

			switch (alt29) {
				case 1 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:228:13: ( non_root_agg )+
					{
					root_0 = (Object)adaptor.nil();


					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:228:13: ( non_root_agg )+
					int cnt27=0;
					loop27:
					while (true) {
						int alt27=2;
						int LA27_0 = input.LA(1);
						if ( (LA27_0==AGGREGATE) ) {
							alt27=1;
						}

						switch (alt27) {
						case 1 :
							// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:228:13: non_root_agg
							{
							pushFollow(FOLLOW_non_root_agg_in_aggregate1457);
							non_root_agg183=non_root_agg();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) adaptor.addChild(root_0, non_root_agg183.getTree());

							}
							break;

						default :
							if ( cnt27 >= 1 ) break loop27;
							if (state.backtracking>0) {state.failed=true; return retval;}
							EarlyExitException eee = new EarlyExitException(27, input);
							throw eee;
						}
						cnt27++;
					}

					}
					break;
				case 2 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:229:6: ROOT_AGG '.' ! ( non_root_agg )*
					{
					root_0 = (Object)adaptor.nil();


					ROOT_AGG184=(Token)match(input,ROOT_AGG,FOLLOW_ROOT_AGG_in_aggregate1465); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					ROOT_AGG184_tree = (Object)adaptor.create(ROOT_AGG184);
					adaptor.addChild(root_0, ROOT_AGG184_tree);
					}

					char_literal185=(Token)match(input,71,FOLLOW_71_in_aggregate1467); if (state.failed) return retval;
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:229:20: ( non_root_agg )*
					loop28:
					while (true) {
						int alt28=2;
						int LA28_0 = input.LA(1);
						if ( (LA28_0==AGGREGATE) ) {
							alt28=1;
						}

						switch (alt28) {
						case 1 :
							// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:229:20: non_root_agg
							{
							pushFollow(FOLLOW_non_root_agg_in_aggregate1470);
							non_root_agg186=non_root_agg();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) adaptor.addChild(root_0, non_root_agg186.getTree());

							}
							break;

						default :
							break loop28;
						}
					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}

			catch (RecognitionException re) {
					reportError(re);
					recover(input,re);
			}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "aggregate"


	public static class non_root_agg_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "non_root_agg"
	// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:232:1: non_root_agg : AGGREGATE '[' ! expr ']' ! '.' !;
	public final ECLParser.non_root_agg_return non_root_agg() throws RecognitionException {
		ECLParser.non_root_agg_return retval = new ECLParser.non_root_agg_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token AGGREGATE187=null;
		Token char_literal188=null;
		Token char_literal190=null;
		Token char_literal191=null;
		ParserRuleReturnScope expr189 =null;

		Object AGGREGATE187_tree=null;
		Object char_literal188_tree=null;
		Object char_literal190_tree=null;
		Object char_literal191_tree=null;

		try {
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:233:4: ( AGGREGATE '[' ! expr ']' ! '.' !)
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:233:6: AGGREGATE '[' ! expr ']' ! '.' !
			{
			root_0 = (Object)adaptor.nil();


			AGGREGATE187=(Token)match(input,AGGREGATE,FOLLOW_AGGREGATE_in_non_root_agg1490); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			AGGREGATE187_tree = (Object)adaptor.create(AGGREGATE187);
			adaptor.addChild(root_0, AGGREGATE187_tree);
			}

			char_literal188=(Token)match(input,73,FOLLOW_73_in_non_root_agg1492); if (state.failed) return retval;
			pushFollow(FOLLOW_expr_in_non_root_agg1495);
			expr189=expr();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, expr189.getTree());

			char_literal190=(Token)match(input,74,FOLLOW_74_in_non_root_agg1497); if (state.failed) return retval;
			char_literal191=(Token)match(input,71,FOLLOW_71_in_non_root_agg1500); if (state.failed) return retval;
			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}

			catch (RecognitionException re) {
					reportError(re);
					recover(input,re);
			}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "non_root_agg"

	// $ANTLR start synpred1_ECL
	public final void synpred1_ECL_fragment() throws RecognitionException {
		// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:108:10: ( assign )
		// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:108:11: assign
		{
		pushFollow(FOLLOW_assign_in_synpred1_ECL180);
		assign();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred1_ECL

	// $ANTLR start synpred2_ECL
	public final void synpred2_ECL_fragment() throws RecognitionException {
		// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:132:6: ( IF '(' expr ')' if_block ELSE if_block )
		// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:132:6: IF '(' expr ')' if_block ELSE if_block
		{
		match(input,IF,FOLLOW_IF_in_synpred2_ECL385); if (state.failed) return;

		match(input,68,FOLLOW_68_in_synpred2_ECL387); if (state.failed) return;

		pushFollow(FOLLOW_expr_in_synpred2_ECL389);
		expr();
		state._fsp--;
		if (state.failed) return;

		match(input,69,FOLLOW_69_in_synpred2_ECL391); if (state.failed) return;

		pushFollow(FOLLOW_if_block_in_synpred2_ECL393);
		if_block();
		state._fsp--;
		if (state.failed) return;

		match(input,ELSE,FOLLOW_ELSE_in_synpred2_ECL395); if (state.failed) return;

		pushFollow(FOLLOW_if_block_in_synpred2_ECL397);
		if_block();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred2_ECL

	// $ANTLR start synpred3_ECL
	public final void synpred3_ECL_fragment() throws RecognitionException {
		// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:213:12: ( sys_id )
		// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:213:13: sys_id
		{
		pushFollow(FOLLOW_sys_id_in_synpred3_ECL1276);
		sys_id();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred3_ECL

	// Delegated rules

	public final boolean synpred3_ECL() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred3_ECL_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred2_ECL() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred2_ECL_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred1_ECL() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred1_ECL_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}



	public static final BitSet FOLLOW_statement_in_ecl102 = new BitSet(new long[]{0xF9FD7390467219B2L,0x0000000000000013L});
	public static final BitSet FOLLOW_stmt_in_statement119 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000100L});
	public static final BitSet FOLLOW_72_in_statement123 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_if_stmt_in_statement143 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_assign_in_stmt185 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_standalone_expr_in_stmt192 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_declar_in_stmt200 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ret_in_stmt209 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_seed_stmt_in_stmt216 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_SEED_in_seed_stmt231 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000010L});
	public static final BitSet FOLLOW_68_in_seed_stmt233 = new BitSet(new long[]{0xF9D57390427219B0L,0x0000000000000011L});
	public static final BitSet FOLLOW_expr_in_seed_stmt235 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000020L});
	public static final BitSet FOLLOW_69_in_seed_stmt237 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_expr_in_standalone_expr265 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_VAR_in_declar278 = new BitSet(new long[]{0x0000008002000000L});
	public static final BitSet FOLLOW_id_in_declar280 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_VAR_in_declar298 = new BitSet(new long[]{0x0000008002000000L});
	public static final BitSet FOLLOW_id_in_declar300 = new BitSet(new long[]{0x0000000000000040L});
	public static final BitSet FOLLOW_ASSIGN_in_declar302 = new BitSet(new long[]{0xF9D57390427219B0L,0x0000000000000011L});
	public static final BitSet FOLLOW_expr_in_declar304 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_RETURN_in_ret333 = new BitSet(new long[]{0xF9D57390427219B0L,0x0000000000000011L});
	public static final BitSet FOLLOW_expr_in_ret335 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_RETURN_in_ret350 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IF_in_if_stmt385 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000010L});
	public static final BitSet FOLLOW_68_in_if_stmt387 = new BitSet(new long[]{0xF9D57390427219B0L,0x0000000000000011L});
	public static final BitSet FOLLOW_expr_in_if_stmt389 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000020L});
	public static final BitSet FOLLOW_69_in_if_stmt391 = new BitSet(new long[]{0xF9FD7390467219B0L,0x0000000000000813L});
	public static final BitSet FOLLOW_if_block_in_if_stmt393 = new BitSet(new long[]{0x0000000000010000L});
	public static final BitSet FOLLOW_ELSE_in_if_stmt395 = new BitSet(new long[]{0xF9FD7390467219B0L,0x0000000000000813L});
	public static final BitSet FOLLOW_if_block_in_if_stmt397 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IF_in_if_stmt417 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000010L});
	public static final BitSet FOLLOW_68_in_if_stmt419 = new BitSet(new long[]{0xF9D57390427219B0L,0x0000000000000011L});
	public static final BitSet FOLLOW_expr_in_if_stmt421 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000020L});
	public static final BitSet FOLLOW_69_in_if_stmt423 = new BitSet(new long[]{0xF9FD7390467219B0L,0x0000000000000813L});
	public static final BitSet FOLLOW_if_block_in_if_stmt425 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_statement_in_if_block451 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_statement_block_in_if_block459 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_75_in_statement_block477 = new BitSet(new long[]{0xF9FD7390467219B0L,0x0000000000001013L});
	public static final BitSet FOLLOW_statement_in_statement_block479 = new BitSet(new long[]{0xF9FD7390467219B0L,0x0000000000001013L});
	public static final BitSet FOLLOW_76_in_statement_block482 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_id_in_assign504 = new BitSet(new long[]{0x0000000000000040L});
	public static final BitSet FOLLOW_ASSIGN_in_assign506 = new BitSet(new long[]{0xF9D57390427219B0L,0x0000000000000011L});
	public static final BitSet FOLLOW_expr_in_assign508 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_sys_id_in_assign525 = new BitSet(new long[]{0x0000000000000040L});
	public static final BitSet FOLLOW_ASSIGN_in_assign527 = new BitSet(new long[]{0xF9D57390427219B0L,0x0000000000000011L});
	public static final BitSet FOLLOW_expr_in_assign529 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_prec5_in_expr552 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_prec4_in_prec5568 = new BitSet(new long[]{0x0000002200000002L});
	public static final BitSet FOLLOW_LAND_in_prec5577 = new BitSet(new long[]{0xF9D57390427219B0L,0x0000000000000011L});
	public static final BitSet FOLLOW_LOR_in_prec5582 = new BitSet(new long[]{0xF9D57390427219B0L,0x0000000000000011L});
	public static final BitSet FOLLOW_prec4_in_prec5590 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_prec3_in_prec4613 = new BitSet(new long[]{0x0000084401840002L});
	public static final BitSet FOLLOW_GT_in_prec4623 = new BitSet(new long[]{0xF9D57390427219B0L,0x0000000000000011L});
	public static final BitSet FOLLOW_LT_in_prec4628 = new BitSet(new long[]{0xF9D57390427219B0L,0x0000000000000011L});
	public static final BitSet FOLLOW_LE_in_prec4633 = new BitSet(new long[]{0xF9D57390427219B0L,0x0000000000000011L});
	public static final BitSet FOLLOW_GE_in_prec4638 = new BitSet(new long[]{0xF9D57390427219B0L,0x0000000000000011L});
	public static final BitSet FOLLOW_EQ_in_prec4643 = new BitSet(new long[]{0xF9D57390427219B0L,0x0000000000000011L});
	public static final BitSet FOLLOW_NE_in_prec4649 = new BitSet(new long[]{0xF9D57390427219B0L,0x0000000000000011L});
	public static final BitSet FOLLOW_prec3_in_prec4658 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_prec2_in_prec3680 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_rand_in_prec3687 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ENTIER_in_prec3694 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000010L});
	public static final BitSet FOLLOW_68_in_prec3696 = new BitSet(new long[]{0xD814638042500020L,0x0000000000000010L});
	public static final BitSet FOLLOW_prec2_in_prec3698 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000020L});
	public static final BitSet FOLLOW_69_in_prec3700 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_SIGN_in_prec3715 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000010L});
	public static final BitSet FOLLOW_68_in_prec3717 = new BitSet(new long[]{0xD814638042500020L,0x0000000000000010L});
	public static final BitSet FOLLOW_prec2_in_prec3719 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000020L});
	public static final BitSet FOLLOW_69_in_prec3721 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_SQRT_in_prec3736 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000010L});
	public static final BitSet FOLLOW_68_in_prec3738 = new BitSet(new long[]{0xD814638042500020L,0x0000000000000010L});
	public static final BitSet FOLLOW_prec2_in_prec3740 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000020L});
	public static final BitSet FOLLOW_69_in_prec3742 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ABS_in_prec3757 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000010L});
	public static final BitSet FOLLOW_68_in_prec3759 = new BitSet(new long[]{0xD814638042500020L,0x0000000000000010L});
	public static final BitSet FOLLOW_prec2_in_prec3761 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000020L});
	public static final BitSet FOLLOW_69_in_prec3763 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LN_in_prec3778 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000010L});
	public static final BitSet FOLLOW_68_in_prec3780 = new BitSet(new long[]{0xD814638042500020L,0x0000000000000010L});
	public static final BitSet FOLLOW_prec2_in_prec3782 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000020L});
	public static final BitSet FOLLOW_69_in_prec3784 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_SIN_in_prec3799 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000010L});
	public static final BitSet FOLLOW_68_in_prec3801 = new BitSet(new long[]{0xD814638042500020L,0x0000000000000010L});
	public static final BitSet FOLLOW_prec2_in_prec3803 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000020L});
	public static final BitSet FOLLOW_69_in_prec3805 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_COS_in_prec3820 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000010L});
	public static final BitSet FOLLOW_68_in_prec3822 = new BitSet(new long[]{0xD814638042500020L,0x0000000000000010L});
	public static final BitSet FOLLOW_prec2_in_prec3824 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000020L});
	public static final BitSet FOLLOW_69_in_prec3826 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_TAN_in_prec3841 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000010L});
	public static final BitSet FOLLOW_68_in_prec3843 = new BitSet(new long[]{0xD814638042500020L,0x0000000000000010L});
	public static final BitSet FOLLOW_prec2_in_prec3845 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000020L});
	public static final BitSet FOLLOW_69_in_prec3847 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ATAN_in_prec3862 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000010L});
	public static final BitSet FOLLOW_68_in_prec3864 = new BitSet(new long[]{0xD814638042500020L,0x0000000000000010L});
	public static final BitSet FOLLOW_prec2_in_prec3866 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000020L});
	public static final BitSet FOLLOW_69_in_prec3868 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_COT_in_prec3883 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000010L});
	public static final BitSet FOLLOW_68_in_prec3885 = new BitSet(new long[]{0xD814638042500020L,0x0000000000000010L});
	public static final BitSet FOLLOW_prec2_in_prec3887 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000020L});
	public static final BitSet FOLLOW_69_in_prec3889 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_prec1_in_prec2910 = new BitSet(new long[]{0x0000820000000002L});
	public static final BitSet FOLLOW_PLUS_in_prec2913 = new BitSet(new long[]{0xD814638042500020L,0x0000000000000010L});
	public static final BitSet FOLLOW_prec1_in_prec2916 = new BitSet(new long[]{0x0000820000000002L});
	public static final BitSet FOLLOW_MINUS_in_prec2920 = new BitSet(new long[]{0xD814638042500020L,0x0000000000000010L});
	public static final BitSet FOLLOW_prec1_in_prec2923 = new BitSet(new long[]{0x0000820000000002L});
	public static final BitSet FOLLOW_pow_in_prec1938 = new BitSet(new long[]{0x0000040000008002L});
	public static final BitSet FOLLOW_MULT_in_prec1941 = new BitSet(new long[]{0xD814638042500020L,0x0000000000000010L});
	public static final BitSet FOLLOW_pow_in_prec1944 = new BitSet(new long[]{0x0000040000008002L});
	public static final BitSet FOLLOW_DIV_in_prec1948 = new BitSet(new long[]{0xD814638042500020L,0x0000000000000010L});
	public static final BitSet FOLLOW_pow_in_prec1951 = new BitSet(new long[]{0x0000040000008002L});
	public static final BitSet FOLLOW_prec0_in_pow967 = new BitSet(new long[]{0x0002000000000002L});
	public static final BitSet FOLLOW_POW_in_pow970 = new BitSet(new long[]{0xD814638042500020L,0x0000000000000010L});
	public static final BitSet FOLLOW_pow_in_pow973 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_atom_in_prec0994 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_MINUS_in_prec01002 = new BitSet(new long[]{0xD814638042500020L,0x0000000000000010L});
	public static final BitSet FOLLOW_NOT_in_prec01006 = new BitSet(new long[]{0xD814638042500020L,0x0000000000000010L});
	public static final BitSet FOLLOW_prec0_in_prec01009 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_INT_in_atom1034 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_REAL_in_atom1041 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_id_in_atom1048 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_68_in_atom1056 = new BitSet(new long[]{0xF9D57390427219B0L,0x0000000000000011L});
	public static final BitSet FOLLOW_expr_in_atom1058 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000020L});
	public static final BitSet FOLLOW_69_in_atom1060 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_STR_in_atom1071 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_TRUE_in_atom1080 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_FALSE_in_atom1089 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_EXP_in_atom1096 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_sys_atom_in_atom1103 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_UNIFORM_in_rand1143 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000010L});
	public static final BitSet FOLLOW_68_in_rand1145 = new BitSet(new long[]{0xF9D57390427219B0L,0x0000000000000011L});
	public static final BitSet FOLLOW_expr_in_rand1147 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000040L});
	public static final BitSet FOLLOW_70_in_rand1149 = new BitSet(new long[]{0xF9D57390427219B0L,0x0000000000000011L});
	public static final BitSet FOLLOW_expr_in_rand1151 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000020L});
	public static final BitSet FOLLOW_69_in_rand1153 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_POISSON_in_rand1171 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000010L});
	public static final BitSet FOLLOW_68_in_rand1173 = new BitSet(new long[]{0xF9D57390427219B0L,0x0000000000000011L});
	public static final BitSet FOLLOW_expr_in_rand1175 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000020L});
	public static final BitSet FOLLOW_69_in_rand1177 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_EXPONENTIAL_in_rand1192 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000010L});
	public static final BitSet FOLLOW_68_in_rand1194 = new BitSet(new long[]{0xF9D57390427219B0L,0x0000000000000011L});
	public static final BitSet FOLLOW_expr_in_rand1196 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000020L});
	public static final BitSet FOLLOW_69_in_rand1198 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NORMAL_in_rand1213 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000010L});
	public static final BitSet FOLLOW_68_in_rand1215 = new BitSet(new long[]{0xF9D57390427219B0L,0x0000000000000011L});
	public static final BitSet FOLLOW_expr_in_rand1217 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000040L});
	public static final BitSet FOLLOW_70_in_rand1219 = new BitSet(new long[]{0xF9D57390427219B0L,0x0000000000000011L});
	public static final BitSet FOLLOW_expr_in_rand1221 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000020L});
	public static final BitSet FOLLOW_69_in_rand1223 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_BINOMIAL_in_rand1240 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000010L});
	public static final BitSet FOLLOW_68_in_rand1242 = new BitSet(new long[]{0xF9D57390427219B0L,0x0000000000000011L});
	public static final BitSet FOLLOW_expr_in_rand1244 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000040L});
	public static final BitSet FOLLOW_70_in_rand1246 = new BitSet(new long[]{0xF9D57390427219B0L,0x0000000000000011L});
	public static final BitSet FOLLOW_expr_in_rand1248 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000020L});
	public static final BitSet FOLLOW_69_in_rand1250 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_sys_id_in_sys_atom1281 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_aggregate_in_sys_atom1288 = new BitSet(new long[]{0x0000400000000000L});
	public static final BitSet FOLLOW_PLACE_in_sys_atom1291 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000200L});
	public static final BitSet FOLLOW_73_in_sys_atom1293 = new BitSet(new long[]{0xF9D57390427219B0L,0x0000000000000011L});
	public static final BitSet FOLLOW_expr_in_sys_atom1295 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000400L});
	public static final BitSet FOLLOW_74_in_sys_atom1297 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000080L});
	public static final BitSet FOLLOW_71_in_sys_atom1299 = new BitSet(new long[]{0x0000010000000000L});
	public static final BitSet FOLLOW_MARK_in_sys_atom1301 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_TIME_in_sys_atom1320 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_aggregate_in_sys_id1336 = new BitSet(new long[]{0x1000400000000000L});
	public static final BitSet FOLLOW_SYS_VAR_in_sys_id1346 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000200L});
	public static final BitSet FOLLOW_73_in_sys_id1348 = new BitSet(new long[]{0xF9D57390427219B0L,0x0000000000000011L});
	public static final BitSet FOLLOW_expr_in_sys_id1350 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000400L});
	public static final BitSet FOLLOW_74_in_sys_id1352 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_PLACE_in_sys_id1378 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000200L});
	public static final BitSet FOLLOW_73_in_sys_id1380 = new BitSet(new long[]{0xF9D57390427219B0L,0x0000000000000011L});
	public static final BitSet FOLLOW_expr_in_sys_id1382 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000400L});
	public static final BitSet FOLLOW_74_in_sys_id1384 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000080L});
	public static final BitSet FOLLOW_71_in_sys_id1386 = new BitSet(new long[]{0x0000010000000000L});
	public static final BitSet FOLLOW_MARK_in_sys_id1388 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000200L});
	public static final BitSet FOLLOW_73_in_sys_id1390 = new BitSet(new long[]{0xF9D57390427219B0L,0x0000000000000011L});
	public static final BitSet FOLLOW_expr_in_sys_id1392 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000400L});
	public static final BitSet FOLLOW_74_in_sys_id1394 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_MARK_in_sys_id1420 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000200L});
	public static final BitSet FOLLOW_73_in_sys_id1422 = new BitSet(new long[]{0xF9D57390427219B0L,0x0000000000000011L});
	public static final BitSet FOLLOW_expr_in_sys_id1424 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000400L});
	public static final BitSet FOLLOW_74_in_sys_id1426 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_non_root_agg_in_aggregate1457 = new BitSet(new long[]{0x0000000000000022L});
	public static final BitSet FOLLOW_ROOT_AGG_in_aggregate1465 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000080L});
	public static final BitSet FOLLOW_71_in_aggregate1467 = new BitSet(new long[]{0x0000000000000022L});
	public static final BitSet FOLLOW_non_root_agg_in_aggregate1470 = new BitSet(new long[]{0x0000000000000022L});
	public static final BitSet FOLLOW_AGGREGATE_in_non_root_agg1490 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000200L});
	public static final BitSet FOLLOW_73_in_non_root_agg1492 = new BitSet(new long[]{0xF9D57390427219B0L,0x0000000000000011L});
	public static final BitSet FOLLOW_expr_in_non_root_agg1495 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000400L});
	public static final BitSet FOLLOW_74_in_non_root_agg1497 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000080L});
	public static final BitSet FOLLOW_71_in_non_root_agg1500 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_assign_in_synpred1_ECL180 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IF_in_synpred2_ECL385 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000010L});
	public static final BitSet FOLLOW_68_in_synpred2_ECL387 = new BitSet(new long[]{0xF9D57390427219B0L,0x0000000000000011L});
	public static final BitSet FOLLOW_expr_in_synpred2_ECL389 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000020L});
	public static final BitSet FOLLOW_69_in_synpred2_ECL391 = new BitSet(new long[]{0xF9FD7390467219B0L,0x0000000000000813L});
	public static final BitSet FOLLOW_if_block_in_synpred2_ECL393 = new BitSet(new long[]{0x0000000000010000L});
	public static final BitSet FOLLOW_ELSE_in_synpred2_ECL395 = new BitSet(new long[]{0xF9FD7390467219B0L,0x0000000000000813L});
	public static final BitSet FOLLOW_if_block_in_synpred2_ECL397 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_sys_id_in_synpred3_ECL1276 = new BitSet(new long[]{0x0000000000000002L});
}
