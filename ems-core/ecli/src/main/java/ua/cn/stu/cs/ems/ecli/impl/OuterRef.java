package ua.cn.stu.cs.ems.ecli.impl;

import ua.cn.stu.cs.ems.ecli.EcliAggregate;
import ua.cn.stu.cs.ems.ecli.EcliToken;
import ua.cn.stu.cs.ems.ecli.Value;

/**
 * Factory that is used to create outer reference wrappers to access token's attributes and aggregate variables in the model.
 *
 * @author n0weak
 */
class OuterRef {

    public static enum RefType {
        VAR, ATTR
    }

    private OuterRef() {
    }

    public static RefDecorator createAttrDecorator(EcliToken mark, Object key, String uid) {
        return new OuterAttribute(mark, key, uid);
    }

    public static RefDecorator createVarDecorator(EcliAggregate aggregate, Object key, String uid) {
        return new OuterVariable(aggregate, key, uid);
    }

    public interface RefDecorator {

        public RefType getType();

        public boolean isDefined();

        public Object getValue();

        public boolean setValue(Value value);

        public Object getKey();
    }

    private static class OuterAttribute implements RefDecorator {
        private final EcliToken mark;
        private final Object key;
        private final String uid;

        public OuterAttribute(EcliToken mark, Object key, String uid) {
            if (null == mark || null == key || null == uid) {
                throw new IllegalArgumentException("null values are not allowed!");
            }
            this.mark = mark;
            this.key = key;
            this.uid = uid;
        }

        public RefType getType() {
            return OuterRef.RefType.ATTR;
        }

        public boolean isDefined() {
            return mark.containsAttribute(key);
        }

        public Object getValue() {
            return mark.getAttribute(key);
        }

        public boolean setValue(Value value) {
            return mark.setAttribute(key, value);
        }

        public Object getKey() {
            return key;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            OuterAttribute that = (OuterAttribute) o;

            if (!uid.equals(that.uid)) return false;

            return true;
        }

        @Override
        public int hashCode() {
            return uid.hashCode();
        }
    }

    private static class OuterVariable implements RefDecorator {
        private final EcliAggregate aggregate;
        private final Object key;
        private final String uid;

        public OuterVariable(EcliAggregate aggregate, Object key, String uid) {
            if (null == aggregate || null == key || null == uid) {
                throw new IllegalArgumentException("null values are not allowed!");
            }
            this.aggregate = aggregate;
            this.key = key;
            this.uid = uid;
        }

        public RefType getType() {
            return OuterRef.RefType.VAR;
        }

        public boolean isDefined() {
            return aggregate.containsVar(key);
        }

        public Object getValue() {
            return aggregate.getVariable(key);
        }

        public boolean setValue(Value value) {
            return aggregate.setVariable(key, value);
        }

        public Object getKey() {
            return key;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            OuterVariable that = (OuterVariable) o;

            if (!uid.equals(that.uid)) return false;

            return true;
        }

        @Override
        public int hashCode() {
            return uid.hashCode();
        }
    }
}