package ua.cn.stu.cs.ems.ecli;

/**
 *  Interface that must be implemented by ECL's user in order
 *  to provide access to aggregates and variables.
 *
 * @author n0weak
 */
public interface EcliAggregate {

    /**
     * Indicates whether variable with specified name exists in the scopes
     * that are reachable to the  current aggregate.
     *
     * @param key key of the variable to be checked
     * @return true if variable exists, false if not
     */
    boolean containsVar(Object key);

    /**
     * Indicates whether current aggregate contains place with the specified
     * key.
     *
     * @param place key of the place
     * @return true if such place exists, false if not
     */
    boolean containsPlace(Object place);

    /**
     * Indicates whether specified place in the current aggregate contains mark.
     *
     * @param place key of the place
     * @return true if the place exists and contains the mark, false if not
     */
    boolean isPlaceMarked(Object place);

    /**
     * Indicates whether current aggregate contains nested aggregate with the
     * specified key.
     *
     * @param aggregate key of the aggregate
     * @return true if such aggregate exists, false if not
     */
    boolean containsAggregate(Object aggregate);

    /**
     * Provides access to the variables from the scopes that are reachable to
     * the current aggregate.
     *
     * @param key key of the variable to be returned
     * @return desired variable value or null if variable with the name
     * specified doesn't exist
     */
    Object getVariable(Object key);

    /**
     * Initializes variable  with supplied value.
     *
     * @param key   - key of the variable
     * @param value - value of the variable. Note that actual value is wrapped
     * in {@link Value} instance.
     * @return true if variable was initialized successfully,
     *         false if the variable was not predefined and dynamic
     *         initialization is forbidden
     */
    boolean setVariable(Object key, Value value);

    /**
     * Returns mark from the place with the specified key.
     *
     * @param place key of the place to get the mark from
     * @return mark from the place or null if it cannot be reached
     */
    EcliToken getTokenAt(Object place);

    /**
     * Returns nested aggregate with the specified key from the current one.
     *
     * @param aggregate key of the nested aggregate
     * @return corresponding aggregate or null if it does not exist
     */
    EcliAggregate getAggregate(Object aggregate);

}
