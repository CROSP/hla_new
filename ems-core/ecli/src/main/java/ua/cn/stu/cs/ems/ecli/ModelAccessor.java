package ua.cn.stu.cs.ems.ecli;

/**
 * Interface that specifies general contract of model-interpreter communication.
 * Represents root aggregate with some additional duties.
 *
 * @author n0weak
 */
public interface ModelAccessor extends EcliAggregate {

    /**
     * @return current modeling time.
     */
    public double getTime();

    /**
     * @return token that is currently processed in the transition
     */
    public EcliToken getCurrentToken();

    /**
     * @return aggregate which contains transition that is requesting interpretation
     */
    public EcliAggregate getCurrentAggregate();

    /**
     * @return root aggregate for thee whole model, user is able to refer to this aggregate using 'ROOT' keyword
     */
    public EcliAggregate getRootAggregate();


}
