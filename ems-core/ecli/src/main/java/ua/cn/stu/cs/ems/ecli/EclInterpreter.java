package ua.cn.stu.cs.ems.ecli;

import org.antlr.runtime.ANTLRInputStream;
import org.antlr.runtime.BaseRecognizer;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.CommonTreeNodeStream;
import ua.cn.stu.cs.ems.ecli.errors.*;
import ua.cn.stu.cs.ems.ecli.impl.ECLLexer;
import ua.cn.stu.cs.ems.ecli.impl.ECLParser;
import ua.cn.stu.cs.ems.ecli.impl.EclTreeWalker;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;


/**
 * @author n0weak
 */
public class EclInterpreter implements Interpreter {

    private final EclRandomData randomData = new EclRandomData();
    private ECLListener eclListener = new ECLListener();
    private boolean silentMode = true;
    private boolean caseSensitive = true;

    /**
     * Default constructor, creates case sensitive interpreter with silent mode enabled, i.e. no printing to stderr will be performed
     */
    public EclInterpreter() {
    }

    /**
     * @param silentMode    interpreter will print all found errors to stderr if that value is set to true.
     * @param caseSensitive defines whether interpreter will distinguish between uppercase and lowercase letters in statements.
     *                      All the letters will be considered as uppercase. Note that recognition of names of the variables will still be case sensitive.
     */
    public EclInterpreter(boolean silentMode, boolean caseSensitive) {
        this.silentMode = silentMode;
        this.caseSensitive = caseSensitive;
    }

    public InterpretationReport interpret(InputStream inputStream, ModelAccessor modelAccessor) throws IOException {
        eclListener.clearErrors();
        eclListener.setInValidationProcess(false);

        CommonTokenStream tokens = getTokenStream(inputStream);
        ECLParser eclParser = createParser(tokens);

        CommonTreeNodeStream treeNodeStream;
        try {
            treeNodeStream = getNodeStream(eclParser);
        } catch (RecognitionException e) {
            throw new ValidationException(eclListener.createParsingError(e, eclParser));
        }
        if (null != treeNodeStream) {
            EclTreeWalker treeWalker = new EclTreeWalker(treeNodeStream, modelAccessor, randomData);
            treeWalker.addWarningListener(eclListener);
            try {
                treeWalker.interpret();
            } catch (RecognitionException e) {
                throw new ValidationException(eclListener.createParsingError(e, eclParser));
            }
            return new InterpretationReport(eclListener.getErrorList(), treeWalker.getRetVal());
        } else {
            return new InterpretationReport(eclListener.getErrorList(), null);
        }

    }

    public ValidationReport validate(InputStream inputStream) throws IOException {
        return validate(inputStream, null);
    }


    public ValidationReport validate(InputStream inputStream, ModelAccessor modelAccessor) throws IOException {
        eclListener.clearErrors();
        eclListener.setInValidationProcess(true);
        EclTreeWalker treeWalker = null;

        try {
            CommonTreeNodeStream treeNodeStream = getNodeStream(inputStream);
            if (null != treeNodeStream) {
                treeWalker = new EclTreeWalker(treeNodeStream, modelAccessor, randomData);
                treeWalker.addWarningListener(eclListener);
                treeWalker.validate();
            }
        } catch (RecognitionException e) {
            throw new InterpretationException("Caught RecognitionException during validation process, possible bug, please report", e);
        } catch (Exception e) {
            if (eclListener.getErrorList().isEmpty()) { //if there were no errors, then exception is definitely caused by inner bug
                throw new InterpretationException("Failed to make the validation, possible bug, please report", e);
            }
        }

        return new ValidationReport(eclListener.getErrorList(),
                treeWalker != null && treeWalker.returnsValue());
    }

    private CommonTokenStream getTokenStream(InputStream inputStream) throws IOException {
        ECLLexer eclLexer = new ECLLexer(caseSensitive ? new ANTLRInputStream(inputStream) :
                new ANTLRNoCaseInputStream(inputStream));
        eclLexer.addErrorListener(eclListener);
        CommonTokenStream tokens = new CommonTokenStream();
        tokens.setTokenSource(eclLexer);
        return tokens;
    }

    private ECLParser createParser(CommonTokenStream tokens) {
        ECLParser eclParser = new ECLParser(tokens);
        eclParser.addErrorListener(eclListener);
        return eclParser;
    }

    private CommonTreeNodeStream getNodeStream(ECLParser eclParser) throws RecognitionException {
        ECLParser.ecl_return ecl_return = eclParser.ecl();
        CommonTree tree = (CommonTree) ecl_return.getTree();

        if (null == tree) {
            return null;
        }

        CommonTreeNodeStream nodes = new CommonTreeNodeStream(tree);
        nodes.setTokenStream(eclParser.getTokenStream());
        return nodes;
    }

    private CommonTreeNodeStream getNodeStream(InputStream inputStream) throws RecognitionException, IOException {
        return getNodeStream(createParser(getTokenStream(inputStream)));
    }


    private class ECLListener implements RecognitionErrorListener {

        private boolean inValidationProcess;
        private List<ParsingError> errors = new ArrayList<ParsingError>();

        public void errorHappened(RecognitionException e, BaseRecognizer recognizer) {
            ParsingError parsingError = createParsingError(e, recognizer);
            if (!silentMode) {
                System.err.println(parsingError.getMessage());
            }
            if (inValidationProcess || parsingError.getErrorCode().getSeverity() == ErrorCode.Severity.WARNING) {
                errors.add(parsingError);
            } else {
                throw new ValidationException(parsingError);
            }
        }

        private void clearErrors() {
            errors.clear();
        }

        /**
         * @return all errors with any severity that were found during validation or all errors with {@link ErrorCode.Severity#WARNING} severity that were found during interpretation
         */
        private List<ParsingError> getErrorList() {
            return new ArrayList<ParsingError>(errors);
        }

        private ParsingError createParsingError(RecognitionException e, BaseRecognizer recognizer) {
            ErrorCode errorCode = ErrorCode.getCodeFor(e);
            String msg = (errorCode.getSeverity() == ErrorCode.Severity.WARNING ? "warn: " : "") +
                    e.line + ":" + e.charPositionInLine + " ";
            msg += errorCode.getErrorMessage(e, recognizer);
            return new ParsingError(e.line, e.charPositionInLine, getEndLine(e), getEndPos(e), errorCode, msg);
        }

        private int getEndPos(RecognitionException re) {
            int endPos;
            if (re instanceof InformativeException) {
                endPos = ((InformativeException) re).getEndPos();
            } else {
                endPos = re.charPositionInLine +
                        ((re.token != null && re.token.getText() != null) ? re.token.getText().length() - 1 : 0);
            }
            return endPos;
        }

        private int getEndLine(RecognitionException re) {
            int endLine;
            if (re instanceof InformativeException) {
                endLine = ((InformativeException) re).getEndLine();
            } else {
                endLine = re.line;
            }
            return endLine;
        }

        public void setInValidationProcess(boolean inValidationProcess) {
            this.inValidationProcess = inValidationProcess;
        }
    }

    private static class ANTLRNoCaseInputStream extends ANTLRInputStream {

        private ANTLRNoCaseInputStream(InputStream input) throws IOException {
            super(input);
        }


        @Override
        public int LT(int i) {
            return Character.toUpperCase(super.LT(i));
        }

        public int LA(int i) {
            return Character.toUpperCase(super.LA(i));
        }


    }
}
