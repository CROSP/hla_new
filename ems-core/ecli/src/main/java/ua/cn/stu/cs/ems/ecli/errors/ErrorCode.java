package ua.cn.stu.cs.ems.ecli.errors;

import org.antlr.runtime.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * The goal of this class is to abstract from ANTLR's exception classes and provide useful methods for error reporting.
 *
 * @author n0weak
 */
public enum ErrorCode implements ErrorMessageCreator {

    UNEXPECTED_EOF(new ErrorMessageCreator() {
        public String getErrorMessage(RecognitionException e, BaseRecognizer recognizer) {
            String msg = "";
            if (e instanceof UnexpectedEOFException) {
                UnexpectedEOFException eofe = (UnexpectedEOFException) e;
                String tokenName;
                if (eofe.expecting == Token.EOF) {
                    tokenName = "EOF";
                } else {
                    tokenName = recognizer.getTokenNames()[eofe.expecting];
                }
                msg = getLocalizedMessage("MismatchedToken", tokenName, recognizer.getTokenErrorDisplay(e.token));
            }
            return msg;
        }
    }, UnexpectedEOFException.class),

    MISMATCHED_TOKEN(new ErrorMessageCreator() {
        public String getErrorMessage(RecognitionException e, BaseRecognizer recognizer) {
            String msg = "";
            if (e instanceof MismatchedTokenException) {
                MismatchedTokenException mte = (MismatchedTokenException) e;
                if (recognizer instanceof Lexer) {
                    msg = getLocalizedMessage("MismatchedToken", ((Lexer) recognizer).getCharErrorDisplay(mte.expecting),
                            ((Lexer) recognizer).getCharErrorDisplay(mte.c));
                } else {
                    String tokenName;
                    if (mte.expecting == Token.EOF) {
                        tokenName = "EOF";
                    } else {
                        tokenName = recognizer.getTokenNames()[mte.expecting];
                    }
                    msg = getLocalizedMessage("MismatchedToken", tokenName, recognizer.getTokenErrorDisplay(e.token));
                }
            }
            return msg;
        }
    }, MismatchedTokenException.class, UnwantedTokenException.class, MissingTokenException.class),

    INVALID_INPUT(new ErrorMessageCreator() {
        public String getErrorMessage(RecognitionException e, BaseRecognizer recognizer) {
            String token;
            if (recognizer instanceof Lexer) {
                token = ((Lexer) recognizer).getCharErrorDisplay(e.c);
            } else {
                token = recognizer.getTokenErrorDisplay(e.token);
            }
            return getLocalizedMessage("InvalidInput", token);
        }
    }, EarlyExitException.class, NoViableAltException.class),

    INVALID_STMT("InvalidStmt"),
    UNDEFINED_VAR("UndefinedVariable"),
    UNREACHABLE_STMT("UnreachableStmt", Severity.WARNING),
    REDEFINED_VAR("RedefinedVariable"),
    UNDEFINED_VALUE("UndefinedVal", Severity.WARNING),
    DIV_BY_0("DivBy0", Severity.WARNING),
    INVALID_REFERENCE("InvalidReference"),
    UNMARKED_PLACE("UnmarkedPlace", Severity.WARNING),

    RECOGNITION_ERROR(new ErrorMessageCreator() {
        public String getErrorMessage(RecognitionException e, BaseRecognizer recognizer) {
            return getLocalizedMessage("RecognitionError");
        }
    }, RecognitionException.class);

    private static final I18NImpl i18NImpl = new I18NImpl();

    private List<Class<? extends RecognitionException>> types;
    private ErrorMessageCreator errorMessageCreator;
    private Severity severity = Severity.ERROR;

    /**
     * Creates error code instance that works with {@link RecognitionException} subclasses, passed as constructor param.
     * Created error has {@link Severity#ERROR} severity.
     *
     * @param errorMessageCreator implementer of {@link ErrorMessageCreator} interface that will be used for message creation
     * @param types               list exceptions that are supported by this error code instance
     */
    ErrorCode(ErrorMessageCreator errorMessageCreator, Class<? extends RecognitionException>... types) {
        this.types = Arrays.asList(types);
        this.errorMessageCreator = errorMessageCreator;
    }

    /**
     * Creates error code instance that works only with {@link ECLSemanticException}.
     * Error messages will be created with {@link DefaultErrMsgCreator}.
     * Severity is {@link Severity#ERROR}.
     *
     * @param msgId identifier for message creation
     */
    ErrorCode(String msgId) {
        this.types = new ArrayList<Class<? extends RecognitionException>>(0);
        this.errorMessageCreator = new DefaultErrMsgCreator(msgId);
    }

    /**
     * Creates error code instance that works only with {@link ECLSemanticException}.
     * Error messages will be created with {@link DefaultErrMsgCreator}.
     *
     * @param msgId    identifier for message creation
     * @param severity error severity
     */
    ErrorCode(String msgId, Severity severity) {
        this.types = new ArrayList<Class<? extends RecognitionException>>(0);
        this.errorMessageCreator = new DefaultErrMsgCreator(msgId);
        this.severity = severity;
    }

    /**
     * Defines error code of exception that is passed as a parameter. Default value is {@link #RECOGNITION_ERROR}, it's returned
     * when no error-specific code was found.
     * If passed exception is of {@link ECLSemanticException} class, then error code is simply obtained
     * via {@link ua.cn.stu.cs.jess.ecli.errors.ECLSemanticException#getErrorCode()} call.
     *
     * @param e error which code has to be defined
     * @return error code
     */
    public static ErrorCode getCodeFor(RecognitionException e) {
        ErrorCode returnCode = RECOGNITION_ERROR;
        if (e instanceof ECLSemanticException) {
            returnCode = ((ECLSemanticException) e).getErrorCode();
        } else {
            for (ErrorCode errorCode : ErrorCode.values()) {
                if (errorCode.types.contains(e.getClass())) {
                    return errorCode;
                }
            }
        }
        return returnCode;
    }

    private static String getLocalizedMessage(String key, Object... formatArgs) {
        return i18NImpl.getLocalizedMessage(key, formatArgs);
    }

    /**
     * Note that exception passed as a parameter must be supported by the current error code
     *
     * @param e          exception that represents generic parsing failure
     * @param recognizer recognizer that found error, used to obtain some useful data, e.g. names of the tokens
     * @return generated error message or null if exception passed as a parameter is not supported
     */
    public String getErrorMessage(RecognitionException e, BaseRecognizer recognizer) {
        return errorMessageCreator.getErrorMessage(e, recognizer);
    }

    /**
     * @return severity of current error
     */
    public Severity getSeverity() {
        return severity;
    }


    public static enum Severity {

        /**
         * Errors with warning severity are produced only by the validator and can be ignored by the user. However, ignoring of the warnings
         * may lead to undefined results, i.e. null return value from {@link ua.cn.stu.cs.jess.ecli.EclInterpreter#interpret(java.io.InputStream, ua.cn.stu.cs.jess.ecli.ModelAccessor)}
         * <p/>
         * At all, warning severity means that no {@link ValidationException} will be thrown during interpretation process. To guarantee correct evaluations you
         * should not ignore warnings.
         */
        WARNING,

        /**
         * Such kind of errors can not be ignored.
         */
        ERROR
    }

    /**
     * Default error message creator that works only with {@link ECLSemanticException} errors. Creates messaged using
     * message id, passed as constructor param, and error arguments, obtained from error
     * by {@link ua.cn.stu.cs.jess.ecli.errors.ECLSemanticException#getErrArgs()} call
     */
    private static class DefaultErrMsgCreator implements ErrorMessageCreator {

        private String msgId;

        private DefaultErrMsgCreator(String msgId) {
            this.msgId = msgId;
        }

        public String getErrorMessage(RecognitionException e, BaseRecognizer recognizer) {
            String result = null;
            if (e instanceof ECLSemanticException) {
                ECLSemanticException eclException = (ECLSemanticException) e;
                result = getLocalizedMessage(msgId + (eclException.getSubCode() != null ? "." + eclException.getSubCode() : ""), eclException.getErrArgs());
            }
            return result;
        }
    }
}
