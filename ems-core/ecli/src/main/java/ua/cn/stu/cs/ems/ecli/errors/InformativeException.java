package ua.cn.stu.cs.ems.ecli.errors;

/**
 * Interface to provide data needed for proper error notifications
 *
 * @author n0weak
 */
public interface InformativeException {

    int getStartLine();

    int getEndLine();

    int getStartPos();

    int getEndPos();
}
