package ua.cn.stu.cs.ems.ecli;

import ua.cn.stu.cs.ems.ecli.errors.InterpretationException;
import ua.cn.stu.cs.ems.ecli.errors.ValidationException;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author n0weak
 */
public interface Interpreter {

    /**
     * Interprets the code, passed in an input stream.
     *
     * @param modelAccessor facade that connects interpreter with simulation system's inner structure, represents root aggregate,
     *                      null value is permitted if that structure is never accessed be the sources
     * @param inputStream   stream to obtain code for interpretation from
     * @return resulting interpretation report
     * @throws IOException             when interpreter fails to read from the passed input stream
     * @throws InterpretationException when interpreter fails because of an inner error
     * @throws ValidationException     when passed code is not valid, i.e. interpreter meets something that can be
     *                                 considered as an error with {@link ua.cn.stu.cs.jess.ecli.errors.ErrorCode.Severity#ERROR} severity.
     */
    public InterpretationReport interpret(InputStream inputStream, ModelAccessor modelAccessor) throws IOException;

    /**
     * Validates the code, no real calculations are performed. Naming and existence of aggregate-level variables, places and mark attributes won't be validated.
     *
     * @param inputStream inputStream stream to obtain code for interpretation from
     * @return error report. All the errors with {@link ua.cn.stu.cs.jess.ecli.errors.ErrorCode.Severity#WARNING} can be ignored, however
     *         it is not recommended. Ignorance of {@link ua.cn.stu.cs.jess.ecli.errors.ErrorCode.Severity#ERROR} errors will lead to the {@link ValidationException}
     *         thrown from {@link #interpret(java.io.InputStream, ModelAccessor)} method.
     * @throws IOException             when interpreter fails to read from the passed input stream
     * @throws InterpretationException when interpreter fails because of an inner error
     */
    public ValidationReport validate(InputStream inputStream) throws IOException;

    /**
     * Validates the code, no real calculations are performed. Naming and existence of aggregate-level variables, places and mark attributes will also be validated
     * as long as proper model accessor is supplied.
     * To validate existence of mark's attributes properly each place must return {@link EcliToken} instance to respond on {@link EcliToken#containsAttribute(Object)} calls.
     *
     * Note that all the statements that can affect model's inner structure (e.g. assignments to aggregate variables etc.) will be performed.
     * Hence the stub accessor should be provided instead of the real one.
     *
     * @param inputStream   inputStream stream to obtain code for interpretation from
     * @param modelAccessor facade that connects interpreter with simulation system's inner structure, represents root aggregate,
     *                      null value is permitted if that structure is never accessed be the sources
     * @return error report. All the errors with {@link ua.cn.stu.cs.jess.ecli.errors.ErrorCode.Severity#WARNING} can be ignored, however
     *         it is not recommended. Ignorance of {@link ua.cn.stu.cs.jess.ecli.errors.ErrorCode.Severity#ERROR} errors will lead to the {@link ValidationException}
     *         thrown from {@link #interpret(java.io.InputStream, ModelAccessor)} method.
     * @throws IOException             when interpreter fails to read from the passed input stream
     * @throws InterpretationException when interpreter fails because of an inner error
     */
    public ValidationReport validate(InputStream inputStream, ModelAccessor modelAccessor) throws IOException;

}
