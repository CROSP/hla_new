// $ANTLR 3.5.2 ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g 2016-04-20 18:12:59

    	package ua.cn.stu.cs.ems.ecli.impl;
		import ua.cn.stu.cs.ems.ecli.errors.*;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class ECLLexer extends Lexer {
	public static final int EOF=-1;
	public static final int T__68=68;
	public static final int T__69=69;
	public static final int T__70=70;
	public static final int T__71=71;
	public static final int T__72=72;
	public static final int T__73=73;
	public static final int T__74=74;
	public static final int T__75=75;
	public static final int T__76=76;
	public static final int ABS=4;
	public static final int AGGREGATE=5;
	public static final int ASSIGN=6;
	public static final int ATAN=7;
	public static final int BINOMIAL=8;
	public static final int BLOCK=9;
	public static final int COMMENT=10;
	public static final int COS=11;
	public static final int COT=12;
	public static final int CURRENT_MARK=13;
	public static final int DIGIT=14;
	public static final int DIV=15;
	public static final int ELSE=16;
	public static final int ENTIER=17;
	public static final int EQ=18;
	public static final int ESC=19;
	public static final int EXP=20;
	public static final int EXPONENTIAL=21;
	public static final int FALSE=22;
	public static final int GE=23;
	public static final int GT=24;
	public static final int ID=25;
	public static final int IF=26;
	public static final int IF_BEGIN=27;
	public static final int IF_END=28;
	public static final int IF_STMT=29;
	public static final int INT=30;
	public static final int INVERT=31;
	public static final int IS_MARKED=32;
	public static final int LAND=33;
	public static final int LE=34;
	public static final int LETTER=35;
	public static final int LN=36;
	public static final int LOR=37;
	public static final int LT=38;
	public static final int MALFORMED_ID=39;
	public static final int MARK=40;
	public static final int MINUS=41;
	public static final int MULT=42;
	public static final int NE=43;
	public static final int NORMAL=44;
	public static final int NOT=45;
	public static final int PLACE=46;
	public static final int PLUS=47;
	public static final int POISSON=48;
	public static final int POW=49;
	public static final int REAL=50;
	public static final int RETURN=51;
	public static final int ROOT_AGG=52;
	public static final int SEED=53;
	public static final int SIGN=54;
	public static final int SIN=55;
	public static final int SQRT=56;
	public static final int STMT_BEGIN=57;
	public static final int STMT_END=58;
	public static final int STR=59;
	public static final int SYS_VAR=60;
	public static final int TAN=61;
	public static final int TIME=62;
	public static final int TRUE=63;
	public static final int UNIFORM=64;
	public static final int VAR=65;
	public static final int VAR_DECL=66;
	public static final int WS=67;

		private List<RecognitionErrorListener> errorListeners = new ArrayList<RecognitionErrorListener>();

		public void addErrorListener(RecognitionErrorListener listener) {
		    errorListeners.add(listener);
	    }

	    public void removeErrorListener(RecognitionErrorListener listener) {
		    errorListeners.remove(listener);
	    }

	    public void fireRecognitionError(RecognitionException e) {
	        for (RecognitionErrorListener listener : errorListeners) {
	            listener.errorHappened(e, this);
	        }
	    }

		public void displayRecognitionError(String[] tokenNames, RecognitionException e) {
			fireRecognitionError(e);	
		}



	// delegates
	// delegators
	public Lexer[] getDelegates() {
		return new Lexer[] {};
	}

	public ECLLexer() {} 
	public ECLLexer(CharStream input) {
		this(input, new RecognizerSharedState());
	}
	public ECLLexer(CharStream input, RecognizerSharedState state) {
		super(input,state);
	}
	@Override public String getGrammarFileName() { return "ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g"; }

	// $ANTLR start "T__68"
	public final void mT__68() throws RecognitionException {
		try {
			int _type = T__68;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:29:7: ( '(' )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:29:9: '('
			{
			match('('); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__68"

	// $ANTLR start "T__69"
	public final void mT__69() throws RecognitionException {
		try {
			int _type = T__69;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:30:7: ( ')' )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:30:9: ')'
			{
			match(')'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__69"

	// $ANTLR start "T__70"
	public final void mT__70() throws RecognitionException {
		try {
			int _type = T__70;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:31:7: ( ',' )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:31:9: ','
			{
			match(','); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__70"

	// $ANTLR start "T__71"
	public final void mT__71() throws RecognitionException {
		try {
			int _type = T__71;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:32:7: ( '.' )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:32:9: '.'
			{
			match('.'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__71"

	// $ANTLR start "T__72"
	public final void mT__72() throws RecognitionException {
		try {
			int _type = T__72;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:33:7: ( ';' )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:33:9: ';'
			{
			match(';'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__72"

	// $ANTLR start "T__73"
	public final void mT__73() throws RecognitionException {
		try {
			int _type = T__73;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:34:7: ( '[' )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:34:9: '['
			{
			match('['); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__73"

	// $ANTLR start "T__74"
	public final void mT__74() throws RecognitionException {
		try {
			int _type = T__74;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:35:7: ( ']' )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:35:9: ']'
			{
			match(']'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__74"

	// $ANTLR start "T__75"
	public final void mT__75() throws RecognitionException {
		try {
			int _type = T__75;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:36:7: ( '{' )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:36:9: '{'
			{
			match('{'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__75"

	// $ANTLR start "T__76"
	public final void mT__76() throws RecognitionException {
		try {
			int _type = T__76;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:37:7: ( '}' )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:37:9: '}'
			{
			match('}'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__76"

	// $ANTLR start "COMMENT"
	public final void mCOMMENT() throws RecognitionException {
		try {
			int _type = COMMENT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:240:9: ( '//' (~ ( '\\r' | '\\n' ) )* ( '\\n' )? )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:240:11: '//' (~ ( '\\r' | '\\n' ) )* ( '\\n' )?
			{
			match("//"); 

			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:240:16: (~ ( '\\r' | '\\n' ) )*
			loop1:
			while (true) {
				int alt1=2;
				int LA1_0 = input.LA(1);
				if ( ((LA1_0 >= '\u0000' && LA1_0 <= '\t')||(LA1_0 >= '\u000B' && LA1_0 <= '\f')||(LA1_0 >= '\u000E' && LA1_0 <= '\uFFFF')) ) {
					alt1=1;
				}

				switch (alt1) {
				case 1 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:
					{
					if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '\t')||(input.LA(1) >= '\u000B' && input.LA(1) <= '\f')||(input.LA(1) >= '\u000E' && input.LA(1) <= '\uFFFF') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					break loop1;
				}
			}

			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:240:36: ( '\\n' )?
			int alt2=2;
			int LA2_0 = input.LA(1);
			if ( (LA2_0=='\n') ) {
				alt2=1;
			}
			switch (alt2) {
				case 1 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:240:36: '\\n'
					{
					match('\n'); 
					}
					break;

			}

			_channel = HIDDEN;
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "COMMENT"

	// $ANTLR start "LETTER"
	public final void mLETTER() throws RecognitionException {
		try {
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:244:3: ( ( 'a' .. 'z' | 'A' .. 'Z' )+ )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:244:5: ( 'a' .. 'z' | 'A' .. 'Z' )+
			{
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:244:5: ( 'a' .. 'z' | 'A' .. 'Z' )+
			int cnt3=0;
			loop3:
			while (true) {
				int alt3=2;
				int LA3_0 = input.LA(1);
				if ( ((LA3_0 >= 'A' && LA3_0 <= 'Z')||(LA3_0 >= 'a' && LA3_0 <= 'z')) ) {
					alt3=1;
				}

				switch (alt3) {
				case 1 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:
					{
					if ( (input.LA(1) >= 'A' && input.LA(1) <= 'Z')||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt3 >= 1 ) break loop3;
					EarlyExitException eee = new EarlyExitException(3, input);
					throw eee;
				}
				cnt3++;
			}

			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LETTER"

	// $ANTLR start "DIGIT"
	public final void mDIGIT() throws RecognitionException {
		try {
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:246:3: ( '0' .. '9' )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:
			{
			if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DIGIT"

	// $ANTLR start "ESC"
	public final void mESC() throws RecognitionException {
		try {
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:250:5: ( '\\\\' ( 'n' | 'r' | 't' | 'b' | 'f' | '\"' | '\\'' | '/' | '\\\\' ) )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:250:9: '\\\\' ( 'n' | 'r' | 't' | 'b' | 'f' | '\"' | '\\'' | '/' | '\\\\' )
			{
			match('\\'); 
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:251:9: ( 'n' | 'r' | 't' | 'b' | 'f' | '\"' | '\\'' | '/' | '\\\\' )
			int alt4=9;
			switch ( input.LA(1) ) {
			case 'n':
				{
				alt4=1;
				}
				break;
			case 'r':
				{
				alt4=2;
				}
				break;
			case 't':
				{
				alt4=3;
				}
				break;
			case 'b':
				{
				alt4=4;
				}
				break;
			case 'f':
				{
				alt4=5;
				}
				break;
			case '\"':
				{
				alt4=6;
				}
				break;
			case '\'':
				{
				alt4=7;
				}
				break;
			case '/':
				{
				alt4=8;
				}
				break;
			case '\\':
				{
				alt4=9;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 4, 0, input);
				throw nvae;
			}
			switch (alt4) {
				case 1 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:251:17: 'n'
					{
					match('n'); 
					setText("\n");
					}
					break;
				case 2 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:252:17: 'r'
					{
					match('r'); 
					setText("\r");
					}
					break;
				case 3 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:253:17: 't'
					{
					match('t'); 
					setText("\t");
					}
					break;
				case 4 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:254:17: 'b'
					{
					match('b'); 
					setText("\b");
					}
					break;
				case 5 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:255:17: 'f'
					{
					match('f'); 
					setText("\f");
					}
					break;
				case 6 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:256:17: '\"'
					{
					match('\"'); 
					setText("\"");
					}
					break;
				case 7 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:257:17: '\\''
					{
					match('\''); 
					setText("\'");
					}
					break;
				case 8 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:258:17: '/'
					{
					match('/'); 
					setText("/");
					}
					break;
				case 9 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:259:17: '\\\\'
					{
					match('\\'); 
					setText("\\");
					}
					break;

			}

			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ESC"

	// $ANTLR start "STR"
	public final void mSTR() throws RecognitionException {
		try {
			int _type = STR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			CommonToken escaped=null;
			int normal;

			StringBuilder lBuf = new StringBuilder();
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:265:3: ( '\"' (escaped= ESC |normal=~ ( '\\\\' | '\"' | '\\n' | '\\r' ) )* '\"' | '\\'' (escaped= ESC |normal=~ ( '\\\\' | '\\'' | '\\n' | '\\r' ) )* '\\'' )
			int alt7=2;
			int LA7_0 = input.LA(1);
			if ( (LA7_0=='\"') ) {
				alt7=1;
			}
			else if ( (LA7_0=='\'') ) {
				alt7=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 7, 0, input);
				throw nvae;
			}

			switch (alt7) {
				case 1 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:266:12: '\"' (escaped= ESC |normal=~ ( '\\\\' | '\"' | '\\n' | '\\r' ) )* '\"'
					{
					match('\"'); 
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:267:12: (escaped= ESC |normal=~ ( '\\\\' | '\"' | '\\n' | '\\r' ) )*
					loop5:
					while (true) {
						int alt5=3;
						int LA5_0 = input.LA(1);
						if ( (LA5_0=='\\') ) {
							alt5=1;
						}
						else if ( ((LA5_0 >= '\u0000' && LA5_0 <= '\t')||(LA5_0 >= '\u000B' && LA5_0 <= '\f')||(LA5_0 >= '\u000E' && LA5_0 <= '!')||(LA5_0 >= '#' && LA5_0 <= '[')||(LA5_0 >= ']' && LA5_0 <= '\uFFFF')) ) {
							alt5=2;
						}

						switch (alt5) {
						case 1 :
							// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:268:5: escaped= ESC
							{
							int escapedStart444 = getCharIndex();
							int escapedStartLine444 = getLine();
							int escapedStartCharPos444 = getCharPositionInLine();
							mESC(); 
							escaped = new CommonToken(input, Token.INVALID_TOKEN_TYPE, Token.DEFAULT_CHANNEL, escapedStart444, getCharIndex()-1);
							escaped.setLine(escapedStartLine444);
							escaped.setCharPositionInLine(escapedStartCharPos444);

							lBuf.append(getText());
							}
							break;
						case 2 :
							// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:270:5: normal=~ ( '\\\\' | '\"' | '\\n' | '\\r' )
							{
							normal= input.LA(1);
							if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '\t')||(input.LA(1) >= '\u000B' && input.LA(1) <= '\f')||(input.LA(1) >= '\u000E' && input.LA(1) <= '!')||(input.LA(1) >= '#' && input.LA(1) <= '[')||(input.LA(1) >= ']' && input.LA(1) <= '\uFFFF') ) {
								input.consume();
							}
							else {
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							lBuf.appendCodePoint(normal);
							}
							break;

						default :
							break loop5;
						}
					}

					match('\"'); 
					setText(lBuf.toString());
					}
					break;
				case 2 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:275:12: '\\'' (escaped= ESC |normal=~ ( '\\\\' | '\\'' | '\\n' | '\\r' ) )* '\\''
					{
					match('\''); 
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:276:4: (escaped= ESC |normal=~ ( '\\\\' | '\\'' | '\\n' | '\\r' ) )*
					loop6:
					while (true) {
						int alt6=3;
						int LA6_0 = input.LA(1);
						if ( (LA6_0=='\\') ) {
							alt6=1;
						}
						else if ( ((LA6_0 >= '\u0000' && LA6_0 <= '\t')||(LA6_0 >= '\u000B' && LA6_0 <= '\f')||(LA6_0 >= '\u000E' && LA6_0 <= '&')||(LA6_0 >= '(' && LA6_0 <= '[')||(LA6_0 >= ']' && LA6_0 <= '\uFFFF')) ) {
							alt6=2;
						}

						switch (alt6) {
						case 1 :
							// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:277:5: escaped= ESC
							{
							int escapedStart543 = getCharIndex();
							int escapedStartLine543 = getLine();
							int escapedStartCharPos543 = getCharPositionInLine();
							mESC(); 
							escaped = new CommonToken(input, Token.INVALID_TOKEN_TYPE, Token.DEFAULT_CHANNEL, escapedStart543, getCharIndex()-1);
							escaped.setLine(escapedStartLine543);
							escaped.setCharPositionInLine(escapedStartCharPos543);

							lBuf.append(getText());
							}
							break;
						case 2 :
							// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:279:5: normal=~ ( '\\\\' | '\\'' | '\\n' | '\\r' )
							{
							normal= input.LA(1);
							if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '\t')||(input.LA(1) >= '\u000B' && input.LA(1) <= '\f')||(input.LA(1) >= '\u000E' && input.LA(1) <= '&')||(input.LA(1) >= '(' && input.LA(1) <= '[')||(input.LA(1) >= ']' && input.LA(1) <= '\uFFFF') ) {
								input.consume();
							}
							else {
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							lBuf.appendCodePoint(normal);
							}
							break;

						default :
							break loop6;
						}
					}

					match('\''); 
					setText(lBuf.toString());
					}
					break;

			}
			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "STR"

	// $ANTLR start "ENTIER"
	public final void mENTIER() throws RecognitionException {
		try {
			int _type = ENTIER;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:285:8: ( 'ENTIER' )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:285:10: 'ENTIER'
			{
			match("ENTIER"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ENTIER"

	// $ANTLR start "SIGN"
	public final void mSIGN() throws RecognitionException {
		try {
			int _type = SIGN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:286:6: ( 'SIGN' )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:286:8: 'SIGN'
			{
			match("SIGN"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "SIGN"

	// $ANTLR start "EXP"
	public final void mEXP() throws RecognitionException {
		try {
			int _type = EXP;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:287:6: ( 'E' )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:287:8: 'E'
			{
			match('E'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "EXP"

	// $ANTLR start "SQRT"
	public final void mSQRT() throws RecognitionException {
		try {
			int _type = SQRT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:288:6: ( 'SQRT' )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:288:8: 'SQRT'
			{
			match("SQRT"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "SQRT"

	// $ANTLR start "ABS"
	public final void mABS() throws RecognitionException {
		try {
			int _type = ABS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:289:6: ( 'ABS' )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:289:8: 'ABS'
			{
			match("ABS"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ABS"

	// $ANTLR start "LN"
	public final void mLN() throws RecognitionException {
		try {
			int _type = LN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:290:5: ( 'LN' )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:290:7: 'LN'
			{
			match("LN"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LN"

	// $ANTLR start "POW"
	public final void mPOW() throws RecognitionException {
		try {
			int _type = POW;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:291:6: ( '^' )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:291:8: '^'
			{
			match('^'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "POW"

	// $ANTLR start "TIME"
	public final void mTIME() throws RecognitionException {
		try {
			int _type = TIME;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:292:6: ( 'TIME' )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:292:8: 'TIME'
			{
			match("TIME"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "TIME"

	// $ANTLR start "ROOT_AGG"
	public final void mROOT_AGG() throws RecognitionException {
		try {
			int _type = ROOT_AGG;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:294:3: ( 'ROOT' )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:294:5: 'ROOT'
			{
			match("ROOT"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ROOT_AGG"

	// $ANTLR start "AGGREGATE"
	public final void mAGGREGATE() throws RecognitionException {
		try {
			int _type = AGGREGATE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:296:3: ( 'A' )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:296:5: 'A'
			{
			match('A'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "AGGREGATE"

	// $ANTLR start "PLACE"
	public final void mPLACE() throws RecognitionException {
		try {
			int _type = PLACE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:297:7: ( 'P' )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:297:9: 'P'
			{
			match('P'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "PLACE"

	// $ANTLR start "MARK"
	public final void mMARK() throws RecognitionException {
		try {
			int _type = MARK;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:298:6: ( 'T' )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:298:8: 'T'
			{
			match('T'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "MARK"

	// $ANTLR start "SYS_VAR"
	public final void mSYS_VAR() throws RecognitionException {
		try {
			int _type = SYS_VAR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:300:3: ( 'V' )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:300:5: 'V'
			{
			match('V'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "SYS_VAR"

	// $ANTLR start "POISSON"
	public final void mPOISSON() throws RecognitionException {
		try {
			int _type = POISSON;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:301:9: ( 'POISSON' )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:301:11: 'POISSON'
			{
			match("POISSON"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "POISSON"

	// $ANTLR start "UNIFORM"
	public final void mUNIFORM() throws RecognitionException {
		try {
			int _type = UNIFORM;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:302:9: ( 'UNIFORM' )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:302:11: 'UNIFORM'
			{
			match("UNIFORM"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "UNIFORM"

	// $ANTLR start "EXPONENTIAL"
	public final void mEXPONENTIAL() throws RecognitionException {
		try {
			int _type = EXPONENTIAL;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:304:3: ( 'EXPONENTIAL' )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:304:5: 'EXPONENTIAL'
			{
			match("EXPONENTIAL"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "EXPONENTIAL"

	// $ANTLR start "NORMAL"
	public final void mNORMAL() throws RecognitionException {
		try {
			int _type = NORMAL;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:305:8: ( 'NORMAL' )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:305:10: 'NORMAL'
			{
			match("NORMAL"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NORMAL"

	// $ANTLR start "BINOMIAL"
	public final void mBINOMIAL() throws RecognitionException {
		try {
			int _type = BINOMIAL;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:307:3: ( 'BINOMIAL' )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:307:5: 'BINOMIAL'
			{
			match("BINOMIAL"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "BINOMIAL"

	// $ANTLR start "SEED"
	public final void mSEED() throws RecognitionException {
		try {
			int _type = SEED;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:308:6: ( 'SEED' )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:308:8: 'SEED'
			{
			match("SEED"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "SEED"

	// $ANTLR start "TRUE"
	public final void mTRUE() throws RecognitionException {
		try {
			int _type = TRUE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:309:9: ( 'TRUE' )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:309:13: 'TRUE'
			{
			match("TRUE"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "TRUE"

	// $ANTLR start "FALSE"
	public final void mFALSE() throws RecognitionException {
		try {
			int _type = FALSE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:310:9: ( 'FALSE' )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:310:13: 'FALSE'
			{
			match("FALSE"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "FALSE"

	// $ANTLR start "IF"
	public final void mIF() throws RecognitionException {
		try {
			int _type = IF;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:311:5: ( 'IF' )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:311:8: 'IF'
			{
			match("IF"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "IF"

	// $ANTLR start "ELSE"
	public final void mELSE() throws RecognitionException {
		try {
			int _type = ELSE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:312:6: ( 'ELSE' )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:312:8: 'ELSE'
			{
			match("ELSE"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ELSE"

	// $ANTLR start "LOR"
	public final void mLOR() throws RecognitionException {
		try {
			int _type = LOR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:313:6: ( '||' )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:313:8: '||'
			{
			match("||"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LOR"

	// $ANTLR start "LAND"
	public final void mLAND() throws RecognitionException {
		try {
			int _type = LAND;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:314:6: ( '&&' )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:314:8: '&&'
			{
			match("&&"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LAND"

	// $ANTLR start "NOT"
	public final void mNOT() throws RecognitionException {
		try {
			int _type = NOT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:315:6: ( '!' )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:315:8: '!'
			{
			match('!'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NOT"

	// $ANTLR start "EQ"
	public final void mEQ() throws RecognitionException {
		try {
			int _type = EQ;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:316:5: ( '==' )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:316:7: '=='
			{
			match("=="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "EQ"

	// $ANTLR start "NE"
	public final void mNE() throws RecognitionException {
		try {
			int _type = NE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:317:5: ( '!=' )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:317:7: '!='
			{
			match("!="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NE"

	// $ANTLR start "GE"
	public final void mGE() throws RecognitionException {
		try {
			int _type = GE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:318:5: ( '>=' )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:318:7: '>='
			{
			match(">="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "GE"

	// $ANTLR start "LE"
	public final void mLE() throws RecognitionException {
		try {
			int _type = LE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:319:5: ( '<=' )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:319:7: '<='
			{
			match("<="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LE"

	// $ANTLR start "GT"
	public final void mGT() throws RecognitionException {
		try {
			int _type = GT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:320:5: ( '>' )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:320:7: '>'
			{
			match('>'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "GT"

	// $ANTLR start "LT"
	public final void mLT() throws RecognitionException {
		try {
			int _type = LT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:321:5: ( '<' )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:321:7: '<'
			{
			match('<'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LT"

	// $ANTLR start "VAR"
	public final void mVAR() throws RecognitionException {
		try {
			int _type = VAR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:322:6: ( 'VAR' )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:322:8: 'VAR'
			{
			match("VAR"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "VAR"

	// $ANTLR start "SIN"
	public final void mSIN() throws RecognitionException {
		try {
			int _type = SIN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:323:6: ( 'SIN' )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:323:8: 'SIN'
			{
			match("SIN"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "SIN"

	// $ANTLR start "COS"
	public final void mCOS() throws RecognitionException {
		try {
			int _type = COS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:324:6: ( 'COS' )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:324:8: 'COS'
			{
			match("COS"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "COS"

	// $ANTLR start "ATAN"
	public final void mATAN() throws RecognitionException {
		try {
			int _type = ATAN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:326:3: ( 'ATAN' )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:326:5: 'ATAN'
			{
			match("ATAN"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ATAN"

	// $ANTLR start "TAN"
	public final void mTAN() throws RecognitionException {
		try {
			int _type = TAN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:327:6: ( 'TAN' )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:327:8: 'TAN'
			{
			match("TAN"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "TAN"

	// $ANTLR start "COT"
	public final void mCOT() throws RecognitionException {
		try {
			int _type = COT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:328:6: ( 'COT' )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:328:8: 'COT'
			{
			match("COT"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "COT"

	// $ANTLR start "PLUS"
	public final void mPLUS() throws RecognitionException {
		try {
			int _type = PLUS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:329:7: ( '+' )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:329:10: '+'
			{
			match('+'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "PLUS"

	// $ANTLR start "MINUS"
	public final void mMINUS() throws RecognitionException {
		try {
			int _type = MINUS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:330:7: ( '-' )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:330:10: '-'
			{
			match('-'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "MINUS"

	// $ANTLR start "MULT"
	public final void mMULT() throws RecognitionException {
		try {
			int _type = MULT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:331:6: ( '*' )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:331:9: '*'
			{
			match('*'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "MULT"

	// $ANTLR start "DIV"
	public final void mDIV() throws RecognitionException {
		try {
			int _type = DIV;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:332:6: ( '/' )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:332:9: '/'
			{
			match('/'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DIV"

	// $ANTLR start "ASSIGN"
	public final void mASSIGN() throws RecognitionException {
		try {
			int _type = ASSIGN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:333:8: ( '=' )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:333:11: '='
			{
			match('='); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ASSIGN"

	// $ANTLR start "RETURN"
	public final void mRETURN() throws RecognitionException {
		try {
			int _type = RETURN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:334:8: ( 'RETURN' )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:334:10: 'RETURN'
			{
			match("RETURN"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "RETURN"

	// $ANTLR start "INT"
	public final void mINT() throws RecognitionException {
		try {
			int _type = INT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:335:6: ( ( DIGIT )+ )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:335:8: ( DIGIT )+
			{
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:335:8: ( DIGIT )+
			int cnt8=0;
			loop8:
			while (true) {
				int alt8=2;
				int LA8_0 = input.LA(1);
				if ( ((LA8_0 >= '0' && LA8_0 <= '9')) ) {
					alt8=1;
				}

				switch (alt8) {
				case 1 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt8 >= 1 ) break loop8;
					EarlyExitException eee = new EarlyExitException(8, input);
					throw eee;
				}
				cnt8++;
			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "INT"

	// $ANTLR start "REAL"
	public final void mREAL() throws RecognitionException {
		try {
			int _type = REAL;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:336:6: ( ( DIGIT )+ '.' ( DIGIT )+ | ( DIGIT )+ 'E' ( '-' | '+' )? ( DIGIT )+ | ( DIGIT )+ '.' ( DIGIT )+ 'E' ( '-' | '+' )? ( DIGIT )+ )
			int alt18=3;
			alt18 = dfa18.predict(input);
			switch (alt18) {
				case 1 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:336:8: ( DIGIT )+ '.' ( DIGIT )+
					{
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:336:8: ( DIGIT )+
					int cnt9=0;
					loop9:
					while (true) {
						int alt9=2;
						int LA9_0 = input.LA(1);
						if ( ((LA9_0 >= '0' && LA9_0 <= '9')) ) {
							alt9=1;
						}

						switch (alt9) {
						case 1 :
							// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:
							{
							if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
								input.consume();
							}
							else {
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							}
							break;

						default :
							if ( cnt9 >= 1 ) break loop9;
							EarlyExitException eee = new EarlyExitException(9, input);
							throw eee;
						}
						cnt9++;
					}

					match('.'); 
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:336:19: ( DIGIT )+
					int cnt10=0;
					loop10:
					while (true) {
						int alt10=2;
						int LA10_0 = input.LA(1);
						if ( ((LA10_0 >= '0' && LA10_0 <= '9')) ) {
							alt10=1;
						}

						switch (alt10) {
						case 1 :
							// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:
							{
							if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
								input.consume();
							}
							else {
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							}
							break;

						default :
							if ( cnt10 >= 1 ) break loop10;
							EarlyExitException eee = new EarlyExitException(10, input);
							throw eee;
						}
						cnt10++;
					}

					}
					break;
				case 2 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:336:28: ( DIGIT )+ 'E' ( '-' | '+' )? ( DIGIT )+
					{
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:336:28: ( DIGIT )+
					int cnt11=0;
					loop11:
					while (true) {
						int alt11=2;
						int LA11_0 = input.LA(1);
						if ( ((LA11_0 >= '0' && LA11_0 <= '9')) ) {
							alt11=1;
						}

						switch (alt11) {
						case 1 :
							// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:
							{
							if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
								input.consume();
							}
							else {
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							}
							break;

						default :
							if ( cnt11 >= 1 ) break loop11;
							EarlyExitException eee = new EarlyExitException(11, input);
							throw eee;
						}
						cnt11++;
					}

					match('E'); 
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:336:39: ( '-' | '+' )?
					int alt12=2;
					int LA12_0 = input.LA(1);
					if ( (LA12_0=='+'||LA12_0=='-') ) {
						alt12=1;
					}
					switch (alt12) {
						case 1 :
							// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:
							{
							if ( input.LA(1)=='+'||input.LA(1)=='-' ) {
								input.consume();
							}
							else {
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							}
							break;

					}

					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:336:52: ( DIGIT )+
					int cnt13=0;
					loop13:
					while (true) {
						int alt13=2;
						int LA13_0 = input.LA(1);
						if ( ((LA13_0 >= '0' && LA13_0 <= '9')) ) {
							alt13=1;
						}

						switch (alt13) {
						case 1 :
							// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:
							{
							if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
								input.consume();
							}
							else {
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							}
							break;

						default :
							if ( cnt13 >= 1 ) break loop13;
							EarlyExitException eee = new EarlyExitException(13, input);
							throw eee;
						}
						cnt13++;
					}

					}
					break;
				case 3 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:336:61: ( DIGIT )+ '.' ( DIGIT )+ 'E' ( '-' | '+' )? ( DIGIT )+
					{
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:336:61: ( DIGIT )+
					int cnt14=0;
					loop14:
					while (true) {
						int alt14=2;
						int LA14_0 = input.LA(1);
						if ( ((LA14_0 >= '0' && LA14_0 <= '9')) ) {
							alt14=1;
						}

						switch (alt14) {
						case 1 :
							// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:
							{
							if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
								input.consume();
							}
							else {
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							}
							break;

						default :
							if ( cnt14 >= 1 ) break loop14;
							EarlyExitException eee = new EarlyExitException(14, input);
							throw eee;
						}
						cnt14++;
					}

					match('.'); 
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:336:72: ( DIGIT )+
					int cnt15=0;
					loop15:
					while (true) {
						int alt15=2;
						int LA15_0 = input.LA(1);
						if ( ((LA15_0 >= '0' && LA15_0 <= '9')) ) {
							alt15=1;
						}

						switch (alt15) {
						case 1 :
							// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:
							{
							if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
								input.consume();
							}
							else {
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							}
							break;

						default :
							if ( cnt15 >= 1 ) break loop15;
							EarlyExitException eee = new EarlyExitException(15, input);
							throw eee;
						}
						cnt15++;
					}

					match('E'); 
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:336:83: ( '-' | '+' )?
					int alt16=2;
					int LA16_0 = input.LA(1);
					if ( (LA16_0=='+'||LA16_0=='-') ) {
						alt16=1;
					}
					switch (alt16) {
						case 1 :
							// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:
							{
							if ( input.LA(1)=='+'||input.LA(1)=='-' ) {
								input.consume();
							}
							else {
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							}
							break;

					}

					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:336:96: ( DIGIT )+
					int cnt17=0;
					loop17:
					while (true) {
						int alt17=2;
						int LA17_0 = input.LA(1);
						if ( ((LA17_0 >= '0' && LA17_0 <= '9')) ) {
							alt17=1;
						}

						switch (alt17) {
						case 1 :
							// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:
							{
							if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
								input.consume();
							}
							else {
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							}
							break;

						default :
							if ( cnt17 >= 1 ) break loop17;
							EarlyExitException eee = new EarlyExitException(17, input);
							throw eee;
						}
						cnt17++;
					}

					}
					break;

			}
			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "REAL"

	// $ANTLR start "ID"
	public final void mID() throws RecognitionException {
		try {
			int _type = ID;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:337:6: ( ( LETTER | '_' ) ( LETTER | DIGIT | '_' )* )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:337:8: ( LETTER | '_' ) ( LETTER | DIGIT | '_' )*
			{
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:337:8: ( LETTER | '_' )
			int alt19=2;
			int LA19_0 = input.LA(1);
			if ( ((LA19_0 >= 'A' && LA19_0 <= 'Z')||(LA19_0 >= 'a' && LA19_0 <= 'z')) ) {
				alt19=1;
			}
			else if ( (LA19_0=='_') ) {
				alt19=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 19, 0, input);
				throw nvae;
			}

			switch (alt19) {
				case 1 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:337:9: LETTER
					{
					mLETTER(); 

					}
					break;
				case 2 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:337:18: '_'
					{
					match('_'); 
					}
					break;

			}

			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:337:23: ( LETTER | DIGIT | '_' )*
			loop20:
			while (true) {
				int alt20=4;
				switch ( input.LA(1) ) {
				case 'A':
				case 'B':
				case 'C':
				case 'D':
				case 'E':
				case 'F':
				case 'G':
				case 'H':
				case 'I':
				case 'J':
				case 'K':
				case 'L':
				case 'M':
				case 'N':
				case 'O':
				case 'P':
				case 'Q':
				case 'R':
				case 'S':
				case 'T':
				case 'U':
				case 'V':
				case 'W':
				case 'X':
				case 'Y':
				case 'Z':
				case 'a':
				case 'b':
				case 'c':
				case 'd':
				case 'e':
				case 'f':
				case 'g':
				case 'h':
				case 'i':
				case 'j':
				case 'k':
				case 'l':
				case 'm':
				case 'n':
				case 'o':
				case 'p':
				case 'q':
				case 'r':
				case 's':
				case 't':
				case 'u':
				case 'v':
				case 'w':
				case 'x':
				case 'y':
				case 'z':
					{
					alt20=1;
					}
					break;
				case '0':
				case '1':
				case '2':
				case '3':
				case '4':
				case '5':
				case '6':
				case '7':
				case '8':
				case '9':
					{
					alt20=2;
					}
					break;
				case '_':
					{
					alt20=3;
					}
					break;
				}
				switch (alt20) {
				case 1 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:337:24: LETTER
					{
					mLETTER(); 

					}
					break;
				case 2 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:337:33: DIGIT
					{
					mDIGIT(); 

					}
					break;
				case 3 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:337:41: '_'
					{
					match('_'); 
					}
					break;

				default :
					break loop20;
				}
			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ID"

	// $ANTLR start "MALFORMED_ID"
	public final void mMALFORMED_ID() throws RecognitionException {
		try {
			int _type = MALFORMED_ID;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:339:3: ( ( LETTER | DIGIT | '_' ) ( LETTER | DIGIT | '_' )* )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:339:5: ( LETTER | DIGIT | '_' ) ( LETTER | DIGIT | '_' )*
			{
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:339:5: ( LETTER | DIGIT | '_' )
			int alt21=3;
			switch ( input.LA(1) ) {
			case 'A':
			case 'B':
			case 'C':
			case 'D':
			case 'E':
			case 'F':
			case 'G':
			case 'H':
			case 'I':
			case 'J':
			case 'K':
			case 'L':
			case 'M':
			case 'N':
			case 'O':
			case 'P':
			case 'Q':
			case 'R':
			case 'S':
			case 'T':
			case 'U':
			case 'V':
			case 'W':
			case 'X':
			case 'Y':
			case 'Z':
			case 'a':
			case 'b':
			case 'c':
			case 'd':
			case 'e':
			case 'f':
			case 'g':
			case 'h':
			case 'i':
			case 'j':
			case 'k':
			case 'l':
			case 'm':
			case 'n':
			case 'o':
			case 'p':
			case 'q':
			case 'r':
			case 's':
			case 't':
			case 'u':
			case 'v':
			case 'w':
			case 'x':
			case 'y':
			case 'z':
				{
				alt21=1;
				}
				break;
			case '0':
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
				{
				alt21=2;
				}
				break;
			case '_':
				{
				alt21=3;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 21, 0, input);
				throw nvae;
			}
			switch (alt21) {
				case 1 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:339:6: LETTER
					{
					mLETTER(); 

					}
					break;
				case 2 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:339:15: DIGIT
					{
					mDIGIT(); 

					}
					break;
				case 3 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:339:23: '_'
					{
					match('_'); 
					}
					break;

			}

			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:339:28: ( LETTER | DIGIT | '_' )*
			loop22:
			while (true) {
				int alt22=4;
				switch ( input.LA(1) ) {
				case 'A':
				case 'B':
				case 'C':
				case 'D':
				case 'E':
				case 'F':
				case 'G':
				case 'H':
				case 'I':
				case 'J':
				case 'K':
				case 'L':
				case 'M':
				case 'N':
				case 'O':
				case 'P':
				case 'Q':
				case 'R':
				case 'S':
				case 'T':
				case 'U':
				case 'V':
				case 'W':
				case 'X':
				case 'Y':
				case 'Z':
				case 'a':
				case 'b':
				case 'c':
				case 'd':
				case 'e':
				case 'f':
				case 'g':
				case 'h':
				case 'i':
				case 'j':
				case 'k':
				case 'l':
				case 'm':
				case 'n':
				case 'o':
				case 'p':
				case 'q':
				case 'r':
				case 's':
				case 't':
				case 'u':
				case 'v':
				case 'w':
				case 'x':
				case 'y':
				case 'z':
					{
					alt22=1;
					}
					break;
				case '0':
				case '1':
				case '2':
				case '3':
				case '4':
				case '5':
				case '6':
				case '7':
				case '8':
				case '9':
					{
					alt22=2;
					}
					break;
				case '_':
					{
					alt22=3;
					}
					break;
				}
				switch (alt22) {
				case 1 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:339:29: LETTER
					{
					mLETTER(); 

					}
					break;
				case 2 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:339:38: DIGIT
					{
					mDIGIT(); 

					}
					break;
				case 3 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:339:46: '_'
					{
					match('_'); 
					}
					break;

				default :
					break loop22;
				}
			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "MALFORMED_ID"

	// $ANTLR start "WS"
	public final void mWS() throws RecognitionException {
		try {
			int _type = WS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:340:6: ( ( ' ' | '\\t' | '\\n' | '\\r' )+ )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:340:8: ( ' ' | '\\t' | '\\n' | '\\r' )+
			{
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:340:8: ( ' ' | '\\t' | '\\n' | '\\r' )+
			int cnt23=0;
			loop23:
			while (true) {
				int alt23=2;
				int LA23_0 = input.LA(1);
				if ( ((LA23_0 >= '\t' && LA23_0 <= '\n')||LA23_0=='\r'||LA23_0==' ') ) {
					alt23=1;
				}

				switch (alt23) {
				case 1 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:
					{
					if ( (input.LA(1) >= '\t' && input.LA(1) <= '\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt23 >= 1 ) break loop23;
					EarlyExitException eee = new EarlyExitException(23, input);
					throw eee;
				}
				cnt23++;
			}

			_channel = HIDDEN;
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "WS"

	@Override
	public void mTokens() throws RecognitionException {
		// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:1:8: ( T__68 | T__69 | T__70 | T__71 | T__72 | T__73 | T__74 | T__75 | T__76 | COMMENT | STR | ENTIER | SIGN | EXP | SQRT | ABS | LN | POW | TIME | ROOT_AGG | AGGREGATE | PLACE | MARK | SYS_VAR | POISSON | UNIFORM | EXPONENTIAL | NORMAL | BINOMIAL | SEED | TRUE | FALSE | IF | ELSE | LOR | LAND | NOT | EQ | NE | GE | LE | GT | LT | VAR | SIN | COS | ATAN | TAN | COT | PLUS | MINUS | MULT | DIV | ASSIGN | RETURN | INT | REAL | ID | MALFORMED_ID | WS )
		int alt24=60;
		alt24 = dfa24.predict(input);
		switch (alt24) {
			case 1 :
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:1:10: T__68
				{
				mT__68(); 

				}
				break;
			case 2 :
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:1:16: T__69
				{
				mT__69(); 

				}
				break;
			case 3 :
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:1:22: T__70
				{
				mT__70(); 

				}
				break;
			case 4 :
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:1:28: T__71
				{
				mT__71(); 

				}
				break;
			case 5 :
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:1:34: T__72
				{
				mT__72(); 

				}
				break;
			case 6 :
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:1:40: T__73
				{
				mT__73(); 

				}
				break;
			case 7 :
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:1:46: T__74
				{
				mT__74(); 

				}
				break;
			case 8 :
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:1:52: T__75
				{
				mT__75(); 

				}
				break;
			case 9 :
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:1:58: T__76
				{
				mT__76(); 

				}
				break;
			case 10 :
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:1:64: COMMENT
				{
				mCOMMENT(); 

				}
				break;
			case 11 :
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:1:72: STR
				{
				mSTR(); 

				}
				break;
			case 12 :
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:1:76: ENTIER
				{
				mENTIER(); 

				}
				break;
			case 13 :
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:1:83: SIGN
				{
				mSIGN(); 

				}
				break;
			case 14 :
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:1:88: EXP
				{
				mEXP(); 

				}
				break;
			case 15 :
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:1:92: SQRT
				{
				mSQRT(); 

				}
				break;
			case 16 :
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:1:97: ABS
				{
				mABS(); 

				}
				break;
			case 17 :
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:1:101: LN
				{
				mLN(); 

				}
				break;
			case 18 :
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:1:104: POW
				{
				mPOW(); 

				}
				break;
			case 19 :
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:1:108: TIME
				{
				mTIME(); 

				}
				break;
			case 20 :
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:1:113: ROOT_AGG
				{
				mROOT_AGG(); 

				}
				break;
			case 21 :
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:1:122: AGGREGATE
				{
				mAGGREGATE(); 

				}
				break;
			case 22 :
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:1:132: PLACE
				{
				mPLACE(); 

				}
				break;
			case 23 :
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:1:138: MARK
				{
				mMARK(); 

				}
				break;
			case 24 :
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:1:143: SYS_VAR
				{
				mSYS_VAR(); 

				}
				break;
			case 25 :
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:1:151: POISSON
				{
				mPOISSON(); 

				}
				break;
			case 26 :
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:1:159: UNIFORM
				{
				mUNIFORM(); 

				}
				break;
			case 27 :
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:1:167: EXPONENTIAL
				{
				mEXPONENTIAL(); 

				}
				break;
			case 28 :
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:1:179: NORMAL
				{
				mNORMAL(); 

				}
				break;
			case 29 :
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:1:186: BINOMIAL
				{
				mBINOMIAL(); 

				}
				break;
			case 30 :
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:1:195: SEED
				{
				mSEED(); 

				}
				break;
			case 31 :
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:1:200: TRUE
				{
				mTRUE(); 

				}
				break;
			case 32 :
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:1:205: FALSE
				{
				mFALSE(); 

				}
				break;
			case 33 :
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:1:211: IF
				{
				mIF(); 

				}
				break;
			case 34 :
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:1:214: ELSE
				{
				mELSE(); 

				}
				break;
			case 35 :
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:1:219: LOR
				{
				mLOR(); 

				}
				break;
			case 36 :
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:1:223: LAND
				{
				mLAND(); 

				}
				break;
			case 37 :
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:1:228: NOT
				{
				mNOT(); 

				}
				break;
			case 38 :
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:1:232: EQ
				{
				mEQ(); 

				}
				break;
			case 39 :
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:1:235: NE
				{
				mNE(); 

				}
				break;
			case 40 :
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:1:238: GE
				{
				mGE(); 

				}
				break;
			case 41 :
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:1:241: LE
				{
				mLE(); 

				}
				break;
			case 42 :
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:1:244: GT
				{
				mGT(); 

				}
				break;
			case 43 :
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:1:247: LT
				{
				mLT(); 

				}
				break;
			case 44 :
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:1:250: VAR
				{
				mVAR(); 

				}
				break;
			case 45 :
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:1:254: SIN
				{
				mSIN(); 

				}
				break;
			case 46 :
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:1:258: COS
				{
				mCOS(); 

				}
				break;
			case 47 :
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:1:262: ATAN
				{
				mATAN(); 

				}
				break;
			case 48 :
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:1:267: TAN
				{
				mTAN(); 

				}
				break;
			case 49 :
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:1:271: COT
				{
				mCOT(); 

				}
				break;
			case 50 :
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:1:275: PLUS
				{
				mPLUS(); 

				}
				break;
			case 51 :
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:1:280: MINUS
				{
				mMINUS(); 

				}
				break;
			case 52 :
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:1:286: MULT
				{
				mMULT(); 

				}
				break;
			case 53 :
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:1:291: DIV
				{
				mDIV(); 

				}
				break;
			case 54 :
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:1:295: ASSIGN
				{
				mASSIGN(); 

				}
				break;
			case 55 :
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:1:302: RETURN
				{
				mRETURN(); 

				}
				break;
			case 56 :
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:1:309: INT
				{
				mINT(); 

				}
				break;
			case 57 :
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:1:313: REAL
				{
				mREAL(); 

				}
				break;
			case 58 :
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:1:318: ID
				{
				mID(); 

				}
				break;
			case 59 :
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:1:321: MALFORMED_ID
				{
				mMALFORMED_ID(); 

				}
				break;
			case 60 :
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\ECL.g:1:334: WS
				{
				mWS(); 

				}
				break;

		}
	}


	protected DFA18 dfa18 = new DFA18(this);
	protected DFA24 dfa24 = new DFA24(this);
	static final String DFA18_eotS =
		"\4\uffff\1\5\2\uffff";
	static final String DFA18_eofS =
		"\7\uffff";
	static final String DFA18_minS =
		"\1\60\1\56\1\60\1\uffff\1\60\2\uffff";
	static final String DFA18_maxS =
		"\1\71\1\105\1\71\1\uffff\1\105\2\uffff";
	static final String DFA18_acceptS =
		"\3\uffff\1\2\1\uffff\1\1\1\3";
	static final String DFA18_specialS =
		"\7\uffff}>";
	static final String[] DFA18_transitionS = {
			"\12\1",
			"\1\2\1\uffff\12\1\13\uffff\1\3",
			"\12\4",
			"",
			"\12\4\13\uffff\1\6",
			"",
			""
	};

	static final short[] DFA18_eot = DFA.unpackEncodedString(DFA18_eotS);
	static final short[] DFA18_eof = DFA.unpackEncodedString(DFA18_eofS);
	static final char[] DFA18_min = DFA.unpackEncodedStringToUnsignedChars(DFA18_minS);
	static final char[] DFA18_max = DFA.unpackEncodedStringToUnsignedChars(DFA18_maxS);
	static final short[] DFA18_accept = DFA.unpackEncodedString(DFA18_acceptS);
	static final short[] DFA18_special = DFA.unpackEncodedString(DFA18_specialS);
	static final short[][] DFA18_transition;

	static {
		int numStates = DFA18_transitionS.length;
		DFA18_transition = new short[numStates][];
		for (int i=0; i<numStates; i++) {
			DFA18_transition[i] = DFA.unpackEncodedString(DFA18_transitionS[i]);
		}
	}

	protected class DFA18 extends DFA {

		public DFA18(BaseRecognizer recognizer) {
			this.recognizer = recognizer;
			this.decisionNumber = 18;
			this.eot = DFA18_eot;
			this.eof = DFA18_eof;
			this.min = DFA18_min;
			this.max = DFA18_max;
			this.accept = DFA18_accept;
			this.special = DFA18_special;
			this.transition = DFA18_transition;
		}
		@Override
		public String getDescription() {
			return "336:1: REAL : ( ( DIGIT )+ '.' ( DIGIT )+ | ( DIGIT )+ 'E' ( '-' | '+' )? ( DIGIT )+ | ( DIGIT )+ '.' ( DIGIT )+ 'E' ( '-' | '+' )? ( DIGIT )+ );";
		}
	}

	static final String DFA24_eotS =
		"\12\uffff\1\51\1\uffff\1\55\1\64\1\67\1\64\1\uffff\1\74\1\64\1\100\1\102"+
		"\5\64\2\uffff\1\111\1\113\1\115\1\117\1\64\3\uffff\1\121\2\64\3\uffff"+
		"\3\64\1\uffff\6\64\1\uffff\2\64\1\uffff\1\140\3\64\1\uffff\3\64\1\uffff"+
		"\1\64\1\uffff\4\64\1\154\10\uffff\1\64\1\uffff\1\121\1\uffff\1\125\1\uffff"+
		"\5\64\1\164\2\64\1\167\1\64\1\uffff\2\64\1\173\3\64\1\177\4\64\1\uffff"+
		"\1\u0084\1\u0085\1\123\2\64\1\u0088\1\u0089\1\uffff\1\u008a\1\u008b\1"+
		"\uffff\1\u008c\1\u008d\1\u008e\1\uffff\1\u008f\2\64\1\uffff\4\64\2\uffff"+
		"\2\64\10\uffff\5\64\1\u009d\1\u009e\1\64\1\u00a0\2\64\1\u00a3\1\64\2\uffff"+
		"\1\64\1\uffff\1\u00a6\1\u00a7\1\uffff\2\64\2\uffff\1\u00aa\1\64\1\uffff"+
		"\1\64\1\u00ad\1\uffff";
	static final String DFA24_eofS =
		"\u00ae\uffff";
	static final String DFA24_minS =
		"\1\11\11\uffff\1\57\1\uffff\4\60\1\uffff\11\60\2\uffff\4\75\1\60\3\uffff"+
		"\1\56\2\60\3\uffff\3\60\1\uffff\6\60\1\uffff\2\60\1\uffff\4\60\1\uffff"+
		"\3\60\1\uffff\1\60\1\uffff\5\60\10\uffff\1\60\1\uffff\1\56\1\uffff\1\53"+
		"\1\uffff\12\60\1\uffff\13\60\1\uffff\7\60\1\uffff\2\60\1\uffff\3\60\1"+
		"\uffff\3\60\1\uffff\4\60\2\uffff\2\60\10\uffff\15\60\2\uffff\1\60\1\uffff"+
		"\2\60\1\uffff\2\60\2\uffff\2\60\1\uffff\2\60\1\uffff";
	static final String DFA24_maxS =
		"\1\175\11\uffff\1\57\1\uffff\4\172\1\uffff\11\172\2\uffff\4\75\1\172\3"+
		"\uffff\3\172\3\uffff\3\172\1\uffff\6\172\1\uffff\2\172\1\uffff\4\172\1"+
		"\uffff\3\172\1\uffff\1\172\1\uffff\5\172\10\uffff\1\172\1\uffff\1\172"+
		"\1\uffff\1\71\1\uffff\12\172\1\uffff\13\172\1\uffff\7\172\1\uffff\2\172"+
		"\1\uffff\3\172\1\uffff\3\172\1\uffff\4\172\2\uffff\2\172\10\uffff\15\172"+
		"\2\uffff\1\172\1\uffff\2\172\1\uffff\2\172\2\uffff\2\172\1\uffff\2\172"+
		"\1\uffff";
	static final String DFA24_acceptS =
		"\1\uffff\1\1\1\2\1\3\1\4\1\5\1\6\1\7\1\10\1\11\1\uffff\1\13\4\uffff\1"+
		"\22\11\uffff\1\43\1\44\5\uffff\1\62\1\63\1\64\3\uffff\1\74\1\12\1\65\3"+
		"\uffff\1\16\6\uffff\1\72\2\uffff\1\25\4\uffff\1\27\3\uffff\1\26\1\uffff"+
		"\1\30\5\uffff\1\47\1\45\1\46\1\66\1\50\1\52\1\51\1\53\1\uffff\1\70\1\uffff"+
		"\1\71\1\uffff\1\73\12\uffff\1\21\13\uffff\1\41\7\uffff\1\55\2\uffff\1"+
		"\20\3\uffff\1\60\3\uffff\1\54\4\uffff\1\56\1\61\2\uffff\1\42\1\15\1\17"+
		"\1\36\1\57\1\23\1\37\1\24\15\uffff\1\40\1\14\1\uffff\1\67\2\uffff\1\34"+
		"\2\uffff\1\31\1\32\2\uffff\1\35\2\uffff\1\33";
	static final String DFA24_specialS =
		"\u00ae\uffff}>";
	static final String[] DFA24_transitionS = {
			"\2\47\2\uffff\1\47\22\uffff\1\47\1\34\1\13\3\uffff\1\33\1\13\1\1\1\2"+
			"\1\43\1\41\1\3\1\42\1\4\1\12\12\44\1\uffff\1\5\1\37\1\35\1\36\2\uffff"+
			"\1\16\1\27\1\40\1\45\1\14\1\30\2\45\1\31\2\45\1\17\1\45\1\26\1\45\1\23"+
			"\1\45\1\22\1\15\1\21\1\25\1\24\4\45\1\6\1\uffff\1\7\1\20\1\46\1\uffff"+
			"\32\45\1\10\1\32\1\11",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"\1\50",
			"",
			"\12\57\7\uffff\13\56\1\54\1\56\1\52\11\56\1\53\2\56\4\uffff\1\60\1\uffff"+
			"\32\56",
			"\12\57\7\uffff\4\56\1\63\3\56\1\61\7\56\1\62\11\56\4\uffff\1\60\1\uffff"+
			"\32\56",
			"\12\57\7\uffff\1\56\1\65\21\56\1\66\6\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\15\56\1\70\14\56\4\uffff\1\60\1\uffff\32\56",
			"",
			"\12\57\7\uffff\1\73\7\56\1\71\10\56\1\72\10\56\4\uffff\1\60\1\uffff"+
			"\32\56",
			"\12\57\7\uffff\4\56\1\76\11\56\1\75\13\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\16\56\1\77\13\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\1\101\31\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\15\56\1\103\14\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\16\56\1\104\13\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\10\56\1\105\21\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\1\106\31\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\5\56\1\107\24\56\4\uffff\1\60\1\uffff\32\56",
			"",
			"",
			"\1\110",
			"\1\112",
			"\1\114",
			"\1\116",
			"\12\57\7\uffff\16\56\1\120\13\56\4\uffff\1\60\1\uffff\32\56",
			"",
			"",
			"",
			"\1\123\1\uffff\12\122\7\uffff\4\125\1\124\25\125\4\uffff\1\125\1\uffff"+
			"\32\125",
			"\12\57\7\uffff\32\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\32\126\4\uffff\1\60\1\uffff\32\126",
			"",
			"",
			"",
			"\12\57\7\uffff\23\56\1\127\6\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\17\56\1\130\12\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\22\56\1\131\7\56\4\uffff\1\60\1\uffff\32\56",
			"",
			"\12\57\7\uffff\32\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\32\126\4\uffff\1\60\1\uffff\32\126",
			"\12\57\7\uffff\32\126\4\uffff\1\60\1\uffff\32\126",
			"\12\57\7\uffff\6\56\1\132\6\56\1\133\14\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\21\56\1\134\10\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\4\56\1\135\25\56\4\uffff\1\60\1\uffff\32\56",
			"",
			"\12\57\7\uffff\22\56\1\136\7\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\1\137\31\56\4\uffff\1\60\1\uffff\32\56",
			"",
			"\12\57\7\uffff\32\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\14\56\1\141\15\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\24\56\1\142\5\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\15\56\1\143\14\56\4\uffff\1\60\1\uffff\32\56",
			"",
			"\12\57\7\uffff\16\56\1\144\13\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\23\56\1\145\6\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\10\56\1\146\21\56\4\uffff\1\60\1\uffff\32\56",
			"",
			"\12\57\7\uffff\21\56\1\147\10\56\4\uffff\1\60\1\uffff\32\56",
			"",
			"\12\57\7\uffff\10\56\1\150\21\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\21\56\1\151\10\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\15\56\1\152\14\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\13\56\1\153\16\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\32\56\4\uffff\1\60\1\uffff\32\56",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"\12\57\7\uffff\22\56\1\155\1\156\6\56\4\uffff\1\60\1\uffff\32\56",
			"",
			"\1\123\1\uffff\12\122\7\uffff\4\125\1\124\25\125\4\uffff\1\125\1\uffff"+
			"\32\125",
			"",
			"\1\123\1\uffff\1\123\2\uffff\12\157",
			"",
			"\12\57\7\uffff\32\126\4\uffff\1\60\1\uffff\32\126",
			"\12\57\7\uffff\10\56\1\160\21\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\16\56\1\161\13\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\4\56\1\162\25\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\15\56\1\163\14\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\32\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\23\56\1\165\6\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\3\56\1\166\26\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\32\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\15\56\1\170\14\56\4\uffff\1\60\1\uffff\32\56",
			"",
			"\12\57\7\uffff\4\56\1\171\25\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\4\56\1\172\25\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\32\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\23\56\1\174\6\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\24\56\1\175\5\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\22\56\1\176\7\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\32\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\5\56\1\u0080\24\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\14\56\1\u0081\15\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\16\56\1\u0082\13\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\22\56\1\u0083\7\56\4\uffff\1\60\1\uffff\32\56",
			"",
			"\12\57\7\uffff\32\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\32\56\4\uffff\1\60\1\uffff\32\56",
			"\12\157\7\uffff\32\125\4\uffff\1\125\1\uffff\32\125",
			"\12\57\7\uffff\4\56\1\u0086\25\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\15\56\1\u0087\14\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\32\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\32\56\4\uffff\1\60\1\uffff\32\56",
			"",
			"\12\57\7\uffff\32\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\32\56\4\uffff\1\60\1\uffff\32\56",
			"",
			"\12\57\7\uffff\32\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\32\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\32\56\4\uffff\1\60\1\uffff\32\56",
			"",
			"\12\57\7\uffff\32\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\21\56\1\u0090\10\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\22\56\1\u0091\7\56\4\uffff\1\60\1\uffff\32\56",
			"",
			"\12\57\7\uffff\16\56\1\u0092\13\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\1\u0093\31\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\14\56\1\u0094\15\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\4\56\1\u0095\25\56\4\uffff\1\60\1\uffff\32\56",
			"",
			"",
			"\12\57\7\uffff\21\56\1\u0096\10\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\4\56\1\u0097\25\56\4\uffff\1\60\1\uffff\32\56",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"\12\57\7\uffff\15\56\1\u0098\14\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\16\56\1\u0099\13\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\21\56\1\u009a\10\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\13\56\1\u009b\16\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\10\56\1\u009c\21\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\32\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\32\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\15\56\1\u009f\14\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\32\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\15\56\1\u00a1\14\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\14\56\1\u00a2\15\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\32\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\1\u00a4\31\56\4\uffff\1\60\1\uffff\32\56",
			"",
			"",
			"\12\57\7\uffff\23\56\1\u00a5\6\56\4\uffff\1\60\1\uffff\32\56",
			"",
			"\12\57\7\uffff\32\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\32\56\4\uffff\1\60\1\uffff\32\56",
			"",
			"\12\57\7\uffff\13\56\1\u00a8\16\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\10\56\1\u00a9\21\56\4\uffff\1\60\1\uffff\32\56",
			"",
			"",
			"\12\57\7\uffff\32\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\1\u00ab\31\56\4\uffff\1\60\1\uffff\32\56",
			"",
			"\12\57\7\uffff\13\56\1\u00ac\16\56\4\uffff\1\60\1\uffff\32\56",
			"\12\57\7\uffff\32\56\4\uffff\1\60\1\uffff\32\56",
			""
	};

	static final short[] DFA24_eot = DFA.unpackEncodedString(DFA24_eotS);
	static final short[] DFA24_eof = DFA.unpackEncodedString(DFA24_eofS);
	static final char[] DFA24_min = DFA.unpackEncodedStringToUnsignedChars(DFA24_minS);
	static final char[] DFA24_max = DFA.unpackEncodedStringToUnsignedChars(DFA24_maxS);
	static final short[] DFA24_accept = DFA.unpackEncodedString(DFA24_acceptS);
	static final short[] DFA24_special = DFA.unpackEncodedString(DFA24_specialS);
	static final short[][] DFA24_transition;

	static {
		int numStates = DFA24_transitionS.length;
		DFA24_transition = new short[numStates][];
		for (int i=0; i<numStates; i++) {
			DFA24_transition[i] = DFA.unpackEncodedString(DFA24_transitionS[i]);
		}
	}

	protected class DFA24 extends DFA {

		public DFA24(BaseRecognizer recognizer) {
			this.recognizer = recognizer;
			this.decisionNumber = 24;
			this.eot = DFA24_eot;
			this.eof = DFA24_eof;
			this.min = DFA24_min;
			this.max = DFA24_max;
			this.accept = DFA24_accept;
			this.special = DFA24_special;
			this.transition = DFA24_transition;
		}
		@Override
		public String getDescription() {
			return "1:1: Tokens : ( T__68 | T__69 | T__70 | T__71 | T__72 | T__73 | T__74 | T__75 | T__76 | COMMENT | STR | ENTIER | SIGN | EXP | SQRT | ABS | LN | POW | TIME | ROOT_AGG | AGGREGATE | PLACE | MARK | SYS_VAR | POISSON | UNIFORM | EXPONENTIAL | NORMAL | BINOMIAL | SEED | TRUE | FALSE | IF | ELSE | LOR | LAND | NOT | EQ | NE | GE | LE | GT | LT | VAR | SIN | COS | ATAN | TAN | COT | PLUS | MINUS | MULT | DIV | ASSIGN | RETURN | INT | REAL | ID | MALFORMED_ID | WS );";
		}
	}

}
