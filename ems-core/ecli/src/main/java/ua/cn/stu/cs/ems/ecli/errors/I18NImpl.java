package ua.cn.stu.cs.ems.ecli.errors;

import java.io.UnsupportedEncodingException;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * @author n0weak
 */
class I18NImpl {

    private static final String MSG_FILE = "messages";
    private static final Locale DEFAULT_LOCALE = Locale.US;
    private ResourceBundle resourceBundle;

    public I18NImpl() {
        this.resourceBundle = loadBundle(MSG_FILE, Locale.getDefault());
    }

    public I18NImpl(Locale locale) {
        this.resourceBundle = loadBundle(MSG_FILE, locale);
    }

    public String getLocalizedMessage(String key, Object... formatArgs) {
        try {
            return String.format(new String(resourceBundle.getString(key).getBytes("ISO8859-1")),
                    formatArgs);
        } catch (UnsupportedEncodingException e) {
            throw new AssertionError("\"ISO8859-1\" encoding must be supported!");
        }
    }

    private ResourceBundle loadBundle(String baseName, Locale locale) {
        ResourceBundle bundle;
        try {
            bundle = ResourceBundle.getBundle(baseName, locale);
        } catch (MissingResourceException e) {
            bundle = ResourceBundle.getBundle(baseName, DEFAULT_LOCALE);
        }
        return bundle;
    }

}
