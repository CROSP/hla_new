package ua.cn.stu.cs.ems.ecli;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static ua.cn.stu.cs.ems.ecli.ValType.getCommonType;

/**
 * Provides methods for all evaluation operations that are supported in ECL
 * by {@link Value} itself.
 *
 * @author n0weak
 */
public class Calculator {

    private static final int SCALE =  15;

    /**
     * Adds two values. If one of the values is of {@link ValType#STRING} type then concatenation is performed.
     *
     * @param a value to be added
     * @param b value to be added
     * @return result of a and b addition
     */
    public static Value add(Value a, Value b) {
        Value result = Value.UNDEFINED;
        if (null != a && null != b) {
            if (ValType.STRING == a.getType() || ValType.STRING == b.getType()) {
                result = concat(a, b);
            } else {
                ValType commonType = getCommonType(a, b);
                switch (commonType) {
                    case INTEGER:
                        result = new Value(a.longValue() + b.longValue());
                        break;
                    case REAL:
                        result = new Value(a.realValue().add(b.realValue()));
                        break;
                }
            }
        }
        return result;
    }

    public static Value sub(Value a, Value b) {
        Object result = Value.UNDEF_VAL;
        if (null != a && null != b) {
            ValType commonType = getCommonType(a, b);
            switch (commonType) {
                case INTEGER:
                    result = a.longValue() - b.longValue();
                    break;
                case REAL:
                    result = a.realValue().subtract(b.realValue());
                    break;
            }
        }
        return new Value(result);
    }

    public static Value multiply(Value a, Value b) {
        Object result = Value.UNDEF_VAL;
        if (null != a && null != b) {
            ValType commonType = getCommonType(a, b);
            switch (commonType) {
                case INTEGER:
                    result = a.longValue() * b.longValue();
                    break;
                case REAL:
                    result = a.realValue().multiply(b.realValue());
                    break;
            }
        }
        return new Value(result);
    }

    public static Value divide(Value a, Value b) {
        Object result = Value.UNDEF_VAL;
        if (null != a && null != b) {
            ValType commonType = getCommonType(a, b);
            switch (commonType) {
                case INTEGER:
                    if (b.longValue() != 0) {
                        result = a.longValue() / (double) b.longValue();
                    }
                    break;
                case REAL:
                    if (b.realValue().compareTo(BigDecimal.ZERO) != 0) {
                        result = a.realValue().divide(b.realValue(), SCALE, RoundingMode.HALF_UP);
                    }
                    break;
            }
        }
        return new Value(result);
    }

    /**
     * @param a value to be concatenated
     * @param b value to be concatenated
     * @return concatenation of string representations of a and b. For {@link Value#UNDEFINED} values "UNDEFINED" representation will be used
     * @throws NullPointerException if any of the passed values is null
     */
    public static Value concat(Value a, Value b) {
        return new Value(a.toString() + b.toString(), ValType.STRING);
    }

    /**
     * @param val value to calculate sinus for, must be castable to double
     * @return sinus for the value or {@link Value#UNDEFINED} if it is null or cannot be casted to double
     */
    public static Value sin(Value val) {
        Double doubleVal;
        if (null != val && ((doubleVal = val.doubleValue()) != null)) {
            return new Value(Math.sin(doubleVal));
        } else {
            return Value.UNDEFINED;
        }
    }

    /**
     * @param val value to calculate cosinus for, must be castable to double
     * @return cosinus for the value or {@link Value#UNDEFINED} if it is null or cannot be casted to double
     */
    public static Value cos(Value val) {
        Double doubleVal;
        if (null != val && ((doubleVal = val.doubleValue()) != null)) {
            return new Value(Math.cos(doubleVal));
        } else {
            return Value.UNDEFINED;
        }
    }

    /**
     * @param val value to calculate tangent for, must be castable to double
     * @return tangent for the value or {@link Value#UNDEFINED} if it is null or cannot be casted to double
     */
    public static Value tan(Value val) {
        Double doubleVal;
        if (null != val && ((doubleVal = val.doubleValue()) != null)) {
            return new Value(Math.tan(doubleVal));
        } else {
            return Value.UNDEFINED;
        }
    }

    /**
     * @param val value to calculate arc tangent for, must be castable to double
     * @return {@link ValType#REAL} arc tangent for the value or {@link Value#UNDEFINED} if it is null or cannot be casted to double
     */
    public static Value atan(Value val) {
        Double doubleVal;
        if (null != val && ((doubleVal = val.doubleValue()) != null)) {
            return new Value(Math.atan(doubleVal));
        } else {
            return Value.UNDEFINED;
        }
    }


    /**
     * @param val value to calculate tangent for, must be castable to double
     * @return tangent for the value or {@link Value#UNDEFINED} if it is null or cannot be casted to double
     */
    public static Value cot(Value val) {
        Double doubleVal;
        if (null != val && ((doubleVal = val.doubleValue()) != null)) {
            return new Value(1 / Math.tan(doubleVal));
        } else {
            return Value.UNDEFINED;
        }
    }

    /**
     * Defines whether values of a and b are equal
     *
     * @param a first value to be compared
     * @param b second value to be compared
     * @return {@link Value#TRUE} if the value of a is equal to the value of b and {@link Value#FALSE} if not
     */
    public static Value eq(Value a, Value b) {
        if (null == a || null == b) {
            return new Value(a == b);
        } else {
            Integer compar = a.compareTo(b);
            return compar == null ? Value.UNDEFINED :
                    (compar == 0 ? Value.TRUE : Value.FALSE);
        }
    }

    /**
     * Defines whether values of a and be are not equal
     *
     * @param a first value to be compared
     * @param b second value to be compared
     * @return {@link Value#TRUE} if the value of a is not equal to the value b and {@link Value#FALSE} if not
     */
    public static Value ne(Value a, Value b) {
        if (null == a || null == b) {
            return new Value(a != b);
        } else {
            Integer compar = a.compareTo(b);
            return compar == null ? Value.UNDEFINED :
                    (compar != 0 ? Value.TRUE : Value.FALSE);
        }
    }

    /**
     * Defines whether a is lower then b
     *
     * @param a first value to be compared
     * @param b second value to be compared
     * @return {@link Value#TRUE} if a is lower then b and {@link Value#FALSE} if not,
     *         {@link Value#UNDEFINED} if values cannot be compared, see {@link Value#compareTo(Value)} for details
     */
    public static Value lt(Value a, Value b) {
        Integer compar = a.compareTo(b);
        if (null == compar) return Value.UNDEFINED;
        return compar < 0 ? Value.TRUE : Value.FALSE;
    }

    /**
     * Defines whether a is greater then b
     *
     * @param a first value to be compared
     * @param b second value to be compared
     * @return {@link Value#TRUE} if a is greater then b and {@link Value#FALSE} if not,
     *         {@link Value#UNDEFINED} if values cannot be compared, see {@link Value#compareTo(Value)} for details
     */
    public static Value gt(Value a, Value b) {
        Integer compar = a.compareTo(b);
        if (null == compar) return Value.UNDEFINED;
        return compar > 0 ? Value.TRUE : Value.FALSE;
    }

    /**
     * Defines whether a is greater or equal to b
     *
     * @param a first value to be compared
     * @param b second value to be compared
     * @return {@link Value#TRUE} if a is greater or equal then b and {@link Value#FALSE} if not,
     *         {@link Value#UNDEFINED} if values cannot be compared, see {@link Value#compareTo(Value)} for details
     */
    public static Value ge(Value a, Value b) {
        Integer compar = a.compareTo(b);
        if (null == compar) return Value.UNDEFINED;
        return compar >= 0 ? Value.TRUE : Value.FALSE;
    }

    /**
     * Defines whether a is lower or equal to b
     *
     * @param a first value to be compared
     * @param b second value to be compared
     * @return {@link Value#TRUE} if a is lower or equal then b and {@link Value#FALSE} if not,
     *         {@link Value#UNDEFINED} if values cannot be compared, see {@link Value#compareTo(Value)} for details
     */
    public static Value le(Value a, Value b) {
        Integer compar = a.compareTo(b);
        if (null == compar) return Value.UNDEFINED;
        return compar <= 0 ? Value.TRUE : Value.FALSE;
    }

    /**
     * Returns inversion of the specified value. Exact behaviour is type-dependent.
     *
     * @param val value whose inversion must be obtained
     * @return inversion of the specified value. If the value is of {@link ValType#INTEGER} or {@link ValType#REAL} type then
     *         negated value is returned. If it is of {@link ValType#BOOL} type then inverted boolean value is returned
     *         (true becomes false and false becomes true). Otherwise no inversion is performed and a copy of the value is returned.
     */
    public static Value invert(Value val) {
        switch (val.getType()) {
            case INTEGER:
                return new Value(-1 * val.longValue());
            case REAL:
                return new Value(val.realValue().negate());
            case BOOL:
                return new Value(!val.boolValue());
            default:
                return val.isTypePredefined() ? new Value(val.getValue(), val.getType()) : new Value(val.getValue());
        }
    }

    /**
     * Applies logical and to the specified values. Applicable only for values of {@link ValType#BOOL} type.
     *
     * @param a first operand
     * @param b second operand
     * @return a && b. If any of the values cannot be casted to {@link ValType#BOOL} then {@link Value#UNDEFINED} is returned.
     */
    public static Value and(Value a, Value b) {
        switch (getCommonType(a, b)) {
            case BOOL:
                return new Value(a.boolValue() && b.boolValue());
            default:
                return Value.UNDEFINED;
        }
    }

    /**
     * Applies logical or to the specified values. Applicable only for values of {@link ValType#BOOL} type.
     *
     * @param a first operand
     * @param b second operand
     * @return a || b. If any of the values cannot be casted to {@link ValType#BOOL} then {@link Value#UNDEFINED} is returned.
     */
    public static Value or(Value a, Value b) {
        switch (getCommonType(a, b)) {
            case BOOL:
                return new Value(a.boolValue() || b.boolValue());
            default:
                return Value.UNDEFINED;
        }
    }

    /**
     * Calculates a^b. Both a and b must be convertable to {@link Double}. Uses {@link Math#pow(double, double)} to perform calculations.
     *
     * @param a the base.
     * @param b the exponent.
     * @return result of a^b, {@link ValType#REAL} type. If a or b is not convertable to {@link Double} then {@link Value#UNDEFINED} is returned.
     * @see Math#pow(double, double)
     */
    public static Value pow(Value a, Value b) {
        Object result = Value.UNDEF_VAL;
        if (null != a && null != b) {
            Double dA = a.doubleValue();
            Double dB = b.doubleValue();
            if (null != dA && null != dB) {
                result = Math.pow(dA, dB);
            }
        }
        return new Value(result);
    }

    /**
     * Returns natural logarithm of a. Uses {@link Math#log(double)} to perform calculations.
     *
     * @param a the base.
     * @return natural logarithm of a, {@link ValType#REAL} type. If a is not convertable to {@link Double} or a <= 0 then {@link Value#UNDEFINED} is returned.
     * @see Math#log(double)
     */
    public static Value ln(Value a) {
        Object result = Value.UNDEF_VAL;
        Double dA;
        if (null != a && null != (dA = a.doubleValue())) {
            result = Math.log(dA);
        }
        return new Value(result);
    }

    /**
     * Returns the absolute value of a. Argument must be of {@link ValType#INTEGER} or {@link ValType#REAL} type
     *
     * @param a argument which absolute value must be obtained.
     * @return absolute value of the argument, or {@link Value#UNDEFINED} if a is not  of {@link ValType#INTEGER} or {@link ValType#REAL}  type
     */
    public static Value abs(Value a) {
        Object result = Value.UNDEF_VAL;
        if (null != a) {
            switch (a.getType()) {
                case INTEGER:
                    result = Math.abs(a.longValue());
                    break;
                case REAL:
                    result = a.realValue().abs();
                    break;
            }
        }
        return new Value(result);
    }

    /**
     * Returns square root of the argument. Uses {@link Math#sqrt(double)} to perform calculations.
     *
     * @param a the value.
     * @return square root of the argument, {@link ValType#REAL} type. If a is not convertable to {@link Double} or a < 0 then {@link Value#UNDEFINED} is returned.
     * @see Math#sqrt(double)
     */
    public static Value sqrt(Value a) {
        Object result = Value.UNDEF_VAL;
        Double dA;
        if (null != a && null != (dA = a.doubleValue())) {
            result = Math.sqrt(dA);
        }
        return new Value(result);
    }

    /**
     * Returns the signum function  of a. Argument must be of {@link ValType#INTEGER} or {@link ValType#REAL} type
     *
     * @param a argument.
     * @return -1, 0, or 1 as the argument is negative, zero, or positive.
     *         {@link Value#UNDEFINED} if a is not  of {@link ValType#INTEGER} or {@link ValType#REAL}  type
     * @see java.math.BigDecimal#signum()
     */
    public static Value sign(Value a) {
        Object result = Value.UNDEF_VAL;
        if (null != a) {
            switch (a.getType()) {
                case INTEGER:
                case REAL:
                    result = a.realValue().signum();
                    break;
            }
        }
        return new Value(result);
    }

    /**
     * Returns maximum {@link ValType#INTEGER} value that is lower or equal to the argument. Argument must be of {@link ValType#INTEGER} or {@link ValType#REAL} type
     *
     * @param a argument.
     * @return maximum {@link ValType#INTEGER} value that is lower or equal to the argument.
     *         {@link Value#UNDEFINED} if argument is not  of {@link ValType#INTEGER} or {@link ValType#REAL}  type
     */
    public static Value entier(Value a) {
        Object result = Value.UNDEF_VAL;
        if (null != a) {
            switch (a.getType()) {
                case INTEGER:
                    result = a.longValue();
                    break;
                case REAL:
                    BigDecimal realValue = a.realValue();
                    if (realValue.signum() >= 0) {
                        result = a.realValue().setScale(0, RoundingMode.DOWN);
                    } else {
                        result = a.realValue().setScale(0, RoundingMode.UP);
                    }
                    break;
            }
        }
        return new Value(result);
    }
}
