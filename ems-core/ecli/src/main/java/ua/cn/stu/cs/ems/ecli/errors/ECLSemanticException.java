package ua.cn.stu.cs.ems.ecli.errors;

import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.tree.CommonTree;

/**
 * Used to indicate syntactically valid, but nonsensical or otherwise bogus input.
 * Subclasses {@link RecognitionException} for handling convenience.
 *
 * @author n0weak
 */
public class ECLSemanticException extends RecognitionException implements InformativeException {

    /**
     * Same as {@link #getEndLine()}
     */
    public Integer endLine;

    /**
     * Same as {@link #getEndPos()}
     */
    public Integer endPos;

    private ErrorCode errorCode;
    private String[] errArgs;
    private boolean isFullyInitialized;
    private String subCode;

    /**
     * @param errorCode error code that specifies exact type of error
     * @param errArgs   error-specific arguments (e.g. variable name when  redefined variable error happens)
     */
    public ECLSemanticException(ErrorCode errorCode, String... errArgs) {
        this.errorCode = errorCode;
        this.errArgs = errArgs;
    }

    /**
     * @param subCode   code which defines exact message for current error. <code> fullCode = errDefaultCode + (subCode != null ? "." + subCode : "")  </code
     * @param errorCode error code that specifies exact type of error
     * @param errArgs   error-specific arguments (e.g. variable name when  redefined variable error happens)
     */
    public ECLSemanticException(String subCode, ErrorCode errorCode, String... errArgs) {
        this.subCode = subCode;
        this.errorCode = errorCode;
        this.errArgs = errArgs;
    }

    /**
     * @param errorCode error code that specifies exact type of error
     * @param startLine line number of the first token in the statement that caused that error
     * @param startPos  position number of the first token in the statement that caused that error
     * @param endLine   line number of the last token in the statement that caused that error
     * @param endPos    position number of the last token in the statement that caused that error
     * @param errArgs   error-specific arguments (e.g. variable name when  redefined variable error happens)
     */
    public ECLSemanticException(ErrorCode errorCode, int startLine, int startPos, int endLine, int endPos, String... errArgs) {
        this.errorCode = errorCode;
        this.line = startLine;
        this.charPositionInLine = startPos;
        this.endLine = endLine;
        this.endPos = endPos;
        this.errArgs = errArgs;
    }

    /**
     * @param errorCode error code that specifies exact type of error
     * @param startTree tree that represents first token in the statement that caused that error
     * @param endTree   tree that represents last token in the statement that caused that error
     * @param errArgs   error-specific arguments (e.g. variable name when  redefined variable error happens)
     */
    public ECLSemanticException(ErrorCode errorCode, CommonTree startTree, CommonTree endTree, String... errArgs) {
        this.errArgs = errArgs;
        this.errorCode = errorCode;
        this.line = startTree.getLine();
        this.charPositionInLine = startTree.getCharPositionInLine();
        this.endLine = endTree.getLine();
        this.endPos = endTree.getCharPositionInLine() +
                (endTree.getText() != null ? endTree.getText().length() - 1 : 0);
    }

    /**
     * @return line number of the first token in the statement that caused that error. Same as {@link #line}
     */
    public int getStartLine() {
        return this.line;
    }

    /**
     * @return line number of the last token in the statement that caused that error. Same as {@link #endLine}
     */
    public int getEndLine() {
        return this.endLine;
    }

    /**
     * @return position number of the first token in the statement that caused that error. Same as {@link #charPositionInLine}
     */
    public int getStartPos() {
        return this.charPositionInLine;
    }

    /**
     * @return position number of the last token in the statement that caused that error. Same as {@link #endPos}
     */
    public int getEndPos() {
        return this.endPos;
    }

    /**
     * @return error code that specifies exact type of error
     */
    public ErrorCode getErrorCode() {
        return errorCode;
    }

    /**
     * @return error-specific arguments (e.g. variable name when  redefined variable error happens)
     */
    public Object[] getErrArgs() {
        return errArgs;
    }

    /**
     * @return true if the exception is fully initialized and needs no more processing, false if it is not and some of its fields
     *         are currently in an invalid state. Default is false;
     */
    public boolean isFullyInitialized() {
        return isFullyInitialized;
    }

    /**
     * @param fullyInitialized true if the exception is fully initialized and needs no more processing, false if it is not and some of its fields
     *                         are currently in an invalid state.
     */
    public void setFullyInitialized(boolean fullyInitialized) {
        isFullyInitialized = fullyInitialized;
    }

    /**
     * @return code which defines exact message for current error. <code> fullCode = errDefaultCode + (subCode != null ? "." + subCode : "")  </code
     */
    public String getSubCode() {
        return subCode;
    }
}
