package ua.cn.stu.cs.ems.ecli.errors;

import org.antlr.runtime.BaseRecognizer;
import org.antlr.runtime.RecognitionException;

/**
 * Implementors of this interface can be used to create context-specific error messages
 * from generic {@link org.antlr.runtime.RecognitionException} error.  
 * 
 * @author n0weak
 */
interface ErrorMessageCreator {

    /**
     * Generates specific to the current context error message from provided recognition exception
     *
     * @param e exception that represents generic parsing failure
     * @param recognizer recognizer that found error, used to obtain some useful data, e.g. names of the tokens
     * @return specific to the current context error message that is generated from provided recognition exception
     */
    public String getErrorMessage(RecognitionException e, BaseRecognizer recognizer);
    
}
