package ua.cn.stu.cs.ems.ecli;

import java.math.BigDecimal;
import java.util.*;

/**
 * Enum that represents types of the values that are supported by ECL.
 *
 * @author n0weak
 */
public enum ValType {

    /**
     * Uses {@link String} as a backend. Hence all in values except null and
     * {@link Value#UNDEF_VAL}
     * are considered to be of this type.
     */
    STRING(-4, new StringCaster()),

    /**
     * Represents null and {@link Value#UNDEF_VAL} values. Can be casted to 
     * {@link #STRING}.
     */
    UNDEFINED(-99, new UndefCaster(), STRING),

    /**
     * Uses {@link Boolean} as a backend. Used only as an inner type, user is
     * not able to define bool values himself.
     * Only {@link Boolean} instances are considered to be of this type.
     * Can be casted to {@link #STRING}.
     */
    BOOL(-3, new BoolCaster(), STRING),

    /**
     * Uses {@link java.math.BigDecimal} as a backend. Hence all the numeric
     * values with exponent value from {@link Integer#MIN_VALUE} to
     * {@link Integer#MAX_VALUE} are considered to be of this type
     * (well, it's hard to imagine ECL's user that needs such a value, so it
     * is fully acceptable to think that any numeric is REAL).
     * Can be casted to {@link #STRING}.
     */
    REAL(-1, new RealCaster(), STRING),

    /**
     * Uses {@link Long} as a backend. Hence all in values from
     * {@link Long#MIN_VALUE} to {@link Long#MAX_VALUE}
     * are considered to be of this type. Can be casted to
     * {@link #REAL}, {@link #STRING}.
     */
    INTEGER(0, new IntCaster(), REAL, STRING);


    /**
     * List of the types, sorted by precedence in ascending order
     */
    private static List<ValType> sortedTypes;

    /**
     * Comparator that is used to sort types by precedence in ascending order
     */
    private static Comparator<ValType> comparator = new Comparator<ValType>() {
        public int compare(ValType o1, ValType o2) {
            int rez;
            if (o1.precedence == o2.precedence) {
                rez = 0;
            } else if (o1.precedence < o2.precedence) {
                rez = 1;
            } else {
                rez = -1;
            }
            return rez;
        }
    };

    static {
        sortedTypes = Arrays.asList(ValType.values());
        Collections.sort(sortedTypes, comparator);
    }

    private int precedence;
    private Caster caster;
    private List<ValType> parentTypes;

    /**
     * @param precedence  precedence of the type. Types with lower precedence 
     *                    are considered to be more common, hence during the 
     *                    typing high-precedence values must be checked before
     *                    low-precedence ones.
     * @param caster      implementation of the {@link Caster} interface that
     *                    will be used for type castings
     * @param parentTypes types that can consume the current one, in other
     *                    words current type can be converted into any of those.
     *                    E.g. integer 10 can be interpreted as real 10.0 or string "10"
     */
    ValType(int precedence, Caster caster, ValType... parentTypes) {
        this.precedence = precedence;
        this.caster = caster;
        this.parentTypes = Arrays.asList(parentTypes);
    }

    static ValType getType(Object obj) {
        for (ValType type : sortedTypes) {
            if (type.isCastable(obj)) {
                return type;
            }
        }
        return null;
    }

    static ValType getCommonType(Value a, Value b) {
        ValType aType = a.getType();
        ValType bType = b.getType();
        if (aType == bType) {
            return aType;
        }
        if (aType.parentTypes.contains(bType)) {
            return bType;
        }
        if (bType.parentTypes.contains(aType)) {
            return aType;
        }
        List<ValType> commonParentTypes = new ArrayList<ValType>(aType.parentTypes);
        commonParentTypes.retainAll(bType.parentTypes);
        if (!commonParentTypes.isEmpty()) {
            Collections.sort(commonParentTypes, comparator);
            return commonParentTypes.get(0);
        }
        return UNDEFINED;
    }

    /**
     * Method that determines whether the object can be casted to the current
     * type.
     *
     * @param obj object to be casted
     * @return true if object can be casted or false of not
     */
    boolean isCastable(Object obj) {
        return caster.isCastable(obj);
    }

    /**
     * Method that casts passed object to the  type that is represented by the
     * caster.
     *
     * @param obj object to be casted
     * @return casted value of the passed object or null if object cannot be
     *         casted. The value must be manually typed to the proper java 
     *         entity.
     */
    Object cast(Object obj) {
        return caster.cast(obj);
    }

    private static class IntCaster implements Caster {
        public boolean isCastable(Object obj) {
            return obj != null && cast(obj) != null;
        }

        public Object cast(Object obj) {
            Long l;
            try {
                l = Long.parseLong(obj.toString().replaceAll("\\.[0]+$",""));
            } catch (Exception e) {
                l = null;
            }
            return l;
        }
    }

    private static class RealCaster implements Caster {
        public boolean isCastable(Object obj) {
            return obj != null && cast(obj) != null;
        }

        public Object cast(Object obj) {
            try {
                return new BigDecimal(obj.toString());
            } catch (Exception e) {
                return null;
            }
        }
    }

    private static class StringCaster implements Caster {
        public boolean isCastable(Object obj) {
            return obj != null 
            		&& !(obj instanceof Double 
            				&& (((Double) obj).isInfinite() 
            					|| ((Double) obj).isNaN())) 
            		&& obj != Value.UNDEF_VAL;
        }

        public Object cast(Object obj) {
            if (null != obj && obj instanceof Boolean) {
                return obj.toString().toUpperCase();
            } else {
                return String.valueOf(obj);
            }
        }
    }

    private static class BoolCaster implements Caster {
        public boolean isCastable(Object obj) {
            return obj != null && cast(obj) != null;
        }

        public Object cast(Object obj) {
            return (obj instanceof Boolean) ? obj : null;
        }
    }

    private static class UndefCaster implements Caster {
        public boolean isCastable(Object obj) {
            return obj == null || obj.equals(Value.UNDEF_VAL) || 
            		(obj instanceof Double && (((Double) obj).isInfinite() || ((Double) obj).isNaN()));
        }

        public Object cast(Object obj) {
            return isCastable(obj) ? Value.UNDEF_VAL : null;
        }


    }
}
