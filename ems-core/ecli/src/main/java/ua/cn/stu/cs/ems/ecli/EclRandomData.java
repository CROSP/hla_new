package ua.cn.stu.cs.ems.ecli;

import org.apache.commons.math.random.RandomDataImpl;

/**
 * @author n0weak
 */
public class EclRandomData {

    private RandomDataImpl randomData = new RandomDataImpl();

    /**
     * Generates a random value from the Poisson distribution with the given mean
     *
     * @param mean mean of the Poisson distribution, must be positive and convertable to {@link Double} (otherwise {@link Value#UNDEFINED} is returned)
     * @return the random Poisson {@link ValType#INTEGER} value or {@link Value#UNDEFINED} if the specified mean is not positive or  convertable to {@link Double}.
     */
    public Value nextPoisson(Value mean) {
        try {
            return new Value(randomData.nextPoisson(mean.doubleValue()));
        } catch (Exception e) {
            return Value.UNDEFINED;
        }
    }

    /**
     * Generates a uniformly distributed random value from the open interval (lower,upper) (i.e., endpoints excluded).
     *
     * @param lower the lower bound, must be convertable to {@link Double}.
     * @param upper the upper bound, must be convertable to {@link Double}.
     * @return a uniformly distributed random {@link ValType#REAL} value from the interval (lower, upper) or
     *         {@link Value#UNDEFINED} if lower >= upper or any of supplied values is not convertable to {@link Double}.
     */
    public Value nextUniform(Value lower, Value upper) {
        try {
            return new Value(randomData.nextUniform(lower.doubleValue(), upper.doubleValue()));
        } catch (Exception e) {
            return Value.UNDEFINED;
        }
    }

    /**
     * Returns a random value from an Exponential distribution with the given mean.
     *
     * @param mean the mean of the distribution , must be convertable to {@link Double}.
     * @return the random Exponential {@link ValType#REAL} value
     *         {@link Value#UNDEFINED} if supplied value is not convertable to {@link Double}.
     */
    public Value nextExponential(Value mean) {
        try {
            return new Value(randomData.nextExponential(mean.doubleValue()));
        } catch (Exception e) {
            return Value.UNDEFINED;
        }
    }

    /**
     * Generate a random value from a Normal (a.k.a. Gaussian) distribution with the given mean, mu and the given standard deviation, sigma. uded).
     *
     * @param mu    the mean of the distribution, must be convertable to {@link Double}.
     * @param sigma the standard deviation of the distribution, must be convertable to {@link Double}.
     * @return the random Normal {@link ValType#REAL} value or
     *         {@link Value#UNDEFINED} if sigma <= 0 or any of supplied values is not convertable to {@link Double}.
     */
    public Value nextNormal(Value mu, Value sigma) {
        try {
            return new Value(randomData.nextGaussian(mu.doubleValue(), sigma.doubleValue()));
        } catch (Exception e) {
            return Value.UNDEFINED;
        }
    }

    /**
     * Generates a random value from the Binomial Distribution. This implementation uses inversion to generate random values.
     *
     * @param numberOfTrials      number of trials of the Binomial distribution, must be convertable to {@link Integer}.
     * @param probabilityOfSuccess probability of success of the Binomial distribution, must be convertable to {@link Double}.
     * @return the random {@link ValType#INTEGER} value sampled from the Binomial distribution  or
     *         {@link Value#UNDEFINED} if probabilityOfSuccess >=0 or probabilityOfSuccess >1 or numberOfTrials < 0 or supplied numberOfTrials and probabilityOfSuccess are not convertable to {@link Integer} and {@link Double} respectivly.
     */
    public Value nextBinomial(Value numberOfTrials, Value probabilityOfSuccess) {
        try {
            return new Value(randomData.nextBinomial(numberOfTrials.intValue(), probabilityOfSuccess.doubleValue()));
        } catch (Exception e) {
            return Value.UNDEFINED;
        }
    }

    /**
     * Reseeds the random number generator with the supplied seed.
     *
     * @param seed the seed value to use, must be of {@link ValType#INTEGER} type
     */
    public void reSeed(Value seed) {
        if (ValType.INTEGER == seed.getType()) {
            randomData.reSeed(seed.longValue());
        }
    }

    /**
     * Reseeds the random number generator with current time in ms.
     */
    public void reSeed() {
        randomData.reSeed();
    }
}
