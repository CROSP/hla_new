package ua.cn.stu.cs.ems.ecli;

/**
 * Interface that must be implemented by ECL's user in order to provide access
 * to inner-token attributes.
 *
 * @author n0weak
 */
public interface EcliToken {

    /**
     * Indicates whether current mark contains attribute with the specified
     * key.
     *
     * @param key key of the attribute to be checked
     * @return true if attribute exists, false if not
     */
    boolean containsAttribute(Object key);

    /**
     * Initializes attribute with supplied value.
     *
     * @param key   - key of the attribute
     * @param value - value of the attribute. Note that actual value is
     * wrapped in {@link Value} instance.
     * @return true if attribute was initialized successfully,
     *         false if the attribute was not predefined and dynamic
     *         initialization is forbidden
     */
    boolean setAttribute(Object key, Value value);

    /**
     * Returns attribute with the specified key, null if such attribute
     * does not exist.
     *
     * @param key - name of the attribute
     * @return value of the attribute with the specified key
     */
    Object getAttribute(Object key);

}
