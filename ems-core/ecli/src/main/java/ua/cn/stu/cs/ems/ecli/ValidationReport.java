package ua.cn.stu.cs.ems.ecli;

import ua.cn.stu.cs.ems.ecli.errors.ParsingError;

import java.util.ArrayList;
import java.util.List;

/**
 * @author n0weak
 */
public class ValidationReport {

    private List<ParsingError> errors;
    private boolean returnsValue;


    public ValidationReport(List<ParsingError> errors, boolean returnsValue) {
        this.errors = new ArrayList<ParsingError>(errors);
        this.returnsValue = returnsValue;
    }

    public List<ParsingError> getParsingErrors() {
        return errors;
    }

    /**
     * @return true if validated code will return some value in any case, false if not.
     * Such input as 'IF (a>b) RETURN 2;' or 'IF (a>b) RETURN 2; ELSE RETURN;' will produce false. 'IF (a>b) RETURN 2; ELSE RETURN -2;' will produce true.
     */
    public boolean returnsValue() {
        return returnsValue;
    }

    boolean validationOK() {
        return errors.isEmpty();
    }
}
