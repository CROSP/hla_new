package ua.cn.stu.cs.ems.ecli.errors;

import org.antlr.runtime.MismatchedTokenException;

/**
 * Separate class for unexpected EOF error.
 *
 * Provides 0/-1 as line/pos values, adapts {@link MismatchedTokenException} to {@link InformativeException}.
 *
 * @author n0weak
 */
public class UnexpectedEOFException extends MismatchedTokenException implements InformativeException {

    public UnexpectedEOFException(MismatchedTokenException mte) {
        this.line = 0;
        this.charPositionInLine = -1;
        this.c = mte.c;
        this.expecting = mte.expecting;
        this.token = mte.token;
        this.input = mte.input;
        this.node = mte.node;
    }

    /**
     * @return 0 (cause EOF itself has not actual line/pos values). Same as {@link #getEndLine()}
     */
    public int getStartLine() {
        return this.line;
    }

    /**
     * @return 0 (cause EOF itself has not actual line/pos values). Same as {@link #getStartLine()}
     */
    public int getEndLine() {
        return this.line;
    }

    /**
     * @return -1 (cause EOF itself has not actual line/pos values). Same as {@link #getEndPos()}
     */
    public int getStartPos() {
        return this.charPositionInLine;
    }

    /**
     * @return -1 (cause EOF itself has not actual line/pos values). Same as {@link #getStartPos()}
     */
    public int getEndPos() {
        return this.charPositionInLine;
    }
}
