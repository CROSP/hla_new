package ua.cn.stu.cs.ems.ecli.errors;

/**
 * Provides information about error that happened during parsing. Exact error's type can be identified by {@link #errorCode} field.
 * <p/>
 * Start/end positions of the source code that caused the error can be obtained via corresponding fields.
 * Note that 0/-1 line/pos values mean that parser found EOF symbol instead of the needed one.
 *
 * @author n0weak
 */
public class ParsingError {

    /**
     * Actual counting starts from 1, 0 value indicates that interpreter failed to obtain valid line/pos values.
     */
    private int startLine = 0;
    /**
     * Actual counting starts from 0, -1 value indicates that interpreter failed to obtain valid line/pos values.
     */
    private int startPos = -1;
    /**
     * Actual counting starts from 1, 0 value indicates that interpreter failed to obtain valid line/pos values.
     */
    private int endLine = 0;
    /**
     * Actual counting starts from 0, -1 value indicates that interpreter failed to obtain valid line/pos values.
     */
    private int endPos = -1;
    private ErrorCode errorCode;
    private String message;

    public ParsingError() {
    }

    public ParsingError(int startLine, int startPos, int endLine, int endPos, ErrorCode errorCode, String message) {
        this.startLine = startLine;
        this.startPos = startPos;
        this.endPos = endPos;
        this.errorCode = errorCode;
        this.message = message;
        this.endLine = endLine;
    }

    public int getEndLine() {
        return endLine;
    }

    public void setEndLine(int endLine) {
        this.endLine = endLine;
    }

    public void setEndPos(int endPos) {
        this.endPos = endPos;
    }

    public int getEndPos() {
        return endPos;
    }

    public int getStartLine() {
        return startLine;
    }

    public void setStartLine(int startLine) {
        this.startLine = startLine;
    }

    public int getStartPos() {
        return startPos;
    }

    public void setStartPos(int startPos) {
        this.startPos = startPos;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(ErrorCode errorCode) {
        this.errorCode = errorCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof ParsingError)) {
            return false;
        }
        ParsingError pe = (ParsingError) obj;
        if (getStartLine() != pe.getStartLine() || getStartPos() != pe.getStartPos() ||
                getEndLine() != pe.getEndLine() || getEndPos() != pe.getEndPos()) {
            return false;
        }
        if (getMessage() == null ? pe.getMessage() != null : !(getMessage().equals(pe.getMessage()))) {
            return false;
        }

        if (getErrorCode() == null ? pe.getErrorCode() != null : !(getErrorCode().equals(pe.getErrorCode()))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + startLine;
        result = 31 * result + startPos;
        result = 31 * result + endLine;
        result = 31 * result + endPos;
        if (null != errorCode) {
            result += errorCode.hashCode();
        }
        if (null != message) {
            result += message.hashCode();
        }
        return result;
    }

    @Override
    public String toString() {
        return "ParsingError{" +
                "startLine=" + startLine +
                ", startPos=" + startPos +
                ", endLine=" + endLine +
                ", endPos=" + endPos +
                ", errorCode=" + errorCode +
                ", message='" + message + '\'' +
                '}';
    }
}
