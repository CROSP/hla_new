package ua.cn.stu.cs.ems.ecli;

import java.math.BigDecimal;

/**
 * Represents any value in ECL. All supported types are enumerated in {@link ValType}.
 *
 * @author n0weak
 */
public class Value {

    /**
     * ECL's equivalent to null. However, it is an instantiated object. It is equal only to itself.
     */
    static final Object UNDEF_VAL = new Object() {
        public boolean equals(Object obj) {
            return this == obj;
        }

        public String toString() {
            return "UNDEFINED";
        }
    };

    /**
     * Pre instantiated bool variable in true state.
     */
    public static final Value TRUE = new Value(true, ValType.BOOL);

    /**
     * Pre instantiated bool variable in false state.
     */
    public static final Value FALSE = new Value(false, ValType.BOOL);

    /**
     * Pre instantiated variable in undefined state.
     */
    public static final Value UNDEFINED = new Value(UNDEF_VAL, ValType.UNDEFINED);

    private ValType valType;
    private Object value;
    private boolean typePredefined;

    /**
     * Default constructor. Initializes variable to {@link #UNDEFINED} state.
     */
    public Value() {
        this.value = UNDEF_VAL;
        this.valType = ValType.UNDEFINED;
    }

    /**
     * @param value value to initialize variable with. If value is null, then variable will be in {@link @UNDEFINED} state.
     */
    public Value(Object value) {
        setValue(value);
    }

    /**
     * Creates value instance with predefined type. Can be useful when user clearly specifies desired type, e.g. defining string values in '' or "".
     *
     * @param value value to initialize variable with. If value is null, then variable will be in {@link @UNDEFINED} state.
     * @param type  desired  type of the value
     * @throws IllegalArgumentException when specified value is not castable to the desired type
     */
    public Value(Object value, ValType type) {
        setValue(value, type);
    }

    /**
     * @return value of the variable
     */
    public Object getValue() {
        return value == null ? UNDEF_VAL : value;
    }

    /**
     * @param value value to reinitialize variable with. If value is null, then variable will be in {@link @UNDEFINED} state.
     */
    public void setValue(Object value) {
        if (value instanceof Value) {
            initFrom((Value) value);
        } else {
            valType = ValType.getType(value);
            this.value = valType.cast(value);
        }
    }

    /**
     * Sets the value with predefined type. Can be useful when user clearly specifies desired type, e.g. defining string values in '' or "".
     *
     * @param value value to reinitialize variable with. If value is null, then variable will be in {@link @UNDEFINED} state.
     * @param type  desired  type of the value
     * @throws IllegalArgumentException when specified value is not castable to the desired type
     */
    public void setValue(Object value, ValType type) {
        if (!type.isCastable(value)) {
            throw new IllegalArgumentException("Value " + value + " is not castable to " + type);
        }

        if (value instanceof Value) {
            initFrom((Value) value);
        } else {
            this.valType = type;
            this.value = valType.cast(value);
            this.typePredefined = true;
        }
    }

    public ValType getType() {
        if (null == valType) {
            valType = ValType.getType(value);
        }
        return valType;
    }


    /**
     * Double representation of the value.
     * {@link Double#NaN}, {@link Double#POSITIVE_INFINITY}, {@link Double#NEGATIVE_INFINITY} values are not supported, null will be returned instead.
     *
     * @return Double that represents this value or null if value cannot be casted to double.
     */
    public Double doubleValue() {
        Double d;
        try {
            d = Double.parseDouble(value.toString());
        } catch (Exception e) {
            d = null;
        }
        if (null != d && (d.isInfinite() || d.isNaN())) {
            d = null;
        }
        return d;
    }

    /**
     * @return Integer value that represents this value or null if variable cannot be casted to Integer.
     */
    public Integer intValue() {
        try {
            return Integer.parseInt(value.toString());
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * @return Long value that represents this value or null if variable cannot be casted to Long.
     */
    public Long longValue() {
        return (Long) (value instanceof Long ? value : ValType.INTEGER.cast(value));
    }

    /**
     * @return BigDecimal that represents this value or null if it cannot be casted to BigDecimal
     */
    public BigDecimal realValue() {
        return (BigDecimal) (value instanceof BigDecimal ? value : ValType.REAL.cast(value));
    }

    /**
     * @return Boolean that represents this value or null if it cannot be casted to Boolean
     */
    public Boolean boolValue() {
        return (Boolean) (value instanceof Boolean ? value : ValType.BOOL.cast(value));
    }

    /**
     * @return String that represents this value
     */
    public String strValue() {
        return (String) (value instanceof String ? value : ValType.STRING.cast(value));
    }

    public boolean isDefined() {
        return !this.equals(UNDEFINED);
    }

    /**
     * @return true if type of the value was predefined during creation and must not be evaluated dynamically, false if not
     */
    public boolean isTypePredefined() {
        return typePredefined;
    }

    @Override
    public String toString() {
        return this.strValue();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Value value = (Value) o;

        if (this.getType() != value.getType()) return false;
        if (!this.value.equals(value.value)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return value.hashCode();
    }

    /**
     * Compares this object with the specified object. Comparing values must have a common type.
     * Comparing defined values with {@link #UNDEFINED} is forbidden, null will be returned in such cases.
     *
     * @param o the object to be compared
     * @return a negative integer, zero, or a positive integer as this object is less than, equal to, or greater than the specified object;
     *         null if one of the object is of {@link ValType#UNDEFINED} type and the other is not or
     *         when common type of the values being compared is not {@link ValType#INTEGER} , {@link ValType#REAL}, {@link ValType#BOOL} or  {@link ValType#STRING}
     * @see Comparable#compareTo(Object)
     */
    public Integer compareTo(Value o) {
        if (this.equals(o)) {
            return 0;
        }

        if (this.getType() == ValType.UNDEFINED || o.getType() == ValType.UNDEFINED) {
            return null;
        }

        ValType commonType = ValType.getCommonType(this, o);
        switch (commonType) {
            case INTEGER:
                return this.longValue().compareTo(o.longValue());
            case REAL:
               // return this.realValue().subtract(o.realValue()).signum();
                return this.realValue().compareTo(o.realValue());
            case BOOL:
                return this.boolValue().compareTo(o.boolValue());
            case STRING:
                return this.strValue().compareTo(o.strValue());
            default:
                return null;
        }
    }

    private void initFrom(Value value) {
        this.valType = value.valType;
        this.value = value.value;
        this.typePredefined = value.typePredefined;
    }
}


