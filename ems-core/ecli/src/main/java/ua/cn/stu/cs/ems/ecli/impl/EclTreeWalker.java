// $ANTLR 3.5.2 ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g 2016-04-20 18:12:59

    	package ua.cn.stu.cs.ems.ecli.impl;

	    import java.lang.Math;
	    import ua.cn.stu.cs.ems.ecli.*;
	    import ua.cn.stu.cs.ems.ecli.errors.*;
	    import java.util.List;
	    import java.util.ArrayList;
	    import java.util.Set;
	    import java.util.HashSet;
	    import java.lang.StringBuilder;


import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class EclTreeWalker extends TreeParser {
	public static final String[] tokenNames = new String[] {
		"<invalid>", "<EOR>", "<DOWN>", "<UP>", "ABS", "AGGREGATE", "ASSIGN", 
		"ATAN", "BINOMIAL", "BLOCK", "COMMENT", "COS", "COT", "CURRENT_MARK", 
		"DIGIT", "DIV", "ELSE", "ENTIER", "EQ", "ESC", "EXP", "EXPONENTIAL", "FALSE", 
		"GE", "GT", "ID", "IF", "IF_BEGIN", "IF_END", "IF_STMT", "INT", "INVERT", 
		"IS_MARKED", "LAND", "LE", "LETTER", "LN", "LOR", "LT", "MALFORMED_ID", 
		"MARK", "MINUS", "MULT", "NE", "NORMAL", "NOT", "PLACE", "PLUS", "POISSON", 
		"POW", "REAL", "RETURN", "ROOT_AGG", "SEED", "SIGN", "SIN", "SQRT", "STMT_BEGIN", 
		"STMT_END", "STR", "SYS_VAR", "TAN", "TIME", "TRUE", "UNIFORM", "VAR", 
		"VAR_DECL", "WS", "'('", "')'", "','", "'.'", "';'", "'['", "']'", "'{'", 
		"'}'"
	};
	public static final int EOF=-1;
	public static final int T__68=68;
	public static final int T__69=69;
	public static final int T__70=70;
	public static final int T__71=71;
	public static final int T__72=72;
	public static final int T__73=73;
	public static final int T__74=74;
	public static final int T__75=75;
	public static final int T__76=76;
	public static final int ABS=4;
	public static final int AGGREGATE=5;
	public static final int ASSIGN=6;
	public static final int ATAN=7;
	public static final int BINOMIAL=8;
	public static final int BLOCK=9;
	public static final int COMMENT=10;
	public static final int COS=11;
	public static final int COT=12;
	public static final int CURRENT_MARK=13;
	public static final int DIGIT=14;
	public static final int DIV=15;
	public static final int ELSE=16;
	public static final int ENTIER=17;
	public static final int EQ=18;
	public static final int ESC=19;
	public static final int EXP=20;
	public static final int EXPONENTIAL=21;
	public static final int FALSE=22;
	public static final int GE=23;
	public static final int GT=24;
	public static final int ID=25;
	public static final int IF=26;
	public static final int IF_BEGIN=27;
	public static final int IF_END=28;
	public static final int IF_STMT=29;
	public static final int INT=30;
	public static final int INVERT=31;
	public static final int IS_MARKED=32;
	public static final int LAND=33;
	public static final int LE=34;
	public static final int LETTER=35;
	public static final int LN=36;
	public static final int LOR=37;
	public static final int LT=38;
	public static final int MALFORMED_ID=39;
	public static final int MARK=40;
	public static final int MINUS=41;
	public static final int MULT=42;
	public static final int NE=43;
	public static final int NORMAL=44;
	public static final int NOT=45;
	public static final int PLACE=46;
	public static final int PLUS=47;
	public static final int POISSON=48;
	public static final int POW=49;
	public static final int REAL=50;
	public static final int RETURN=51;
	public static final int ROOT_AGG=52;
	public static final int SEED=53;
	public static final int SIGN=54;
	public static final int SIN=55;
	public static final int SQRT=56;
	public static final int STMT_BEGIN=57;
	public static final int STMT_END=58;
	public static final int STR=59;
	public static final int SYS_VAR=60;
	public static final int TAN=61;
	public static final int TIME=62;
	public static final int TRUE=63;
	public static final int UNIFORM=64;
	public static final int VAR=65;
	public static final int VAR_DECL=66;
	public static final int WS=67;

	// delegates
	public TreeParser[] getDelegates() {
		return new TreeParser[] {};
	}

	// delegators


	public EclTreeWalker(TreeNodeStream input) {
		this(input, new RecognizerSharedState());
	}
	public EclTreeWalker(TreeNodeStream input, RecognizerSharedState state) {
		super(input, state);
	}

	@Override public String[] getTokenNames() { return EclTreeWalker.tokenNames; }
	@Override public String getGrammarFileName() { return "ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g"; }


		private Heap heap = new Heap();
		private EclRandomData randomData;
		private ModelAccessor modelAccessor;
		private List<RecognitionErrorListener> warningListeners = new ArrayList<RecognitionErrorListener>();
		
		/**
		 * Indicates whether current token is contained inside assign statement.
		 */
		private boolean inAssign = false;
		
		/**
	     * list of ids that have already appeared in errors reporting, no need to report about 'em again
	     */	 
		private Set<Object> reportedIds = new HashSet<Object>();  
		
		/**
		 * Indicates whether walker must validate input or interpret it.
		 */
		private boolean validating = false;

		/**
		 * Represents number of current token in current statement, counting starts from 1.
		 */
		private int tokenNo = 0;
		
		/** 
		 * Interpretation result, expression from 'RETURN' statement 
		 * @see ua.cn.stu.cs.jess.ecli.InterpretationReport#getResult() 
		 */
		private Value retVal;
		
		/** Defines whether interpreter must make evaluation, i.e. execute rule actions. Used by if-the-else statement during interpretation. **/
		private boolean evaluate = true;
		
		/**
	     * @see ua.cn.stu.cs.jess.ecli.ValidationReport#returnsValue()
	     */
		private boolean returnsValue;

		/*
		 * Indicates whether statements that are being currently processed are specified after 'RETURN' statement,
		 * hence they are unreachable.
		 * Used during validation.
		 */
	    private boolean afterReturn;
		
		public EclTreeWalker(TreeNodeStream input, ModelAccessor modelAccessor, EclRandomData randomData) {
			this(input);
			this.modelAccessor = modelAccessor;
			this.randomData = randomData;
		}
		

		public Value getRetVal() {
			return retVal;
		}

		/**
	     * @see ua.cn.stu.cs.jess.ecli.ValidationReport#returnsValue()
	     */
		public boolean returnsValue() {
			return returnsValue;
		}

		public void addWarningListener(RecognitionErrorListener listener) {
		    warningListeners.add(listener);
	    }

	    public void warningErrorListener(RecognitionErrorListener listener) {
		    warningListeners.remove(listener);
	    }

	    public void warn(RecognitionException e) {
	        for (RecognitionErrorListener listener : warningListeners) {
	            listener.errorHappened(e, this);
	        }
	    }


		public void displayRecognitionError(String[] tokenNames, RecognitionException e){
		    if(validating) {
			    warn(e);
			} else {
			    super.displayRecognitionError(tokenNames, e);
			}
		}

		public Object recoverFromMismatchedSet(IntStream input, RecognitionException e, BitSet follow) 
						throws RecognitionException{
	        if (validating) {
	            return super.recoverFromMismatchedSet(input, e, follow);
	        } else {
	            throw e;
	        }
		}
		
		public Object match(IntStream input, int ttype, BitSet follow) throws RecognitionException {
			tokenNo++;
			return super.match(input, ttype, follow);		
		}

		protected Object recoverFromMismatchedToken(IntStream input, int ttype, BitSet follow)
						throws RecognitionException {
	        if (validating) {
	            return super.recoverFromMismatchedToken(input, ttype, follow);
	        } else {
	            throw new MismatchedTokenException(ttype, input);
	        }
		}
		
		private void reportUndefinedRef(OuterRef.RefDecorator ref) throws ECLSemanticException {
			switch (ref.getType()) {
				case VAR:
					handleError(new ECLSemanticException("MissingVariable", ErrorCode.INVALID_REFERENCE, ref.getKey().toString()));
				case ATTR:
					handleError(new ECLSemanticException("MissingAttribute", ErrorCode.INVALID_REFERENCE, ref.getKey().toString()));
			}
		}

		private void handleError(ECLSemanticException e) throws ECLSemanticException{
	        if (validating || ErrorCode.Severity.WARNING == e.getErrorCode().getSeverity()) {
	            statement_stack.peek().warnings.add(e);
	        } else {
	            throw e;
	        }
		}
		



	// $ANTLR start "interpret"
	// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:171:1: interpret : ecl ;
	public final void interpret() throws RecognitionException {

			validating = false;	

		try {
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:180:13: ( ecl )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:180:16: ecl
			{
			pushFollow(FOLLOW_ecl_in_interpret75);
			ecl();
			state._fsp--;

			}


				if (null != retVal) {
					returnsValue = true;
				}

		}

					catch (RecognitionException re) {
					    if (validating) {
		                    reportError(re);
		                    recover(input,re);
					    } else {
						    throw re;
						}
					} catch (InterpretationException ie) {
						throw ie;
					} catch (Exception e) {
						throw new InterpretationException("Interpretation failed due to inner error. May be a bug, please report", e);
					}	

		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "interpret"



	// $ANTLR start "validate"
	// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:182:1: validate : ecl ;
	public final void validate() throws RecognitionException {

			validating = true;

		try {
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:186:13: ( ecl )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:186:16: ecl
			{
			pushFollow(FOLLOW_ecl_in_validate101);
			ecl();
			state._fsp--;

			}

		}

					catch (RecognitionException re) {
					    if (validating) {
		                    reportError(re);
		                    recover(input,re);
					    } else {
						    throw re;
						}
					} catch (InterpretationException ie) {
						throw ie;
					} catch (Exception e) {
						throw new InterpretationException("Interpretation failed due to inner error. May be a bug, please report", e);
					}	

		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "validate"



	// $ANTLR start "ecl"
	// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:188:1: ecl : ( statement )* ;
	public final void ecl() throws RecognitionException {
		boolean statement1 =false;

		try {
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:188:7: ( ( statement )* )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:188:9: ( statement )*
			{
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:188:9: ( statement )*
			loop1:
			while (true) {
				int alt1=2;
				int LA1_0 = input.LA(1);
				if ( (LA1_0==IF_BEGIN||LA1_0==STMT_BEGIN) ) {
					alt1=1;
				}

				switch (alt1) {
				case 1 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:188:10: statement
					{
					pushFollow(FOLLOW_statement_in_ecl112);
					statement1=statement();
					state._fsp--;


											if (!validating) {
												if (statement1) { 
													return;
												}
											}	
					                    
					}
					break;

				default :
					break loop1;
				}
			}

			}

		}

					catch (RecognitionException re) {
					    if (validating) {
		                    reportError(re);
		                    recover(input,re);
					    } else {
						    throw re;
						}
					} catch (InterpretationException ie) {
						throw ie;
					} catch (Exception e) {
						throw new InterpretationException("Interpretation failed due to inner error. May be a bug, please report", e);
					}	

		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ecl"


	protected static class statement_scope {
		Integer startLine;
		Integer startPos;
		List<ECLSemanticException> warnings;
		boolean reportUndefined;
		//indicates whether 'undefined value' errors should be reported for this stmt;
	}
	protected Stack<statement_scope> statement_stack = new Stack<statement_scope>();


	// $ANTLR start "statement"
	// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:198:1: statement returns [boolean ret] : ( STMT_BEGIN stmt STMT_END | IF_BEGIN if_stmt IF_END );
	public final boolean statement() throws RecognitionException {
		statement_stack.push(new statement_scope());
		boolean ret = false;


		CommonTree STMT_BEGIN2=null;
		CommonTree STMT_END4=null;
		boolean stmt3 =false;
		boolean if_stmt5 =false;


		    statement_stack.peek().warnings = new ArrayList<ECLSemanticException>();
		    statement_stack.peek().reportUndefined = true;

		try {
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:209:11: ( STMT_BEGIN stmt STMT_END | IF_BEGIN if_stmt IF_END )
			int alt2=2;
			int LA2_0 = input.LA(1);
			if ( (LA2_0==STMT_BEGIN) ) {
				alt2=1;
			}
			else if ( (LA2_0==IF_BEGIN) ) {
				alt2=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 2, 0, input);
				throw nvae;
			}

			switch (alt2) {
				case 1 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:209:13: STMT_BEGIN stmt STMT_END
					{
					STMT_BEGIN2=(CommonTree)match(input,STMT_BEGIN,FOLLOW_STMT_BEGIN_in_statement158); 

										tokenNo = 0;
					 	                if (!validating) {
					                        if (!evaluate) {consumeUntil(input, STMT_END); input.consume(); return false;} //skipping current statement
					                        statement_stack.peek().startLine = (STMT_BEGIN2!=null?STMT_BEGIN2.getLine():0);
					                        statement_stack.peek().startPos = STMT_BEGIN2.getCharPositionInLine();
										}
									
					pushFollow(FOLLOW_stmt_in_statement167);
					stmt3=stmt();
					state._fsp--;

					STMT_END4=(CommonTree)match(input,STMT_END,FOLLOW_STMT_END_in_statement169); 

									    ret = stmt3;
									    if (validating) {
					                        if (afterReturn) {
					                            reportError(new ECLSemanticException(ErrorCode.UNREACHABLE_STMT, STMT_BEGIN2, STMT_END4));
					                        } else if (stmt3) {
					                            afterReturn = true;
					                            returnsValue = true;
					                        }
					                    }
									    for (ECLSemanticException e: statement_stack.peek().warnings) {
					                        if (!e.isFullyInitialized()) {
					                            e.line = (STMT_BEGIN2!=null?STMT_BEGIN2.getLine():0);
					                            e.charPositionInLine = STMT_BEGIN2.getCharPositionInLine();
					                            e.endLine = (STMT_END4!=null?STMT_END4.getLine():0);
					                            e.endPos = STMT_END4.getCharPositionInLine();
					                            e.setFullyInitialized(true);
					                        }
					                        if (validating) {
					                            reportError(e);
					                        } else {
					                            warn(e);
					                        }
					                    }
									
					}
					break;
				case 2 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:242:6: IF_BEGIN if_stmt IF_END
					{
					match(input,IF_BEGIN,FOLLOW_IF_BEGIN_in_statement178); 

								        if (!validating) {
										    if (!evaluate) {
												consumeUntil(input, IF_END); input.consume(); return false; //skipping current statement
											} 
										}
									
					pushFollow(FOLLOW_if_stmt_in_statement186);
					if_stmt5=if_stmt();
					state._fsp--;


									    if (!validating) {  
									        if (if_stmt5) {
												return true;
											}
									    }
									
					match(input,IF_END,FOLLOW_IF_END_in_statement194); 
					ret = if_stmt5;
					}
					break;

			}
		}
		catch (RecognitionException re) {

							//need to initialize end line and position of the error
							if (re instanceof ECLSemanticException && !((ECLSemanticException)re).isFullyInitialized() ) {	
								ECLSemanticException ese = (ECLSemanticException) re;
								//we've already passed syntax validation phase, hence there is definitely STMT_END token somewhere
								//it will indicate the end of out statement
								consumeUntil(input, STMT_END);
								CommonTree endTree = (CommonTree) getCurrentInputSymbol(input);
								ese.line = statement_stack.peek().startLine;
								ese.charPositionInLine = statement_stack.peek().startPos;
								ese.endLine = endTree.getLine();
								ese.endPos = endTree.getCharPositionInLine();
								ese.setFullyInitialized(true);
			                } 
								throw re;
						
		}
		catch (InterpretationException ie) {

				throw ie;

		}
		catch (Exception e) {

				throw new InterpretationException("Interpretation failed due to inner error. May be a bug, please report", e);

		}

		finally {
			// do for sure before leaving
			statement_stack.pop();
		}
		return ret;
	}
	// $ANTLR end "statement"



	// $ANTLR start "stmt"
	// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:281:1: stmt returns [boolean ret] : ( standalone_expr | assign | declar | seed_stmt |r= ret );
	public final boolean stmt() throws RecognitionException {
		boolean ret = false;


		boolean r =false;

		try {
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:282:4: ( standalone_expr | assign | declar | seed_stmt |r= ret )
			int alt3=5;
			switch ( input.LA(1) ) {
			case ABS:
			case ATAN:
			case BINOMIAL:
			case COS:
			case COT:
			case CURRENT_MARK:
			case DIV:
			case ENTIER:
			case EQ:
			case EXP:
			case EXPONENTIAL:
			case FALSE:
			case GE:
			case GT:
			case ID:
			case INT:
			case INVERT:
			case IS_MARKED:
			case LAND:
			case LE:
			case LN:
			case LOR:
			case LT:
			case MALFORMED_ID:
			case MARK:
			case MINUS:
			case MULT:
			case NE:
			case NORMAL:
			case PLUS:
			case POISSON:
			case POW:
			case REAL:
			case SIGN:
			case SIN:
			case SQRT:
			case STR:
			case SYS_VAR:
			case TAN:
			case TIME:
			case TRUE:
			case UNIFORM:
				{
				alt3=1;
				}
				break;
			case ASSIGN:
				{
				alt3=2;
				}
				break;
			case VAR_DECL:
				{
				alt3=3;
				}
				break;
			case SEED:
				{
				alt3=4;
				}
				break;
			case RETURN:
				{
				alt3=5;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 3, 0, input);
				throw nvae;
			}
			switch (alt3) {
				case 1 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:282:6: standalone_expr
					{
					pushFollow(FOLLOW_standalone_expr_in_stmt239);
					standalone_expr();
					state._fsp--;

					}
					break;
				case 2 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:283:6: assign
					{
					pushFollow(FOLLOW_assign_in_stmt246);
					assign();
					state._fsp--;

					}
					break;
				case 3 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:284:6: declar
					{
					pushFollow(FOLLOW_declar_in_stmt253);
					declar();
					state._fsp--;

					}
					break;
				case 4 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:285:6: seed_stmt
					{
					pushFollow(FOLLOW_seed_stmt_in_stmt260);
					seed_stmt();
					state._fsp--;

					}
					break;
				case 5 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:286:8: r= ret
					{
					pushFollow(FOLLOW_ret_in_stmt271);
					r=ret();
					state._fsp--;

					ret = r;
					}
					break;

			}
		}

					catch (RecognitionException re) {
					    if (validating) {
		                    reportError(re);
		                    recover(input,re);
					    } else {
						    throw re;
						}
					} catch (InterpretationException ie) {
						throw ie;
					} catch (Exception e) {
						throw new InterpretationException("Interpretation failed due to inner error. May be a bug, please report", e);
					}	

		finally {
			// do for sure before leaving
		}
		return ret;
	}
	// $ANTLR end "stmt"



	// $ANTLR start "seed_stmt"
	// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:289:1: seed_stmt : ^( SEED a= expr ) ;
	public final void seed_stmt() throws RecognitionException {
		Value a =null;

		try {
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:289:11: ( ^( SEED a= expr ) )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:289:13: ^( SEED a= expr )
			{
			match(input,SEED,FOLLOW_SEED_in_seed_stmt290); 
			match(input, Token.DOWN, null); 
			pushFollow(FOLLOW_expr_in_seed_stmt294);
			a=expr();
			state._fsp--;

			match(input, Token.UP, null); 


			                    if (!validating) {
			                        randomData.reSeed(a);
			                    }
			                
			}

		}

					catch (RecognitionException re) {
					    if (validating) {
		                    reportError(re);
		                    recover(input,re);
					    } else {
						    throw re;
						}
					} catch (InterpretationException ie) {
						throw ie;
					} catch (Exception e) {
						throw new InterpretationException("Interpretation failed due to inner error. May be a bug, please report", e);
					}	

		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "seed_stmt"



	// $ANTLR start "standalone_expr"
	// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:296:1: standalone_expr : expr ;
	public final void standalone_expr() throws RecognitionException {
		try {
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:297:4: ( expr )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:297:6: expr
			{
			pushFollow(FOLLOW_expr_in_standalone_expr315);
			expr();
			state._fsp--;


										handleError(new ECLSemanticException(ErrorCode.INVALID_STMT));
									
			}

		}

					catch (RecognitionException re) {
					    if (validating) {
		                    reportError(re);
		                    recover(input,re);
					    } else {
						    throw re;
						}
					} catch (InterpretationException ie) {
						throw ie;
					} catch (Exception e) {
						throw new InterpretationException("Interpretation failed due to inner error. May be a bug, please report", e);
					}	

		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "standalone_expr"



	// $ANTLR start "declar"
	// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:302:1: declar : ^( VAR_DECL id ( expr )? ) ;
	public final void declar() throws RecognitionException {
		TreeRuleReturnScope id6 =null;
		Value expr7 =null;

		try {
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:302:9: ( ^( VAR_DECL id ( expr )? ) )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:302:11: ^( VAR_DECL id ( expr )? )
			{
			match(input,VAR_DECL,FOLLOW_VAR_DECL_in_declar333); 
			match(input, Token.DOWN, null); 
			pushFollow(FOLLOW_id_in_declar335);
			id6=id();
			state._fsp--;

			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:302:25: ( expr )?
			int alt4=2;
			int LA4_0 = input.LA(1);
			if ( (LA4_0==ABS||(LA4_0 >= ATAN && LA4_0 <= BINOMIAL)||(LA4_0 >= COS && LA4_0 <= CURRENT_MARK)||LA4_0==DIV||(LA4_0 >= ENTIER && LA4_0 <= EQ)||(LA4_0 >= EXP && LA4_0 <= ID)||(LA4_0 >= INT && LA4_0 <= LE)||(LA4_0 >= LN && LA4_0 <= NORMAL)||(LA4_0 >= PLUS && LA4_0 <= REAL)||(LA4_0 >= SIGN && LA4_0 <= SQRT)||(LA4_0 >= STR && LA4_0 <= UNIFORM)) ) {
				alt4=1;
			}
			switch (alt4) {
				case 1 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:302:25: expr
					{
					pushFollow(FOLLOW_expr_in_declar337);
					expr7=expr();
					state._fsp--;

					}
					break;

			}

			match(input, Token.UP, null); 


			                                if (!(id6!=null?((EclTreeWalker.id_return)id6).isMalformed:false)) {
			                                    if (heap.containsVar((id6!=null?((EclTreeWalker.id_return)id6).text:null))) {
			                                        handleError(new ECLSemanticException(ErrorCode.REDEFINED_VAR, (id6!=null?((EclTreeWalker.id_return)id6).text:null)));
			                                    } else {
			                                        heap.putVar((id6!=null?((EclTreeWalker.id_return)id6).text:null), expr7 != null? expr7 : Value.UNDEFINED);
													if (statement_block_stack.size() > 0) {
														statement_block_stack.peek().localVars.add((id6!=null?((EclTreeWalker.id_return)id6).text:null));
													}
			                                    }
			                                }
			                            
			}

		}

					catch (RecognitionException re) {
					    if (validating) {
		                    reportError(re);
		                    recover(input,re);
					    } else {
						    throw re;
						}
					} catch (InterpretationException ie) {
						throw ie;
					} catch (Exception e) {
						throw new InterpretationException("Interpretation failed due to inner error. May be a bug, please report", e);
					}	

		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "declar"



	// $ANTLR start "ret"
	// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:317:1: ret returns [boolean ret] : ( ^( RETURN expr ) | RETURN );
	public final boolean ret() throws RecognitionException {
		boolean ret = false;


		Value expr8 =null;

		try {
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:318:4: ( ^( RETURN expr ) | RETURN )
			int alt5=2;
			int LA5_0 = input.LA(1);
			if ( (LA5_0==RETURN) ) {
				int LA5_1 = input.LA(2);
				if ( (LA5_1==DOWN) ) {
					alt5=1;
				}
				else if ( (LA5_1==STMT_END) ) {
					alt5=2;
				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 5, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 5, 0, input);
				throw nvae;
			}

			switch (alt5) {
				case 1 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:318:8: ^( RETURN expr )
					{
					match(input,RETURN,FOLLOW_RETURN_in_ret366); 
					match(input, Token.DOWN, null); 
					pushFollow(FOLLOW_expr_in_ret368);
					expr8=expr();
					state._fsp--;

					match(input, Token.UP, null); 


										ret = true;
					                    if (!validating) {
					                        this.retVal = expr8;
					                    }
					                
					}
					break;
				case 2 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:324:6: RETURN
					{
					match(input,RETURN,FOLLOW_RETURN_in_ret378); 

								        if (validating) {
					                        ret = false;
					                    } else {
											ret = true;
								            this.retVal = null;
								        }
								    
					}
					break;

			}
		}

					catch (RecognitionException re) {
					    if (validating) {
		                    reportError(re);
		                    recover(input,re);
					    } else {
						    throw re;
						}
					} catch (InterpretationException ie) {
						throw ie;
					} catch (Exception e) {
						throw new InterpretationException("Interpretation failed due to inner error. May be a bug, please report", e);
					}	

		finally {
			// do for sure before leaving
		}
		return ret;
	}
	// $ANTLR end "ret"



	// $ANTLR start "if_stmt"
	// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:334:1: if_stmt returns [boolean ret] : ^( IF_STMT expr a= if_block (b= if_block )? ) ;
	public final boolean if_stmt() throws RecognitionException {
		boolean ret = false;


		boolean a =false;
		boolean b =false;
		Value expr9 =null;

		try {
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:336:4: ( ^( IF_STMT expr a= if_block (b= if_block )? ) )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:336:6: ^( IF_STMT expr a= if_block (b= if_block )? )
			{
			match(input,IF_STMT,FOLLOW_IF_STMT_in_if_stmt414); 
			match(input, Token.DOWN, null); 
			pushFollow(FOLLOW_expr_in_if_stmt416);
			expr9=expr();
			state._fsp--;


						            if (!validating) {
			                            if ((expr9.getType() == ValType.BOOL && !expr9.boolValue()) ||
			                                    (!expr9.isDefined())) {
			                                evaluate = false;
			                            }
								    }
								
			pushFollow(FOLLOW_if_block_in_if_stmt427);
			a=if_block();
			state._fsp--;


								    if (validating) {
								        afterReturn = false;
								    } else {
								        if (a) return true;
								        evaluate = !evaluate;
			                        }
								
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:352:6: (b= if_block )?
			int alt6=2;
			int LA6_0 = input.LA(1);
			if ( (LA6_0==BLOCK||LA6_0==IF_BEGIN||LA6_0==STMT_BEGIN) ) {
				alt6=1;
			}
			switch (alt6) {
				case 1 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:352:7: b= if_block
					{
					pushFollow(FOLLOW_if_block_in_if_stmt439);
					b=if_block();
					state._fsp--;


										        if (!validating && b) {
										            return true;
										        }
										    
					}
					break;

			}

			match(input, Token.UP, null); 


			                        if (validating) {
			                            if (!a || !b) { //If both parts of if-else statement contain return statement,
			                                                       //then all others are definitely placed after return, hence are unreachable.
			                                                       //Otherwise other statements may be reachable.
			                                afterReturn = false;
			                                returnsValue = false;
			                            } else {
			                                afterReturn = true;
			                                ret = true;
			                            }
			                        } else {
			                            ret = false;
			                        }
							
			}

			evaluate = true;
		}

					catch (RecognitionException re) {
					    if (validating) {
		                    reportError(re);
		                    recover(input,re);
					    } else {
						    throw re;
						}
					} catch (InterpretationException ie) {
						throw ie;
					} catch (Exception e) {
						throw new InterpretationException("Interpretation failed due to inner error. May be a bug, please report", e);
					}	

		finally {
			// do for sure before leaving
		}
		return ret;
	}
	// $ANTLR end "if_stmt"



	// $ANTLR start "if_block"
	// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:376:1: if_block returns [boolean ret] : ( statement | statement_block );
	public final boolean if_block() throws RecognitionException {
		boolean ret = false;


		boolean statement10 =false;
		boolean statement_block11 =false;

		try {
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:377:4: ( statement | statement_block )
			int alt7=2;
			int LA7_0 = input.LA(1);
			if ( (LA7_0==IF_BEGIN||LA7_0==STMT_BEGIN) ) {
				alt7=1;
			}
			else if ( (LA7_0==BLOCK) ) {
				alt7=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 7, 0, input);
				throw nvae;
			}

			switch (alt7) {
				case 1 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:377:6: statement
					{
					pushFollow(FOLLOW_statement_in_if_block483);
					statement10=statement();
					state._fsp--;

					ret = statement10;
					}
					break;
				case 2 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:378:6: statement_block
					{
					pushFollow(FOLLOW_statement_block_in_if_block493);
					statement_block11=statement_block();
					state._fsp--;

					ret = statement_block11;
					}
					break;

			}
		}

					catch (RecognitionException re) {
					    if (validating) {
		                    reportError(re);
		                    recover(input,re);
					    } else {
						    throw re;
						}
					} catch (InterpretationException ie) {
						throw ie;
					} catch (Exception e) {
						throw new InterpretationException("Interpretation failed due to inner error. May be a bug, please report", e);
					}	

		finally {
			// do for sure before leaving
		}
		return ret;
	}
	// $ANTLR end "if_block"


	protected static class statement_block_scope {
		List<String> localVars;
		//contains names of all the variables which were defined in this block;
	}
	protected Stack<statement_block_scope> statement_block_stack = new Stack<statement_block_scope>();


	// $ANTLR start "statement_block"
	// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:382:1: statement_block returns [boolean ret] : ^( BLOCK ( statement )* ) ;
	public final boolean statement_block() throws RecognitionException {
		statement_block_stack.push(new statement_block_scope());
		boolean ret = false;


		boolean statement12 =false;


			statement_block_stack.peek().localVars = new ArrayList<String>();

		try {
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:394:4: ( ^( BLOCK ( statement )* ) )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:394:7: ^( BLOCK ( statement )* )
			{
			match(input,BLOCK,FOLLOW_BLOCK_in_statement_block533); 
			if ( input.LA(1)==Token.DOWN ) {
				match(input, Token.DOWN, null); 
				// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:394:15: ( statement )*
				loop8:
				while (true) {
					int alt8=2;
					int LA8_0 = input.LA(1);
					if ( (LA8_0==IF_BEGIN||LA8_0==STMT_BEGIN) ) {
						alt8=1;
					}

					switch (alt8) {
					case 1 :
						// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:394:16: statement
						{
						pushFollow(FOLLOW_statement_in_statement_block536);
						statement12=statement();
						state._fsp--;


									                    if (statement12) {
									                        if (validating) {
									                            ret = true;
						                                    } else {
						                                        return true;
						                                    }
						                                }
									                
						}
						break;

					default :
						break loop8;
					}
				}

				match(input, Token.UP, null); 
			}

			}


				for (String name: statement_block_stack.peek().localVars) {
					heap.removeVar(name);
				}

		}

					catch (RecognitionException re) {
					    if (validating) {
		                    reportError(re);
		                    recover(input,re);
					    } else {
						    throw re;
						}
					} catch (InterpretationException ie) {
						throw ie;
					} catch (Exception e) {
						throw new InterpretationException("Interpretation failed due to inner error. May be a bug, please report", e);
					}	

		finally {
			// do for sure before leaving
			statement_block_stack.pop();
		}
		return ret;
	}
	// $ANTLR end "statement_block"



	// $ANTLR start "assign"
	// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:409:1: assign : ( ^( ASSIGN id expr ) | ^( ASSIGN sys_id expr ) );
	public final void assign() throws RecognitionException {
		TreeRuleReturnScope id13 =null;
		Value expr14 =null;
		OuterRef.RefDecorator sys_id15 =null;
		Value expr16 =null;


			inAssign = true;

		try {
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:416:4: ( ^( ASSIGN id expr ) | ^( ASSIGN sys_id expr ) )
			int alt9=2;
			int LA9_0 = input.LA(1);
			if ( (LA9_0==ASSIGN) ) {
				int LA9_1 = input.LA(2);
				if ( (LA9_1==DOWN) ) {
					int LA9_2 = input.LA(3);
					if ( (LA9_2==ID||LA9_2==MALFORMED_ID) ) {
						alt9=1;
					}
					else if ( (LA9_2==CURRENT_MARK||LA9_2==MARK||LA9_2==SYS_VAR) ) {
						alt9=2;
					}

					else {
						int nvaeMark = input.mark();
						try {
							for (int nvaeConsume = 0; nvaeConsume < 3 - 1; nvaeConsume++) {
								input.consume();
							}
							NoViableAltException nvae =
								new NoViableAltException("", 9, 2, input);
							throw nvae;
						} finally {
							input.rewind(nvaeMark);
						}
					}

				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 9, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 9, 0, input);
				throw nvae;
			}

			switch (alt9) {
				case 1 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:416:6: ^( ASSIGN id expr )
					{
					match(input,ASSIGN,FOLLOW_ASSIGN_in_assign605); 
					match(input, Token.DOWN, null); 
					pushFollow(FOLLOW_id_in_assign607);
					id13=id();
					state._fsp--;

					pushFollow(FOLLOW_expr_in_assign609);
					expr14=expr();
					state._fsp--;

					match(input, Token.UP, null); 


													if (heap.containsVar((id13!=null?((EclTreeWalker.id_return)id13).text:null))) {
													    if (!(id13!=null?((EclTreeWalker.id_return)id13).isMalformed:false)) {
					                                        heap.putVar((id13!=null?((EclTreeWalker.id_return)id13).text:null), expr14);
					                                        if (Value.UNDEFINED.equals(expr14)) {  //error was already reported for expr if its value is undefined, no need to report again for the var
					                                            reportedIds.add((id13!=null?((EclTreeWalker.id_return)id13).text:null));
					                                        } else {
					                                            reportedIds.remove((id13!=null?((EclTreeWalker.id_return)id13).text:null));
					                                        }
													    }
													} else {
														handleError(new ECLSemanticException(ErrorCode.UNDEFINED_VAR, (id13!=null?((EclTreeWalker.id_return)id13).text:null)));
													}
												
					}
					break;
				case 2 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:430:6: ^( ASSIGN sys_id expr )
					{
					match(input,ASSIGN,FOLLOW_ASSIGN_in_assign620); 
					match(input, Token.DOWN, null); 
					pushFollow(FOLLOW_sys_id_in_assign622);
					sys_id15=sys_id();
					state._fsp--;

					pushFollow(FOLLOW_expr_in_assign624);
					expr16=expr();
					state._fsp--;

					match(input, Token.UP, null); 


								                    if (null == modelAccessor) {
													    return;
												    }
													if (null != sys_id15) {
														if (!sys_id15.setValue(expr16)) {
															reportUndefinedRef(sys_id15);
														}
														if (Value.UNDEFINED.equals(expr16)) {  //error was already reported for expr if its value is undefined, no need to report again for the ref
					                                        reportedIds.add(sys_id15);
													    } else {
													        reportedIds.remove(sys_id15);
													    }
													}
												
					}
					break;

			}

				inAssign = false;

		}

					catch (RecognitionException re) {
					    if (validating) {
		                    reportError(re);
		                    recover(input,re);
					    } else {
						    throw re;
						}
					} catch (InterpretationException ie) {
						throw ie;
					} catch (Exception e) {
						throw new InterpretationException("Interpretation failed due to inner error. May be a bug, please report", e);
					}	

		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "assign"



	// $ANTLR start "expr"
	// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:448:1: expr returns [Value value] : ( ^( LAND a= expr b= expr ) | ^( LOR a= expr b= expr ) | ^( GT a= expr b= expr ) | ^( LT a= expr b= expr ) | ^( LE a= expr b= expr ) | ^( GE a= expr b= expr ) | ^( EQ a= expr b= expr ) | ^( NE a= expr b= expr ) | ^( SIN a= expr ) | ^( COS a= expr ) | ^( TAN a= expr ) | ^( ATAN a= expr ) | ^( COT a= expr ) | ^( LN a= expr ) | ^( ABS a= expr ) | ^( ENTIER a= expr ) | ^( SIGN a= expr ) | ^( SQRT a= expr ) | ^( PLUS a= expr b= expr ) | ^( MINUS a= expr b= expr ) | ^( MULT a= expr b= expr ) | ^( DIV a= expr b= expr ) | ^( INVERT a= expr ) | ^( POW a= expr b= expr ) | INT | REAL | EXP | id | STR | TRUE | FALSE | rand | sys_atom );
	public final Value expr() throws RecognitionException {
		Value value = null;


		CommonTree INT17=null;
		CommonTree REAL18=null;
		CommonTree STR20=null;
		Value a =null;
		Value b =null;
		TreeRuleReturnScope id19 =null;
		Value rand21 =null;
		Value sys_atom22 =null;

		try {
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:457:4: ( ^( LAND a= expr b= expr ) | ^( LOR a= expr b= expr ) | ^( GT a= expr b= expr ) | ^( LT a= expr b= expr ) | ^( LE a= expr b= expr ) | ^( GE a= expr b= expr ) | ^( EQ a= expr b= expr ) | ^( NE a= expr b= expr ) | ^( SIN a= expr ) | ^( COS a= expr ) | ^( TAN a= expr ) | ^( ATAN a= expr ) | ^( COT a= expr ) | ^( LN a= expr ) | ^( ABS a= expr ) | ^( ENTIER a= expr ) | ^( SIGN a= expr ) | ^( SQRT a= expr ) | ^( PLUS a= expr b= expr ) | ^( MINUS a= expr b= expr ) | ^( MULT a= expr b= expr ) | ^( DIV a= expr b= expr ) | ^( INVERT a= expr ) | ^( POW a= expr b= expr ) | INT | REAL | EXP | id | STR | TRUE | FALSE | rand | sys_atom )
			int alt10=33;
			switch ( input.LA(1) ) {
			case LAND:
				{
				alt10=1;
				}
				break;
			case LOR:
				{
				alt10=2;
				}
				break;
			case GT:
				{
				alt10=3;
				}
				break;
			case LT:
				{
				alt10=4;
				}
				break;
			case LE:
				{
				alt10=5;
				}
				break;
			case GE:
				{
				alt10=6;
				}
				break;
			case EQ:
				{
				alt10=7;
				}
				break;
			case NE:
				{
				alt10=8;
				}
				break;
			case SIN:
				{
				alt10=9;
				}
				break;
			case COS:
				{
				alt10=10;
				}
				break;
			case TAN:
				{
				alt10=11;
				}
				break;
			case ATAN:
				{
				alt10=12;
				}
				break;
			case COT:
				{
				alt10=13;
				}
				break;
			case LN:
				{
				alt10=14;
				}
				break;
			case ABS:
				{
				alt10=15;
				}
				break;
			case ENTIER:
				{
				alt10=16;
				}
				break;
			case SIGN:
				{
				alt10=17;
				}
				break;
			case SQRT:
				{
				alt10=18;
				}
				break;
			case PLUS:
				{
				alt10=19;
				}
				break;
			case MINUS:
				{
				alt10=20;
				}
				break;
			case MULT:
				{
				alt10=21;
				}
				break;
			case DIV:
				{
				alt10=22;
				}
				break;
			case INVERT:
				{
				alt10=23;
				}
				break;
			case POW:
				{
				alt10=24;
				}
				break;
			case INT:
				{
				alt10=25;
				}
				break;
			case REAL:
				{
				alt10=26;
				}
				break;
			case EXP:
				{
				alt10=27;
				}
				break;
			case ID:
			case MALFORMED_ID:
				{
				alt10=28;
				}
				break;
			case STR:
				{
				alt10=29;
				}
				break;
			case TRUE:
				{
				alt10=30;
				}
				break;
			case FALSE:
				{
				alt10=31;
				}
				break;
			case BINOMIAL:
			case EXPONENTIAL:
			case NORMAL:
			case POISSON:
			case UNIFORM:
				{
				alt10=32;
				}
				break;
			case CURRENT_MARK:
			case IS_MARKED:
			case MARK:
			case SYS_VAR:
			case TIME:
				{
				alt10=33;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 10, 0, input);
				throw nvae;
			}
			switch (alt10) {
				case 1 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:457:6: ^( LAND a= expr b= expr )
					{
					match(input,LAND,FOLLOW_LAND_in_expr654); 
					match(input, Token.DOWN, null); 
					pushFollow(FOLLOW_expr_in_expr658);
					a=expr();
					state._fsp--;

					pushFollow(FOLLOW_expr_in_expr662);
					b=expr();
					state._fsp--;

					match(input, Token.UP, null); 

					value = Calculator.and(a, b);
					}
					break;
				case 2 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:458:6: ^( LOR a= expr b= expr )
					{
					match(input,LOR,FOLLOW_LOR_in_expr673); 
					match(input, Token.DOWN, null); 
					pushFollow(FOLLOW_expr_in_expr677);
					a=expr();
					state._fsp--;

					pushFollow(FOLLOW_expr_in_expr681);
					b=expr();
					state._fsp--;

					match(input, Token.UP, null); 

					value = Calculator.or(a, b);
					}
					break;
				case 3 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:459:6: ^( GT a= expr b= expr )
					{
					match(input,GT,FOLLOW_GT_in_expr692); 
					match(input, Token.DOWN, null); 
					pushFollow(FOLLOW_expr_in_expr696);
					a=expr();
					state._fsp--;

					pushFollow(FOLLOW_expr_in_expr700);
					b=expr();
					state._fsp--;

					match(input, Token.UP, null); 

					value = Calculator.gt(a, b);
					}
					break;
				case 4 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:460:6: ^( LT a= expr b= expr )
					{
					match(input,LT,FOLLOW_LT_in_expr711); 
					match(input, Token.DOWN, null); 
					pushFollow(FOLLOW_expr_in_expr715);
					a=expr();
					state._fsp--;

					pushFollow(FOLLOW_expr_in_expr719);
					b=expr();
					state._fsp--;

					match(input, Token.UP, null); 

					value = Calculator.lt(a, b);
					}
					break;
				case 5 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:461:6: ^( LE a= expr b= expr )
					{
					match(input,LE,FOLLOW_LE_in_expr730); 
					match(input, Token.DOWN, null); 
					pushFollow(FOLLOW_expr_in_expr734);
					a=expr();
					state._fsp--;

					pushFollow(FOLLOW_expr_in_expr738);
					b=expr();
					state._fsp--;

					match(input, Token.UP, null); 

					value = Calculator.le(a, b);
					}
					break;
				case 6 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:462:6: ^( GE a= expr b= expr )
					{
					match(input,GE,FOLLOW_GE_in_expr749); 
					match(input, Token.DOWN, null); 
					pushFollow(FOLLOW_expr_in_expr753);
					a=expr();
					state._fsp--;

					pushFollow(FOLLOW_expr_in_expr757);
					b=expr();
					state._fsp--;

					match(input, Token.UP, null); 

					value = Calculator.ge(a, b);
					}
					break;
				case 7 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:463:6: ^( EQ a= expr b= expr )
					{
					match(input,EQ,FOLLOW_EQ_in_expr768); 
					match(input, Token.DOWN, null); 
					pushFollow(FOLLOW_expr_in_expr772);
					a=expr();
					state._fsp--;

					pushFollow(FOLLOW_expr_in_expr776);
					b=expr();
					state._fsp--;

					match(input, Token.UP, null); 

					value = Calculator.eq(a, b);
					}
					break;
				case 8 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:464:6: ^( NE a= expr b= expr )
					{
					match(input,NE,FOLLOW_NE_in_expr787); 
					match(input, Token.DOWN, null); 
					pushFollow(FOLLOW_expr_in_expr791);
					a=expr();
					state._fsp--;

					pushFollow(FOLLOW_expr_in_expr795);
					b=expr();
					state._fsp--;

					match(input, Token.UP, null); 

					value = Calculator.ne(a, b);
					}
					break;
				case 9 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:465:6: ^( SIN a= expr )
					{
					match(input,SIN,FOLLOW_SIN_in_expr806); 
					match(input, Token.DOWN, null); 
					pushFollow(FOLLOW_expr_in_expr810);
					a=expr();
					state._fsp--;

					match(input, Token.UP, null); 

					value = Calculator.sin(a);
					}
					break;
				case 10 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:466:6: ^( COS a= expr )
					{
					match(input,COS,FOLLOW_COS_in_expr824); 
					match(input, Token.DOWN, null); 
					pushFollow(FOLLOW_expr_in_expr828);
					a=expr();
					state._fsp--;

					match(input, Token.UP, null); 

					value = Calculator.cos(a);
					}
					break;
				case 11 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:467:6: ^( TAN a= expr )
					{
					match(input,TAN,FOLLOW_TAN_in_expr839); 
					match(input, Token.DOWN, null); 
					pushFollow(FOLLOW_expr_in_expr843);
					a=expr();
					state._fsp--;

					match(input, Token.UP, null); 

					value = Calculator.tan(a);
					}
					break;
				case 12 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:468:6: ^( ATAN a= expr )
					{
					match(input,ATAN,FOLLOW_ATAN_in_expr854); 
					match(input, Token.DOWN, null); 
					pushFollow(FOLLOW_expr_in_expr858);
					a=expr();
					state._fsp--;

					match(input, Token.UP, null); 

					value = Calculator.atan(a);
					}
					break;
				case 13 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:469:6: ^( COT a= expr )
					{
					match(input,COT,FOLLOW_COT_in_expr869); 
					match(input, Token.DOWN, null); 
					pushFollow(FOLLOW_expr_in_expr873);
					a=expr();
					state._fsp--;

					match(input, Token.UP, null); 

					value = Calculator.cot(a);
					}
					break;
				case 14 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:470:6: ^( LN a= expr )
					{
					match(input,LN,FOLLOW_LN_in_expr884); 
					match(input, Token.DOWN, null); 
					pushFollow(FOLLOW_expr_in_expr888);
					a=expr();
					state._fsp--;

					match(input, Token.UP, null); 

					value = Calculator.ln(a);
					}
					break;
				case 15 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:471:6: ^( ABS a= expr )
					{
					match(input,ABS,FOLLOW_ABS_in_expr899); 
					match(input, Token.DOWN, null); 
					pushFollow(FOLLOW_expr_in_expr903);
					a=expr();
					state._fsp--;

					match(input, Token.UP, null); 

					value = Calculator.abs(a);
					}
					break;
				case 16 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:472:6: ^( ENTIER a= expr )
					{
					match(input,ENTIER,FOLLOW_ENTIER_in_expr914); 
					match(input, Token.DOWN, null); 
					pushFollow(FOLLOW_expr_in_expr918);
					a=expr();
					state._fsp--;

					match(input, Token.UP, null); 

					value = Calculator.entier(a);
					}
					break;
				case 17 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:473:6: ^( SIGN a= expr )
					{
					match(input,SIGN,FOLLOW_SIGN_in_expr929); 
					match(input, Token.DOWN, null); 
					pushFollow(FOLLOW_expr_in_expr933);
					a=expr();
					state._fsp--;

					match(input, Token.UP, null); 

					value = Calculator.sign(a);
					}
					break;
				case 18 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:474:6: ^( SQRT a= expr )
					{
					match(input,SQRT,FOLLOW_SQRT_in_expr944); 
					match(input, Token.DOWN, null); 
					pushFollow(FOLLOW_expr_in_expr948);
					a=expr();
					state._fsp--;

					match(input, Token.UP, null); 

					value = Calculator.sqrt(a);
					}
					break;
				case 19 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:475:6: ^( PLUS a= expr b= expr )
					{
					match(input,PLUS,FOLLOW_PLUS_in_expr959); 
					match(input, Token.DOWN, null); 
					pushFollow(FOLLOW_expr_in_expr963);
					a=expr();
					state._fsp--;

					pushFollow(FOLLOW_expr_in_expr967);
					b=expr();
					state._fsp--;

					match(input, Token.UP, null); 

					value = Calculator.add(a, b);
					}
					break;
				case 20 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:476:7: ^( MINUS a= expr b= expr )
					{
					match(input,MINUS,FOLLOW_MINUS_in_expr981); 
					match(input, Token.DOWN, null); 
					pushFollow(FOLLOW_expr_in_expr985);
					a=expr();
					state._fsp--;

					pushFollow(FOLLOW_expr_in_expr989);
					b=expr();
					state._fsp--;

					match(input, Token.UP, null); 

					value = Calculator.sub(a, b);
					}
					break;
				case 21 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:477:6: ^( MULT a= expr b= expr )
					{
					match(input,MULT,FOLLOW_MULT_in_expr1002); 
					match(input, Token.DOWN, null); 
					pushFollow(FOLLOW_expr_in_expr1006);
					a=expr();
					state._fsp--;

					pushFollow(FOLLOW_expr_in_expr1010);
					b=expr();
					state._fsp--;

					match(input, Token.UP, null); 

					value = Calculator.multiply(a, b);
					}
					break;
				case 22 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:478:7: ^( DIV a= expr b= expr )
					{
					match(input,DIV,FOLLOW_DIV_in_expr1023); 
					match(input, Token.DOWN, null); 
					pushFollow(FOLLOW_expr_in_expr1027);
					a=expr();
					state._fsp--;

					pushFollow(FOLLOW_expr_in_expr1031);
					b=expr();
					state._fsp--;

					match(input, Token.UP, null); 


								        if (((Double)0d).equals(b.doubleValue())) {
					                        handleError(new ECLSemanticException(ErrorCode.DIV_BY_0));
					                    }
								        value = Calculator.divide(a, b);
								    
					}
					break;
				case 23 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:484:6: ^( INVERT a= expr )
					{
					match(input,INVERT,FOLLOW_INVERT_in_expr1042); 
					match(input, Token.DOWN, null); 
					pushFollow(FOLLOW_expr_in_expr1046);
					a=expr();
					state._fsp--;

					match(input, Token.UP, null); 

					value = Calculator.invert(a);
					}
					break;
				case 24 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:485:6: ^( POW a= expr b= expr )
					{
					match(input,POW,FOLLOW_POW_in_expr1058); 
					match(input, Token.DOWN, null); 
					pushFollow(FOLLOW_expr_in_expr1062);
					a=expr();
					state._fsp--;

					pushFollow(FOLLOW_expr_in_expr1066);
					b=expr();
					state._fsp--;

					match(input, Token.UP, null); 

					value = Calculator.pow(a, b);
					}
					break;
				case 25 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:486:6: INT
					{
					INT17=(CommonTree)match(input,INT,FOLLOW_INT_in_expr1076); 
					value = new Value((INT17!=null?INT17.getText():null));
					}
					break;
				case 26 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:487:6: REAL
					{
					REAL18=(CommonTree)match(input,REAL,FOLLOW_REAL_in_expr1087); 
					value = new Value((REAL18!=null?REAL18.getText():null));
					}
					break;
				case 27 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:488:6: EXP
					{
					match(input,EXP,FOLLOW_EXP_in_expr1096); 
					value = new Value(Math.exp(1));
					}
					break;
				case 28 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:489:6: id
					{
					pushFollow(FOLLOW_id_in_expr1106);
					id19=id();
					state._fsp--;


								        if (!(id19!=null?((EclTreeWalker.id_return)id19).isMalformed:false)) {
											if (inAssign || tokenNo > 1) {
												if (heap.containsVar((id19!=null?((EclTreeWalker.id_return)id19).text:null))) {
													value = heap.getVar((id19!=null?((EclTreeWalker.id_return)id19).text:null));
													if (Value.UNDEFINED.equals(value)) {
														if (reportedIds.contains((id19!=null?((EclTreeWalker.id_return)id19).text:null))) {   //error must be reported only once per variable
															statement_stack.peek().reportUndefined = false;
														} else {
															reportedIds.add((id19!=null?((EclTreeWalker.id_return)id19).text:null));
														}
													}
												} else {
													handleError(new ECLSemanticException(ErrorCode.UNDEFINED_VAR, (id19!=null?((EclTreeWalker.id_return)id19).text:null)));
												}
											} else {
												handleError(new ECLSemanticException(ErrorCode.INVALID_STMT, (id19!=null?((EclTreeWalker.id_return)id19).text:null)));
											}
										}
								    
					}
					break;
				case 29 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:509:6: STR
					{
					STR20=(CommonTree)match(input,STR,FOLLOW_STR_in_expr1116); 
					 value = new Value((STR20!=null?STR20.getText():null), ValType.STRING); 
					}
					break;
				case 30 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:510:8: TRUE
					{
					match(input,TRUE,FOLLOW_TRUE_in_expr1128); 
					 value = new Value(true, ValType.BOOL); 
					}
					break;
				case 31 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:511:8: FALSE
					{
					match(input,FALSE,FOLLOW_FALSE_in_expr1142); 
					 value = new Value(false, ValType.BOOL); 
					}
					break;
				case 32 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:512:6: rand
					{
					pushFollow(FOLLOW_rand_in_expr1153);
					rand21=rand();
					state._fsp--;


								         value = rand21;
								         if (validating) {
								            if (Value.UNDEFINED.equals(rand21) && 0 == statement_stack.peek().warnings.size()) {
											    handleError(new ECLSemanticException(ErrorCode.UNDEFINED_VALUE));
					                        }
					                        value = null; //random values must not be taken into account during the validation stage.
					                    }
								    
					}
					break;
				case 33 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:521:6: sys_atom
					{
					pushFollow(FOLLOW_sys_atom_in_expr1163);
					sys_atom22=sys_atom();
					state._fsp--;

					value = sys_atom22;
					}
					break;

			}

			    if (validating && null == value) { //means that calculations were not performed
					value = Value.UNDEFINED;
				} else if (Value.UNDEFINED.equals(value) && statement_stack.peek().reportUndefined && 0 == statement_stack.peek().warnings.size()) {  //'undefined value' error must be reported only if there are no other errors found on that line
			        handleError(new ECLSemanticException(ErrorCode.UNDEFINED_VALUE));
			        statement_stack.peek().reportUndefined = false;
			    }

		}

					catch (RecognitionException re) {
					    if (validating) {
		                    reportError(re);
		                    recover(input,re);
					    } else {
						    throw re;
						}
					} catch (InterpretationException ie) {
						throw ie;
					} catch (Exception e) {
						throw new InterpretationException("Interpretation failed due to inner error. May be a bug, please report", e);
					}	

		finally {
			// do for sure before leaving
		}
		return value;
	}
	// $ANTLR end "expr"


	public static class id_return extends TreeRuleReturnScope {
		public String text;
		public boolean isMalformed;
	};


	// $ANTLR start "id"
	// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:524:1: id returns [String text, boolean isMalformed] : ( ID | MALFORMED_ID );
	public final EclTreeWalker.id_return id() throws RecognitionException {
		EclTreeWalker.id_return retval = new EclTreeWalker.id_return();
		retval.start = input.LT(1);

		CommonTree ID23=null;
		CommonTree MALFORMED_ID24=null;

		try {
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:525:4: ( ID | MALFORMED_ID )
			int alt11=2;
			int LA11_0 = input.LA(1);
			if ( (LA11_0==ID) ) {
				alt11=1;
			}
			else if ( (LA11_0==MALFORMED_ID) ) {
				alt11=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 11, 0, input);
				throw nvae;
			}

			switch (alt11) {
				case 1 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:525:6: ID
					{
					ID23=(CommonTree)match(input,ID,FOLLOW_ID_in_id1186); 
					retval.text = (ID23!=null?ID23.getText():null);
					}
					break;
				case 2 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:526:6: MALFORMED_ID
					{
					MALFORMED_ID24=(CommonTree)match(input,MALFORMED_ID,FOLLOW_MALFORMED_ID_in_id1195); 

											retval.isMalformed = true;
											ECLSemanticException e = new ECLSemanticException(ErrorCode.INVALID_INPUT, MALFORMED_ID24.getLine(),
													MALFORMED_ID24.getCharPositionInLine(), MALFORMED_ID24.getLine(), MALFORMED_ID24.getCharPositionInLine() +
													(MALFORMED_ID24.getText() == null ? 0 : MALFORMED_ID24.getText().length()-1));
											e.token = MALFORMED_ID24.getToken();
											e.setFullyInitialized(true);
											handleError(e);
											
					}
					break;

			}
		}

					catch (RecognitionException re) {
					    if (validating) {
		                    reportError(re);
		                    recover(input,re);
					    } else {
						    throw re;
						}
					} catch (InterpretationException ie) {
						throw ie;
					} catch (Exception e) {
						throw new InterpretationException("Interpretation failed due to inner error. May be a bug, please report", e);
					}	

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "id"



	// $ANTLR start "rand"
	// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:538:1: rand returns [Value value] : ( ^( UNIFORM a= expr b= expr ) | ^( POISSON a= expr ) | ^( EXPONENTIAL a= expr ) | ^( NORMAL a= expr b= expr ) | ^( BINOMIAL a= expr b= expr ) );
	public final Value rand() throws RecognitionException {
		Value value = null;


		Value a =null;
		Value b =null;

		try {
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:539:4: ( ^( UNIFORM a= expr b= expr ) | ^( POISSON a= expr ) | ^( EXPONENTIAL a= expr ) | ^( NORMAL a= expr b= expr ) | ^( BINOMIAL a= expr b= expr ) )
			int alt12=5;
			switch ( input.LA(1) ) {
			case UNIFORM:
				{
				alt12=1;
				}
				break;
			case POISSON:
				{
				alt12=2;
				}
				break;
			case EXPONENTIAL:
				{
				alt12=3;
				}
				break;
			case NORMAL:
				{
				alt12=4;
				}
				break;
			case BINOMIAL:
				{
				alt12=5;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 12, 0, input);
				throw nvae;
			}
			switch (alt12) {
				case 1 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:539:6: ^( UNIFORM a= expr b= expr )
					{
					match(input,UNIFORM,FOLLOW_UNIFORM_in_rand1217); 
					match(input, Token.DOWN, null); 
					pushFollow(FOLLOW_expr_in_rand1221);
					a=expr();
					state._fsp--;

					pushFollow(FOLLOW_expr_in_rand1225);
					b=expr();
					state._fsp--;

					match(input, Token.UP, null); 

					value = randomData.nextUniform(a, b);
					}
					break;
				case 2 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:540:6: ^( POISSON a= expr )
					{
					match(input,POISSON,FOLLOW_POISSON_in_rand1237); 
					match(input, Token.DOWN, null); 
					pushFollow(FOLLOW_expr_in_rand1241);
					a=expr();
					state._fsp--;

					match(input, Token.UP, null); 

					value = randomData.nextPoisson(a);
					}
					break;
				case 3 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:541:6: ^( EXPONENTIAL a= expr )
					{
					match(input,EXPONENTIAL,FOLLOW_EXPONENTIAL_in_rand1256); 
					match(input, Token.DOWN, null); 
					pushFollow(FOLLOW_expr_in_rand1260);
					a=expr();
					state._fsp--;

					match(input, Token.UP, null); 

					value = randomData.nextExponential(a);
					}
					break;
				case 4 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:542:6: ^( NORMAL a= expr b= expr )
					{
					match(input,NORMAL,FOLLOW_NORMAL_in_rand1273); 
					match(input, Token.DOWN, null); 
					pushFollow(FOLLOW_expr_in_rand1277);
					a=expr();
					state._fsp--;

					pushFollow(FOLLOW_expr_in_rand1281);
					b=expr();
					state._fsp--;

					match(input, Token.UP, null); 

					value = randomData.nextNormal(a, b);
					}
					break;
				case 5 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:543:6: ^( BINOMIAL a= expr b= expr )
					{
					match(input,BINOMIAL,FOLLOW_BINOMIAL_in_rand1293); 
					match(input, Token.DOWN, null); 
					pushFollow(FOLLOW_expr_in_rand1297);
					a=expr();
					state._fsp--;

					pushFollow(FOLLOW_expr_in_rand1301);
					b=expr();
					state._fsp--;

					match(input, Token.UP, null); 

					value = randomData.nextBinomial(a, b);
					}
					break;

			}
		}

					catch (RecognitionException re) {
					    if (validating) {
		                    reportError(re);
		                    recover(input,re);
					    } else {
						    throw re;
						}
					} catch (InterpretationException ie) {
						throw ie;
					} catch (Exception e) {
						throw new InterpretationException("Interpretation failed due to inner error. May be a bug, please report", e);
					}	

		finally {
			// do for sure before leaving
		}
		return value;
	}
	// $ANTLR end "rand"


	protected static class sys_atom_scope {
		EcliAggregate agg;
	}
	protected Stack<sys_atom_scope> sys_atom_stack = new Stack<sys_atom_scope>();


	// $ANTLR start "sys_atom"
	// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:546:1: sys_atom returns [Value value] : ( ^( IS_MARKED aggregate expr ) | sys_id | TIME );
	public final Value sys_atom() throws RecognitionException {
		sys_atom_stack.push(new sys_atom_scope());
		Value value = null;


		TreeRuleReturnScope aggregate25 =null;
		Value expr26 =null;
		OuterRef.RefDecorator sys_id27 =null;


			if (!validating && null == this.modelAccessor) {
				throw new InterpretationException("Model accessor is required for proper interpretation, null value was supplied!");
			}
			sys_atom_stack.peek().agg = modelAccessor;

		try {
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:556:4: ( ^( IS_MARKED aggregate expr ) | sys_id | TIME )
			int alt13=3;
			switch ( input.LA(1) ) {
			case IS_MARKED:
				{
				alt13=1;
				}
				break;
			case CURRENT_MARK:
			case MARK:
			case SYS_VAR:
				{
				alt13=2;
				}
				break;
			case TIME:
				{
				alt13=3;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 13, 0, input);
				throw nvae;
			}
			switch (alt13) {
				case 1 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:556:6: ^( IS_MARKED aggregate expr )
					{
					match(input,IS_MARKED,FOLLOW_IS_MARKED_in_sys_atom1337); 
					match(input, Token.DOWN, null); 
					pushFollow(FOLLOW_aggregate_in_sys_atom1339);
					aggregate25=aggregate();
					state._fsp--;

					pushFollow(FOLLOW_expr_in_sys_atom1341);
					expr26=expr();
					state._fsp--;

					match(input, Token.UP, null); 

					 
										sys_atom_stack.peek().agg = (aggregate25!=null?((EclTreeWalker.aggregate_return)aggregate25).agg:null);
										if (null != sys_atom_stack.peek().agg) {
					                        if (!sys_atom_stack.peek().agg.containsPlace(expr26.getValue())) {
					                            handleError(new ECLSemanticException("MissingPlace", ErrorCode.INVALID_REFERENCE, expr26.toString()));
					                        }
					                        value = new Value(sys_atom_stack.peek().agg.isPlaceMarked(expr26.getValue()));
					                    }
									
					}
					break;
				case 2 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:565:6: sys_id
					{
					pushFollow(FOLLOW_sys_id_in_sys_atom1351);
					sys_id27=sys_id();
					state._fsp--;

					 
										if (null == sys_id27) {
											value = validating? null : Value.UNDEFINED;
										} else {
											if (!sys_id27.isDefined()) {
												reportUndefinedRef(sys_id27);
											} else {
												value = new Value(sys_id27.getValue());
												if (Value.UNDEFINED.equals(value)) {
					                                if (reportedIds.contains(sys_id27)) {   //error must be reported only once per ref
					                                    statement_stack.peek().reportUndefined = false;
					                                } else {
					                                    reportedIds.add(sys_id27);
					                                }
					                            }
											}
										}
									
					}
					break;
				case 3 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:583:6: TIME
					{
					match(input,TIME,FOLLOW_TIME_in_sys_atom1360); 

										if (null != modelAccessor) {
											value = new Value(modelAccessor.getTime());
										}
									
					}
					break;

			}
		}

					catch (RecognitionException re) {
					    if (validating) {
		                    reportError(re);
		                    recover(input,re);
					    } else {
						    throw re;
						}
					} catch (InterpretationException ie) {
						throw ie;
					} catch (Exception e) {
						throw new InterpretationException("Interpretation failed due to inner error. May be a bug, please report", e);
					}	

		finally {
			// do for sure before leaving
			sys_atom_stack.pop();
		}
		return value;
	}
	// $ANTLR end "sys_atom"


	protected static class sys_id_scope {
		EcliAggregate agg;
	}
	protected Stack<sys_id_scope> sys_id_stack = new Stack<sys_id_scope>();


	// $ANTLR start "sys_id"
	// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:592:1: sys_id returns [OuterRef.RefDecorator ref] : ( ^( SYS_VAR aggregate expr ) | ^( MARK aggregate p= expr a= expr ) | ^( CURRENT_MARK a= expr ) );
	public final OuterRef.RefDecorator sys_id() throws RecognitionException {
		sys_id_stack.push(new sys_id_scope());
		OuterRef.RefDecorator ref = null;


		Value p =null;
		Value a =null;
		TreeRuleReturnScope aggregate28 =null;
		Value expr29 =null;
		TreeRuleReturnScope aggregate30 =null;


			if (null == this.modelAccessor) {
				throw new InterpretationException("Model accessor is required for proper interpretation, null value was supplied!");
			}
			sys_id_stack.peek().agg = modelAccessor.getCurrentAggregate();

		try {
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:602:4: ( ^( SYS_VAR aggregate expr ) | ^( MARK aggregate p= expr a= expr ) | ^( CURRENT_MARK a= expr ) )
			int alt14=3;
			switch ( input.LA(1) ) {
			case SYS_VAR:
				{
				alt14=1;
				}
				break;
			case MARK:
				{
				alt14=2;
				}
				break;
			case CURRENT_MARK:
				{
				alt14=3;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 14, 0, input);
				throw nvae;
			}
			switch (alt14) {
				case 1 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:602:6: ^( SYS_VAR aggregate expr )
					{
					match(input,SYS_VAR,FOLLOW_SYS_VAR_in_sys_id1395); 
					match(input, Token.DOWN, null); 
					pushFollow(FOLLOW_aggregate_in_sys_id1397);
					aggregate28=aggregate();
					state._fsp--;

					pushFollow(FOLLOW_expr_in_sys_id1399);
					expr29=expr();
					state._fsp--;

					match(input, Token.UP, null); 


											sys_id_stack.peek().agg = (aggregate28!=null?((EclTreeWalker.aggregate_return)aggregate28).agg:null);
											if (null != sys_id_stack.peek().agg) {
											    ref = OuterRef.createVarDecorator(sys_id_stack.peek().agg, expr29.getValue(), (aggregate28!=null?((EclTreeWalker.aggregate_return)aggregate28).uid:null).append('\0').append(expr29).toString());
											}
										
					}
					break;
				case 2 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:608:6: ^( MARK aggregate p= expr a= expr )
					{
					match(input,MARK,FOLLOW_MARK_in_sys_id1411); 
					match(input, Token.DOWN, null); 
					pushFollow(FOLLOW_aggregate_in_sys_id1413);
					aggregate30=aggregate();
					state._fsp--;

					pushFollow(FOLLOW_expr_in_sys_id1417);
					p=expr();
					state._fsp--;

					pushFollow(FOLLOW_expr_in_sys_id1421);
					a=expr();
					state._fsp--;

					match(input, Token.UP, null); 


											sys_id_stack.peek().agg = (aggregate30!=null?((EclTreeWalker.aggregate_return)aggregate30).agg:null);
											if (null != sys_id_stack.peek().agg) {
					                            if (!sys_id_stack.peek().agg.containsPlace(p.getValue())) {
					                                 handleError(new ECLSemanticException("MissingPlace", ErrorCode.INVALID_REFERENCE, p.toString()));
					                            } else if (sys_id_stack.peek().agg.isPlaceMarked(p.getValue())) {
					                                    ref = OuterRef.createAttrDecorator(sys_id_stack.peek().agg.getTokenAt(p.getValue()), a.getValue(),
					                                            (aggregate30!=null?((EclTreeWalker.aggregate_return)aggregate30).uid:null).append('\0').append(p).append('\0').append(a).toString());
					                            } else {
													if (validating) {
														handleError(new ECLSemanticException("MissingAttribute", ErrorCode.INVALID_REFERENCE, a.toString()));
													} else {
														handleError(new ECLSemanticException(ErrorCode.UNMARKED_PLACE, p.getValue().toString()));
					                                }
													ref = null;
					                            }
					                        }
										
					}
					break;
				case 3 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:626:6: ^( CURRENT_MARK a= expr )
					{
					match(input,CURRENT_MARK,FOLLOW_CURRENT_MARK_in_sys_id1432); 
					match(input, Token.DOWN, null); 
					pushFollow(FOLLOW_expr_in_sys_id1436);
					a=expr();
					state._fsp--;

					match(input, Token.UP, null); 


												EcliToken currentToken = modelAccessor.getCurrentToken();
												if (null != currentToken) {
													ref = OuterRef.createAttrDecorator(currentToken, a.getValue(),
					                                        new StringBuilder().append('\t').append("root").append('\t').append(a).toString());
					                           
												} else if (!validating) {
													handleError(new ECLSemanticException("CurrentTokenUndefined", ErrorCode.INVALID_REFERENCE));
												}
											
					}
					break;

			}
		}

					catch (RecognitionException re) {
					    if (validating) {
		                    reportError(re);
		                    recover(input,re);
					    } else {
						    throw re;
						}
					} catch (InterpretationException ie) {
						throw ie;
					} catch (Exception e) {
						throw new InterpretationException("Interpretation failed due to inner error. May be a bug, please report", e);
					}	

		finally {
			// do for sure before leaving
			sys_id_stack.pop();
		}
		return ref;
	}
	// $ANTLR end "sys_id"


	public static class aggregate_return extends TreeRuleReturnScope {
		public EcliAggregate agg;
		public StringBuilder uid;
	};


	// $ANTLR start "aggregate"
	// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:642:1: aggregate returns [EcliAggregate agg, StringBuilder uid] : ( ROOT_AGG )? ( AGGREGATE expr )* ;
	public final EclTreeWalker.aggregate_return aggregate() throws RecognitionException {
		EclTreeWalker.aggregate_return retval = new EclTreeWalker.aggregate_return();
		retval.start = input.LT(1);

		Value expr31 =null;


			retval.agg = null;
			retval.uid = new StringBuilder();

		try {
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:647:4: ( ( ROOT_AGG )? ( AGGREGATE expr )* )
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:647:6: ( ROOT_AGG )? ( AGGREGATE expr )*
			{
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:647:6: ( ROOT_AGG )?
			int alt15=2;
			int LA15_0 = input.LA(1);
			if ( (LA15_0==ROOT_AGG) ) {
				alt15=1;
			}
			switch (alt15) {
				case 1 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:647:7: ROOT_AGG
					{
					match(input,ROOT_AGG,FOLLOW_ROOT_AGG_in_aggregate1468); 

										retval.agg = modelAccessor.getRootAggregate();
										if (retval.agg == null) {
											handleError(new ECLSemanticException("RootUndefined", ErrorCode.INVALID_REFERENCE));
										}
										retval.uid.append('\t').append("rootagg"); //using '/t' as a delimiter to avoid ambiguity with root and non-root aggregate refs (ROOT != a['rootagg'])
									
					}
					break;

			}


								if (retval.agg == null && (retval.agg = modelAccessor.getCurrentAggregate()) == null) {
										handleError(new ECLSemanticException("CurrentAggregateUndefined", ErrorCode.INVALID_REFERENCE));
								}
							
			// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:660:5: ( AGGREGATE expr )*
			loop16:
			while (true) {
				int alt16=2;
				int LA16_0 = input.LA(1);
				if ( (LA16_0==AGGREGATE) ) {
					alt16=1;
				}

				switch (alt16) {
				case 1 :
					// ua\\cn\\stu\\cs\\ems\\ecli\\impl\\EclTreeWalker.g:660:6: AGGREGATE expr
					{
					match(input,AGGREGATE,FOLLOW_AGGREGATE_in_aggregate1490); 
					pushFollow(FOLLOW_expr_in_aggregate1492);
					expr31=expr();
					state._fsp--;


											if (!retval.agg.containsAggregate(expr31.getValue())) {
												handleError(new ECLSemanticException("MissingAggregate", ErrorCode.INVALID_REFERENCE, expr31.toString()));
					                            retval.agg = null;
											} else {
											    retval.uid.append('\0').append(expr31);  //using '/0' as a delimiter to avoid ambiguity (a['1'].a['134'] != a['11'].a['34'])
												retval.agg = retval.agg.getAggregate(expr31.getValue());
											}
										
					}
					break;

				default :
					break loop16;
				}
			}

			}

		}

					catch (RecognitionException re) {
					    if (validating) {
		                    reportError(re);
		                    recover(input,re);
					    } else {
						    throw re;
						}
					} catch (InterpretationException ie) {
						throw ie;
					} catch (Exception e) {
						throw new InterpretationException("Interpretation failed due to inner error. May be a bug, please report", e);
					}	

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "aggregate"

	// Delegated rules



	public static final BitSet FOLLOW_ecl_in_interpret75 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ecl_in_validate101 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_statement_in_ecl112 = new BitSet(new long[]{0x0200000008000002L});
	public static final BitSet FOLLOW_STMT_BEGIN_in_statement158 = new BitSet(new long[]{0xF9EF9FF7C3F6B9D0L,0x0000000000000005L});
	public static final BitSet FOLLOW_stmt_in_statement167 = new BitSet(new long[]{0x0400000000000000L});
	public static final BitSet FOLLOW_STMT_END_in_statement169 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IF_BEGIN_in_statement178 = new BitSet(new long[]{0x0000000020000000L});
	public static final BitSet FOLLOW_if_stmt_in_statement186 = new BitSet(new long[]{0x0000000010000000L});
	public static final BitSet FOLLOW_IF_END_in_statement194 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_standalone_expr_in_stmt239 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_assign_in_stmt246 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_declar_in_stmt253 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_seed_stmt_in_stmt260 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ret_in_stmt271 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_SEED_in_seed_stmt290 = new BitSet(new long[]{0x0000000000000004L});
	public static final BitSet FOLLOW_expr_in_seed_stmt294 = new BitSet(new long[]{0x0000000000000008L});
	public static final BitSet FOLLOW_expr_in_standalone_expr315 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_VAR_DECL_in_declar333 = new BitSet(new long[]{0x0000000000000004L});
	public static final BitSet FOLLOW_id_in_declar335 = new BitSet(new long[]{0xF9C79FF7C3F6B998L,0x0000000000000001L});
	public static final BitSet FOLLOW_expr_in_declar337 = new BitSet(new long[]{0x0000000000000008L});
	public static final BitSet FOLLOW_RETURN_in_ret366 = new BitSet(new long[]{0x0000000000000004L});
	public static final BitSet FOLLOW_expr_in_ret368 = new BitSet(new long[]{0x0000000000000008L});
	public static final BitSet FOLLOW_RETURN_in_ret378 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IF_STMT_in_if_stmt414 = new BitSet(new long[]{0x0000000000000004L});
	public static final BitSet FOLLOW_expr_in_if_stmt416 = new BitSet(new long[]{0x0200000008000200L});
	public static final BitSet FOLLOW_if_block_in_if_stmt427 = new BitSet(new long[]{0x0200000008000208L});
	public static final BitSet FOLLOW_if_block_in_if_stmt439 = new BitSet(new long[]{0x0000000000000008L});
	public static final BitSet FOLLOW_statement_in_if_block483 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_statement_block_in_if_block493 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_BLOCK_in_statement_block533 = new BitSet(new long[]{0x0000000000000004L});
	public static final BitSet FOLLOW_statement_in_statement_block536 = new BitSet(new long[]{0x0200000008000008L});
	public static final BitSet FOLLOW_ASSIGN_in_assign605 = new BitSet(new long[]{0x0000000000000004L});
	public static final BitSet FOLLOW_id_in_assign607 = new BitSet(new long[]{0xF9C79FF7C3F6B990L,0x0000000000000001L});
	public static final BitSet FOLLOW_expr_in_assign609 = new BitSet(new long[]{0x0000000000000008L});
	public static final BitSet FOLLOW_ASSIGN_in_assign620 = new BitSet(new long[]{0x0000000000000004L});
	public static final BitSet FOLLOW_sys_id_in_assign622 = new BitSet(new long[]{0xF9C79FF7C3F6B990L,0x0000000000000001L});
	public static final BitSet FOLLOW_expr_in_assign624 = new BitSet(new long[]{0x0000000000000008L});
	public static final BitSet FOLLOW_LAND_in_expr654 = new BitSet(new long[]{0x0000000000000004L});
	public static final BitSet FOLLOW_expr_in_expr658 = new BitSet(new long[]{0xF9C79FF7C3F6B990L,0x0000000000000001L});
	public static final BitSet FOLLOW_expr_in_expr662 = new BitSet(new long[]{0x0000000000000008L});
	public static final BitSet FOLLOW_LOR_in_expr673 = new BitSet(new long[]{0x0000000000000004L});
	public static final BitSet FOLLOW_expr_in_expr677 = new BitSet(new long[]{0xF9C79FF7C3F6B990L,0x0000000000000001L});
	public static final BitSet FOLLOW_expr_in_expr681 = new BitSet(new long[]{0x0000000000000008L});
	public static final BitSet FOLLOW_GT_in_expr692 = new BitSet(new long[]{0x0000000000000004L});
	public static final BitSet FOLLOW_expr_in_expr696 = new BitSet(new long[]{0xF9C79FF7C3F6B990L,0x0000000000000001L});
	public static final BitSet FOLLOW_expr_in_expr700 = new BitSet(new long[]{0x0000000000000008L});
	public static final BitSet FOLLOW_LT_in_expr711 = new BitSet(new long[]{0x0000000000000004L});
	public static final BitSet FOLLOW_expr_in_expr715 = new BitSet(new long[]{0xF9C79FF7C3F6B990L,0x0000000000000001L});
	public static final BitSet FOLLOW_expr_in_expr719 = new BitSet(new long[]{0x0000000000000008L});
	public static final BitSet FOLLOW_LE_in_expr730 = new BitSet(new long[]{0x0000000000000004L});
	public static final BitSet FOLLOW_expr_in_expr734 = new BitSet(new long[]{0xF9C79FF7C3F6B990L,0x0000000000000001L});
	public static final BitSet FOLLOW_expr_in_expr738 = new BitSet(new long[]{0x0000000000000008L});
	public static final BitSet FOLLOW_GE_in_expr749 = new BitSet(new long[]{0x0000000000000004L});
	public static final BitSet FOLLOW_expr_in_expr753 = new BitSet(new long[]{0xF9C79FF7C3F6B990L,0x0000000000000001L});
	public static final BitSet FOLLOW_expr_in_expr757 = new BitSet(new long[]{0x0000000000000008L});
	public static final BitSet FOLLOW_EQ_in_expr768 = new BitSet(new long[]{0x0000000000000004L});
	public static final BitSet FOLLOW_expr_in_expr772 = new BitSet(new long[]{0xF9C79FF7C3F6B990L,0x0000000000000001L});
	public static final BitSet FOLLOW_expr_in_expr776 = new BitSet(new long[]{0x0000000000000008L});
	public static final BitSet FOLLOW_NE_in_expr787 = new BitSet(new long[]{0x0000000000000004L});
	public static final BitSet FOLLOW_expr_in_expr791 = new BitSet(new long[]{0xF9C79FF7C3F6B990L,0x0000000000000001L});
	public static final BitSet FOLLOW_expr_in_expr795 = new BitSet(new long[]{0x0000000000000008L});
	public static final BitSet FOLLOW_SIN_in_expr806 = new BitSet(new long[]{0x0000000000000004L});
	public static final BitSet FOLLOW_expr_in_expr810 = new BitSet(new long[]{0x0000000000000008L});
	public static final BitSet FOLLOW_COS_in_expr824 = new BitSet(new long[]{0x0000000000000004L});
	public static final BitSet FOLLOW_expr_in_expr828 = new BitSet(new long[]{0x0000000000000008L});
	public static final BitSet FOLLOW_TAN_in_expr839 = new BitSet(new long[]{0x0000000000000004L});
	public static final BitSet FOLLOW_expr_in_expr843 = new BitSet(new long[]{0x0000000000000008L});
	public static final BitSet FOLLOW_ATAN_in_expr854 = new BitSet(new long[]{0x0000000000000004L});
	public static final BitSet FOLLOW_expr_in_expr858 = new BitSet(new long[]{0x0000000000000008L});
	public static final BitSet FOLLOW_COT_in_expr869 = new BitSet(new long[]{0x0000000000000004L});
	public static final BitSet FOLLOW_expr_in_expr873 = new BitSet(new long[]{0x0000000000000008L});
	public static final BitSet FOLLOW_LN_in_expr884 = new BitSet(new long[]{0x0000000000000004L});
	public static final BitSet FOLLOW_expr_in_expr888 = new BitSet(new long[]{0x0000000000000008L});
	public static final BitSet FOLLOW_ABS_in_expr899 = new BitSet(new long[]{0x0000000000000004L});
	public static final BitSet FOLLOW_expr_in_expr903 = new BitSet(new long[]{0x0000000000000008L});
	public static final BitSet FOLLOW_ENTIER_in_expr914 = new BitSet(new long[]{0x0000000000000004L});
	public static final BitSet FOLLOW_expr_in_expr918 = new BitSet(new long[]{0x0000000000000008L});
	public static final BitSet FOLLOW_SIGN_in_expr929 = new BitSet(new long[]{0x0000000000000004L});
	public static final BitSet FOLLOW_expr_in_expr933 = new BitSet(new long[]{0x0000000000000008L});
	public static final BitSet FOLLOW_SQRT_in_expr944 = new BitSet(new long[]{0x0000000000000004L});
	public static final BitSet FOLLOW_expr_in_expr948 = new BitSet(new long[]{0x0000000000000008L});
	public static final BitSet FOLLOW_PLUS_in_expr959 = new BitSet(new long[]{0x0000000000000004L});
	public static final BitSet FOLLOW_expr_in_expr963 = new BitSet(new long[]{0xF9C79FF7C3F6B990L,0x0000000000000001L});
	public static final BitSet FOLLOW_expr_in_expr967 = new BitSet(new long[]{0x0000000000000008L});
	public static final BitSet FOLLOW_MINUS_in_expr981 = new BitSet(new long[]{0x0000000000000004L});
	public static final BitSet FOLLOW_expr_in_expr985 = new BitSet(new long[]{0xF9C79FF7C3F6B990L,0x0000000000000001L});
	public static final BitSet FOLLOW_expr_in_expr989 = new BitSet(new long[]{0x0000000000000008L});
	public static final BitSet FOLLOW_MULT_in_expr1002 = new BitSet(new long[]{0x0000000000000004L});
	public static final BitSet FOLLOW_expr_in_expr1006 = new BitSet(new long[]{0xF9C79FF7C3F6B990L,0x0000000000000001L});
	public static final BitSet FOLLOW_expr_in_expr1010 = new BitSet(new long[]{0x0000000000000008L});
	public static final BitSet FOLLOW_DIV_in_expr1023 = new BitSet(new long[]{0x0000000000000004L});
	public static final BitSet FOLLOW_expr_in_expr1027 = new BitSet(new long[]{0xF9C79FF7C3F6B990L,0x0000000000000001L});
	public static final BitSet FOLLOW_expr_in_expr1031 = new BitSet(new long[]{0x0000000000000008L});
	public static final BitSet FOLLOW_INVERT_in_expr1042 = new BitSet(new long[]{0x0000000000000004L});
	public static final BitSet FOLLOW_expr_in_expr1046 = new BitSet(new long[]{0x0000000000000008L});
	public static final BitSet FOLLOW_POW_in_expr1058 = new BitSet(new long[]{0x0000000000000004L});
	public static final BitSet FOLLOW_expr_in_expr1062 = new BitSet(new long[]{0xF9C79FF7C3F6B990L,0x0000000000000001L});
	public static final BitSet FOLLOW_expr_in_expr1066 = new BitSet(new long[]{0x0000000000000008L});
	public static final BitSet FOLLOW_INT_in_expr1076 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_REAL_in_expr1087 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_EXP_in_expr1096 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_id_in_expr1106 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_STR_in_expr1116 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_TRUE_in_expr1128 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_FALSE_in_expr1142 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_rand_in_expr1153 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_sys_atom_in_expr1163 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ID_in_id1186 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_MALFORMED_ID_in_id1195 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_UNIFORM_in_rand1217 = new BitSet(new long[]{0x0000000000000004L});
	public static final BitSet FOLLOW_expr_in_rand1221 = new BitSet(new long[]{0xF9C79FF7C3F6B990L,0x0000000000000001L});
	public static final BitSet FOLLOW_expr_in_rand1225 = new BitSet(new long[]{0x0000000000000008L});
	public static final BitSet FOLLOW_POISSON_in_rand1237 = new BitSet(new long[]{0x0000000000000004L});
	public static final BitSet FOLLOW_expr_in_rand1241 = new BitSet(new long[]{0x0000000000000008L});
	public static final BitSet FOLLOW_EXPONENTIAL_in_rand1256 = new BitSet(new long[]{0x0000000000000004L});
	public static final BitSet FOLLOW_expr_in_rand1260 = new BitSet(new long[]{0x0000000000000008L});
	public static final BitSet FOLLOW_NORMAL_in_rand1273 = new BitSet(new long[]{0x0000000000000004L});
	public static final BitSet FOLLOW_expr_in_rand1277 = new BitSet(new long[]{0xF9C79FF7C3F6B990L,0x0000000000000001L});
	public static final BitSet FOLLOW_expr_in_rand1281 = new BitSet(new long[]{0x0000000000000008L});
	public static final BitSet FOLLOW_BINOMIAL_in_rand1293 = new BitSet(new long[]{0x0000000000000004L});
	public static final BitSet FOLLOW_expr_in_rand1297 = new BitSet(new long[]{0xF9C79FF7C3F6B990L,0x0000000000000001L});
	public static final BitSet FOLLOW_expr_in_rand1301 = new BitSet(new long[]{0x0000000000000008L});
	public static final BitSet FOLLOW_IS_MARKED_in_sys_atom1337 = new BitSet(new long[]{0x0000000000000004L});
	public static final BitSet FOLLOW_aggregate_in_sys_atom1339 = new BitSet(new long[]{0xF9C79FF7C3F6B990L,0x0000000000000001L});
	public static final BitSet FOLLOW_expr_in_sys_atom1341 = new BitSet(new long[]{0x0000000000000008L});
	public static final BitSet FOLLOW_sys_id_in_sys_atom1351 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_TIME_in_sys_atom1360 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_SYS_VAR_in_sys_id1395 = new BitSet(new long[]{0x0000000000000004L});
	public static final BitSet FOLLOW_aggregate_in_sys_id1397 = new BitSet(new long[]{0xF9C79FF7C3F6B990L,0x0000000000000001L});
	public static final BitSet FOLLOW_expr_in_sys_id1399 = new BitSet(new long[]{0x0000000000000008L});
	public static final BitSet FOLLOW_MARK_in_sys_id1411 = new BitSet(new long[]{0x0000000000000004L});
	public static final BitSet FOLLOW_aggregate_in_sys_id1413 = new BitSet(new long[]{0xF9C79FF7C3F6B990L,0x0000000000000001L});
	public static final BitSet FOLLOW_expr_in_sys_id1417 = new BitSet(new long[]{0xF9C79FF7C3F6B990L,0x0000000000000001L});
	public static final BitSet FOLLOW_expr_in_sys_id1421 = new BitSet(new long[]{0x0000000000000008L});
	public static final BitSet FOLLOW_CURRENT_MARK_in_sys_id1432 = new BitSet(new long[]{0x0000000000000004L});
	public static final BitSet FOLLOW_expr_in_sys_id1436 = new BitSet(new long[]{0x0000000000000008L});
	public static final BitSet FOLLOW_ROOT_AGG_in_aggregate1468 = new BitSet(new long[]{0x0000000000000022L});
	public static final BitSet FOLLOW_AGGREGATE_in_aggregate1490 = new BitSet(new long[]{0xF9C79FF7C3F6B990L,0x0000000000000001L});
	public static final BitSet FOLLOW_expr_in_aggregate1492 = new BitSet(new long[]{0x0000000000000022L});
}
