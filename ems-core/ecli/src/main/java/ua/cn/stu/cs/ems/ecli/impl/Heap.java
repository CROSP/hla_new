package ua.cn.stu.cs.ems.ecli.impl;

import ua.cn.stu.cs.ems.ecli.Value;

import java.util.HashMap;
import java.util.Map;

/**
 * User: n0weak
 */
class Heap {

    private Map<String, Value> memory = new HashMap<String, Value>();

    public void putVar(String name, Value value) {
        memory.put(name, value);
    }

    public boolean containsVar(String name) {
        return memory.containsKey(name);
    }

    public Value getVar(String name) {
        return memory.get(name);
    }

    public void removeVar(String name) {
        memory.remove(name);
    }

    public void clear() {
        memory.clear();
    }

}
