package ua.cn.stu.cs.ems.ecli.errors;

/**
 * @author n0weak
 */
public class ValidationException extends InterpretationException {
    private ParsingError error;

    public ValidationException(ParsingError error) {
        this.error = error;
    }

    public ParsingError getParsingError() {
        return error;
    }

    public String toString() {
        return error.getMessage();
    }

    public String getMessage() {
        return error.getMessage();
    }

}
