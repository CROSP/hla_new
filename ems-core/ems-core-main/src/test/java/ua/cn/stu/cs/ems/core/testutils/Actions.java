package ua.cn.stu.cs.ems.core.testutils;

import org.hamcrest.Description;
import org.jmock.api.Action;
import org.jmock.api.Invocation;
import ua.cn.stu.cs.ems.core.ActiveInstance;
import ua.cn.stu.cs.ems.core.aggregates.AggregateVariable;
import ua.cn.stu.cs.ems.core.models.Model;
import ua.cn.stu.cs.ems.core.transitions.SpecialTransition;
import ua.cn.stu.cs.ems.ecli.Value;

/**
 * Jess specific actions factory for JMock
 * @author proger
 */
public class Actions {
    public static Action addDelayableToModel(Model model, ActiveInstance instance, double delay) {
        return new AddDelayableToModel(model, instance, delay);
    }

    public static Action addSpecialTransitionToModel(SpecialTransition transition, Model model) {
        return new AddSpecialTransitionToModel(transition, model);
    }

    public static Action setVariableValue(AggregateVariable variable, Value value) {
        return new SetVariableValue(variable, value);
    }

    public static Action increaseCounter(Counter counter) {
        return new IncreaseCounter(counter);
    }
}

/**
 * Action for jmock library that adds instance to model's instance queue
 * @author proger
 */
class AddDelayableToModel implements Action {

    Model model;
    ActiveInstance instance;
    double delay;

    public AddDelayableToModel(Model model, ActiveInstance instance, double delay) {
        this.model = model;
        this.instance = instance;
        this.delay = delay;
    }

    public void describeTo(Description description) {
        description.appendText("Adds delayable ")
                   .appendValue(instance)
                   .appendText(" to a model ")
                   .appendValue(model)
                   .appendText(" on delay ")
                   .appendValue(delay);
    }

    public Object invoke(Invocation invctn) throws Throwable {
        model.addDelayable(instance, delay);
        return null;
    }

}

class AddSpecialTransitionToModel implements Action {

    Model model;
    SpecialTransition specialTransition;

    public AddSpecialTransitionToModel(SpecialTransition specialTransition, Model model) {
        this.specialTransition = specialTransition;
        this.model = model;
    }


    public void describeTo(Description description) {
        description.appendText("Add special transition ")
                   .appendValue(specialTransition)
                   .appendText("to a model ")
                   .appendValue(model);
    }

    public Object invoke(Invocation invctn) throws Throwable {
        model.addSpecialTransition(specialTransition);
        return null;
    }
}

class SetVariableValue implements Action {
    AggregateVariable variable;
    Value value;

    public SetVariableValue(AggregateVariable variable, Value value) {
        this.variable = variable;
        this.value = value;
    }

    public void describeTo(Description description) {
        description.appendText("Set ")
                   .appendValue(variable)
                   .appendText(" value ")
                   .appendValue(value);
    }

    public Object invoke(Invocation invctn) throws Throwable {
        variable.setValue(value);
        return null;
    }

}

class IncreaseCounter implements Action {

    Counter counter;

    public IncreaseCounter(Counter counter) {
        this.counter = counter;
    }

    public void describeTo(Description description) {
        description.appendText("Increase value of a counter");
    }

    public Object invoke(Invocation invctn) throws Throwable {
        counter.increase();
        return null;
    }
}