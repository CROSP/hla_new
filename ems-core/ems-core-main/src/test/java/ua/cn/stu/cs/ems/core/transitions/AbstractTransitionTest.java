package ua.cn.stu.cs.ems.core.transitions;

import ua.cn.stu.cs.ems.core.AbstractTransition;
import ua.cn.stu.cs.ems.core.Token;
import org.jmock.Expectations;
import ua.cn.stu.cs.ems.core.models.Model;
import ua.cn.stu.cs.ems.core.aggregates.Aggregate;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.junit.runner.RunWith;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JMock;
import java.util.ArrayList;
import ua.cn.stu.cs.ems.core.Place;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
;import static org.hamcrest.CoreMatchers.*;;
import static ua.cn.stu.cs.ems.core.testutils.MocksFactory.*;

/**
 *
 * @author proger
 */
@RunWith(JMock.class)
public class AbstractTransitionTest {

    Mockery mockery = new JUnit4Mockery();

    public AbstractTransitionTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testGetInputPlaces() {
        AbstractTransition transition = createAbstractTransitionImpl();

        final Place pos1 = new Place("P1");
        final Place pos2 = new Place("P2");
        transition.addInputPlace(pos1, 0);
        transition.addInputPlace(pos2, 1);

        Place[] expected = new ArrayList<Place>() {{
                add(pos1);
                add(pos2);
        }}.toArray(new Place[0]);

        Place[] result = transition.getInputPlaces().toArray(new Place[0]);

        assertArrayEquals(expected, result);

    }

    @Test
    public void testGetOutputPlaces() {
        AbstractTransition transition = createAbstractTransitionImpl();

        final Place pos1 = new Place("P1");
        final Place pos2 = new Place("P2");
        transition.addOutputPlace(pos1, 0);
        transition.addOutputPlace(pos2, 1);

        Place[] expected = new ArrayList<Place>() {
            {
                add(pos1);
                add(pos2);
            }
        }.toArray(new Place[0]);

        Place[] result = transition.getOutputPlaces().toArray(new Place[0]);

        assertArrayEquals(expected, result);
    }

    @Test
    public void testDelInputPlace() {
        AbstractTransition transition = createAbstractTransitionImpl();
        transition.addInputPlace(new Place("P"), 0);

        transition.disconnectInputPlace(0);
        assertThat(transition.getInputPlaces().size(), is(0));
    }

    @Test
    public void testDelOutputPlace() {
        AbstractTransition transition = createAbstractTransitionImpl();
        transition.addOutputPlace(new Place("P"), 0);

        transition.disconnectOutputPlace(0);
        assertThat(transition.getOutputPlaces().size(), is(0));
    }

    @Test(expected = RuntimeException.class)
    public void testAddNullPlace() {
        AbstractTransition transition = createAbstractTransitionImpl();

        transition.addInputPlace(null, 0);
    }

    @Test(expected = RuntimeException.class)
    public void testSetNullPlace() {
        AbstractTransition transition = createAbstractTransitionImpl();

        transition.addInputPlace(new Place("P1"), 0);
        transition.replaceInputPlace(null, 0);
    }

    @Test(expected = RuntimeException.class)
    public void testAddPlaceWithNegativePos() {
        AbstractTransition transtion = createAbstractTransitionImpl();

        transtion.addInputPlace(new Place("P1"), -1);
    }

    @Test(expected = RuntimeException.class)
    public void testSetPlaceWithNegativePos() {
        AbstractTransition transition = createAbstractTransitionImpl();

        transition.replaceInputPlace(new Place("P1"), -1);
    }

    @Test(expected = NullPointerException.class)
    public void testSetNullDelayFunction() {
        AbstractTransition transition = createAbstractTransitionImpl();
        transition.setDelayFunction(null);
    }

    @Test
    // Check if transition adds itself to delay queue
    public void testTransitionDelay() {
        final AbstractTransition transition = createAbstractTransitionImpl();
        Place place = new Place("P1");
        transition.addInputPlace(place, 0);
        place.setOutputActiveInstance(transition);

        final Aggregate aggregate = mockery.mock(Aggregate.class);
        final Model model = mockery.mock(Model.class);

        mockery.checking(new Expectations() {
            {
                oneOf(aggregate).getModel();
                will(returnValue(model));
            }
        });

        mockery.checking(new Expectations() {
            {
                oneOf(model).addDelayable(transition, 0);
            }
        });

        transition.setParentAggregate(aggregate);

        place.setToken(new Token());

    }

    @Test
    public void testInputLimit() {
        AbstractTransition transition = createAbstractTransitionImpl(1,  // limit number of input places
                                                                     AbstractTransition.UNLIMITED); // unlimited number of output places

        transition.addInputPlace(new Place("p1"), 0);


        boolean added = false;
        try {
            transition.addInputPlace(new Place("p2"), 1);
            added = true;
        } catch (Throwable t) {/* We expect this one. Do nothing*/}

        assertFalse("Number of added transition more that input limit", added);
    }

    @Test
    public void testOutputLimit() {
        AbstractTransition transition = createAbstractTransitionImpl(AbstractTransition.UNLIMITED,  // limit number of input places
                                                                     1); // unlimited number of output places

        transition.addOutputPlace(new Place("p1"), 0);


        boolean added = false;
        try {
            transition.addOutputPlace(new Place("p2"), 1);
            added = true;
        } catch (Throwable t) {/* We expect this one. Do nothing*/}

        assertFalse("Number of added transition more that input limit", added);
    }

    @Test
    public void testDualInputConnection() {
        AbstractTransition transition = createAbstractTransitionImpl();
        Place inputPlace = new Place("P");
        
        transition.addInputPlace(inputPlace, 0);

        assertEquals("When place is connected to transition's input, this transition should become place's output active instance",
                     transition, inputPlace.getOutputActiveInstance());

        Place newInputPlace = new Place("newP");

        transition.replaceInputPlace(newInputPlace, 0);

        assertEquals("When input place is replaced, old place's output active instance should be null",
                     null, inputPlace.getOutputActiveInstance());
        assertEquals("When input place is replaced, new place's output active instance should be set",
                transition, newInputPlace.getOutputActiveInstance());

        transition.disconnectInputPlace(0);

        assertEquals("When input place is disconnected from the transition, it's output active instance should be null",
                     null, newInputPlace.getOutputActiveInstance());
    }

    @Test
    public void testDualOutputConnection() {
        AbstractTransition transition = createAbstractTransitionImpl();
        Place outputPlace = new Place("P");

        transition.addOutputPlace(outputPlace, 0);

        assertEquals("When place is connected to transition's output, this transition should become place's input active instance",
                     transition, outputPlace.getInputActiveInstance());

        Place newOutputPlace = new Place("newP");

        transition.replaceOutputPlace(newOutputPlace, 0);

        assertEquals("When output place is replaced, old place's input active instance should be null",
                     null, outputPlace.getInputActiveInstance());
        assertEquals("When output place is replaced, new place's output active instance should be set",
                transition, newOutputPlace.getInputActiveInstance());

        transition.disconnectOutputPlace(0);

        assertEquals("When output place is disconnected from the transition, it's input active instance should be null",
                     null, newOutputPlace.getInputActiveInstance());
    }

    @Test
    public void testDisconnectInputPlace() {
      AbstractTransition transition = createAbstractTransitionImpl();
        Place inputPlace = new Place("P");
        transition.addInputPlace(inputPlace, 0);

        transition.disconnectInputPlace("P");

        assertEquals("If input place is disconnected number of input places should decrease",
                transition.getInputPlaces().size(), 0);
        assertEquals("If input place is disconnected it's output active instace should be null",
                inputPlace.getOutputActiveInstance(), null);
    }

    @Test
    public void testDisconnectOutputPlace() {
      AbstractTransition transition = createAbstractTransitionImpl();
        Place outputPlace = new Place("P");
        transition.addInputPlace(outputPlace, 0);

        transition.disconnectOutputPlace("P");

        assertEquals("If output place is disconnected number of output places should decrease",
                transition.getOutputPlaces().size(), 0);
        assertEquals("If output place is disconnected it's input active instace should be null",
                outputPlace.getInputActiveInstance(), null);
    }
}
