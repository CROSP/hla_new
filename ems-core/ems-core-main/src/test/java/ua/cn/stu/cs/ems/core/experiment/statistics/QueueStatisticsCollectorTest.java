package ua.cn.stu.cs.ems.core.experiment.statistics;

import org.junit.Ignore;
import ua.cn.stu.cs.ems.core.Token;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import ua.cn.stu.cs.ems.core.queues.FIFOQueue;
import ua.cn.stu.cs.ems.core.testutils.MockModel;
import static org.junit.Assert.*;
;import static org.hamcrest.CoreMatchers.*;;

/**
 *
 * @author proger
 */
public class QueueStatisticsCollectorTest {

    public QueueStatisticsCollectorTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Test
    public void testCheckNuberOfPassedToken() {
        MockModel model = new MockModel();
        TestQueue queue = new TestQueue();

        QueueStatisticsCollector qsc = new QueueStatisticsCollector(model, queue);
        Token t = new Token();

        qsc.startStatCollection();
        queue.testAddToken(t);
        queue.testAddToken(t);
        queue.testAddToken(t);
        queue.testAddToken(t);
        queue.testAddToken(t);

        // Remove 4 tokens
        queue.testRemoveToken();
        queue.testRemoveToken();
        queue.testRemoveToken();
        queue.testRemoveToken();

        qsc.stopStatCollection();

        StatisticsResult sr = qsc.getResult();

        assertThat(sr.getResultValue(StatisticsParameters.NUMBER_OF_PASSED_TOKENS), equalTo(4d));
    }

    @Test
    public void testCheckNuberOfPassedToken2() {
        MockModel model = new MockModel();
        TestQueue queue = new TestQueue();

        QueueStatisticsCollector qsc = new QueueStatisticsCollector(model, queue);
        Token t = new Token();

        qsc.startStatCollection();
        queue.testAddToken(t);
        queue.testAddToken(t);
        queue.testAddToken(t);
        queue.testAddToken(t);
        queue.testAddToken(t);

        // Remove 2 tokens
        queue.testRemoveToken();
        queue.testRemoveToken();

        qsc.stopStatCollection();

        // This tokens shouldn't be counted
        queue.testRemoveToken();
        queue.testRemoveToken();

        qsc.stopStatCollection();

        StatisticsResult sr = qsc.getResult();

        assertThat(sr.getResultValue(StatisticsParameters.NUMBER_OF_PASSED_TOKENS), equalTo(2d));
    }

    @Test
    public void testAverageQueueLength() {
        MockModel model = new MockModel();
        TestQueue queue = new TestQueue();

        model.setFinishTime(20);

        QueueStatisticsCollector qsc = new QueueStatisticsCollector(model, queue);
        Token t = new Token();

        qsc.startStatCollection();
        // 1 * 5
        queue.testAddToken(t);
        model.setModelTime(5);
        // 2 * 5
        queue.testAddToken(t);
        model.setModelTime(10);
        // 3 * 5
        queue.testAddToken(t);
        model.setModelTime(15);
        // 2 * 5
        queue.testRemoveToken();
        model.setModelTime(20);
        qsc.stopStatCollection();

        double expectedAverageQueueLength = (5 + 10 + 15 + 10) / 20;
        StatisticsResult sr = qsc.getResult();

        assertThat(sr.getResultValue(StatisticsParameters.AVERAGE_QUEUE_LENGTH), equalTo(expectedAverageQueueLength));

    }

    @Test
    public void testAverageQueueLengthWithResets() {
        MockModel model = new MockModel();
        TestQueue queue = new TestQueue();

        QueueStatisticsCollector qsc = new QueueStatisticsCollector(model, queue);
        Token t = new Token();

        model.setFinishTime(20);

        queue.testAddToken(t);
        model.setModelTime(3);

        qsc.startStatCollection();
        // 1 * 2
        model.setModelTime(5);

        // 2 * 5
        queue.testAddToken(t);
        model.setModelTime(10);

        StatisticsResult sr = qsc.getResult();
        // double expectedAverageLength = (1 * 2 + 2 * 5) / 7d;
        double expectedAverageLength = (1 * 2 + 2 * 5) / 17d;
        assertThat(sr.getResultValue(StatisticsParameters.AVERAGE_QUEUE_LENGTH), equalTo(expectedAverageLength));

        qsc.reset();

        // 2 * 5
        model.setModelTime(15);

        // 3 * 5
        queue.testAddToken(t);
        model.setModelTime(20);

        qsc.stopStatCollection();

        sr = qsc.getResult();
        expectedAverageLength = (2 * 5 + 3 * 5) / 10d;
        assertThat(sr.getResultValue(StatisticsParameters.AVERAGE_QUEUE_LENGTH), equalTo(expectedAverageLength));
    }
}

/**
 * Queue's interface doesn't allow to add/remove elements from queue (it's done
 * automatically due setting/getting tokens to/from places) so we create this
 * simple class to control queue in test.
 */
@Ignore
class TestQueue extends FIFOQueue {

    public TestQueue() {
        super("TestQueue");
    }

    public void testAddToken(Token token) {
        super.add(token);
    }

    public void testRemoveToken() {
        super.remove();
    }
}
