package ua.cn.stu.cs.ems.core.queues;

import java.util.ArrayList;
import org.junit.*;
import ua.cn.stu.cs.ems.core.Token;
import static ua.cn.stu.cs.ems.core.queues.TestQueueOrder.assertQueueOrder;
import ua.cn.stu.cs.ems.ecli.Value;

/**
 *
 * @author proger
 */
public class AbstractPriorityQueueTest {

    public AbstractPriorityQueueTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }


    @Test
    public void testPriority() {
        TokenPriorityFunction tpf = new TokenPriorityFunction() {

            public int calculateTokenPriority(Queue queue, Token token) {
                return token.getValue("attr").intValue();
            }
        };

        AbstractPriorityQueue queue = createAbstractPriorityQueueInstance("Q", tpf);
        
        final Token t1 = new Token(); t1.setValue("attr", new Value(1d));
        final Token t2 = new Token(); t2.setValue("attr", new Value(2d));
        final Token t3 = new Token(); t3.setValue("attr", new Value(3d));
        
        ArrayList<Token> input = new ArrayList<Token>() {{ 
            add(t1);
            add(t3);
            add(t2);
        }};
        
        ArrayList<Token> output = new ArrayList<Token>() {{ 
            add(t1);
            add(t2);
            add(t3);
        }};
        
        assertQueueOrder(queue, input, output);

    }

    private AbstractPriorityQueue createAbstractPriorityQueueInstance(String name, TokenPriorityFunction tpf) {
        return new AbstractPriorityQueue(name, tpf) {

            public int getPriorityForQueue(int priorityEstimation) {
                return priorityEstimation;
            }
        };
    }
}
