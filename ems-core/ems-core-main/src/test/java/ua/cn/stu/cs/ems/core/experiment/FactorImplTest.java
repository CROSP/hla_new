package ua.cn.stu.cs.ems.core.experiment;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ua.cn.stu.cs.ems.core.aggregates.Aggregate;
import ua.cn.stu.cs.ems.ecli.Value;
import static org.junit.Assert.*;
;import static org.hamcrest.CoreMatchers.*;;
import static ua.cn.stu.cs.ems.core.testutils.InstanceFactory.*;

/**
 *
 * @author proger
 */
public class FactorImplTest {

    public FactorImplTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testGetNumberOfValues() {
        Aggregate a = createAggregateInstance();
        a.setVariable("var", new Value(0));
        StepValueGenerator svg = new StepValueGenerator(0, 10, 1);

        FactorImpl f = new FactorImpl(a.getVariable("var"), svg);
        assertThat(f.numberOfValues(), is(10));
    }

    @Test
    public void testGetValueNumber() {
        Aggregate a = createAggregateInstance();
        a.setVariable("var", new Value(0));
        StepValueGenerator svg = new StepValueGenerator(0, 20, 2);
        FactorImpl f = new FactorImpl(a.getVariable("var"), svg);

        f.nextValue();
        assertThat(f.getValueNumber(), is(0));
        assertThat(f.getCurrentValue(), is(0d));

        f.nextValue();
        assertThat(f.getValueNumber(), is(1));
        assertThat(f.getCurrentValue(), is(2d));
    }

    @Test
    public void testSetValue() {
        Aggregate a = createAggregateInstance();
        a.setVariable("var", new Value(100));
        StepValueGenerator svg = new StepValueGenerator(0, 20, 2);
        FactorImpl f = new FactorImpl(a.getVariable("var"), svg);

        f.nextValue();
        f.setValue();
        assertThat(a.getVariable("var").getValue().doubleValue(), is(0d));

        f.nextValue();
        f.setValue();
        assertThat(a.getVariable("var").getValue().doubleValue(), is(2d));
    }
    
    @Test
    public void testHasMoreValues() {
      Aggregate a = createAggregateInstance();
        a.setVariable("var", new Value(100));
        
        StepValueGenerator svg = new StepValueGenerator(0, 3, 1);
        FactorImpl f = new FactorImpl(a.getVariable("var"), svg);
        
        assertTrue(f.hasMoreValues());
        f.nextValue();
        assertTrue(f.hasMoreValues());
        f.nextValue();
        assertTrue(f.hasMoreValues());
        f.nextValue();
        assertFalse(f.hasMoreValues());
    }

}