package ua.cn.stu.cs.ems.core.experiment.statistics;

import org.junit.Ignore;
import ua.cn.stu.cs.ems.core.models.AbstractModel;

/**
 *
 * @author proger
 */
@Ignore
class TestModel extends AbstractModel {

    double time = 0;

    public double getModelingTime() {
        return time;
    }

    public void setTime(final double newTime) {
        time = newTime;
        notifyModelTimeChanged();
    }

    public void start() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    protected void specializedReset() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void step() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void setInitModel(boolean init) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}