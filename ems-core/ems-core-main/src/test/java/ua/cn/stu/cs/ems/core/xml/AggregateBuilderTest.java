package ua.cn.stu.cs.ems.core.xml;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;
import org.jdom.Document;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.BeforeClass;
import org.junit.Test;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.aggregates.*;
import ua.cn.stu.cs.ems.core.queues.*;
import ua.cn.stu.cs.ems.core.transitions.*;
import ua.cn.stu.cs.ems.core.transitions.functions.*;
import ua.cn.stu.cs.ems.core.utils.Coordinates;

/**
 *
 * @author proger
 */
public class AggregateBuilderTest {

    public AggregateBuilderTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Test
    public void testBuildTAggregate() throws JDOMException, IOException, PNMLParsingException {
        String xml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<pnml xmlns='http://www.pnml.org/version-2009/grammar/pnml'>"
                + "<net id='rootAgregate' type='http://www.cs.stu.cn.ua/jess/enets'>"
                + "<page id='main'>"
                + "<transition id='ValidT'>"
                + "<name>"
                + "<text>XTest</text>"
                + "<graphics><offset x='20' y='30'/></graphics>"
                + "</name>"
                + "<definition type='transition' subType='T'>"
                + "<delayFunction type='ecli'><![CDATA[RETURN 123;]]></delayFunction>"
                + "<transformationFunction type='ecli'><![CDATA[T['attr'] = 123;]]></transformationFunction>"
                + "</definition>"
                + "<graphics><position x='100' y='200'/></graphics>"
                + "</transition></page></net></pnml>";

        AggregateDefinition ad = parseAndBuildAggregateDefinition(xml);

        assertEquals("rootAgregate", ad.getName());
        
        TTransition tt = (TTransition) ad.getTransition("ValidT");
        assertEquals("ValidT", tt.getName());
        
        InterpretedTransformationFunction itf = (InterpretedTransformationFunction) tt.getTransformationFunction();
        assertEquals("T['attr'] = 123;", itf.getSource());

        InterpretedDelayFunction idf = (InterpretedDelayFunction) tt.getDelayFunction();
        assertEquals("RETURN 123;", idf.getSource());

        Coordinates nameCoordinates = tt.getNameCoordinates();
        assertEquals(20, nameCoordinates.getX());
        assertEquals(30, nameCoordinates.getY());

        Coordinates objectCoordinates = tt.getObjectCoordinates();
        assertEquals(100, objectCoordinates.getX());
        assertEquals(200, objectCoordinates.getY());
    }

    @Test
    public void testParseTTransitionWithDefaultFunction() throws JDOMException, IOException, PNMLParsingException {
        String xml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<pnml xmlns='http://www.pnml.org/version-2009/grammar/pnml'>"
                + "<net id='rootAgregate' type='http://www.cs.stu.cn.ua/jess/enets'>"
                + "<page id='main'>"
                + "<transition id='ValidT'>"
                + "<name>"
                + "<text>XTest</text>"
                + "<graphics><offset x='20' y='30'/></graphics>"
                + "</name>"
                + "<definition type='transition' subType='T'>"
                + "</definition>"
                + "<graphics><position x='100' y='200'/></graphics>"
                + "</transition></page></net></pnml>";

        AggregateDefinition ad = parseAndBuildAggregateDefinition(xml);

        assertEquals("rootAgregate", ad.getName());

        TTransition tt = (TTransition) ad.getTransition("ValidT");
        assertEquals("ValidT", tt.getName());

        assertTrue(tt.getTransformationFunction() instanceof DefaultTransformationFunction);
        assertTrue(tt.getDelayFunction() instanceof DefaultDelayFunction);
    }

    @Test
    public void testParseAllTransitions() throws JDOMException, IOException, PNMLParsingException {
        String xml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<pnml xmlns='http://www.pnml.org/version-2009/grammar/pnml'>"
                + "<net id='rootAgregate' type='http://www.cs.stu.cn.ua/jess/enets'>"
                + "<page id='main'>"

                + "<transition id='T'>"
                + "<name>"
                + "<text>T</text>"
                + "<graphics><offset x='20' y='30'/></graphics>"
                + "</name>"
                + "<definition type='transition' subType='T'></definition>"
                + "<graphics><position x='100' y='200'/></graphics></transition>"

                + "<transition id='F'>"
                + "<name>"
                + "<text>F</text>"
                + "<graphics><offset x='20' y='30'/></graphics>"
                + "</name>"
                + "<definition type='transition' subType='F'></definition>"
                + "<graphics><position x='100' y='200'/></graphics></transition>"

                + "<transition id='J'>"
                + "<name>"
                + "<text>J</text>"
                + "<graphics><offset x='20' y='30'/></graphics>"
                + "</name>"
                + "<definition type='transition' subType='J'></definition>"
                + "<graphics><position x='100' y='200'/></graphics></transition>"

                + "<transition id='X'>"
                + "<name>"
                + "<text>X</text>"
                + "<graphics><offset x='20' y='30'/></graphics>"
                + "</name>"
                + "<definition type='transition' subType='X'></definition>"
                + "<graphics><position x='100' y='200'/></graphics></transition>"

                + "<transition id='Y'>"
                + "<name>"
                + "<text>Y</text>"
                + "<graphics><offset x='20' y='30'/></graphics>"
                + "</name>"
                + "<definition type='transition' subType='Y'></definition>"
                + "<graphics><position x='100' y='200'/></graphics></transition>"
                + "</page></net></pnml>";

        AggregateDefinition ad = parseAndBuildAggregateDefinition(xml);
        assertEquals("rootAgregate", ad.getName());

        Transition t = ad.getTransition("T");
        assertTrue(t instanceof TTransition);
        assertEquals("T", t.getName());

        Transition f = ad.getTransition("F");
        assertTrue(f instanceof FTransition);
        assertEquals("F", f.getName());
        
        Transition j = ad.getTransition("J");
        assertTrue(j instanceof JTransition);
        assertEquals("J", j.getName());

        Transition x = ad.getTransition("X");
        assertTrue(x instanceof XTransition);
        assertEquals("X", x.getName());

        Transition y = ad.getTransition("Y");
        assertTrue(y instanceof YTransition);
        assertEquals("Y", y.getName());
    }

    @Test
    public void testSpecialTransitionParsing() throws JDOMException, IOException, PNMLParsingException {
        String xml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<pnml xmlns='http://www.pnml.org/version-2009/grammar/pnml'>"
                + "<net id='rootAgregate' type='http://www.cs.stu.cn.ua/jess/enets'>"
                + "<page id='main'>"
                + "<transition id='Y'>"
                + "<name>"
                + "<text>Y</text>"
                + "<graphics><offset x='20' y='30'/></graphics>"
                + "</name>"
                + "<definition type='transition' subType='Y'>"
                + "<delayFunction type='ecli'><![CDATA[RETURN 123;]]></delayFunction>"
                + "<transformationFunction type='ecli'><![CDATA[T['attr'] = 123;]]></transformationFunction>"
                + "<permitingFunction type='ecli'><![CDATA[RETURN 1;]]></permitingFunction>"
                + "</definition>"
                + "<graphics><position x='100' y='200'/></graphics>"
                + "</transition></page></net></pnml>";

        AggregateDefinition ad = parseAndBuildAggregateDefinition(xml);

        YTransition y = (YTransition) ad.getTransition("Y");

        InterpretedDelayFunction idf = (InterpretedDelayFunction) y.getDelayFunction();
        assertEquals("RETURN 123;", idf.getSource());

        InterpretedTransformationFunction itf = (InterpretedTransformationFunction) y.getTransformationFunction();
        assertEquals("T['attr'] = 123;", itf.getSource());

        InterpretedPermitingFunction ipf = (InterpretedPermitingFunction) y.getPermitingFunction();
        assertEquals("RETURN 1;", ipf.getSource());
    }

    @Test
    public void testInputsOutputsParsing() throws JDOMException, IOException, PNMLParsingException {
        String xml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<pnml xmlns='http://www.pnml.org/version-2009/grammar/pnml'>"
                + "<net id='rootAgregate' type='http://www.cs.stu.cn.ua/jess/enets'>"
                + "<page id='main'>"

                + "<transition id='Input1'>"
                + "<name><text>Input1</text>"
                + "<graphics><offset x='22' y='-14'/></graphics></name>"
                + "<definition type='input'/>"
                + "<graphics><position x='950' y='484'/></graphics></transition>"

                + "<transition id='Input2'>"
                + "<name><text>Input1</text>"
                + "<graphics><offset x='22' y='-14'/></graphics></name>"
                + "<definition type='input'/>"
                + "<graphics><position x='950' y='484'/></graphics></transition>"

                + "<transition id='Output1'>"
                + "<name><text>Output1</text>"
                + "<graphics><offset x='22' y='-14'/></graphics></name>"
                + "<definition type='output'/>"
                + "<graphics><position x='950' y='484'/></graphics></transition>"

                + "<transition id='Output2'>"
                + "<name><text>Output1</text>"
                + "<graphics><offset x='22' y='-14'/></graphics></name>"
                + "<definition type='output'/>"
                + "<graphics><position x='950' y='484'/></graphics></transition>"
               
                + "</page></net></pnml>";

        AggregateDefinition ad = parseAndBuildAggregateDefinition(xml);

        Input input1 = ad.getInput("Input1");
        Input input2 = ad.getInput("Input2");
        List<Input> inputs = ad.getInputs();
        assertTrue(input1 == inputs.get(0));
        assertTrue(input2 == inputs.get(1));

        Output output1 = ad.getOutput("Output1");
        Output output2 = ad.getOutput("Output2");
        List<Output> outputs = ad.getOutputs();
        assertTrue(output1 == outputs.get(0));
        assertTrue(output2 == outputs.get(1));
    }

    @Test
    public void testQueuesParsing() throws JDOMException, IOException, PNMLParsingException {
         String xml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<pnml xmlns='http://www.pnml.org/version-2009/grammar/pnml'>"
                + "<net id='rootAgregate' type='http://www.cs.stu.cn.ua/jess/enets'>"
                + "<page id='main'>"

                + "<transition id='FIFO'>"
                + "<name><text>FIFO</text><graphics><offset x='22' y='-14'/></graphics></name>"
                + "<definition type='queue' subType='FIFO'></definition>"
                + "<graphics><position x='950' y='484'/></graphics>"
                + "</transition>"

                + "<transition id='LIFO'>"
                + "<name><text>LIFO</text><graphics><offset x='22' y='-14'/></graphics></name>"
                + "<definition type='queue' subType='LIFO'></definition>"
                + "<graphics><position x='950' y='484'/></graphics>"
                + "</transition>"

                + "<transition id='PriorityLIFO'>"
                + "<name><text>PriorityLIFO</text><graphics><offset x='22' y='-14'/></graphics></name>"
                + "<definition type='queue' subType='PriorityLIFO'><priorityFunction><![CDATA[RETURN 1;]]></priorityFunction></definition>"
                + "<graphics><position x='950' y='484'/></graphics>"
                + "</transition>"

                + "<transition id='PriorityFIFO'>"
                + "<name><text>PriorityFIFO</text><graphics><offset x='22' y='-14'/></graphics></name>"
                + "<definition type='queue' subType='PriorityFIFO'><priorityFunction><![CDATA[RETURN 2;]]></priorityFunction></definition>"
                + "<graphics><position x='950' y='484'/></graphics>"
                + "</transition>"

                + "</page></net></pnml>";

         AggregateDefinition ad = parseAndBuildAggregateDefinition(xml);

         FIFOQueue fifo = (FIFOQueue) ad.getQueue("FIFO");
         assertEquals("FIFO", fifo.getName());

         LIFOQueue lifo = (LIFOQueue) ad.getQueue("LIFO");
         assertEquals("LIFO", lifo.getName());

         FIFOPriorityQueue pfifo = (FIFOPriorityQueue) ad.getQueue("PriorityFIFO");
         assertEquals("PriorityFIFO", pfifo.getName());
         InterpretedTokenPriorityFunction itpf = (InterpretedTokenPriorityFunction) pfifo.getPriorityFunction();
         assertEquals("RETURN 2;", itpf.getSource());

         LIFOPriorityQueue plifo = (LIFOPriorityQueue) ad.getQueue("PriorityLIFO");
         assertEquals("PriorityLIFO", plifo.getName());
         InterpretedTokenPriorityFunction itpfl = (InterpretedTokenPriorityFunction) plifo.getPriorityFunction();
         assertEquals("RETURN 1;", itpfl.getSource());
    }

    @Test
    public void testBuildAggregateWithChildAggregate() throws JDOMException, IOException, PNMLParsingException {
        AggregateDefinition child = new AggregateDefinition("child");
        AggregateRegistry ar = new AggregateRegistry();
        ar.addAggregateDefinition(child);

        String xml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<pnml xmlns='http://www.pnml.org/version-2009/grammar/pnml'>"
                + "<net id='rootAgregate' type='http://www.cs.stu.cn.ua/jess/enets'>"
                + "<page id='main'>"

                + "<transition id='A'>"
                + "<name><text>A</text><graphics><offset x='22' y='-14'/></graphics></name>"
                + "<definition type='aggregate' subType='child'></definition>"
                + "<graphics><position x='950' y='484'/></graphics>"
                + "</transition>"

                + "</page></net></pnml>";


         AggregateDefinition ad = parseAndBuildAggregateDefinition(xml, ar);
         AggregateDefinitionReference adr = (AggregateDefinitionReference) ad.getAggregate("A");
         assertEquals("A", adr.getName());
         
         AggregateDefinition childAD = adr.getAggregateDefinition();
         assertEquals("child", childAD.getName());
    }

    @Test
    public void testBuildVariables() throws JDOMException, IOException, PNMLParsingException {
        String xml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<pnml xmlns='http://www.pnml.org/version-2009/grammar/pnml'>"
                + "<net id='rootAgregate' type='http://www.cs.stu.cn.ua/jess/enets'>"
                + "<variables>"
                + "<variable name='var1' value='12.34' />"
                + "<variable name='var2' value='-12.34' />"
                + "<variable name='var3' value='+12.34' />"
	        + "</variables>"

                + "<page id='main'>"
                + "</page></net></pnml>";

        AggregateDefinition ad = parseAndBuildAggregateDefinition(xml);

        AggregateVariable av1 = ad.getVariable("var1");
        assertEquals(12.34, av1.getValue().doubleValue(), 0.00001);

        AggregateVariable av2 = ad.getVariable("var2");
        assertEquals(-12.34, av2.getValue().doubleValue(), 0.00001);

        AggregateVariable av3 = ad.getVariable("var3");
        assertEquals(12.34, av3.getValue().doubleValue(), 0.00001);
    }

    @Test
    public void testBuildAggregateWithNoVariables() throws JDOMException, IOException, PNMLParsingException {
        String xml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<pnml xmlns='http://www.pnml.org/version-2009/grammar/pnml'>"
                + "<net id='rootAgregate' type='http://www.cs.stu.cn.ua/jess/enets'>"

                + "<page id='main'>"
                + "</page></net></pnml>";

        AggregateDefinition ad = parseAndBuildAggregateDefinition(xml);

        assertTrue(ad.getVariablesNames().isEmpty());
    }

    @Test
    public void testPlacesParsing() throws JDOMException, IOException, PNMLParsingException {
        String xml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<pnml xmlns='http://www.pnml.org/version-2009/grammar/pnml'>"
                + "<net id='rootAgregate' type='http://www.cs.stu.cn.ua/jess/enets'>"

                + "<page id='main'>"
                + "<place id='place'>"
                + "<name><text>place</text><graphics><offset x='20' y='10'/></graphics></name>"
                + "<token>"
                + "<attribute name='val1' value='12' />"
                + "<attribute name='val2' value='-12' />"
                + "</token>"
                + "<graphics><position x='100' y='200'/></graphics>"
                + "</place>"

                + "</page></net></pnml>";

        AggregateDefinition ad = parseAndBuildAggregateDefinition(xml);

        Place p = ad.getPlace("place");

        Token t = p.getToken();
        assertEquals(new Double(12), t.getValue("val1").doubleValue());
        assertEquals(new Double(-12), t.getValue("val2").doubleValue());

        Coordinates nameCoordinates = p.getNameCoordinates();
        assertEquals(20, nameCoordinates.getX());
        assertEquals(10, nameCoordinates.getY());

        Coordinates objectCoordinates = p.getObjectCoordinates();
        assertEquals(100, objectCoordinates.getX());
        assertEquals(200, objectCoordinates.getY());
    }

    @Test
    public void testEmptyPlaceParsing() throws JDOMException, IOException, PNMLParsingException {
        String xml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<pnml xmlns='http://www.pnml.org/version-2009/grammar/pnml'>"
                + "<net id='rootAgregate' type='http://www.cs.stu.cn.ua/jess/enets'>"

                + "<page id='main'>"
                + "<place id='place'>"
                + "<name><text>place</text><graphics><offset x='22' y='-10'/></graphics></name>"
                + "<graphics><position x='334' y='404'/></graphics>"
                + "</place>"

                + "</page></net></pnml>";

        AggregateDefinition ad = parseAndBuildAggregateDefinition(xml);

        Place p = ad.getPlace("place");

        Token t = p.getToken();
        assertNull(t);
    }

    @Test
    public void testParsePlaceWithEmptyToken() throws JDOMException, IOException, PNMLParsingException {
        String xml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<pnml xmlns='http://www.pnml.org/version-2009/grammar/pnml'>"
                + "<net id='rootAgregate' type='http://www.cs.stu.cn.ua/jess/enets'>"

                + "<page id='main'>"
                + "<place id='place'>"
                + "<name><text>place</text><graphics><offset x='22' y='-10'/></graphics></name>"
                + "<token>"
                + "</token>"
                + "<graphics><position x='334' y='404'/></graphics>"
                + "</place>"

                + "</page></net></pnml>";

        AggregateDefinition ad = parseAndBuildAggregateDefinition(xml);
        Place p = ad.getPlace("place");

        Token t = p.getToken();
        assertNotNull(t);
    }

    @Test
    public void testBuildingArcBetweenPlaceAndTransition() throws JDOMException, IOException, PNMLParsingException {
        String xml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<pnml xmlns='http://www.pnml.org/version-2009/grammar/pnml'>"
                + "<net id='rootAgregate' type='http://www.cs.stu.cn.ua/jess/enets'>"
                + "<page id='main'>"

                + "<place id='input'>"
                + "<name><text>input</text><graphics><offset x='22' y='-10'/></graphics></name>"
                + "<graphics><position x='334' y='404'/></graphics>"
                + "</place>"

                + "<place id='output1'>"
                + "<name><text>output1</text><graphics><offset x='22' y='-10'/></graphics></name>"
                + "<graphics><position x='334' y='404'/></graphics>"
                + "</place>"

                + "<place id='output2'>"
                + "<name><text>output2</text><graphics><offset x='22' y='-10'/></graphics></name>"
                + "<graphics><position x='334' y='404'/></graphics>"
                + "</place>"

                + "<transition id='F'>"
                + "<name>"
                + "<text>F</text>"
                + "<graphics><offset x='20' y='30'/></graphics>"
                + "</name>"
                + "<definition type='transition' subType='F'></definition>"
                + "<graphics><position x='100' y='200'/></graphics></transition>"

                + "<arc id='ValidT' source='input' target='F'>"
                + "<graphics><position x='950' y='484'/></graphics></arc>"

                + "<arc id='ValidT' source='F' target='output1'>"
                + "<graphics><position x='950' y='484'/></graphics></arc>"

                + "<arc id='ValidT' source='F' target='output2'>"
                + "<graphics><position x='950' y='484'/></graphics></arc>"

                + "</page></net></pnml>";

        AggregateDefinition ad = parseAndBuildAggregateDefinition(xml);
        FTransition ft = (FTransition) ad.getTransition("F");

        Place input = ad.getPlace("input");
        Place output1 = ad.getPlace("output1");
        Place output2 = ad.getPlace("output2");

        assertEquals(input, ft.getInputPlace());
        assertEquals(output1, ft.getOutputPlaces().get(0));
        assertEquals(output2, ft.getOutputPlaces().get(1));
    }

    @Test
    public void testBuildArcsBetweenPlacesAndQueues() throws JDOMException, IOException, PNMLParsingException {
        String xml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<pnml xmlns='http://www.pnml.org/version-2009/grammar/pnml'>"
                + "<net id='rootAgregate' type='http://www.cs.stu.cn.ua/jess/enets'>"
                + "<page id='main'>"

                + "<place id='input'>"
                + "<name><text>input</text><graphics><offset x='22' y='-10'/></graphics></name>"
                + "<graphics><position x='334' y='404'/></graphics>"
                + "</place>"

                + "<transition id='FIFO'>"
                + "<name><text>FIFO</text><graphics><offset x='22' y='-14'/></graphics></name>"
                + "<definition type='queue' subType='FIFO'></definition>"
                + "<graphics><position x='950' y='484'/></graphics>"
                + "</transition>"

                + "<place id='output'>"
                + "<name><text>output2</text><graphics><offset x='22' y='-10'/></graphics></name>"
                + "<graphics><position x='334' y='404'/></graphics>"
                + "</place>"

                + "<arc id='ValidT' source='input' target='FIFO'>"
                + "<graphics><position x='950' y='484'/></graphics></arc>"

                + "<arc id='ValidT' source='FIFO' target='output'>"
                + "<graphics><position x='950' y='484'/></graphics></arc>"

                + "</page></net></pnml>";

        AggregateDefinition ad = parseAndBuildAggregateDefinition(xml);
        Queue q = ad.getQueue("FIFO");
        Place input = ad.getPlace("input");
        Place output = ad.getPlace("output");

        assertEquals(input, q.getInputPlace());
        assertEquals(output, q.getOutputPlace());
    }

    @Test
    public void testBuildArcsWithInputsOutputs() throws JDOMException, IOException, PNMLParsingException {
        String xml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<pnml xmlns='http://www.pnml.org/version-2009/grammar/pnml'>"
                + "<net id='rootAgregate' type='http://www.cs.stu.cn.ua/jess/enets'>"
                + "<page id='main'>"

                + "<transition id='input'>"
                + "<name><text>input</text><graphics><offset x='22' y='-14'/></graphics></name>"
                + "<definition type='input'></definition>"
                + "<graphics><position x='950' y='484'/></graphics>"
                + "</transition>"

                + "<place id='inputPlace'>"
                + "<name><text>inputPlace</text><graphics><offset x='22' y='-10'/></graphics></name>"
                + "<graphics><position x='334' y='404'/></graphics>"
                + "</place>"

                + "<transition id='FIFO'>"
                + "<name><text>FIFO</text><graphics><offset x='22' y='-14'/></graphics></name>"
                + "<definition type='queue' subType='FIFO'></definition>"
                + "<graphics><position x='950' y='484'/></graphics>"
                + "</transition>"

                + "<place id='outputPlace'>"
                + "<name><text>outputPlace</text><graphics><offset x='22' y='-10'/></graphics></name>"
                + "<graphics><position x='334' y='404'/></graphics>"
                + "</place>"

                + "<transition id='output'>"
                + "<name><text>output</text><graphics><offset x='22' y='-14'/></graphics></name>"
                + "<definition type='output'></definition>"
                + "<graphics><position x='950' y='484'/></graphics>"
                + "</transition>"

                + "<arc id='ValidT' source='input' target='inputPlace'>"
                + "<graphics><position x='950' y='484'/></graphics></arc>"

                + "<arc id='ValidT' source='inputPlace' target='FIFO'>"
                + "<graphics><position x='950' y='484'/></graphics></arc>"

                + "<arc id='ValidT' source='FIFO' target='outputPlace'>"
                + "<graphics><position x='950' y='484'/></graphics></arc>"

                + "<arc id='ValidT' source='outputPlace' target='output'>"
                + "<graphics><position x='950' y='484'/></graphics></arc>"

                + "</page></net></pnml>";

        AggregateDefinition ad = parseAndBuildAggregateDefinition(xml);
        Input input = ad.getInput("input");
        Place inputPlace = ad.getPlace("inputPlace");
        Queue fifo = ad.getQueue("FIFO");
        Place outputPlace = ad.getPlace("outputPlace");
        Output output = ad.getOutput("output");

        assertEquals(inputPlace, input.getOutputPlace());
        assertEquals(input, inputPlace.getInputActiveInstance());

        assertEquals(inputPlace, fifo.getInputPlace());
        assertEquals(fifo, inputPlace.getOutputActiveInstance());

        assertEquals(outputPlace, fifo.getOutputPlace());
        assertEquals(fifo, outputPlace.getInputActiveInstance());

        assertEquals(outputPlace, output.getInputPlace());
        assertEquals(output, outputPlace.getOutputActiveInstance());
    }

    @Test
    public void testInputOutputArcsBuilding() throws JDOMException, IOException, PNMLParsingException {
        AggregateDefinition child = new AggregateDefinition("child");
        child.addInput(new InputImpl("input1"), 0);
        child.addInput(new InputImpl("input2"), 1);
        child.addOutput(new OutputImpl("output1"), 0);
        child.addOutput(new OutputImpl("output2"), 0);

        AggregateRegistry registry = new AggregateRegistry();
        registry.addAggregateDefinition(child);

        String xml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<pnml xmlns='http://www.pnml.org/version-2009/grammar/pnml'>"
                + "<net id='rootAgregate' type='http://www.cs.stu.cn.ua/jess/enets'>"
                + "<page id='main'>"

                + "<transition id='A'>"
                + "<name><text>A</text><graphics><offset x='22' y='-14'/></graphics></name>"
                + "<definition type='aggregate' subType='child'></definition>"
                + "<graphics><position x='950' y='484'/></graphics>"
                + "</transition>"

                + "<place id='inputPlace'>"
                + "<name><text>inputPlace</text><graphics><offset x='22' y='-10'/></graphics></name>"
                + "<graphics><position x='334' y='404'/></graphics>"
                + "</place>"

                + "<place id='outputPlace'>"
                + "<name><text>outputPlace</text><graphics><offset x='22' y='-10'/></graphics></name>"
                + "<graphics><position x='334' y='404'/></graphics>"
                + "</place>"

                + "<arc id='ValidT' source='inputPlace' target='A.input1'>"
                + "<graphics><position x='950' y='484'/></graphics></arc>"

                + "<arc id='ValidT' source='A.output1' target='outputPlace'>"
                + "<graphics><position x='950' y='484'/></graphics></arc>"

                + "<arc id='ValidT' source='A.output2' target='A.input2'>"
                + "<graphics><position x='950' y='484'/></graphics></arc>"

                + "</page></net></pnml>";

        AggregateDefinition ad = parseAndBuildAggregateDefinition(xml, registry);
        Place inputPlace = ad.getPlace("inputPlace");
        Place outputPlace = ad.getPlace("outputPlace");

        AggregateDefinitionReference adr = (AggregateDefinitionReference) ad.getAggregate("A");
        Input input1 = adr.getInput("input1");
        Input input2 = adr.getInput("input2");

        Output output1 = adr.getOutput("output1");
        Output output2 = adr.getOutput("output2");

        assertEquals(inputPlace, input1.getInputPlace());
        assertEquals(input1, inputPlace.getOutputActiveInstance());

        assertEquals(outputPlace, output1.getOutputPlace());
        assertEquals(output1, outputPlace.getInputActiveInstance());

        assertEquals(input2, output2.getConnectedInputs().get(0));
        assertEquals(output2, input2.getConnectedOutput());
    }

    @Test
    public void testReadArcBetweenPlaceAndTransition() throws Exception {
        String xml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<pnml xmlns='http://www.pnml.org/version-2009/grammar/pnml'>"
                + "<net id='rootAgregate' type='http://www.cs.stu.cn.ua/jess/enets'>"
                + "<page id='main'>"

                + "<transition id='T'>"
                + "<name><text>T</text><graphics><offset x='22' y='-14'/></graphics></name>"
                + "<definition type='transition' subType='T'></definition>"
                + "<graphics><position x='950' y='484'/></graphics>"
                + "</transition>"

                + "<place id='place'>"
                + "<name><text>place</text><graphics><offset x='22' y='-10'/></graphics></name>"
                + "<graphics><position x='334' y='404'/></graphics>"
                + "</place>"

                + "<arc id='arc' source='T' target='place'>"
                + "<graphics>"
                + "<position x='1' y='4'/>"
                + "<position x='2' y='5'/>"
                + "<position x='3' y='6'/>"
                + "</graphics></arc>"

                + "</page></net></pnml>";

         AggregateDefinition ad = parseAndBuildAggregateDefinition(xml);

         List<ArcCoordinates> coordinates = ad.getArcCoordinates();
         ArcCoordinates ac = coordinates.get(0);
         assertEquals(null, ac.getSourceParent());
         assertEquals(null, ac.getTargetParent());
         assertEquals("T", ac.getSourceName());
         assertEquals("place", ac.getTargetName());


         assertEquals(1, ac.getCoordinates().get(0).getX());
         assertEquals(4, ac.getCoordinates().get(0).getY());

         assertEquals(2, ac.getCoordinates().get(1).getX());
         assertEquals(5, ac.getCoordinates().get(1).getY());

         assertEquals(3, ac.getCoordinates().get(2).getX());
         assertEquals(6, ac.getCoordinates().get(2).getY());
    }

    @Test(expected=PNMLParsingException.class)
    public void testArcBuildingBetweenNonExistingComponents() throws Exception {
        String xml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<pnml xmlns='http://www.pnml.org/version-2009/grammar/pnml'>"
                + "<net id='rootAgregate' type='http://www.cs.stu.cn.ua/jess/enets'>"
                + "<page id='main'>"
                // Arc connects non existing components. Exception!
                + "<arc id='arc' source='T' target='place'>"
                + "<graphics>"
                + "<position x='1' y='4'/>"
                + "<position x='2' y='5'/>"
                + "<position x='3' y='6'/>"
                + "</graphics></arc>"

                + "</page></net></pnml>";

         AggregateDefinition ad = parseAndBuildAggregateDefinition(xml);
    }

    @Test
    public void testArcBuildingWithNoArcsInXML() throws Exception {
        String xml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<pnml xmlns='http://www.pnml.org/version-2009/grammar/pnml'>"
                + "<net id='rootAgregate' type='http://www.cs.stu.cn.ua/jess/enets'>"
                + "<page id='main'>"

                + "<transition id='T'>"
                + "<name><text>T</text><graphics><offset x='22' y='-14'/></graphics></name>"
                + "<definition type='transition' subType='T'></definition>"
                + "<graphics><position x='950' y='484'/></graphics>"
                + "</transition>"

                + "<place id='place'>"
                + "<name><text>place</text><graphics><offset x='22' y='-10'/></graphics></name>"
                + "<graphics><position x='334' y='404'/></graphics>"
                + "</place>"

                + "</page></net></pnml>";

         AggregateDefinition ad = parseAndBuildAggregateDefinition(xml);

         List<ArcCoordinates> coordinates = ad.getArcCoordinates();
         assertEquals(0, coordinates.size());
    }

    @Test
    public void testInputOutputArcCoordinatesBuilding() throws Exception {
        AggregateDefinition child = new AggregateDefinition("child");
        child.addInput(new InputImpl("input1"), 0);
        child.addOutput(new OutputImpl("output1"), 0);

        AggregateRegistry registry = new AggregateRegistry();
        registry.addAggregateDefinition(child);

        String xml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<pnml xmlns='http://www.pnml.org/version-2009/grammar/pnml'>"
                + "<net id='rootAgregate' type='http://www.cs.stu.cn.ua/jess/enets'>"
                + "<page id='main'>"

                + "<transition id='A'>"
                + "<name><text>A</text><graphics><offset x='22' y='-14'/></graphics></name>"
                + "<definition type='aggregate' subType='child'></definition>"
                + "<graphics><position x='950' y='484'/></graphics>"
                + "</transition>"

                + "<arc id='ValidT' source='A.output1' target='A.input1'>"
                + "<graphics>"
                + "<position x='10' y='40'/>"
                + "<position x='20' y='50'/>"
                + "<position x='30' y='60'/>"
                + "</graphics></arc>"

                + "</page></net></pnml>";

        AggregateDefinition ad = parseAndBuildAggregateDefinition(xml, registry);

        List<ArcCoordinates> coordinates = ad.getArcCoordinates();
        ArcCoordinates ac = coordinates.get(0);
        assertEquals("A", ac.getSourceParent());
        assertEquals("A", ac.getTargetParent());
        assertEquals("output1", ac.getSourceName());
        assertEquals("input1", ac.getTargetName());


        assertEquals(10, ac.getCoordinates().get(0).getX());
        assertEquals(40, ac.getCoordinates().get(0).getY());

        assertEquals(20, ac.getCoordinates().get(1).getX());
        assertEquals(50, ac.getCoordinates().get(1).getY());

        assertEquals(30, ac.getCoordinates().get(2).getX());
        assertEquals(60, ac.getCoordinates().get(2).getY());
    }


    private AggregateDefinition parseAndBuildAggregateDefinition(String xml) throws JDOMException, IOException, PNMLParsingException {
        return parseAndBuildAggregateDefinition(xml, new AggregateRegistry());
    }

    private AggregateDefinition parseAndBuildAggregateDefinition(String xml, AggregateRegistry aggregateRegistry) throws JDOMException, IOException, PNMLParsingException {
        SAXBuilder saxBuilder = new SAXBuilder();
        Document document = saxBuilder.build(new StringReader(xml));

        AggregateBuilder ab = new AggregateBuilder();

        return ab.buildAggregateDefinition(document, aggregateRegistry);
    }

}