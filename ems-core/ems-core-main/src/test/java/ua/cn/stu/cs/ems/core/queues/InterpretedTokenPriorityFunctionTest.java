package ua.cn.stu.cs.ems.core.queues;

import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.BeforeClass;
import org.junit.Test;
import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.aggregates.Aggregate;
import ua.cn.stu.cs.ems.core.models.ENetworksModel;
import ua.cn.stu.cs.ems.core.testutils.InstanceFactory;
import ua.cn.stu.cs.ems.ecli.Value;

/**
 *
 * @author proger
 */
public class InterpretedTokenPriorityFunctionTest {

    public InterpretedTokenPriorityFunctionTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Test
    public void testCalculateTokenPriority() {
        String source = "RETURN T['attr'] * T['attr'];";
        ENetworksModel model = new ENetworksModel();
        Aggregate a = InstanceFactory.createAggregateInstanceWithName("A");
        FIFOQueue fifo = new FIFOQueue("FIFO");
        a.addQueue(fifo);
        model.setRootAggregate(a);
        
        InterpretedTokenPriorityFunction itpf = new InterpretedTokenPriorityFunction(source);

        Token t = new Token();
        t.setValue("attr", new Value(2d));

        assertEquals(4, itpf.calculateTokenPriority(fifo, t));
    }

}