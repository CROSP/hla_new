
package ua.cn.stu.cs.ems.core.xml;

import org.junit.Ignore;
import java.io.IOException;
import java.io.InputStream;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
;import static org.hamcrest.CoreMatchers.*;;
import org.xml.sax.SAXException;

/**
 *
 * @author proger
 */
public class XMLValidationTest {

    public XMLValidationTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Test
    public void testPNMLWithTransitions() throws IOException, SAXException {
        validatePNMLFile("RNG/transitionTests/ValidT.xml", true);
        validatePNMLFile("RNG/transitionTests/TWithPermitingFunction.xml", false);
        validatePNMLFile("RNG/transitionTests/WrongTransitionName.xml", false);
    }

    @Test
    public void testPNMLWithArcs() throws IOException, SAXException {
        validatePNMLFile("RNG/arcsTests/validArc.xml", true);
    }

    @Test
    public void testPNMLWithChildAggregates() throws IOException, SAXException {
        validatePNMLFile("RNG/aggregateTests/noAggregateTypeSpecified.xml", false);
        validatePNMLFile("RNG/aggregateTests/validAggregate.xml", true);
        validatePNMLFile("RNG/aggregateTests/wrongAggregateTypeName.xml", false);
    }

    @Test
    public void testPNMLWithPlaceMarking() throws IOException, SAXException {
        validatePNMLFile("RNG/placeMarkingTests/validPlace.xml", true);
        validatePNMLFile("RNG/placeMarkingTests/placeWithEmptyToken.xml", true);
        validatePNMLFile("RNG/placeMarkingTests/wrongAttributeDefinition.xml", false);
        validatePNMLFile("RNG/placeMarkingTests/wrongAttributeValue.xml", false);
    }

    @Test
    public void testPNMLWithQueues() throws IOException, SAXException {
        validatePNMLFile("RNG/queueTests/validNonPriorityQueue.xml", true);
        validatePNMLFile("RNG/queueTests/validPriorityQueueWithFunction.xml", true);
        validatePNMLFile("RNG/queueTests/priorityQueueWithoutFunction.xml", false);
    }

    @Test
    public void testPNMLWithVariables() throws IOException, SAXException {
        validatePNMLFile("RNG/variableTests/correctVariables.xml", true);
        validatePNMLFile("RNG/variableTests/nonDoubleVariable.xml", false);
    }

    /**
     * This test returns "Content is not allowed in prolog" exception. Goggled but
     * don't know what how to fix it.
     */
    @Ignore
    public void testModelDefinition() throws IOException, SAXException {
        validateModelFile("RNG/modelDefinitionTests", true);
    }

    private void validatePNMLFile(String xmlFileName, boolean isValid) throws IOException, SAXException {
        validateXMLFile(xmlFileName, XMLValidator.ENETS_RNG, isValid);
    }

    private void validateModelFile(String xmlFileName, boolean isValid) throws IOException, SAXException {
        validateXMLFile(xmlFileName, XMLValidator.MODEL_RNG, isValid);
    }

    private void validateXMLFile(String xmlFileName, String schemeFileName, boolean isValid) throws IOException, SAXException {
        XMLValidator validator = new XMLValidator();

        InputStream xmlIS = ClassLoader.getSystemResourceAsStream(xmlFileName);
        boolean result = validator.isValid(xmlIS, schemeFileName);

        assertThat(result, is(isValid));
    }

}
