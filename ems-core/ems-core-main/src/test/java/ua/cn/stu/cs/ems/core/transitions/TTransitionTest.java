package ua.cn.stu.cs.ems.core.transitions;

import ua.cn.stu.cs.ems.core.Token;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.jmock.Mockery;
import org.junit.runner.RunWith;
import org.jmock.integration.junit4.JMock;
import ua.cn.stu.cs.ems.core.Place;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

import ua.cn.stu.cs.ems.core.transitions.functions.TransformationFunction;
import ua.cn.stu.cs.ems.ecli.Value;

/**
 *
 * @author proger
 */
@RunWith(JMock.class)
public class TTransitionTest {
    Mockery context = new JUnit4Mockery();

    public TTransitionTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testAddInputPlace() {
        TTransition transition = createTTransition();
        Place place = new Place("P");
        transition.setInputPlace(place);

        assertEquals(1, transition.numberOfInputPlaces());
        assertEquals(place, transition.getInputPlaces().get(0));
    }

    @Test
    public void testAddOutputPlace() {
        TTransition transition = createTTransition();
        Place place = new Place("P");
        transition.setOutputPlace(place);

        assertEquals("If output place was added, numberOfOutputPlaces should be 1",
                     1, transition.numberOfOutputPlaces());
        assertEquals("If output place was added first output place should be equal to added place",
                     place, transition.getOutputPlaces().get(0));
    }

    @Test
    public void testSetInputPlace() {
        TTransition transition = createTTransition();
        Place oldPlace = new Place("oldPlace");
        Place newPlace = new Place("newPlace");

        transition.setInputPlace(oldPlace);
        transition.setInputPlace(newPlace);

        assertEquals("If output place was changed, numberOfOutputPlaces should be 1",
                     1, transition.numberOfInputPlaces());
        assertEquals("If output place was changed first output place should be equal to last set place",
                     newPlace, transition.getInputPlaces().get(0));
    }

    @Test
    public void testSetOutputplace() {
        TTransition transition = createTTransition();
        Place oldplace = new Place("oldPlace");
        Place newplace = new Place("newPlace");

        transition.setOutputPlace(oldplace);
        transition.setOutputPlace(newplace);

        assertEquals("If output place was changed, numberOfOutputPlaces should be 1",
                     1, transition.numberOfOutputPlaces());
        assertEquals("If output place was changed first output place should be equal to last set place",
                     newplace, transition.getOutputPlaces().get(0));
    }

    @Test
    public void testTransition() {
        TTransition transition = createTTransition();
        Place inputPlace = new Place("inputPlace");
        Place outputPlace = new Place("outputPlace");

        Token token = new Token();

        TransformationFunction tf = new TransformationFunction() {

            public Token transform(Transition transition, Token token) {
                token.setValue("attr", new Value(1d));
                return token;
            }

            public TransformationFunction copy() {
                throw new UnsupportedOperationException("Not supported yet.");
            }
        };
        transition.setTransformationFunction(tf);
        Token expectedToken = new Token();
        expectedToken.setValue("attr", new Value(1d));

        connectTransition(transition, inputPlace, outputPlace);

        inputPlace.setToken(token);

        transition.onDelayFinished();

        assertFalse(outputPlace.isEmpty());
        assertTrue(inputPlace.isEmpty());
        assertEquals(expectedToken, outputPlace.getToken());
    }



    @Test
    public void testTTransitionPredicateNoTokens() {
        TTransition transition = getConnectedTransition();

        assertFalse(transition.canFire());
    }

    @Test
    public void testTTransitionPredicateInputToken() {
        TTransition transition = getConnectedTransition();

        transition.getInputPlace().setToken(new Token());

        assertTrue(transition.canFire());
    }

    @Test
    public void testTTransitionPredicateOutputToken() {
        TTransition transition = getConnectedTransition();

        transition.getOutputPlace().setToken(new Token());

        assertFalse(transition.canFire());
    }

    @Test
    public void testTTransitionPredicateBothTokens() {
        TTransition transition = getConnectedTransition();

        transition.getInputPlace().setToken(new Token());
        transition.getOutputPlace().setToken(new Token());

        assertFalse(transition.canFire());
    }

    private void connectTransition(TTransition transition, Place inputPos, Place outputPos) {
        transition.setInputPlace(inputPos);
        transition.setOutputPlace(outputPos);
    }

    // Return transition with input place and output place connected to it
    private TTransition getConnectedTransition() {
        TTransition transition = createTTransition();
        Place input = new Place("input");
        Place output = new Place("output");

        connectTransition(transition, input, output);

        return transition;
    }

    private TTransition createTTransition() {
        return new TTransition("T");
    }
}