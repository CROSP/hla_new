package ua.cn.stu.cs.ems.core.experiment.statistics;

import ua.cn.stu.cs.ems.core.transitions.TTransition;
import ua.cn.stu.cs.ems.core.aggregates.Aggregate;
import ua.cn.stu.cs.ems.core.models.ENetworksModel;
import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.Place;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import ua.cn.stu.cs.ems.core.testutils.MockModel;
import static org.junit.Assert.*;
;import static org.hamcrest.CoreMatchers.*;;
import static ua.cn.stu.cs.ems.core.testutils.InstanceFactory.*;

/**
 *
 * @author proger
 */
public class PlaceStatisticsCollectorTest {

    public PlaceStatisticsCollectorTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    // In this test we create model with two transitions and three places:
    // O->|->O->|->O
    // We calculate how many tokens will pass through second place if inially a token is placed in first place
    @Test
    public void testNumberOfPassedTokens() {
        TTransition t1 = new TTransition("T1");
        TTransition t2 = new TTransition("T2");

        Place p1 = new Place("p1");
        Place p2 = new Place("p2");
        Place p3 = new Place("p3");

        p1.setToken(new Token());

        t1.setInputPlace(p1);
        t1.setOutputPlace(p2);

        t2.setInputPlace(p2);
        t2.setOutputPlace(p3);

        Aggregate root = createAggregateInstance();
        root.addPlaces(p1, p2, p3);
        root.addTransitions(t1, t2);

        final ENetworksModel model = new ENetworksModel();

        model.setRootAggregate(root);
        PlaceStatisticsCollector psc = new PlaceStatisticsCollector(model, p2);
        psc.startStatCollection();

        model.setFinishTime(10D);
        model.start();

        StatisticsResult sr = psc.getResult();
        // We expect one token to pass
        assertThat(sr.getResultValue(StatisticsParameters.NUMBER_OF_PASSED_TOKENS), equalTo(1d));
    }

    @Test
    public void testOccupiedCoefficient1() {
        MockModel model = new MockModel();
        Place place = new Place("P");

        PlaceStatisticsCollector psc = new PlaceStatisticsCollector(model, place);
        place.setToken(new Token());

        model.setFinishTime(30);

        model.setModelTime(10);
        // Place is occupied for 5 time units
        psc.startStatCollection();
        model.setModelTime(15);
        place.setToken(null);

        // Collection was started at time=10. Finish time = 30.
        //=> totalMonitoredTime=30-10=20. Occupied coeffiecient should be 5 / 20 = 0.25
        model.setModelTime(20);

        StatisticsResult sr = psc.getResult();
        assertThat(sr.getResultValue(StatisticsParameters.OCCUPIED_COEFFICIENT), equalTo(0.25));

        place.setToken(new Token());

        // One more time change and turn statistics off. Now occupied coefficient should be equal (5+10)/20 = 0.75
        model.setModelTime(30);
        place.setToken(null);
        psc.stopStatCollection();

        sr = psc.getResult();
        assertThat(sr.getResultValue(StatisticsParameters.OCCUPIED_COEFFICIENT), equalTo(0.75));

        // Time is advanced by statistics isn't collected.
        model.setModelTime(200);

        sr = psc.getResult();
        assertThat(sr.getResultValue(StatisticsParameters.OCCUPIED_COEFFICIENT), equalTo(0.75));

        /*
        // At time 20 statistics is collected for 10 time units and place was occupied for 5 time units. So
        // occupied coeffiecient should be 5 / 10 = 0.5
        model.setModelTime(20);
        
        StatisticsResult sr = psc.getResult();
        assertThat(sr.getResultValue(StatisticsParameters.OCCUPIED_COEFFICIENT), equalTo(0.5));
        
        // One more time change and turn statistics off. Now occupied coefficient should be equal 5/20 = 0.25
        model.setModelTime(30);
        psc.stopStatCollection();
        
        sr = psc.getResult();
        assertThat(sr.getResultValue(StatisticsParameters.OCCUPIED_COEFFICIENT), equalTo(0.25));
        
        // Time is advanced by statistics isn't collected.
        model.setModelTime(200);
        
        sr = psc.getResult();
        assertThat(sr.getResultValue(StatisticsParameters.OCCUPIED_COEFFICIENT), equalTo(0.25));
         */
    }

    @Test
    public void testOccupiedCoefficient2() {
        MockModel model = new MockModel();
        Place place = new Place("P");

        model.setFinishTime(40);

        PlaceStatisticsCollector psc = new PlaceStatisticsCollector(model, place);
        psc.startStatCollection();

        model.setModelTime(5);
        place.setToken(new Token());
        model.setModelTime(10);

        StatisticsResult sr = psc.getResult();
        assertThat(sr.getResultValue(StatisticsParameters.OCCUPIED_COEFFICIENT), equalTo(0.125));

        psc.reset();
        model.setModelTime(20);
        sr = psc.getResult();
        double res=1.0/3.0;
        assertThat(sr.getResultValue(StatisticsParameters.OCCUPIED_COEFFICIENT), equalTo(res));

        psc.reset();
        model.setModelTime(25);
        place.setToken(null);
        model.setModelTime(30);
        sr = psc.getResult();
        assertThat(sr.getResultValue(StatisticsParameters.OCCUPIED_COEFFICIENT), equalTo(0.25));

        /*
        model.setModelTime(5);
        place.setToken(new Token());
        model.setModelTime(10);
        
        StatisticsResult sr = psc.getResult();
        assertThat(sr.getResultValue(StatisticsParameters.OCCUPIED_COEFFICIENT), equalTo(0.5));
        
        psc.reset();
        model.setModelTime(20);
        sr = psc.getResult();
        assertThat(sr.getResultValue(StatisticsParameters.OCCUPIED_COEFFICIENT), equalTo(1d));
        
        psc.reset();
        model.setModelTime(25);
        place.setToken(null);
        model.setModelTime(30);
        sr = psc.getResult();
        assertThat(sr.getResultValue(StatisticsParameters.OCCUPIED_COEFFICIENT), equalTo(0.5));
         */
    }
}
