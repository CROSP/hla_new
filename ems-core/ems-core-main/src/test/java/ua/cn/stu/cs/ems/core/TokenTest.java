package ua.cn.stu.cs.ems.core;


import java.util.ArrayList;
import org.junit.Test;

import ua.cn.stu.cs.ems.ecli.Value;
import static org.junit.Assert.*;
;import static org.hamcrest.CoreMatchers.*;;


/**
 *
 * @author proger
 */
public class TokenTest{   

    @Test
    public void testSetAttribute() {        
        String name = "key";
        Double value = 123d;
        Token token = new Token();
        token.setValue(name, new Value(value));
        Double result = token.getValue(name).doubleValue();

        assertEquals("Token should return correct value of attribute", result, value);
    }

    @Test
    public void testGetNonexistingAttribute() {
        String name = "unknown";
        Token token = new Token();
        Integer expected = null;
        Double result = token.getValue(name) != null ? token.getValue(name).doubleValue() : null;
        
        assertEquals("Access to nonexisting token should return null", expected, result);
    }

    @Test(expected=NullPointerException.class)
    public void testSetValueWithNullName() {
        String name = null;
        Double value = 1d;
        Token token = new Token();
        token.setValue(name, new Value(value));
    }

    @Test
    public void testEquals() {
        Token token1 = new Token();
        Token token2 = new Token();

        token1.setValue("name1", new Value(1d));
        token1.setValue("name2", new Value(2d));

        token2.setValue("name1", new Value(1d));
        token2.setValue("name2", new Value(2d));

        assertEquals("Two marks with same key-values should be equal", token1, token2);
    }

    @Test
    public void testCopyConstructor() {
        Token original = new Token();
        original.setValue("attr", new Value(20d));
        original.setValue("attr", new Value(40d));

        Token copy = new Token(original);

        assertThat("Token's copy constructor should create identical copy of input token",
                copy, equalTo(original));
    }

    @Test
    public void testAddAttributeArray() {
        Attribute a1 = new Attribute("a1", new Value(1));
        Attribute a2 = new Attribute("a2", new Value(2));

        Token t = new Token();
        t.addAttributes(a1, a2);

        assertEquals(2, t.getAttributeNames().size());
        assertEquals(new Double(1), t.getValue("a1").doubleValue());
        assertEquals(new Double(2), t.getValue("a2").doubleValue());
    }

    @Test
    public void testAddAttributeCollection() {
        final Attribute a1 = new Attribute("a1", new Value(1));
        final Attribute a2 = new Attribute("a2", new Value(2));

        ArrayList<Attribute> list = new ArrayList<Attribute>() {{
            add(a1);
            add(a2);
        }};

        Token t = new Token();
        t.addAttributes(list);

        assertEquals(2, t.getAttributeNames().size());
        assertEquals(new Double(1), t.getValue("a1").doubleValue());
        assertEquals(new Double(2), t.getValue("a2").doubleValue());
    }

}
