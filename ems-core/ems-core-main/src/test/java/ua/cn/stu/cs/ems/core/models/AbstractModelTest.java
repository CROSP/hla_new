package ua.cn.stu.cs.ems.core.models;

import org.jmock.integration.junit4.JUnit4Mockery;
import org.jmock.Mockery;
import ua.cn.stu.cs.ems.core.transitions.AbstractSpecialTransition;
import org.jmock.integration.junit4.JMock;
import ua.cn.stu.cs.ems.core.aggregates.AbstractAggregate;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import ua.cn.stu.cs.ems.core.aggregates.Aggregate;
import ua.cn.stu.cs.ems.core.aggregates.AggregateInstance;
import ua.cn.stu.cs.ems.core.transitions.SpecialTransition;
import ua.cn.stu.cs.ems.core.transitions.TTransition;
import ua.cn.stu.cs.ems.core.transitions.YTransition;
import static org.junit.Assert.*;

;import static org.hamcrest.CoreMatchers.*;;

/**
 *
 * @author proger
 */
@RunWith(JMock.class)
public class AbstractModelTest {

    public AbstractModelTest() {
    }
    Mockery mockContext = new JUnit4Mockery();

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testSetRootAggregate() {
        AbstractModel model = createAbstractModelInstance();
        Aggregate aggregate = new AbstractAggregate("A1") {
        };

        model.setRootAggregate(aggregate);
        Model result = aggregate.getModel();

        assertThat(model, equalTo(result));

    }

    @Test(expected = NullPointerException.class)
    public void testAddNullDelayable() {
        AbstractModel model = createAbstractModelInstance();
        model.addDelayable(null, 0);
    }

    @Test(expected = NullPointerException.class)
    public void testAddNullSpecialTransition() {
        AbstractModel model = createAbstractModelInstance();
        model.addSpecialTransition(null);
    }

    @Test
    public void testAddDelayable() {
        AbstractModel model = createAbstractModelInstance();
        Aggregate agg = new AggregateInstance("A1");
        model.setRootAggregate(agg);
        TTransition t = new TTransition("TT");
        agg.addTransition(t);
        assertThat(model.addDelayable(t, 0), is(true));
        SpecialTransition st = new YTransition("Y");//mockContext.mock(SpecialTransition.class);
        agg.addTransition(st);

        SpecialTransition st2 = new YTransition("X");
        agg.addTransition(st2);
        /*
         * Special transition can under some conditions add itself twice to delayed queue
         * model should show if addition was succesful.
         */
        assertThat(model.addDelayable(st, 0), is(true));
        assertThat(model.addDelayable(st, 0), is(false));

        assertThat(model.addDelayable(st2, 0), is(true));
        assertThat(model.addDelayable(st2, 0), is(false));

    }

    private AbstractModel createAbstractModelInstance() {
        return new AbstractModelImpl();
    }
}

class AbstractModelImpl extends AbstractModel {

    public double getModelingTime() {
        return 0;
    }

    public void start() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void addSpecialTransition(AbstractSpecialTransition transition) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    protected void specializedReset() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void step() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void setInitModel(boolean init) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
