package ua.cn.stu.cs.ems.core.transitions;

import org.junit.*;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.transitions.functions.TransformationFunction;
import ua.cn.stu.cs.ems.ecli.Value;

import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;


/**
 * @author proger
 */
public class FTransitionTest {

    public FTransitionTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testFTransitionPredicateWithNoTokens() {
        FTransition transition = createFTransitionWithConnectedPlaces();

        assertThat(transition.canFire(), is(false));
    }

    @Test
    public void testFTransitionPredicateWithInputToken() {
        FTransition transition = createFTransitionWithConnectedPlaces();

        transition.getInputPlaces().get(0).setToken(new Token());
        assertThat(transition.canFire(), is(true));
    }

    @Test
    public void testFTransitionPredicateWithBothTokens() {
        FTransition transition = createFTransitionWithConnectedPlaces();

        transition.getOutputPlaces().get(0).setToken(new Token());
        assertThat(transition.canFire(), is(false));
    }

    @Test
    public void testTransfer() {
        FTransition transition = createFTransitionWithConnectedPlaces();

        Token token = new Token();
        token.setValue("attr", new Value(1d));
        transition.getInputPlace().setToken(token);
        TransformationFunction tf = new TransformationFunction() {
            public Token transform(Transition transition, Token token) {
                Token newToken = new Token(token);
                newToken.setValue("attr", new Value(token.getValue("attr").doubleValue() + 1));
                return newToken;
            }

            public TransformationFunction copy() {
                throw new UnsupportedOperationException("Not supported yet.");
            }
        };

        Token expectedToken = tf.transform(transition, token);
        transition.setTransformationFunction(tf);

        transition.onDelayFinished();

        for (Place pos : transition.getOutputPlaces())
            assertThat("After F transition fired all it's output places should have " +
                            "transformed input place's token",
                    pos.getToken(), equalTo(expectedToken));

        assertThat("After F transition fired it's input place should be empty",
                transition.getInputPlace().getToken(), nullValue());
    }

    @Test
    public void testSetInputPostion() {
        FTransition transition = createFTransition();

        assertThat(transition.getInputPlace(), nullValue());
        assertThat(transition.getInputPlaces().size(), equalTo(0));

        Place pos1 = new Place("P1");
        transition.setInputPlace(pos1);

        assertThat(transition.getInputPlace(), is(pos1));
        assertThat(transition.getInputPlaces().size(), equalTo(1));

        Place pos2 = new Place("P2");
        transition.setInputPlace(pos2);

        assertThat(transition.getInputPlace(), is(pos2));
        assertThat(transition.getInputPlaces().size(), equalTo(1));

    }

    private FTransition createFTransitionWithConnectedPlaces() {
        FTransition transition = createFTransition();

        for (int i = 0; i < 3; i++) {
            Place pos = new Place("Place");
            transition.addOutputPlace(pos, i);
        }
        transition.setInputPlace(new Place("Place"));

        return transition;
    }

    private FTransition createFTransition() {
        return new FTransition("F");
    }
}