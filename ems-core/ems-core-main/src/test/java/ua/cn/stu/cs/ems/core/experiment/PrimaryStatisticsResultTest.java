package ua.cn.stu.cs.ems.core.experiment;

import java.util.ArrayList;
import java.util.List;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import ua.cn.stu.cs.ems.core.experiment.statistics.StatisticsParameters;
import ua.cn.stu.cs.ems.core.experiment.statistics.StatisticsResult;
import static org.junit.Assert.*;
;import static org.hamcrest.CoreMatchers.*;;
import static ua.cn.stu.cs.ems.core.testutils.Matchers.*;

/**
 *
 * @author proger
 */
public class PrimaryStatisticsResultTest {
    
    public PrimaryStatisticsResultTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Test
    public void testEqualsWhenShouldBeEqual() {
        PrimaryStatisticsResult psr1 = new PrimaryStatisticsResult(1, 
                createPrimaryStatisticsElement(new int[] {1, 2, 3}, new double[] {1, 2, 3}));
        PrimaryStatisticsResult psr2 = new PrimaryStatisticsResult(1, 
                createPrimaryStatisticsElement(new int[] {1, 2, 3}, new double[] {1, 2, 3}));
        
        assertThat(psr1, equalTo(psr2));
    }
    
    @Test
    public void testEqualsWhenShouldNOTBeEqual1() {
        PrimaryStatisticsResult psr1 = new PrimaryStatisticsResult(1, 
                createPrimaryStatisticsElement(new int[] {1, 2, 3}, new double[] {1, 2, 3}));
        PrimaryStatisticsResult psr2 = new PrimaryStatisticsResult(1, 
                createPrimaryStatisticsElement(new int[] {1, 2, 3}, new double[] {100, 200, 300}));
        
        assertThat(psr1, not(equalTo(psr2)));
    }
    
    @Test
    public void testEqualsWhenShouldNOTBeEqual2() {
        PrimaryStatisticsResult psr1 = new PrimaryStatisticsResult(1, 
                createPrimaryStatisticsElement(new int[] {1, 2, 3}, new double[] {1, 2, 3}));
        PrimaryStatisticsResult psr2 = new PrimaryStatisticsResult(1234, 
                createPrimaryStatisticsElement(new int[] {1, 2, 3}, new double[] {1, 2, 3}));
        
        assertThat(psr1, not(equalTo(psr2)));
    }
    
    private List<PrimaryStatisticsElement> createPrimaryStatisticsElement(int[] times, double[] values) {
        List<PrimaryStatisticsElement> primaryStatistics = new ArrayList<PrimaryStatisticsElement>();
        
        for (int i = 0; i < times.length; i++) {
            StatisticsResult sr = new StatisticsResult();
            sr.setResultValue(StatisticsParameters.OCCUPIED_COEFFICIENT, values[i]);
            
            PrimaryStatisticsElement pse = new PrimaryStatisticsElement(times[i], sr);
            primaryStatistics.add(pse);
        }
        
        return primaryStatistics;
    }
}
