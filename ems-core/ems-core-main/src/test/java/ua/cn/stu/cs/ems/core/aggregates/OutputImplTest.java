package ua.cn.stu.cs.ems.core.aggregates;

import ua.cn.stu.cs.ems.core.models.Model;
import java.util.ArrayList;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.junit.runner.RunWith;
import org.jmock.integration.junit4.JMock;
import java.util.List;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.States;
import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.ecli.Value;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
;import static org.hamcrest.CoreMatchers.*;;

/**
 *
 * @author proger
 */
@RunWith(JMock.class)
public class OutputImplTest {

    Mockery mockery = new JUnit4Mockery();

    public OutputImplTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testSetTokenToInputPlace() {
        final Output output = createOutputWithInputAndOutputPlaces();
        Place inputPlace = output.getInputPlaces().get(0);
        Token token = new Token(); 
        token.setValue("attr", new Value(100d));
        
        final Model model = mockery.mock(Model.class, "model");
        final Aggregate aggregate = mockery.mock(Aggregate.class, "aggregate");
        output.setParentAggregate(aggregate);

        mockery.checking(new Expectations() {{
            oneOf(aggregate).getModel(); will(returnValue(model));
            oneOf(model).addDelayable(output, 0);
        }});

        inputPlace.setToken(token);
        
    }

    @Test(expected=IncorrectOutputConnection.class)
    public void testConnectInputToOutputWithConnectedPlace() {
        Output output = createOutputWithInputAndOutputPlaces();

        output.addInput(new InputImpl("I"), 0);
    }

    @Test
    public void testGetInputs() {
        Output output = createOutputWithInputAndOutputPlaces();
        output.connectOutputPlace(null);

        output.addInput(new InputImpl("I"), 0);
        output.addInput(new InputImpl("I"), 1);

        List<Input> result = output.getConnectedInputs();

        assertThat(result.size(), is(2));

    }

    @Test
    public void testSetInput() {
        Output output = createOutputInstance();
        Input input1 = new InputImpl("I");
        Input input2 = new InputImpl("I");

        output.addInput(input1, 0);
        output.addInput(input2, 1);

        Input input3 = new InputImpl("I");
        output.replaceInput(input3, 0);

        assertThat(output.getConnectedInputs().get(0), equalTo(input3));
        assertThat(output.getConnectedInputs().get(1), equalTo(input2));
        assertThat(output.getConnectedInputs().size(), is(2));
    }

    @Test
    public void testConflictResolvingStrategy() {
        final Output output = createOutputWithInputAndOutputPlaces();
        // Disconect ouput from connected to it ouput place
        output.connectOutputPlace(null);

        // Create three inputs one of wich occupied by a token
        final Input input1 = mockery.mock(Input.class, "occupiedInput");
        final Input input2 = mockery.mock(Input.class, "freeInput1");
        final Input input3 = mockery.mock(Input.class, "freeInput2");        

        // Create our conflict resolution srategy that will order to set
        // incoming token to a first free input if one exists
        output.setConflictResolutionStrategy(new ConflictResolutionStrategy() {

            public List<Input> getInputsToSendToken(Output output) {
                List<Input> result = new ArrayList<Input>();

                for (Input input : output.getConnectedInputs())
                    if (input.canSetToken()) {
                        result.add(input);
                        break;
                    }

                return result;
            }
        });
        
        final Token token = new Token();
        token.setValue("attr", new Value(1d));
        
        
        mockery.checking(new Expectations() {{
            // Allow to ask if token can be set to each of inputs
            allowing(input1).canSetToken(); will(returnValue(false));
            allowing(input2).canSetToken(); will(returnValue(true));
            allowing(input3).canSetToken(); will(returnValue(true));
            
            allowing(input1).connectOutput(output);
            allowing(input2).connectOutput(output);
            allowing(input3).connectOutput(output);

            // Acording to the strategy token should be set only to the input2
            never(input1).setToken(token);
            oneOf(input2).setToken(token);
            never(input3).setToken(token);
        }});

        // Connect inputs to the ouput
        output.addInput(input1, 0);
        output.addInput(input2, 1);
        output.addInput(input3, 2);

        Place inputPlace = output.getInputPlaces().get(0);
        // Check expectations
        inputPlace.setToken(token);
        output.onDelayFinished();
        
        assertThat(inputPlace.getToken(), nullValue());
    }

    @Test(expected=NullPointerException.class)
    public void testSetNullConflictResolveStrategy() {
        Output output = createOutputInstance();

        output.setConflictResolutionStrategy(null);
    }

    @Test
    public void testSetTokenToInputWhenNoOutputPlacesOrInputs() {
        Output output = createOutputWithInputAndOutputPlaces();
        output.connectOutputPlace(null);

        Token token = new Token();
        Place inputPlace = output.getInputPlaces().get(0);
        inputPlace.setToken(token);

        assertThat(inputPlace.getToken(), equalTo(token));
    }

    @Test(expected=NullPointerException.class)
    public void testSetNullInput() {
        Output output = createOutputInstance();

        output.addInput(null, 0);
    }

    @Test
    public void testDualInputPlaceConnection() {
        Output output = createOutputInstance();
        Place inputPlace = new Place("P");

        output.connectInputPlace(inputPlace);
        assertEquals("When input place is connected to output, place's output active instance should be set",
                     output, inputPlace.getOutputActiveInstance());

        output.connectInputPlace(null);
        assertEquals("When input place is disconnected from the output, place's output active instance should be null",
                     null, inputPlace.getOutputActiveInstance());
    }

    @Test
    public void testDualOutputPlaceConnection() {
        Output output = createOutputInstance();
        Place outputPlace = new Place("P");

        output.connectOutputPlace(outputPlace);
        assertEquals("When output place is connected to output, place's input active instance should be set",
                     output, outputPlace.getInputActiveInstance());

        output.connectOutputPlace(null);
        assertEquals("When output place is disconnected from the output, place's input active instance should be null",
                     null, outputPlace.getInputActiveInstance());
    }

    @Test
    public void testDualInputOutputConnection() {
        final Output output = createOutputInstance();
        final Input input = mockery.mock(Input.class);

        mockery.checking(new Expectations() {{
            oneOf(input).connectOutput(output);
        }});

        output.addInput(input, 0);
    }

    @Test
    public void testDisconnectionFromOutput() {
        final Output output = createOutputInstance();
        final Input input = mockery.mock(Input.class);

        final States connectedDisconnected = mockery.states("connectedDisconected").startsAs("disconnected");
        mockery.checking(new Expectations() {{
            oneOf(input).connectOutput(output); when(connectedDisconnected.is("disconnected")); then(connectedDisconnected.is("connected"));
            oneOf(input).connectOutput(null); when(connectedDisconnected.is("connected"));
        }});

        output.addInput(input, 0);
        output.disconnectInput(0);
    }

    private Output createOutputWithInputAndOutputPlaces() {
        Output output = createOutputInstance();
        Place inputPlace = new Place("P1");
        Place outputPlace = new Place("P2");

        output.connectInputPlace(inputPlace);
        output.connectOutputPlace(outputPlace);
        
        return output;
    }

    private Output createOutputInstance() {
        return new OutputImpl("O");
    }

}