package ua.cn.stu.cs.ems.core.models;

import java.util.Set;
import ua.cn.stu.cs.ems.core.aggregates.AggregateVariable;
import ua.cn.stu.cs.ems.core.transitions.functions.DelayFunction;
import ua.cn.stu.cs.ems.core.aggregates.Input;
import org.jmock.States;
import ua.cn.stu.cs.ems.core.aggregates.Output;
import java.util.HashSet;
import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.Place;
import org.jmock.Expectations;
import ua.cn.stu.cs.ems.core.aggregates.Aggregate;
import ua.cn.stu.cs.ems.core.transitions.Transition;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.jmock.Mockery;
import org.jmock.Sequence;
import org.junit.runner.RunWith;
import org.jmock.integration.junit4.JMock;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ua.cn.stu.cs.ems.core.aggregates.AggregateInstance;
import ua.cn.stu.cs.ems.core.transitions.SpecialTransition;
import ua.cn.stu.cs.ems.core.transitions.TTransition;
import ua.cn.stu.cs.ems.core.transitions.functions.ConstDelayFunction;
import ua.cn.stu.cs.ems.core.transitions.functions.PermitingFunction;
import ua.cn.stu.cs.ems.ecli.Value;
import static org.junit.Assert.*;
;import static org.hamcrest.CoreMatchers.*;;
import static ua.cn.stu.cs.ems.core.testutils.Actions.*;
import static ua.cn.stu.cs.ems.core.testutils.InstanceFactory.*;

/**
 *
 * @author proger
 */
@RunWith(JMock.class)
public class ENetworksModelTest {

    Mockery mockery = new JUnit4Mockery();

    public ENetworksModelTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testSetModelingTime() {
        ENetworksModel model = createENetModelInstance();
        double time = 123;

        model.setModelingTime(time);
        double result = model.getModelingTime();

        assertThat(result, equalTo(time));
    }

    @Test
    // Check if special transition fire if no of its input places is changed but
    // one of its dependent places is changed
    public void testIfSpecialTransitionFired() {
        ENetworksModel model = createENetModelInstance();
        final Aggregate rootAggregate = createAggregateInstance();
        final SpecialTransition specialTransition = mockery.mock(SpecialTransition.class, "specialTransition");
        final PermitingFunction permitingFunction = mockery.mock(PermitingFunction.class, "permitingFunction");

        final TTransition transition = createTTransitionWithConnectedPlaces("");

        mockery.checking(new Expectations() {

            {
                allowing(specialTransition).setParentAggregate(rootAggregate);
                allowing(specialTransition).getName();
                allowing(specialTransition).getPermitingFunction();
                will(returnValue(permitingFunction));
                allowing(permitingFunction).getPlaceDependencies(specialTransition);
                will(returnValue(new HashSet<Place>() {

                    {
                        add(transition.getOutputPlace());
                    }
                }));
                allowing(permitingFunction).getVariableDependencies(specialTransition);
                will(returnValue(new HashSet<AggregateVariable>()));

                // Special transition should be called two times. First as a part of
                // a model initialization, second as a reaction of state change of
                // a place not connected to it
                exactly(2).of(specialTransition).fireIfPosible();
            }
        });


        connectTransitionsToAggregate(rootAggregate, specialTransition, transition);
        connectPlacesToAggregate(rootAggregate, transition.getInputPlace(), transition.getOutputPlace());
        model.setRootAggregate(rootAggregate);

        transition.getInputPlace().setToken(new Token());
        model.start();

    }

    @Test
    // Check when in delayed queue there an output and there is special transition
    // in special transitions set that can fire special transition will fire
    // before the output
    public void testSpecialTransitionOutputPriority() {
        final ENetworksModel model = createENetModelInstance();
        final Aggregate rootAggregate = createAggregateInstance();
        final SpecialTransition specialTransition = mockery.mock(SpecialTransition.class, "specialTransition");
        final PermitingFunction permitingFunction = mockery.mock(PermitingFunction.class, "permitingFunction");
        final Output output = mockery.mock(Output.class, "output");

        final TTransition transition = createTTransitionWithConnectedPlaces("");
        final Place outputPlace = transition.getOutputPlace();

        // State machine to control special transition behavior
        final States specialTransitionState = mockery.states("SpecialTransitionState").startsAs("init");
        final States outputState = mockery.states("outputState").startsAs("init");
        // Sequence to check if output fires after the special transition
        final Sequence priorityCheck = mockery.sequence("priorityChecking");

        mockery.checking(new Expectations() {

            {
                // allow different methods during model initialization
                allowing(specialTransition).setParentAggregate(rootAggregate);
                allowing(output).setParentAggregate(rootAggregate);
                allowing(output).connectInputPlace(outputPlace);
                allowing(specialTransition).getName();
                will(returnValue("specialTransition"));
                allowing(output).getName();
                will(returnValue("output"));
                allowing(specialTransition).getParentAggregate();
                will(returnValue(rootAggregate));
                allowing(output).getParentAggregate();
                will(returnValue(rootAggregate));


                // Define permiting function that declares it dependancy
                allowing(specialTransition).getPermitingFunction();
                will(returnValue(permitingFunction));
                allowing(permitingFunction).getPlaceDependencies(specialTransition);
                will(returnValue(new HashSet<Place>() {

                    {
                        add(outputPlace);
                    }
                }));
                allowing(permitingFunction).getVariableDependencies(specialTransition);
                will(returnValue(new HashSet<Place>()));

                oneOf(output).fireIfPosible();
                when(outputState.is("init"));
                // First invoke of specialTransition - during model initialization - do nothing
                oneOf(specialTransition).fireIfPosible();
                when(specialTransitionState.is("init"));
                then(specialTransitionState.is("waitForDependency"));
                // Output's input position now has token, so add it to model delayable queue
                oneOf(output).placeStateChanged(outputPlace);
                will(addDelayableToModel(model, output, 0));
                // Second invoke of specialTransition - because dependent place has changed - add itself to model delayble queue
                oneOf(specialTransition).fireIfPosible();
                when(specialTransitionState.is("waitForDependency"));
                will(addDelayableToModel(model, specialTransition, 0));

                // Check sequence of invocations. First - special transition, then - output
                oneOf(specialTransition).onDelayFinished();
                inSequence(priorityCheck);
                oneOf(output).onDelayFinished();
                inSequence(priorityCheck);

            }
        });

        outputPlace.setOutputActiveInstance(output);
        output.connectInputPlace(outputPlace);

        connectTransitionsToAggregate(rootAggregate, specialTransition, transition);
        connectPlacesToAggregate(rootAggregate, transition.getInputPlace(), transition.getOutputPlace());
        connectOutputsToAggregate(rootAggregate, output);


        model.setRootAggregate(rootAggregate);

        transition.getInputPlace().setToken(new Token());
        model.start();

    }

    @Test
    public void testIfOutputsAndInputsFireAtModelInitialization() {
        final Output output = mockery.mock(Output.class, "output");
        final Input input = mockery.mock(Input.class, "input");

        final Aggregate rootAggregate = createAggregateInstance();

        ENetworksModel model = new ENetworksModel();
        model.setRootAggregate(rootAggregate);


        mockery.checking(new Expectations() {

            {
                allowing(output).setParentAggregate(rootAggregate);
                allowing(input).setParentAggregate(rootAggregate);
                allowing(output).getName();
                will(returnValue("output"));
                allowing(input).getName();
                will(returnValue("input"));

                oneOf(output).fireIfPosible();
                oneOf(input).fireIfPosible();
            }
        });

        connectOutputsToAggregate(rootAggregate, output);
        connectInputsToAggregate(rootAggregate, input);

        model.start();
    }

    @Test
    public void testIfYTransitionDontFireTwoTimes() {
        TTransition tt1 = createTTransitionWithConnectedPlaces("1");
        TTransition tt2 = createTTransitionWithConnectedPlaces("2");

        Place input1 = tt1.getInputPlace();
        Place input2 = tt2.getInputPlace();
        Place output1 = tt1.getOutputPlace();
        Place output2 = tt2.getOutputPlace();

        final ENetworksModel model = createModelWithRootAggregate();
        final Aggregate rootAggregate = model.getRootAggregate();

        final SpecialTransition st = mockery.mock(SpecialTransition.class, "specialTransition");
        final PermitingFunction pf = mockery.mock(PermitingFunction.class, "permitingFunction");

        output1.setOutputActiveInstance(st);
        output2.setOutputActiveInstance(st);

        mockery.checking(new Expectations() {

            {
                allowing(st).setParentAggregate(rootAggregate);
                allowing(st).getName();
                will(returnValue("specialTransition"));
                allowing(st).getParentAggregate();
                will(returnValue(rootAggregate));

                allowing(st).getPermitingFunction();
                will(returnValue(pf));
                allowing(pf).getPlaceDependencies(st);
                will(returnValue(new HashSet<Place>()));
                allowing(pf).getVariableDependencies(st);
                will(returnValue(new HashSet<AggregateVariable>()));

                allowing(st).placeStateChanged(with(any(Place.class)));
                will(addSpecialTransitionToModel(st, model));
                allowing(st).fireIfPosible();
                will(addDelayableToModel(model, st, 100));

                oneOf(st).onDelayFinished();

            }
        });

        tt1.setDelayFunction(new ConfigurableDelayFunction(10));
        tt1.setDelayFunction(new ConfigurableDelayFunction(20));

        connectTransitionsToAggregate(rootAggregate, tt1, tt2, st);
        connectPlacesToAggregate(rootAggregate, input1, input2, output1, output2);

        input1.setToken(new Token());
        input2.setToken(new Token());

        model.start();
    }

    @Test
    public void testIfModelListenerCalled() {
        final ENetworksModel model = createENetModelInstance();
        final ModelListener listener = mockery.mock(ModelListener.class, "listener");

        model.setRootAggregate(createAggregateInstance());

        // Add same listener twice to check that listener won't be notified
        // several times on a same event
        model.addModelListener(listener);
        model.addModelListener(listener);

        mockery.checking(new Expectations() {

            {
                oneOf(listener).modelStarted(model);
                allowing(listener).modelTimeChanged(model, 0);
                one(listener).modelFinished(model);
            }
        });

        model.start();
    }

    @Test
    // Test if SpecialTransition is called by model if one variables
    // that this transtion's permiting function result depends on will be
    // changed
    public void testIfSpecialTransitionCalledWhenVariableValueChanged() {
        final ENetworksModel model = createENetModelInstance();
        final Aggregate rootAggregate = createAggregateInstance();
        rootAggregate.setVariable("variable", new Value(10d));
        final AggregateVariable variable = rootAggregate.getVariable("variable");

        final Transition transition = mockery.mock(Transition.class, "transition");
        final SpecialTransition specialTransition = mockery.mock(SpecialTransition.class, "specialTransition");
        final PermitingFunction permitingFunction = mockery.mock(PermitingFunction.class, "permitingFunction");

        final Set<AggregateVariable> permFuncVarDependencies = new HashSet<AggregateVariable>() {

            {
                add(variable);
            }
        };
        final Set<Place> permFuncPlaceDependencies = new HashSet<Place>();

        final States specialTransitionState = mockery.states("SpecialTransitionState").startsAs("init");

        mockery.checking(new Expectations() {

            {
                // Define return values and which methods are allowed to call
                allowing(transition).getName();
                will(returnValue("transition"));
                allowing(transition).setParentAggregate(rootAggregate);
                allowing(transition).getParentAggregate();
                will(returnValue(rootAggregate));

                allowing(specialTransition).getName();
                will(returnValue("specialTransition"));
                allowing(specialTransition).setParentAggregate(rootAggregate);
                allowing(specialTransition).getPermitingFunction();
                will(returnValue(permitingFunction));
                allowing(specialTransition).getParentAggregate();
                will(returnValue(rootAggregate));

                allowing(permitingFunction).getPlaceDependencies(specialTransition);
                will(returnValue(permFuncPlaceDependencies));
                allowing(permitingFunction).getVariableDependencies(specialTransition);
                will(returnValue(permFuncVarDependencies));

                // Initialization all transition will be called
                oneOf(transition).fireIfPosible();
                will(addDelayableToModel(model, transition, 0));
                oneOf(specialTransition).fireIfPosible();
                when(specialTransitionState.is("init"));
                then(specialTransitionState.is("waitingForCall"));

                // TTransition delay finished it changes root aggregate's variable
                oneOf(transition).onDelayFinished();
                will(setVariableValue(variable, new Value(10d)));
                // That what we check!!
                oneOf(specialTransition).fireIfPosible();
                when(specialTransitionState.is("waitingForCall"));
            }
        });

        model.setRootAggregate(rootAggregate);
        rootAggregate.addTransitions(transition, specialTransition);

        model.start();
    }

    @Test
    public void testChangeTime() {
        final ModelListener listener = mockery.mock(ModelListener.class, "listener");
        final ENetworksModel model = createENetModelInstance();

        mockery.checking(new Expectations() {

            {
                oneOf(listener).modelTimeChanged(model, 100);
            }
        });

        model.addModelListener(listener);
        model.setModelingTime(100);
    }

    @Test
    public void testTimersCall() {
        final ModelTimeListener timer1 = mockery.mock(ModelTimeListener.class, "timer1");
        final ModelTimeListener timer2 = mockery.mock(ModelTimeListener.class, "timer2");
        final ModelTimeListener timer3 = mockery.mock(ModelTimeListener.class, "timer3");

        final ENetworksModel model = new ENetworksModel();
        model.addTimer(timer1, 0);
        model.addTimer(timer2, 5);
        model.addTimer(timer3, 500);

        final Sequence seq = mockery.sequence("timerCallSequence");
        mockery.checking(new Expectations() {

            {
                oneOf(timer1).modelTimeChanged(model, 0);
                inSequence(seq);
                oneOf(timer2).modelTimeChanged(model, 5);
                inSequence(seq);
                never(timer3).modelTimeChanged(model, 500);
            }
        });

        model.setModelingTime(0);
        model.setModelingTime(4);
        model.setModelingTime(6);
        model.setModelingTime(19);
        model.setModelingTime(20);
        model.setModelingTime(25);
    }

    @Test
    /* WARNING. This test runs infinitely working model and tries to
     * interrupt. If this test doesn't finish in reasonable time then
     * something is wrong with model interruption mechanism.     *
     */
    public void testModelInterruption() {
        ENetworksModel model = createENetModelInstance();
        Aggregate infinite = createInfinitelyWorkingAggregate();
        model.setRootAggregate(infinite);

        final ModelTimeListener mtl = new ModelTimeListener() {

            public void modelTimeChanged(Model model, double newModelTime) {
                if (newModelTime == 10) {
                    model.interrupt();
                }
                model.addTimer(this, 1);
            }
        };

        model.addTimer(mtl, 1);
        model.start();

        assertThat(model.getModelingTime(), is(10D));
    }

    private Aggregate createInfinitelyWorkingAggregate() {
        Aggregate aggregate = createAggregateInstance();

        TTransition t1 = new TTransition("T1");
        t1.setDelayFunction(new ConstDelayFunction(1));
        TTransition t2 = new TTransition("T2");
        t2.setDelayFunction(new ConstDelayFunction(1));
        Place p1 = new Place("P1");
        Place p2 = new Place("P2");

        t1.setInputPlace(p1);
        t1.setOutputPlace(p2);
        t2.setInputPlace(p2);
        t2.setOutputPlace(p1);

        aggregate.addTransitions(t1, t2);
        aggregate.addPlaces(p1, p2);
        p1.setToken(new Token());

        return aggregate;
    }

    private ENetworksModel createENetModelInstance() {
        return new ENetworksModel();
    }

    // sufixx is needed to avoid name clash in aggregate
    private TTransition createTTransitionWithConnectedPlaces(String sufixx) {
        TTransition transition = new TTransition("T" + sufixx);
        Place input = new Place("inputPlace" + sufixx);
        Place output = new Place("outputPlace" + sufixx);

        transition.setInputPlace(input);
        input.setOutputActiveInstance(transition);
        transition.setOutputPlace(output);
        output.setInputActiveInstance(transition);

        return transition;
    }

    private void connectPlacesToAggregate(Aggregate rootAggregate, Place... places) {
        for (Place p : places) {
            rootAggregate.addPlace(p);
            p.setParentAggregate(rootAggregate);
        }
    }

    private void connectTransitionsToAggregate(Aggregate rootAggregate, Transition... transitions) {
        for (Transition t : transitions) {
            rootAggregate.addTransition(t);
            t.setParentAggregate(rootAggregate);
        }

    }

    private void connectOutputsToAggregate(Aggregate rootAggregate, Output... outputs) {
        int i = 0;
        for (Output o : outputs) {
            rootAggregate.addOutput(o, i++);
            o.setParentAggregate(rootAggregate);
        }
    }

    private void connectInputsToAggregate(Aggregate rootAggregate, Input... inputs) {
        int j = 0;
        for (Input i : inputs) {
            rootAggregate.addInput(i, j++);
            i.setParentAggregate(rootAggregate);
        }
    }

    private ENetworksModel createModelWithRootAggregate() {
        ENetworksModel model = new ENetworksModel();
        Aggregate rootAggregate = new AggregateInstance("A");
        model.setRootAggregate(rootAggregate);

        return model;
    }

    private class ConfigurableDelayFunction implements DelayFunction {

        double delay;

        public ConfigurableDelayFunction(double delay) {
            this.delay = delay;
        }

        public double getDelayTime(Transition transition) {
            return delay;
        }

        public DelayFunction copy() {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }
}
