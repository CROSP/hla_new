package ua.cn.stu.cs.ems.core.experiment;

import java.util.ArrayList;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author proger
 */
public class StepValueGeneratorTest {

    public StepValueGeneratorTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Test
    public void testStepValueGenerator() {
        StepValueGenerator svg = new StepValueGenerator(0, 10, 1);

        ArrayList<Double> expected = new ArrayList<Double>();
        for (int i = 0; i < 10; i++) {expected.add(new Double(i));}

        ArrayList<Double> result = new ArrayList<Double>();
        while(svg.hasMore()) {
            result.add(svg.nextValue());
        }

        assertArrayEquals(expected.toArray(), result.toArray());
    }

}