package ua.cn.stu.cs.ems.core.queues;

import static org.hamcrest.Matchers.is;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JMock;
import org.jmock.integration.junit4.JUnit4Mockery;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import org.junit.*;
import org.junit.runner.RunWith;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.aggregates.Aggregate;
import ua.cn.stu.cs.ems.core.models.Model;

/**
 *
 * @author proger
 */
@RunWith(JMock.class)
public class NonPriorityQueueTest {

    Mockery mockery = new JUnit4Mockery();

    public NonPriorityQueueTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testConnectInputPlace() {
        AbstractQueue queue = createAbstractQueueInstace();

        Place inputPlace = new Place("P");
        queue.connectInputPlace(inputPlace);

        assertThat("If place was connected to queue's input number of places connected to input should be 1",
                   queue.getInputPlaces().size(), is(1));
        assertThat("If place was connected to queue's input it should be among input places",
                   queue.getInputPlaces().get(0), is(inputPlace));
    }


    @Test
    public void testConnectOutputPlace() {
        AbstractQueue queue = createAbstractQueueInstace();

        Place outputPlace = new Place("P");
        queue.connectOutputPlace(outputPlace);

        assertThat("If place was connected to queue's output number of places connected to output should be 1",
                   queue.getOutputPlaces().size(), is(1));
        assertThat("If place was connected to queue's output it should be among output places",
                   queue.getOutputPlaces().get(0), is(outputPlace));
    }


    @Test
    public void testDisconnectInputPlace() {
        AbstractQueue queue = createAbstractQueueWithConnectedPlaces();
        queue.connectInputPlace(null);

        assertThat("If place was disconnected from queue's input number of input places should be 0",
                   queue.getInputPlaces().size(), is(0));
    }

    @Test
    public void testDisconnectOutputPlace() {
        AbstractQueue queue = createAbstractQueueWithConnectedPlaces();
        queue.connectOutputPlace(null);

        assertThat("If place was disconnected from queue's output number of output places should be 0",
                   queue.getOutputPlaces().size(), is(0));
    }

    @Test
    public void testIfTokenConsumed() {
        final AbstractQueue queue = createAbstractQueueWithConnectedPlaces();
        final Place input = queue.getInputPlace();
        final Aggregate aggregate = mockery.mock(Aggregate.class, "aggregate");
        final Model model = mockery.mock(Model.class, "model");

        mockery.checking(new Expectations() {{
            oneOf(aggregate).addQueue(queue);
            allowing(aggregate).getModel(); will(returnValue(model));
            oneOf(model).addDelayable(queue, 0);
        }});

        aggregate.addQueue(queue);
        queue.setParentAggregate(aggregate);
        // This will not permit queue to put token out
        queue.getOutputPlace().setToken(new Token());

        Token toSet = new Token();
        input.setToken(toSet);

        queue.onDelayFinished();

        assertTrue("If a token is set to the input place of the queue, queue should consume it",
                   input.isEmpty());
        assertThat("If a token was consumed, number of elements in token's queue should be 1",
                   queue.getTokensInQueue().size(), is(1));
    }


    private AbstractQueue createAbstractQueueWithConnectedPlaces() {
        AbstractQueue queue = createAbstractQueueInstace();
        Place input = new Place("input");
        Place output = new Place("output");

        queue.connectInputPlace(input);
        queue.connectOutputPlace(output);

        return queue;

    }

    private AbstractQueue createAbstractQueueInstace() {
        return new NonPriorityQueue("Q") {

            @Override
            protected int tookenNumberToPeek() {
                return 0;
            }
        };
    }
}