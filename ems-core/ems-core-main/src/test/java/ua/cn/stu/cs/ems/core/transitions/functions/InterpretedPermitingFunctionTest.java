package ua.cn.stu.cs.ems.core.transitions.functions;

import ua.cn.stu.cs.ems.core.aggregates.AggregateVariable;
import ua.cn.stu.cs.ems.core.Token;
import java.util.Set;
import java.util.HashSet;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.aggregates.Aggregate;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ua.cn.stu.cs.ems.core.models.ENetworksModel;
import ua.cn.stu.cs.ems.core.transitions.XTransition;
import ua.cn.stu.cs.ems.ecli.Value;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;
import static ua.cn.stu.cs.ems.core.testutils.InstanceFactory.*;
import static ua.cn.stu.cs.ems.core.testutils.Matchers.*;

/**
 *
 * @author proger
 */
public class InterpretedPermitingFunctionTest {

    public InterpretedPermitingFunctionTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testGetDependencies() {
        String source =
                  "IF (P['P3'].T)"
                + "   RETURN P['P1'].T['attr'];"
                + "ELSE"
                + "   RETURN V['var1'] + P['P4'].T['attr'];";

        InterpretedPermitingFunction ipf = new InterpretedPermitingFunction(source);

        ENetworksModel model = new ENetworksModel();
        Aggregate aggregate = createAggregateInstance();
        XTransition xt = new XTransition("XT");
        final Place p1 = new Place("P1");
        final Place p2 = new Place("P2");
        final Place p3 = new Place("P3");
        final Place p4 = new Place("P4");

        xt.addInputPlace(p1, 0);
        xt.addOutputPlace(p2, 0);

        model.setRootAggregate(aggregate);
        aggregate.addTransition(xt);
        aggregate.addPlaces(p1, p2, p3, p4);

        final AggregateVariable var = aggregate.getVariable("var1");

        Set<Place> expectedPlaceDependencies = new HashSet<Place>() {{ add(p3); add(p4); }};
        Set<AggregateVariable> expectedVariableDependencies = new HashSet<AggregateVariable>() {{ add(var); }};

        Set<Place> placeDependencies = ipf.getPlaceDependencies(xt);
        Set<AggregateVariable> variableDependencies = ipf.getVariableDependencies(xt);

        assertThat(placeDependencies, hasSameElements(expectedPlaceDependencies));
        assertThat(variableDependencies, hasSameElements(expectedVariableDependencies));
    }


    @Test
    public void testGetPlaceNumber() {
        String source = "RETURN V['name'] + P['place'].T['attr'];";

        InterpretedPermitingFunction ipf = new InterpretedPermitingFunction(source);

        ENetworksModel model = new ENetworksModel();
        Aggregate aggregate = createAggregateInstance();
        aggregate.setVariable("name", new Value(7d));

        XTransition xt = new XTransition("XT");
        final Place p1 = new Place("place");
        Token t = new Token(); t.setValue("attr", new Value(10d));
        p1.setToken(t);

        model.setRootAggregate(aggregate);
        aggregate.addTransition(xt);
        aggregate.addPlaces(p1);

        assertThat(ipf.getPlaceNumber(xt), is(17));

    }
}