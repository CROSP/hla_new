package ua.cn.stu.cs.ems.core.queues;

import java.util.ArrayList;
import org.junit.*;
import ua.cn.stu.cs.ems.core.Token;
import static ua.cn.stu.cs.ems.core.queues.TestQueueOrder.assertQueueOrder;
import ua.cn.stu.cs.ems.ecli.Value;

/**
 *
 * @author proger
 */
public class FIFOQueueTest {

    public FIFOQueueTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testFIFOQueue() {
        FIFOQueue queue = new FIFOQueue("FIFOQ");
        final Token t1 = new Token(); t1.setValue("a", new Value(1d));
        final Token t2 = new Token(); t2.setValue("a", new Value(2d));
        final Token t3 = new Token(); t3.setValue("a", new Value(3d));
        ArrayList<Token> inputOrder = new ArrayList<Token>() {{
            add(t1);
            add(t2);
            add(t3);
        }};

        assertQueueOrder(queue, inputOrder, inputOrder);
    }

}