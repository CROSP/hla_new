package ua.cn.stu.cs.ems.core.queues;

import java.util.List;
import static org.hamcrest.Matchers.*;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JMock;
import org.jmock.integration.junit4.JUnit4Mockery;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import org.junit.*;
import org.junit.runner.RunWith;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.aggregates.Aggregate;
import ua.cn.stu.cs.ems.core.models.Model;

/**
 *
 * @author proger
 */
@RunWith(JMock.class)
public class AbstractQueueTest {

    Mockery mockery = new JUnit4Mockery();

    public AbstractQueueTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testConnectInputPlace() {
        AbstractQueue queue = createAbstractQueueInstance();

        Place input = new Place("P");
        queue.connectInputPlace(input);

        assertThat(queue.getInputPlaces().size(), is(1));
        assertEquals(queue, input.getOutputActiveInstance());

        queue.connectInputPlace(null);

        assertThat(queue.getInputPlaces().size(), is(0));
        assertEquals(null, input.getOutputActiveInstance());
    }

    @Test
    public void testConnectOutputPlace() {
        AbstractQueue queue = createAbstractQueueInstance();

        Place output = new Place("P");
        queue.connectOutputPlace(output);

        assertThat(queue.getOutputPlaces().size(), is(1));
        assertEquals(queue, output.getInputActiveInstance());

        queue.connectOutputPlace(null);

        assertThat(queue.getOutputPlaces().size(), is(0));
        assertEquals(null, output.getInputActiveInstance());
    }

    @Test
    public void testListenersCallWhenElementAdded() {
        final AbstractQueue queue = createAbstractQueueInstance();
        final QueueStateListener listener = mockery.mock(QueueStateListener.class);

        queue.addQueueStateListener(listener);
        final Token token = new Token();

        mockery.checking(new Expectations() {{
            oneOf(listener).tokenAddedToQueue(queue, token);
        }});

        queue.add(token);
    }

    @Test
    public void testListenersCallWhenElementPeeked() {
        final AbstractQueue queue = createAbstractQueueInstance();
        final QueueStateListener listener = mockery.mock(QueueStateListener.class);

        queue.addQueueStateListener(listener);

        mockery.checking(new Expectations() {{
            oneOf(listener).tokenRemovedFromQueue(queue, null);
        }});

        queue.remove();
    }

    @Test
    public void testIfEmptyQueueAddsItselfToModel() {
        final AbstractQueue queue = createAbstractQueueInstance();
        Place outputPlace = new Place("P"); outputPlace.setToken(new Token());
        queue.connectOutputPlace(outputPlace);

        final Aggregate aggregate = mockery.mock(Aggregate.class, "aggregate");
        final Model model = mockery.mock(Model.class, "model");

        mockery.checking(new Expectations() {{
            allowing(aggregate).getModel(); will(returnValue(model));
            never(model).addDelayable(queue, 0);
        }});

        queue.setParentAggregate(aggregate);
        outputPlace.setToken(null);

    }

    @Test
    public void testDisconnectInputPlace() {
        Queue q = createAbstractQueueInstance();
        Place inputPlace = new Place("inputPlace");
        q.connectInputPlace(inputPlace);

        q.disconnectInputPlace(inputPlace.getName());

        assertThat(q.getInputPlace(), nullValue());
        assertThat(q.getInputPlaces().size(), is(0));
    }

    @Test
    public void testDisconnectOutputPlace() {
        Queue q = createAbstractQueueInstance();
        Place outputPlace = new Place("outputPlace");
        q.connectOutputPlace(outputPlace);

        q.disconnectOutputPlace(outputPlace.getName());

        assertThat(q.getOutputPlace(), nullValue());
        assertThat(q.getOutputPlaces().size(), is(0));
    }

    private AbstractQueue createAbstractQueueInstance() {
        return new AbstractQueueImpl("queue");
    }


    public class AbstractQueueImpl extends AbstractQueue {

        public AbstractQueueImpl(String name) {
            super(name);
        }

        public boolean isQueueEmpty() {
            return true;
        }

        public Token removeTokenFromQueue() {
            return null;
        }

        public void addToken(Token token) {
        }

        public List<Token> getTokensInQueue() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void clear() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public int length() {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }

}
