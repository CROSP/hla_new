package ua.cn.stu.cs.ems.core.utils;

import ua.cn.stu.cs.ems.core.Place;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ua.cn.stu.cs.ems.core.aggregates.AggregateInstance;
import static org.junit.Assert.*;
;import static org.hamcrest.CoreMatchers.*;;

/**
 *
 * @author proger
 */
public class JessUtilsTest {

    public JessUtilsTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testGetAggregateChildFullName() {
        AggregateInstance parent = getAggregateWithName("parent");
        AggregateInstance child = getAggregateWithName("child");
        AggregateInstance grandChild = getAggregateWithName("gc");

        parent.addChildAggregate(child);
        child.setParentAggregate(parent);

        child.addChildAggregate(grandChild);
        grandChild.setParentAggregate(child);

        Place place = new Place("place");
        grandChild.addPlace(place);
        place.setParentAggregate(grandChild);

        String expected = "parent.child.gc.place";
        String result = JessUtils.getAggregateChildFullName(place);
        assertThat(result, equalTo(expected));
    }

    private AggregateInstance getAggregateWithName(String name) {
        return new AggregateInstance(name);
    }

}