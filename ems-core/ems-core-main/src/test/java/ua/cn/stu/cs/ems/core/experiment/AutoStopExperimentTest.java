/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.cn.stu.cs.ems.core.experiment;

import static org.junit.Assert.assertTrue;
import org.junit.*;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.aggregates.Aggregate;
import ua.cn.stu.cs.ems.core.aggregates.AggregateChild;
import ua.cn.stu.cs.ems.core.aggregates.AggregateInstance;
import ua.cn.stu.cs.ems.core.experiment.statistics.StatisticsParameters;
import ua.cn.stu.cs.ems.core.models.ENetworksModel;
import ua.cn.stu.cs.ems.core.models.Model;
import ua.cn.stu.cs.ems.core.transitions.TTransition;
import ua.cn.stu.cs.ems.core.transitions.functions.ConstDelayFunction;
import ua.cn.stu.cs.ems.ecli.Value;

/**
 *
 * @author Dmitry Krivenko <dmitrykrivenko@gmal.com>
 */
public class AutoStopExperimentTest {

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test AutoStopExperiment with respond variable
     */
    @Test
    public void testAutoStopExperimentWithVariableRespond() {
        TTransition t1 = new TTransition("T1");
        TTransition t2 = new TTransition("T2");

        t1.setDelayFunction(new ConstDelayFunction(5));

        Place p0 = new Place("p0");
        Place p1 = new Place("p1");


        p0.setToken(new Token());

        t1.setInputPlace(p0);
        t1.setOutputPlace(p1);


        t2.setInputPlace(p1);
        t2.setOutputPlace(p0);


        Aggregate root = createRootAggregate();
        root.setVariable("var", new Value(0));
        root.addPlaces(p0, p1);
        root.addTransitions(t1, t2);

        final ENetworksModel model = new ENetworksModel();

        model.setRootAggregate(root);

        Respond respond = new Respond(root.getVariable("var"));
        TestAutoStopExperiment ause = new TestAutoStopExperiment(model, respond, null, 0.4d, 0.9d);

        model.setFinishTime(10D);
        ause.setRootAggregate(root);
        ause.startExperiment();
        assertTrue(ause.numberOfRuns == 34);
    }

    /**
     * Test AutoStopExperiment with respond average numerical characteristic
     */
    @Test
    public void testAutoStopExperimentWithAvgNumChRespond() {
        TTransition t1 = new TTransition("T1");
        TTransition t2 = new TTransition("T2");

        t1.setDelayFunction(new ConstDelayFunction(10));

        Place p0 = new Place("p0");
        Place p1 = new Place("p1");


        p0.setToken(new Token());

        t1.setInputPlace(p0);
        t1.setOutputPlace(p1);


        t2.setInputPlace(p1);
        t2.setOutputPlace(p0);


        Aggregate root = createRootAggregate();
        root.addPlaces(p0, p1);
        root.addTransitions(t1, t2);

        final ENetworksModel model = new ENetworksModel();

        model.setRootAggregate(root);

        Respond respond = new Respond(StatisticsParameters.NUMBER_OF_PASSED_TOKENS.getPresentation());
        TestAutoStopExperiment ause = new TestAutoStopExperiment(model, respond, p0, 0.5d, 0.9d);

        model.setFinishTime(50D);
        while (ause.numberOfRuns < 5) {
            ause.runAvgNumCh();
        }
        t1.setDelayFunction(new ConstDelayFunction(5));
        while (!ause.lastRun()) {
            ause.runAvgNumCh();
        }

        assertTrue(ause.numberOfRuns == 35);
    }

    private AggregateInstance createRootAggregate() {
        return new AggregateInstance("Root");
    }

    private class TestAutoStopExperiment extends AutoStopExperiment {

        public int numberOfRuns;
        private Aggregate rootAggregate;
        private double arr[] = {1, 2, 3, 4, 5};

        public TestAutoStopExperiment(Model model, Respond respondAutostop, AggregateChild instance,
                double interval, double probability) {
            super(model, respondAutostop, instance, interval, probability);
        }

        /**
         * Run for variable as respond of autostop
         */
        @Override
        public void run() {
            rootAggregate.setVariable("var", new Value(arr[numberOfRuns % arr.length]));
            super.run();
            numberOfRuns++;
        }

        /**
         * Run for average numerical characteristic as respond for autostop
         */
        public void runAvgNumCh() {
            super.run();
            numberOfRuns++;
        }

        public void setRootAggregate(Aggregate root) {
            this.rootAggregate = root;
        }
    }
}
