package ua.cn.stu.cs.ems.core.aggregates;

import org.junit.Ignore;
import java.util.HashSet;
import java.util.Set;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

/**
 *
 * @author proger
 */
public class AggregateInstanceTest {

    public AggregateInstanceTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

     @Test
    public void testGetAggregate() {
        AggregateInstance rootAggregate = new AggregateInstance("root");
        AggregateInstance aggregate1 = new AggregateInstance("A1");
        AggregateInstance aggregate2 = new AggregateInstance("A2");

        rootAggregate.addChildAggregate(aggregate1);
        rootAggregate.addChildAggregate(aggregate2);

        assertEquals(rootAggregate.getAggregate("A1"), aggregate1);
    }

    @Test
    public void testGetNonExistingAggregate() {
        Aggregate rootAggregate = new AggregateInstance("A");

        assertThat(rootAggregate.getAggregate("nonExisting"), nullValue());
    }

    @Ignore
    public void testAddChildAggregates() {
        AggregateInstance rootAggregate = new AggregateInstance("A");
        Set<AggregateInstance> aggregates = new HashSet<AggregateInstance>() {{
            add(new AggregateInstance("A1"));
            add(new AggregateInstance("A2"));
            add(new AggregateInstance("A3"));
        }};

        for (AggregateInstance a : aggregates) {
            rootAggregate.addChildAggregate(a);
        }

        
    }

    @Test
    public void testDualConnectionWithChildAggregates() {
        AggregateInstance parent = new AggregateInstance("parent");
        AggregateInstance child = new AggregateInstance("child");

        parent.addChildAggregate(child);
        assertEquals("Parent aggregate should be set when child aggregate added",
                child.getParentAggregate(), parent);

        parent.disconnectChildAggregate(child);
        assertThat("Parent aggregate should be null when child aggregate is dissconnected",
                child.getParentAggregate(), nullValue());
    }

}