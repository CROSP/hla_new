package ua.cn.stu.cs.ems.core.aggregates;

import ua.cn.stu.cs.ems.core.queues.AbstractQueue;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.jmock.Mockery;
import org.junit.runner.RunWith;
import org.jmock.integration.junit4.JMock;
import ua.cn.stu.cs.ems.core.models.Model;
import java.util.Set;
import java.util.HashSet;
import org.jmock.Expectations;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.*;


import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.transitions.TTransition;
import ua.cn.stu.cs.ems.core.transitions.Transition;
import ua.cn.stu.cs.ems.ecli.Value;
import static ua.cn.stu.cs.ems.core.testutils.Matchers.hasSameElements;
import static ua.cn.stu.cs.ems.core.testutils.MocksFactory.*;

/**
 *
 * @author proger
 */
/**
 *
 * @author proger
 */
@RunWith(JMock.class)
public class AbstractAggregateTest {

    Mockery mockery = new JUnit4Mockery();

    public AbstractAggregateTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testGetChildPositions() {
        AbstractAggregate aggregate = createAbstractAggregateImpl();
        final Place pos1 = new Place("P1");
        final Place pos2 = new Place("P2");

        aggregate.addPlace(pos1);
        aggregate.addPlace(pos2);

        Set<Place> expected = new HashSet<Place>() {{
            add(pos1);
            add(pos2);
        }};

        Set<Place> result = aggregate.getPlaces();

        assertThat(result, is(hasSameElements(expected)));
    }

    @Test
    public void testGetChildTransitions() {
        AbstractAggregate aggregate = createAbstractAggregateImpl();
        final TTransition tr1 = new TTransition("T1");
        final TTransition tr2 = new TTransition("T2");

        aggregate.addTransition(tr1);
        aggregate.addTransition(tr2);

        Set<Transition> expected = new HashSet<Transition>() {{
            add(tr1);
            add(tr2);
        }};

        Set<Transition> result = aggregate.getTransitions();

        assertThat(result, is(hasSameElements(expected)));
    }

    @Test
    public void testGetModel() {
        AbstractAggregate aggregate = createAbstractAggregateImpl();
        Model model = mockery.mock(Model.class, "model");
        
        aggregate.setModel(model);

        Model result = aggregate.getModel();

        assertThat(result, equalTo(model));
    }

    @Test
    public void testGetModelFromParent() {
        final Aggregate parentAggregate = mockery.mock(Aggregate.class, "parentAggregate");
        final Model model = mockery.mock(Model.class, "model");
        
        AbstractAggregate aggregate = createAbstractAggregateImpl();
        aggregate.setParentAggregate(parentAggregate);

        mockery.checking(new Expectations() {{
            allowing(parentAggregate).getModel(); will(returnValue(model));
        }});

        assertThat(aggregate.getModel(), equalTo(model));
    }

    @Test
    public void testGetModelFromParentWithNoModelSet() {
        AbstractAggregate parentAggregate = createAbstractAggregateImpl();
        parentAggregate.setModel(null);

        AbstractAggregate aggregate = createAbstractAggregateImpl();
        aggregate.setParentAggregate(parentAggregate);

        assertThat(aggregate.getModel(), nullValue());
    }

    @Test
    public void testGetOutputByName() {
        AbstractAggregate aggregate = createAbstractAggregateImpl();
        Output output1 = new OutputImpl("O1");
        Output output2 = new OutputImpl("O2");
        output1.changeName("output1");
        output2.changeName("output2");


        aggregate.addOutput(output1, 0);
        aggregate.addOutput(output2, 1);

        assertThat(aggregate.getOutput("output1"), equalTo(output1));
    }


    @Test
    public void testGetOutputByNonexistingName() {
        AbstractAggregate aggregate = createAbstractAggregateImpl();

        assertThat(aggregate.getOutput("nonExistingName"), nullValue());
    }

    @Test(expected=NullPointerException.class)
    public void testAddNullOutput() {
        AbstractAggregate aggregate = createAbstractAggregateImpl();

        aggregate.addOutput(null, 0);
    }

    @Test
    public void testGetOutputs() {
        AbstractAggregate aggregate = createAbstractAggregateImpl();
        Output output1 = new OutputImpl("O1");
        Output output2 = new OutputImpl("O2");

        output1.changeName("output1");
        output2.changeName("output2");

        aggregate.addOutput(output1, 0);
        aggregate.addOutput(output2, 1);

        assertThat(aggregate.getOutputs().get(0), equalTo(output1));
        assertThat(aggregate.getOutputs().get(1), equalTo(output2));
        assertThat(aggregate.getOutputs().size(), is(2));
    }

    @Test
    public void testGetInputByName() {
        AbstractAggregate aggregate = createAbstractAggregateImpl();
        Input input1 = new InputImpl("I1");
        Input input2 = new InputImpl("I2");
        input1.changeName("input1");
        input2.changeName("input2");

        aggregate.addInput(input1, 0);
        aggregate.addInput(input2, 1);

        assertThat(aggregate.getInput("input1"), equalTo(input1));
    }

    @Test
    public void testGetNonExistingInput() {
        Aggregate aggregate = createAbstractAggregateImpl();

        assertThat(aggregate.getInput("nonExistingInput"), nullValue());
    }

   

    @Test
    public void testDualConnectionWithChildTransitions() {
        Aggregate parent = createAbstractAggregateWithName("parent");
        Transition transition = createAbstractTransitionImpl();

        parent.addTransition(transition);
        assertThat("Parent agggregate should be set when child transition added",
                transition.getParentAggregate(), is(parent));

        parent.disconnectChildTransition(transition);
        assertThat("Parent aggregate should be null when child transition is dissconnected",
                transition.getParentAggregate(), nullValue());
    }

    @Test
    public void testDualConnectionWithChildPlaces() {
        Aggregate parent = createAbstractAggregateWithName("parent");
        Place place = new Place("P");

        parent.addPlace(place);
        assertThat("Parent aggregate should be set when child place added",
                place.getParentAggregate(), is(parent));

        parent.disconnectChildPlace(place);
        assertThat("Parent aggregate should be null when child place is dissconnected",
                place.getParentAggregate(), nullValue());
    }

    @Test
    public void testDualConnectionWithChildInputs() {
        Aggregate parent = createAbstractAggregateWithName("parent");
        Input input = new InputImpl("I");

        parent.addInput(input, 0);
        assertThat("Parent aggregate should be set when child input added",
                input.getParentAggregate(), is(parent));

        parent.disconnectInput(input);
        assertThat("Parent aggregate should be null when child input is dissconnected",
                input.getParentAggregate(), nullValue());
    }

    @Test
    public void testDualConnectionWithChildOutputs() {
        Aggregate parent = createAbstractAggregateWithName("parent");
        Output output = new OutputImpl("O");

        parent.addOutput(output, 0);
        assertThat("Parent aggregate should be set when child output added",
                output.getParentAggregate(), is(parent));

        parent.disconnectOutput(output);
        assertThat("Parent aggregate should be null when child output is dissconnected",
                output.getParentAggregate(), nullValue());
    }

    @Test
    public void testDualConnectionWithChildQueues() {
        Aggregate parent = createAbstractAggregateWithName("parent");
        AbstractQueue queue = createAbstractQueue();

        parent.addQueue(queue);
        assertThat("Parent aggregate should be set when child queue added",
                   queue.getParentAggregate(), is(parent));

        parent.disconnectQueue(queue);
        assertThat("Parent aggregate should be null when child output is dissconnected",
                   queue.getParentAggregate(), nullValue());
    }

    @Test
    public void testGetAggregateChild() {
        AbstractAggregate parent = createAbstractAggregateWithName("parent");

        assertThat("When getAggregateChild called with name of non existing child as a parametr it should return null",
                parent.getAggregateChild("noSuchChild"), nullValue());

        Place place = new Place("place");
        parent.addPlace(place);

        assertEquals(place, parent.getAggregateChild("place"));
    }

    @Test(expected=NameClashException.class)
    public void testNameClash() {
        String name = "name";
        AbstractAggregate parent = createAbstractAggregateWithName("parent");

        // Two objects has the same name - it's a name clash
        parent.addPlace(new Place(name));
        parent.addInput(new InputImpl(name), 0);
    }

    @Test
    public void testChildNameChange() {
        AbstractAggregate parent = createAbstractAggregateWithName("parent");
        final String place = "place";
        final String newName = "newName";
        Place p = new Place(place);

        parent.addPlace(p);        
        p.changeName(newName);
        
        assertThat(parent.getPlace(newName), equalTo(p));
        assertEquals(p, parent.getAggregateChild(newName));
        assertThat(parent.getPlace(place), equalTo(null));
        assertEquals(null, parent.getAggregateChild(place));
    }

    @Test
    public void testGetNonExistingVariable() {
        AbstractAggregate parent = createAbstractAggregateWithName("parent");
        assertThat(parent.getVariable("nonExistingOne"), nullValue());
    }

    @Test
    public void testSetVariable() {
        AbstractAggregate parent = createAbstractAggregateWithName("parent");
        Double value = new Double(123.456);
        String name = "varName";
        parent.setVariable(name, new Value(value));

        assertThat(parent.getVariable(name).getValue().doubleValue(), equalTo(value));
    }

    @Test
    public void testGetVariablesNames() {
        AbstractAggregate parent = createAbstractAggregateImpl();
        Set<String> varNames = new HashSet<String>() {{
            add("var1");
            add("var2");
            add("var3");
        }};

        for (String name : varNames) {
            parent.setVariable(name, new Value(0d));
        }

        assertThat("Aggregate.getVariablesNames should return names of all added varibles",
                parent.getVariablesNames(), hasSameElements(varNames));
    }

    @Test
    public void testRemoveVariable() {
        AbstractAggregate parent = createAbstractAggregateImpl();
        parent.setVariable("var1", new Value(123d));
        parent.setVariable("var2", new Value(321d));

        parent.removeVariable("var1");

        assertThat("If variable removed from aggregate aggregate.getVariable should return null",
                parent.getVariable("var1"), nullValue());

        Set<String> names = new HashSet<String>() {{ add("var2"); }};
        assertThat(parent.getVariablesNames(), hasSameElements(names));
    }

    @Test
    public void testGetInputNumber() {
        InputImpl i1 = new InputImpl("i1");
        InputImpl i2 = new InputImpl("i2");

        AbstractAggregate parent = createAbstractAggregateImpl();

        parent.addInput(i1, 0);
        parent.addInput(i2, 1);

        assertEquals(new Integer(0), parent.getInputNumber("i1"));
        assertEquals(new Integer(1), parent.getInputNumber("i2"));
        assertEquals(null, parent.getInputNumber("notExists"));
    }

    @Test
    public void testGetOutputNumber() {
        OutputImpl o1 = new OutputImpl("o1");
        OutputImpl o2 = new OutputImpl("o2");

        AbstractAggregate parent = createAbstractAggregateImpl();

        parent.addOutput(o1, 0);
        parent.addOutput(o2, 1);

        assertEquals(new Integer(0), parent.getOutputNumber("o1"));
        assertEquals(new Integer(1), parent.getOutputNumber("o2"));
        assertEquals(null, parent.getOutputNumber("notExists"));
    }

    @Test
    public void testClearVariables() {
        AbstractAggregate parent = createAbstractAggregateImpl();

        parent.setVariable("var1", new Value(1));
        parent.setVariable("var2", new Value(2));

        parent.clearVariables();

        assertEquals(0, parent.getVariablesNames().size());
    }

    private AbstractAggregate createAbstractAggregateImpl() {
        return new AbstractAggregateImpl("A");
    }


    private AbstractAggregate createAbstractAggregateWithName(String name) {
        return new AbstractAggregateImpl(name);
    }

}

class AbstractAggregateImpl extends AbstractAggregate {

    public AbstractAggregateImpl(String name) {
        super(name);
    }

}
