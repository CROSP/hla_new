package ua.cn.stu.cs.ems.core.experiment.statistics;

import org.jmock.Mockery;
import org.jmock.integration.junit4.JMock;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import ua.cn.stu.cs.ems.core.AbstractTransition;
import ua.cn.stu.cs.ems.core.testutils.MockModel;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

/**
 * @author proger
 */
@RunWith(JMock.class)
public class TransitionStatisticsCollectorTest {

    Mockery mockery = new JUnit4Mockery();

    public TransitionStatisticsCollectorTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Test
    // We assume that in real transition token is passed when delay is finished
    public void testNumberOfPassedTokens() {
        MockModel model = new MockModel();
        MockTransition transition = new MockTransition("name");

        TransitionStatisticsCollector tsc = new TransitionStatisticsCollector(model, transition);

        tsc.startStatCollection();

        transition.startDelay();
        transition.stopDelay();

        transition.startDelay();
        transition.stopDelay();

        transition.startDelay();
        transition.stopDelay();

        transition.startDelay();
        transition.stopDelay();

        tsc.stopStatCollection();

        StatisticsResult sr = tsc.getResult();

        assertThat(sr.getResultValue(StatisticsParameters.NUMBER_OF_PASSED_TOKENS), equalTo(4d));
    }

    @Test
    public void testNumberOfPassedTokensWithPause() {
        MockModel model = new MockModel();
        MockTransition transition = new MockTransition("name");

        TransitionStatisticsCollector tsc = new TransitionStatisticsCollector(model, transition);

        transition.startDelay();
        transition.stopDelay();

        transition.startDelay();
        transition.stopDelay();

        tsc.startStatCollection();

        transition.startDelay();
        transition.stopDelay();

        transition.startDelay();
        transition.stopDelay();

        tsc.stopStatCollection();

        StatisticsResult sr = tsc.getResult();
        assertThat(sr.getResultValue(StatisticsParameters.NUMBER_OF_PASSED_TOKENS), equalTo(2d));
    }

    @Test
    public void testNumberOfPassedTokensWithResets() {
        MockModel model = new MockModel();
        MockTransition transition = new MockTransition("name");

        TransitionStatisticsCollector tsc = new TransitionStatisticsCollector(model, transition);

        tsc.startStatCollection();

        transition.startDelay();
        transition.stopDelay();

        transition.startDelay();
        transition.stopDelay();

        transition.startDelay();
        transition.stopDelay();

        StatisticsResult sr = tsc.getResult();
        assertThat(sr.getResultValue(StatisticsParameters.NUMBER_OF_PASSED_TOKENS), equalTo(3d));
        tsc.reset();

        transition.startDelay();
        transition.stopDelay();

        tsc.stopStatCollection();

        sr = tsc.getResult();
        assertThat(sr.getResultValue(StatisticsParameters.NUMBER_OF_PASSED_TOKENS), equalTo(1d));
    }

    @Test
    public void testOccupiedCoefficient() {
        MockModel model = new MockModel();
        MockTransition transition = new MockTransition("name");

        TransitionStatisticsCollector tsc = new TransitionStatisticsCollector(model, transition);

        model.setFinishTime(25);

        tsc.startStatCollection();

        model.setModelTime(5);

        // Delay for 5
        transition.startDelay();
        model.setModelTime(10);
        transition.stopDelay();

        model.setModelTime(20);

        // Delay for 5
        transition.startDelay();
        model.setModelTime(25);
        transition.stopDelay();

        tsc.stopStatCollection();

        double expectedOccupiedCoefficient = 10d / 25d;
        StatisticsResult sr = tsc.getResult();
        assertThat(sr.getResultValue(StatisticsParameters.OCCUPIED_COEFFICIENT), equalTo(expectedOccupiedCoefficient));
    }

    @Test
    public void testOccupiedCoefficientWithPauses() {
        MockModel model = new MockModel();
        MockTransition transition = new MockTransition("name");

        TransitionStatisticsCollector tsc = new TransitionStatisticsCollector(model, transition);

        model.setFinishTime(25);

        transition.startDelay();
        model.setModelTime(5);

        tsc.startStatCollection();

        // Recorded delay is 5
        model.setModelTime(10);
        transition.stopDelay();

        model.setModelTime(20);
        transition.startDelay();

        model.setModelTime(25);
        tsc.stopStatCollection();

        model.setModelTime(200);

        double expectedOccupiedCoefficient = (5 + 5) / 20d;
        StatisticsResult sr = tsc.getResult();
        assertThat(sr.getResultValue(StatisticsParameters.OCCUPIED_COEFFICIENT), equalTo(expectedOccupiedCoefficient));
    }

    @Test
    public void testOccupiedCoefficientWithResets() {
        MockModel model = new MockModel();
        MockTransition transition = new MockTransition("name");

        TransitionStatisticsCollector tsc = new TransitionStatisticsCollector(model, transition);

        model.setFinishTime(50);

        transition.startDelay();
        model.setModelTime(5);

        tsc.startStatCollection();

        // Recorded delay is 5
        model.setModelTime(10);
        transition.stopDelay();

        model.setModelTime(15);

        // double expectedOccupiedCoefficient = 5 / 10d;
        double expectedOccupiedCoefficient = 5 / 45d;
        StatisticsResult sr = tsc.getResult();
        assertThat(sr.getResultValue(StatisticsParameters.OCCUPIED_COEFFICIENT), equalTo(expectedOccupiedCoefficient));

        tsc.reset();

        model.setModelTime(25);
        transition.startDelay();

        model.setModelTime(30);

        //expectedOccupiedCoefficient = 5 / 15d;
        expectedOccupiedCoefficient = 5 / 35d;
        sr = tsc.getResult();
        assertThat(sr.getResultValue(StatisticsParameters.OCCUPIED_COEFFICIENT), equalTo(expectedOccupiedCoefficient));

        tsc.reset();

        model.setModelTime(35);
        transition.stopDelay();
        model.setModelTime(45);

        tsc.stopStatCollection();

        // expectedOccupiedCoefficient = 5 / 15d;
        expectedOccupiedCoefficient = 5 / 20d;
        sr = tsc.getResult();
        assertThat(sr.getResultValue(StatisticsParameters.OCCUPIED_COEFFICIENT), equalTo(expectedOccupiedCoefficient));
    }

    private class MockTransition extends AbstractTransition {

        public MockTransition(String name) {
            super(name);
        }

        @Override
        protected boolean canFire() {
            return false;
        }

        @Override
        protected int maxInputSize() {
            return 0;
        }

        @Override
        protected int maxOutputSize() {
            return 0;
        }

        @Override
        protected void fireTransition() {
        }

        public void startDelay() {
            notifyDelayStarted();
        }

        public void stopDelay() {
            notifyDelayFinished();
        }
    }
}