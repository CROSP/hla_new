package ua.cn.stu.cs.ems.core.xml;

import org.custommonkey.xmlunit.XMLTestCase;
import org.jdom.Document;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.aggregates.*;
import ua.cn.stu.cs.ems.core.queues.*;
import ua.cn.stu.cs.ems.core.transitions.TTransition;
import ua.cn.stu.cs.ems.core.transitions.YTransition;
import ua.cn.stu.cs.ems.core.transitions.functions.InterpretedDelayFunction;
import ua.cn.stu.cs.ems.core.transitions.functions.InterpretedTransformationFunction;
import ua.cn.stu.cs.ems.core.utils.Coordinates;
import ua.cn.stu.cs.ems.ecli.Value;

import java.util.ArrayList;

/**
 * There are too many tests bellow. I wrote all of them because I don't know how
 * to make XMLUnit ignore difference in elements order. Because of that I had
 * to split single test into several tests.
 *
 * @author proger
 */
public class AggregateWriterTest extends XMLTestCase {

    public AggregateWriterTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {

    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Test
    public void testAggregateWithTransition() throws Exception {
        String expectedXml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<pnml xmlns='http://www.pnml.org/version-2009/grammar/pnml'>"
                + "<net id='root' type='http://www.cs.stu.cn.ua/jess/enets'>"
                + "<page id='root'>"

                + "<transition id='ValidT'>"
                + "<name>"
                + "<text>ValidT</text>"
                + "<graphics><offset x='20' y='30'/></graphics>"
                + "</name>"
                + "<definition type='transition' subType='T'>"
                + "<delayFunction type='ecli'>RETURN 123;</delayFunction>"
                + "<transformationFunction type='ecli'>T['attr'] = 123;</transformationFunction>"
                + "</definition>"
                + "<graphics><position x='100' y='200'/></graphics></transition>"

                + "</page></net></pnml>";
        AggregateDefinition ad = new AggregateDefinition("root");

        TTransition tt = new TTransition("ValidT");
        tt.setDelayFunction(new InterpretedDelayFunction("RETURN 123;"));
        tt.setTransformationFunction(new InterpretedTransformationFunction("T['attr'] = 123;"));

        tt.setNameCoordinates(new Coordinates(20, 30));
        tt.setObjectCoordinates(new Coordinates(100, 200));

        ad.addTransition(tt);

        assertGenerateValidXML(ad, expectedXml);
    }

    @Test
    public void testWriteYTransition() throws Exception {
        String xml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<pnml xmlns='http://www.pnml.org/version-2009/grammar/pnml'>"
                + "<net id='root' type='http://www.cs.stu.cn.ua/jess/enets'>"
                + "<page id='root'>"

                + "<transition id='Y'>"
                + "<name>"
                + "<text>Y</text>"
                + "<graphics><offset x='20' y='30'/></graphics>"
                + "</name>"
                + "<definition type='transition' subType='Y'></definition>"
                + "<graphics><position x='100' y='200'/></graphics></transition>"

                + "</page></net></pnml>";

        AggregateDefinition ad = new AggregateDefinition("root");

        YTransition ty = new YTransition("Y");
        ty.setNameCoordinates(new Coordinates(20, 30));
        ty.setObjectCoordinates(new Coordinates(100, 200));
        ad.addTransition(ty);

        assertGenerateValidXML(ad, xml);
    }

    @Test
    public void testWritingFIFOQueue() throws Exception {
        String xml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<pnml xmlns='http://www.pnml.org/version-2009/grammar/pnml'>"
                + "<net id='root' type='http://www.cs.stu.cn.ua/jess/enets'>"
                + "<page id='root'>"

                + "<transition id='FIFO'>"
                + "<name><text>FIFO</text><graphics><offset x='1' y='2'/></graphics></name>"
                + "<definition type='queue' subType='FIFO'></definition>"
                + "<graphics><position x='100' y='200'/></graphics>"
                + "</transition>"

                + "</page></net></pnml>";

        AggregateDefinition ad = new AggregateDefinition("root");
        FIFOQueue fifo = new FIFOQueue("FIFO");
        fifo.setNameCoordinates(new Coordinates(1, 2));
        fifo.setObjectCoordinates(new Coordinates(100, 200));
        ad.addQueue(fifo);

        assertGenerateValidXML(ad, xml);
    }

    @Test
    public void testWritingLIFOQueue() throws Exception {
        String xml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<pnml xmlns='http://www.pnml.org/version-2009/grammar/pnml'>"
                + "<net id='root' type='http://www.cs.stu.cn.ua/jess/enets'>"
                + "<page id='root'>"

                + "<transition id='LIFO'>"
                + "<name><text>LIFO</text><graphics><offset x='1' y='2'/></graphics></name>"
                + "<definition type='queue' subType='LIFO'></definition>"
                + "<graphics><position x='100' y='200'/></graphics>"
                + "</transition>"

                + "</page></net></pnml>";

        AggregateDefinition ad = new AggregateDefinition("root");
        LIFOQueue lifo = new LIFOQueue("LIFO");
        lifo.setNameCoordinates(new Coordinates(1, 2));
        lifo.setObjectCoordinates(new Coordinates(100, 200));
        ad.addQueue(lifo);

        assertGenerateValidXML(ad, xml);
    }

    @Test
    public void testWritingPriorityFIFOQueue() throws Exception {
        String xml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<pnml xmlns='http://www.pnml.org/version-2009/grammar/pnml'>"
                + "<net id='root' type='http://www.cs.stu.cn.ua/jess/enets'>"
                + "<page id='root'>"

                + "<transition id='PriorityFIFO'>"
                + "<name><text>PriorityFIFO</text><graphics><offset x='1' y='2'/></graphics></name>"
                + "<definition type='queue' subType='PriorityFIFO'><priorityFunction>RETURN 2;</priorityFunction></definition>"
                + "<graphics><position x='100' y='200'/></graphics>"
                + "</transition>"

                + "</page></net></pnml>";


        AggregateDefinition ad = new AggregateDefinition("root");
        FIFOPriorityQueue pfifo = new FIFOPriorityQueue("PriorityFIFO", new InterpretedTokenPriorityFunction("RETURN 2;"));
        pfifo.setNameCoordinates(new Coordinates(1, 2));
        pfifo.setObjectCoordinates(new Coordinates(100, 200));
        ad.addQueue(pfifo);

        assertGenerateValidXML(ad, xml);
    }

    @Test
    public void testWritingPriorityLIFOQueue() throws Exception {
        String xml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<pnml xmlns='http://www.pnml.org/version-2009/grammar/pnml'>"
                + "<net id='root' type='http://www.cs.stu.cn.ua/jess/enets'>"
                + "<page id='root'>"

                + "<transition id='PriorityLIFO'>"
                + "<name><text>PriorityLIFO</text><graphics><offset x='1' y='2'/></graphics></name>"
                + "<definition type='queue' subType='PriorityLIFO'><priorityFunction>RETURN 1;</priorityFunction></definition>"
                + "<graphics><position x='100' y='200'/></graphics>"
                + "</transition>"

                + "</page></net></pnml>";

        AggregateDefinition ad = new AggregateDefinition("root");
        LIFOPriorityQueue plifo = new LIFOPriorityQueue("PriorityLIFO", new InterpretedTokenPriorityFunction("RETURN 1;"));
        plifo.setNameCoordinates(new Coordinates(1, 2));
        plifo.setObjectCoordinates(new Coordinates(100, 200));
        ad.addQueue(plifo);

        assertGenerateValidXML(ad, xml);
    }

    @Test
    public void testCreateEmptyPlace() throws Exception {
        String xml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<pnml xmlns='http://www.pnml.org/version-2009/grammar/pnml'>"
                + "<net id='root' type='http://www.cs.stu.cn.ua/jess/enets'>"
                + "<page id='root'>"

                + "<place id='place'>"
                + "<name><text>place</text><graphics><offset x='1' y='2'/></graphics></name>"
                + "<graphics><position x='100' y='200'/></graphics>"
                + "</place>"

                + "</page></net></pnml>";

        AggregateDefinition ad = new AggregateDefinition("root");
        Place p = new Place("place");
        ad.addPlace(p);
        p.setNameCoordinates(new Coordinates(1, 2));
        p.setObjectCoordinates(new Coordinates(100, 200));

        assertGenerateValidXML(ad, xml);
    }

    @Test
    public void testCreatePlaceWithEmptyToken() throws Exception {
        String xml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<pnml xmlns='http://www.pnml.org/version-2009/grammar/pnml'>"
                + "<net id='root' type='http://www.cs.stu.cn.ua/jess/enets'>"
                + "<page id='root'>"

                + "<place id='place'>"
                + "<name><text>place</text><graphics><offset x='1' y='2'/></graphics></name>"
                + "<token/>"
                + "<graphics><position x='100' y='200'/></graphics>"
                + "</place>"

                + "</page></net></pnml>";

        AggregateDefinition ad = new AggregateDefinition("root");
        Place p = new Place("place");
        p.setToken(new Token());
        ad.addPlace(p);
        p.setNameCoordinates(new Coordinates(1, 2));
        p.setObjectCoordinates(new Coordinates(100, 200));

        assertGenerateValidXML(ad, xml);
    }

    @Test
    public void testCreatePlaceWithTokenWithAttributes() throws Exception {
        String xml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<pnml xmlns='http://www.pnml.org/version-2009/grammar/pnml'>"
                + "<net id='root' type='http://www.cs.stu.cn.ua/jess/enets'>"
                + "<page id='root'>"

                + "<place id='place'>"
                + "<name><text>place</text><graphics><offset x='1' y='2'/></graphics></name>"
                + "<token><attribute name='val1' value='12.1' /></token>"
                + "<graphics><position x='100' y='200'/></graphics>"
                + "</place>"

                + "</page></net></pnml>";

        AggregateDefinition ad = new AggregateDefinition("root");
        Place p = new Place("place");
        Token t = new Token();
        t.setValue("val1", new Value(12.1d));
        p.setToken(t);
        ad.addPlace(p);
        p.setNameCoordinates(new Coordinates(1, 2));
        p.setObjectCoordinates(new Coordinates(100, 200));

        assertGenerateValidXML(ad, xml);
    }

    @Test
    public void testCreateInput() throws Exception {
        String xml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<pnml xmlns='http://www.pnml.org/version-2009/grammar/pnml'>"
                + "<net id='root' type='http://www.cs.stu.cn.ua/jess/enets'>"
                + "<page id='root'>"

                + "<transition id='Input'>"
                + "<name><text>Input</text><graphics><offset x='1' y='2'/></graphics></name>"
                + "<definition type='input'/>"
                + "<graphics><position x='100' y='200'/></graphics>"
                + "</transition>"

                + "</page></net></pnml>";

        AggregateDefinition ad = new AggregateDefinition("root");
        Input i = new InputImpl("Input");
        i.setNameCoordinates(new Coordinates(1, 2));
        i.setObjectCoordinates(new Coordinates(100, 200));
        ad.addInput(i, 0);

        assertGenerateValidXML(ad, xml);
    }

    @Test
    public void testCreateOutput() throws Exception {
        String xml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<pnml xmlns='http://www.pnml.org/version-2009/grammar/pnml'>"
                + "<net id='root' type='http://www.cs.stu.cn.ua/jess/enets'>"
                + "<page id='root'>"

                + "<transition id='Output'>"
                + "<name><text>Output</text><graphics><offset x='1' y='2'/></graphics></name>"
                + "<definition type='output'/>"
                + "<graphics><position x='100' y='200'/></graphics>"
                + "</transition>"

                + "</page></net></pnml>";

        AggregateDefinition ad = new AggregateDefinition("root");
        Output o = new OutputImpl("Output");
        o.setNameCoordinates(new Coordinates(1, 2));
        o.setObjectCoordinates(new Coordinates(100, 200));
        ad.addOutput(o, 0);

        assertGenerateValidXML(ad, xml);
    }

    @Test
    public void testCreateAggregateChild() throws Exception {
        String xml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<pnml xmlns='http://www.pnml.org/version-2009/grammar/pnml'>"
                + "<net id='root' type='http://www.cs.stu.cn.ua/jess/enets'>"
                + "<page id='root'>"

                + "<transition id='A'>"
                + "<name><text>A</text><graphics><offset x='1' y='2'/></graphics></name>"
                + "<definition type='aggregate' subType='child'/>"
                + "<graphics><position x='100' y='200'/></graphics>"
                + "</transition>"

                + "</page></net></pnml>";

        AggregateDefinition child = new AggregateDefinition("child");
        AggregateDefinition ad = new AggregateDefinition("root");
        AggregateDefinitionReference adr = ad.addChildAggreagateDefinition(child, "A");
        adr.setNameCoordinates(new Coordinates(1, 2));
        adr.setObjectCoordinates(new Coordinates(100, 200));

        assertGenerateValidXML(ad, xml);
    }

    @Test
    public void testCreateInputOutputArc() throws Exception {
        String xml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<pnml xmlns='http://www.pnml.org/version-2009/grammar/pnml'>"
                + "<net id='root' type='http://www.cs.stu.cn.ua/jess/enets'>"
                + "<page id='root'>"

                + "<arc  source='A.output' target='A.input'>"
                + "<graphics/>"
                + "</arc>"

                + "<transition id='A'>"
                + "<name><text>A</text><graphics><offset x='1' y='2'/></graphics></name>"
                + "<definition type='aggregate' subType='child'/>"
                + "<graphics><position x='100' y='200'/></graphics>"
                + "</transition>"

                + "</page></net></pnml>";

        AggregateDefinition child = new AggregateDefinition("child");
        child.addInput(new InputImpl("input"), 0);
        child.addOutput(new OutputImpl("output"), 0);

        AggregateDefinition ad = new AggregateDefinition("root");
        AggregateDefinitionReference adr = ad.addChildAggreagateDefinition(child, "A");

        Input childInput = adr.getInput("input");
        Output childOutput = adr.getOutput("output");
        childOutput.addInput(childInput, 0);

        adr.setNameCoordinates(new Coordinates(1, 2));
        adr.setObjectCoordinates(new Coordinates(100, 200));
        ad.addArcCoordinates(new ArcCoordinates("A", "output", "A", "input", new ArrayList<Coordinates>()));

        assertGenerateValidXML(ad, xml);
    }


    @Test
    public void testCreateInputPlaceArc() throws Exception {
        String xml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<pnml xmlns='http://www.pnml.org/version-2009/grammar/pnml'>"
                + "<net id='root' type='http://www.cs.stu.cn.ua/jess/enets'>"
                + "<page id='root'>"

                + "<place id='place'>"
                + "<name><text>place</text><graphics><offset x='1' y='2'/></graphics></name>"
                + "<graphics><position x='100' y='200'/></graphics>"
                + "</place>"

                + "<arc  source='place' target='A.input'>"
                + "<graphics/>"
                + "</arc>"

                + "<transition id='A'>"
                + "<name><text>A</text><graphics><offset x='1' y='2'/></graphics></name>"
                + "<definition type='aggregate' subType='child'/>"
                + "<graphics><position x='100' y='200'/></graphics>"
                + "</transition>"

                + "</page></net></pnml>";

        AggregateDefinition child = new AggregateDefinition("child");
        child.addInput(new InputImpl("input"), 0);

        AggregateDefinition ad = new AggregateDefinition("root");
        AggregateDefinitionReference adr = ad.addChildAggreagateDefinition(child, "A");

        Place p = new Place("place");
        p.setNameCoordinates(new Coordinates(1, 2));
        p.setObjectCoordinates(new Coordinates(100, 200));

        Input childInput = adr.getInput("input");
        childInput.setInputPlace(p);
        ad.addPlace(p);

        adr.setNameCoordinates(new Coordinates(1, 2));
        adr.setObjectCoordinates(new Coordinates(100, 200));

        assertGenerateValidXML(ad, xml);
    }

    @Test
    public void testCreateOutputPlaceArc() throws Exception {
        String xml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<pnml xmlns='http://www.pnml.org/version-2009/grammar/pnml'>"
                + "<net id='root' type='http://www.cs.stu.cn.ua/jess/enets'>"
                + "<page id='root'>"

                + "<place id='place'>"
                + "<name><text>place</text><graphics><offset x='1' y='2'/></graphics></name>"
                + "<graphics><position x='100' y='200'/></graphics>"
                + "</place>"

                + "<arc source='A.output' target='place'>"
                + "<graphics/>"
                + "</arc>"

                + "<transition id='A'>"
                + "<name><text>A</text><graphics><offset x='1' y='2'/></graphics></name>"
                + "<definition type='aggregate' subType='child'/>"
                + "<graphics><position x='100' y='200'/></graphics>"
                + "</transition>"

                + "</page></net></pnml>";

        AggregateDefinition child = new AggregateDefinition("child");
        child.addOutput(new OutputImpl("output"), 0);

        AggregateDefinition ad = new AggregateDefinition("root");
        AggregateDefinitionReference adr = ad.addChildAggreagateDefinition(child, "A");

        Place p = new Place("place");
        p.setNameCoordinates(new Coordinates(1, 2));
        p.setObjectCoordinates(new Coordinates(100, 200));

        Output childOutput = adr.getOutput("output");
        childOutput.connectOutputPlace(p);
        ad.addPlace(p);

        adr.setNameCoordinates(new Coordinates(1, 2));
        adr.setObjectCoordinates(new Coordinates(100, 200));

        assertGenerateValidXML(ad, xml);
    }

    @Test
    public void testQueueInputPlaceArcs() throws Exception {
        String xml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<pnml xmlns='http://www.pnml.org/version-2009/grammar/pnml'>"
                + "<net id='root' type='http://www.cs.stu.cn.ua/jess/enets'>"
                + "<page id='root'>"

                + "<place id='input'>"
                + "<name><text>input</text><graphics><offset x='1' y='2'/></graphics></name>"
                + "<graphics><position x='100' y='200'/></graphics>"
                + "</place>"

                + "<arc  source='input' target='queue'/>"

                + "<transition id='queue'>"
                + "<name><text>queue</text><graphics><offset x='1' y='2'/></graphics></name>"
                + "<definition type='queue' subType='FIFO'/>"
                + "<graphics><position x='100' y='200'/></graphics>"
                + "</transition>"

                + "</page></net></pnml>";

        AggregateDefinition ad = new AggregateDefinition("root");

        FIFOQueue fifo = new FIFOQueue("queue");
        fifo.setNameCoordinates(new Coordinates(1, 2));
        fifo.setObjectCoordinates(new Coordinates(100, 200));

        Place input = new Place("input");
        input.setNameCoordinates(new Coordinates(1, 2));
        input.setObjectCoordinates(new Coordinates(100, 200));

        fifo.connectInputPlace(input);

        ad.addQueue(fifo);
        ad.addPlace(input);

        assertGenerateValidXML(ad, xml);
    }

    @Test
    public void testQueueOutputPlaceArcs() throws Exception {
        String xml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<pnml xmlns='http://www.pnml.org/version-2009/grammar/pnml'>"
                + "<net id='root' type='http://www.cs.stu.cn.ua/jess/enets'>"
                + "<page id='root'>"

                + "<place id='output'>"
                + "<name><text>output</text><graphics><offset x='1' y='2'/></graphics></name>"
                + "<graphics><position x='100' y='200'/></graphics>"
                + "</place>"

                + "<arc  source='queue' target='output'/>"

                + "<transition id='queue'>"
                + "<name><text>queue</text><graphics><offset x='1' y='2'/></graphics></name>"
                + "<definition type='queue' subType='FIFO'/>"
                + "<graphics><position x='100' y='200'/></graphics>"
                + "</transition>"

                + "</page></net></pnml>";

        AggregateDefinition ad = new AggregateDefinition("root");

        FIFOQueue fifo = new FIFOQueue("queue");
        fifo.setNameCoordinates(new Coordinates(1, 2));
        fifo.setObjectCoordinates(new Coordinates(100, 200));

        Place output = new Place("output");
        output.setNameCoordinates(new Coordinates(1, 2));
        output.setObjectCoordinates(new Coordinates(100, 200));
        fifo.connectOutputPlace(output);

        ad.addQueue(fifo);
        ad.addPlace(output);

        assertGenerateValidXML(ad, xml);
    }

    @Test
    public void testTransitionInputPlaceArc() throws Exception {
        String xml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<pnml xmlns='http://www.pnml.org/version-2009/grammar/pnml'>"
                + "<net id='root' type='http://www.cs.stu.cn.ua/jess/enets'>"
                + "<page id='root'>"

                + "<place id='input'>"
                + "<name><text>input</text><graphics><offset x='1' y='2'/></graphics></name>"
                + "<graphics><position x='100' y='200'/></graphics>"
                + "</place>"

                + "<arc  source='input' target='T'/>"

                + "<transition id='T'>"
                + "<name><text>T</text><graphics><offset x='1' y='2'/></graphics></name>"
                + "<definition type='transition' subType='T'/>"
                + "<graphics><position x='100' y='200'/></graphics>"
                + "</transition>"

                + "</page></net></pnml>";

        AggregateDefinition ad = new AggregateDefinition("root");

        TTransition tt = new TTransition("T");
        tt.setNameCoordinates(new Coordinates(1, 2));
        tt.setObjectCoordinates(new Coordinates(100, 200));

        Place input = new Place("input");
        input.setNameCoordinates(new Coordinates(1, 2));
        input.setObjectCoordinates(new Coordinates(100, 200));

        tt.setInputPlace(input);

        ad.addTransition(tt);
        ad.addPlace(input);

        assertGenerateValidXML(ad, xml);
    }

    @Test
    public void testTransitionOutputPlaceArc() throws Exception {
        String xml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<pnml xmlns='http://www.pnml.org/version-2009/grammar/pnml'>"
                + "<net id='root' type='http://www.cs.stu.cn.ua/jess/enets'>"

                + "<page id='root'>"

                + "<place id='output'>"
                + "<name><text>output</text><graphics><offset x='1' y='2'/></graphics></name>"
                + "<graphics><position x='100' y='200'/></graphics>"
                + "</place>"

                + "<arc  source='T' target='output'/>"

                + "<transition id='T'>"
                + "<name><text>T</text><graphics><offset x='1' y='2'/></graphics></name>"
                + "<definition type='transition' subType='T'/>"
                + "<graphics><position x='100' y='200'/></graphics>"
                + "</transition>"

                + "</page></net></pnml>";

        AggregateDefinition ad = new AggregateDefinition("root");

        TTransition tt = new TTransition("T");
        tt.setNameCoordinates(new Coordinates(1, 2));
        tt.setObjectCoordinates(new Coordinates(100, 200));

        Place output = new Place("output");
        output.setNameCoordinates(new Coordinates(1, 2));
        output.setObjectCoordinates(new Coordinates(100, 200));

        tt.setOutputPlace(output);

        ad.addTransition(tt);
        ad.addPlace(output);

        assertGenerateValidXML(ad, xml);
    }

    @Test
    public void testWriteVariables() throws Exception {
        String xml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<pnml xmlns='http://www.pnml.org/version-2009/grammar/pnml'>"
                + "<net id='root' type='http://www.cs.stu.cn.ua/jess/enets'>"

                + "<variables><variable name='var' value='12' /></variables>"

                + "<page id='root'>"
                + "</page></net></pnml>";

        AggregateDefinition ad = new AggregateDefinition("root");

        ad.setVariable("var", new Value(12));

        assertGenerateValidXML(ad, xml);
    }

    @Test
    public void testWriteInputPlaceArc() throws Exception {
        String xml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<pnml xmlns='http://www.pnml.org/version-2009/grammar/pnml'>"
                + "<net id='root' type='http://www.cs.stu.cn.ua/jess/enets'>"

                + "<page id='root'>"

                + "<place id='P'>"
                + "<name><text>P</text><graphics><offset x='1' y='2'/></graphics></name>"
                + "<graphics><position x='100' y='200'/></graphics>"
                + "</place>"

                + "<arc  source='I' target='P'/>"

                + "<transition id='I'>"
                + "<name><text>I</text><graphics><offset x='1' y='2'/></graphics></name>"
                + "<definition type='input'/>"
                + "<graphics><position x='100' y='200'/></graphics>"
                + "</transition>"

                + "</page></net></pnml>";

        AggregateDefinition ad = new AggregateDefinition("root");

        Input i = new InputImpl("I");
        i.setNameCoordinates(new Coordinates(1, 2));
        i.setObjectCoordinates(new Coordinates(100, 200));

        Place P = new Place("P");
        P.setNameCoordinates(new Coordinates(1, 2));
        P.setObjectCoordinates(new Coordinates(100, 200));

        i.setOutputPlace(P);

        ad.addInput(i, 0);
        ad.addPlace(P);

        assertGenerateValidXML(ad, xml);
    }

    @Test
    public void testWriteOutputPlaceArc() throws Exception {
        String xml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<pnml xmlns='http://www.pnml.org/version-2009/grammar/pnml'>"
                + "<net id='root' type='http://www.cs.stu.cn.ua/jess/enets'>"

                + "<page id='root'>"

                + "<place id='P'>"
                + "<name><text>P</text><graphics><offset x='1' y='2'/></graphics></name>"
                + "<graphics><position x='100' y='200'/></graphics>"
                + "</place>"

                + "<arc  source='P' target='O'/>"

                + "<transition id='O'>"
                + "<name><text>O</text><graphics><offset x='1' y='2'/></graphics></name>"
                + "<definition type='output'/>"
                + "<graphics><position x='100' y='200'/></graphics>"
                + "</transition>"

                + "</page></net></pnml>";

        AggregateDefinition ad = new AggregateDefinition("root");

        Output o = new OutputImpl("O");
        o.setNameCoordinates(new Coordinates(1, 2));
        o.setObjectCoordinates(new Coordinates(100, 200));

        Place P = new Place("P");
        P.setNameCoordinates(new Coordinates(1, 2));
        P.setObjectCoordinates(new Coordinates(100, 200));

        o.connectInputPlace(P);

        ad.addOutput(o, 0);
        ad.addPlace(P);

        assertGenerateValidXML(ad, xml);
    }

    @Test
    public void testWriteArcCoordinates() throws Exception {
        String xml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<pnml xmlns='http://www.pnml.org/version-2009/grammar/pnml'>"
                + "<net id='root' type='http://www.cs.stu.cn.ua/jess/enets'>"

                + "<page id='root'>"

                + "<place id='P'>"
                + "<name><text>P</text><graphics><offset x='1' y='2'/></graphics></name>"
                + "<graphics><position x='100' y='200'/></graphics>"
                + "</place>"

                + "<arc source='T' target='P'>"
                + "<graphics>"
                + "<position x='10' y='40'/>"
                + "<position x='20' y='50'/>"
                + "<position x='30' y='60'/>"
                + "</graphics>"
                + "</arc>"

                + "<transition id='T'>"
                + "<name><text>T</text><graphics><offset x='1' y='2'/></graphics></name>"
                + "<definition type='transition' subType='T'/>"
                + "<graphics><position x='100' y='200'/></graphics>"
                + "</transition>"

                + "</page></net></pnml>";

        AggregateDefinition ad = new AggregateDefinition("root");

        Place p = new Place("P");
        p.setNameCoordinates(new Coordinates(1, 2));
        p.setObjectCoordinates(new Coordinates(100, 200));

        TTransition t = new TTransition("T");
        t.setNameCoordinates(new Coordinates(1, 2));
        t.setObjectCoordinates(new Coordinates(100, 200));
        t.setOutputPlace(p);

        ad.addTransition(t);
        ad.addPlace(p);

        ArrayList<Coordinates> coordinates = new ArrayList<Coordinates>() {{
            add(new Coordinates(10, 40));
            add(new Coordinates(20, 50));
            add(new Coordinates(30, 60));
        }};
        ArcCoordinates ac = new ArcCoordinates(null, "T", null, "P", coordinates);
        ad.addArcCoordinates(ac);

        assertGenerateValidXML(ad, xml);
    }

    @Test
    public void testNotWriteArcIfTransitionWasDeleted() throws Exception {
        String xml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<pnml xmlns='http://www.pnml.org/version-2009/grammar/pnml'>"
                + "<net id='root' type='http://www.cs.stu.cn.ua/jess/enets'>"

                + "<page id='root'>"

                + "<place id='P'>"
                + "<name><text>P</text><graphics><offset x='1' y='2'/></graphics></name>"
                + "<graphics><position x='100' y='200'/></graphics>"
                + "</place>"

                + "</page></net></pnml>";

        AggregateDefinition ad = new AggregateDefinition("root");

        Place p = new Place("P");
        p.setNameCoordinates(new Coordinates(1, 2));
        p.setObjectCoordinates(new Coordinates(100, 200));

        TTransition t = new TTransition("T");
        t.setNameCoordinates(new Coordinates(1, 2));
        t.setObjectCoordinates(new Coordinates(100, 200));
        t.setOutputPlace(p);

        ad.addTransition(t);
        ad.addPlace(p);

        ArrayList<Coordinates> coordinates = new ArrayList<Coordinates>() {{
            add(new Coordinates(10, 40));
            add(new Coordinates(20, 50));
            add(new Coordinates(30, 60));
        }};
        ArcCoordinates ac = new ArcCoordinates(null, "T", null, "P", coordinates);
        ad.addArcCoordinates(ac);

        // Delete transition from aggregate defintion
        ad.disconnectChildTransition(t);

        assertGenerateValidXML(ad, xml);
    }

    @Test
    public void testNotWriteArcIfPlaceWasDeleted() throws Exception {
        String xml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<pnml xmlns='http://www.pnml.org/version-2009/grammar/pnml'>"
                + "<net id='root' type='http://www.cs.stu.cn.ua/jess/enets'>"

                + "<page id='root'>"

                + "<transition id='T'>"
                + "<name><text>T</text><graphics><offset x='1' y='2'/></graphics></name>"
                + "<definition type='transition' subType='T'/>"
                + "<graphics><position x='100' y='200'/></graphics>"
                + "</transition>"

                + "</page></net></pnml>";

        AggregateDefinition ad = new AggregateDefinition("root");

        Place p = new Place("P");
        p.setNameCoordinates(new Coordinates(1, 2));
        p.setObjectCoordinates(new Coordinates(100, 200));

        TTransition t = new TTransition("T");
        t.setNameCoordinates(new Coordinates(1, 2));
        t.setObjectCoordinates(new Coordinates(100, 200));
        t.setOutputPlace(p);

        ad.addTransition(t);
        ad.addPlace(p);

        ArrayList<Coordinates> coordinates = new ArrayList<Coordinates>() {{
            add(new Coordinates(10, 40));
            add(new Coordinates(20, 50));
            add(new Coordinates(30, 60));
        }};
        ArcCoordinates ac = new ArcCoordinates(null, "T", null, "P", coordinates);
        ad.addArcCoordinates(ac);

        // Delete place from aggregate defintion
        ad.disconnectChildPlace(p);

        assertGenerateValidXML(ad, xml, true);
    }

    private void assertGenerateValidXML(AggregateDefinition ad, String expectedXml) throws Exception {
        assertGenerateValidXML(ad, expectedXml, false);
    }

    private void assertGenerateValidXML(AggregateDefinition ad, String expectedXml, boolean output) throws Exception {
        Document dom = new AggregateWriter().createDOMFromAggregateDefinition(ad);

        XMLUtils utils = new XMLUtils();
        utils.assertDOMToXML(dom, expectedXml, output);
    }

}