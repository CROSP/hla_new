package ua.cn.stu.cs.ems.core.aggregates;

import ua.cn.stu.cs.ems.core.queues.Queue;
import ua.cn.stu.cs.ems.core.transitions.Transition;
import java.util.List;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.queues.FIFOQueue;
import ua.cn.stu.cs.ems.core.transitions.TTransition;
import static org.junit.Assert.*;
import static ua.cn.stu.cs.ems.core.testutils.InstanceFactory.*;
;import static org.hamcrest.CoreMatchers.*;;
/**
 *
 * @author proger
 */
public class AggregateValidatorTest {

    public AggregateValidatorTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Test
    public void validateEmptyAggregate() {
        Aggregate emptyAggregate = createAggregateInstance();
        ValidationResult result = validate(emptyAggregate, true);

        List<ValidationError> errors = result.getErrors();
        assertThat(errors.size(), is(1));

        ValidationError error = errors.get(0);
        assertThat(error.getParents().isEmpty(), is(true));
        assertEquals(error.getObjectWithError(), emptyAggregate);
    }

    @Test
    public void validateAggregateWithNotConnectedPlace() {
        Aggregate parent = createAggregateInstance();
        Place p = new Place("P");        
        parent.addPlace(p);

        ValidationResult result = validate(parent, true);

        List<ValidationError> errors = result.getErrors();
        assertThat(errors.size(), is(1));

        ValidationError error = errors.get(0);
        assertThat(error.getParents().get(0), is(parent));
        assertEquals(p, error.getObjectWithError());
    }

    @Test
    public void validateAggregateWithPlaceThatHasOnlyOneConnection() {
        Aggregate parent = createAggregateInstance();
        Place p = new Place("P");
        Input i = new InputImpl("i");
        i.setInputPlace(p);
        parent.addPlace(p);

        ValidationResult result = validate(parent, true);

        List<ValidationError> errors = result.getErrors();
        assertThat(errors.size(), is(0));
    }

    @Test
    public void validateAggregateWithNotConnectedTransition() {
        Aggregate parent = createAggregateInstance();
        Transition t = new TTransition("T");
        
        parent.addTransition(t);

        ValidationResult result = validate(parent, true);

        List<ValidationError> errors = result.getErrors();
        assertThat(errors.size(), is(1));

        ValidationError error = errors.get(0);
        assertThat(error.getParents().get(0), is(parent));
        assertEquals(t, error.getObjectWithError());
    }

    @Test
    public void validateAggregateWithNotConnectedQueue() {
        Aggregate parent = createAggregateInstance();
        Queue q = new FIFOQueue("queue");
        
        parent.addQueue(q);

        ValidationResult result = validate(parent, true);

        List<ValidationError> errors = result.getErrors();
        assertThat(errors.size(), is(1));

        ValidationError error = errors.get(0);
        assertThat(error.getParents().get(0), is(parent));
        assertEquals(q, error.getObjectWithError());
    }

    @Test
    public void validateAggregateWithNotConnectedInput() {
        Aggregate parent = createAggregateInstance();
        Input i = new InputImpl("ii");

        parent.addInput(i, 0);

        ValidationResult result = validate(parent, true);

        List<ValidationError> errors = result.getErrors();
        // Two errors - empty aggregate and input with no output place
        assertThat(errors.size(), is(2));
    }

    @Test
    public void validateAggregateWithNotConnectedOutput() {
        Aggregate parent = createAggregateInstance();
        Output o = new OutputImpl("oi");

        parent.addOutput(o, 0);

        ValidationResult result = validate(parent, true);

        List<ValidationError> errors = result.getErrors();
        // Two errors - empty aggregate and output with no input place
        assertThat(errors.size(), is(2));
    }

    @Test
    public void testChildAggregates() {
        AggregateInstance parent = new AggregateInstance("parent");
        AggregateInstance child = new AggregateInstance("child");

        parent.addChildAggregate(child);

        ValidationResult result = validate(parent, true);

        List<ValidationError> errors = result.getErrors();        
        assertThat(errors.size(), is(1));

        ValidationError error = errors.get(0);
        assertEquals(error.getParents().get(0), parent);
        assertEquals(child, error.getObjectWithError());
    }

    @Test
    public void testAggregateWithEmptyAggregateAndNoChildInspection() {
        AggregateInstance parent = new AggregateInstance("parent");
        AggregateInstance child = new AggregateInstance("child");

        parent.addChildAggregate(child);
        // We doesn't want to check child aggregates, so we should find no errors
        ValidationResult result = validate(parent, false);

        List<ValidationError> errors = result.getErrors();
        assertThat(errors.size(), is(0));
    }

    private ValidationResult validate(Aggregate a, boolean checkChilds) {
        AggregateValidator validator = new AggregateValidator();
        return validator.validate(a, checkChilds);
    }


}