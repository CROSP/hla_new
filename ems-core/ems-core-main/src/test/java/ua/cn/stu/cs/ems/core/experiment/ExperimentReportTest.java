package ua.cn.stu.cs.ems.core.experiment;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.*;

/**
 *
 * @author proger
 */
public class ExperimentReportTest {

    public ExperimentReportTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Test
    public void testEqualsEmptyReports() {
        ExperimentReport er1 = new ExperimentReport();
        ExperimentReport er2 = new ExperimentReport();

        assertThat(er1, equalTo(er2));
    }

    @Test
    public void testEqualsWhenShouldBeEqual() {
        ExperimentReport er1 = new ExperimentReport();
        ExperimentReport er2 = new ExperimentReport();

        er1.addSecondaryStatisticsElement(new SecondaryStatisticsElement(1, 2, 3, 4, 5, 6, 7));
        er2.addSecondaryStatisticsElement(new SecondaryStatisticsElement(1, 2, 3, 4, 5, 6, 7));

        er1.addPrimaryStatisticsObject("object1");
        er2.addPrimaryStatisticsObject("object1");

        assertThat(er1, equalTo(er2));
    }
}
