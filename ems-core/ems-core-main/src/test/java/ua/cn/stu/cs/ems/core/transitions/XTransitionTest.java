package ua.cn.stu.cs.ems.core.transitions;

import ua.cn.stu.cs.ems.core.models.ENetworksModel;
import ua.cn.stu.cs.ems.core.aggregates.AggregateInstance;
import ua.cn.stu.cs.ems.core.aggregates.Aggregate;
import ua.cn.stu.cs.ems.core.transitions.functions.TransformationFunction;
import org.jmock.integration.junit4.JMock;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JUnit4Mockery;
import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.Place;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import ua.cn.stu.cs.ems.core.transitions.functions.InterpretedPermitingFunction;
import ua.cn.stu.cs.ems.core.transitions.functions.PermitingFunction;
import ua.cn.stu.cs.ems.ecli.Value;
import static org.junit.Assert.*;
;import static org.hamcrest.CoreMatchers.*;;

/**
 *
 * @author proger
 */
@RunWith(JMock.class)
public class XTransitionTest {

    Mockery mockery = new JUnit4Mockery();

    public XTransitionTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testPredicateNoTokens() {
        XTransition transition = createXTransitionWithConnectedPlaces();
        assertThat("If there is no token in input and output place XTransition's predicate should be false",
                transition.canFire(), is(false));
    }

    @Test
    public void testPredicateWithInputTokenAndDefaultPermitingFunction() {
        XTransition transition = createXTransitionWithConnectedPlaces();
        Place input = transition.getInputPlaces().get(0);

        input.setToken(new Token());
        assertThat("If there is a token in input place and no token in output"
                + "positions XTransition's predicate should be true with default permiting function",
                transition.canFire(), is(true));
    }

    @Test
    public void testPredicateWithNoInputPosition() {
        XTransition transition = createXTransitionWithConnectedPlaces();
        deleteInputPlace(transition);
        assertThat("If there is no place connected to a XTransition, predicate should be false",
                transition.canFire(), is(false));
    }

    @Test
    public void testPredicateIfPermitingFunctionReturnsNull() {
        final XTransition transition = createXTransitionWithConnectedPlaces();
        transition.getInputPlace().setToken(new Token());

        final PermitingFunction permitingFunction = mockery.mock(PermitingFunction.class);

        mockery.checking(new Expectations() {

            {
                oneOf(permitingFunction).getPlaceNumber(transition);
                will(returnValue(null));
            }
        });

        transition.setPermitingFunction(permitingFunction);
        assertThat("If permiting function result is null, predicate should be false",
                transition.canFire(), is(false));
    }

    @Test
    public void testPredicateIfPermitingFunctionReturnsNumberMoreOutputPositionsNumber() {
        final XTransition transition = createXTransitionWithConnectedPlaces();
        transition.getInputPlace().setToken(new Token());

        final PermitingFunction permitingFunction = mockery.mock(PermitingFunction.class);

        mockery.checking(new Expectations() {

            {
                oneOf(permitingFunction).getPlaceNumber(transition);
                will(returnValue(new Integer(100)));
            }
        });

        transition.setPermitingFunction(permitingFunction);
        assertThat(transition.canFire(), is(false));
    }

    @Test
    public void testPredicateIfPermitingFunctionReturnsNegativeValue() {
        final XTransition transition = createXTransitionWithConnectedPlaces();
        transition.getInputPlace().setToken(new Token());

        final PermitingFunction permitingFunction = mockery.mock(PermitingFunction.class);

        mockery.checking(new Expectations() {

            {
                oneOf(permitingFunction).getPlaceNumber(transition);
                will(returnValue(new Integer(-1)));
            }
        });

        transition.setPermitingFunction(permitingFunction);
        assertThat(transition.canFire(), is(false));
    }

    @Test
    public void testPredicateIfOutputPositionIsNotEmpty() {
        XTransition transition = createXTransitionWithConnectedPlaces();
        transition.getInputPlace().setToken(new Token());
        transition.getOutputPlaces().get(0).setToken(new Token());

        assertThat("If output position isn't empty predicate should be false",
                transition.canFire(), is(false));
    }

    @Test
    public void testTransitionFire() {
        XTransition transition = createXTransitionWithConnectedPlaces();
        Token token = new Token();
        token.setValue("attr", new Value(1d));
        transition.getInputPlace().setToken(token);

        TransformationFunction tf = new TransformationFunction() {

            public Token transform(Transition transition, Token token) {
                Token newToken = new Token(token);
                newToken.setValue("attr", new Value(token.getValue("attr").doubleValue() + 1));
                return newToken;
            }

            public TransformationFunction copy() {
                throw new UnsupportedOperationException("Not supported yet.");
            }
        };

        transition.setTransformationFunction(tf);
        Token expectedToken = tf.transform(transition, token);

        // Let's simulate model here
        transition.fireIfPosible();
        transition.onDelayFinished();

        assertThat("After X transition fired it's input place should be empty",
                transition.getInputPlace().isEmpty(), is(true));
        assertThat("After X transition fired it output place, selected by permiting function"
                + "should contain transformed token from input place",
                transition.getOutputPlaces().get(0).getToken(), equalTo(expectedToken));
    }

    @Test
    public void testXtransitionWithPermitinFunction() {
        Aggregate root = new AggregateInstance("root");
        ENetworksModel model = new ENetworksModel();
        model.setRootAggregate(root);

        XTransition x = new XTransition("x");

        Place p0 = new Place("p0");
        Place p1 = new Place("p1");
        Place p2 = new Place("p2");
        Place in = new Place("in");

        in.setToken(new Token());

        x.connectInputPlace(in);
        x.addOutputPlace(p0, 0);
        x.addOutputPlace(p1, 1);
        x.addOutputPlace(p2, 2);

        root.addPlaces(p0, p1, p2, in);
        root.addTransitions(x);

        x.setPermitingFunction(new InterpretedPermitingFunction(""
                + "IF (P['p0'].T==FALSE) RETURN 0; IF (P['p1'].T==FALSE) RETURN 1;"
                + "IF (P['p2'].T==FALSE) RETURN 2;"));

        if (x.canFire()) {
            x.fireTransition();
        }

        assertNull(in.getToken());
        assertNotNull(p0.getToken());

        in.setToken(new Token());

        assertNotNull(in.getToken());

        if (x.canFire()) {
            x.fireTransition();
        }

        assertNull(in.getToken());
        assertNotNull(p1.getToken());

        in.setToken(new Token());

        assertNotNull(in.getToken());

        if (x.canFire()) {
            x.fireTransition();
        }

        assertNull(in.getToken());
        assertNotNull(p2.getToken());
    }

    private XTransition createXTransitionWithConnectedPlaces() {
        XTransition transition = new XTransition("X1");

        Place input = new Place("input");
        transition.connectInputPlace(input);

        for (int i = 0; i < 3; i++) {
            Place output = new Place("ouptut" + i);
            transition.addOutputPlace(output, i);
        }

        return transition;
    }

    private void deleteInputPlace(XTransition transition) {
        Place input = transition.getInputPlaces().get(0);
        input.setOutputActiveInstance(null);
        transition.delInputPlace();
    }
}