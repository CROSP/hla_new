package ua.cn.stu.cs.ems.core.experiment;

import ua.cn.stu.cs.ems.core.transitions.functions.InterpretedDelayFunction;
import ua.cn.stu.cs.ems.core.transitions.FTransition;
import ua.cn.stu.cs.ems.core.queues.FIFOQueue;
import ua.cn.stu.cs.ems.core.Token;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.testutils.Counter;
import org.jmock.Expectations;
import ua.cn.stu.cs.ems.core.aggregates.Aggregate;
import ua.cn.stu.cs.ems.core.models.Model;
import org.jmock.integration.junit4.JMock;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import ua.cn.stu.cs.ems.core.aggregates.AggregateChild;
import ua.cn.stu.cs.ems.core.experiment.statistics.StatisticsParameters;
import ua.cn.stu.cs.ems.core.models.ENetworksModel;
import ua.cn.stu.cs.ems.core.transitions.TTransition;
import ua.cn.stu.cs.ems.core.transitions.functions.ConstDelayFunction;
import ua.cn.stu.cs.ems.ecli.Value;
import static org.junit.Assert.*;
;import static org.hamcrest.CoreMatchers.*;;
import static ua.cn.stu.cs.ems.core.testutils.InstanceFactory.*;
import static ua.cn.stu.cs.ems.core.testutils.MocksFactory.*;
import static ua.cn.stu.cs.ems.core.testutils.Actions.*;
import static ua.cn.stu.cs.ems.core.testutils.Matchers.*;

/**
 *
 * @author proger
 */
@RunWith(JMock.class)
public class AbstractExperimentTest {

    Mockery mockery = new JUnit4Mockery();

    public AbstractExperimentTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testNumberOfModelStarts() {
        final Aggregate aggregate = createAggregateInstance();
        final Model model = mockery.mock(Model.class, "model");

        aggregate.setVariable("v1", new Value(0d));
        aggregate.setVariable("respond", new Value(0d));

        FactorImpl f1 = new FactorImpl(aggregate.getVariable("v1"), new StepValueGenerator(0, 10, 1));
        int numberOfValues = f1.numberOfValues();

        final Counter counter = new Counter(0);

        mockery.checking(new Expectations() {

            {
                allowing(model).getRootAggregate();
                will(returnValue(aggregate));
                allowing(model).reset();
                allowing(model).start();
                will(increaseCounter(counter));
                allowing(model).addTimer(createAnyModelTimeListener(), 0);
                allowing(model).getModelingTime();
                allowing(model).getFinishTime();
            }
        });

        Respond respond = new Respond(aggregate.getVariable("respond"));
        Experiment experiment = createExperimentInstance(model, f1, respond, 1);
        experiment.startExperiment();

        assertThat(counter.getValue(), is(numberOfValues));
    }

    @Test
    public void testNumberOfSecondaryStatisticsElementIfSecondaryStatisticsShouldBeSelected() {
        final Aggregate aggregate = createAggregateInstance();
        final Model model = new ENetworksModel();
        model.setRootAggregate(aggregate);

        aggregate.setVariable("v1", new Value(0d));
        aggregate.setVariable("respond", new Value(0d));

        FactorImpl f1 = new FactorImpl(aggregate.getVariable("v1"), new StepValueGenerator(0, 10, 1));
        int numberOfValues = f1.numberOfValues();

        Respond respond = new Respond(aggregate.getVariable("respond"));
        Experiment experiment = createExperimentInstance(model, f1, respond, 1);
        experiment.startExperiment();

        ExperimentReport er = experiment.getReport();
        assertThat(numberOfValues, equalTo(er.getSecondaryStatistics().size()));
        assertThat(f1.numberOfValues(), equalTo(er.getFactorValues().size()));
    }

    @Test
    public void testNumberOfSecondaryStatisticsElementIfSecondaryStatisticsShouldNOTBeSelected() {
        final Aggregate aggregate = createAggregateInstance();
        final Model model = new ENetworksModel();
        model.setRootAggregate(aggregate);

        aggregate.setVariable("v1", new Value(0d));
        aggregate.setVariable("respond", new Value(0d));

        FactorImpl f1 = new FactorImpl(aggregate.getVariable("v1"), new StepValueGenerator(0, 10, 1));


        Experiment experiment = createExperimentInstance(model, f1, 1);
        experiment.startExperiment();

        ExperimentReport er = experiment.getReport();
        assertThat(0, equalTo(er.getSecondaryStatistics().size()));
        assertThat(f1.numberOfValues(), equalTo(er.getFactorValues().size()));
    }

    @Test
    public void testNumberOfSecondaryStatisticsElementIfSecondaryStatisticsShouldNOTBeSelectedAndFactorsShoulNOTBeUsed() {
        Place p = new Place("P");

        final Aggregate aggregate = createAggregateInstanceWithName("A");
        aggregate.addPlace(p);

        final ENetworksModel model = new ENetworksModel();
        model.setRootAggregate(aggregate);

        aggregate.setVariable("v1", new Value(0d));
        aggregate.setVariable("respond", new Value(0d));

        model.setFinishTime(30D);

        Experiment experiment = createExperimentInstance(model, 10);
        experiment.collectStatisticsOn(p);
        experiment.setIsPrimaryStatisticsShouldBeCollected(true);
        experiment.setPrimaryStatisticsDumpPeriod(10);

        experiment.startExperiment();

        ExperimentReport er = experiment.getReport();
        // Model runs 10 times
        assertThat(10, equalTo(er.getPrimaryStatisticsFor("A.P").size()));
        // No factor values used
        assertThat(0, equalTo(er.getFactorValues().size()));
    }

    @Test
    public void testGetStatisticObjects() {
        final Place p = new Place("p");

        HashSet<AggregateChild> expected = new HashSet<AggregateChild>() {

            {
                add(p); // add(t); add(lpq);
            }
        };
        ENetworksModel model = new ENetworksModel();
        AbstractExperiment experiment = createExperimentInstance(model, null, null, 3);

        experiment.collectStatisticsOn(p);

        assertThat(experiment.getStatisticsObjects(), hasSameElements(expected));
    }

    @Test
    public void testGetObjectNamesFromReport() {
        Aggregate aggregate = createAggregateInstanceWithName("A");

        Place p = new Place("P");
        TTransition t = new TTransition("T");

        aggregate.addPlace(p);
        aggregate.addTransition(t);

        ENetworksModel model = new ENetworksModel();
        AbstractExperiment experiment = createExperimentInstance(model, null, null, 3);

        experiment.collectStatisticsOn(p);
        experiment.collectStatisticsOn(t);

        ExperimentReport er = experiment.getReport();

        Set<String> objectNames = er.getStatisticsObjectsNames();

        HashSet<String> expectedNames = new HashSet<String>() {

            {
                add("A.P");
                add("A.T");
            }
        };

        assertThat(objectNames, hasSameElements(expectedNames));
    }

    @Test
    public void testCheckIfNoPrimaryStatisticsCollectedWhenNoStatisticsShouldBeCollectedAtAll() {
        Aggregate aggregate = createAggregateInstanceWithName("A");

        Place p1 = new Place("P1");
        Place p2 = new Place("P2");
        TTransition t = new TTransition("T");

        t.setInputPlace(p1);
        t.setOutputPlace(p2);

        aggregate.addPlaces(p1, p2);
        aggregate.addTransition(t);

        ENetworksModel model = new ENetworksModel();
        model.setRootAggregate(aggregate);
        AbstractExperiment experiment = createExperimentInstance(model, 1);

        experiment.collectStatisticsOn(p1);
        experiment.startExperiment();

        ExperimentReport er = experiment.getReport();

        List<PrimaryStatisticsResult> psList = er.getPrimaryStatisticsFor("A.P1");

        assertThat(psList.size(), equalTo(0));
    }

    @Test
    // If last collection time was just before model stoped, statistics shouldn't be collected second time
    public void testCheckPrimaryStatisticsDoesNotCollectedTwice() {
        Aggregate aggregate = createAggregateInstanceWithName("A");

        TTransition t1 = new TTransition("T1");
        TTransition t2 = new TTransition("T2");

        t2.setDelayFunction(new ConstDelayFunction(10));

        Place p1 = new Place("P1");
        Place p2 = new Place("P2");
        Place p3 = new Place("P3");

        p1.setToken(new Token());

        t1.setInputPlace(p1);
        t1.setOutputPlace(p2);

        t2.setInputPlace(p2);
        t2.setOutputPlace(p3);

        aggregate.addPlaces(p1, p2, p3);
        aggregate.addTransitions(t1, t2);

        ENetworksModel model = new ENetworksModel();
        model.setRootAggregate(aggregate);
        AbstractExperiment experiment = createExperimentInstance(model, 1);

        experiment.collectStatisticsOn(p2);
        experiment.setDelayBeforeCollectingPrimaryStatistics(0);
        experiment.setPrimaryStatisticsDumpPeriod(10);
        experiment.setIsPrimaryStatisticsShouldBeCollected(true);

        experiment.startExperiment();

        ExperimentReport er = experiment.getReport();

        List<PrimaryStatisticsResult> psList = er.getPrimaryStatisticsFor("A.P2");

        List<PrimaryStatisticsElement> pseList = psList.get(0).getDumpedPrimaryStatisticsElements();

        assertThat(pseList.size(), equalTo(1));
    }

    @Test
    public void testExperimentStepWithoutFactors() {
        TTransition t1 = new TTransition("T1");
        TTransition t2 = new TTransition("T2");

        t1.setDelayFunction(new ConstDelayFunction(5));

        Place p0 = new Place("p0");
        Place p1 = new Place("p1");


        p0.setToken(new Token());

        t1.setInputPlace(p0);
        t1.setOutputPlace(p1);


        t2.setInputPlace(p1);
        t2.setOutputPlace(p0);


        Aggregate root = createAggregateInstanceWithName("A");
        root.addPlaces(p0, p1);
        root.addTransitions(t1, t2);

        final ENetworksModel model = new ENetworksModel();

        model.setRootAggregate(root);

        CountDownExperiment cdeStep = new CountDownExperiment(model, 1);
        CountDownExperiment cde = new CountDownExperiment(model, 1);


        cdeStep.collectStatisticsOn(p0);
        cdeStep.setDelayBeforeCollectingPrimaryStatistics(0);
        cdeStep.setPrimaryStatisticsDumpPeriod(10);
        cdeStep.setIsPrimaryStatisticsShouldBeCollected(true);

        cde.collectStatisticsOn(p0);
        cde.setDelayBeforeCollectingPrimaryStatistics(0);
        cde.setPrimaryStatisticsDumpPeriod(10);
        cde.setIsPrimaryStatisticsShouldBeCollected(true);

        model.setFinishTime(50D);
        while (!cdeStep.isExperimentFinished()) {
            cdeStep.stepExperiment();
        }
        cde.startExperiment();
        ExperimentReport reportStep = cdeStep.getReport();
        ExperimentReport report = cde.getReport();
        List<PrimaryStatisticsResult> psrList = report.getPrimaryStatisticsFor("A.p0");
        List<PrimaryStatisticsResult> psrListStep = reportStep.getPrimaryStatisticsFor("A.p0");

        for (int i = 0; i < 0; i++) {
            List<PrimaryStatisticsElement> list = psrList.get(i).getDumpedPrimaryStatisticsElements();
            List<PrimaryStatisticsElement> listStep = psrListStep.get(i).getDumpedPrimaryStatisticsElements();
            for (int j = 0; j < list.size(); j++) {
                assertEquals(list.get(j).getStatisticsResult().getResultValue(
                        StatisticsParameters.NUMBER_OF_PASSED_TOKENS), listStep.get(j).
                        getStatisticsResult().getResultValue(StatisticsParameters.NUMBER_OF_PASSED_TOKENS));

            }
            for (int j = 0; j < list.size(); j++) {
                assertEquals(list.get(j).getStatisticsResult().getResultValue(
                        StatisticsParameters.AVERAGE_TIME_IN_DELAY), listStep.get(j).
                        getStatisticsResult().getResultValue(StatisticsParameters.AVERAGE_TIME_IN_DELAY));

            }
            for (int j = 0; j < list.size(); j++) {
                assertEquals(list.get(j).getStatisticsResult().getResultValue(
                        StatisticsParameters.OCCUPIED_COEFFICIENT), listStep.get(j).
                        getStatisticsResult().getResultValue(StatisticsParameters.OCCUPIED_COEFFICIENT));

            }
        }
    }

    @Test
    public void testExperimentStepWithFactors() {
        final ENetworksModel model = new ENetworksModel();
        Aggregate agr = createAggregateInstance();
        agr.setVariable("var", new Value(0));
        agr.setVariable("respond", new Value(0));

        FIFOQueue fifo = new FIFOQueue("FIFO");

        Place tInput = new Place("TInput");
        Place tOutput = new Place("TOutput");
        Place qOutput = new Place("QOutput");
        Place cycle = new Place("Cycle");

        FTransition f = new FTransition("F");
        f.addOutputPlace(tOutput, 0);
        f.addOutputPlace(cycle, 1);
        f.setInputPlace(tInput);

        TTransition t = new TTransition("T");
        t.setInputPlace(cycle);
        t.setOutputPlace(tInput);

        fifo.connectInputPlace(tOutput);
        fifo.connectOutputPlace(qOutput);

        qOutput.setToken(new Token());
        tInput.setToken(new Token());

        f.setDelayFunction(new InterpretedDelayFunction("V['respond']=V['var']; RETURN V['respond'];"));

        agr.addPlaces(tInput, tOutput, qOutput, cycle);
        agr.addQueue(fifo);
        agr.addTransitions(f, t);

        model.setRootAggregate(agr);
        model.setFinishTime(25D);

        FactorImpl factor = new FactorImpl(agr.getVariable("var"), new StepValueGenerator(1, 3, 1));
        factor.setFactorName("Intensity");
        Respond respond = new Respond(StatisticsParameters.AVERAGE_QUEUE_LENGTH.getPresentation());

        CountDownExperiment pscStep = new CountDownExperiment(model, factor, respond, 1);
        CountDownExperiment psc = new CountDownExperiment(model, factor, respond, 1);

        pscStep.setDelayBeforeCollectingPrimaryStatistics(0);
        pscStep.setIsPrimaryStatisticsShouldBeCollected(true);
        pscStep.collectStatisticsOn(fifo);
        pscStep.collectSecondaryStatisticsOn(fifo);

        psc.setDelayBeforeCollectingPrimaryStatistics(0);
        psc.setIsPrimaryStatisticsShouldBeCollected(true);
        psc.collectStatisticsOn(fifo);
        psc.collectSecondaryStatisticsOn(fifo);

        pscStep.startExperiment();
        while (!psc.isExperimentFinished()) {
            psc.stepExperiment();
        }

        ExperimentReport reportStep = pscStep.getReport();
        ExperimentReport report = psc.getReport();
        List<PrimaryStatisticsResult> psrList = report.getPrimaryStatisticsFor("A.FIFO");
        List<PrimaryStatisticsResult> psrListStep = reportStep.getPrimaryStatisticsFor("A.FIFO");

        for (int i = 0; i < 0; i++) {
            List<PrimaryStatisticsElement> list = psrList.get(i).getDumpedPrimaryStatisticsElements();
            List<PrimaryStatisticsElement> listStep = psrListStep.get(i).getDumpedPrimaryStatisticsElements();
            for (int j = 0; j < list.size(); j++) {
                assertEquals(list.get(j).getStatisticsResult().getResultValue(
                        StatisticsParameters.NUMBER_OF_PASSED_TOKENS), listStep.get(j).
                        getStatisticsResult().getResultValue(StatisticsParameters.NUMBER_OF_PASSED_TOKENS));

            }
            for (int j = 0; j < list.size(); j++) {
                assertEquals(list.get(j).getStatisticsResult().getResultValue(
                        StatisticsParameters.AVERAGE_QUEUE_LENGTH), listStep.get(j).
                        getStatisticsResult().getResultValue(StatisticsParameters.AVERAGE_QUEUE_LENGTH));

            }
            for (int j = 0; j < list.size(); j++) {
                assertEquals(list.get(j).getStatisticsResult().getResultValue(
                        StatisticsParameters.AVERAGE_TIME_IN_QUEUE), listStep.get(j).
                        getStatisticsResult().getResultValue(StatisticsParameters.AVERAGE_TIME_IN_QUEUE));

            }
        }
    }

    @Test
    public void testSeveralStepExperiments() {
        TTransition t1 = new TTransition("T1");
        TTransition t2 = new TTransition("T2");

        t1.setDelayFunction(new ConstDelayFunction(5));

        Place p0 = new Place("p0");
        Place p1 = new Place("p1");


        p0.setToken(new Token());

        t1.setInputPlace(p0);
        t1.setOutputPlace(p1);


        t2.setInputPlace(p1);
        t2.setOutputPlace(p0);


        Aggregate root = createAggregateInstanceWithName("A");
        root.addPlaces(p0, p1);
        root.addTransitions(t1, t2);

        final ENetworksModel model = new ENetworksModel();

        model.setRootAggregate(root);

        CountDownExperiment cdeStep = new CountDownExperiment(model, 1);

        cdeStep.collectStatisticsOn(p0);
        cdeStep.setDelayBeforeCollectingPrimaryStatistics(0);
        cdeStep.setPrimaryStatisticsDumpPeriod(10);
        cdeStep.setIsPrimaryStatisticsShouldBeCollected(true);

        model.setFinishTime(50D);
        int count1 = 0;
        while (!cdeStep.isExperimentFinished()) {
            cdeStep.stepExperiment();
            count1++;
        }

        ExperimentReport reportStep1 = cdeStep.getReport();

        int count2 = 0;
        while (!cdeStep.isExperimentFinished()) {
            cdeStep.stepExperiment();
            count2++;
        }
        assertFalse(count2 > 0);

        cdeStep.stopStepExperiment();

        while (!cdeStep.isExperimentFinished()) {
            cdeStep.stepExperiment();
            count2++;
        }
        assertEquals(count1, count2);

        ExperimentReport reportStep2 = cdeStep.getReport();

        assertNotSame(reportStep1, reportStep2);

        List<PrimaryStatisticsResult> psrList = reportStep1.getPrimaryStatisticsFor("A.p0");
        List<PrimaryStatisticsResult> psrListStep = reportStep2.getPrimaryStatisticsFor("A.p0");

        for (int i = 0; i < 0; i++) {
            List<PrimaryStatisticsElement> list = psrList.get(i).getDumpedPrimaryStatisticsElements();
            List<PrimaryStatisticsElement> listStep = psrListStep.get(i).getDumpedPrimaryStatisticsElements();
            for (int j = 0; j < list.size(); j++) {
                assertEquals(list.get(j).getStatisticsResult().getResultValue(
                        StatisticsParameters.NUMBER_OF_PASSED_TOKENS), listStep.get(j).
                        getStatisticsResult().getResultValue(StatisticsParameters.NUMBER_OF_PASSED_TOKENS));

            }
            for (int j = 0; j < list.size(); j++) {
                assertEquals(list.get(j).getStatisticsResult().getResultValue(
                        StatisticsParameters.AVERAGE_TIME_IN_DELAY), listStep.get(j).
                        getStatisticsResult().getResultValue(StatisticsParameters.AVERAGE_TIME_IN_DELAY));

            }
            for (int j = 0; j < list.size(); j++) {
                assertEquals(list.get(j).getStatisticsResult().getResultValue(
                        StatisticsParameters.OCCUPIED_COEFFICIENT), listStep.get(j).
                        getStatisticsResult().getResultValue(StatisticsParameters.OCCUPIED_COEFFICIENT));

            }
        }
    }

    private AbstractExperiment createExperimentInstance(final Model model, FactorImpl factor, Respond respondVar, final int numberOfRuns) {
        return new MockAbstractExperiment(model, factor, respondVar, numberOfRuns);
    }

    private AbstractExperiment createExperimentInstance(final Model model, FactorImpl factor, final int numberOfRuns) {
        return new MockAbstractExperiment(model, factor, numberOfRuns);
    }

    private AbstractExperiment createExperimentInstance(final Model model, final int numberOfRuns) {
        return new MockAbstractExperiment(model, numberOfRuns);
    }

    private class MockAbstractExperiment extends AbstractExperiment {

        int currentRun = 0;
        int lastRunNumber;

        public MockAbstractExperiment(Model model, FactorImpl factor, Respond respond, int numberOfRuns) {
            super(model, factor, respond);
            lastRunNumber = numberOfRuns;
        }

        public MockAbstractExperiment(Model model, FactorImpl factor, int numberOfRuns) {
            super(model, factor);
            lastRunNumber = numberOfRuns;
        }

        public MockAbstractExperiment(Model model, int numberOfRuns) {
            super(model);
            lastRunNumber = numberOfRuns;
        }

        @Override
        protected void run() {
            currentRun++;
            getModel().start();
        }

        @Override
        protected boolean lastRun() {
            if (currentRun >= lastRunNumber) {
                return true;
            } else {
                return false;
            }
        }

        @Override
        protected void specificReset() {
            currentRun = 0;
        }

        @Override
        protected void runWithStep() {
            getModel().step();
        }
    }
}