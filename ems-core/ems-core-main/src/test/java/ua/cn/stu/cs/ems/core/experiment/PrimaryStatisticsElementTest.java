package ua.cn.stu.cs.ems.core.experiment;

import ua.cn.stu.cs.ems.core.experiment.statistics.StatisticsParameters;
import ua.cn.stu.cs.ems.core.experiment.statistics.StatisticsResult;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.*;

/**
 *
 * @author proger
 */
public class PrimaryStatisticsElementTest {
    
    public PrimaryStatisticsElementTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Test
    public void testEqualsWhenShouldBeEqual() {
        StatisticsResult sr1 = new StatisticsResult();
        sr1.setResultValue(StatisticsParameters.OCCUPIED_COEFFICIENT, 1d);
        
        StatisticsResult sr2 = new StatisticsResult();
        sr2.setResultValue(StatisticsParameters.OCCUPIED_COEFFICIENT, 1d);
        
        PrimaryStatisticsElement pse1 = new PrimaryStatisticsElement(10, sr1);
        PrimaryStatisticsElement pse2 = new PrimaryStatisticsElement(10, sr2);
        
        assertThat(pse1, equalTo(pse2));
    }
    
    @Test
    public void testEqualsWhenShouldBeEqual1() {
        StatisticsResult sr1 = new StatisticsResult();
        sr1.setResultValue(StatisticsParameters.OCCUPIED_COEFFICIENT, 1d);
        
        StatisticsResult sr2 = new StatisticsResult();
        sr2.setResultValue(StatisticsParameters.OCCUPIED_COEFFICIENT, 100d);
        
        PrimaryStatisticsElement pse1 = new PrimaryStatisticsElement(10, sr1);
        PrimaryStatisticsElement pse2 = new PrimaryStatisticsElement(10, sr2);
        
        assertThat(pse1, not(equalTo(pse2)));
    }
    
    @Test
    public void testEqualsWhenShouldBeEqual2() {
        StatisticsResult sr1 = new StatisticsResult();
        sr1.setResultValue(StatisticsParameters.OCCUPIED_COEFFICIENT, 1d);
        
        StatisticsResult sr2 = new StatisticsResult();
        sr2.setResultValue(StatisticsParameters.OCCUPIED_COEFFICIENT, 1d);
        
        PrimaryStatisticsElement pse1 = new PrimaryStatisticsElement(10, sr1);
        PrimaryStatisticsElement pse2 = new PrimaryStatisticsElement(20, sr2);
        
        assertThat(pse1, not(equalTo(pse2)));
    }
}
