package ua.cn.stu.cs.ems.core.testutils;

/**
 *
 * @author proger
 */
public class Counter {
    int value;

    public Counter(int initValue) {
        value = initValue;
    }

    public void increase() {
        value++;
    }

    public int getValue() {
        return value;
    }
}
