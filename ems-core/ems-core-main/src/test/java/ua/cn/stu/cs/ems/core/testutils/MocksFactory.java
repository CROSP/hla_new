package ua.cn.stu.cs.ems.core.testutils;

import java.util.List;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.models.Model;
import ua.cn.stu.cs.ems.core.models.ModelListener;
import ua.cn.stu.cs.ems.core.models.ModelTimeListener;
import ua.cn.stu.cs.ems.core.queues.AbstractQueue;
import ua.cn.stu.cs.ems.core.AbstractTransition;

/**
 *
 * @author proger
 */
public class MocksFactory {
    public static AbstractTransition createAbstractTransitionImpl() {
        return createAbstractTransitionImpl(0, 0);
    }

    public static AbstractTransition createAbstractTransitionImpl(final int maxInSize, final int maxOutSize) {
        return new AbstractTransition("T") {

            public void setInput(Place place, int pos) {
                replaceInputPlace(place, pos);
            }

            @Override
            protected boolean canFire() {
                return true;
            }

            @Override
            protected int maxInputSize() {
                return maxInSize;
            }

            @Override
            protected int maxOutputSize() {
                return maxOutSize;
            }

            @Override
            protected void fireTransition() {
                throw new UnsupportedOperationException("Not supported yet.");
            }
        };
    }

    public static AbstractQueue createAbstractQueue() {
        return new AbstractQueue("queue") {

            @Override
            protected boolean isQueueEmpty() {
                throw new UnsupportedOperationException("Not supported yet.");
            }

            @Override
            protected Token removeTokenFromQueue() {
                throw new UnsupportedOperationException("Not supported yet.");
            }

            @Override
            protected void addToken(Token token) {
                throw new UnsupportedOperationException("Not supported yet.");
            }

            public List<Token> getTokensInQueue() {
                throw new UnsupportedOperationException("Not supported yet.");
            }

            public void clear() {
                throw new UnsupportedOperationException("Not supported yet.");
            }

            public int length() {
                throw new UnsupportedOperationException("Not supported yet.");
            }
        };
    }

    public static ModelTimeListener createAnyModelTimeListener() {
        return new ModelTimeListener() {

            public void modelTimeChanged(Model model, double newModelTime) {
                throw new UnsupportedOperationException("Not supported yet.");
            }

            @Override
            public boolean equals(Object o) {
                return true;
            }
        };
    }

    public static ModelListener createAnyModelListener() {
        return new ModelListener() {

            public void modelStarted(Model model) {
                throw new UnsupportedOperationException("Not supported yet.");
            }

            public void modelFinished(Model model) {
                throw new UnsupportedOperationException("Not supported yet.");
            }

            public void modelTimeChanged(Model model, double newModelTime) {
                throw new UnsupportedOperationException("Not supported yet.");
            }

            @Override
            public boolean equals(Object o) {
                return true;
            }
        };
    }
}
