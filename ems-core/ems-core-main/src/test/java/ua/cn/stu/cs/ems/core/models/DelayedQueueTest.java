package ua.cn.stu.cs.ems.core.models;

import ua.cn.stu.cs.ems.core.ActiveInstance;
import ua.cn.stu.cs.ems.core.aggregates.Input;
import ua.cn.stu.cs.ems.core.aggregates.Output;
import ua.cn.stu.cs.ems.core.transitions.Transition;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.jmock.Mockery;
import org.junit.runner.RunWith;
import org.jmock.integration.junit4.JMock;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
;import static org.hamcrest.CoreMatchers.*;;

/**
 *
 * @author proger
 */
@RunWith(JMock.class)
public class DelayedQueueTest {

    Mockery mockContext = new JUnit4Mockery();

    public DelayedQueueTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {

    }

    @After
    public void tearDown() {
    }


    @Test
    public void testGetFirstTransition() {
        ActiveInstance t1 = mockContext.mock(Transition.class, "tr1");
        ActiveInstance t2 = mockContext.mock(Transition.class, "tr2");

        DelayedQueue delayedTransitionQueue = new DelayedQueue();
        delayedTransitionQueue.addDelayable(t2, 100);
        delayedTransitionQueue.addDelayable(t1, 200);

        ActiveInstance first = delayedTransitionQueue.getFirstDelayedInstance();
        ActiveInstance second = delayedTransitionQueue.getFirstDelayedInstance();

        assertThat(first, equalTo(t2));
        assertThat(second, equalTo(t1));
        assertThat(delayedTransitionQueue.getSize(), equalTo(0));
    }

    @Test
    public void testGetFirstTransitionTime() {
        Transition t1 = mockContext.mock(Transition.class, "tr1");
        Transition t2 = mockContext.mock(Transition.class, "tr2");

        DelayedQueue delayedTransitionQueue = new DelayedQueue();
        delayedTransitionQueue.addDelayable(t2, 100);
        delayedTransitionQueue.addDelayable(t1, 200);

        double firstTime = delayedTransitionQueue.getFirstDelayableTime();

        assertThat(firstTime, equalTo(new Double(100)));
    }

    @Test
    public void testGetFromEmptyQueue() {
        DelayedQueue delayedTransitionQueue = new DelayedQueue();
        ActiveInstance delayable = delayedTransitionQueue.getFirstDelayedInstance();
        assertThat(delayable, nullValue());
    }

    @Test
    public void testGetSize() {
        Transition t1 = mockContext.mock(Transition.class, "tr1");
        Transition t2 = mockContext.mock(Transition.class, "tr2");

        DelayedQueue delayedTransitionQueue = new DelayedQueue();

        assertThat(delayedTransitionQueue.getSize(), equalTo(0));

        delayedTransitionQueue.addDelayable(t1, 0);
        delayedTransitionQueue.addDelayable(t2, 1);

        assertThat(delayedTransitionQueue.getSize(), equalTo(2));
    }

    @Test
    public void testCheckOutputsTransitionsPriority() {
        Output out1 = mockContext.mock(Output.class, "O1");
        Output out2 = mockContext.mock(Output.class, "O2");
        Transition transition = mockContext.mock(Transition.class, "Transition");

        DelayedQueue queue = new DelayedQueue();

        queue.addDelayable(out1, 0);
        queue.addDelayable(transition, 0);
        queue.addDelayable(out2, 0);

        assertEquals(transition, queue.getFirstDelayedInstance());
    }

    @Test
    public void testCheckInputsTransitionPriority() {
        Input in1 = mockContext.mock(Input.class, "O1");
        Input in2 = mockContext.mock(Input.class, "O2");
        Transition transition = mockContext.mock(Transition.class, "Transition");

        DelayedQueue queue = new DelayedQueue();

        queue.addDelayable(in1, 0);
        queue.addDelayable(transition, 0);
        queue.addDelayable(in2, 0);

        assertEquals(transition, queue.getFirstDelayedInstance());
    }

    @Test
    public void testGetFirstElementPriority() {
        Input in = mockContext.mock(Input.class);
        Transition transition = mockContext.mock(Transition.class);
        DelayedQueue dq = new DelayedQueue();

        dq.addDelayable(in, 0);
        dq.addDelayable(transition, 0);
        assertThat(dq.getFirstDelayablePriority(), is(DelayedQueue.TRANSITION_QUEUE_PRIORITY));

        dq.getFirstDelayedInstance();
        assertThat(dq.getFirstDelayablePriority(), is(DelayedQueue.INPUT_OUTPUT_PRIORITY));
    }
}