package ua.cn.stu.cs.ems.core.queues;

import java.util.ArrayList;
import org.junit.*;
import ua.cn.stu.cs.ems.core.Token;
import static ua.cn.stu.cs.ems.core.queues.TestQueueOrder.assertQueueOrder;
import ua.cn.stu.cs.ems.ecli.Value;

/**
 *
 * @author proger
 */
public class LIFOQueueTest {

    public LIFOQueueTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testLIFOQueue() {
        Queue queue = new LIFOQueue("LIFOQ");

        final Token t1 = new Token(); t1.setValue("a", new Value(1d));
        final Token t2 = new Token(); t2.setValue("a", new Value(2d));
        final Token t3 = new Token(); t3.setValue("a", new Value(3d));

        ArrayList<Token> inputOrder = new ArrayList<Token>() {{
            add(t1);
            add(t2);
            add(t3);
        }};

        ArrayList<Token> outputOrder = new ArrayList<Token>() {{
            add(t1);
            add(t3);
            add(t2);
        }};

        assertQueueOrder(queue, inputOrder, outputOrder);
    }

    private ArrayList<Token> invertList(ArrayList<Token> inputOrder) {
        ArrayList<Token> result = new ArrayList<Token>();

        for (Token t : inputOrder) {
            result.add(0, t);
        }

        return result;
    }

}
