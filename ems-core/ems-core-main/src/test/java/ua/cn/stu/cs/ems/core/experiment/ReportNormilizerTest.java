package ua.cn.stu.cs.ems.core.experiment;

import ua.cn.stu.cs.ems.core.experiment.statistics.StatisticsResult;
import java.util.List;
import java.util.ArrayList;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import ua.cn.stu.cs.ems.core.experiment.statistics.StatisticsParameters;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.*;

/**
 *
 * @author proger
 */
public class ReportNormilizerTest {
    
    public ReportNormilizerTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Test
    public void testNormalizeEmptyReport() {
        ExperimentReport report = new ExperimentReport();
        
        ExperimentReport expectedReport = ReportNormilizer.normilizeReport(report);
        assertThat(expectedReport, equalTo(report));        
    }
    
  /*  @Test
    public void testNormalizeSecondaryStatistics() {
        ExperimentReport report = new ExperimentReport();
        
        report.addSecondaryStatisticsElement(new SecondaryStatisticsElement(1, 2));
        report.addSecondaryStatisticsElement(new SecondaryStatisticsElement(1, 6));
        report.addSecondaryStatisticsElement(new SecondaryStatisticsElement(2, 4));
        report.addSecondaryStatisticsElement(new SecondaryStatisticsElement(2, 8));
        report.addSecondaryStatisticsElement(new SecondaryStatisticsElement(3, 6));
        report.addSecondaryStatisticsElement(new SecondaryStatisticsElement(3, 10));
        
        ExperimentReport expected = new ExperimentReport();
        expected.addSecondaryStatisticsElement(new SecondaryStatisticsElement(1, 4));
        expected.addSecondaryStatisticsElement(new SecondaryStatisticsElement(2, 6));
        expected.addSecondaryStatisticsElement(new SecondaryStatisticsElement(3, 8));
        
        ExperimentReport result = ReportNormilizer.normilizeReport(report);
        
        assertThat(result, equalTo(expected));
    } 
    */
    
    @Test
    public void testNormalizePrimaryStatistics() {
        ExperimentReport report = new ExperimentReport();
        
        String object = "object";
        
        
        report.addPrimaryStatisticsObject(object);        
        
        report.addPrimaryStatisticsResult(object, new PrimaryStatisticsResult(1, 
                                          createPrimaryStatisticsElement(new int[] {1, 2, 3}, 
                                                                         new double[] {2, 4, 6}, 
                                                                         new double[] {4, 6, 8}, 
                                                                         new double[] {10, 12, 14})));
        report.addPrimaryStatisticsResult(object, new PrimaryStatisticsResult(1, 
                                          createPrimaryStatisticsElement(new int[] {1, 2, 3}, 
                                                                         new double[] {4, 6, 8}, 
                                                                         new double[] {10, 12, 14}, 
                                                                         new double[] {16, 18, 20})));
        
        report.addPrimaryStatisticsResult(object, new PrimaryStatisticsResult(2, 
                                          createPrimaryStatisticsElement(new int[] {1, 2, 3}, 
                                                                         new double[] {6, 8, 10}, 
                                                                         new double[] {12, 14, 16}, 
                                                                         new double[] {18, 20, 22})));
        report.addPrimaryStatisticsResult(object, new PrimaryStatisticsResult(2, 
                                          createPrimaryStatisticsElement(new int[] {1, 2, 3}, 
                                                                         new double[] {10, 12, 14}, 
                                                                         new double[] {16, 18, 20}, 
                                                                         new double[] {22, 24, 26})));
        
        ExperimentReport expected = new ExperimentReport();
        
        expected.addPrimaryStatisticsObject(object);        
        
        expected.addPrimaryStatisticsResult(object, new PrimaryStatisticsResult(1, 
                                          createPrimaryStatisticsElement(new int[] {1, 2, 3}, 
                                                                         new double[] {3, 5, 7}, 
                                                                         new double[] {7, 9, 11}, 
                                                                         new double[] {13, 15, 17})));
        
        expected.addPrimaryStatisticsResult(object, new PrimaryStatisticsResult(2, 
                                          createPrimaryStatisticsElement(new int[] {1, 2, 3}, 
                                                                         new double[] {8, 10, 12}, 
                                                                         new double[] {14, 16, 18}, 
                                                                         new double[] {20, 22, 24})));
        
        ExperimentReport result = ReportNormilizer.normilizeReport(report);
        
        assertThat(result, equalTo(expected));        
    }
    
    @Test
    public void testCopyingFactorValues() {
        ExperimentReport original = new ExperimentReport();
        
        original.addFactorValue(1d);
        original.addFactorValue(2d);
        original.addFactorValue(3d);
        
        ExperimentReport normilizedReport = ReportNormilizer.normilizeReport(original);
        
        List<Double> factorValues = normilizedReport.getFactorValues();
        
        List<Double> expectedValues = new ArrayList<Double>() {{ 
            add(1d);
            add(2d);
            add(3d);
        }};
        
        assertThat(factorValues, equalTo(expectedValues));
    }
    
    private List<PrimaryStatisticsElement> createPrimaryStatisticsElement(int[] times, double[] values1, 
                                                                          double[] values2, double[] values3) {
        List<PrimaryStatisticsElement> primaryStatistics = new ArrayList<PrimaryStatisticsElement>();
        
        for (int i = 0; i < times.length; i++) {
            StatisticsResult sr = new StatisticsResult();
            sr.setResultValue(StatisticsParameters.OCCUPIED_COEFFICIENT, values1[i]);
            sr.setResultValue(StatisticsParameters.NUMBER_OF_PASSED_TOKENS, values2[i]);
            sr.setResultValue(StatisticsParameters.AVERAGE_TIME_IN_DELAY, values3[i]);
            
            PrimaryStatisticsElement pse = new PrimaryStatisticsElement(times[i], sr);
            primaryStatistics.add(pse);
        }
        
        return primaryStatistics;
    }
}


