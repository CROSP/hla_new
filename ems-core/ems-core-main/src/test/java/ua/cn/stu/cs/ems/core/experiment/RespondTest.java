/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.cn.stu.cs.ems.core.experiment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.*;
import ua.cn.stu.cs.ems.core.aggregates.Aggregate;
import ua.cn.stu.cs.ems.core.aggregates.AggregateInstance;
import ua.cn.stu.cs.ems.core.experiment.statistics.StatisticsParameters;
import ua.cn.stu.cs.ems.ecli.Value;

/**
 *
 * @author Dmitry Krivenko <dmitrykrivenko@gmal.com>
 */
public class RespondTest {

    public RespondTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testRespondAvgNumCharacteristics() {
        Respond respond = new Respond(StatisticsParameters.AVERAGE_OCCUPIED_TIME.getPresentation());

        assertEquals(respond.getName(), StatisticsParameters.AVERAGE_OCCUPIED_TIME.getPresentation());
    }

    @Test
    public void testRespondValueForVariable() {
        Aggregate agr = new AggregateInstance("root");
        agr.setVariable("var", new Value(17d));

        Respond respond = new Respond(agr.getVariable("var"));

        assertTrue("Get value is not = value of variable", respond.getValue() == 17d);
    }

    @Test
    public void testRespondValueForAvgNumCharacteristics() {
        Respond respond = new Respond(StatisticsParameters.AVERAGE_QUEUE_LENGTH.getPresentation());

        assertTrue("Get value is not = init value(0) for Average Numerical Characteristics", respond.getValue() == 0d);
    }
}
