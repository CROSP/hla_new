package ua.cn.stu.cs.ems.core.ecli;

import ua.cn.stu.cs.ems.ecli.Value;
import ua.cn.stu.cs.ems.core.Token;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import ua.cn.stu.cs.ems.ecli.ValType;
import static org.junit.Assert.*;
;import static org.hamcrest.CoreMatchers.*;;

/**
 *
 * @author proger
 */
public class EcliTokenAccessorTest {

    public EcliTokenAccessorTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Test
    public void testGetNonExistingAttributeAttribute() {
        Token currentToken = new Token();
        EcliTokenAccessor eta = new EcliTokenAccessor(currentToken);

        boolean actual = eta.containsAttribute("nonExistingKey");
        assertThat("If attribute isn't set in token, then EcliTokenAccessor.containsAttribute should return false",
                actual, is(false));
    }

    @Test
    public void testSetAttribute() {
        Token currentToken = new Token(); currentToken.setValue("existed", new Value(0d));
        EcliTokenAccessor eta = new EcliTokenAccessor(currentToken);
        eta.setAttribute("existed", new Value(1L, ValType.INTEGER));
        eta.setAttribute("notExisted", new Value(2L, ValType.INTEGER));

        assertThat("If existed attribute was changed by token accessor backend token should also changed",
                currentToken.getValue("existed").doubleValue(), is(1d));
        assertThat("If not existed attribute was changed by token accesor beckend token should "
                + "contain new attribute with value that was set" ,
                currentToken.getValue("notExisted").doubleValue(), is(2d));
    }

    @Test
    public void testGetAttribute() {
        Token currentToken = new Token(); 
        currentToken.setValue("attr", new Value(100d));
        EcliTokenAccessor eta = new EcliTokenAccessor(currentToken);

        assertThat("If attribute set in token EcliTokenAccessor.getAttribute should return it's value",
                ((Value) eta.getAttribute("attr")).doubleValue(), equalTo(Double.valueOf(100)));
    }

}