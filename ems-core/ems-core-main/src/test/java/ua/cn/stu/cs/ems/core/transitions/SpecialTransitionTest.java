package ua.cn.stu.cs.ems.core.transitions;

import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.aggregates.Aggregate;
import org.jmock.Expectations;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.models.Model;
import org.jmock.Mockery;
import org.junit.runner.RunWith;
import org.jmock.integration.junit4.JMock;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author proger
 */
@RunWith(JMock.class)
public class SpecialTransitionTest {

    Mockery mockery = new JUnit4Mockery();

    public SpecialTransitionTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test(expected=NullPointerException.class)
    public void setNullPermitingFunction() {
        AbstractSpecialTransition transition = createSpecialTransitionInstance();

        transition.setPermitingFunction(null);
    }

    @Test
    public void testAddintSelfToSpecialTransitionList() {
        final Model model = mockery.mock(Model.class, "model");
        final Aggregate aggregate = mockery.mock(Aggregate.class, "aggregate");
        final AbstractSpecialTransition transition = createSpecialTransitionWithConnectedPlaces();
        transition.setParentAggregate(aggregate);

        Place input = transition.getInputPlaces().get(0);

        mockery.checking(new Expectations() {{
            oneOf(aggregate).getModel(); will(returnValue(model));
            oneOf(model).addSpecialTransition(transition);
        }});

        input.setToken(new Token());
    }

    private AbstractSpecialTransition createSpecialTransitionWithConnectedPlaces() {
        AbstractSpecialTransition transition = createSpecialTransitionInstance();
        Place input = new Place("input");
        Place output = new Place("output");

        input.setOutputActiveInstance(transition);
        transition.addInputPlace(input, 0);
        output.setInputActiveInstance(transition);
        transition.addOutputPlace(output, 0);

        return transition;
    }

    private AbstractSpecialTransition createSpecialTransitionInstance() {
        return new AbstractSpecialTransition("XY") {

            @Override
            protected boolean canFire() {
                throw new UnsupportedOperationException("Not supported yet.");
            }
            
            @Override
            protected int maxInputSize() {
                return 0;
            }

            @Override
            protected int maxOutputSize() {
                return 0;
            }

            @Override
            protected void fireTransition() {
                throw new UnsupportedOperationException("Not supported yet.");
            }
        };
    }

}