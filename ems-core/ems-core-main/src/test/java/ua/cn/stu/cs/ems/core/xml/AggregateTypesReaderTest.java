package ua.cn.stu.cs.ems.core.xml;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.BeforeClass;
import org.junit.Test;
import ua.cn.stu.cs.ems.core.aggregates.AggregateDefinition;
import ua.cn.stu.cs.ems.core.aggregates.AggregateDefinitionReference;
import ua.cn.stu.cs.ems.core.aggregates.AggregateRegistry;

/**
 *
 * @author proger
 */
public class AggregateTypesReaderTest {

    public AggregateTypesReaderTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Test
    public void testLoadRegistryWithOneAggregate() throws Exception {
        final String rootXML = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<pnml xmlns='http://www.pnml.org/version-2009/grammar/pnml'>"
                + "<net id='root' type='http://www.cs.stu.cn.ua/jess/enets'>"
                + "<page id='root'>"

                + "<transition id='ValidT'>"
                + "<name>"
                + "<text>ValidT</text>"
                + "<graphics><offset x='20' y='30'/></graphics>"
                + "</name>"
                + "<definition type='transition' subType='T'>"
                + "<delayFunction type='ecli'>RETURN 123;</delayFunction>"
                + "<transformationFunction type='ecli'>T['attr'] = 123;</transformationFunction>"
                + "</definition>"
                + "<graphics><position x='100' y='200'/></graphics></transition>"

                + "</page></net></pnml>";

        HashMap<String, InputStream> typesSource = new HashMap<String, InputStream>() {{
            put("root", new ByteArrayInputStream(rootXML.getBytes("UTF-8")));
        }};

        AggregateTypesReader reader = new AggregateTypesReader();

        AggregateRegistry ar = reader.readAggregateTypes(typesSource);
        List<AggregateDefinition> lst = ar.getAggregateDefinitions();
        assertEquals(1, lst.size());

        AggregateDefinition root = lst.get(0);
        assertEquals("root", root.getName());
        assertEquals(1, root.getTransitions().size());
    }

    @Test
    public void testLoadRegistryWithAggregateTree() throws Exception {
        final String rootXML = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<pnml xmlns='http://www.pnml.org/version-2009/grammar/pnml'>"
                + "<net id='root' type='http://www.cs.stu.cn.ua/jess/enets'>"
                + "<page id='root'>"

                + "<transition id='A1'>"
                + "<name>"
                + "<text>A1</text>"
                + "<graphics><offset x='20' y='30'/></graphics>"
                + "</name>"
                + "<definition type='aggregate' subType='child1'>"
                + "</definition>"
                + "<graphics><position x='100' y='200'/></graphics></transition>"

                + "<transition id='A2'>"
                + "<name>"
                + "<text>A2</text>"
                + "<graphics><offset x='20' y='30'/></graphics>"
                + "</name>"
                + "<definition type='aggregate' subType='child1'>"
                + "</definition>"
                + "<graphics><position x='100' y='200'/></graphics></transition>"

                + "<transition id='A3'>"
                + "<name>"
                + "<text>A3</text>"
                + "<graphics><offset x='20' y='30'/></graphics>"
                + "</name>"
                + "<definition type='aggregate' subType='child2'>"
                + "</definition>"
                + "<graphics><position x='100' y='200'/></graphics></transition>"

                + "</page></net></pnml>";

        final String child1XML = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<pnml xmlns='http://www.pnml.org/version-2009/grammar/pnml'>"
                + "<net id='child1' type='http://www.cs.stu.cn.ua/jess/enets'>"
                + "<page id='child1'>"

                + "<transition id='A1'>"
                + "<name>"
                + "<text>A1</text>"
                + "<graphics><offset x='20' y='30'/></graphics>"
                + "</name>"
                + "<definition type='aggregate' subType='child2'>"
                + "</definition>"
                + "<graphics><position x='100' y='200'/></graphics></transition>"

                + "</page></net></pnml>";

        final String child2XML = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<pnml xmlns='http://www.pnml.org/version-2009/grammar/pnml'>"
                + "<net id='child2' type='http://www.cs.stu.cn.ua/jess/enets'>"
                + "<page id='child2'>"
                
                + "</page></net></pnml>";

        HashMap<String, InputStream> typesSource = new HashMap<String, InputStream>() {{
            put("root", new ByteArrayInputStream(rootXML.getBytes("UTF-8")));
            put("child1", new ByteArrayInputStream(child1XML.getBytes("UTF-8")));
            put("child2", new ByteArrayInputStream(child2XML.getBytes("UTF-8")));
        }};

        AggregateTypesReader reader = new AggregateTypesReader();
        AggregateRegistry registry = reader.readAggregateTypes(typesSource);

        AggregateDefinition root = registry.getAggregateDefinition("root");
        AggregateDefinition child1 = registry.getAggregateDefinition("child1");
        AggregateDefinition child2 = registry.getAggregateDefinition("child2");

        assertNotNull(root);
        assertNotNull(child1);
        assertNotNull(child2);

        AggregateDefinitionReference A1 = (AggregateDefinitionReference) root.getAggregate("A1");
        AggregateDefinitionReference A2 = (AggregateDefinitionReference) root.getAggregate("A2");
        AggregateDefinitionReference A3 = (AggregateDefinitionReference) root.getAggregate("A3");

        assertEquals("child1", A1.getAggregateDefinition().getName());
        assertEquals("child1", A2.getAggregateDefinition().getName());
        assertEquals("child2", A3.getAggregateDefinition().getName());

        AggregateDefinitionReference child1A1 = (AggregateDefinitionReference) A1.getAggregateDefinition().getAggregate("A1");
        assertEquals("child2", child1A1.getAggregateDefinition().getName());
        
    }

    //TODO Add test that checks if filename isn't equals to aggregate name
    @Test(expected=PNMLParsingException.class)
    public void testCheckIfFilenameEqualsToAggregateName() throws Exception {
        final String rootXML = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<pnml xmlns='http://www.pnml.org/version-2009/grammar/pnml'>"
                + "<net id='root' type='http://www.cs.stu.cn.ua/jess/enets'>"
                + "<page id='root'>"

                + "</page></net></pnml>";

        // File name doesn't equlas to aggregate type name.
        HashMap<String, InputStream> typesSource = new HashMap<String, InputStream>() {{
            put("notRoot", new ByteArrayInputStream(rootXML.getBytes("UTF-8")));
        }};

        AggregateTypesReader reader = new AggregateTypesReader();
        // Where is my exception?
        reader.readAggregateTypes(typesSource);
    }

    @Test(expected=PNMLParsingException.class)
    public void testCheckIfFilenameForAggregateTypeNotExists() throws Exception {
        final String rootXML = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<pnml xmlns='http://www.pnml.org/version-2009/grammar/pnml'>"
                + "<net id='root' type='http://www.cs.stu.cn.ua/jess/enets'>"
                + "<page id='root'>"


                + "<transition id='A1'>"
                + "<name>"
                + "<text>A1</text>"
                + "<graphics><offset x='20' y='30'/></graphics>"
                + "</name>"
                + "<definition type='aggregate' subType='child'>"
                + "</definition>"
                + "<graphics><position x='100' y='200'/></graphics></transition>"

                + "</page></net></pnml>";

        // No InputStream for 'child' type
        HashMap<String, InputStream> typesSource = new HashMap<String, InputStream>() {{
            put("root", new ByteArrayInputStream(rootXML.getBytes("UTF-8")));
        }};

        AggregateTypesReader reader = new AggregateTypesReader();
        // Where is my exception?
        reader.readAggregateTypes(typesSource);
    }

    @Test(expected=PNMLParsingException.class
    ,timeout = 3000
    )
    public void testCheckForLoops() throws Exception {
        final String A1XML = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<pnml xmlns='http://www.pnml.org/version-2009/grammar/pnml'>"
                + "<net id='A1' type='http://www.cs.stu.cn.ua/jess/enets'>"
                + "<page id='A1'>"

                + "<transition id='A'>"
                + "<name>"
                + "<text>A</text>"
                + "<graphics><offset x='20' y='30'/></graphics>"
                + "</name>"
                + "<definition type='aggregate' subType='A2'>"
                + "</definition>"
                + "<graphics><position x='100' y='200'/></graphics></transition>"

                + "</page></net></pnml>";

        final String A2XML = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<pnml xmlns='http://www.pnml.org/version-2009/grammar/pnml'>"
                + "<net id='A2' type='http://www.cs.stu.cn.ua/jess/enets'>"
                + "<page id='A2'>"

                + "<transition id='A'>"
                + "<name>"
                + "<text>A</text>"
                + "<graphics><offset x='20' y='30'/></graphics>"
                + "</name>"
                + "<definition type='aggregate' subType='A1'>"
                + "</definition>"
                + "<graphics><position x='100' y='200'/></graphics></transition>"

                + "</page></net></pnml>";

        // A1 includes A2 and A2 includes A1 - loop
        HashMap<String, InputStream> typesSource = new HashMap<String, InputStream>() {{
            put("A1", new ByteArrayInputStream(A1XML.getBytes("UTF-8")));
            put("A2", new ByteArrayInputStream(A2XML.getBytes("UTF-8")));
        }};

        AggregateTypesReader reader = new AggregateTypesReader();
        // Where is my exception?
        reader.readAggregateTypes(typesSource);
    }
}
