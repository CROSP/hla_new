package ua.cn.stu.cs.ems.core.transitions;

import ua.cn.stu.cs.ems.core.aggregates.Aggregate;
import ua.cn.stu.cs.ems.core.transitions.functions.TransformationFunction;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.runner.RunWith;
import org.jmock.integration.junit4.JMock;
import org.jmock.integration.junit4.JUnit4Mockery;
import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.Place;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ua.cn.stu.cs.ems.core.aggregates.AggregateInstance;
import ua.cn.stu.cs.ems.core.models.ENetworksModel;
import ua.cn.stu.cs.ems.core.transitions.functions.InterpretedPermitingFunction;
import ua.cn.stu.cs.ems.core.transitions.functions.PermitingFunction;
import ua.cn.stu.cs.ems.ecli.Value;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.*;


/**
 *
 * @author proger
 */
@RunWith(JMock.class)
public class YTransitionTest {

    Mockery mockery = new JUnit4Mockery();

    public YTransitionTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testConnectOutputPlace() {
        YTransition transition = createYTransitionInstance();
        Place output = new Place("O");
        transition.connectOutputPlace(output);

        assertThat("When output place connected, number of ouput places shoud be 1",
                transition.getOutputPlaces().size(), is(1));
        assertThat(transition.getOutputPlaces().get(0), equalTo(output));
    }

    @Test
    public void testPredicateWithNoTokens() {
        YTransition transition = createYTransitionInstanceWithConnectedPlaces();

        assertThat("If there is no tokens in input and output places YTransition predicate should return false",
                transition.canFire(), is(false));
    }

    @Test
    public void testPredicateIfOutputPlaceIsNotEmpty() {
        YTransition transition = createYTransitionInstanceWithConnectedPlaces();
        transition.getOutputPlace().setToken(new Token());

        assertThat("If YTransition's output place isn't empty predicate should return false",
                transition.canFire(), is(false));
    }

    @Test
    public void testPredicateIfPermitingFunctionResultIsNegative() {
        final YTransition transition = createYTransitionInstanceWithConnectedPlaces();

        final PermitingFunction permitingFunction = mockery.mock(PermitingFunction.class);

        mockery.checking(new Expectations() {

            {
                oneOf(permitingFunction).getPlaceNumber(transition);
                will(returnValue(-100));
            }
        });

        transition.setPermitingFunction(permitingFunction);

        assertThat("If permiting function result is negative YTransition's predicate should be false",
                transition.canFire(), is(false));
    }

    @Test
    public void testPredicateIfPermitingFunctionResultMoreThenNubmerOfInputPositions() {
        final YTransition transition = createYTransitionInstanceWithConnectedPlaces();

        final PermitingFunction permitingFunction = mockery.mock(PermitingFunction.class);

        mockery.checking(new Expectations() {

            {
                oneOf(permitingFunction).getPlaceNumber(transition);
                will(returnValue(100));
            }
        });

        transition.setPermitingFunction(permitingFunction);

        assertThat("If permiting function result more then number of input positions"
                + "YTransition's predicate should return false",
                transition.canFire(), is(false));
    }

    @Test
    public void testPredicateIfPermitingFunctionResultIsNull() {
        final YTransition transition = createYTransitionInstanceWithConnectedPlaces();

        final PermitingFunction permitingFunction = mockery.mock(PermitingFunction.class);

        mockery.checking(new Expectations() {

            {
                oneOf(permitingFunction).getPlaceNumber(transition);
                will(returnValue(null));
            }
        });

        transition.setPermitingFunction(permitingFunction);

        assertThat("If permiting function result is negative, "
                + "YTransition predicate should return false", transition.canFire(), is(false));
    }

    @Test
    public void testPredicateWithDefaultFunctionAndNotEmptyInputPlace() {
        YTransition transition = createYTransitionInstanceWithConnectedPlaces();
        transition.getInputPlaces().get(0).setToken(new Token());

        assertThat("If Ytransition has default permiting function and it's first input place "
                + "is not empty and output is empty, predicate should be true",
                transition.canFire(), is(true));
    }

    @Test
    public void testTransitionFired() {
        YTransition transition = createYTransitionInstanceWithConnectedPlaces();
        Token token = new Token();
        token.setValue("attr", new Value(1d));
        Place firstInputPlace = transition.getInputPlaces().get(0);
        Place outputPlace = transition.getOutputPlace();
        firstInputPlace.setToken(token);

        TransformationFunction tf = new TransformationFunction() {

            public Token transform(Transition trasnition, Token token) {
                Token newToken = new Token(token);
                newToken.setValue("attr", new Value(token.getValue("attr").doubleValue() + 1));
                return newToken;
            }

            public TransformationFunction copy() {
                throw new UnsupportedOperationException("Not supported yet.");
            }
        };

        transition.setTransformationFunction(tf);
        Token expectedToken = tf.transform(transition, token);

        // Simulating model
        transition.fireIfPosible();
        transition.onDelayFinished();

        assertThat("After Y transition fired it's input place selected by permiting function "
                + "should be empty",
                firstInputPlace.isEmpty(), is(true));
        assertThat("After Y transition fired it's output place should contain "
                + "transformed token from input position",
                outputPlace.getToken(), equalTo(expectedToken));
    }

    @Test
    public void testYTransitionWithPermitinFunction() {
        Aggregate root = new AggregateInstance("root");
        ENetworksModel model = new ENetworksModel();
        model.setRootAggregate(root);

        YTransition y = new YTransition("Y");
        Place p0 = new Place("p0");
        Place p1 = new Place("p1");
        Place p2 = new Place("p2");
        Place out = new Place("out");

        y.addInputPlace(p0, 0);
        y.addInputPlace(p1, 1);
        y.addInputPlace(p2, 2);

        y.connectOutputPlace(out);

        p0.setToken(new Token());

        root.addPlaces(p0, p1, p2, out);
        root.addTransitions(y);

        y.setPermitingFunction(new InterpretedPermitingFunction(""
                + "IF (P['p0'].T==TRUE) RETURN 0; IF (P['p1'].T==TRUE) RETURN 1;"
                + "IF (P['p2'].T==TRUE) RETURN 2;"));
        if (y.canFire()) {
            y.fireTransition();
        }

        assertNull(p0.getToken());
        assertNotNull(out.getToken());

        out.setToken(null);
        p1.setToken(new Token());

        assertNotNull(p1.getToken());
        if (y.canFire()) {
            y.fireTransition();
        }

        assertNull(p1.getToken());
        assertNotNull(out.getToken());

        out.setToken(null);
        p2.setToken(new Token());

        assertNotNull(p2.getToken());
        if (y.canFire()) {
            y.fireTransition();
        }

        assertNull(p2.getToken());
        assertNotNull(out.getToken());
    }

    private YTransition createYTransitionInstanceWithConnectedPlaces() {
        YTransition result = createYTransitionInstance();

        Place output = new Place("output");
        result.connectOutputPlace(output);

        for (int i = 0; i < 3; i++) {
            Place input = new Place("P" + i);
            result.addInputPlace(input, i);
        }

        return result;
    }

    private YTransition createYTransitionInstance() {
        return new YTransition("Y");
    }
}