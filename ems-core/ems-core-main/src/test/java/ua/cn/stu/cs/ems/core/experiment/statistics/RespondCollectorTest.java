/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.cn.stu.cs.ems.core.experiment.statistics;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.*;

/**
 *
 * @author Dmitry Krivenko <dmitrykrivenko@gmal.com>
 */
public class RespondCollectorTest {

    public RespondCollectorTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testRespondCollector() {
        RespondCollector rc = new RespondCollector();
        rc.startStatCollection();
        assertTrue(rc.isStatisticsCollected());
        assertTrue(rc.isEmpty());

        rc.updateStat(1d);
        rc.updateStat(3d);
        rc.updateStat(2d);
        rc.updateStat(4d);

        rc.stopStatCollection();
        assertFalse(rc.isStatisticsCollected());
        assertFalse(rc.isEmpty());

        assertTrue(rc.getAverageRespond() == 2.5d);
        assertTrue(rc.getMinRespond() == 1d);
        assertTrue(rc.getMaxRespond() == 4d);
        assertTrue(rc.getVariance() == 1.25);
        assertTrue(rc.getCoefficientOfVariation() == (Math.sqrt(1.25) / 2.5));
        assertTrue(rc.getSampleSize() == 4);
    }
}
