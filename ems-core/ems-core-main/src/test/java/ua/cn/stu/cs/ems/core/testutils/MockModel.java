package ua.cn.stu.cs.ems.core.testutils;

import ua.cn.stu.cs.ems.core.ActiveInstance;
import ua.cn.stu.cs.ems.core.aggregates.Aggregate;
import ua.cn.stu.cs.ems.core.models.Model;
import ua.cn.stu.cs.ems.core.models.ModelListener;
import ua.cn.stu.cs.ems.core.models.ModelTimeListener;
import ua.cn.stu.cs.ems.core.transitions.SpecialTransition;

/**
 *
 * @author proger
 */
public class MockModel implements Model {

    private double currentTime;
    private double finishTime;

    public void setModelTime(double newTime) {
        currentTime = newTime;
    }

    public void setFinishTime(double newTime) {
        finishTime = newTime;
    }

    public void setRootAggregate(Aggregate aggregate) {
    }

    public Aggregate getRootAggregate() {
        return null;
    }

    public double getModelingTime() {
        return currentTime;
    }

    public double getFinishTime() {
        return finishTime;
    }

    public boolean addDelayable(ActiveInstance instance, double delay) {
        return false;
    }

    public void addSpecialTransition(SpecialTransition transition) {
    }

    public void start() {
    }

    public void reset() {
    }

    public void addModelListener(ModelListener listener) {
    }

    public void removeModelListener(ModelListener listener) {
    }

    public void addTimer(ModelTimeListener timeListener, double delay) {
    }

    public void interrupt() {
    }

    public void step() {
    }

    public void setInitModel(boolean init) {
    }
}
