package ua.cn.stu.cs.ems.core;

import java.util.List;
import ua.cn.stu.cs.ems.core.transitions.Transition;
import ua.cn.stu.cs.ems.ecli.Value;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JMock;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.junit.Assert.*;
;import static org.hamcrest.CoreMatchers.*;;

/**
 *
 * @author proger
 */
@RunWith(JMock.class)
public class PlaceTest {
    Mockery context = new JUnit4Mockery();

    public PlaceTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testPlaceSetToken() {
        Place pos = createPlace();
        Token token = new Token();
        token.setValue("name", new Value(1234d));
        pos.setToken(token);

        Token result = pos.getToken();
        assertEquals(result, token);
    }

    @Test
    public void testSetTokenToPlace() {
        final Transition input = context.mock(Transition.class, "input");
        final Transition output = context.mock(Transition.class, "output");

        final Place pos = createPlace();
        pos.setOutputActiveInstance(output);
        pos.setInputActiveInstance(input);

        context.checking(new Expectations() {{
            oneOf(output).placeStateChanged(pos);
        }});

        pos.setToken(new Token());
    }

    @Test
    public void testCleanTokenFromPlace() {
        final PlaceStateChangedListener listener = context.mock(PlaceStateChangedListener.class);

        final Place pos = createPlace();
        pos.addStateChangeListener(listener);

        final Transition inputTransition = context.mock(Transition.class, "inputTransition");
        final Transition outputTrasition = context.mock(Transition.class, "outputTransition");
        pos.setInputActiveInstance(inputTransition);
        pos.setOutputActiveInstance(outputTrasition);

        context.checking(new Expectations() {{
            oneOf(listener).placeStateChanged(pos);
            oneOf(inputTransition).placeStateChanged(pos);

            never(outputTrasition).placeStateChanged(pos);
        }});

        pos.setToken(null);
    }

    @Test
    public void testIsEmpty() {
        Place pos = createPlace();
        pos.setToken(null);

        assertTrue(pos.isEmpty());
    }

    @Test
    public void testGetPlaceStateChangedListeners() {
        Place pos = createPlace();
        for (int i = 0; i < 3; i++) {
            PlaceStateChangedListener listener =
                    context.mock(PlaceStateChangedListener.class, new Integer(i).toString());
            pos.addStateChangeListener(listener);
        }

        List<PlaceStateChangedListener> result = pos.getPlaceStateChangedListeners();
        assertThat(result.size(), is(3));
    }

    @Test
    public void testGetStateListenersFromNewPlace() {
        Place pos = createPlace();

        assertThat(pos.getPlaceStateChangedListeners().size(), is(0));
    }

    @Test(expected=JessException.class)
    public void testSetTokenToNotEmptyPlace() {
        Place place = new Place("P");
        place.setToken(new Token());
        place.setToken(new Token());
    }

    private Place createPlace() {
        return new Place("P1");
    }

}