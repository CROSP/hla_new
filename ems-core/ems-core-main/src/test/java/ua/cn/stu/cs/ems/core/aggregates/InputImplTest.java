package ua.cn.stu.cs.ems.core.aggregates;

import java.util.List;
import org.jmock.Expectations;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

;import static org.hamcrest.CoreMatchers.*;;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JMock;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.junit.runner.RunWith;
import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.models.Model;
import ua.cn.stu.cs.ems.ecli.Value;

/**
 *
 * @author proger
 *
 */
@RunWith(JMock.class)
public class InputImplTest {

    Mockery mockery = new JUnit4Mockery();

    public InputImplTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testGetInputPlaces() {
        InputImpl input = createInputImpl();

        Place pos = new Place("P1");
        input.setInputPlace(pos);

        List<Place> inputPlaces = input.getInputPlaces();

        assertThat(inputPlaces.size(), equalTo(1));
        assertThat(inputPlaces.get(0), equalTo(pos));
    }

    @Test
    public void testGetOutputPlaces() {
        InputImpl input = createInputImpl();

        Place pos = new Place("P1");
        input.setOutputPlace(pos);

        List<Place> outputPlaces = input.getOutputPlaces();

        assertThat(outputPlaces.size(), equalTo(1));
        assertThat(outputPlaces.get(0), equalTo(pos));
    }

    @Test
    public void testInputPlaceStateChanged() {
        final Input input = createInputWithTwoConnectedPlaces();
        Place inputPlace = input.getInputPlaces().get(0);

        final Model model = mockery.mock(Model.class, "model");
        final Aggregate aggregate = mockery.mock(Aggregate.class, "aggregate");
        input.setParentAggregate(aggregate);

        mockery.checking(new Expectations() {{
            oneOf(aggregate).getModel(); will(returnValue(model));
            oneOf(model).addDelayable(input, 0);
        }});

        Token token = new Token();
        token.setValue("attr", new Value(1d));
        inputPlace.setToken(token);
    }

    @Test
    public void testSetToken() {
        InputImpl input = createInputImpl();
        Place outputPlace = new Place("P1");

        input.setOutputPlace(outputPlace);
        Token token = new Token();
        token.setValue("attr", new Value(123d));

        input.setToken(token);
        
        assertThat(outputPlace.getToken(), equalTo(token));

    }

    @Test
    public void testInputInvoice() {
        final InputImpl input = createInputImpl();
        Place place = new Place("P");
        input.setOutputPlace(place);
        place.setToken(new Token());
        place.setInputActiveInstance(input);

        final Output output = mockery.mock(Output.class, "output");
        input.connectOutput(output);

        mockery.checking(new Expectations() {{
            oneOf(output).inputCanBeSet(input);
        }});

        place.setToken(null);
        // simulate model activity
        input.onDelayFinished();

    }

    @Test
    public void testInputPlaceDualConnection() {
        InputImpl input = createInputImpl();
        Place inputPlace = new Place("P");

        input.setInputPlace(inputPlace);
        assertEquals("If input place is connected to an input, input place's output active instance should be set",
                  input, inputPlace.getOutputActiveInstance());

        input.setInputPlace(null);
        assertEquals("If input place is connected to an input, input place's output active instance should be null",
                  null, inputPlace.getOutputActiveInstance());
    }

    @Test
    public void testOutputDualConnection() {
        InputImpl input = createInputImpl();
        Place outputPlace = new Place("P");

        input.setOutputPlace(outputPlace);
        assertEquals("If output place is connected to an input, output place's input active instance should be set",
                  input, outputPlace.getInputActiveInstance());

        input.setOutputPlace(null);
        assertEquals("If output place is connected to an input, output place's input active instance should be null",
                  null, outputPlace.getInputActiveInstance());
    }

    private InputImpl createInputWithTwoConnectedPlaces() {
        InputImpl input = createInputImpl();

        Place inputPos = new Place("P1");
        Place outpuPos = new Place("P2");
        input.setOutputPlace(outpuPos);
        input.setInputPlace(inputPos);
        
        return input;
    }
    
    private InputImpl createInputImpl() {
        return new InputImpl("I");
    }

}