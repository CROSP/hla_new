package ua.cn.stu.cs.ems.core.aggregates;

import ua.cn.stu.cs.ems.ecli.Value;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JMock;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author proger
 */
@RunWith(JMock.class)
public class VariableTest {

    Mockery mockery = new JUnit4Mockery();

    public VariableTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Test
    public void testVariableListenerCalledOnVariableValueChanged() {
        final VariableChangedListener listener = mockery.mock(VariableChangedListener.class, "listener");
        Aggregate root = new AggregateInstance("root");
        final AggregateVariable var = new AggregateVariable(root, "variable", new Value(100d));
        var.addVariableChangeListener(listener);

        mockery.checking(new Expectations() {

            {
                oneOf(listener).variableValueChanged(var);
            }
        });

        var.setValue(new Value(10d));
    }
}