package ua.cn.stu.cs.ems.core.experiment;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
;import static org.hamcrest.CoreMatchers.*;;

/**
 *
 * @author proger
 */
public class SecondaryStatisticsElementTest {

    public SecondaryStatisticsElementTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Test
    public void testEqualsWhenShouldBeEqual() {
        SecondaryStatisticsElement sse1 = new SecondaryStatisticsElement(1d, 2d, 3d, 4d, 5d, 6d, 7d);
        SecondaryStatisticsElement sse2 = new SecondaryStatisticsElement(1d, 2d, 3d, 4d, 5d, 6d, 7d);

        assertThat(sse1, equalTo(sse2));
    }

    @Test
    public void testEqualsWhenShouldNOTBeEqual1() {
        SecondaryStatisticsElement sse1 = new SecondaryStatisticsElement(1d,2d,3d,4d,5d, 6d,7d);
        SecondaryStatisticsElement sse2 = new SecondaryStatisticsElement(1d,2d,3d,4d,5d, 6d,8d);

        assertThat(sse1, not(equalTo(sse2)));
    }

    @Test
    public void testEqualsWhenShouldNOTBeEqual2() {
        SecondaryStatisticsElement sse1 = new SecondaryStatisticsElement(1d, 2d,3d,4d,5d, 6d,7d);
        SecondaryStatisticsElement sse2 = new SecondaryStatisticsElement(11d,2d,3d,4d,5d, 6d,7d);

        assertThat(sse1, not(equalTo(sse2)));
    }
}
