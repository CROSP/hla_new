package ua.cn.stu.cs.ems.core.xml;

import org.jdom.Document;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import ua.cn.stu.cs.ems.core.aggregates.AggregateDefinition;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.aggregates.AggregateInstance;
import ua.cn.stu.cs.ems.core.queues.FIFOPriorityQueue;
import ua.cn.stu.cs.ems.core.queues.InterpretedTokenPriorityFunction;
import ua.cn.stu.cs.ems.core.transitions.TTransition;
import ua.cn.stu.cs.ems.core.transitions.Transition;
import ua.cn.stu.cs.ems.core.transitions.functions.InterpretedDelayFunction;
import ua.cn.stu.cs.ems.core.transitions.functions.InterpretedTransformationFunction;
import ua.cn.stu.cs.ems.ecli.Value;

/**
 *
 * @author proger
 */
public class ModelWriterTest {

    public ModelWriterTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Test
    public void testWriteModelWithAddedVariables() throws Exception {
        AggregateDefinition ad = new AggregateDefinition("root");
        ad.setVariable("var", new Value(20.05));

        AggregateInstance instance = ad.createInstance("A");
        instance.setVariable("newVar", new Value(5));

        String xml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<model xmlns='http://www.cs.stu.cn.ua/jess/enetsdefinitions'>"
                + "<rootAggregate type='root'>"
                + "<variables><variable name='newVar' value='5'/></variables>"
                + "</rootAggregate></model>";

        ModelWriter writer = new ModelWriter();
        Document dom = writer.getModelDOM(instance);
        assertModelDOMCorrect(dom, xml);
    }

    @Test
    public void testWriteModelWithoutAddedVariables() throws Exception {
        AggregateDefinition ad = new AggregateDefinition("root");
        ad.setVariable("var", new Value(20.05));

        AggregateInstance instance = ad.createInstance("A");

        String xml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<model xmlns='http://www.cs.stu.cn.ua/jess/enetsdefinitions'>"
                + "<rootAggregate type='root'>"
                + "</rootAggregate></model>";

        ModelWriter writer = new ModelWriter();
        Document dom = writer.getModelDOM(instance);
        assertModelDOMCorrect(dom, xml);
    }

    @Test
    public void testWriteModelWithChangedFunction() throws Exception {
        AggregateDefinition ad = new AggregateDefinition("root");
        ad.addTransition(new TTransition("T"));

        AggregateInstance instance = ad.createInstance("A");
        Transition t = instance.getTransition("T");
        t.setDelayFunction(new InterpretedDelayFunction("RETURN 1;"));
        t.setTransformationFunction(new InterpretedTransformationFunction("T['attr'] = 15"));

        String xml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<model xmlns='http://www.cs.stu.cn.ua/jess/enetsdefinitions'>"
                + "<rootAggregate type='root'>"
                
                + "<transition name='T'>"
                + "<transformationFunction type='ecli'><![CDATA[T['attr'] = 15]]></transformationFunction>"
                + "<delayFunction type='ecli'><![CDATA[RETURN 1;]]></delayFunction>"
                + "</transition>"

                + "</rootAggregate></model>";

        ModelWriter writer = new ModelWriter();
        Document dom = writer.getModelDOM(instance);
        assertModelDOMCorrect(dom, xml);
    }

    @Test
    public void testWriteModelWithOneFunctionChanged() throws Exception {
        AggregateDefinition ad = new AggregateDefinition("root");
        Transition defTransition = new TTransition("T");
        ad.addTransition(defTransition);
        defTransition.setDelayFunction(new InterpretedDelayFunction("RETURN 1;"));

        AggregateInstance instance = ad.createInstance("A");
        Transition t = instance.getTransition("T");

        t.setTransformationFunction(new InterpretedTransformationFunction("T['attr'] = 15"));

        String xml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<model xmlns='http://www.cs.stu.cn.ua/jess/enetsdefinitions'>"
                + "<rootAggregate type='root'>"

                + "<transition name='T'>"
                + "<transformationFunction type='ecli'><![CDATA[T['attr'] = 15]]></transformationFunction>"
                + "</transition>"

                + "</rootAggregate></model>";

        ModelWriter writer = new ModelWriter();
        Document dom = writer.getModelDOM(instance);
        assertModelDOMCorrect(dom, xml);
    }

    @Test
    public void testWriteModelWithFunctionChanged() throws Exception {
        AggregateDefinition ad = new AggregateDefinition("root");
        FIFOPriorityQueue q1 = new FIFOPriorityQueue("q1", new InterpretedTokenPriorityFunction("RETURN 1;"));
        FIFOPriorityQueue q2 = new FIFOPriorityQueue("q2", new InterpretedTokenPriorityFunction("RETURN 2;"));
        ad.addQueues(q1, q2);

        AggregateInstance instance = ad.createInstance("A");
        FIFOPriorityQueue instanceQ1 = (FIFOPriorityQueue) instance.getQueue("q1");
        instanceQ1.setPriorityFunction(new InterpretedTokenPriorityFunction("RETURN 100;"));

        String xml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<model xmlns='http://www.cs.stu.cn.ua/jess/enetsdefinitions'>"
                + "<rootAggregate type='root'>"

                + "<queue name='q1'>"
                + "<priorityFunction type='ecli'><![CDATA[RETURN 100;]]></priorityFunction>"
                + "</queue>"

                + "</rootAggregate></model>";

        ModelWriter writer = new ModelWriter();
        Document dom = writer.getModelDOM(instance);
        assertModelDOMCorrect(dom, xml);
    }

    @Test
    public void testWritingModelWithTwoAggregates() throws Exception {
        AggregateDefinition child = new AggregateDefinition("child");
        FIFOPriorityQueue q1 = new FIFOPriorityQueue("q1", new InterpretedTokenPriorityFunction("RETURN 1;"));
        child.addQueue(q1);

        AggregateDefinition parent = new AggregateDefinition("parent");
        parent.addChildAggreagateDefinition(child, "baby");

        AggregateInstance instance = parent.createInstance("Root");

        FIFOPriorityQueue fifo = (FIFOPriorityQueue) instance.getAggregate("baby").getQueue("q1");
        fifo.setPriorityFunction(new InterpretedTokenPriorityFunction("RETURN 100;"));

        String xml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<model xmlns='http://www.cs.stu.cn.ua/jess/enetsdefinitions'>"
                + "<rootAggregate type='parent'>"

                + "<aggregate name='baby'>"
                + "<queue name='q1'>"
                + "<priorityFunction type='ecli'><![CDATA[RETURN 100;]]></priorityFunction>"
                + "</queue>"
                + "</aggregate>"

                + "</rootAggregate></model>";

        ModelWriter writer = new ModelWriter();
        Document dom = writer.getModelDOM(instance);
        assertModelDOMCorrect(dom, xml);
    }

    @Test
    public void testWritingModelWithCleanMarkingChanged() throws Exception {
        AggregateDefinition parent = new AggregateDefinition("Root");
        Place p1 = new Place("P"); p1.setToken(new Token());
        parent.addPlace(p1);

        AggregateInstance instance = parent.createInstance("Root");
        instance.getPlace("P").setToken(null);

        String xml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<model xmlns='http://www.cs.stu.cn.ua/jess/enetsdefinitions'>"
                + "<rootAggregate type='Root'>"

                + "<place name='P'>"
                + "</place>"

                + "</rootAggregate></model>";

        ModelWriter writer = new ModelWriter();
        Document dom = writer.getModelDOM(instance);
        assertModelDOMCorrect(dom, xml);
    }

    @Test
    public void testWritingModelWithoutMarkingChanged() throws Exception {
        AggregateDefinition parent = new AggregateDefinition("Root");
        Place p1 = new Place("P"); p1.setToken(new Token());
        parent.addPlace(p1);

        AggregateInstance instance = parent.createInstance("Root");

        String xml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<model xmlns='http://www.cs.stu.cn.ua/jess/enetsdefinitions'>"
                + "<rootAggregate type='Root'>"

                + "</rootAggregate></model>";

        ModelWriter writer = new ModelWriter();
        Document dom = writer.getModelDOM(instance);
        assertModelDOMCorrect(dom, xml);
    }
    
    @Test
    public void testWritingModelWithEmptyToken() throws Exception {
        AggregateDefinition parent = new AggregateDefinition("Root");
        Place p1 = new Place("P");
        parent.addPlace(p1);

        AggregateInstance instance = parent.createInstance("Root");
        instance.getPlace("P").setToken(new Token());

        String xml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<model xmlns='http://www.cs.stu.cn.ua/jess/enetsdefinitions'>"
                + "<rootAggregate type='Root'>"

                + "<place name='P'>"
                + "<token/>"
                + "</place>"

                + "</rootAggregate></model>";

        ModelWriter writer = new ModelWriter();
        Document dom = writer.getModelDOM(instance);
        assertModelDOMCorrect(dom, xml);
    }

    @Test
    public void testWritingModelWithNotEmptyToken() throws Exception {
        AggregateDefinition parent = new AggregateDefinition("Root");
        Place p1 = new Place("P");
        parent.addPlace(p1);

        AggregateInstance instance = parent.createInstance("Root");
        Token t = new Token(); t.setValue("attr", new Value(1.1d));
        instance.getPlace("P").setToken(t);

        String xml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<model xmlns='http://www.cs.stu.cn.ua/jess/enetsdefinitions'>"
                + "<rootAggregate type='Root'>"

                + "<place name='P'>"
                + "<token>"
                + "<attribute name='attr' value='1.1'/>"
                + "</token>"
                + "</place>"

                + "</rootAggregate></model>";

        ModelWriter writer = new ModelWriter();
        Document dom = writer.getModelDOM(instance);
        assertModelDOMCorrect(dom, xml);
    }

    private void assertModelDOMCorrect(Document dom, String xml) throws Exception {
        assertModelDOMCorrect(dom, xml, false);
    }

    private void assertModelDOMCorrect(Document dom, String xml, boolean output) throws Exception {
        XMLUtils utils = new XMLUtils();
        utils.assertDOMToXML(dom, xml, output);
    }



}