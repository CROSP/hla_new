package ua.cn.stu.cs.ems.core.queues;

import java.util.ArrayList;
import org.junit.*;
import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.aggregates.Aggregate;
import ua.cn.stu.cs.ems.core.models.ENetworksModel;
import static ua.cn.stu.cs.ems.core.queues.TestQueueOrder.assertQueueOrder;
import ua.cn.stu.cs.ems.core.testutils.InstanceFactory;
import ua.cn.stu.cs.ems.ecli.Value;

/**
 *
 * @author proger
 */
public class FIFOPriorityQueueTest {

    public FIFOPriorityQueueTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testFIFOPriorityQueue() {
        String source = "RETURN 1/T['attr'];";
        ENetworksModel model = new ENetworksModel();
        Aggregate a = InstanceFactory.createAggregateInstanceWithName("A");

        InterpretedTokenPriorityFunction itpf = new InterpretedTokenPriorityFunction(source);

        AbstractPriorityQueue queue = createFIFOPriorityQueueInstance("Q", itpf);
        a.addQueue(queue);
        model.setRootAggregate(a);

        final Token t1 = new Token();
        t1.setValue("attr", new Value(0.1d));
        final Token t2 = new Token();
        t2.setValue("attr", new Value(0.2d));
        final Token t3 = new Token();
        t3.setValue("attr", new Value(0.5d));
        final Token t4 = new Token();
        t4.setValue("attr", new Value(1d));

        ArrayList<Token> input = new ArrayList<Token>() {

            {
                add(t1);
                add(t3);
                add(t2);
                add(t4);
            }
        };

        ArrayList<Token> output = new ArrayList<Token>() {

            {
                add(t1);
                add(t2);
                add(t3);
                add(t4);
            }
        };

        assertQueueOrder(queue, input, output);
    }

    private FIFOPriorityQueue createFIFOPriorityQueueInstance(String name, TokenPriorityFunction tpf) {
        return new FIFOPriorityQueue(name, tpf);
    }
}