package ua.cn.stu.cs.ems.core.aggregates;


import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JMock;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.junit.*;
import org.junit.runner.RunWith;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.*;

/**
 * @author proger
 */
@RunWith(JMock.class)
public class AbstractAggregateChildTest {

    Mockery mockery = new JUnit4Mockery();

    public AbstractAggregateChildTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testChangeNameWithNoNameClash() {
        final AbstractAggregateChild aac = createAbstractAggregateChild("aac");
        final Aggregate aggregate = mockery.mock(Aggregate.class);
        aac.setParentAggregate(aggregate);

        final String newName = "newName";

        mockery.checking(new Expectations() {{
            oneOf(aggregate).getAggregateChild(newName);
            will(returnValue(null));
            allowing(aggregate).childNameChanged(aac, "aac", newName);
        }});


        boolean changeResult = aac.changeName(newName);

        assertTrue(changeResult);
        assertThat(aac.getName(), equalTo(newName));
    }

    @Test
    public void testChangeNameWithNameClash() {
        String oldName = "aac";
        final AbstractAggregateChild aac = createAbstractAggregateChild(oldName);
        final Aggregate aggregate = mockery.mock(Aggregate.class);
        aac.setParentAggregate(aggregate);

        final String newName = "newName";

        mockery.checking(new Expectations() {{
            oneOf(aggregate).getAggregateChild(newName);
            will(returnValue(aac));
        }});

        boolean changeResult = aac.changeName(newName);

        assertFalse(changeResult);
        assertThat(aac.getName(), equalTo(oldName));
    }

    @Test
    public void testChangeNameToTheOldName() {
        final String oldName = "aac";
        final AbstractAggregateChild aac = createAbstractAggregateChild(oldName);
        final Aggregate aggregate = mockery.mock(Aggregate.class);
        aac.setParentAggregate(aggregate);

        mockery.checking(new Expectations() {{
            allowing(aggregate).getAggregateChild(oldName);
            will(returnValue(aac));
        }});

        boolean changeResult = aac.changeName(oldName);

        assertTrue(changeResult);
        assertThat(aac.getName(), equalTo(oldName));
    }

    private AbstractAggregateChild createAbstractAggregateChild(String name) {
        return new AbstractAggregateChild(name) {
        };
    }

}