package ua.cn.stu.cs.ems.core.testutils;

import java.util.Set;
import org.hamcrest.Matcher;
import org.hamcrest.Factory;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;
/**
 *
 * @author proger
 */
public class Matchers {
    @Factory
    public static <T> Matcher<Set> hasSameElements(Set set) {
        return new SameElements(set);
    }
}

/**
 * JMock matcher to check if two set has same elements
 * @author proger
 */
class SameElements extends TypeSafeMatcher<Set> {

    Set set;

    public SameElements(Set set) {
        this.set = set;
    }

    @Override
    public boolean matchesSafely(Set item) {
        if (item.size() != set.size())
            return false;
        for (Object o : item)
            if (! set.contains(o))
                return false;
        return true;

    }

    public void describeTo(Description description) {
        description.appendText("Sets contains different elements");
    }

}