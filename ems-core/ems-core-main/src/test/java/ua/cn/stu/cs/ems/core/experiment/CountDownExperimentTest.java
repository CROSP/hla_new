package ua.cn.stu.cs.ems.core.experiment;

import ua.cn.stu.cs.ems.core.Token;
import java.util.ArrayList;
import java.util.List;
import ua.cn.stu.cs.ems.core.models.Model;
import ua.cn.stu.cs.ems.core.Place;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import ua.cn.stu.cs.ems.core.aggregates.AggregateInstance;
import ua.cn.stu.cs.ems.core.aggregates.AggregateVariable;
import ua.cn.stu.cs.ems.core.models.ENetworksModel;
import ua.cn.stu.cs.ems.core.transitions.TTransition;
import ua.cn.stu.cs.ems.core.transitions.functions.InterpretedDelayFunction;
import ua.cn.stu.cs.ems.ecli.Value;
import static org.junit.Assert.*;

/**
 *
 * @author proger
 */
public class CountDownExperimentTest {

    public CountDownExperimentTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Test
    public void testCollectSecondaryStatistics() {
        Place in = new Place("PIn");
        Place out = new Place("POut");
        TTransition tt = new TTransition("TT");

        in.setToken(new Token());

        tt.setInputPlace(in);
        tt.setOutputPlace(out);

        tt.setDelayFunction(new InterpretedDelayFunction("V['respond'] = V['factor']; RETURN 1;"));

        AggregateInstance rootAggregate = createRootAggregate();
        rootAggregate.addPlaces(in, out);
        rootAggregate.addTransition(tt);

        rootAggregate.setVariable("factor", new Value(0));
        rootAggregate.setVariable("respond", new Value(0));

        AggregateVariable factorVar = rootAggregate.getVariable("factor");
        AggregateVariable respondVar = rootAggregate.getVariable("respond");

        FactorImpl factor = new FactorImpl(factorVar, new StepValueGenerator(1, 4, 1));

        Model m = new ENetworksModel();
        m.setRootAggregate(rootAggregate);

        Respond respond = new Respond(respondVar);
        CountDownExperiment cde = new CountDownExperiment(m, factor, respond, 3);
        cde.startExperiment();

        List<SecondaryStatisticsElement> resultSecondaryStatistics = cde.getSecondaryStatistics();
        List<SecondaryStatisticsElement> expectedSecondaryStatistics = new ArrayList<SecondaryStatisticsElement>() {

            {
                add(new SecondaryStatisticsElement(1, 1, 0, 0, 1, 1, 3));
                add(new SecondaryStatisticsElement(2, 2, 0, 0, 2, 2, 3));
                add(new SecondaryStatisticsElement(3, 3, 0, 0, 3, 3, 3));

            }
        };


        assertArrayEquals(expectedSecondaryStatistics.toArray(new SecondaryStatisticsElement[1]),
                resultSecondaryStatistics.toArray(new SecondaryStatisticsElement[1]));
    }

    private AggregateInstance createRootAggregate() {
        return new AggregateInstance("Root");
    }
}
