package ua.cn.stu.cs.ems.core.testutils;


import ua.cn.stu.cs.ems.core.aggregates.AggregateInstance;

/**
 *
 * @author proger
 */
public class InstanceFactory {
    public static AggregateInstance createAggregateInstance() {
        return new AggregateInstance("A");
    }

    public static AggregateInstance createAggregateInstanceWithName(String name) {
        return new AggregateInstance(name);
    }
}
