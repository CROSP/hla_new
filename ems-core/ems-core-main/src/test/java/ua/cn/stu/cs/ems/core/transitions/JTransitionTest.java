package ua.cn.stu.cs.ems.core.transitions;

import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.Place;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ua.cn.stu.cs.ems.core.transitions.functions.TransformationFunction;
import ua.cn.stu.cs.ems.ecli.Value;
import static org.junit.Assert.*;
;import static org.hamcrest.CoreMatchers.*;;

/**
 *
 * @author proger
 */
public class JTransitionTest {

    public JTransitionTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testTransfer() {
        JTransition transition = createJTransition();
        // Initialize input places for transition

        for (int i = 0; i < 5; i++) {
            Place p = new Place("Place");
            Token m = new Token();
            m.setValue("attr", new Value(new Double(i)));

            p.setToken(m);
            transition.addInputPlace(p, i);
        }

        TransformationFunction tf = new TransformationFunction() {

            public Token transform(Transition transition, Token token) {
                Token newToken = new Token(token);
                newToken.setValue("attr", new Value(token.getValue("attr").doubleValue() + 1));
                return newToken;
            }

            public TransformationFunction copy() {
                throw new UnsupportedOperationException("Not supported yet.");
            }
        };

        Token expectedToken = tf.transform(transition, transition.getInputPlaces().get(0).getToken());
        transition.setOutputPlace(new Place("P"));
        transition.setTransformationFunction(tf);

        // Simulate invoke from model
        transition.onDelayFinished();

        for (Place pos : transition.getInputPlaces()) {
            assertThat("JTransition's input places should be empty after a transition fired",
                    pos.getToken(), nullValue());
        }

        Token token = transition.getOutputPlaces().get(0).getToken();
        assertThat("After J-transition fired it's output place's should contain transformed token from first input place",
                token, equalTo(expectedToken));
    }

    @Test
    public void testTransitionPredicateWithNoTokens() {
        JTransition transition = getTransitionWithConnectedPlaces();

        assertThat("If all places connected to transition are empty it shouldn't be ready to fire",
                transition.canFire(),
                is(false));
    }

    @Test
    public void testTransitionPredicateWithAllTokensSet() {
        JTransition transition = getTransitionWithConnectedPlaces();

        for (Place pos : transition.getInputPlaces()) {
            pos.setToken(new Token());
        }

        transition.getOutputPlace().setToken(new Token());

        assertThat("If all places connected to transition contains tokens it shouldn't be ready to fire",
                transition.canFire(),
                is(false));
    }

    @Test
    public void testTransitionPredicateWithAllInputTokensSet() {
        JTransition transition = getTransitionWithConnectedPlaces();

        for (Place pos : transition.getInputPlaces()) {
            pos.setToken(new Token());
        }

        assertThat("If all input places connected to transition contain tokens"
                + "and there is no token in output place, transtion should be ready to fire",
                transition.canFire(),
                is(true));
    }

    private JTransition getTransitionWithConnectedPlaces() {
        JTransition transition = createJTransition();

        for (int i = 0; i < 3; i++) {
            Place p = new Place("P");
            transition.addInputPlace(p, i);
            p.setOutputActiveInstance(transition);
        }

        Place output = new Place("P");
        output.setInputActiveInstance(transition);
        transition.setOutputPlace(output);

        return transition;
    }

    private JTransition createJTransition() {
        return new JTransition("J");
    }
}
