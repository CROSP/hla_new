
package ua.cn.stu.cs.ems.core.aggregates;

import ua.cn.stu.cs.ems.core.queues.Queue;
import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.transitions.Transition;
import ua.cn.stu.cs.ems.core.Place;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import ua.cn.stu.cs.ems.core.queues.FIFOPriorityQueue;
import ua.cn.stu.cs.ems.core.queues.FIFOQueue;
import ua.cn.stu.cs.ems.core.queues.TokenPriorityFunction;
import ua.cn.stu.cs.ems.core.transitions.FTransition;
import ua.cn.stu.cs.ems.core.transitions.JTransition;
import ua.cn.stu.cs.ems.core.transitions.TTransition;
import static org.junit.Assert.*;
;import static org.hamcrest.CoreMatchers.*;;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author proger
 */
public class AggregateDefinitionTest {
    
    final static Logger logger = LoggerFactory.getLogger(AggregateDefinitionTest.class);

    public AggregateDefinitionTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Test
    public void createAggregateWithOneTransition() {
        AggregateDefinition ad = new AggregateDefinition("ad");

        TTransition tt = new TTransition("T");
        Place input = new Place("input");
        Place output = new Place("output");
        tt.setInputPlace(input);
        tt.setOutputPlace(output);

        ad.addTransition(tt);
        ad.addPlaces(input, output);

        String instanceName = "instance";
        AggregateInstance ai = ad.createInstance(instanceName);
        assertEquals("Agregate instance's name should be equals to specified in AggregateDefinition.createInstance()",
                instanceName, ai.getName());

        Transition ait = ai.getTransition("T");
        Place aiInput = ai.getPlace("input");
        Place aiOutput = ai.getPlace("output");

        assertTrue(ait instanceof TTransition);
        assertThat(ait.getInputPlaces().get(0), is(aiInput));
        assertThat(ait.getOutputPlaces().get(0), is(aiOutput));

        assertEquals(aiInput.getOutputActiveInstance(), ait);
        assertEquals(aiOutput.getInputActiveInstance(), ait);
    }

    @Test
    public void createAggreateWithTwoTransitions() {
        AggregateDefinition ad = new AggregateDefinition("ad");

        Place input = new Place("input");
        Place output = new Place("output");
        Place fj1 = new Place("fj1");
        Place fj2 = new Place("fj2");

        FTransition ft = new FTransition("F");
        JTransition jt = new JTransition("J");

        ft.setInputPlace(input);
        ft.addOutputPlace(fj1, 0);
        ft.addOutputPlace(fj2, 1);

        jt.addInputPlace(fj1, 0);
        jt.addInputPlace(fj2, 1);
        jt.setOutputPlace(output);

        ad.addPlaces(input, output, fj1, fj2);
        ad.addTransitions(ft, jt);

        AggregateInstance ai = ad.createInstance("instance");

        FTransition aiFT = (FTransition) ai.getTransition("F");
        JTransition aiJT = (JTransition) ai.getTransition("J");

        Place aiInput = ai.getPlace("input");
        Place aiOutput = ai.getPlace("output");
        Place aiFJ1 = ai.getPlace("fj1");
        Place aiFJ2 = ai.getPlace("fj2");

        assertEquals(aiInput, aiFT.getInputPlace());
        assertEquals(aiFT, aiInput.getOutputActiveInstance());

        assertEquals(aiFT, aiFJ1.getInputActiveInstance());
        assertEquals(aiFT, aiFJ2.getInputActiveInstance());

        assertEquals(aiJT, aiFJ1.getOutputActiveInstance());
        assertEquals(aiJT, aiFJ2.getOutputActiveInstance());

        assertEquals(aiOutput, aiJT.getOutputPlace());
        assertEquals(aiJT, aiOutput.getInputActiveInstance());
    }

    @Test
    public void testCreatingAggregateWithQueues() {
        AggregateDefinition ad = new AggregateDefinition("ad");
        FIFOQueue fifo = new FIFOQueue("FIFO");

        TokenPriorityFunction tpf = new TokenPriorityFunction() {

            public int calculateTokenPriority(Queue queue, Token token) {
                return 0;
            }
        };

        FIFOPriorityQueue fifop = new FIFOPriorityQueue("FIFOP", tpf);

        Place first = new Place("first");
        Place middle = new Place("middle");
        Place last = new Place("last");

        fifo.connectInputPlace(first);
        fifo.connectOutputPlace(middle);
        fifop.connectInputPlace(middle);
        fifop.connectOutputPlace(last);

        ad.addPlaces(first, middle, last);
        ad.addQueues(fifo, fifop);

        AggregateInstance ai = ad.createInstance("instance");

        FIFOQueue aiFIFO = (FIFOQueue) ai.getQueue("FIFO");
        FIFOPriorityQueue aiFIFOP = (FIFOPriorityQueue) ai.getQueue("FIFOP");

        Place aiFirst = ai.getPlace("first");
        Place aiMiddle = ai.getPlace("middle");
        Place aiLast = ai.getPlace("last");

        assertEquals(aiFirst, aiFIFO.getInputPlace());
        assertEquals(aiFIFO, aiFirst.getOutputActiveInstance());

        assertEquals(aiMiddle, aiFIFO.getOutputPlace());
        assertEquals(aiFIFO, aiMiddle.getInputActiveInstance());

        assertEquals(aiMiddle, aiFIFOP.getInputPlace());
        assertEquals(aiFIFOP, aiMiddle.getOutputActiveInstance());

        assertEquals(aiLast, aiFIFOP.getOutputPlace());
        assertEquals(aiFIFOP, aiLast.getInputActiveInstance());
    }

    @Test
    public void testInputsOutputsCopy() {
        AggregateDefinition ad = new AggregateDefinition("ad");

        InputImpl input = new InputImpl("input");
        OutputImpl output = new OutputImpl("output");
        Place place = new Place("place");

        input.setOutputPlace(place);
        output.connectInputPlace(place);

        ad.addInput(input, 0);
        ad.addOutput(output, 0);
        ad.addPlace(place);

        AggregateInstance ai = ad.createInstance("instance");

        InputImpl aiInput = (InputImpl) ai.getInput("input");
        OutputImpl aiOutput = (OutputImpl) ai.getOutput("output");
        Place aiPlace = ai.getPlace("place");

        assertEquals(aiPlace, aiInput.getOutputPlaces().get(0));
        assertEquals(aiInput, aiPlace.getInputActiveInstance());

        assertEquals(aiPlace, aiOutput.getInputPlaces().get(0));
        assertEquals(aiOutput, aiPlace.getOutputActiveInstance());
    }

    @Test
    public void testCreateEmptyAggregateWithInputAndOutput() {
        AggregateDefinition ad = new AggregateDefinition("ad");

        InputImpl input = new InputImpl("input");
        OutputImpl output = new OutputImpl("output");

        ad.addInput(input, 0);
        ad.addOutput(output, 0);

        AggregateInstance ai = ad.createInstance("instance");
        InputImpl aiInput = (InputImpl) ai.getInput("input");
        OutputImpl aiOutput = (OutputImpl) ai.getOutput("output");

        assertEquals(0, aiInput.getOutputPlaces().size());
        assertEquals(0, aiOutput.getInputPlaces().size());
    }

    @Test
    public void testCreateAggregateWithChildAggregate() {
        AggregateDefinition child = new AggregateDefinition("child");

        InputImpl ii = new InputImpl("input");
        OutputImpl oi = new OutputImpl("output");
        Place innerPlace = new Place("inner");

        ii.setOutputPlace(innerPlace);
        oi.connectInputPlace(innerPlace);

        child.addInput(ii, 0);
        child.addOutput(oi, 0);
        child.addPlace(innerPlace);

        AggregateDefinition parent = new AggregateDefinition("parent");
        AggregateDefinitionReference childReference = parent.addChildAggreagateDefinition(child, "childAgr");
        Input refInput = childReference.getInput("input");
        Output refOutput = childReference.getOutput("output");

        Place input = new Place("input");
        Place output = new Place("output");

        refInput.setInputPlace(input);
        refOutput.connectOutputPlace(output);
        
        parent.addPlace(input);
        parent.addPlace(output);

        AggregateInstance aiParent = parent.createInstance("instance");
        Place aiInputPlace = aiParent.getPlace("input");
        Place aiOutputPlace = aiParent.getPlace("output");
        AggregateInstance aiChild = (AggregateInstance) aiParent.getAggregate("childAgr");

        Place aiInnerPlace = aiChild.getPlace("inner");
        Input aiInput = aiChild.getInput("input");
        Output aiOutput = aiChild.getOutput("output");
        
        assertEquals(aiInput, aiInputPlace.getOutputActiveInstance());
        assertEquals(aiInputPlace, aiInput.getInputPlaces().get(0));

        assertEquals(aiInnerPlace, aiInput.getOutputPlace());
        assertEquals(aiInput, aiInnerPlace.getInputActiveInstance());

        assertEquals(aiOutput, aiInnerPlace.getOutputActiveInstance());
        assertEquals(aiInnerPlace, aiOutput.getInputPlace());

        assertEquals(aiOutputPlace, aiOutput.getOutputPlaces().get(0));
        assertEquals(aiOutput, aiOutputPlace.getInputActiveInstance());
    }
    
    @Test
    public void testCreateAggregateWithTwoConnectedChildAggregates() {
        AggregateDefinition child1 = new AggregateDefinition("child1");

        InputImpl i = new InputImpl("I0");
        OutputImpl o = new OutputImpl("O0");
        Place innerPlace = new Place("P0");

        i.setOutputPlace(innerPlace);
        o.connectInputPlace(innerPlace);

        child1.addInput(i, 0);
        child1.addOutput(o, 0);
        child1.addPlace(innerPlace);

        AggregateDefinition child2 = new AggregateDefinition("child2");
        
        i = new InputImpl("I0");
        o = new OutputImpl("O0");
        innerPlace = new Place("P0");
        
        i.setOutputPlace(innerPlace);
        o.connectInputPlace(innerPlace);

        child2.addInput(i, 0);
        child2.addOutput(o, 0);
        child2.addPlace(innerPlace);

        
        
        AggregateDefinition parent = new AggregateDefinition("parent");
        AggregateDefinitionReference childReference1 = parent.addChildAggreagateDefinition(child1, "childAgr1");
        AggregateDefinitionReference childReference2 = parent.addChildAggreagateDefinition(child2, "childAgr2");
//        childReference2.getInput("I0").connectOutput(childReference1.getOutput("O0"));
        childReference1.getOutput("O0").addInput(childReference2.getInput("I0"), 0);

        Output origOut = child1.getOutput("O0");
        Output refOut = childReference1.getOutput("O0");
        
        Input origIn = child2.getInput("I0");
        Input refIn = childReference2.getInput("I0");
        
        assertNotSame("Place must be same in reference and real aggregate for outputs", 
                refOut.getInputPlace(), origOut.getInputPlace());
        assertNotSame("Place can be same in reference and real aggregate for inputs", 
                refIn.getOutputPlace(), origIn.getOutputPlace());
    }
}