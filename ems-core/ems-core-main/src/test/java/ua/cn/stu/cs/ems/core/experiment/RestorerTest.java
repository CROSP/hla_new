package ua.cn.stu.cs.ems.core.experiment;

import ua.cn.stu.cs.ems.core.aggregates.AggregateInstance;
import java.util.HashMap;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.aggregates.Aggregate;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ua.cn.stu.cs.ems.core.transitions.TTransition;
import ua.cn.stu.cs.ems.ecli.Value;
import static org.junit.Assert.*;
import static ua.cn.stu.cs.ems.core.testutils.InstanceFactory.*;

/**
 *
 * @author proger
 */
public class RestorerTest {

    public RestorerTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testRestoring() {
        AggregateInstance aggregate = createAggregateInstance();

        Token token = new Token(); token.setValue("attr", new Value(1d));
        TTransition transition = new TTransition("T");
        Place p1 = new Place("p1"); p1.setToken(new Token(token));
        Place p2 = new Place("p2");

        transition.setInputPlace(p1);
        transition.setOutputPlace(p2);

        aggregate.addPlaces(p1, p2);
        aggregate.addTransition(transition);

        HashMap<String, Double> variables = new HashMap<String, Double>() {{
            this.put("var1", 1d);
            this.put("var2", 2d);
            this.put("var3", 100d);
        }};

        AggregateInstance parent = createAggregateInstance();
        parent.addChildAggregate(aggregate);
        setVariables(parent, variables);

        Restorer restorer = new Restorer(parent);

        parent.setVariable("var3", new Value(654321d));
        parent.setVariable("var1", new Value(123456d));
        p1.setToken(null);
        p2.setToken(new Token());

        restorer.restoreState();
        checkVariables(parent, variables);

        assertTrue(p2.isEmpty());
        assertFalse(p1.isEmpty());
        assertEquals(p1.getToken(), token);
    }

    private void setVariables(Aggregate aggregate, HashMap<String, Double> variables) {
        for (String name : variables.keySet())
            aggregate.setVariable(name, new Value(variables.get(name)));
    }

    private void checkVariables(Aggregate aggregate, HashMap<String, Double> variables) {
        assertEquals(aggregate.getVariablesNames().size(), variables.size());

        for (String name : variables.keySet()) {
            double expected = variables.get(name);
            assertEquals(expected, aggregate.getVariable(name).getValue().doubleValue(), 0.001);
        }

    }

}