package ua.cn.stu.cs.ems.core.experiment.statistics;

import java.util.Set;
import java.util.HashSet;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
;import static org.hamcrest.CoreMatchers.*;;
import static ua.cn.stu.cs.ems.core.testutils.Matchers.*;

/**
 *
 * @author proger
 */
public class StatisticsResultTest {

    public StatisticsResultTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Test
    public void testGetValue() {
        StatisticsResult result = new StatisticsResult();

        Double setVal = 123d;
        result.setResultValue(StatisticsParameters.AVERAGE_OCCUPIED_TIME, setVal);

        assertThat(result.getResultValue(StatisticsParameters.AVERAGE_OCCUPIED_TIME), equalTo(setVal));
        assertThat(result.getResultValue(null), nullValue());
    }

    @Test
    public void testGetKeys() {
        StatisticsResult result = new StatisticsResult();
        Set<StatisticsParameters> expectedKeys = new HashSet<StatisticsParameters>() {{
            add(StatisticsParameters.AVERAGE_OCCUPIED_TIME);
            add(StatisticsParameters.AVERAGE_QUEUE_LENGTH);
            add(StatisticsParameters.AVERAGE_TIME_IN_DELAY);
        }};

        result.setResultValue(StatisticsParameters.AVERAGE_OCCUPIED_TIME, 123d);
        result.setResultValue(StatisticsParameters.AVERAGE_QUEUE_LENGTH, 234d);
        result.setResultValue(StatisticsParameters.AVERAGE_TIME_IN_DELAY, 345d);

        assertThat(result.getKeys(), hasSameElements(expectedKeys));
    }
    
    @Test
    public void testEqualsWhenSholdBeEqual() {
        StatisticsResult sr1 = new StatisticsResult();
        sr1.setResultValue(StatisticsParameters.OCCUPIED_COEFFICIENT, 1d);
        sr1.setResultValue(StatisticsParameters.AVERAGE_QUEUE_LENGTH, 2d);
        sr1.setResultValue(StatisticsParameters.AVERAGE_TIME_IN_DELAY, 3d);
        
        StatisticsResult sr2 = new StatisticsResult();
        sr2.setResultValue(StatisticsParameters.OCCUPIED_COEFFICIENT, 1d);
        sr2.setResultValue(StatisticsParameters.AVERAGE_QUEUE_LENGTH, 2d);
        sr2.setResultValue(StatisticsParameters.AVERAGE_TIME_IN_DELAY, 3d);
        
        assertThat(sr1, equalTo(sr2));
    }
    
    @Test
    public void testEqualsWhenShouldNOTBeEqual() {
        StatisticsResult sr1 = new StatisticsResult();
        sr1.setResultValue(StatisticsParameters.OCCUPIED_COEFFICIENT, 1d);
        sr1.setResultValue(StatisticsParameters.AVERAGE_QUEUE_LENGTH, 2d);
        sr1.setResultValue(StatisticsParameters.AVERAGE_TIME_IN_DELAY, 3d);
        
        StatisticsResult sr2 = new StatisticsResult();
        sr2.setResultValue(StatisticsParameters.OCCUPIED_COEFFICIENT, 100d);
        sr2.setResultValue(StatisticsParameters.AVERAGE_QUEUE_LENGTH, 200d);
        sr2.setResultValue(StatisticsParameters.AVERAGE_TIME_IN_DELAY, 300d);
        
        assertThat(sr1, not(equalTo(sr2)));
    }

}