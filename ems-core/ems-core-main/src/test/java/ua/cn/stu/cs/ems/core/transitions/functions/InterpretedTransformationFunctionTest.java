package ua.cn.stu.cs.ems.core.transitions.functions;

import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.aggregates.Aggregate;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ua.cn.stu.cs.ems.core.models.ENetworksModel;
import ua.cn.stu.cs.ems.core.transitions.TTransition;
import ua.cn.stu.cs.ems.core.transitions.Transition;
import ua.cn.stu.cs.ems.ecli.Value;
import static org.junit.Assert.*;
;import static org.hamcrest.CoreMatchers.*;;
import static ua.cn.stu.cs.ems.core.testutils.InstanceFactory.*;

/**
 *
 * @author proger
 */
public class InterpretedTransformationFunctionTest {

    public InterpretedTransformationFunctionTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testTokenTransformation() {
        String source = "T['attr'] = 123;";
        InterpretedTransformationFunction itf = new InterpretedTransformationFunction(source);

        ENetworksModel model = new ENetworksModel();
        Aggregate aggregate = createAggregateInstance();
        Place p = new Place("P");
        model.setRootAggregate(aggregate);
        Transition t = new TTransition("T");
        aggregate.addTransition(t);
        t.setTransformationFunction(itf);

        Token token = new Token(); token.setValue("attr", new Value(0d));
        Token result = itf.transform(t, token);

        assertThat(result.getValue("attr").doubleValue(), equalTo(123d));

    }

}