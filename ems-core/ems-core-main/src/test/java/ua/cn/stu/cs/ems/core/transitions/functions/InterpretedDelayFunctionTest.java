package ua.cn.stu.cs.ems.core.transitions.functions;

import ua.cn.stu.cs.ems.core.aggregates.Aggregate;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ua.cn.stu.cs.ems.core.models.ENetworksModel;
import ua.cn.stu.cs.ems.core.transitions.TTransition;
import ua.cn.stu.cs.ems.ecli.Value;
import static org.junit.Assert.*;
import static ua.cn.stu.cs.ems.core.testutils.InstanceFactory.*;

/**
 *
 * @author proger
 */
public class InterpretedDelayFunctionTest {

    public InterpretedDelayFunctionTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testInterpretedDelay() {
        String source =
                  "VAR val = ROOT.V['varName'];"
                + "RETURN val + 5;";

        InterpretedDelayFunction idf = new InterpretedDelayFunction(source);

        ENetworksModel model = new ENetworksModel();
        Aggregate aggregate = createAggregateInstance();
        aggregate.setVariable("varName", new Value(10d));
        TTransition transition = new TTransition("T");

        transition.setDelayFunction(idf);
        aggregate.addTransition(transition);
        model.setRootAggregate(aggregate);

        double result = idf.getDelayTime(transition);
        assertEquals(15, result,0);
    }

}