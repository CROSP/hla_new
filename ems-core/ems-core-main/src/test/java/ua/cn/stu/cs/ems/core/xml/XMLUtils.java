package ua.cn.stu.cs.ems.core.xml;

import org.custommonkey.xmlunit.XMLTestCase;
import org.custommonkey.xmlunit.XMLUnit;
import org.jdom.Document;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;


/**
 *
 * @author proger
 */
class XMLUtils extends XMLTestCase  {
    void assertDOMToXML(Document dom, String expectedXml, boolean output) throws Exception {
        XMLOutputter outputter = new XMLOutputter();
        Format prettyFormat = Format.getPrettyFormat();
        outputter.setFormat(prettyFormat);
        String aggregateXML = outputter.outputString(dom);

        if (output) {
            System.out.println(aggregateXML);
        }
        XMLUnit.setIgnoreWhitespace(true);
        XMLUnit.setIgnoreDiffBetweenTextAndCDATA(true);
        XMLUnit.setIgnoreAttributeOrder(true);


        assertXMLEqual("Comparing jDOM output with expected XML",
                expectedXml, aggregateXML);
    }

}
