package ua.cn.stu.cs.ems.core.xml;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import ua.cn.stu.cs.ems.core.aggregates.AggregateDefinition;
import static org.junit.Assert.*;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.aggregates.AggregateInstance;
import ua.cn.stu.cs.ems.core.aggregates.AggregateRegistry;
import ua.cn.stu.cs.ems.core.queues.FIFOPriorityQueue;
import ua.cn.stu.cs.ems.core.queues.InterpretedTokenPriorityFunction;
import ua.cn.stu.cs.ems.core.transitions.XTransition;
import ua.cn.stu.cs.ems.core.transitions.functions.DefaultDelayFunction;
import ua.cn.stu.cs.ems.core.transitions.functions.DefaultPermitingFunction;
import ua.cn.stu.cs.ems.core.transitions.functions.DefaultTransformationFunction;
import ua.cn.stu.cs.ems.core.transitions.functions.InterpretedDelayFunction;
import ua.cn.stu.cs.ems.core.transitions.functions.InterpretedPermitingFunction;
import ua.cn.stu.cs.ems.core.transitions.functions.InterpretedTransformationFunction;
import ua.cn.stu.cs.ems.ecli.Value;

/**
 *
 * @author proger
 */
public class ModelReaderTest {

    public ModelReaderTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Test
    public void testReadModelWithChangedVariables() throws Exception {
        AggregateDefinition ad = new AggregateDefinition("root");
        ad.setVariable("var", new Value(1));

        AggregateRegistry registry = new AggregateRegistry();
        registry.addAggregateDefinition(ad);

        String xml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<model xmlns='http://www.cs.stu.cn.ua/jess/enetsdefinitions'>"
                + "<rootAggregate type='root'>"
                + "<variables><variable name='newVar' value='5.0'/></variables>"
                + "</rootAggregate></model>";

        AggregateInstance instance = readModel(xml, registry);

        assertEquals(1d, instance.getVariable("var").getValue().doubleValue(), 0.0001);
        assertEquals(5d, instance.getVariable("newVar").getValue().doubleValue(), 0.0001);
    }

    @Test
    public void testReadModelWithTransitionFunctionsChanged() throws Exception {
        AggregateDefinition ad = new AggregateDefinition("root");
        XTransition xt = new XTransition("XT");
        ad.addTransition(xt);

        AggregateRegistry registry = new AggregateRegistry();
        registry.addAggregateDefinition(ad);

        String xml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<model xmlns='http://www.cs.stu.cn.ua/jess/enetsdefinitions'>"
                + "<rootAggregate type='root'>"

                + "<transition name='XT'>"
                + "<transformationFunction type='ecli'><![CDATA[T['attr'] = 15;]]></transformationFunction>"
                + "<delayFunction type='ecli'><![CDATA[RETURN 1;]]></delayFunction>"
                + "<permitingFunction type='ecli'><![CDATA[RETURN 2;]]></permitingFunction>"
                + "</transition>"

                + "</rootAggregate></model>";

        AggregateInstance instance = readModel(xml, registry);

        XTransition x = (XTransition) instance.getTransition("XT");

        assertEquals(new InterpretedTransformationFunction("T['attr'] = 15;"), x.getTransformationFunction());
        assertEquals(new InterpretedDelayFunction("RETURN 1;"), x.getDelayFunction());
        assertEquals(new InterpretedPermitingFunction("RETURN 2;"), x.getPermitingFunction());
    }

    @Test
    public void testReadModelWithTransitionFunctionsChangedToDefault() throws Exception {
        AggregateDefinition ad = new AggregateDefinition("root");
        XTransition xt = new XTransition("XT");
        xt.setDelayFunction(new InterpretedDelayFunction("RETURN 1;"));
        xt.setTransformationFunction(new InterpretedTransformationFunction("T['attr'] = 15;"));
        xt.setPermitingFunction(new InterpretedPermitingFunction("RETURN 2;"));

        ad.addTransition(xt);

        AggregateRegistry registry = new AggregateRegistry();
        registry.addAggregateDefinition(ad);

        String xml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<model xmlns='http://www.cs.stu.cn.ua/jess/enetsdefinitions'>"
                + "<rootAggregate type='root'>"

                + "<transition name='XT'>"
                + "<transformationFunction type='default'/>"
                + "<delayFunction type='default'/>"
                + "<permitingFunction type='default'/>"
                + "</transition>"

                + "</rootAggregate></model>";

        AggregateInstance instance = readModel(xml, registry);

        XTransition x = (XTransition) instance.getTransition("XT");

        assertEquals(new DefaultTransformationFunction(), x.getTransformationFunction());
        assertEquals(new DefaultDelayFunction(), x.getDelayFunction());
        assertEquals(new DefaultPermitingFunction(), x.getPermitingFunction());
    }

    @Test
    public void testReadModelWithQueueFunctionChanged() throws Exception {
        AggregateDefinition ad = new AggregateDefinition("root");

        FIFOPriorityQueue fifop = new FIFOPriorityQueue("Q", new InterpretedTokenPriorityFunction("RETURN 100;"));
        ad.addQueue(fifop);

        AggregateRegistry registry = new AggregateRegistry();
        registry.addAggregateDefinition(ad);

        String xml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<model xmlns='http://www.cs.stu.cn.ua/jess/enetsdefinitions'>"
                + "<rootAggregate type='root'>"

                + "<queue name='Q'>"
                + "<priorityFunction type='ecli'>RETURN 1;</priorityFunction>"
                + "</queue>"

                + "</rootAggregate></model>";

        AggregateInstance instance = readModel(xml, registry);
        FIFOPriorityQueue q = (FIFOPriorityQueue) instance.getQueue("Q");
        assertEquals(new InterpretedTokenPriorityFunction("RETURN 1;"), q.getPriorityFunction());
    }

    @Test
    public void testReadModelWithChangeInChildAggregate() throws Exception {
        AggregateDefinition child = new AggregateDefinition("child");
        child.addQueue(new FIFOPriorityQueue("Q", new InterpretedTokenPriorityFunction("RETURN 5;")));

        AggregateDefinition parent = new AggregateDefinition("root");
        parent.addChildAggreagateDefinition(child, "A");

        AggregateRegistry registry = new AggregateRegistry();
        registry.addAggregateDefinition(parent);
        registry.addAggregateDefinition(child);

        String xml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<model xmlns='http://www.cs.stu.cn.ua/jess/enetsdefinitions'>"
                + "<rootAggregate type='root'>"

                + "<aggregate name='A'>"
                + "<queue name='Q'>"
                + "<priorityFunction type='ecli'>RETURN 1;</priorityFunction>"
                + "</queue>"
                + "</aggregate>"

                + "</rootAggregate></model>";

        AggregateInstance instance = readModel(xml, registry);
        AggregateInstance childInstance = (AggregateInstance) instance.getAggregate("A");
        FIFOPriorityQueue fifopInstance = (FIFOPriorityQueue) childInstance.getQueue("Q");

        assertEquals(new InterpretedTokenPriorityFunction("RETURN 1;"), fifopInstance.getPriorityFunction());
    }

    @Test
    public void testReadModelWithDifferentMarking() throws Exception {
        AggregateDefinition ad = new AggregateDefinition("root");
        ad.addPlace(new Place("P1"));

        Place p2 = new Place("P2");
        p2.setToken(new Token());
        ad.addPlace(p2);
        
        Place p3 = new Place("P3");
        Token t = new Token(); t.setValue("attr", new Value(1d));
        p3.setToken(t);
        ad.addPlace(p3);

        AggregateRegistry registry = new AggregateRegistry();
        registry.addAggregateDefinition(ad);


        String xml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<model xmlns='http://www.cs.stu.cn.ua/jess/enetsdefinitions'>"
                + "<rootAggregate type='root'>"

                + "<place name='P1'>"
                + "<token/>"
                + "</place>"

                + "<place name='P2'>"
                + "</place>"

                + "<place name='P3'>"
                + "<token>"
                + "<attribute name='attr' value='1'/>"
                + "</token>"
                + "</place>"

                + "</rootAggregate></model>";

        AggregateInstance instance = readModel(xml, registry);
        assertEquals(0, instance.getPlace("P1").getToken().getAttributeNames().size());
        assertEquals(null, instance.getPlace("P2").getToken());

        assertEquals(1, instance.getPlace("P3").getToken().getAttributeNames().size());
        assertEquals(1, instance.getPlace("P3").getToken().getValue("attr").doubleValue(), 0.00001);

    }

    private AggregateInstance readModel(String xml, AggregateRegistry registry) throws Exception {
        ModelReader reader = new ModelReader();
        InputStream is = new ByteArrayInputStream(xml.getBytes("UTF-8"));

        return reader.readModel(is, registry);
    }

}