package ua.cn.stu.cs.ems.core;

import org.junit.Ignore;
import org.jmock.Expectations;
import ua.cn.stu.cs.ems.core.models.Model;
import ua.cn.stu.cs.ems.core.aggregates.Aggregate;
import org.jmock.Mockery;
import java.util.List;
import org.jmock.integration.junit4.JMock;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.junit.Assert.*;
;import static org.hamcrest.CoreMatchers.*;;

/**
 *
 * @author proger
 */
@RunWith(JMock.class)
public class ActiveInstanceTest {

    Mockery mockery = new JUnit4Mockery();
    public ActiveInstanceTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testGetName() {
        AbstractActiveInstance instance = getAbstractActiveInstance();
        String name = "name";
        instance.changeName(name);

        assertThat(instance.getName(), equalTo(name));
    }

    @Test(expected=NullPointerException.class)
    public void testSetNullName() {
        AbstractActiveInstance instance = getAbstractActiveInstance();
        instance.changeName(null);
    }

    @Test
    public void testListenerCalledOnDelayStarted() {
        final ActiveInstanceListener listener = mockery.mock(ActiveInstanceListener.class);
        final Aggregate aggregate = mockery.mock(Aggregate.class);
        final Model model = mockery.mock(Model.class);

        final AbstractActiveInstance transition = getAbstractActiveInstance();
        transition.addListener(listener);
        transition.setParentAggregate(aggregate);

        mockery.checking(new Expectations() {{
            oneOf(listener).instanceDelayStarted(transition);
            allowing(aggregate).getModel(); will(returnValue(model));
            allowing(model).addDelayable(transition, 1); will(returnValue(true));
        }});

        transition.delayFor(1);
    }

    @Test
    public void testListenerCalledOnDelayFinished() {
        final ActiveInstanceListener listener = mockery.mock(ActiveInstanceListener.class);
        final Aggregate aggregate = mockery.mock(Aggregate.class);
        final Model model = mockery.mock(Model.class);

        final TestAbstractActiveInstance transition = getAbstractActiveInstance();
        transition.addListener(listener);
        transition.setParentAggregate(aggregate);

         mockery.checking(new Expectations() {{
            oneOf(listener).instanceDelayFinished(transition);
            allowing(aggregate).getModel(); will(returnValue(model));
            allowing(model).addDelayable(transition, 1); will(returnValue(true));
        }});

        transition.onDelayFinished();
        assertTrue(transition.isFired());
    }

    private TestAbstractActiveInstance getAbstractActiveInstance() {
        return new TestAbstractActiveInstance("name");
    }

}

@Ignore
class TestAbstractActiveInstance extends AbstractActiveInstance {

    public TestAbstractActiveInstance(String name) {
        super(name);
    }
    boolean fired = false;

    public List<Place> getInputPlaces() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public List<Place> getOutputPlaces() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void placeStateChanged(Place position) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void fireIfPosible() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    protected void fireTransition() {
        fired = true;
    }

    public boolean isFired() {
        return fired;
    }

  public void disconnectInputPlace(String name) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  public void disconnectOutputPlace(String name) {
    throw new UnsupportedOperationException("Not supported yet.");
  }
}
