package ua.cn.stu.cs.ems.core.queues;

import org.junit.Ignore;
import ua.cn.stu.cs.ems.core.Place;
import java.util.List;
import ua.cn.stu.cs.ems.core.Token;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.*;

/**
 *
 * @author proger
 */
@Ignore
class TestQueueOrder {
    /**
     * Method to test different queue's disciplines. According to the E nets theory
     * if there is a token in a queue, then output position is considered as not an
     * empty. So first added to queue's input place token will immediately appear
     * at the output place. So with LIFO queue and input (t1, t2, t3) we will receive
     * (t1, t3, t2)
     * @param queue - queue to test
     * @param inputOrder - defines token to input into queue
     * @param outputOrder - defines order of token on the output
     */
    public static void assertQueueOrder(Queue queue, List<Token> inputOrder, List<Token> outputOrder) {
        assertThat("Number of token to input in queue, should be equals to number" +
                   "of tokens that are expected to be read from queue's output",
                   inputOrder.size(), equalTo(outputOrder.size()));

        Queue q = connectPlacedToQueue(queue);
        Place input = q.getInputPlace();
        Place output = q.getOutputPlace();

        for (Token inputToken : inputOrder) {
            input.setToken(inputToken);
            q.onDelayFinished();
        }

        for (Token outputToken : outputOrder) {
            q.onDelayFinished();
            assertThat(output.getToken(), equalTo(outputToken));
            output.setToken(null);
        }

    }

    private static Queue connectPlacedToQueue(Queue queue) {
        Place input = new Place("input");
        Place output = new Place("output");

        queue.connectInputPlace(input);
        queue.connectOutputPlace(output);

        return queue;
    }

}
