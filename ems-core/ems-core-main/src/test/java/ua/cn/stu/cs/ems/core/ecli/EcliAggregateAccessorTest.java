package ua.cn.stu.cs.ems.core.ecli;

import ua.cn.stu.cs.ems.core.aggregates.AggregateInstance;
import ua.cn.stu.cs.ems.core.Token;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.aggregates.Aggregate;
import ua.cn.stu.cs.ems.ecli.ValType;
import ua.cn.stu.cs.ems.ecli.Value;
import static ua.cn.stu.cs.ems.core.testutils.InstanceFactory.*;
;import static org.hamcrest.CoreMatchers.*;;

/**
 *
 * @author proger
 */
public class EcliAggregateAccessorTest {

    public EcliAggregateAccessorTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Test
    public void testContainsPlace() {
        Aggregate aggregate = createAggregateInstance();
        Place place = new Place("P");
        aggregate.addPlace(place);

        EcliAggregateAccessor eaa = new EcliAggregateAccessor(aggregate);

        assertThat(eaa.containsPlace("P"), is(true));
        assertThat(eaa.containsPlace("nonExisting"), is(false));
    }

    @Test
    public void testIsPlaceMarked() {
        Aggregate aggregate = createAggregateInstance();
        Place marked = new Place("Marked"); marked.setToken(new Token());
        Place notMarked = new Place("notMarked");
        aggregate.addPlace(marked);
        aggregate.addPlace(notMarked);

        EcliAggregateAccessor eaa = new EcliAggregateAccessor(aggregate);

        assertThat(eaa.isPlaceMarked("Marked"), is(true));
        assertThat(eaa.isPlaceMarked("notMarked"), is(false));
    }

    @Test
    public void testContainsAggregate() {
        AggregateInstance aggregate = createAggregateInstance();
        aggregate.addChildAggregate(createAggregateInstanceWithName("child"));

        EcliAggregateAccessor eaa = new EcliAggregateAccessor(aggregate);

        assertThat(eaa.containsAggregate("child"), is(true));
        assertThat(eaa.containsAggregate("nonExistingOne"), is(false));
    }

    @Test
    public void testContainsVar() {
        Aggregate aggregate = createAggregateInstance();
        aggregate.setVariable("name", new Value(123));

        EcliAggregateAccessor eaa = new EcliAggregateAccessor(aggregate);

        assertThat(eaa.containsVar("name"), is(true));
        assertThat(eaa.containsVar("nonExistingOne"), is(false));
    }

    @Test
    public void testGetVariable() {
        Aggregate aggregate = createAggregateInstance();
        aggregate.setVariable("name", new Value(123));

        EcliAggregateAccessor eaa = new EcliAggregateAccessor(aggregate);

        assertEquals(new Double(123), ((Value)eaa.getVariable("name")).doubleValue());
    }

    @Test
    public void testSetVariable() {
        Aggregate aggregate = createAggregateInstance();
        aggregate.setVariable("old", new Value(123));

        EcliAggregateAccessor eaa = new EcliAggregateAccessor(aggregate);
        eaa.setVariable("old", new Value(new Double(1), ValType.REAL));
        eaa.setVariable("new", new Value(new Double(2), ValType.REAL));

        assertEquals(new Double(1), aggregate.getVariable("old").getValue().doubleValue(), 0.00001);
        assertEquals(new Double(2), aggregate.getVariable("new").getValue().doubleValue(), 0.00001);

    }

    @Test
    public void testGetTokenAt() {
        Aggregate aggregate = createAggregateInstance();
        Place child = new Place("child");
        Token token = new Token();
        child.setToken(token);
        aggregate.addPlace(child);

        EcliAggregateAccessor eaa = new EcliAggregateAccessor(aggregate);

        EcliTokenAccessor eta = eaa.getTokenAt("child");
        assertThat(eta.getToken(), equalTo(token));
    }

    @Test
    public void testGetAggregate() {
        AggregateInstance aggregate = new AggregateInstance("A");
        AggregateInstance child = new AggregateInstance("child");
        aggregate.addChildAggregate(child);

        EcliAggregateAccessor eaa = new EcliAggregateAccessor(aggregate);

        assertEquals(eaa.getAggregate("child").getBackEndAggregate(), child);
        assertThat(eaa.getAggregate("nonExisting"), nullValue());
    }

}