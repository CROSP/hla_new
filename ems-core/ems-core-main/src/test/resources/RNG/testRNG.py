#!/usr/bin/python3.1

# Simple regression test suite to test is RELAX NG scheme for validating 
# E nets XML file is correct. It requires installed jing utility that validates
# specified file according to RNG scheme

import os

def testWithJing(RNGFile, XMLPath):
    log("Testing " + XMLPath)
    res = os.system("jing -i " + RNGFile + " " + XMLPath)
    return res == 0

def log(str):
        print("[RNGTest] " + str)


log("Testing syntax of ENets RNG file")
assert testWithJing("enets.rng", "");

log("Testing transitions")
assert testWithJing("enets.rng", "transitionTests/ValidT.xml")
assert testWithJing("enets.rng", "transitionTests/TWithPermitingFunction.xml") == False
assert testWithJing("enets.rng", "transitionTests/ConstTransformationFunction.xml") == False
assert testWithJing("enets.rng", "transitionTests/WrongTransitionName.xml") == False

log("Testing variables")
assert testWithJing("enets.rng", "variableTests/correctVariables.xml")
assert testWithJing("enets.rng", "variableTests/nonDoubleVariable.xml") == False

log("Testing places")
assert testWithJing("enets.rng", "placeMarkingTests/validPlace.xml")
assert testWithJing("enets.rng", "placeMarkingTests/wrongAttributeDefinition.xml") == False
assert testWithJing("enets.rng", "placeMarkingTests/wrongAttributeValue.xml") == False

log("Testing queues")
assert testWithJing("enets.rng", "queueTests/validNonPriorityQueue.xml")
assert testWithJing("enets.rng", "queueTests/priorityQueueWithoutFunction.xml") == False
assert testWithJing("enets.rng", "queueTests/validPriorityQueueWithFunction.xml")

log("Testing arcs")
assert testWithJing("enets.rng", "arcsTests/validArc.xml")

log("Testing aggregates")
assert testWithJing("enets.rng", "aggregateTests/validAggregate.xml")
assert testWithJing("enets.rng", "aggregateTests/noAggregateTypeSpecified.xml") == False
assert testWithJing("enets.rng", "aggregateTests/wrongAggregateTypeName.xml") == False

log("Testing model definition")
assert testWithJing("modeldefinition.rng", "modelDefinitionTests/validModelDefinition.xml")

log("Testing experiments definition")
assert testWithJing("experiments.rng", "experimentsTest/validExperiment.xml")


print("ALL TESTS PASSED")


