<grammar ns="http://www.cs.stu.cn.ua/jess/experiments" 
         xmlns="http://relaxng.org/ns/structure/1.0" 
         xmlns:a="http://relaxng.org/ns/compatibility/annotations/1.0"
         datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes">
         
         <a:documentation>
                This RELAX NG scheme defines format for saving settings for 
                experiments. Experiments settings defines how long, how many times
                for which variables statistics will be collected.
         </a:documentation>
         
         <start>
                <ref name="experiments.definition" />
         </start>
         
         <a:documentation>
                Root element is just an enumeration of experiment configurations.
         </a:documentation>
         <define name="experiments.definition">
                <element name="experiments">
                        <zeroOrMore>
                                <ref name="experiment.definition"/>
                        </zeroOrMore>
                </element>
         </define>         
         
         <a:documentation>
                Experiment description consist of type, name, list of variables 
                which values will be changed during experiment run and experiment
                type specific parameters.
         </a:documentation>
         <define name="experiment.definition">
                <element name="experiment">
                        <attribute name="type"/>
                        <ref name="experiment.name"/>
                        <ref name="variables.definition"/>
                        <ref name="experiment.parameters"/>                
                </element>
         </define>
         
         <a:documentation>
                Unique name for the experiment.
         </a:documentation>
         <define name="experiment.name">
                <element name="name">
                        <text/>
                </element>
         </define>
         
         <a:documentation>
                List of variables which values will be set during experiment run.
         </a:documentation>
         <define name="variables.definition">
                <element name="variables">
                        <oneOrMore>
                                <ref name="variable.definition"/>
                        </oneOrMore>
                </element>
         </define>
         
         <a:documentation>
                Definition of variable is full path to aggregate where this variable
                can be found, variable's name and type of the values generator.
                Full path is just concateneted names of all aggregates from the 
                root aggregate to the aggregate with the variable, separated by a '.'.
         </a:documentation>
         <define name="variable.definition">
                <element name="variable">
                        <element name="agregate">
                                <text/>
                        </element>
                        <element name="name">
                                <text/>
                        </element>
                
                        <ref name="generator.definition" />
                </element>
         </define>
         
         <a:documentation>
                Definition of generator of values for a specific variable.
         </a:documentation>
         <define name="generator.definition">
                <element name="generator">
                        <attribute name="type"/>
                        <ref name="parameters.definition" />
                </element>
         </define>
         
         <a:documentation>
                Experiment type's specific parameters.
         </a:documentation>
         <define name="experiment.parameters">
                <element name="parameters">
                        <ref name="parameters.definition"/>
                </element>
         </define>
         
         <define name="parameters.definition">
                <oneOrMore>
                        <element name="parameter">
                                <attribute name="name"/>
                                <attribute name="value"/>
                        </element>
                </oneOrMore>
         </define>
        
         
</grammar>
