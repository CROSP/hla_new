package ua.cn.stu.cs.ems.core.transitions.functions;

import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.transitions.Transition;

/**
 * TransformationFunction - interface for functions that transfrom token when.
 * it passes through transition.
 */
public interface TransformationFunction {

    /**
     * Apply token transformation.
     * @param transition - transition that transforming a token
     * @param token - token that pass through transition
     * @return Token - transformation result.
     */
    Token transform(Transition transition, Token token);


    /**
     * Create a copy of this transformation function.
     * @return cope of this transformation function
     */
    TransformationFunction copy();
}
