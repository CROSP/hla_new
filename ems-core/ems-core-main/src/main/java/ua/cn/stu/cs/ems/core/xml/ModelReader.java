package ua.cn.stu.cs.ems.core.xml;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ListIterator;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.Namespace;
import org.jdom.input.SAXBuilder;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.aggregates.AggregateDefinition;
import ua.cn.stu.cs.ems.core.aggregates.AggregateInstance;
import ua.cn.stu.cs.ems.core.aggregates.AggregateRegistry;
import ua.cn.stu.cs.ems.core.queues.AbstractPriorityQueue;
import ua.cn.stu.cs.ems.core.queues.InterpretedTokenPriorityFunction;
import ua.cn.stu.cs.ems.core.transitions.SpecialTransition;
import ua.cn.stu.cs.ems.core.transitions.Transition;
import ua.cn.stu.cs.ems.core.transitions.functions.DefaultDelayFunction;
import ua.cn.stu.cs.ems.core.transitions.functions.DefaultPermitingFunction;
import ua.cn.stu.cs.ems.core.transitions.functions.DefaultTransformationFunction;
import ua.cn.stu.cs.ems.core.transitions.functions.InterpretedDelayFunction;
import ua.cn.stu.cs.ems.core.transitions.functions.InterpretedPermitingFunction;
import ua.cn.stu.cs.ems.core.transitions.functions.InterpretedTransformationFunction;
import ua.cn.stu.cs.ems.ecli.Value;

/**
 * ModelReader reads model from a file. Model is difference in functions/marking/variables
 * between aggregate definition and aggregate instance.
 * @author proger
 */
public class ModelReader {

    private static final Namespace NS = Namespace.getNamespace("http://www.cs.stu.cn.ua/jess/enetsdefinitions");

    /**
     * Read model from a file
     * @param file - file to read from
     * @param registry - aggregate definition registry to read aggregate structures from
     * @return root aggregate instance for a model
     * @throws JDOMException
     * @throws IOException
     * @throws PNMLParsingException
     */
    public AggregateInstance readModel(File file, AggregateRegistry registry) throws JDOMException, IOException, PNMLParsingException {
        return readModel(new FileInputStream(file), registry);
    }

    AggregateInstance readModel(InputStream is, AggregateRegistry registry) throws JDOMException, IOException, PNMLParsingException {
        SAXBuilder saxBuilder = new SAXBuilder();
        Document document = saxBuilder.build(is);

        Element modelElement = document.getRootElement();
        Element rootAggregateElement = modelElement.getChild("rootAggregate", NS);
        AggregateInstance rootAggregate = createModel(rootAggregateElement, registry);

        fillAggregate(rootAggregate, rootAggregateElement);

        return rootAggregate;
    }

    private AggregateInstance createModel(Element rootAggregateElement, AggregateRegistry registry) {
        String rootName = rootAggregateElement.getAttributeValue("type");
        AggregateDefinition ad = registry.getAggregateDefinition(rootName);

        return ad.createInstance(rootName);
    }

    private void fillAggregate(AggregateInstance aggregate, Element aggregateElement) throws PNMLParsingException {
        fillVariables(aggregate, aggregateElement);
        fillTransitions(aggregate, aggregateElement);
        fillQueues(aggregate, aggregateElement);
        fillMarking(aggregate, aggregateElement);

        ListIterator<Element> aggregatesIter = aggregateElement.getChildren("aggregate", NS).listIterator();

        while (aggregatesIter.hasNext()) {
            Element childAggregateElement = aggregatesIter.next();
            String childName = childAggregateElement.getAttributeValue("name");
            AggregateInstance child = (AggregateInstance) aggregate.getAggregate(childName);

            fillAggregate(child, childAggregateElement);

        }

    }

    private void fillVariables(AggregateInstance aggregate, Element aggregateElement) {
        Element variablesElement = aggregateElement.getChild("variables", NS);

        if (variablesElement != null) {
            ListIterator<Element> varsIter = variablesElement.getChildren().listIterator();

            while (varsIter.hasNext()) {
                Element variableElement = varsIter.next();

                String varName = variableElement.getAttributeValue("name");
                String varValue = variableElement.getAttributeValue("value");

                aggregate.setVariable(varName, new Value(varValue));
            }
        }
    }

    private void fillTransitions(AggregateInstance aggregate, Element aggregateElement) throws PNMLParsingException {
         ListIterator<Element> transitionsIter = aggregateElement.getChildren("transition", NS).listIterator();

         while (transitionsIter.hasNext()) {
             Element transitionElement = transitionsIter.next();
             String transitionName = transitionElement.getAttributeValue("name");
             Transition transition = aggregate.getTransition(transitionName);

             Element transformationFunctionElement = transitionElement.getChild("transformationFunction", NS);
             Element delayFunctionElement = transitionElement.getChild("delayFunction", NS);

             if (transformationFunctionElement != null) {
                 String type = transformationFunctionElement.getAttributeValue("type");

                 if (type.equals("ecli")) {
                    transition.setTransformationFunction(new InterpretedTransformationFunction(transformationFunctionElement.getText()));
                 }
                 else if (type.equals("default")) {
                     transition.setTransformationFunction(new DefaultTransformationFunction());
                 }
                 else {
                     throw new PNMLParsingException("Unknown function type " + type);
                 }
             }

             if (delayFunctionElement != null) {
                 String type = delayFunctionElement.getAttributeValue("type");

                 if (type.equals("ecli")) {
                     transition.setDelayFunction(new InterpretedDelayFunction(delayFunctionElement.getText()));
                 }
                 else if (type.equals("default")) {
                     transition.setDelayFunction(new DefaultDelayFunction());
                 }
                 else {
                     throw new PNMLParsingException("Unknown function type " + type);
                 }
             }

             if (transition instanceof SpecialTransition) {
                 SpecialTransition specialTransition = (SpecialTransition) transition;

                 Element permitingFunctionElement = transitionElement.getChild("permitingFunction", NS);

                 if (permitingFunctionElement != null) {
                     String type = permitingFunctionElement.getAttributeValue("type");

                     if (type.equals("ecli")) {
                         specialTransition.setPermitingFunction(new InterpretedPermitingFunction(permitingFunctionElement.getText()));
                     }
                     else if (type.equals("default")) {
                         specialTransition.setPermitingFunction(new DefaultPermitingFunction());
                     }
                     else {
                         throw new PNMLParsingException("Unknown function type " + type);
                     }
                 }
             }
         }
    }

    private void fillQueues(AggregateInstance aggregate, Element aggregateElement) {
        ListIterator<Element> queuesIter = aggregateElement.getChildren("queue", NS).listIterator();

        while (queuesIter.hasNext()) {
            Element queueElement = queuesIter.next();
            String queueName = queueElement.getAttributeValue("name");
            AbstractPriorityQueue priorityQueue = (AbstractPriorityQueue) aggregate.getQueue(queueName);

            Element priorityQueueElement = queueElement.getChild("priorityFunction", NS);
            priorityQueue.setPriorityFunction(new InterpretedTokenPriorityFunction(priorityQueueElement.getText()));
        }
    }

    private void fillMarking(AggregateInstance aggregate, Element aggregateElement) {
        ListIterator<Element> placeIter = aggregateElement.getChildren("place", NS).listIterator();

        while (placeIter.hasNext()) {
            Element placeElement = placeIter.next();
            String placeName =  placeElement.getAttributeValue("name");

            Place place = aggregate.getPlace(placeName);

            Element tokenElement = placeElement.getChild("token", NS);

            if (tokenElement != null) {
                Token newToken = new Token();
                ListIterator<Element> attributesIter = tokenElement.getChildren().listIterator();

                while (attributesIter.hasNext()) {
                    Element attributeElement = attributesIter.next();
                    String attributeName = attributeElement.getAttributeValue("name");
                    String attributeValue = attributeElement.getAttributeValue("value");

                    newToken.setValue(attributeName, new Value(attributeValue));
                }

                place.initToken(newToken);
            }
            else {
                place.initToken(null);
            }
        }
    }
}
