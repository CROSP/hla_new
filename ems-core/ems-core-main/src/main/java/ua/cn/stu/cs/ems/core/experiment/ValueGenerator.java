package ua.cn.stu.cs.ems.core.experiment;

/**
 * Interface for generating value for a factor.
 * @author proger
 */
interface ValueGenerator {
    /**
     * Is ValueGenerator has more values to generate.
     * @return 
     */
    boolean hasMore();

    /**
     * Get next value.
     * @return 
     */
    double nextValue();
}
