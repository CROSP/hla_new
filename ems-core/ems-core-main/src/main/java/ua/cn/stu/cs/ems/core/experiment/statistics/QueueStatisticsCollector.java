package ua.cn.stu.cs.ems.core.experiment.statistics;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.experiment.PrimaryStatisticsElement;
import ua.cn.stu.cs.ems.core.models.Model;
import ua.cn.stu.cs.ems.core.queues.Queue;
import ua.cn.stu.cs.ems.core.queues.QueueStateListener;
import static ua.cn.stu.cs.ems.core.utils.AssertUtils.assertNotNull;
import ua.cn.stu.cs.ems.core.utils.JessUtils;

/**
 *
 * Statistic collector for queues. It collectsStat following statistics
 * parameters: <ul> <li> Average queue length = (Int queue_length dt) / T; where
 * T - length of time duration when primary statistics was collected, Int -
 * integral, queue_length - length of a queue, dt - defines variable of
 * integration, t is model time. <li> Number of passed tokens = number of tokens
 * that left a queue <li> Average time in the queue = average queue length /
 * number of passed tokens </ul>
 *
 * Because queue length is a step function, integral can be calculate with a
 * following formula: sum( Li * Ti) / T; where sum - mathematical sign of
 * summation T - length of statistics collection time interval; Li - length of a
 * queue in i-th time interval during which queue length was constant; Ti -
 * duration of i-th time interval
 *
 * @author proger
 */
public class QueueStatisticsCollector extends StatisticsCollector implements QueueStateListener {
    // A queue for which primary statistics is collected

    private Queue queue;
    // Accumulated queue length. This should be divided by statistics collection time (see average queue lengthi)
    // in JavaDoc
    private double accumulatedQueueLength;
    // Total number of tokens that were removed from the queue
    private double totalNumberOfRemovedTokens;
    // Time when last adding/removing operation was permitet on the queue
    private double lastQueueChangeTime;
    // Time when primary statistics collection started
    private double collectionStartTime;
    final static Logger logger = LoggerFactory.getLogger(QueueStatisticsCollector.class);

    public QueueStatisticsCollector(Model model, Queue queue) {
        super(model);
        assertNotNull(queue, "Can't collect statistics on a null queue");

        this.queue = queue;
        queue.addQueueStateListener(this);
    }

    public List<StatisticsParameters> getStatisticsParameters() {
        return new ArrayList<StatisticsParameters>() {

            {
                add(StatisticsParameters.AVERAGE_QUEUE_LENGTH);
                add(StatisticsParameters.AVERAGE_TIME_IN_QUEUE);
                add(StatisticsParameters.NUMBER_OF_PASSED_TOKENS);
            }
        };
    }

    @Override
    public final void startStatCollection() {
        if (!collectsStat) {
            collectsStat = true;
            reset();
        }
    }

    @Override
    public final void stopStatCollection() {
        if (collectsStat) {
            updateAccumulatedQueueLength(queue.length());
            collectsStat = false;
            collectionFinishTime = model.getModelingTime();
            logger.info("Current statistics for queue {}: {}", JessUtils.getAggregateChildFullName(queue), getCurrentResult());
        }
    }

    @Override
    public final StatisticsResult getResult() {
        if (collectsStat) {
            updateAccumulatedQueueLength(queue.length());
        }

        StatisticsResult sr = new StatisticsResult();
        //long totalMonitoredTime = getFinishTime() - collectionStartTime;
        double totalMonitoredTime = model.getFinishTime() - collectionStartTime;
        double averageQueueLength = accumulatedQueueLength / totalMonitoredTime;
        sr.setResultValue(StatisticsParameters.AVERAGE_QUEUE_LENGTH, averageQueueLength);
        sr.setResultValue(StatisticsParameters.NUMBER_OF_PASSED_TOKENS, (double) totalNumberOfRemovedTokens);
        sr.setResultValue(StatisticsParameters.AVERAGE_TIME_IN_QUEUE, averageQueueLength / totalNumberOfRemovedTokens);

        return sr;
    }

    private StatisticsResult getCurrentResult() {
        StatisticsResult sr = new StatisticsResult();
        double totalMonitoredTime = model.getFinishTime() - collectionStartTime;
        double averageQueueLength = accumulatedQueueLength / totalMonitoredTime;
        sr.setResultValue(StatisticsParameters.AVERAGE_QUEUE_LENGTH, averageQueueLength);
        sr.setResultValue(StatisticsParameters.NUMBER_OF_PASSED_TOKENS, (double) totalNumberOfRemovedTokens);
        sr.setResultValue(StatisticsParameters.AVERAGE_TIME_IN_QUEUE, averageQueueLength / totalNumberOfRemovedTokens);

        return sr;
    }

    @Override
    public final void reset() {
        collectionStartTime = model.getModelingTime();
        lastQueueChangeTime = model.getModelingTime();
        accumulatedQueueLength = 0;
        totalNumberOfRemovedTokens = 0;
    }

    public final void tokenAddedToQueue(Queue queue, Token token) {
        if (collectsStat) {
            updateAccumulatedQueueLength(queue.length() - 1);
        }
        if (statisticsResults != null) {
            StatisticsResult sr = getResult();
            PrimaryStatisticsElement psr = new PrimaryStatisticsElement(model.getModelingTime(), sr);
            statisticsResults.get(this.queue).add(psr);
        }
        logger.info("Current statistics for queue {}: {}", JessUtils.getAggregateChildFullName(queue), getCurrentResult());
    }

    public final void tokenRemovedFromQueue(Queue queue, Token token) {
        if (collectsStat && token != null) {
            updateAccumulatedQueueLength(queue.length() + 1);
            totalNumberOfRemovedTokens++;
        }
        if (statisticsResults != null) {
            StatisticsResult sr = getResult();
            PrimaryStatisticsElement psr = new PrimaryStatisticsElement(model.getModelingTime(), sr);
            statisticsResults.get(this.queue).add(psr);
        }
        logger.info("Current statistics for queue {}: {}", JessUtils.getAggregateChildFullName(queue), getCurrentResult());
    }

    private void updateAccumulatedQueueLength(long queueLength) {
        accumulatedQueueLength += queueLength * (model.getModelingTime() - lastQueueChangeTime);
        lastQueueChangeTime = model.getModelingTime();
    }
}
