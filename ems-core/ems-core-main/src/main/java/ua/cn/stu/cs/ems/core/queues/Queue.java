package ua.cn.stu.cs.ems.core.queues;

import java.util.List;
import ua.cn.stu.cs.ems.core.ActiveInstance;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.Token;

/**
 * Interface for queues in E networks. According to E Nets modeling
 * <li> If there is  * a token at queue's input place this token is automaticaly consumed
 * <li> If there is any token in the queue, output place is considered as not empty. First token ready to leave 
 *      the queue will fill it.
 * @author proger
 */
public interface Queue extends ActiveInstance {
    /**
     * Connect place to queue's input.
     * @param input - place to connect. null - disconnect previously connected place
     */
    void connectInputPlace(Place input);

    /**
     * Get place connected to queue's input
     * @return - input place
     */
    Place getInputPlace();

    /**
     * Connect place to queue's output;
     * @param place - place to connect. null - disconnect previously connected place
     */
    void connectOutputPlace(Place place);

    /**
     * Get place connected to queue's output
     * @return - output place
     */
    Place getOutputPlace();

    /**
     * Get tokens that are now in the queue
     * @return list of tokens in the queue
     */
    List<Token> getTokensInQueue();

    /**
     * Add state listener to a queue.
     * @param listener - listener to add. Can't be null.
     */
    void addQueueStateListener(QueueStateListener listener);

    /**
     * Remover state listener from a queue.
     * @param listener - listener to remove. Can't be null.
     */
    void removeQueueStateListener(QueueStateListener listener);

    void clear();

    int length();

}
