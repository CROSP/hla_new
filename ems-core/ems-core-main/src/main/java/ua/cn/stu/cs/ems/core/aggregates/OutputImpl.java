package ua.cn.stu.cs.ems.core.aggregates;

import java.util.ArrayList;
import java.util.List;
import ua.cn.stu.cs.ems.core.AbstractActiveInstance;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.Token;
import static ua.cn.stu.cs.ems.core.utils.AssertUtils.*;

/**
 * Class Output - class that represent output of an aggregate in Petri nets. An
 * output can be connected to several inputs or to a single place. If an output
 * is connected to several inputs it use ConflictResolutionStrategy instance to
 * select where it will set tokens.
 *
 * @see Aggregate
 * @see Input
 * @see ConflictResolvingStrategy
 */
public class OutputImpl extends AbstractActiveInstance implements Output {

    private Place inputPlace;
    private Place outputPlace;
    private List<Input> connectedInputs = new ArrayList<Input>();
    private ConflictResolutionStrategy conflictResolvingStrategy;

    public OutputImpl(String name) {
        super(name);
        conflictResolvingStrategy = new DefaultConflictResolvingStrategy();
    }

    /**
     * Add input to an output
     *
     * @param input - input to add
     * @param pos - number in list of connected inputs
     */
    public void addInput(Input input, int pos) {
        assertNotNull(input, "Connected input can't be null");
        assertBounds(pos, 0, connectedInputs.size());

        if (outputPlace != null) {
            throw new IncorrectOutputConnection("It's imposible to connect both inputs and places to a single output");
        }

        connectedInputs.add(pos, input);
        input.connectOutput(this);
    }

    public void disconnectInput(int i) {
        assertBounds(i, 0, connectedInputs.size() - 1);

        Input input = connectedInputs.get(i);
        input.connectOutput(null);
        connectedInputs.remove(i);
    }

    /**
     * Replace input that connected to an output.
     *
     * @param input - input that will replace old input;
     * @param pos - number of input to be replaced
     */
    public void replaceInput(Input input, int pos) {
        assertNotNull(input, "New input can't be null");
        assertBounds(pos, 0, connectedInputs.size() - 1);

        connectedInputs.set(pos, input);
    }

    /**
     * Connect input place to an output.
     *
     * @param place - place to connect. null - means that output has no
     * connected input place
     */
    public void connectInputPlace(Place place) {
        if (inputPlace != null) {
            inputPlace.setOutputActiveInstance(null);
        }

        inputPlace = place;

        if (place != null) {
            place.setOutputActiveInstance(this);
        }
    }

    /**
     * Connect output place to an output.
     *
     * @param place - place to connect. null - means that output has no
     * connected output place
     */
    public void connectOutputPlace(Place place) {
        if (outputPlace != null) {
            outputPlace.setInputActiveInstance(null);
        }

        outputPlace = place;

        if (place != null) {
            place.setInputActiveInstance(this);
        }

    }

    public List<Place> getInputPlaces() {
        return createListWithSinglePlace(inputPlace);
    }

    public List<Place> getOutputPlaces() {
        return createListWithSinglePlace(outputPlace);
    }

    public void placeStateChanged(Place position) {
        fireIfPosible();
    }

    public void fireIfPosible() {
        if (inputPlace == null) {
            return;
        }
        if (inputPlace.isEmpty()) {
            return;
        }

        delayFor(0);
    }

    protected void fireTransition() {
        Token inputToken = inputPlace.getToken();
        if (inputToken != null) {
            boolean isOutputFired = false;
            if (outputPlace != null) {
                if (outputPlace.isEmpty()) {
                    outputPlace.setToken(inputToken);
                    isOutputFired = true;
                }
            } else {
                List<Input> inputs = getConflictResolutionStrategy().getInputsToSendToken(this);

                for (Input input : inputs) {
                    input.setToken(inputToken);
                    isOutputFired = true;
                }
            }
            if (isOutputFired == true) {
                inputPlace.setToken(null);
            }
        }
    }

    /**
     * Get inputs connected to an output
     *
     * @return list of connected inputs.
     */
    public List<Input> getConnectedInputs() {
        List<Input> result = new ArrayList<Input>(connectedInputs);

        return result;
    }

    /**
     * Set conflict resolve strategy
     *
     * @param strategy - conflict resolve strategy
     */
    public void setConflictResolutionStrategy(ConflictResolutionStrategy strategy) {
        assertNotNull(strategy, "Conflict resolution strategy can't be null");

        conflictResolvingStrategy = strategy;
    }

    /**
     * Get conflict resolve strategy
     *
     * @return - conflict resolve strategy
     */
    public ConflictResolutionStrategy getConflictResolutionStrategy() {
        return conflictResolvingStrategy;
    }

    public void inputCanBeSet(Input input) {
        fireIfPosible();
    }

    public void disconnectInputPlace(String name) {
        assertNotNull(inputPlace, "No input place is connected");
        assertTrue(inputPlace.getName().equals(name),
                "Imposible to disconnect output place with name '" + name + "'. Input place's name is '" + inputPlace.getName() + "'");

        inputPlace = null;
    }

    public void disconnectOutputPlace(String name) {
        assertNotNull(outputPlace, "No output place is connected");
        assertTrue(outputPlace.getName().equals(name),
                "Imposible to disconnect output place with name '" + name + "'. Output place's name is '" + outputPlace.getName() + "'");

        outputPlace = null;
    }

    public Place getInputPlace() {
        return inputPlace;
    }

    public Place getOutputPlace() {
        return outputPlace;
    }
}

class DefaultConflictResolvingStrategy implements ConflictResolutionStrategy {

    public DefaultConflictResolvingStrategy() {
    }

    public List<Input> getInputsToSendToken(Output output) {
        List<Input> result = new ArrayList<Input>();

        for (Input input : output.getConnectedInputs()) {
            if (input.canSetToken()) {
                result.add(input);
            }
        }

        return result;
    }
}
