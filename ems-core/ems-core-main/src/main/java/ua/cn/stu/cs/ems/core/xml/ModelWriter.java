package ua.cn.stu.cs.ems.core.xml;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Namespace;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.aggregates.Aggregate;
import ua.cn.stu.cs.ems.core.aggregates.AggregateDefinition;
import ua.cn.stu.cs.ems.core.aggregates.AggregateInstance;
import ua.cn.stu.cs.ems.core.queues.AbstractPriorityQueue;
import ua.cn.stu.cs.ems.core.queues.InterpretedTokenPriorityFunction;
import ua.cn.stu.cs.ems.core.queues.Queue;
import ua.cn.stu.cs.ems.core.transitions.SpecialTransition;
import ua.cn.stu.cs.ems.core.transitions.Transition;
import ua.cn.stu.cs.ems.core.transitions.functions.DefaultDelayFunction;
import ua.cn.stu.cs.ems.core.transitions.functions.DefaultPermitingFunction;
import ua.cn.stu.cs.ems.core.transitions.functions.DefaultTransformationFunction;
import ua.cn.stu.cs.ems.core.transitions.functions.DelayFunction;
import ua.cn.stu.cs.ems.core.transitions.functions.InterpretedDelayFunction;
import ua.cn.stu.cs.ems.core.transitions.functions.InterpretedPermitingFunction;
import ua.cn.stu.cs.ems.core.transitions.functions.InterpretedTransformationFunction;
import ua.cn.stu.cs.ems.core.transitions.functions.PermitingFunction;
import ua.cn.stu.cs.ems.core.transitions.functions.TransformationFunction;
import ua.cn.stu.cs.ems.ecli.Value;

/**
 * ModelWriter - writes model to a file. Model is changes of functions/marking/variables
 * compare to aggregate definition which is just a structure.
 * @author proger
 */
public class ModelWriter {

    public static final String modelDefinitionNS = "http://www.cs.stu.cn.ua/jess/enetsdefinitions";
    private static final Namespace NS = Namespace.getNamespace(modelDefinitionNS);

    /**
     * Write model to a file
     * @param file - file to read model to
     * @param instance - root aggregate of the model
     * @throws FileNotFoundException
     * @throws IOException
     */
    public void writeModel(File file, AggregateInstance instance) throws FileNotFoundException, IOException {
        FileOutputStream fos = new FileOutputStream(file);
        Document dom = getModelDOM(instance);

        XMLOutputter outputter = new XMLOutputter(Format.getPrettyFormat());
        outputter.output(dom, fos);
    }

    /**
     * Create jDOM tree of a model
     * @param instance - root aggregate of a model
     * @return jDOM tree of a model
     */
    Document getModelDOM(AggregateInstance instance) {        
        Document document = new Document();
        Element modelElement = new Element("model", NS);
        document.setRootElement(modelElement);

        Element rootAggregateElement = new Element("rootAggregate", NS);
        rootAggregateElement.setAttribute("type", instance.getAggregateDefinition().getName());

        fillModelDifferences(rootAggregateElement, instance);
        modelElement.addContent(rootAggregateElement);

        return document;
    }

    /**
     * Find and add DOM tree differences between model and definition
     * @param aggregateElement - DOM element for adding differences
     * @param instance - model's aggregate
     * @return true is differences were found, false otherwise.
     */
    private boolean fillModelDifferences(Element aggregateElement, AggregateInstance instance) {
        boolean changed = false;
        changed |= writeVariables(instance, aggregateElement);
        changed |= writeTransitions(instance, aggregateElement);
        changed |= writeQueues(instance, aggregateElement);
        changed |= writePlaces(instance, aggregateElement);

        for (Aggregate child : instance.getChildAggregates()) {
            Element childAggregateElement = new Element("aggregate", NS);
            childAggregateElement.setAttribute("name", child.getName());

            if (fillModelDifferences(childAggregateElement, (AggregateInstance) child)) {
                aggregateElement.addContent(childAggregateElement);
                changed = true;
            }

        }

        return changed;
    }

    /**
     * Add to DOM tree info about new variables in the model's aggregate
     * @param instance - aggregate from model
     * @param aggregateElement - DOM element to add differences
     * @return true is new variables were added, false otherwise
     */
    private boolean writeVariables(AggregateInstance instance, Element aggregateElement) {
        AggregateDefinition definition = instance.getAggregateDefinition();
        Element variablesElement = null;
        boolean changed = false;
        
        for (String varName : instance.getVariablesNames()) {
            if (definition.getVariable(varName) == null) {
                if (variablesElement == null) {
                    variablesElement = new Element("variables", NS);
                }
                Element variableElement = new Element("variable", NS);
                variableElement.setAttribute("name", varName);
                Value varValue = instance.getVariable(varName).getValue();
                variableElement.setAttribute("value", varValue.toString());
                variablesElement.addContent(variableElement);

                changed = true;
            }
        }
        if (variablesElement != null) {
            aggregateElement.addContent(variablesElement);
        }

        return changed;
    }

    /**
     * Add to DOM info about changed functions on transition
     * @param instance - model's aggregate
     * @param aggregateElement - DOM element to add diffrences to
     * @return true if any function was changed, false otherwise
     */
    private boolean writeTransitions(AggregateInstance instance, Element aggregateElement) {
        AggregateDefinition definition = instance.getAggregateDefinition();
        boolean changed = false;

        for (Transition definitionTransition : definition.getTransitions()) {
            Element transitionElement = new Element("transition", NS);
            boolean transitionChange = false;

            Transition instanceTransition = instance.getTransition(definitionTransition.getName());

            DelayFunction definitionDelayFunction = definitionTransition.getDelayFunction();
            DelayFunction instanceDelayFunction = instanceTransition.getDelayFunction();

            if (! definitionDelayFunction.equals(instanceDelayFunction)) {
                Element delayFunctionElement = new Element("delayFunction", NS);
                transitionChange = true;

                if (instanceDelayFunction instanceof DefaultDelayFunction) {
                    delayFunctionElement.setAttribute("type", "default");
                }
                else {
                    InterpretedDelayFunction idf = (InterpretedDelayFunction) instanceDelayFunction;

                    delayFunctionElement.setAttribute("type", "ecli");
                    delayFunctionElement.setText(idf.getSource());
                }

                transitionElement.addContent(delayFunctionElement);
                changed = true;
            }

            TransformationFunction definitionTransitionFunction = definitionTransition.getTransformationFunction();
            TransformationFunction instanceTransformationFunction = instanceTransition.getTransformationFunction();

            if (! definitionTransitionFunction.equals(instanceTransformationFunction)) {
                Element transformationFunctionElement = new Element("transformationFunction", NS);
                transitionChange = true;

                if (instanceTransformationFunction instanceof DefaultTransformationFunction) {
                    transformationFunctionElement.setAttribute("type", "default");
                }
                else {
                    InterpretedTransformationFunction idf = (InterpretedTransformationFunction) instanceTransformationFunction;

                    transformationFunctionElement.setAttribute("type", "ecli");
                    transformationFunctionElement.setText(idf.getSource());
                }

                transitionElement.addContent(transformationFunctionElement);
                changed = true;
            }

            if (definitionTransition instanceof SpecialTransition) {
                SpecialTransition specialDefinitionTransition = (SpecialTransition) definitionTransition;
                SpecialTransition specialInstanceTransition = (SpecialTransition) instanceTransition;

                PermitingFunction definitionPermitingFunction = specialDefinitionTransition.getPermitingFunction();
                PermitingFunction instancePermitingFunction = specialInstanceTransition.getPermitingFunction();

                if (! definitionPermitingFunction.equals(instancePermitingFunction)) {
                    Element permitingFunctionElement = new Element("permitingFunction", NS);
                    transitionChange = true;

                    if (instancePermitingFunction instanceof DefaultPermitingFunction) {
                        permitingFunctionElement.setAttribute("type", "default");
                    }
                    else {
                        InterpretedPermitingFunction ipf = (InterpretedPermitingFunction) instancePermitingFunction;
                        permitingFunctionElement.setAttribute("type", "ecli");
                        permitingFunctionElement.setText(ipf.getSource());
                    }

                    transitionElement.addContent(permitingFunctionElement);
                    changed = true;
                }
            }


            if (transitionChange) {
                transitionElement.setAttribute("name", definitionTransition.getName());
                aggregateElement.addContent(transitionElement);
            }
        }
        return changed;
    }

    /**
     * Add info about changed functions on priority queues.
     * @param instance - model's aggregate
     * @param aggregateElement - DOM element to add info about changed functions
     * @return true if any function was changed
     */
    private boolean writeQueues(AggregateInstance instance, Element aggregateElement) {
        AggregateDefinition definition = instance.getAggregateDefinition();
        boolean changed = false;

        for (Queue definitionQueue : definition.getQueues()) {
            if (definitionQueue instanceof AbstractPriorityQueue) {
                AbstractPriorityQueue priorityDefinitionQueue = (AbstractPriorityQueue) definitionQueue;
                AbstractPriorityQueue priorityInstaceQueue = (AbstractPriorityQueue) instance.getQueue(definitionQueue.getName());

                InterpretedTokenPriorityFunction instanceTPF = (InterpretedTokenPriorityFunction) priorityInstaceQueue.getPriorityFunction();
                InterpretedTokenPriorityFunction definitionTPF = (InterpretedTokenPriorityFunction) priorityDefinitionQueue.getPriorityFunction();

                if (! instanceTPF.equals(definitionTPF)) {
                    Element queueElement = new Element("queue", NS);
                    queueElement.setAttribute("name", definitionQueue.getName());
                    Element functionElement = new Element("priorityFunction", NS);

                    functionElement.setAttribute("type", "ecli");
                    functionElement.setText(instanceTPF.getSource());

                    queueElement.addContent(functionElement);
                    aggregateElement.addContent(queueElement);
                    changed = true;
                }
            }
        }
        return changed;
    }

    /**
     * Add info about changed places in aggregate
     * @param instance - aggregate instance with changes
     * @param aggregateElement - jDOM element to add changes info
     * @return true if change found, false otherwise
     */
    private boolean writePlaces(AggregateInstance instance, Element aggregateElement) {
        AggregateDefinition definition = instance.getAggregateDefinition();
        boolean changed = false;

        for (Place definitionPlace : definition.getPlaces()) {
            Place instancePlace = instance.getPlace(definitionPlace.getName());
            Token definitionToken = definitionPlace.getToken();
            Token instanceToken = instancePlace.getToken();

            if ( ! equalTokens(instanceToken, definitionToken)) {
                changed = true;
                Element placeElement = new Element("place", NS);
                placeElement.setAttribute("name", instancePlace.getName());

                if (instanceToken != null) {
                    Element tokenElement = new Element("token", NS);
                    for (String attributeName : instanceToken.getAttributeNames()) {
                        Element attributeElement = new Element("attribute", NS);
                        Value value = instanceToken.getValue(attributeName);

                        attributeElement.setAttribute("name", attributeName);
                        attributeElement.setAttribute("value", value.toString());

                        tokenElement.addContent(attributeElement);
                    }

                    placeElement.addContent(tokenElement);
                }

                aggregateElement.addContent(placeElement);
            }
        }

        return changed;
    }

    private boolean equalTokens(Token t1, Token t2) {
        if (t1 == null && t2 == null) {
            return true;
        }

        if (t1 != null && t1.equals(t2)) {
            return true;
        }

        return false;
    }
}
