package ua.cn.stu.cs.ems.core.queues;

import ua.cn.stu.cs.ems.core.Token;

/**
 * Interface to watch for queue state changes.
 * @author proger
 */
public interface QueueStateListener {

    /**
     * Called when new token was added to a queue.
     * @param queue
     * @param token
     */
     public void tokenAddedToQueue(Queue queue, Token token);

     /**
      * Called when token was removed from a queue
      * @param queue
      * @param token
      */
     public void tokenRemovedFromQueue(Queue queue, Token token);
}
