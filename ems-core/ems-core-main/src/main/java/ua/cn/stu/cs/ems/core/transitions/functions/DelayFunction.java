package ua.cn.stu.cs.ems.core.transitions.functions;

import ua.cn.stu.cs.ems.core.transitions.Transition;

/**
 * DelayFunction - interface for all kind of functions that calculate transition
 * delay time.
 */
public interface DelayFunction {

    /**
     * Calculate delay time for transition.
     * @param transition - transition is going to be delayed
     * @return double - delay time
     */
    public double getDelayTime(Transition transition);

    /**
     * Create a copy of this delay function.
     * @return cope of this delay function
     */
    public DelayFunction copy();
}
