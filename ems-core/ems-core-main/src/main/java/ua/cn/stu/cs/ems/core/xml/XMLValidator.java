package ua.cn.stu.cs.ems.core.xml;

import com.thaiopensource.util.PropertyMapBuilder;
import com.thaiopensource.validate.SchemaReader;
import com.thaiopensource.validate.ValidateProperty;
import com.thaiopensource.validate.ValidationDriver;
import com.thaiopensource.validate.prop.rng.RngProperty;
import java.io.IOException;
import java.io.InputStream;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;


/**
 * XMLValidator checks XML file according to specified RELAX NG schema.
 * @author Ivan Mushketik
 */
public class XMLValidator {

    public static final String ENETS_RNG = "RNG/enets.rng";
    public static final String MODEL_RNG = "RNG/modeldefinition.rng";
   

    /**
     * Validate XML file according to a RNG scheme
     * @param xmlToValidate - stream with XML to validate.
     * @param schemeName - name of the RNG scheme that will be read from classpath
     * @return object that holds warnings, errors and fatal errors that occurred during XML validation
     * @throws IOException
     * @throws SAXException
     * @see ValidationResult
     */
    public ValidationResult validate(InputStream xmlToValidate, String schemeName) throws IOException, SAXException {
            RNGErrorHandler eh = new RNGErrorHandler();
            PropertyMapBuilder properties = new PropertyMapBuilder();
            properties.put(ValidateProperty.ERROR_HANDLER, eh);
            RngProperty.CHECK_ID_IDREF.add(properties);
            properties.put(RngProperty.CHECK_ID_IDREF, null);

            SchemaReader sr = null;
            ValidationDriver driver = new ValidationDriver(properties.toPropertyMap(), sr);
            InputSource in = new InputSource(ClassLoader.getSystemResourceAsStream(schemeName));
            driver.loadSchema(in);

            driver.validate(new InputSource(xmlToValidate));

            return eh.getValidationResult();
    }

    /**
     * Checks if XML is valid according to a RNG scheme
     * @param xmlToValidate - stream with XML to validate
     * @param schemeName - name of the RNG scheme that will be read from classpath
     * @return true if XML is valid and false otherwise.
     * @throws IOException
     * @throws SAXException
     */
    public boolean isValid(InputStream xmlToValidate, String schemeName) throws IOException, SAXException {
        ValidationResult vr = validate(xmlToValidate, schemeName);

        return !vr.hasErrors() && !vr.hasFatalErrors();
    }

    
}
