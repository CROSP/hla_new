package ua.cn.stu.cs.ems.core.experiment;

import ua.cn.stu.cs.ems.core.aggregates.AggregateVariable;

/**
 *
 * @author proger
 */
public class NullFactor implements Factor {

    private boolean isUsed;

    public double getCurrentValue() {
        return Double.NaN;
    }

    public int getValueNumber() {
        return isUsed ? 0 : 1;
    }

    public AggregateVariable getVariable() {
        return null;
    }

    public boolean hasMoreValues() {
        return !isUsed;
    }

    public void nextValue() {
        isUsed = true;
    }

    public int numberOfValues() {
        return 1;
    }

    public void setValue() {
    }

    public String getFactorName() {
        return "NullFactor";
    }
}