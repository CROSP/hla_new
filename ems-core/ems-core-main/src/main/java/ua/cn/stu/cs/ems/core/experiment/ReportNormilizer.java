package ua.cn.stu.cs.ems.core.experiment;

import java.util.*;
import ua.cn.stu.cs.ems.core.experiment.statistics.StatisticsParameters;
import ua.cn.stu.cs.ems.core.experiment.statistics.StatisticsResult;

/**
 * When experiment returns it's report it returns info about each model start.
 * What is worse, it return statistics that was collected at each time interval
 * for each object during each model run, for each factor values. Although this
 * data is useful it's hard to visualize all this data. This class helps to
 * normalize (to make it possible to visualize) report during experiment run.
 *
 * @author proger
 */
public final class ReportNormilizer {

    private ReportNormilizer() {
    }

    /**
     * Normalization doing following work: <ul> <li>Normalize secondary
     * statistics <li>Normalize primary statistics </ul>
     *
     * Secondary statistics normalization is easier. Secondary statistics
     * consists of respond values for each run of model. Normalized secondary
     * statistics consists of average of respond values with the same factor
     * value. <p> Primary statistics normalization is not much different.
     * Primary statistics is values that were collected by statistics collectors
     * at specified moments in time during each model run. Normalized primary
     * statistics constains the same data but instead of storing info about each
     * model run it stores average for the same factor value.
     *
     * @param report - report to normalize.
     * @return normalized report
     */
    /*
     * In ExperimentReport responses (and primary statistics) for the same
     * factor values are always placed near each other. If factor values are 1,
     * 2; secondary statistics may look like: [1, 1], [1, 1.2], [1, 1.1], [2,
     * 1], [2, 1.2], [2, 1.1] ([X, Y]) X - factor value; Y - respond
     *
     */
    public static ExperimentReport normilizeReport(ExperimentReport report) {
        ExperimentReport result = new ExperimentReport();

        copyFactorValues(report, result);
        moveVariablesState(report, result); // fix it, need copy!!!
        normalizePrimaryStatistics(report, result);
        normilizeSecondaryStatistics(report, result);

        return result;
    }

    private static void normalizePrimaryStatistics(ExperimentReport report, ExperimentReport result) {
        // For all object for which primary statistics was collected
        for (String objectName : report.getStatisticsObjectsNames()) {
            result.addPrimaryStatisticsObject(objectName);

            List<PrimaryStatisticsResult> psrList = report.getPrimaryStatisticsFor(objectName);

            if (!psrList.isEmpty()) {
                Double prevFactorValue = null;
                ArrayList<PrimaryStatisticsResult> resultsForSameFactor = new ArrayList<PrimaryStatisticsResult>();

                for (PrimaryStatisticsResult psr : psrList) {
                    Double factorValue = psr.getFactorValue();

                    // If found new factor value and this is not primary statistics result
                    if (!factorValue.equals(prevFactorValue) && prevFactorValue != null) {
                        // Calculate average for all model runs with same factor value
                        PrimaryStatisticsResult normilizedPSR = createNormilizedPSR(prevFactorValue, resultsForSameFactor);
                        result.addPrimaryStatisticsResult(objectName, normilizedPSR);

                        resultsForSameFactor.clear();
                    }

                    resultsForSameFactor.add(psr);
                    prevFactorValue = factorValue;
                }

                // Don't forget about last factor value
                PrimaryStatisticsResult normilizedPSR = createNormilizedPSR(prevFactorValue, resultsForSameFactor);
                result.addPrimaryStatisticsResult(objectName, normilizedPSR);
            }

        }
    }

    private static PrimaryStatisticsResult createNormilizedPSR(Double factorValue, ArrayList<PrimaryStatisticsResult> resultsForSameFactor) {
        /*
         * We should calculate average for each time interval. For this we
         * should move through lists that represents all model runs with same
         * factor values simulataniously
         */
        Iterator<PrimaryStatisticsElement>[] iterators = getPrimaryStatisticsElementsIterators(resultsForSameFactor);
        ArrayList<PrimaryStatisticsElement> normilizedPrimaryStatisticsElements = new ArrayList<PrimaryStatisticsElement>();

        if (iterators.length > 0) {

            while (true) {
                double time = 0;
                ArrayList<StatisticsResult> resultsForCurrentTime = new ArrayList<StatisticsResult>();

                // Check all iterators
                for (int i = 0; i < iterators.length; i++) {
                    Iterator iter = iterators[i];

                    // If iterator has more data, this means that model worked for at least one interval
                    if (iter.hasNext()) {
                        PrimaryStatisticsElement pse = iterators[i].next();
                        resultsForCurrentTime.add(pse.getStatisticsResult());
                        time = pse.getTime();
                    }
                }

                // If all iterators are exhausted, no more time intervals left to check 
                if (resultsForCurrentTime.isEmpty()) {
                    break;
                }

                // Create statistics result each parameter value is average of values of same parameter of all 
                // statistics results
                StatisticsResult normalizedSR = createNormilizedStatisticsResult(resultsForCurrentTime);

                PrimaryStatisticsElement normalizedPSE = new PrimaryStatisticsElement(time, normalizedSR);
                normilizedPrimaryStatisticsElements.add(normalizedPSE);

            }
        }

        PrimaryStatisticsResult result = new PrimaryStatisticsResult(factorValue, normilizedPrimaryStatisticsElements);
        return result;
    }

    private static Iterator<PrimaryStatisticsElement>[] getPrimaryStatisticsElementsIterators(ArrayList<PrimaryStatisticsResult> resultsForSameFactor) {
        Iterator<PrimaryStatisticsElement>[] iterators = new Iterator[resultsForSameFactor.size()];

        for (int i = 0; i < iterators.length; i++) {
            iterators[i] = resultsForSameFactor.get(i).getDumpedPrimaryStatisticsElements().iterator();
        }

        return iterators;
    }

    private static StatisticsResult createNormilizedStatisticsResult(ArrayList<StatisticsResult> resultsForCurrentTime) {
        Set<StatisticsParameters> statisticsParams = resultsForCurrentTime.get(0).getKeys();
        HashMap<StatisticsParameters, Double> sums = new HashMap<StatisticsParameters, Double>();

        // Save what parameters were collected for this object
        for (StatisticsParameters param : statisticsParams) {
            sums.put(param, 0d);
        }

        // Sum values for each parameter
        for (StatisticsResult result : resultsForCurrentTime) {
            for (StatisticsParameters param : result.getKeys()) {
                sums.put(param, sums.get(param) + result.getResultValue(param));
            }
        }

        // Find averadge value
        for (StatisticsParameters param : sums.keySet()) {
            sums.put(param, sums.get(param) / resultsForCurrentTime.size());
        }

        StatisticsResult result = new StatisticsResult();

        // Create StatisticsResult with average values
        for (StatisticsParameters param : sums.keySet()) {
            result.setResultValue(param, sums.get(param));
        }

        return result;

    }

    private static void normilizeSecondaryStatistics(ExperimentReport report, ExperimentReport result) {
        List<SecondaryStatisticsElement> sseList = report.getSecondaryStatistics();

        if (!sseList.isEmpty()) {
            Double prevFactorValue = null;
            Double respondsSum = 0d;
            int numberOfRespondsWithSameFactorValue = 0;

            for (SecondaryStatisticsElement sse : sseList) {
                Double factorValue = sse.getFactorValue();

                // If we found new factor value and it's not first factor value
                if (!factorValue.equals(prevFactorValue) && prevFactorValue != null) {
                    // Calculate average respond for this factor value
                    Double normalizedValue = respondsSum / numberOfRespondsWithSameFactorValue;
                    /*
                     * SecondaryStatisticsElement normalizedSSE = new
                     * SecondaryStatisticsElement(prevFactorValue,
                     * normalizedValue);
                     *
                     * // Add new value to normalized report
                     * result.addSecondaryStatisticsElement(normalizedSSE);
                     */

                    respondsSum = 0d;
                    numberOfRespondsWithSameFactorValue = 0;
                }

                // We have already seen this factor value

                Double respond = sse.getRespond();
                // Calculating sum of respond values to find average later
                respondsSum += respond;
                numberOfRespondsWithSameFactorValue++;

                prevFactorValue = factorValue;
            }

            // Don't forget last factor value
            Double normalizedValue = respondsSum / numberOfRespondsWithSameFactorValue;
            /*
             * SecondaryStatisticsElement normalizedSSE = new
             * SecondaryStatisticsElement(prevFactorValue, normalizedValue);
             */

            //  result.addSecondaryStatisticsElement(normalizedSSE);
        }
    }

    private static void copyFactorValues(ExperimentReport report, ExperimentReport result) {
        for (Double factorValue : report.getFactorValues()) {
            result.addFactorValue(factorValue);
        }
    }
    
    private static void moveVariablesState(ExperimentReport report, ExperimentReport result) {
        result.setVariablesState(report.getVariablesState());
    }
}