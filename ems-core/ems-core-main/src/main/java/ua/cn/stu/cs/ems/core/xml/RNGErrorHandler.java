package ua.cn.stu.cs.ems.core.xml;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 * Error handler that saves all warnings, errors and fatal errors that occurred
 * during XML validation.
 * @author Ivan Mushketik
 */
public class RNGErrorHandler implements ErrorHandler {

    ValidationResult vr = new ValidationResult();

    public ValidationResult getValidationResult() {
        return vr;
    }

    public void warning(SAXParseException saxpe) throws SAXException {
        vr.addWarning(saxpe.getLocalizedMessage());
    }

    public void error(SAXParseException saxpe) throws SAXException {
        vr.addError(saxpe.getLocalizedMessage());
    }

    public void fatalError(SAXParseException saxpe) throws SAXException {
        vr.addFatalError(saxpe.getLocalizedMessage());
    }
}
