package ua.cn.stu.cs.ems.core.ecli;

import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.aggregates.Aggregate;
import ua.cn.stu.cs.ems.core.models.Model;
import ua.cn.stu.cs.ems.ecli.EcliAggregate;
import ua.cn.stu.cs.ems.ecli.EcliToken;
import ua.cn.stu.cs.ems.ecli.ModelAccessor;

/**
 * Accessor that does not commit changes to the model. Should be used for testing purposes only.
 *
 * @author n0weak
 */
public class IdleModelAccessor extends IdleAggregateAccessor implements ModelAccessor {

    private Model model;
    private Token currentToken;
    private Aggregate currentAggregate;

    public IdleModelAccessor(Model model, Aggregate currentAggregate) {
        super(model.getRootAggregate());
        this.model = model;
        this.currentAggregate = currentAggregate;
    }

    public double getTime() {
        return model.getModelingTime();
    }

    public EcliToken getCurrentToken() {
        return wrap(currentToken);
    }

    public void setCurrentToken(Token currentToken) {
        this.currentToken = currentToken;
    }

    public EcliAggregate getCurrentAggregate() {
        return wrap(currentAggregate);
    }

    public EcliAggregate getRootAggregate() {
        return wrap(model.getRootAggregate());
    }

}
