package ua.cn.stu.cs.ems.core.utils;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.codehaus.plexus.util.StringOutputStream;
import org.jdom.Document;
import org.jdom.output.XMLOutputter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.cn.stu.cs.ems.core.aggregates.*;
import ua.cn.stu.cs.ems.core.xml.AggregateWriter;

/**
 *
 * @author proger
 */
public class JessUtils {

    final static Logger logger = LoggerFactory.getLogger(JessUtils.class);
    public final static double EPSILON = 1e-10;

    /**
     * Utils classes shouldn't have public constructors.
     */
    private JessUtils() {
        throw new UnsupportedOperationException(
                "This utility class is not intended to be instanciated!");
    }

    public static String getAggregateChildFullName(AggregateChild aggregateChild) {
        final String parentsName = getAggregatesFullName(aggregateChild.getParentAggregate());

        return parentsName + aggregateChild.getName();
    }

    public static String getAggregateChildFullName(AggregateVariable aggregateVariable) {
        final String parentsName = getAggregatesFullName(aggregateVariable.getParentAggregate());

        return parentsName + aggregateVariable.getName();
    }

    private static String getAggregatesFullName(Aggregate parentAggregate) {
        String result = "";
        Aggregate aggregate = parentAggregate;

        while (aggregate != null) {
            result = aggregate.getName() + "." + result;
            aggregate = aggregate.getParentAggregate();
        }

        return result;
    }

    public static String getAggregateChildName(Aggregate root, AggregateChild child) {
        String res = getAggregateChildFullName(root, child);
        if (res != null && res.indexOf('.') > -1) {
            return res.substring(res.indexOf('.') + 1);
        }
        return res;
    }

    public static String getAggregateChildFullName(Aggregate root, AggregateChild child) {
        final String parentsName = getAggregatesFullName(child.getParentAggregate());

        return parentsName + child.getName();
    }

    public static String getAggregateFullName(Aggregate root, Aggregate child) {
        for (Aggregate a : root.getChildAggregates()) {
            if (a == child) {
                return root.getName() + "." + child.getName();
            } else {
                String name = getAggregateFullName(a, child);
                if (name != null) {
                    return root.getName() + "." + name;
                }
            }
        }
        return null;
    }

    public static AggregateChild getAggregateChild(Aggregate root, String path) {
        if (path == null || path.isEmpty()) {
            return root;
        }
        int dot = path.indexOf('.');
        if (dot < 0) {
            return root.getAggregateChild(path);
        }
        root = root.getAggregate(path.substring(0, dot));
        if (root == null) {
            return null;
        }
        return getAggregateChild(root, path.substring(dot + 1));
    }

    /**
     * ***********************************************************************
     */
    public static List<String> listAggregateChilds(Aggregate rroot, Aggregate root) {
        List<String> list = new ArrayList<String>();

        for (Aggregate a : root.getChildAggregates()) {
            String name = getAggregateFullName(rroot, a);
            name = name.substring(name.indexOf('.') + 1);
            list.add(name);
            list.addAll(listAggregateChilds(rroot, a));
        }

        return list;
    }

    private static List<String> readAggregateIO(Aggregate a, boolean outputs) {
        List<String> io = new ArrayList<String>();

        String ioPath = JessUtils.getAggregateChildFullName(a);
        if (ioPath == null) {
            ioPath = a.getName() + ".";
        } else {
            ioPath = ioPath + ".";
        }

        if (!outputs) {
            for (Input i : a.getInputs()) {
                if ((i.getConnectedOutput() == null && i.getInputPlace() == null)) {
                    io.add(ioPath.substring(ioPath.indexOf('.') + 1) + i.getName());
                }
            }
        } else {
            for (Output o : a.getOutputs()) {
                if (o.getOutputPlace() == null && o.getConnectedInputs().isEmpty()) {
                    io.add(ioPath.substring(ioPath.indexOf('.') + 1) + o.getName());
                }
            }
        }

        return io;
    }

    public static List<String> getAggregateInputs(Aggregate root) {
        List<String> inputs = new ArrayList<String>();
        for (Input i : root.getInputs()) {
            inputs.add(i.getName());
        }

        List<String> childs = JessUtils.listAggregateChilds(root, root);
        for (String childName : childs) {
            inputs.addAll(readAggregateIO(
                    (Aggregate) JessUtils.getAggregateChild(root, childName), false));
        }

        Collections.sort(inputs, new IOComparator());
        return inputs;
    }

    public static List<String> getAggregateOutputs(Aggregate root) {
        List<String> outputs = new ArrayList<String>();
        for (Output o : root.getOutputs()) {
            outputs.add(o.getName());
        }

        List<String> childs = JessUtils.listAggregateChilds(root, root);
        for (String childName : childs) {
            outputs.addAll(readAggregateIO(
                    (Aggregate) JessUtils.getAggregateChild(root, childName), true));
        }

        Collections.sort(outputs, new IOComparator());
        return outputs;
    }

    private static class IOComparator implements Comparator<String> {

        public int compare(String o1, String o2) {
            if (o1.length() > o2.length()) {
                return 1;
            } else if (o1.length() < o2.length()) {
                return -1;
            }
            return o1.compareTo(o2);
        }
    }

    public static String aggregateToString(AggregateDefinition ad) {
        AggregateWriter writer = new AggregateWriter();
        Document dom = writer.createDOMFromAggregateDefinition(ad);
        OutputStream os = new StringOutputStream();

        XMLOutputter outputter = new XMLOutputter();
//        Format prettyFormat = Format.getPrettyFormat();
//        outputter.setFormat(prettyFormat);
        try {
            outputter.output(dom, os);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return os.toString();
    }
}
