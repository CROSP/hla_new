package ua.cn.stu.cs.ems.core.xml;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Namespace;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.cn.stu.cs.ems.core.JessException;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.aggregates.*;
import ua.cn.stu.cs.ems.core.queues.*;
import ua.cn.stu.cs.ems.core.transitions.*;
import ua.cn.stu.cs.ems.core.transitions.functions.*;
import ua.cn.stu.cs.ems.core.utils.Coordinates;
import ua.cn.stu.cs.ems.core.utils.JessUtils;

/**
 * AggregateWriter - build DOM tree for jDOM library from aggregate definition.
 *
 * @author proger
 */
public class AggregateWriter {

    public static final Namespace NS = Namespace.getNamespace("http://www.pnml.org/version-2009/grammar/pnml");
    public static final String PNML_TYPE = "http://www.cs.stu.cn.ua/jess/enets";
    final static Logger logger = LoggerFactory.getLogger(AggregateWriter.class);

    /**
     * Create DOM tree from aggregate definition.
     *
     * @param ad - aggregate definition
     * @return DOM tree for jDOM.
     */
    public Document createDOMFromAggregateDefinition(AggregateDefinition ad) {
        logger.debug("creating DOM from aggregate ad");
        Document document = new Document();
        Element pnml = new Element("pnml", NS);
        document.setRootElement(pnml);

        Element net = new Element("net", NS);
        net.setAttribute("id", ad.getName());
        net.setAttribute("type", PNML_TYPE);

        createVariablesDOM(net, ad);

        Element page = new Element("page", NS);
        page.setAttribute("id", ad.getName());

        createAggregateChildsDOM(ad, page);

        net.addContent(page);
        pnml.addContent(net);

        return document;
    }

    private void createVariablesDOM(Element netElement, AggregateDefinition ad) {
        Set<String> variablesNames = ad.getVariablesNames();

        if (variablesNames.size() > 0) {
            Element variablesElement = new Element("variables", NS);

            for (String varName : variablesNames) {
                Element variableElement = new Element("variable", NS);
                variableElement.setAttribute("name", varName);
                variableElement.setAttribute("value", ad.getVariable(varName).getValue().toString());

                variablesElement.addContent(variableElement);
            }

            netElement.addContent(variablesElement);
        }
    }

    private void createAggregateChildsDOM(AggregateDefinition ad, Element pageElement) {

        for (Place p : ad.getPlaces()) {
            createPlaceElement(p, pageElement);
        }

        for (Transition t : ad.getTransitions()) {
            createTransitionElement(t, pageElement, ad);
        }

        for (Queue q : ad.getQueues()) {
            createTransitionElement(q, pageElement, ad);
        }

        for (Input i : ad.getInputs()) {
            createTransitionElement(i, pageElement, ad);
        }

        for (Output o : ad.getOutputs()) {
            createTransitionElement(o, pageElement, ad);
        }

        for (Aggregate a : ad.getChildAggregates()) {
            createTransitionElement(a, pageElement, ad);
        }

    }

    private void createPlaceElement(Place p, Element pageElement) {
        Element placeElement = new Element("place", NS);

        placeElement.setAttribute("id", p.getName());
        placeElement.addContent(createName(p.getName(), p.getNameCoordinates()));

        Token token = p.getToken();
        if (token != null) {
            Element tokenElement = new Element("token", NS);
            for (String name : token.getAttributeNames()) {
                Element attributeElement = new Element("attribute", NS);
                attributeElement.setAttribute("name", name);
                attributeElement.setAttribute("value", token.getValue(name).toString());

                tokenElement.addContent(attributeElement);
            }

            placeElement.addContent(tokenElement);
        }

        placeElement.addContent(createGraphics(p.getObjectCoordinates()));

        pageElement.addContent(placeElement);
    }

    private void createTransitionElement(AggregateChild ai, Element pageElement, AggregateDefinition ad) {
        Element transitionElement = new Element("transition", NS);
        transitionElement.setAttribute("id", ai.getName());
        transitionElement.addContent(createName(ai.getName(), ai.getNameCoordinates()));

        Element definitionElement = new Element("definition", NS);
        fillDefinitionElement(definitionElement, ai);
        addArcs(pageElement, ai, ad);
        transitionElement.addContent(definitionElement);

        transitionElement.addContent(createGraphics(ai.getObjectCoordinates()));

        pageElement.addContent(transitionElement);
    }

    private Element createName(String name, Coordinates nameCoordinates) {
        Element nameElement = new Element("name", NS);

        Element textElement = new Element("text", NS);
        textElement.addContent(name);
        nameElement.addContent(textElement);

        Element graphicsElement = new Element("graphics", NS);
        Element offsetElement = new Element("offset", NS);
        offsetElement.setAttribute("x", Integer.toString(nameCoordinates.getX()));
        offsetElement.setAttribute("y", Integer.toString(nameCoordinates.getY()));
        graphicsElement.addContent(offsetElement);

        nameElement.addContent(graphicsElement);

        return nameElement;
    }

    private void fillDefinitionElement(Element definitionElement, AggregateChild ai) {
        if (ai instanceof Transition) {
            Transition t = (Transition) ai;
            definitionElement.setAttribute("type", "transition");
            if (t instanceof TTransition) {
                definitionElement.setAttribute("subType", "T");
            } else if (t instanceof FTransition) {
                definitionElement.setAttribute("subType", "F");
            } else if (t instanceof JTransition) {
                definitionElement.setAttribute("subType", "J");
            } else if (t instanceof XTransition) {
                definitionElement.setAttribute("subType", "X");
                createPermitingFunction(definitionElement, (SpecialTransition) t);
            } else if (t instanceof YTransition) {
                definitionElement.setAttribute("subType", "Y");
                createPermitingFunction(definitionElement, (SpecialTransition) t);
            } else {
                throw new JessException("Unknown transition type " + t.getClass().getName());
            }

            createDelayFunction(definitionElement, t);
            createTransformationFunction(definitionElement, t);
        } else if (ai instanceof Queue) {
            definitionElement.setAttribute("type", "queue");

            if (ai instanceof FIFOQueue) {
                definitionElement.setAttribute("subType", "FIFO");
            } else if (ai instanceof LIFOQueue) {
                definitionElement.setAttribute("subType", "LIFO");
            } else if (ai instanceof FIFOPriorityQueue) {
                definitionElement.setAttribute("subType", "PriorityFIFO");
                createPriorityFunction(definitionElement, (AbstractPriorityQueue) ai);
            } else if (ai instanceof LIFOPriorityQueue) {
                definitionElement.setAttribute("subType", "PriorityLIFO");
                createPriorityFunction(definitionElement, (AbstractPriorityQueue) ai);
            } else {
                throw new JessException("Unknown queue type " + ai.getClass().getName());
            }
        } else if (ai instanceof Input) {
            definitionElement.setAttribute("type", "input");
        } else if (ai instanceof Output) {
            definitionElement.setAttribute("type", "output");
        } else if (ai instanceof Aggregate) {
            AggregateDefinitionReference adr = (AggregateDefinitionReference) ai;
            AggregateDefinition ad = adr.getAggregateDefinition();

            definitionElement.setAttribute("type", "aggregate");
            definitionElement.setAttribute("subType", ad.getName());
        } else {
            throw new JessException("Unknown ActiveInstance type " + ai.getClass().getName());
        }

    }

    private Element createGraphics(Coordinates objectCoordinates) {
        Element graphicsElement = new Element("graphics", NS);

        Element positionElement = new Element("position", NS);
        positionElement.setAttribute("x", Integer.toString(objectCoordinates.getX()));
        positionElement.setAttribute("y", Integer.toString(objectCoordinates.getY()));

        graphicsElement.addContent(positionElement);

        return graphicsElement;
    }

    private void createPermitingFunction(Element definitionElement, SpecialTransition specialTransition) {
        PermitingFunction pf = specialTransition.getPermitingFunction();

        if (pf instanceof InterpretedPermitingFunction) {
            InterpretedPermitingFunction ipf = (InterpretedPermitingFunction) pf;

            Element permitingFunctionElement = new Element("permitingFunction", NS);
            permitingFunctionElement.setAttribute("type", "ecli");
            permitingFunctionElement.setText(ipf.getSource());
            definitionElement.addContent(permitingFunctionElement);
        }
    }

    private void createDelayFunction(Element definitionElement, Transition t) {
        DelayFunction df = t.getDelayFunction();

        if (df instanceof InterpretedDelayFunction) {
            InterpretedDelayFunction idf = (InterpretedDelayFunction) df;
            Element delayFunctionElement = new Element("delayFunction", NS);
            delayFunctionElement.setAttribute("type", "ecli");
            delayFunctionElement.setText(idf.getSource());

            definitionElement.addContent(delayFunctionElement);
        }
    }

    private void createTransformationFunction(Element definitionElement, Transition t) {
        TransformationFunction tf = t.getTransformationFunction();

        if (tf instanceof InterpretedTransformationFunction) {
            InterpretedTransformationFunction itf = (InterpretedTransformationFunction) tf;
            Element delayFunctionElement = new Element("transformationFunction", NS);
            delayFunctionElement.setAttribute("type", "ecli");
            delayFunctionElement.setText(itf.getSource());

            definitionElement.addContent(delayFunctionElement);
        }
    }

    private void createPriorityFunction(Element definitionElement, AbstractPriorityQueue abstractPriorityQueue) {
        TokenPriorityFunction tpf = abstractPriorityQueue.getPriorityFunction();

        if (tpf instanceof InterpretedTokenPriorityFunction) {
            InterpretedTokenPriorityFunction itpf = (InterpretedTokenPriorityFunction) tpf;
            Element priorityFunctionElement = new Element("priorityFunction", NS);
            priorityFunctionElement.setText(itpf.getSource());
            definitionElement.addContent(priorityFunctionElement);
        }
    }

    private Collection<Aggregate> getAllAggregateChilds(Aggregate root) {
        Collection<Aggregate> list = new ArrayList<Aggregate>();

        for (Aggregate a : root.getChildAggregates()) {
            list.add(a);
            list.addAll(getAllAggregateChilds(a));
        }

        return list;
    }

    private void addArcs(Element pageElement, AggregateChild ac, AggregateDefinition ad) {
        if (ac instanceof Transition) {
            Transition t = (Transition) ac;

            for (Place inputPlace : t.getInputPlaces()) {
                createNewArc(pageElement, null, inputPlace.getName(), null, t.getName(), ad);
            }

            for (Place outputPlace : t.getOutputPlaces()) {
                createNewArc(pageElement, null, t.getName(), null, outputPlace.getName(), ad);
            }
        } else if (ac instanceof Queue) {
            Queue q = (Queue) ac;

            if (q.getInputPlace() != null) {
                createNewArc(pageElement, null, q.getInputPlace().getName(), null, q.getName(), ad);
            }

            if (q.getOutputPlace() != null) {
                createNewArc(pageElement, null, q.getName(), null, q.getOutputPlace().getName(), ad);
            }

        } else if (ac instanceof Aggregate) {
            // FIX add arcs
//            logger.debug(ad.getName() + ": adding arcs for " + ac.getName());
            AggregateDefinitionReference rAdr = (AggregateDefinitionReference) ac;

            Collection<Aggregate> childs = getAllAggregateChilds(rAdr);
            childs.add(rAdr);

            for (Aggregate adr : childs) {
                for (Output o : adr.getOutputs()) {
                    if (o.getOutputPlace() != null) {
                        int index = -1;
                        String sp = JessUtils.getAggregateFullName(ad, o.getParentAggregate()) + "." + o.getName();
                        sp = sp.substring(sp.indexOf('.') + 1);
                        String s = "";

                        index = sp.indexOf('.');
                        if (index > -1) {
                            s = sp.substring(index + 1);
                            sp = sp.substring(0, index);
                        } else {
                            s = sp;
                            sp = null;
                        }
                        if (ad == o.getOutputPlace().getParentAggregate()) {
                            createNewArc(pageElement, sp, s, null, o.getOutputPlace().getName(), ad);
                        }
                    } else {
                        for (Input i : o.getConnectedInputs()) {
                            int index = -1;
                            String sp = JessUtils.getAggregateFullName(ad, adr);
                            sp = sp.substring(sp.indexOf('.') + 1);
                            String s = o.getName();

                            index = sp.indexOf('.');
                            if (index > -1) {
                                s = sp.substring(index + 1) + "." + s;
                                sp = sp.substring(0, index);
                            }


                            String tp = JessUtils.getAggregateFullName(ad, i.getParentAggregate());
                            tp = tp.substring(tp.indexOf('.') + 1);
                            String t = i.getName();
                            index = tp.indexOf('.');
                            if (index > -1) {
                                t = tp.substring(index + 1) + "." + t;
                                tp = tp.substring(0, index);
                            }

                            if (ad.getArcCoordinates(sp, s, tp, t) != null) {
                                createNewArc(pageElement, sp, s, tp, t, ad);
                            }
                        }
                    }
                }

                for (Input i : adr.getInputs()) {
                    if (i.getInputPlace() != null && i.getInputPlace().getParentAggregate() == ad) {
                        String tp = JessUtils.getAggregateFullName(ad, i.getParentAggregate()) + "." + i.getName();
                        tp = tp.substring(tp.indexOf('.') + 1);
                        String t = tp.substring(tp.indexOf('.') + 1);
                        tp = tp.substring(0, tp.indexOf('.'));

                        createNewArc(pageElement, null, i.getInputPlace().getName(), tp, t, ad);
                    }
                }
            }
        } else if (ac instanceof Input) {
            Input sourceInput = (Input) ac;
            if (sourceInput.getOutputPlace() != null) {
                createNewArc(pageElement, null, sourceInput.getName(),
                        null, sourceInput.getOutputPlace().getName(), ad);
            }
        } else if (ac instanceof Output) {
            Output sourceOutput = (Output) ac;
            if (sourceOutput.getInputPlace() != null) {
                createNewArc(pageElement, null, sourceOutput.getInputPlace().getName(),
                        null, sourceOutput.getName(), ad);
            }
        }
    }

    private void createNewArc(Element pageElement, String sourceParentName, String sourceName,
            String targetParentName, String targetName, AggregateDefinition ad) {
        AggregateChild ac1 = getAggregateChild(ad, sourceParentName, sourceName);
        AggregateChild ac2 = getAggregateChild(ad, targetParentName, targetName);
        // If one of aggregate childs was deleted, no arc should be written to
        // a PNML file
        if (ac1 == null || ac2 == null) {
            return;
        }

        Element arcElement = new Element("arc", NS);

        String pnmlSourceName = (sourceParentName == null) ? sourceName
                : sourceParentName + "." + sourceName;

        String pnmlTargetName = (targetParentName == null) ? targetName
                : targetParentName + "." + targetName;

        arcElement.setAttribute("source", pnmlSourceName);
        arcElement.setAttribute("target", pnmlTargetName);

        ArcCoordinates ac = ad.getArcCoordinates(sourceParentName, sourceName, targetParentName, targetName);
        if (ac != null) {
            Element graphicsElement = new Element("graphics", NS);
            for (Coordinates coordinates : ac.getCoordinates()) {
                Element positionElement = new Element("position", NS);

                positionElement.setAttribute("x", Integer.toString(coordinates.getX()));
                positionElement.setAttribute("y", Integer.toString(coordinates.getY()));

                graphicsElement.addContent(positionElement);
            }

            arcElement.addContent(graphicsElement);
        } else if (sourceParentName != null || targetParentName != null) {
            // if have some aggregates (source, target or both) need to add 
            // empty graphics ot model will be fail on restore
            Element graphicsElement = new Element("graphics", NS);
            arcElement.addContent(graphicsElement);
        }

        pageElement.addContent(arcElement);
    }

    private AggregateChild getAggregateChild(AggregateDefinition ad, String parentName, String childName) {
        Aggregate elementParent = null;

        if (parentName == null || ad.getName().equals(parentName)) {
            elementParent = ad;
        } else {
            elementParent = ad.getAggregate(parentName);
        }

        while (childName.contains(".")) {
            parentName = childName.substring(0, childName.indexOf("."));
            childName = childName.substring(childName.indexOf(".") + 1);
            elementParent = elementParent.getAggregate(parentName);
        }
        return elementParent.getAggregateChild(childName);
    }
}
