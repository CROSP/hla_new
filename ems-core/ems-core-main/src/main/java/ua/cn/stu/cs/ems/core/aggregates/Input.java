package ua.cn.stu.cs.ems.core.aggregates;

import ua.cn.stu.cs.ems.core.ActiveInstance;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.Settable;
import ua.cn.stu.cs.ems.core.Token;

/**
 * Interface that represent aggregate's input. Each input can have input and output
 * objects connected to it. Input object can be either a place or an output. Output object
 * can be a place only.
 * @author proger
 */
public interface Input extends  Settable, ActiveInstance {
    /**
     * Set a token to this input. If a place is connected to this input's output and it's empty
     * the token should be set to the place. Otherwise it's an error.
     * @param token - token to set.
     */
    void setToken(Token token);

    /**
     * Check if a token can be set to this input.
     * @return true if a token can be set, false otherwise.
     */
    boolean canSetToken();

    /**
     * Connect input place.
     * @param place - input place to connect.
     */
    void setInputPlace(Place place);

    /**
     * Connect output place
     * @param place - output place to connect.
     */
    void setOutputPlace(Place place);

    /**
     * Connect output to the input.
     * @param output - output to connect. Can't be null.
     */
    void connectOutput(Output output);

    /**
     * Get output place that is connected to the output of this input.
     * @return output place if one is connected to this input, null otherwise.
     */
    Place getOutputPlace();

    /**
     * Get place that is connected to the input of this input.
     * @return place if this input has an input place, null otherwise.
     */
    public Place getInputPlace();

    /**
     * Get an output connected to this input.
     * @return output if any connected to this input, null otherwise.
     */
    public Output getConnectedOutput();
}
