package ua.cn.stu.cs.ems.core.transitions;

import ua.cn.stu.cs.ems.core.utils.JessUtils;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.Place;
import static ua.cn.stu.cs.ems.core.utils.AssertUtils.*;

/**
 *
 * @author proger
 */
public class XTransition extends AbstractSpecialTransition {

    final static Logger logger = LoggerFactory.getLogger(XTransition.class);
    Integer permitingFunctionResult = null;

    public XTransition(String name) {
        super(name);
    }

    @Override
    protected boolean canFire() {
        Place inputPlace = getInputPlace();
        if (inputPlace == null || inputPlace.isEmpty()) {
            return false;
        }

        permitingFunctionResult = getPermitingFunction().getPlaceNumber(this);
        logger.info("In {} transition permiting function return {}",
                JessUtils.getAggregateChildFullName(this), permitingFunctionResult);
 
        if (!permitingFunctionResultIsCorrect(permitingFunctionResult)) {
            return false;
        }

        return true;
    }

    protected void fireTransition() {
        Token inputToken = getInputPlace().getToken();
        Token token = getTransformationFunction().transform(this, new Token(inputToken));

        getOutputPlaces().get(permitingFunctionResult).setToken(token);
        getInputPlace().setToken(null);
    }

    public void connectInputPlace(Place place) {
        assertNotNull(place);
        if (getInputPlaces().size() > 0) {
            replaceInputPlace(place, 0);
        } else {
            addInputPlace(place, 0);
        }
    }

    public Place getInputPlace() {
        if (getInputPlaces().size() > 0) {
            return getInputPlaces().get(0);
        } else {
            return null;
        }
    }

    public void setInputPlace(Place place) {
        if (getInputPlaces().size() > 0) {
            replaceInputPlace(place, 0);
        } else {
            addInputPlace(place, 0);
        }
    }

    public void delInputPlace() {
        disconnectInputPlace(0);
    }

    private boolean permitingFunctionResultIsCorrect(Integer result) {
        if (permitingFunctionResult == null) {
            return false;
        }

        if (permitingFunctionResult >= getOutputPlaces().size() || permitingFunctionResult < 0) {
            return false;
        }

        if (!getOutputPlaces().get(result).isEmpty()) {
            return false;
        }

        return true;
    }

    @Override
    protected int maxInputSize() {
        return 1;
    }

    @Override
    protected int maxOutputSize() {
        return UNLIMITED;
    }
}
