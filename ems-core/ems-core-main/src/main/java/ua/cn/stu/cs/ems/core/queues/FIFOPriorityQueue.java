package ua.cn.stu.cs.ems.core.queues;

/**
 * FIFO priority queue. First token to leave queues has highest priority.
 * @author proger
 */
public class FIFOPriorityQueue extends AbstractPriorityQueue {

    public FIFOPriorityQueue(String name, TokenPriorityFunction tpf) {
        super(name, tpf);
    }

    @Override
    protected int getPriorityForQueue(int priority) {
        return -priority;
    }


}
