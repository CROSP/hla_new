package ua.cn.stu.cs.ems.core.utils;

/**
 * Class that stores coordinates of graphical object.
 * @author proger
 */
public class Coordinates {

    private int x;
    private int y;

    /**
     * Create coordinate object with specified x, y coordinates.
     * @param theX
     * @param theY 
     */
    public Coordinates(int theX, int theY) {
        x = theX;
        y = theY;
    }

    /**
     * Create coordinate object with (0, 0) coordinates.
     */
    public Coordinates() {
    }

    /**
     * Get X coordinate.
     * @return x coordinate
     */
    public final int getX() {
        return x;
    }

    /**
     * Set X coordinate.
     * @param x new value for x coordinate
     */
    public final void setX(int theX) {
        x = theX;
    }

    /**
     * Get Y coordinate.
     * @return y coordinate
     */
    public final int getY() {
        return y;
    }

    /**
     * Set Y coordinate.
     * @param theY new value for y coordinate
     */
    public final void setY(int theY) {
        y = theY;
    }
}
