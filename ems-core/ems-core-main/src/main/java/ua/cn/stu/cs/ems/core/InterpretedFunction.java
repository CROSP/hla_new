package ua.cn.stu.cs.ems.core;

import static ua.cn.stu.cs.ems.core.utils.AssertUtils.assertNotNull;

/**
 * Base class for all functions which behavior is defined by ecli code.
 * @author proger
 */
public class InterpretedFunction {
    protected String source;

    /**
     * @param theSource - ecli source
     */
    protected InterpretedFunction(String theSource) {
        assertNotNull(theSource, "Ecli source can't be null");
        source = theSource;
    }

    /**
     * Get ecli source.
     * @return ecli source for this function.
     */
    public final String getSource() {
        return source;
    }

    @Override
    public final boolean equals(Object o) {
        if (o instanceof InterpretedFunction) {
            final InterpretedFunction intFunc = (InterpretedFunction) o;

            return intFunc.getSource().equals(source);
        }
        return false;
    }

    @Override
    public final int hashCode() {
        int hash = 7;
        hash = 89 * hash + (this.source != null ? this.source.hashCode() : 0);
        return hash;
    }
}
