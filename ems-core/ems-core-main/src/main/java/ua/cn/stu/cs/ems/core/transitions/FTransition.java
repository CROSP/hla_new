package ua.cn.stu.cs.ems.core.transitions;

import ua.cn.stu.cs.ems.core.AbstractTransition;
import java.util.List;
import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.Place;

/**
 * FTransition - class that represents F-type transition in E nets. It can has
 * any number of output places and only one input places.
 * @author proger
 */
public class FTransition extends AbstractTransition {

    public FTransition(String name) {
        super(name);
    }

    /**
     * Check if transition is ready to transfer token.
     * @return true if all conditions for transition activations are true
     */
    @Override
    protected boolean canFire() {
        for (Place pos : getOutputPlaces())
            if (!pos.isEmpty())
                return false;

        Place inputPlace = getInputPlaces().get(0);
        if (inputPlace.isEmpty())
            return false;

        return true;
    }

    /**
     * When delay of this transfer is finished this method is called by model.
     * By E nets theory when delay time finishes this transition copy token
     * from it's input place to all output places.
     */
    protected void fireTransition() {
        Place inputPlace = getInputPlace();

        for (Place place : getOutputPlaces()) {
            Token inputToken = inputPlace.getToken();
            Token newToken = getTransformationFunction().transform(this, new Token(inputToken));
            place.setToken(newToken);
        }

        inputPlace.setToken(null);
    }

    /**
     * Set input places for this transition
     * @param place - input place
     */
    public void setInputPlace(Place place) {
        List<Place> inputPlaces = getInputPlaces();
        
        if (inputPlaces.isEmpty())
            super.addInputPlace(place, 0);
        else
            super.replaceInputPlace(place, 0);
    }

    /**
     * Get input place for this transition
     * @return - input place
     */
    public Place getInputPlace() {
        if (getInputPlaces().isEmpty())
            return null;
        else
            return getInputPlaces().get(0);
    }

    @Override
    protected int maxInputSize() {
        return 1;
    }

    @Override
    protected int maxOutputSize() {
        return UNLIMITED;
    }
}
