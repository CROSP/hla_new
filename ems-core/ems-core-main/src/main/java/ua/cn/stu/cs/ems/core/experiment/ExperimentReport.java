package ua.cn.stu.cs.ems.core.experiment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import ua.cn.stu.cs.ems.core.experiment.statistics.VariablesStateCollector;

/**
 *
 * @author proger
 */
public final class ExperimentReport {
    
    // Values of variable respond after each model run
    private List<SecondaryStatisticsElement> secondaryStatistics = 
            new ArrayList<SecondaryStatisticsElement>();
    
    // Result of primary statistics collection for each aggregate child for all pased runs
    private HashMap<String, List<PrimaryStatisticsResult>> primaryStatisticsResult = 
            new HashMap<String, List<PrimaryStatisticsResult>>();
    
    private List<Double> factorValues = new ArrayList<Double>();

    private VariablesStateCollector collectedVariablesState;
    
    private boolean primaryStatisticsCollected;
    private boolean secondaryStatisticsCollected;
    
    /**
     * Get collected secondary statistics.
     * @return collected secondary statistics.
     */
    public final List<SecondaryStatisticsElement> getSecondaryStatistics() {
        return secondaryStatistics;
    }
    
    public final void addSecondaryStatisticsElement(SecondaryStatisticsElement sse) {
    	secondaryStatisticsCollected = true;
        secondaryStatistics.add(sse);
    }
   
    /**
     * Get collected primary statistics for an aggregate child.
     * @param element - aggregate child to collect primary statistics for
     * @return collected primary statistics for an aggregate child
     */
    public final List<PrimaryStatisticsResult> getPrimaryStatisticsFor(String elementName) {
        return primaryStatisticsResult.get(elementName);
    }
    
    public final void addPrimaryStatisticsResult(String elementName, PrimaryStatisticsResult psr) {
        List<PrimaryStatisticsResult> psrList = primaryStatisticsResult.get(elementName);
        psrList.add(psr);
    }
        
    /**
     * Get names of all object for which primary statistics was collected.
     * @return set of names of all objects that are used to collect statistics.
     */
    public final Set<String> getStatisticsObjectsNames() {
        return primaryStatisticsResult.keySet();
    }
    
    /**
     * Add name of an object for which primary statistics was collected.
     * @param primaryStatisticsObjectName - name of an object for which primary statistics was collected.
     */
    public final void addPrimaryStatisticsObject(String primaryStatisticsObjectName) {
    	primaryStatisticsCollected = true;
        primaryStatisticsResult.put(primaryStatisticsObjectName, new ArrayList<PrimaryStatisticsResult>());
    }
    
    public final void addFactorValue(Double factorValue) {
    	factorValues.add(factorValue);
    }

    public final List<Double> getFactorValues() {
    	return factorValues;
    }

    public final boolean isPrimaryStatisticsCollected() {
    	return primaryStatisticsCollected;
    }

    public final boolean isSecondaryStatisticsCollected() {
    	return secondaryStatisticsCollected;
    }
    
    public void setVariablesState(VariablesStateCollector collected) {
        this.collectedVariablesState = collected;
    }

    public VariablesStateCollector getVariablesState() {
        return collectedVariablesState;
    }

    @Override
    public final boolean equals(Object obj) {
    	if (obj instanceof ExperimentReport) {
    		ExperimentReport other = (ExperimentReport) obj;

    		if (!this.primaryStatisticsResult.equals(other.primaryStatisticsResult)) {
    			return false;
    		}

    		if (!this.secondaryStatistics.equals(other.secondaryStatistics)) {
    			return false;
    		}

	    	if (!this.factorValues.equals(other.factorValues)) {
	    		return false;
	    	}

	    	return true;            
    	}
    
    	return false;
    }
    	
    @Override
    public int hashCode() {
    	int hash = 3;
    	hash = 47 * hash + (this.secondaryStatistics != null ? this.secondaryStatistics.hashCode() : 0);
    	hash = 47 * hash + (this.primaryStatisticsResult != null ? this.primaryStatisticsResult.hashCode() : 0);
    	return hash;
    }
}
