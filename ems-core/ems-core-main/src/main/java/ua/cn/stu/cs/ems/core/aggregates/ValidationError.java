package ua.cn.stu.cs.ems.core.aggregates;

import java.util.ArrayList;
import java.util.List;
import static ua.cn.stu.cs.ems.core.utils.AssertUtils.*;

/**
 * ValidationError represent and error that was find during aggregate validation.
 * It contains list of aggregates from parent to the aggregate where an error was found,
 * object with an error and error message.
 * @author proger
 */
public class ValidationError {

    private List<Aggregate> parents;
    private AggregateChild object;
    private String errorMessage;

    public ValidationError(List<Aggregate> parents, AggregateChild object, String error) {
        assertNotNull(parents, "Parents can't be null");
        assertNotNull(object, "Object with error can't be null");
        assertNotNull(error, "Error description can't be null");

        this.parents = new ArrayList<Aggregate>(parents);
        this.object = object;
        this.errorMessage = error;
    }

    /**
     * Return path to an object with an error. If an error occurred in a parrent aggregate
     * then this result list should be empty.
     * @return list of parent aggregates.
     */
    public List<Aggregate> getParents() {
        return parents;
    }

    /**
     * Return object where an error was found.
     * @return object with an error.
     */
    public AggregateChild getObjectWithError() {
        return object;
    }

    /**
     * Return error message.
     * @return error message.
     */
    public String getErrorMessage() {
        return errorMessage;
    }
}
