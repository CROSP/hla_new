package ua.cn.stu.cs.ems.core.experiment;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import ua.cn.stu.cs.ems.core.queues.Queue;
import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.Place;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import ua.cn.stu.cs.ems.core.aggregates.Aggregate;
import ua.cn.stu.cs.ems.ecli.Value;
import static ua.cn.stu.cs.ems.core.utils.AssertUtils.*;

/**
 * Restorer - class that can be used to restore state of the model as it was before
 * the model run.
 * During experiment we need to run the same model again and again, but variables,
 * places' states and so on change during modeling. We need to save model's state
 * before modeling and restore it after it to do experiment again.
 * @author proger
 */
// TODO set places and variables values silently to avoid wrong statistics collection
public class Restorer {

    private static final Logger logger = LoggerFactory.getLogger(Restorer.class);
    // Root of the model
    private Aggregate rootAggregate;
    // State of variables of aggregates
    private List<AggregateVariableValue> varState = new ArrayList<AggregateVariableValue>();
    // State of places
    private HashMap<Place, Token> placesState = new HashMap<Place, Token>();

    public Restorer(Aggregate theRootAggregate) {
        assertNotNull(theRootAggregate, "Root aggregate can't be null");

        rootAggregate = theRootAggregate;
        saveState(theRootAggregate);
    }

    /**
     * Restore state of the aggregate that was set in constructor and it's children.
     */
    public final void restoreState() {

        logger.debug("Begin of restore state of model:");
        for (AggregateVariableValue avv : varState) {
            avv.aggregate.setVariable(avv.name, avv.value);
        }

        restorePlacesAndQueues(rootAggregate);
        logger.debug("End of restore state of model");
    }

    private void restorePlacesAndQueues(Aggregate rootAggregate) {
        for (Place p : rootAggregate.getPlaces()) {
            p.setToken(null);
            Token oldToken = placesState.get(p);
            if (oldToken != null) {
                p.setToken(oldToken);
            }
        }

        for (Queue q : rootAggregate.getQueues()) {
            q.clear();
        }

        for (Aggregate child : rootAggregate.getChildAggregates()) {
            restorePlacesAndQueues(child);
        }
    }

    private void saveState(Aggregate aggregate) {
        for (String name : aggregate.getVariablesNames()) {
            varState.add(new AggregateVariableValue(aggregate, name, aggregate.getVariable(name).getValue()));
        }

        for (Place place : aggregate.getPlaces()) {
            if (!place.isEmpty()) {
                placesState.put(place, place.getToken());
            }
        }

        for (Aggregate child : aggregate.getChildAggregates()) {
            saveState(child);
        }
    }
}

class AggregateVariableValue {

    Aggregate aggregate;
    String name;
    Value value;

    public AggregateVariableValue(Aggregate theAggregate, String theName, Value theValue) {
        aggregate = theAggregate;
        name = theName;
        value = theValue;
    }
}
