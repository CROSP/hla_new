package ua.cn.stu.cs.ems.core.aggregates;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import java.util.Set;
import java.util.HashSet;

import ua.cn.stu.cs.ems.core.utils.JessUtils;
import ua.cn.stu.cs.ems.ecli.Value;
import static ua.cn.stu.cs.ems.core.utils.AssertUtils.*;

/**
 * AggregateVariable - class that represent variable in aggregate. It's constructor
 * has default access specificator because it's only make sense to create Variables
 * inside aggregates, but this class is public so any aggregate's user can get variable
 * and set his/her variable listener on it or save link to it.
 * @author proger
 */
public class AggregateVariable {

    final static Logger logger = LoggerFactory.getLogger(AggregateVariable.class);
    private Aggregate parentAggregate;
    private String name;
    private Value value;
    Set<VariableChangedListener> listeners = new HashSet<VariableChangedListener>();

    AggregateVariable(Aggregate aggregate, String name, Value initValue) {
        assertNotNull(name, "Variable's name can't be null");

        parentAggregate = aggregate;
        this.name = name;
        value = initValue;
        logger.info("Value {} set to variable {}", getValue(),
                JessUtils.getAggregateChildFullName(this));
    }

    /**
     * Get variable's name
     * @return variable's name
     */
    public String getName() {
        return name;
    }

    /**
     * Get variable's value
     * @return variable's value
     */
    public final Value getValue() {
        return value;
    }

    /**
     * Set variable's value
     * @param value to set.
     */
    public void setValue(Value value) {
        this.value = value;
        notifyVariableValueChanged();
        logger.info("Value {} set to variable {}", getValue(),
                JessUtils.getAggregateChildFullName(this));
    }

    /**
     * Get parent aggregate of variable
     * @return aggregate of this variable if this variable is a child of any aggregate or
     * null otherwise.
     */
    public Aggregate getParentAggregate() {
        return parentAggregate;
    }

    /**
     * Add listener for this variable.
     * @param listener - listener.
     */
    public void addVariableChangeListener(VariableChangedListener listener) {
        assertNotNull(listener, "Listener can't be null");
        listeners.add(listener);
    }

    private void notifyVariableValueChanged() {
        for (VariableChangedListener listener : listeners) {
            listener.variableValueChanged(this);
        }
    }

    public void removeVariableChangeListener(VariableChangedListener varListener) {
        listeners.remove(varListener);
    }
    
     @Override
    public String toString() {
        return "Variable{" + "name=" + name +  ", value=" + value +"}";
    }
}
