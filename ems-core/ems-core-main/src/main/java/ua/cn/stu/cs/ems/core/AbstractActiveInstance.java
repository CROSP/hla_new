package ua.cn.stu.cs.ems.core;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import ua.cn.stu.cs.ems.core.aggregates.AbstractAggregateChild;
import ua.cn.stu.cs.ems.core.aggregates.Aggregate;
import ua.cn.stu.cs.ems.core.models.Model;

/**
 * Basic implementation of abstract active instance.
 * @author proger
 */
public abstract class AbstractActiveInstance extends AbstractAggregateChild implements ActiveInstance {


    private Set<ActiveInstanceListener> listeners = new HashSet<ActiveInstanceListener>();

    /**
     * Create AbstractActiveInstace with a specified name.
     * @param name - name of new AbstractActiveInstance
     */
    protected AbstractActiveInstance(String name) {
        super(name);
    }

    /**
     * Delay for specified time.
     * It will only work if current AbstractActiveInstance has a parent aggregate
     * and parent aggregate has a model.
     * @param time - time to stay in delay
     */
    protected final void delayFor(double time) {
        final Aggregate aggregate = getParentAggregate();
        if (aggregate == null) {
            return;
        }

        final Model model = aggregate.getModel();
        if (model == null) {
            return;
        }

        if (model.addDelayable(this, time)) {
            notifyDelayStarted();
        }
    }

    /**
     * Create a List<Place> with a single place in it.
     * @param place - a place that will be added to a created list
     * @return list with a single place
     */
    protected final List<Place> createListWithSinglePlace(Place place) {
        final ArrayList<Place> result = new ArrayList<Place>();

        if (place != null) {
            result.add(place);
        }

        return result;
    }

    /**
     * Method that should be called when ActiveInstance's delay is finished.
     */
    public void onDelayFinished() {
        fireTransition();
        notifyDelayFinished();
    }

    /**
     * Execute transition.
     * Implementation of this method should perform model's state change according
     * to an active instance type.
     */
    protected abstract void fireTransition();

    /**
     * Add a listener.
     * @param listener a listener to add.
     */
    public final void addListener(ActiveInstanceListener listener) {
        listeners.add(listener);
    }

    /**
     * Remove a listener.
     * @param listener a listener to remove
     */
    public final void removeListener(ActiveInstanceListener listener) {
        listeners.remove(listener);
    }

    protected void notifyDelayStarted() {
        for (ActiveInstanceListener listener : listeners) {
            listener.instanceDelayStarted(this);
        }
    }

    protected void notifyDelayFinished() {
        for (ActiveInstanceListener listener : listeners) {
            listener.instanceDelayFinished(this);
        }
    }
}
