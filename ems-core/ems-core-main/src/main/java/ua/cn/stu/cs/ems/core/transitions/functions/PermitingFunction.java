package ua.cn.stu.cs.ems.core.transitions.functions;

import java.util.Set;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.aggregates.AggregateVariable;
import ua.cn.stu.cs.ems.core.transitions.SpecialTransition;

/**
 * PermitingFunction - interface for functions that define number of input/output
 * place to get/set token from in X/Y transition.
 * Calculating a result of permitting function can depend on places that doesn't
 * connected to a transition that used by this function or on variables of other
 * aggregates. Interface has special methods to get this dependencies.
 * @author proger
 */
public interface PermitingFunction {

    /**
     * Calculate number of input/output place.
     * @param transition - transition that use this function
     * @return - number of input/output place
     */
    Integer getPlaceNumber(SpecialTransition transition);

    /**
     * Get set of places that are used to calculate result of this function.
     * Input/output places of transition should be in dependencies.
     * @param transition - transition that use this function
     * @return set of place dependencies
     */
    Set<Place> getPlaceDependencies(SpecialTransition transition);

    /**
     * Get set of variables that are used to calculate result of this function.
     * @param transition - transition that is uses this permiting function
     * @return set of variable dependencies
     */
    Set<AggregateVariable> getVariableDependencies(SpecialTransition transition);

    /**
     * Create a copy of this permitting function.
     * @return copy of this permitting function
     */
    PermitingFunction copy();
}
