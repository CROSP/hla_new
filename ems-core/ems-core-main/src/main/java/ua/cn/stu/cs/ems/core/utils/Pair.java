package ua.cn.stu.cs.ems.core.utils;

/**
 * Class for a pair of values of any type.
 * @author proger
 */
public class Pair<F, S> {
    private F first;
    private S second;

    /**
     * Create pair of two elements.
     * @param theFirst - first element in a pair
     * @param theSecond - second element in pair
     */
    public Pair(F theFirst, S theSecond) {
        first = theFirst;
        second = theSecond;
    }

    /**
     * @return the first
     */
    public final F getFirst() {
        return first;
    }

    /**
     * @return the second
     */
    public final S getSecond() {
        return second;
    }
}
