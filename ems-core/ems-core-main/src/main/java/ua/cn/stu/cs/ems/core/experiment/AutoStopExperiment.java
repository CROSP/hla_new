/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.cn.stu.cs.ems.core.experiment;

import java.util.ArrayList;
import java.util.Iterator;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.aggregates.AggregateChild;
import ua.cn.stu.cs.ems.core.experiment.statistics.*;
import ua.cn.stu.cs.ems.core.models.Model;
import ua.cn.stu.cs.ems.core.queues.Queue;
import ua.cn.stu.cs.ems.core.transitions.Transition;

/**
 *
 * @author Dmitry Krivenko <dmitrykrivenko@gmal.com>
 */
public class AutoStopExperiment extends AbstractExperiment {

    private int DEFAULT_NUM_OF_RUNS = 30;
    // Number of times model should be run with each value of a factor    
    private int numberOfRuns;
    private double confidenceInterval;
    private double confidenceProbability;
    private boolean firstRun = true;
    // Respond variable for this experiment
    private Respond respondAutoStop;
    // Respond collector
    private RespondCollector respondCollectorAutoStop;
    // Collector for autostop
    private StatisticsCollector autostopStatisticsCollector;
    //Array of respond collectors for result of autostop
   // private ArrayList<RespondCollector> respondCollectors = new ArrayList<RespondCollector>();

    /**
     * Create AutoStopExperiment. This constructor should be used when secondary statistics should be collected.
     * @param model - model to run experiment for.
     * @param factor - factor of an experiment.
     * @param respond - variable or  average numerical characteristic that will be used to get respond of the factor changes.
     * @param respondAutostop - variable or  average numerical characteristic that will be used to get respond for the autostop.
     * @param instance - object whose average numerical characteristic will be used as a response for the autostop.
     * @param interval - confidence interval.
     * @param probability - confidence probability.
     */
    public AutoStopExperiment(Model model, Factor factor, Respond respond,
            Respond respondAutostop, AggregateChild instance, double interval,
            double probability) {
        super(model, factor, respond);
        respondAutoStop = respondAutostop;
        confidenceInterval = interval;
        confidenceProbability = probability;
        init(instance);
    }

    /**
     * Create AutoStopExperiment. This constructor should be used when secondary statistics should NOT be collected for 
     * several factor values.
     * @param model - model to run experiment for.
     * @param factor - factor of an experiment.
     * @param respondAutostop - variable or  average numerical characteristic that will be used to get respond for the autostop.
     * @param instance - object whose average numerical characteristic will be used as a response for the autostop.
     * @param interval - confidence interval.
     * @param probability - confidence probability.
     */
    public AutoStopExperiment(Model model, Factor factor, Respond respondAutostop,
            AggregateChild instance, double interval, double probability) {
        super(model, factor);
        respondAutoStop = respondAutostop;
        confidenceInterval = interval;
        confidenceProbability = probability;
        init(instance);
    }

    /**
     * Create AutoStopExperiment. This constructor should be used when secondary statistics should NOT be collected  
     * WITHOUT using factor.
     * @param model - model to run experiment for.
     * @param respondAutostop - variable or  average numerical characteristic that will be used to get respond for the autostop.
     * @param instance - object whose average numerical characteristic will be used as a response for the autostop.
     * @param interval - confidence interval.
     * @param probability - confidence probability.
     */
    public AutoStopExperiment(Model model, Respond respondAutostop, AggregateChild instance,
            double interval, double probability) {
        super(model);
        respondAutoStop = respondAutostop;
        confidenceInterval = interval;
        confidenceProbability = probability;
        init(instance);
    }

    @Override
    protected void run() {
        if (firstRun) {
            respondCollectorAutoStop.startStatCollection();
            firstRun = false;
        }
        startAutostopStatCollection();
        getModel().start();
        stopAutostopStatCollection();
        numberOfRuns++;
    }

    @Override
    protected void runWithStep() {
        getModel().step();
    }

    @Override
    protected boolean lastRun() {
        if (numberOfRuns < DEFAULT_NUM_OF_RUNS) {
            return false;
        }
        boolean result = confidenceInterval > confidenceInterval();
        if (result) {
            respondCollectorAutoStop.stopStatCollection();
            /*  RespondCollector resp = new RespondCollector(respondCollectorAutoStop);
            respondCollectors.add(resp);*/
            firstRun = true;
        }
        return result;
    }

    @Override
    protected void specificReset() {
        numberOfRuns = 0;
    }

    private double confidenceInterval() {
        double dn = 0;
        if (confidenceProbability == 0.9d) {
            dn = 1.64 * Math.sqrt(staticCorrectedDispersion() / numberOfRuns);
        } else if (confidenceProbability == 0.95d) {
            dn = 1.96 * Math.sqrt(staticCorrectedDispersion() / numberOfRuns);
        } else if (confidenceProbability == 0.99d) {
            dn = 2.58 * Math.sqrt(staticCorrectedDispersion() / numberOfRuns);
        }
        return dn;
    }

    private void setAutoStopRespond() {
        //If respond is not variable
        if (autostopStatisticsCollector != null) {
            Iterator<StatisticsParameters> it = autostopStatisticsCollector.getStatisticsParameters().iterator();
            while (it.hasNext()) {
                StatisticsParameters param = it.next();
                String paramName = param.getPresentation();
                if (respondAutoStop.getName().equals(paramName)) {
                    respondAutoStop.setValue(autostopStatisticsCollector.getResult().getResultValue(param));
                    break;
                }
            }
        }
        respondCollectorAutoStop.updateStat((Double) respondAutoStop.getValue());
    }

    private double staticCorrectedDispersion() {
        double result = 0;
        double expectation = mathExpectation();
        ArrayList<Double> respondValues = (ArrayList<Double>) respondCollectorAutoStop.getValuesOfRespond();
        for (int i = 0; i < respondValues.size(); i++) {
            result += Math.pow(respondValues.get(i) - expectation, 2);
        }
        return result / (numberOfRuns - 1);
    }

    private double mathExpectation() {
        double result = 0;
        ArrayList<Double> respondValues = (ArrayList<Double>) respondCollectorAutoStop.getValuesOfRespond();
        for (int i = 0; i < respondValues.size(); i++) {
            result += respondValues.get(i);
        }
        result = result / numberOfRuns;
        return result;
    }

    private void init(AggregateChild instance) {
        numberOfRuns = 0;
        respondCollectorAutoStop = new RespondCollector();
        if (instance != null) {
            if (instance instanceof Place) {
                collectedAutostopStatisticsOn((Place) instance);
            } else if (instance instanceof Transition) {
                collectedAutostopStatisticsOn((Transition) instance);
            } else if (instance instanceof Queue) {
                collectedAutostopStatisticsOn((Queue) instance);
            }
        }
    }

    private void collectedAutostopStatisticsOn(Place place) {
        autostopStatisticsCollector = new PlaceStatisticsCollector(getModel(), place);
    }

    private void collectedAutostopStatisticsOn(Transition transition) {
        autostopStatisticsCollector = new TransitionStatisticsCollector(getModel(), transition);
    }

    private void collectedAutostopStatisticsOn(Queue queue) {
        autostopStatisticsCollector = new QueueStatisticsCollector(getModel(), queue);
    }

    private void startAutostopStatCollection() {
        if (autostopStatisticsCollector != null) {
            autostopStatisticsCollector.startStatCollection();
        }
    }

    private void stopAutostopStatCollection() {
        if (autostopStatisticsCollector != null) {
            autostopStatisticsCollector.stopStatCollection();
        }
        setAutoStopRespond();
    }

    /*public RespondCollector getAutoStopResult() {
    RespondCollector result = new RespondCollector();
    for(int i=0; i<respondCollectors.size(); i++){
    
    }
    }*/
}
