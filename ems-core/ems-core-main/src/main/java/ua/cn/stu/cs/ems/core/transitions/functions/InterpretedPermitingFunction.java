package ua.cn.stu.cs.ems.core.transitions.functions;

import ua.cn.stu.cs.ems.core.InterpretedFunction;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import org.codehaus.plexus.util.StringInputStream;
import ua.cn.stu.cs.ems.core.JessException;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.aggregates.Aggregate;
import ua.cn.stu.cs.ems.core.aggregates.AggregateVariable;
import ua.cn.stu.cs.ems.core.ecli.EcliAggregateAccessor;
import ua.cn.stu.cs.ems.core.ecli.EcliModelAccessor;
import ua.cn.stu.cs.ems.core.models.Model;
import ua.cn.stu.cs.ems.core.transitions.SpecialTransition;
import ua.cn.stu.cs.ems.ecli.EclInterpreter;
import ua.cn.stu.cs.ems.ecli.EcliAggregate;
import ua.cn.stu.cs.ems.ecli.EcliToken;
import ua.cn.stu.cs.ems.ecli.InterpretationReport;
import ua.cn.stu.cs.ems.ecli.ModelAccessor;
import ua.cn.stu.cs.ems.ecli.Value;

/**
 * InterpretedPermitingFunction - function that calculate input/output place to
 * get/put token to/from according to algorithm defined by ecli code.
 * @author proger
 */
public class InterpretedPermitingFunction extends InterpretedFunction implements PermitingFunction {

    private EclInterpreter ei = new EclInterpreter();
    private Set<Place> placeDependencies;
    private Set<AggregateVariable> variableDependencies;

    public InterpretedPermitingFunction(String source) {
        super(source);
    }

    @Override
    public Integer getPlaceNumber(SpecialTransition transition) {
        try {
            ModelAccessor ma = new EcliModelAccessor(transition.getParentAggregate().getModel(), transition.getParentAggregate());
            InterpretationReport ir = ei.interpret(new StringInputStream(source), ma);
            if (ir.getResult() != null) {
                return ir.getResult().realValue().intValue();
            } else {
                return -1;
            }
        } catch (IOException ex) {
            throw new JessException("Exception during interpreted permiting function execution", ex);
        }
    }

    public Set<Place> getPlaceDependencies(SpecialTransition transition) {
        if (placeDependencies == null) {
            collectDependencies(transition);
        }
        return new HashSet<Place>(placeDependencies);
    }

    public PermitingFunction copy() {
        return new InterpretedPermitingFunction(source);
    }

    public Set<AggregateVariable> getVariableDependencies(SpecialTransition transition) {
        if (variableDependencies == null) {
            collectDependencies(transition);
        }
        return new HashSet<AggregateVariable>(variableDependencies);
    }

    // To calculate dependencies we will call EcliInterpreter.validate and
    // save all places it appeal to during validation
    private void collectDependencies(SpecialTransition transition) throws JessException {
        placeDependencies = new HashSet<Place>();
        variableDependencies = new HashSet<AggregateVariable>();

        Model model = transition.getParentAggregate().getModel();
        Aggregate currentAggregate = transition.getParentAggregate();
        // Create decorator to save all dependencies
        EcliSourceDependenciesModelDecorator esdmd = new EcliSourceDependenciesModelDecorator(new EcliModelAccessor(model, currentAggregate), new EcliAggregateAccessor(currentAggregate));
        try {
            ei.validate(new StringInputStream(source), esdmd);
        } catch (IOException ex) {
            throw new JessException("Exception during getting dependencies for interpreted permiting function", ex);
        }

        // Special transition's input and output places aren't dependencies
        // we need to collect. Let's delete them from result set
        dependenciesClean:
        for (Iterator<Place> it = placeDependencies.iterator(); it.hasNext();) {
            Place depPlace = it.next();
            if (transition.getInputPlaces().contains(depPlace) || transition.getOutputPlaces().contains(depPlace)) {
                it.remove();
                continue dependenciesClean;
            }
        }
    }

    private class EcliSourceDependenciesAggregateDecorator implements EcliAggregate {

        EcliAggregateAccessor aggregateAccessor;

        public EcliSourceDependenciesAggregateDecorator(EcliAggregateAccessor accessor) {
            aggregateAccessor = accessor;
        }

        public boolean containsVar(Object key) {
            variableDependencies.add(aggregateAccessor.getAggregateVariable(key));
            return aggregateAccessor.containsVar(key);
        }

        public boolean containsPlace(Object place) {
            placeDependencies.add(aggregateAccessor.getPlace(place));
            return aggregateAccessor.containsPlace(place);
        }

        public boolean isPlaceMarked(Object place) {
            placeDependencies.add(aggregateAccessor.getPlace(place));
            return aggregateAccessor.isPlaceMarked(place);
        }

        public boolean containsAggregate(Object aggregate) {
            return aggregateAccessor.containsAggregate(aggregate);
        }

        public Object getVariable(Object key) {
            variableDependencies.add(aggregateAccessor.getAggregateVariable(key));
            return aggregateAccessor.getVariable(key);
        }

        public boolean setVariable(Object key, Value value) {
            variableDependencies.add(aggregateAccessor.getAggregateVariable(key));
            return aggregateAccessor.setVariable(key, value);
        }

        public EcliToken getTokenAt(Object place) {
            placeDependencies.add(aggregateAccessor.getPlace(place));
            return aggregateAccessor.getTokenAt(place);
        }

        public EcliAggregate getAggregate(Object aggregate) {
            return new EcliSourceDependenciesAggregateDecorator(aggregateAccessor.getAggregate(aggregate));
        }
    }

    private class EcliSourceDependenciesModelDecorator extends EcliSourceDependenciesAggregateDecorator implements ModelAccessor {

        EcliModelAccessor modelAccessor;
        EcliAggregateAccessor currentAggregate;

        public EcliSourceDependenciesModelDecorator(EcliModelAccessor modelAccessor, EcliAggregateAccessor currentAggregate) {
            super(modelAccessor.getRootAggregate());

            this.modelAccessor = modelAccessor;
            this.currentAggregate = currentAggregate;
        }

        public double getTime() {
            return modelAccessor.getTime();
        }

        public EcliToken getCurrentToken() {
            return modelAccessor.getCurrentToken();
        }

        public EcliAggregate getCurrentAggregate() {
            return new EcliSourceDependenciesAggregateDecorator(currentAggregate);
        }

        public EcliAggregate getRootAggregate() {
            return new EcliSourceDependenciesAggregateDecorator(modelAccessor.getRootAggregate());
        }
    }
}
