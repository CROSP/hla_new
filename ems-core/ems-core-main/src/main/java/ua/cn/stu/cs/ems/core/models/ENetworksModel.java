package ua.cn.stu.cs.ems.core.models;

import java.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.cn.stu.cs.ems.core.ActiveInstance;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.PlaceStateChangedListener;
import ua.cn.stu.cs.ems.core.aggregates.*;
import ua.cn.stu.cs.ems.core.transitions.SpecialTransition;
import ua.cn.stu.cs.ems.core.transitions.Transition;
import ua.cn.stu.cs.ems.core.utils.JessUtils;

/**
 * Model that is used for non-distributed modeling.
 *
 * @author Dmitry Krivenko <dmitrykrivenko@gmal.com>
 */
public class ENetworksModel extends AbstractModel {

    private static final Logger logger = LoggerFactory.getLogger(ENetworksModel.class);
    private boolean initEModel = false;
    private double prevModelTime = -1;
    private double modelingTime;
    HashMap<Place, List<SpecialTransition>> placeDependents = new HashMap<Place, List<SpecialTransition>>();
    HashMap<AggregateVariable, List<SpecialTransition>> variableDependents = new HashMap<AggregateVariable, List<SpecialTransition>>();

    public ENetworksModel() {
    }

    public void setModelingTime(double time) {
        // Save removed timer here, because removed timer can add itself
        // in timers list again and removing with help of iterator
        // will cause ConcurentModificationException
        ArrayList<ModelTimer> timersToRemove = new ArrayList<ModelTimer>();

        for (int i = 0; i < timers.size(); i++) {
            ModelTimer mt = timers.get(i);
            if (mt.time <= time) {
                modelingTime = mt.time;
                mt.listener.modelTimeChanged(this, getModelingTime());
                timersToRemove.add(mt);
                notifyModelTimeChanged();
            } else {
                break;
            }
        }

        for (ModelTimer mt : timersToRemove) {
            timers.remove(mt);
        }

        modelingTime = time;
        notifyModelTimeChanged();
    }

    public double getModelingTime() {
        return modelingTime;
    }

    protected boolean isStopConditions() {

        if (getDelayedQueue().isEmpty() || isInterrupted()) {
            return true;
        }

        return false;
    }

    public void start() {
        start(0);
    }

    public void start(int delay) {
        long time = System.currentTimeMillis();
        notifyModelStarted();
        setModelingTime(0);
        logger.debug("Begin  of model initializatioin:");
        initializeModel();
        logger.debug("End of model initializatioin");

        double prevModelingTime = -1;

        while (!isStopConditions()) {
            if (!getDelayedQueue().isEmpty() && getSpecialTransitionSet().isEmpty() && !isInterrupted()) {
                double newModelingTime = getDelayedQueue().getFirstDelayableTime();
                if (getFinishTime() > 0 && newModelingTime > getFinishTime()) {
                    if (prevModelingTime != getFinishTime()) {
                        logger.debug("Modeling time {}:", getFinishTime());
                    }
                    setModelingTime(getFinishTime());
                    break;
                } else {
                    if (prevModelingTime != newModelingTime) {
                        logger.debug("Modeling time {}:", newModelingTime);
                        prevModelingTime = newModelingTime;
                    }
                    setModelingTime(newModelingTime);
                }
            }

            while (moreDelayablesOfCurrentPriority() && !isInterrupted()) {
                ActiveInstance ai = getDelayedQueue().getFirstDelayedInstance();

                if (ai instanceof SpecialTransition) {
                    currentDelayedSpecialTransitions.remove((SpecialTransition) ai);
                }

                logger.debug("Activation of Object {}: ", JessUtils.getAggregateChildFullName(ai));

                ai.onDelayFinished();

                logger.debug("Object {} activated", JessUtils.getAggregateChildFullName(ai));
            }

            if (!getSpecialTransitionSet().isEmpty()) {
                int delayQueueSize = getDelayedQueue().getSize();
                while (getDelayedQueue().getSize() <= delayQueueSize
                        && !getSpecialTransitionSet().isEmpty()) {
                    SpecialTransition firstSpecialTransition = getSpecialTransition();

                    firstSpecialTransition.fireIfPosible();
                    removeSpecialTransition(firstSpecialTransition);
                }
            }
        }

        notifyModelFinished();        
        logger.debug("Time execution: {}s", String.valueOf((float) (System.currentTimeMillis() - time) / 1000));
    }

    public void step() {
        initEModel();
        if (!isStopConditions()) {

            if (!getDelayedQueue().isEmpty() && getSpecialTransitionSet().isEmpty()) {
                double newModelingTime = getDelayedQueue().getFirstDelayableTime();
                if (getFinishTime() > 0 && newModelingTime > getFinishTime()) {
                    if (prevModelTime != getFinishTime()) {
                        logger.info("Modeling time {}:", getFinishTime());
                    }
                    setModelingTime(getFinishTime());
                    notifyModelFinished();
                    setInitModel(false);
                    return;
                } else {
                    if (prevModelTime != newModelingTime) {
                        logger.info("Modeling time {}:", newModelingTime);
                        prevModelTime = newModelingTime;
                    }
                    setModelingTime(newModelingTime);
                }
            }

            if (moreDelayablesOfCurrentPriority()) {
                ActiveInstance ai = getDelayedQueue().getFirstDelayedInstance();

                if (ai instanceof SpecialTransition) {
                    currentDelayedSpecialTransitions.remove((SpecialTransition) ai);
                }

                logger.info("Activation of Object {}: ", JessUtils.getAggregateChildFullName(ai));

                ai.onDelayFinished();

                logger.info("Object {} activated", JessUtils.getAggregateChildFullName(ai));
            }

            if (!getSpecialTransitionSet().isEmpty()) {
                int delayQueueSize = getDelayedQueue().getSize();
                while (getDelayedQueue().getSize() <= delayQueueSize
                        && !getSpecialTransitionSet().isEmpty()) {
                    SpecialTransition firstSpecialTransition = getSpecialTransition();

                    firstSpecialTransition.fireIfPosible();
                    removeSpecialTransition(firstSpecialTransition);
                }
            }
        } else {
            notifyModelFinished();
            setInitModel(false);
        }
    }

    @Override
    protected void specializedReset() {
        modelingTime = 0;

        // Removing place listeners
        for (Place p : placeDependents.keySet()) {
            p.delStateListener(placeListener);
        }
        placeDependents.clear();

        // Removing variables listeners
        for (AggregateVariable v : variableDependents.keySet()) {
            v.removeVariableChangeListener(varListener);
        }
        variableDependents.clear();
    }

    /**
     * This method helps to analyse is there any delayable in delayableQueue
     * that can be awaken. We do it according to following rules: -- Any
     * transition can be awaken when it delay finished. -- Input or output can
     * be awaken when no transitions are waiting in delay queue or in
     * specialDelayedQueue
     *
     * @return
     */
    protected boolean moreDelayablesOfCurrentPriority() {

        if (getDelayedQueue().isEmpty()) {
            return false;
        }

        if (getDelayedQueue().getFirstDelayableTime() != getModelingTime()) {
            return false;
        }

        if (getDelayedQueue().getFirstDelayablePriority() != DelayedQueue.TRANSITION_QUEUE_PRIORITY
                && !getSpecialTransitionSet().isEmpty()) {
            return false;
        }


        return true;
    }

    protected void initializeModel() {
        getDelayedQueue().clear();

        // List to save all special positions in all aggregates
        List<SpecialTransition> specialTransitions = new ArrayList<SpecialTransition>();

        initalizeDelayables(getRootAggregate(), specialTransitions);

        // Save all special transitions delayables
        for (SpecialTransition sp : specialTransitions) {
            for (Place dependency : sp.getPermitingFunction().getPlaceDependencies(sp)) {
                List<SpecialTransition> transitionList = placeDependents.get(dependency);
                transitionList.add(sp);
            }

            for (AggregateVariable dependency : sp.getPermitingFunction().getVariableDependencies(sp)) {
                List<SpecialTransition> transitionList = variableDependents.get(dependency);
                transitionList.add(sp);
            }
        }
    }

    /**
     * Initialize all transtions, inputs and outputs in aggregate and in all
     * it's child aggregates
     *
     * @param aggregate - aggregate to initialize
     * @param specialTransition - list to collect all special transitions to
     */
    protected void initalizeDelayables(Aggregate aggregate, List<SpecialTransition> specialTransition) {
        // Initialize transitions
        for (Transition t : aggregate.getTransitions()) {
            t.fireIfPosible();
            if (t instanceof SpecialTransition) {
                specialTransition.add((SpecialTransition) t);
            }
        }

        // Save places to dependency hash
        for (Place p : aggregate.getPlaces()) {
            List<SpecialTransition> emptyList = new ArrayList<SpecialTransition>();
            placeDependents.put(p, emptyList);
            p.addStateChangeListener(placeListener);
        }

        for (String varName : aggregate.getVariablesNames()) {
            AggregateVariable var = aggregate.getVariable(varName);
            List<SpecialTransition> emptyList = new ArrayList<SpecialTransition>();

            variableDependents.put(var, emptyList);
            var.addVariableChangeListener(varListener);
        }

        // Initialize outputs
        for (Output o : aggregate.getOutputs()) {
            o.fireIfPosible();
        }

        // Initialize inputs
        for (Input i : aggregate.getInputs()) {
            i.fireIfPosible();
        }

        // Do the same for all child aggregates
        for (Aggregate child : aggregate.getChildAggregates()) {
            initalizeDelayables(child, specialTransition);
        }

    }

    /**
     * Get one special transition from specialTransitionSet
     *
     * @return special transition
     */
    protected SpecialTransition getSpecialTransition() {
        Set<SpecialTransition> spSet = getSpecialTransitionSet();
        Iterator<SpecialTransition> iterator = spSet.iterator();

        return iterator.next();
    }

    /**
     * remove transition from specialTransitionSet
     *
     * @param firstSpecialTransition
     */
    protected void removeSpecialTransition(SpecialTransition firstSpecialTransition) {
        getSpecialTransitionSet().remove(firstSpecialTransition);
    }
    /**
     * Listener that listenes to all changes in positions in model. If any of
     * places state changed we look if any special transition depend on this
     * place. If we find one we add them in specialTransitionSet to check latter
     * if it can fire.
     */
    PlaceStateChangedListener placeListener = new PlaceStateChangedListener() {

        public void placeStateChanged(Place place) {
            List<SpecialTransition> transitions = placeDependents.get(place);

            for (SpecialTransition st : transitions) {
                addSpecialTransition(st);
            }
        }
    };
    VariableChangedListener varListener = new VariableChangedListener() {

        public void variableValueChanged(AggregateVariable var) {
            List<SpecialTransition> transitions = variableDependents.get(var);

            if (transitions != null) {
                for (SpecialTransition st : transitions) {
                    addSpecialTransition(st);
                }
            }
        }
    };

    protected void initEModel() {
        if (!initEModel) {
            notifyModelStarted();
            setModelingTime(0);
            logger.debug("Begin  of model initializatioin:");
            initializeModel();
            logger.debug("End of model initializatioin");
            setInitModel(true);
        }
    }

    public void setInitModel(boolean init) {
        initEModel = init;
    }
}
