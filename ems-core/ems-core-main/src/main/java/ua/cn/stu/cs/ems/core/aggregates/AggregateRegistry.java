
package ua.cn.stu.cs.ems.core.aggregates;

import java.util.ArrayList;
import java.util.List;

import static ua.cn.stu.cs.ems.core.utils.AssertUtils.*;

/**
 * AggregateRegistry - registry to hold all aggregate definitions that can be used
 * by the user.
 * @author proger
 */
public class AggregateRegistry {

    private List<AggregateDefinition> definitions = new ArrayList<AggregateDefinition>();

    /**
     * Add aggregate definition to the registry.
     * @param aggregateDefinition - definition to add
     */
    public void addAggregateDefinition(AggregateDefinition aggregateDefinition) {
        assertNotNull(aggregateDefinition, "Aggregate definition can't be null");

        definitions.add(aggregateDefinition);
    }

    /**
     * Get aggregate definition by it's name.
     * @param name - aggregate definition name
     * @return aggregate definition if one with specified name exists or null otherwise.
     */
    public AggregateDefinition getAggregateDefinition(String name) {
        assertNotNull(name, "Name can't be null");

        for (AggregateDefinition definition : definitions) {
            if (definition.getName().equals(name)) {
                return definition;
            }
        }

        return null;
    }

    public List<AggregateDefinition> getAggregateDefinitions() {
        return new ArrayList<AggregateDefinition>(definitions);
    }
}
