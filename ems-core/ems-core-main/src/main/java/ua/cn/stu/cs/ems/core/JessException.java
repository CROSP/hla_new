package ua.cn.stu.cs.ems.core;

/**
 * Exception that is thrown in some fatal situation.
 * @author proger
 */
public class JessException extends RuntimeException {
    public JessException() {
        super();
    }

    /**
     * Create exception with a specified message.
     * @param msg - message for a new exception.
     */
    public JessException(String msg) {
        super(msg);
    }

    /**
     * Create exception when it was caused by another throwable.
     * @param throwable - cause of a new exception.
     */
    public JessException(Throwable throwable) {
        super(throwable);
    }

    /**
     * Create exception with a message and a cause.
     * @param msg - message for a new exception.
     * @param throwable - cause of a new exception.
     */
    public JessException(String msg, Throwable throwable) {
        super(msg, throwable);
    }
}
