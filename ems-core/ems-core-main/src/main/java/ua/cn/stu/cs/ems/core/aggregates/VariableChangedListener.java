package ua.cn.stu.cs.ems.core.aggregates;

/**
 * VariableChangedListener - lister that is notified every time variable's value
 * is changed.
 * @author proger
 */
public interface VariableChangedListener {

    /**
     * Method that is called when variable's value was changed.
     * @param var - variable which value was changed.
     */
    void variableValueChanged(AggregateVariable var);
}
