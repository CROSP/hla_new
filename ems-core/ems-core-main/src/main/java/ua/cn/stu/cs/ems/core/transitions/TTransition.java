package ua.cn.stu.cs.ems.core.transitions;

import ua.cn.stu.cs.ems.core.AbstractTransition;
import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.Place;

/**
 * Class TTransition
 */
public class TTransition extends AbstractTransition {

  
    public TTransition(String name) {
        super(name);
    }

    /**
     * @param        place
     */
    public void setInputPlace(Place place) {
        if (numberOfInputPlaces() == 0)
            addInputPlace(place, 0);
        else
            replaceInputPlace(place, 0);
    }

    /**
     * @param        place
     */
    public void setOutputPlace(Place place) {
        if (numberOfOutputPlaces() == 0)
            addOutputPlace(place, 0);
        else
            replaceOutputPlace(place, 0);
    }

    protected void fireTransition() {
        Place inputPlace = getInputPlace();
        Place outputPlace = getOutputPlace();

        Token token = inputPlace.getToken();
        inputPlace.setToken(null);

        Token resultToken = getTransformationFunction().transform(this, new Token(token));
        outputPlace.setToken(resultToken);
    }

    @Override
    protected boolean canFire() {

       return (! getInputPlace().isEmpty()) && (getOutputPlace().isEmpty());
    }

    public Place getOutputPlace() {
        return getOutputPlaces().get(0);
    }

    public Place getInputPlace() {
        return getInputPlaces().get(0);
    }

    @Override
    protected int maxInputSize() {
        return 1;
    }

    @Override
    protected int maxOutputSize() {
        return 1;
    }

}
