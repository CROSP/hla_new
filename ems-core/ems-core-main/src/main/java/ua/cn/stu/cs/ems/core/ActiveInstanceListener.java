package ua.cn.stu.cs.ems.core;

/**
 * Listener for events that occurs with active instance.
 * @author proger
 */
public interface ActiveInstanceListener {

    /**
     * This method is called when active instance is set into delay.
     * @param instance - instance that was set into delay.
     */
    void instanceDelayStarted(AbstractActiveInstance instance);

    /**
     * This method is called when active instance's delay is finished.
     * @param instance - instance which delay is finished.
     */
    void instanceDelayFinished(AbstractActiveInstance instance);
}
