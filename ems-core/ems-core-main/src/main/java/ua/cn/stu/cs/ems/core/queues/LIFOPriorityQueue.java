package ua.cn.stu.cs.ems.core.queues;

/**
 *
 * @author proger
 */
public class LIFOPriorityQueue extends AbstractPriorityQueue {

    public LIFOPriorityQueue(String name, TokenPriorityFunction tpf) {
        super(name, tpf);
    }

    @Override
    protected int getPriorityForQueue(int priority) {
        return +priority;
    }

}
