package ua.cn.stu.cs.ems.core.transitions.functions;

import ua.cn.stu.cs.ems.core.InterpretedFunction;
import java.io.IOException;
import org.codehaus.plexus.util.StringInputStream;
import ua.cn.stu.cs.ems.core.JessException;
import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.ecli.EcliModelAccessor;
import ua.cn.stu.cs.ems.core.transitions.Transition;
import ua.cn.stu.cs.ems.ecli.EclInterpreter;
/**
 * InterpretedTransformationFunction - class to transform token due to an algorithm
 * defined by ecli language.
 * @author proger
 */
public class InterpretedTransformationFunction extends InterpretedFunction implements TransformationFunction {

    EclInterpreter ei = new EclInterpreter();
    
    /**
     * Create interpreted transformation function, that uses specified source code to calculate it's result.
     * @param source - source that is used for calculation
     */
    public InterpretedTransformationFunction(String source) {
        super(source);
    }

    public final Token transform(Transition transition, Token token) {
        try {
            final EcliModelAccessor ema = new EcliModelAccessor(transition.getParentAggregate().getModel(), 
                                                          transition.getParentAggregate());
            ema.setCurrentToken(token);
            ei.interpret(new StringInputStream(source), ema);
            return token;
        } 
        catch (IOException ex) {
            throw new JessException("IOException exception during execution of interpreted transformation function", 
                                    ex);
        }
    }

    public final TransformationFunction copy() {
        return new InterpretedTransformationFunction(source);
    }

}
