package ua.cn.stu.cs.ems.core.aggregates;

import java.util.List;

/**
 * If output is connected to several inputs, some inputs can be connected to
 * tokens that aren't empty, so new token can't be set to this input. Implementations
 * of this interface should specify what to do in this situation: set new token to
 * all "settable" inputs, not set anything, set token to a first "settable" input etc.
 * @author proger
 */
public interface ConflictResolutionStrategy {
    /**
     * Returns list of the inputs that are "settable" to set token to them
     * @param output - output which inputs should be analyzed by this strategy.
     * @return list of inputs.
     */
    List<Input> getInputsToSendToken(Output output);
}
