package ua.cn.stu.cs.ems.core;

/**
 * Global constants for JESS core.
 * @author proger
 */
public final class Constants {

    // Extension of files with aggregate definitions
    public static final String AGGREGATE_PNML_EXTENSION = "aggregateType";
    // Extension of files with model definitions
    public static final String MODEL_XML_EXTENSION = "model";
    
    // Utility classes should not have a public or default constructor.
    private Constants() {
    }
}
