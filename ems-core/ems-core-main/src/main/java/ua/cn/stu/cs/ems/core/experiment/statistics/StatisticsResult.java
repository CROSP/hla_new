package ua.cn.stu.cs.ems.core.experiment.statistics;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import static ua.cn.stu.cs.ems.core.utils.AssertUtils.assertNotNull;

/**
 * Container for result of statistics collectors
 * @author proger
 */
public class StatisticsResult {

    HashMap<StatisticsParameters, Double> values = new HashMap<StatisticsParameters, Double>();

    public StatisticsResult() {
    }

    public final void setResultValue(StatisticsParameters key, Double value) {
        assertNotNull(key, "Statistics result key can't be null");
        assertNotNull(value, "Statistics value can't be null");

        values.put(key, value);
    }

    public final Double getResultValue(StatisticsParameters key) {
        return values.get(key);
    }

    public final Set<StatisticsParameters> getKeys() {
        HashSet<StatisticsParameters> result = new HashSet<StatisticsParameters>(values.keySet());

        return result;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 17 * hash + (this.values != null ? this.values.hashCode() : 0);
        return hash;
    }
    
    @Override
    public final boolean equals(Object obj) {
        if (obj instanceof StatisticsResult) {
            StatisticsResult other = (StatisticsResult) obj;
            
            return this.values.equals(other.values);
        }
        
        return false;
    }
    
    @Override
    public final String toString() {
        String result = "{ ";//"StatisticsResult { ";
        
        for (StatisticsParameters param : values.keySet()) {
            result += param.toString() + " " + values.get(param) + "; ";
        }
        
        result += " }";
        
        return result;
    }
}