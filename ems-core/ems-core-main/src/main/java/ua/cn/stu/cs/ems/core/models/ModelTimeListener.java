package ua.cn.stu.cs.ems.core.models;

/**
 *
 * @author proger
 */
public interface ModelTimeListener {

    /**
     * Model time changed
     * @param model
     */
    void modelTimeChanged(Model model, double newModelTime);

}
