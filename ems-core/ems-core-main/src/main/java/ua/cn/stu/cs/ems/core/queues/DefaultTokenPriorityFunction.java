package ua.cn.stu.cs.ems.core.queues;

import ua.cn.stu.cs.ems.core.Token;

/**
 * Default token priority function that assumes that all tokens have equal priority.
 * @author proger
 */
public class DefaultTokenPriorityFunction implements TokenPriorityFunction {

    public int calculateTokenPriority(Queue queue, Token token) {
        return 0;
    }

}
