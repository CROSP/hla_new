package ua.cn.stu.cs.ems.core.ecli;

import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.aggregates.Aggregate;
import ua.cn.stu.cs.ems.core.aggregates.AggregateVariable;
import ua.cn.stu.cs.ems.ecli.Value;

import static ua.cn.stu.cs.ems.core.utils.AssertUtils.assertNotNull;

/**
 * Accessor that gives ability ecli interpreter to access state of an aggregate.
 * @author proger
 */
public class EcliAggregateAccessor implements ua.cn.stu.cs.ems.ecli.EcliAggregate {

    Aggregate aggregate;

    /**
     * Create ecli accesor of a specified aggregate.
     */
    public EcliAggregateAccessor(Aggregate theAggregate) {
        assertNotNull(theAggregate, "Backend aggregate can't be null");
        aggregate = theAggregate;
    }

    public boolean containsPlace(Object place) {
        return aggregate.getPlace(String.valueOf(place)) != null;
    }

    public boolean isPlaceMarked(Object place) {
        Place p = aggregate.getPlace(String.valueOf(place));
        return !p.isEmpty();
    }

    public boolean containsAggregate(Object aggregate) {
        return this.aggregate.getAggregate(String.valueOf(aggregate)) != null;
    }

    public boolean containsVar(Object key) {
        return aggregate.getVariable(String.valueOf(key)) != null;
    }

    public Object getVariable(Object key) {
        return aggregate.getVariable(String.valueOf(key)).getValue();
    }

    public boolean setVariable(Object key, Value value) {
        try {
            aggregate.setVariable(String.valueOf(key), value);
        } 
        catch (Exception ex) {
            return false;
        }

        return true;
    }

    public EcliTokenAccessor getTokenAt(Object place) {
        Place p = aggregate.getPlace(String.valueOf(place));
        if (null != p) {
            return wrap(p.getToken());
        } 
        else {
            return null;
        }
    }

    public EcliAggregateAccessor getAggregate(Object aggregate) {
        return wrap(this.aggregate.getAggregate(String.valueOf(aggregate)));
    }

    public Aggregate getBackEndAggregate() {
        return aggregate;
    }

    public Place getPlace(Object key) {
        return aggregate.getPlace(String.valueOf(key));
    }

    public AggregateVariable getAggregateVariable(Object name) {
        return aggregate.getVariable(String.valueOf(name));
    }

    /**
     * Create ecli accessor for a token.
     * @param token - a token to create accessor for.
     * @return an ecli accessor for a token
     */
    protected EcliTokenAccessor wrap(Token token) {
        if (null != token) {
            return new EcliTokenAccessor(token);
        } 
        else {
            return null;
        }
    }

    /**
     * Create ecli accessor for a child aggregate.
     * @param child - child aggregate
     * @return ecli accessor for a child aggregate.
     */
    protected EcliAggregateAccessor wrap(Aggregate child) {
        if (null != child) {
            return new EcliAggregateAccessor(child);
        } 
        else {
            return null;
        }
    }

}
