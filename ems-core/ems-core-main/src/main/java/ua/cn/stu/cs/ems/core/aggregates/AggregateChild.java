package ua.cn.stu.cs.ems.core.aggregates;

import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.queues.Queue;
import ua.cn.stu.cs.ems.core.transitions.Transition;
import ua.cn.stu.cs.ems.core.utils.Coordinates;

/**
 * AggregateChild - interface for all objects that can be child of the aggregate.
 * @see Aggregate
 * @see Place
 * @see Transition
 * @see Queue
 * @author proger
 */
public interface AggregateChild {

    /**
     * Change name of this object.
     * @param name - new name of the object
     * @return true if name was changed successfully, false otherwise.
     */
    public boolean changeName(String name);

    /**
     * Get name of the object.
     * @return name of the object
     */
    public String getName();

    /**
     * Get parent aggregate of this object
     * @return aggregate of this object if this object is a child of any aggregate or
     * null otherwise.
     */
    public Aggregate getParentAggregate();

    /**
     * Set parent aggregate of this object.
     * @param aggregate - new parent aggregate of this object.
     */
    public void setParentAggregate(Aggregate aggregate);

    /**
     * Get coordinates of the position were name of this object should be drawn.
     * @return name's coordinates.
     */
    public Coordinates getNameCoordinates();

    /**
     * Set coordinates of the position were name of this object should be drawn.
     * @param newCoordinates - new name's coordinates.
     */
    public void setNameCoordinates(Coordinates newCoordinates);

    /**
     * Get coordinates of the position were object should be drawn.
     * @return object's coordinates
     */
    public Coordinates getObjectCoordinates();

    /**
     * Set coordinates of the position were object should be drawn.
     * @param newCoordinates - new object's coordinates.
     */
    public void setObjectCoordinates(Coordinates newCoordinates);
}
