package ua.cn.stu.cs.ems.core.transitions;

import ua.cn.stu.cs.ems.core.transitions.functions.PermitingFunction;

/**
 *
 * @author proger
 */
public interface SpecialTransition extends Transition {
    PermitingFunction getPermitingFunction();
    void setPermitingFunction(PermitingFunction function);
}
