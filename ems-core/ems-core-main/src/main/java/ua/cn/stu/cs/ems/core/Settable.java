package ua.cn.stu.cs.ems.core;

import ua.cn.stu.cs.ems.core.aggregates.InputImpl;

/**
 * Interface Settable - represent ability to set token on this object.
 *
 * @see Position
 * @see InputImpl
 */
public interface Settable {

    /**
     * @param        token
     */
    public void setToken(Token token);
}
