package ua.cn.stu.cs.ems.core.xml;


import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.concurrent.ConcurrentLinkedQueue;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jgrapht.DirectedGraph;
import org.jgrapht.alg.CycleDetector;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.cn.stu.cs.ems.core.aggregates.AggregateDefinition;
import ua.cn.stu.cs.ems.core.aggregates.AggregateRegistry;
import ua.cn.stu.cs.ems.core.utils.ResettableFileInputStream;

/**
 * AggregateTypeReader reads all aggregate types from the specified directory to
 * the aggregate registry.
 * This class doesn't validate PNML file according to RNG schemas. Use XMLValidation
 * explicitely on input files.
 * @author proger
 */
public class AggregateTypesReader {

    final static Logger logger = LoggerFactory.getLogger(AggregateTypesReader.class);
     
    public AggregateRegistry readAggregateTypes(File[] files) throws JDOMException, IOException, PNMLParsingException {
        HashMap<String, InputStream> aggregateSources = new HashMap<String, InputStream>();

        for (File file : files) {
            String aggregateName = cutOffExtension(file.getName());
            InputStream is = new ResettableFileInputStream(file);
            is.mark(Integer.MAX_VALUE);

            aggregateSources.put(aggregateName, is);
        }

        return readAggregateTypes(aggregateSources);
    }

    private String cutOffExtension(String fileName) {
        int dotPos = fileName.lastIndexOf('.');

        return fileName.substring(0, dotPos);
    }

    /**
     * Read aggregate registry.
     * @param aggregateSources - hash were keys - names of sources of aggregate definitions
     * (files) and values is InputStreams with data to read aggregate definition from.
     * @return aggregate registry if no errors occurred.
     * @throws JDOMException
     * @throws IOException
     * @throws PNMLParsingException
     */
    // TODO add comments 
    public AggregateRegistry readAggregateTypes(HashMap<String, InputStream> aggregateSources) throws JDOMException, IOException, PNMLParsingException {
        AggregateRegistry registry = new AggregateRegistry();
        // DOM queue stores parsed DOM trees and aditional info for aggregates that weren't build to aggregate definitions yet
        // We need to because we need to build child aggregates before we build
        // parent aggregate to check if such aggregate type exists and if child aggregate type
        // has necessary inputs/outputs
        
        ConcurrentLinkedQueue<DomQueueElement> domQueue = new ConcurrentLinkedQueue<DomQueueElement>();
        
        AggregateBuilder builder = new AggregateBuilder();

        for (String aggregateName : aggregateSources.keySet()) {

            AggregateDefinition aggregateDefinition = registry.getAggregateDefinition(aggregateName);
            
            // Check if we already build this aggregate definition
            if (aggregateDefinition == null) {
                // One of possible semantic errors - loops in aggregate defintion
                // For example if A1 includes A2 and A2 includes A1 this can't be build
                // We will build graph of which aggregate has which childs
                DirectedGraph cycleFinder = createCycleFinderGraph();

                DomQueueElement dqe = createDomQueueElement(aggregateSources, aggregateName, cycleFinder);
                
                Document dom = dqe.dom;
                List<String> childNames = dqe.childrenNames;
                boolean canBuild = true;
                for (String childName : childNames) {
                    AggregateDefinition ad = registry.getAggregateDefinition(childName);
                    // Check if child has already been build
                    if (ad == null) {
                        // adding aggregate to queue for later building
                        domQueue.add(dqe);
                        canBuild = false;
                    }
                }
                if (canBuild) {
                    AggregateDefinition newAD = builder.buildAggregateDefinition(dom, registry);
                    checkAggregateTypeName(newAD, dqe);
                    // Check if this aggregate has already been build
                    if (registry.getAggregateDefinition(newAD.getName()) == null) {
                        registry.addAggregateDefinition(newAD);
                    }
                }
                else {
                    domQueue.add(dqe);
                }
            }
        }
        
        int retries = domQueue.size();
        while (!domQueue.isEmpty()) {
            DomQueueElement domEl = domQueue.poll();
            Document dom = domEl.dom;

            List<String> childNames = domEl.childrenNames;
            boolean canBuild = true;
            for (String childName : childNames) {
                AggregateDefinition ad = registry.getAggregateDefinition(childName);
               // Check if child has already been build
               if (ad == null) {
                  canBuild = false;
                  break;
                }
             }

             // All child is build or this aggregate has no childs
             if (canBuild) {
                 AggregateDefinition newAD = builder.buildAggregateDefinition(dom, registry);
                 checkAggregateTypeName(newAD, domEl);
                 // Check if this aggregate has already been build
                 if (registry.getAggregateDefinition(newAD.getName()) == null) {
                     registry.addAggregateDefinition(newAD);
                 }
                 retries = domQueue.size();
             }
             else {
                 retries--;
                 domQueue.add(domEl);
             }
             
             if (retries == -1 && !domQueue.isEmpty()) {
                 // list of types are broken
                 throw new PNMLParsingException("Types are broken: not all childs found");
             }
        }

        logger.debug("reading types finished");
        return registry;
    }

    // Aggregate definition name should be equals to source name
    private void checkAggregateTypeName(AggregateDefinition newAD, DomQueueElement domEl) throws PNMLParsingException {
        // XML file name isn't equals to aggregate type name
        if (!newAD.getName().equals(domEl.name)) {
            throw new PNMLParsingException("Read file with name " + domEl.name + " but it describes aggregate type " + newAD.getName());
        }
    }

    private Document parseDOM(InputStream is) throws JDOMException, IOException {
        SAXBuilder saxBuilder = new SAXBuilder();
        Document document = saxBuilder.build(is);

        return document;
    }


    private List<String> getChildNames(Document parsedDom) throws JDOMException {
        ArrayList<String> result = new ArrayList<String>();
        findChilds(parsedDom.getRootElement(), result);

        return result;
    }

    private void findChilds(Element element, ArrayList<String> result) {
        if (element.getName().equals("definition") && element.getAttributeValue("type").equals("aggregate")) {
            result.add(element.getAttributeValue("subType"));
        }
        else {
            ListIterator<Element> listIterator = element.getChildren().listIterator();

            while (listIterator.hasNext()) {
                Element elem = listIterator.next();

                findChilds(elem, result);
            }
        }
    }

    private InputStream getInputStream(HashMap<String, InputStream> aggregateSources, String name) throws IOException, PNMLParsingException {
        InputStream is = aggregateSources.get(name);
        if (is != null) {
            is.reset();
            return is;
        }
        else {
            throw new PNMLParsingException("Can't find file with with PNML for " + name);
        }
    }

    private DomQueueElement createDomQueueElement(HashMap<String, InputStream> aggregateSources, 
            String aggregateName, DirectedGraph cycleFinder) 
            throws JDOMException, IOException, PNMLParsingException {
        Document parsedDom = parseDOM(getInputStream(aggregateSources, aggregateName));
        List<String> childNames = getChildNames(parsedDom);

        checkForLoops(cycleFinder, aggregateName, childNames);

        return new DomQueueElement(parsedDom, childNames, aggregateName);
    }

    private DirectedGraph createCycleFinderGraph() {
        return new DefaultDirectedGraph<String, DefaultEdge>(DefaultEdge.class);
    }

    // Add parent-child relationships to a graph and check if there exists a loop
    private void checkForLoops(DirectedGraph cycleFinder, String parentName, List<String> childNames) throws PNMLParsingException {
        DefaultDirectedGraph<String, DefaultEdge> dependencyGraph = (DefaultDirectedGraph<String, DefaultEdge>) cycleFinder;

        dependencyGraph.addVertex(parentName);

        for (String childName : childNames) {
            dependencyGraph.addVertex(childName);
            dependencyGraph.addEdge(parentName, childName);
        }

        CycleDetector<String, DefaultEdge> cycleDetector = new CycleDetector<String, DefaultEdge>(dependencyGraph);

        if (cycleDetector.detectCycles()) {
            throw new PNMLParsingException("Cycle found in aggregate dependencies");
        }
    }

}

class DomQueueElement {
    // jDOM tree for aggregate definition
    Document dom;
    // List of children's names
    List<String> childrenNames;
    // Name of the aggregate/source
    String name;

    public DomQueueElement(Document dom, List<String> childNames, String name) {
        this.dom = dom;
        this.childrenNames = childNames;
        this.name = name;
    }
}
