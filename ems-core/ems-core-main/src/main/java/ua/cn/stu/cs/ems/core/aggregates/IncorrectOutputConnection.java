package ua.cn.stu.cs.ems.core.aggregates;

/**
 *
 * @author proger
 */
public class IncorrectOutputConnection extends RuntimeException {
    public IncorrectOutputConnection() {
        super();
    }

    public IncorrectOutputConnection(String msg) {
        super(msg);
    }
}
