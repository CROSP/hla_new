package ua.cn.stu.cs.ems.core;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ua.cn.stu.cs.ems.core.aggregates.AbstractAggregateChild;
import ua.cn.stu.cs.ems.core.utils.JessUtils;

/**
 * Class Place - place in Petri Nets. It can be connect to any active instance.
 * When a token is set to place or a token is deleted from it all
 * PlaceStateChangedListeners that was added to this place and some
 * active instances are invoked.
 *
 * @see ActiveInstance
 * @see Token
 * @see PlaceStateChangedListener
 */
public class Place extends AbstractAggregateChild {

    final static Logger logger = LoggerFactory.getLogger(Place.class);
    // Token in this postion
    private Token currentToken;
    // Set of listeners that include objects for collecting statistic information
    private Set<PlaceStateChangedListener> listeners = new HashSet<PlaceStateChangedListener>();
    private ActiveInstance inputActiveInstance;
    private ActiveInstance outputActiveInstance;

    /**
     * Create place with a specified name.
     * @param name - name of a new place.
     */
    public Place(String name) {
        super(name);
    }

    /**
     * Get a token that was set in this place.
     * @return token if there is one in this place or null.
     */
    public Token getToken() {
        return currentToken;
    }

    /**
     * Set a token to current place.
     * @param newToken - token to set to this place
     */
    public void setToken(Token newToken) {

        if (newToken != null && currentToken != null) {
            throw new JessException("Attempt to put not null token in a not empty place."
                    + "Possibly a bug. Please report");
        }

        this.currentToken = newToken;

        logger.info("Token {} set to place {}", currentToken, 
                JessUtils.getAggregateChildFullName(this));

        if (newToken != null) {
            if (outputActiveInstance != null) {
                outputActiveInstance.placeStateChanged(this);
            }
        } // token was deleted by output
        else {
            if (inputActiveInstance != null) {
                inputActiveInstance.placeStateChanged(this);
            }
        }

        // Inform every listener about new token
        for (PlaceStateChangedListener listner : listeners) {
            listner.placeStateChanged(this);
        }
    }

    /**
     * Setting new token during initialization.
     * @param newToken - new token to set
     */
    public final void initToken(Token newToken) {
        currentToken = newToken;
    }

    /**
     * Check if this place has no token
     * @return true if there is no token in this place, false otherwise
     */
    public final boolean isEmpty() {
        return currentToken == null;
    }

    /**
     * Set a modeling instance for which this place is an output.
     * @param activeInstance - a modeling instance for which this place is an output.
     */
    public final void setInputActiveInstance(ActiveInstance activeInstance) {
        inputActiveInstance = activeInstance;
    }

    /**
     * Set a modeling instance for which this place is an input.
     * @param activeInstance - a modeling instance for which this place is an input.
     */
    public final void setOutputActiveInstance(ActiveInstance activeInstance) {
        outputActiveInstance = activeInstance;
    }

    /**
     * Return a modeling instance for which this place is an output.
     * @return a modeling instance for which this place is an output if one exists, or null otherwise.
     */
    public final ActiveInstance getInputActiveInstance() {
        return inputActiveInstance;
    }

    /**
     * Return a modeling instance for which this place is an input.
     * @return a modeling instance for which this place is an input if one exists, or null otherwise.
     */
    public final ActiveInstance getOutputActiveInstance() {
        return outputActiveInstance;
    }

    /**
     * Add listener that is invoked every time when token is set or deleted from.
     * current place.
     * @param listener - listener to add.
     */
    public final void addStateChangeListener(PlaceStateChangedListener listener) {
        listeners.add(listener);
    }

    /**
     * Remove place listener.
     * @param listener - listener to remove
     */
    public final void delStateListener(PlaceStateChangedListener listener) {
        listeners.remove(listener);
    }

    /**
     * Get list of listeners that were subscribed to this place.
     * @return list of subscribed listeners.
     */
    public final List<PlaceStateChangedListener> getPlaceStateChangedListeners() {
        List<PlaceStateChangedListener> result = new ArrayList<PlaceStateChangedListener>();

        for (PlaceStateChangedListener listener : listeners) {
            result.add(listener);
        }

        return result;
    }

    @Override
    public final String toString() {
        String result = "";
        result += getName() + " (";
        if (getToken() != null) {
            result += getToken().toString();
        }

        result += ")";
        return result;
    }
}
