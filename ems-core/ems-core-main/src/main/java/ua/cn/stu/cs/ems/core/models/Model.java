package ua.cn.stu.cs.ems.core.models;

import ua.cn.stu.cs.ems.core.ActiveInstance;
import ua.cn.stu.cs.ems.core.aggregates.Aggregate;
import ua.cn.stu.cs.ems.core.aggregates.Input;
import ua.cn.stu.cs.ems.core.aggregates.Output;
import ua.cn.stu.cs.ems.core.queues.Queue;
import ua.cn.stu.cs.ems.core.transitions.SpecialTransition;
import ua.cn.stu.cs.ems.core.transitions.Transition;

/**
 * Model is the main element of E-networks modeling.
 * It has ability to run specified aggregate (called root aggregate). Also it
 * directs modeling process. All active instances of model like transitions, queues,
 * inputs, outputs before changing net state should stay in delay. To do this they should
 * call addDelayable method of model. Model decides itself when it is time to
 * wake up an active instance. It decision is based on an active instance priority,
 * active instance delay time and other network events.
 *
 * @see Transition
 * @see Queue
 * @see Input
 * @see Output
 */
public interface Model {

    /**
     * Set aggregate whose underlying E-net will be run.
     * @param aggregate - root aggregate
     */
    public void setRootAggregate(Aggregate aggregate);

    /**
     * Get current root aggregate
     * @return Aggregate - root aggregate
     */
    public Aggregate getRootAggregate();

    /**
     * Get current modeling time
     * @return double - current modeling time.
     */
    public double getModelingTime();

    /**
     * Set delayable to delay.
     * @param instance - delayable to set into delay.
     * @param delay - time to stay in delay
     * @return true if delayable was set into delay.
     */
    public boolean addDelayable(ActiveInstance instance, double delay);

    /**
     * Special transitions should be checked separately from usual delayables
     * and other transtions because they have permitting functions whose result
     * depends on the network state. So when any event that can change permitting
     * function's result happens special transition should be saved in a model
     * internal data structure and only when all usual transition that should fired
     * at current model times has fired, model should check if special transition
     * can fire.
     *
     * @param transition
     */
    public void addSpecialTransition(SpecialTransition transition);

    /**
     * Start model execution.
     */
    public void start();

    /**
     * Start model execution with step.
     */
    public void step();

    /**
     * Reset model internals to the initial state.
     * This method can be used to run model several times with same root aggregate.
     * Method doesn't restore initial state of root aggregate and it's subaggregates
     * it's should be handled separately.
     */
    public void reset();

    /**
     * Add model listener for this model.
     * @param listener - model listener to add.
     */
    public void addModelListener(ModelListener listener);

    /**
     * Removes model listener from this model.
     * @param listener - model listener to be removed
     */
    public void removeModelListener(ModelListener listener);

    /**
     * Add model listener that will be called after specified delay inteval is
     * passed.
     * @param timeListener - listener to call after delay
     * @param delay - interval of time to wait before calling listener
     */
    public void addTimer(ModelTimeListener timeListener, double delay);

    /**
     * Interrupt work of model.
     * If model works too long or even infinitely this method can be used
     * to stop model.
     */
    public void interrupt();

    /**
     * Get finish time of modeling
     * @return double - finish time.
     */
    public double getFinishTime();

    /**
     * This method used for reset model in step experiment
     * @param init 
     */
    public void setInitModel(boolean init);
}
