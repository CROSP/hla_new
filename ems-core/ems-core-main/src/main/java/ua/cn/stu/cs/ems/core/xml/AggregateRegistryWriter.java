package ua.cn.stu.cs.ems.core.xml;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import org.jdom.Document;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import ua.cn.stu.cs.ems.core.Constants;
import ua.cn.stu.cs.ems.core.aggregates.AggregateDefinition;
import ua.cn.stu.cs.ems.core.aggregates.AggregateRegistry;
import static ua.cn.stu.cs.ems.core.utils.AssertUtils.assertTrue;

/**
 * AggregateRegistryWriter - saves all registry definitions
 * @author proger
 */
public class AggregateRegistryWriter {

    /**
     * Save aggregate registry in PNML format. Every definition is saved in a separate
     * file.
     * @param registry - registry with aggregate definitions
     * @param dirToSave - directory to save files with aggregate definitions to.
     * @throws IOException
     */
    public void saveAggregateRegistry(AggregateRegistry registry, File dirToSave) throws IOException {
        FileOutputStream fos = null;

        try {
            assertTrue(dirToSave.isDirectory(), "No such directory " + dirToSave.getCanonicalPath());

            AggregateWriter writer = new AggregateWriter();
            for (AggregateDefinition ad : registry.getAggregateDefinitions()) {
                Document dom = writer.createDOMFromAggregateDefinition(ad);
                File fileToSave = new File(dirToSave, getFileName(ad));
                fileToSave.createNewFile();
                fos = new FileOutputStream(fileToSave);


                XMLOutputter outputter = new XMLOutputter();
                Format prettyFormat = Format.getPrettyFormat();
                outputter.setFormat(prettyFormat);

                outputter.output(dom, fos);
            }
        } catch (IOException ex) {
            throw ex;
        } finally {
            if (fos != null) {
                fos.close();
            }
        }
    }

    private String getFileName(AggregateDefinition ad) {
        return ad.getName() + "." + Constants.AGGREGATE_PNML_EXTENSION;
    }
}
