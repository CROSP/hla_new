package ua.cn.stu.cs.ems.core.queues;

import java.util.ArrayList;
import java.util.List;
import java.util.Comparator;
import java.util.PriorityQueue;
import ua.cn.stu.cs.ems.core.Token;
import static ua.cn.stu.cs.ems.core.utils.AssertUtils.*;
/**
 * AbstractPriorityQueue - parent class for all priority queues.
 * @author proger
 */
public abstract class AbstractPriorityQueue extends AbstractQueue {

    private TokenPriorityFunction priorityFunction;
    private PriorityQueue<TokenPriorityPair> queue = new PriorityQueue<TokenPriorityPair>(5, new TokenPriorityPairComparator());

    public AbstractPriorityQueue(String name, TokenPriorityFunction priorityFunction) {
        super(name);

        assertNotNull(priorityFunction);
        this.priorityFunction = priorityFunction;
    }

    @Override
    protected void addToken(Token token) {
        int tokenPriority = priorityFunction.calculateTokenPriority(this, token);
        int priorityForPriorityQueue = getPriorityForQueue(tokenPriority);

        TokenPriorityPair tpp = new TokenPriorityPair(token, priorityForPriorityQueue);
        queue.add(tpp);
    }

    @Override
    protected Token removeTokenFromQueue() {
        if (queue.isEmpty())
            return null;

        TokenPriorityPair tpp = queue.poll();
        return tpp.token;

    }


    public List<Token> getTokensInQueue() {
        List<Token> result = new ArrayList<Token>();

        for (TokenPriorityPair tpp : queue)
            result.add(tpp.token);

        return result;
    }

    public boolean isQueueEmpty() {
        return queue.isEmpty();
    }

    public void clear() {
        queue.clear();
    }

    public int length() {
        return queue.size();
    }

    public TokenPriorityFunction getPriorityFunction() {
        return priorityFunction;
    }

    public void setPriorityFunction(TokenPriorityFunction newPriorityFunction) {
        assertNotNull(newPriorityFunction, "Priority function can't be null");
        priorityFunction = newPriorityFunction;
    }

    /**
     * This method defines priority that used to select place in priority queue
     * based on result of TokenPriorityFunction.
     * It's needed because priority is defined by user function that defines priority
     * of token and place in priority queue is defined by the token's priority and
     * queue discipline (LIFO, FIFO)
     * @param priorityEstimation
     * @return
     */
    protected abstract int getPriorityForQueue(int priorityEstimation);

}

class TokenPriorityPair {
    Token token;
    int priority;

    TokenPriorityPair(Token token, int priority) {
        this.token = token;
        this.priority = priority;
    }
}

class TokenPriorityPairComparator implements Comparator<TokenPriorityPair> {

    public int compare(TokenPriorityPair o1, TokenPriorityPair o2) {
        return o1.priority - o2.priority;
    }
    
}
