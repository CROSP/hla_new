package ua.cn.stu.cs.ems.core.models;

/**
 * Interfae to observe changes in model state.
 * @author proger
 */
public interface ModelListener extends ModelTimeListener {
    /**
     * Model started
     * @param mode
     */
    public void modelStarted(Model model);

    /**
     * Model finished
     * @param model
     */
    public void modelFinished(Model model);
}
