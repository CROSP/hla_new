package ua.cn.stu.cs.ems.core.utils;

/**
 *
 * @author proger
 */
public class ArrayUtils {
    
    private ArrayUtils() {}
    
    public static Double[] createBoxedArray(double[] dArr) {
        Double[] result = new Double[dArr.length];
        
        for (int i = 0; i < dArr.length; i++) {
            result[i] = Double.valueOf(dArr[i]);
        }
        
        return result;
    }
    
    public static double[] createUnboxedArray(Double[] dArr) {
        double[] result = new double[dArr.length];
        
        for (int i = 0; i < dArr.length; i++) {
            result[i] = dArr[i];
        }
        
        return result;
    }
}