package ua.cn.stu.cs.ems.core.experiment.statistics;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import ua.cn.stu.cs.ems.core.aggregates.Aggregate;
import ua.cn.stu.cs.ems.core.aggregates.AggregateVariable;
import ua.cn.stu.cs.ems.core.aggregates.VariableChangedListener;
import ua.cn.stu.cs.ems.core.models.Model;
import static ua.cn.stu.cs.ems.core.utils.AssertUtils.assertFalse;
import static ua.cn.stu.cs.ems.core.utils.AssertUtils.assertNotNull;
import ua.cn.stu.cs.ems.core.utils.JessUtils;

/**
 *
 * @author leonid
 */
// TODO write some tests
public class VariablesStateCollector {
    
    private Model model;
    private HashMap<String, AggregateVariableStateCollector> collectors = 
            new HashMap<String, AggregateVariableStateCollector>();
    
    private boolean freezed;
    
    public VariablesStateCollector(Model model) {
        assertNotNull(model, "Model cant be null");
        this.model = model;
        freezed = false;
        if (model.getRootAggregate() != null) // WRONG. tests using model without aggregate, need fix tests 
            generateCollectors(model.getRootAggregate());
    }
    
    private void generateCollectors(Aggregate child) {
        for (String vname: child.getVariablesNames()) {
            AggregateVariable av = child.getVariable(vname);
            final AggregateVariableStateCollector cl = new AggregateVariableStateCollector(av);
            av.addVariableChangeListener(new VariableChangedListener() {

                public void variableValueChanged(AggregateVariable var) {
                    // BUG. in tests used variable without parent aggregate and model
                    if (var.getParentAggregate() != null && var.getParentAggregate().getModel() != null) {
                        cl.save(var.getParentAggregate().getModel().getModelingTime());
                    }
                }
            });
            
            collectors.put(JessUtils.getAggregateChildFullName(av), cl);
        }
        for (Aggregate a: child.getChildAggregates()) {
            generateCollectors(a);
        }
    }
    
    public void saveState(double time) {
        assertFalse(freezed, "Variables already saved. Create new collector to "
                + "save state");
        for (String avName: collectors.keySet()) {
            collectors.get(avName).save(time);
        }
    }
    
    public void setFreezed() {
        freezed = true;
    }
        
    public Set<String> getChildNames() {
        return new HashSet<String>(collectors.keySet());
    }
    
    public AggregateVariableStateCollector getChildStatistic(String childName) {
        return collectors.get(childName);
    }
}
