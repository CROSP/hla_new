package ua.cn.stu.cs.ems.core.queues;

import java.io.IOException;
import org.codehaus.plexus.util.StringInputStream;
import ua.cn.stu.cs.ems.core.InterpretedFunction;
import ua.cn.stu.cs.ems.core.JessException;
import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.ecli.EcliModelAccessor;
import ua.cn.stu.cs.ems.ecli.EclInterpreter;
import ua.cn.stu.cs.ems.ecli.InterpretationReport;

/**
 * Function that calculate token's priority with help of ecli interpreter.
 * @author proger
 */
public class InterpretedTokenPriorityFunction extends InterpretedFunction implements TokenPriorityFunction {
    

    public InterpretedTokenPriorityFunction(String source) {
        super(source);
    }

    public int calculateTokenPriority(Queue queue, Token token) {
        try {
            EclInterpreter ei = new EclInterpreter();
            EcliModelAccessor ema = new EcliModelAccessor(queue.getParentAggregate().getModel(), queue.getParentAggregate());
            ema.setCurrentToken(token);
            InterpretationReport ir = ei.interpret(new StringInputStream(source), ema);

            return ir.getResult().intValue();
        } catch (IOException ex) {
            throw new JessException("IOException occured during calculation of token's priority", ex);
        }
    }
}
