package ua.cn.stu.cs.ems.core.experiment;

import ua.cn.stu.cs.ems.core.experiment.statistics.StatisticsResult;

/**
 * Element of the primary statistics result. It stores statistics result that
 * was dumped for an aggregate child in a certain moment of model time.
 *
 * @author proger
 */
public class PrimaryStatisticsElement {

    private double time;
    private StatisticsResult statisticsResult;

    /**
     * Default constructor without parameters.
     */
    public PrimaryStatisticsElement() {
    }

    /**
     * Convenience constructor with parameters.
     *
     * @param theTime
     * @param theStatisticsResult
     */
    public PrimaryStatisticsElement(double theTime, StatisticsResult theStatisticsResult) {
        time = theTime;
        statisticsResult = theStatisticsResult;
    }

    /**
     * Model time at which statistics was dumped.
     *
     * @return time at which statistics was dumped
     */
    public final double getTime() {
        return time;
    }

    /**
     * Statistics result that was dumped.
     *
     * @return statistics result that was dumped.
     */
    public final StatisticsResult getStatisticsResult() {
        return statisticsResult;
    }

    @Override
    public final int hashCode() {
        int hash = 7;
        hash = 71 * hash + (int) (Double.doubleToLongBits(this.time) ^ (Double.doubleToLongBits(this.time) >>> 32));
        hash = 71 * hash + (this.statisticsResult != null ? this.statisticsResult.hashCode() : 0);
        return hash;
    }

    @Override
    public final boolean equals(Object obj) {
        if (obj instanceof PrimaryStatisticsElement) {
            PrimaryStatisticsElement other = (PrimaryStatisticsElement) obj;

            if (this.time != other.time) {
                return false;
            }

            if (!this.statisticsResult.equals(other.statisticsResult)) {
                return false;
            }

            return true;
        }

        return false;
    }

    @Override
    public final String toString() {
        return "PrimaryStatisticsElement(time: " + time + "; " + statisticsResult + ")";
    }
}