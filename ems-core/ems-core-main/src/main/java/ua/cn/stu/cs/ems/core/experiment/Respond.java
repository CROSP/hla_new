/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.cn.stu.cs.ems.core.experiment;

import ua.cn.stu.cs.ems.core.aggregates.AggregateVariable;

/**
 * Class for  choosing the type of response
 * @author Dmitry Krivenko <dmitrykrivenko@gmal.com>
 */
public class Respond {

    public static final String TYPE_VARIABLE = "variable";
    private String name;
    private double value;
    private AggregateVariable respondVariable;

    /*
     * If respond is average numerical characteristics
     */
    public Respond(String nameRespond) {
        name = nameRespond;
        value = 0;
    }

    /*
     * If respond is variable
     */
    public Respond(AggregateVariable variable) {
        respondVariable = variable;
        name = TYPE_VARIABLE;
        value = respondVariable.getValue().doubleValue();
    }

    /**
     * @return the name
     */
    public final String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public final void setName(String name) {
        this.name = name;
    }

    /**
     * @return the value
     */
    public final double getValue() {
        if (name.equals(TYPE_VARIABLE)) {
            value = respondVariable.getValue().doubleValue();
        }
        return value;
    }

    /**
     * @param value the value to set
     */
    public final void setValue(double value) {
        this.value = value;
    }
}
