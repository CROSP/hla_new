package ua.cn.stu.cs.ems.core.transitions.functions;

import java.util.HashSet;
import java.util.Set;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.aggregates.AggregateVariable;
import ua.cn.stu.cs.ems.core.transitions.SpecialTransition;

/**
 * Default permitting function - permitting function that always returns 0, as a number of input/output place that 
 * should be used by X/Y transition to perform it's operations.
 * @author proger
 */
public class DefaultPermitingFunction implements PermitingFunction {

    public final Integer getPlaceNumber(SpecialTransition transition) {
        return Integer.valueOf(0);
    }

    public final Set<Place> getPlaceDependencies(SpecialTransition transition) {
        return new HashSet<Place>();
    }

    public final Set<AggregateVariable> getVariableDependencies(SpecialTransition transition) {
        return new HashSet<AggregateVariable>();
    }

    @Override
    public final boolean equals(Object o) {
        return (o instanceof DefaultPermitingFunction);
    }

    public final PermitingFunction copy() {
        return new DefaultPermitingFunction();
    }

    @Override
    public final int hashCode() {
        int hash = 7;
        return hash;
    }
}
