package ua.cn.stu.cs.ems.core.aggregates;

import java.util.List;
import ua.cn.stu.cs.ems.core.ActiveInstance;
import ua.cn.stu.cs.ems.core.Place;

/**
 *
 * @author proger
 */
public interface Output extends ActiveInstance {


    /**
     * Connect input place to an output.
     * @param place - place to connect. null - means that output has no connected input place
     */
    void connectInputPlace(Place place);

    /**
     * Connect output place to an output.
     * @param place - place to connect. null - means that output has no connected output place
     */
    void connectOutputPlace(Place place);

    /**
     * Get conflict resolve strategy
     * @return - conflict resolve strategy
     */
    ConflictResolutionStrategy getConflictResolutionStrategy();

    /**
     * Set conflict resolve strategy
     * @param strategy - conflict resolve strategy
     */
    void setConflictResolutionStrategy(ConflictResolutionStrategy strategy);

    
    /**
     * Add input to an output
     * @param input - input to add
     * @param pos - number in list of connected inputs
     */
    void addInput(Input input, int pos);


    void inputCanBeSet(Input input);


    public List<Input> getConnectedInputs();
    
    /**
     * Replace input that connected to an output.
     * @param input - input that will replace old input;
     * @param pos - number of input to be replaced
     */
    void replaceInput(Input input, int pos);

    public void disconnectInput(int i);

    public Place getInputPlace();

    public Place getOutputPlace();

}
