package ua.cn.stu.cs.ems.core;

import java.util.List;
import ua.cn.stu.cs.ems.core.aggregates.AggregateChild;

/**
 * Active instance - any object of E-nets that can change state of the network.
 * @author proger
 */
public interface ActiveInstance extends PlaceStateChangedListener, AggregateChild {

    /**
     * Get list of output places.
     * @return list of output places
     */
    List<Place> getInputPlaces();

    /**
     * Return number of output places connected to this transition.
     * @return number of output places
     */
    List<Place> getOutputPlaces();

    /**
     * Disconnect input place from this active instance.
     * @param name - name of the place to disconnect
     */
    void disconnectInputPlace(String name);

    /**
     * Disconnect output place from this active instance.
     * @param name - name of the place to disconnect
     */
    void disconnectOutputPlace(String name);

    /**
     * This method should be called to activate instance when it's delay is finished.
     */
    void onDelayFinished();

    /**
     * This method first checks if all conditions for transition to fire are met
     * and if so adds this transition to delay queue.
     */
    void fireIfPosible();

    /**
     * Add new listener to this ActiveInstance.
     * @param listener - new listener to add
     */
    void addListener(ActiveInstanceListener listener);

    /**
     * Remove listener.
     * @param listener - listener to remove
     */
    void removeListener(ActiveInstanceListener listener);
}
