package ua.cn.stu.cs.ems.core.transitions.functions;

import ua.cn.stu.cs.ems.core.transitions.Transition;

/**
 * Delay function with constant delay.
 * @author proger
 */
public class ConstDelayFunction implements DelayFunction {

    // Delay value
    private double delayTime;
    
    /**
     * Create delay function that always return specified delay.
     * @param delayTime - delay time that will be returned by this delay function
     */
    public ConstDelayFunction(double delayTime) {
        this.delayTime = delayTime;
    }
    
    public final double getDelayTime(Transition transition) {
        return delayTime;
    }

    public final DelayFunction copy() {
        return new ConstDelayFunction(delayTime);
    }

}
