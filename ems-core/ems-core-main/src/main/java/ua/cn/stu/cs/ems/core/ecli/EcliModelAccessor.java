package ua.cn.stu.cs.ems.core.ecli;

import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.aggregates.Aggregate;
import ua.cn.stu.cs.ems.core.models.Model;
import ua.cn.stu.cs.ems.ecli.EcliToken;
import ua.cn.stu.cs.ems.ecli.ModelAccessor;


/**
 * Accessor for ecli interpreter to access a model's state.
 * @author proger
 */
public class EcliModelAccessor extends EcliAggregateAccessor implements ModelAccessor {

    private Model model;
    private Token currentToken;
    private Aggregate currentAggregate;
    
    public EcliModelAccessor(Model theModel, Aggregate theCurrentAggregate) {
        super(theModel.getRootAggregate());
        model = theModel;
        currentAggregate = theCurrentAggregate;
    }

    public double getTime() {
        return model.getModelingTime();
    }

    public EcliToken getCurrentToken() {
        return wrap(currentToken);
    }

    public void setCurrentToken(Token currentToken) {
        this.currentToken = currentToken;
    }

    public EcliAggregateAccessor getCurrentAggregate() {
        return wrap(currentAggregate);
    }

    public EcliAggregateAccessor getRootAggregate() {
        return wrap(model.getRootAggregate());
    }


}
