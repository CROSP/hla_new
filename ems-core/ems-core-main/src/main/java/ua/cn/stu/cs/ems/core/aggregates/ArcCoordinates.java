package ua.cn.stu.cs.ems.core.aggregates;

import java.util.List;
import ua.cn.stu.cs.ems.core.utils.Coordinates;
import static ua.cn.stu.cs.ems.core.utils.AssertUtils.*;

/**
 *
 * ArcCoordinates saves coordinates throw which arc should be build.
 * sourcParent and targetParent can be null. null value means that source/target
 * object belongs to the parent aggregate. non null value represents name of the
 * child aggregate.
 * @author proger
 */
public class ArcCoordinates {

    private String sourceParent;
    private String sourceName;
    private String targetParent;
    private String targetName;
    private List<Coordinates> coordinates;

    public ArcCoordinates() {

    }

    public ArcCoordinates(String sourceParent, String sourceName,
                          String targetParent, String targetName, List<Coordinates> coordiantes) {
        assertNotNull(coordiantes, "Coordinates can't be null");
        assertNotNull(sourceName, "Source name can't be null");
        assertNotNull(targetName, "Target name can't be null");

        this.sourceParent = sourceParent;
        this.sourceName = sourceName;
        this.targetName = targetName;
        this.targetParent = targetParent;
        this.coordinates = coordiantes;
    }

    /**
     *
     * @return the source's parent name
     */
    public String getSourceParent() {
        return sourceParent;
    }

    /**
     * Set source's parent name
     * @param sourceParent source's parent name
     */
    public void setSourceParent(String sourceParent) {
        this.sourceParent = sourceParent;
    }

    /**
     * Get source object's name
     * @return the source's name
     */
    public String getSourceName() {
        return sourceName;
    }

    /**
     * Set source object's name
     * @param sourceName the source object's name to set
     */
    public void setSourceName(String sourceName) {
        assertNotNull(sourceName, "Source name can't be null");
        this.sourceName = sourceName;
    }

    /**
     * Get target object's parent's name
     * @return the target object's parent's name
     */
    public String getTargetParent() {
        return targetParent;
    }

    /**
     * Set target object's parent's name
     * @param targetParent the target parent's object's name
     */
    public void setTargetParent(String targetParent) {        
        this.targetParent = targetParent;
    }

    /**
     * Get target object's name
     * @return the target object's name
     */
    public String getTargetName() {
        return targetName;
    }

    /**
     * Set target object's name
     * @param targetName the target object's name to set
     */
    public void setTargetName(String targetName) {
        assertNotNull(targetName, "Target name can't be null");
        this.targetName = targetName;
    }

    /**
     * Get list of coordinates to build arc between source and target objects
     * @return the coordinates
     */
    public List<Coordinates> getCoordinates() {
        return coordinates;
    }

    /**
     * list of coordinates points to build arc between source and target objects
     * @param coordinates the coordinates to set
     */
    public void setCoordinates(List<Coordinates> coordinates) {
        this.coordinates = coordinates;
    }


}
