package ua.cn.stu.cs.ems.core.ecli;

import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.ecli.EcliToken;
import ua.cn.stu.cs.ems.ecli.Value;

import static ua.cn.stu.cs.ems.core.utils.AssertUtils.*;

/**
 * Accessor for ecli interpreter that gives ability to access tokens attributes.
 * @author proger
 */
public class EcliTokenAccessor implements EcliToken {

    Token token;

    /**
     * Create accessor for a given token.
     * @param theToken - token to create accessor for
     */
    public EcliTokenAccessor(Token theToken) {
        assertNotNull(theToken, "Backend token object can't b null");

        token = theToken;
    }

    public boolean containsAttribute(Object key) {
        return token.getValue(String.valueOf(key)) != null;
    }

    public boolean setAttribute(Object key, Value value) {
        try {
            token.setValue(String.valueOf(key), value);
        }
        // Catch everyting
        catch (Exception ex) {
            return false;
        }

        return true;
    }

    public Object getAttribute(Object key) {
        return token.getValue(String.valueOf(key));
    }

    public Token getToken() {
        return token;
    }
}
