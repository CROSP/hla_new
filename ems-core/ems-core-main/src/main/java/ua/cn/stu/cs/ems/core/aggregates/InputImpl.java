package ua.cn.stu.cs.ems.core.aggregates;

import java.util.List;
import ua.cn.stu.cs.ems.core.AbstractActiveInstance;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.Token;
import static ua.cn.stu.cs.ems.core.utils.AssertUtils.*;

/**
 * Class InputImpl
 */
public class InputImpl extends AbstractActiveInstance implements Input {

    private Output connectedOutput;

    public InputImpl(String name) {
        super(name);
    }
    private Place inputPlace;
    private Place outputPlace;

    public void setToken(Token token) {
        assertNotNull(token);
        assertTrue(outputPlace.isEmpty(),
                "Input's output place should be empty when token is set to an input");

        outputPlace.setToken(token);
    }

    public boolean canSetToken() {
        if (outputPlace == null) {
            return false;
        }

        if (!outputPlace.isEmpty()) {
            return false;
        }

        return true;
    }

    public List<Place> getInputPlaces() {
        return createListWithSinglePlace(inputPlace);
    }

    public List<Place> getOutputPlaces() {
        return createListWithSinglePlace(outputPlace);
    }

    public void placeStateChanged(Place place) {
        fireIfPosible();
    }

    public void fireIfPosible() {
        if (outputPlace == null) {
            return;
        }

        if (outputPlace.isEmpty()) {
            delayFor(0);
        }
    }

    protected void fireTransition() {
        if (outputPlace == null) {
            return;
        }

        if (connectedOutput != null) {
            connectedOutput.inputCanBeSet(this);
        } else if (inputPlace != null && !inputPlace.isEmpty()) {
            Token inputToken = new Token(inputPlace.getToken());
            outputPlace.setToken(inputToken);
            inputPlace.setToken(null);

        }
    }

    public void setInputPlace(Place place) {
        if (inputPlace != null) {
            inputPlace.setOutputActiveInstance(null);
        }

        inputPlace = place;
        if (place != null) {
            place.setOutputActiveInstance(this);
        }
    }

    public void setOutputPlace(Place place) {
        if (outputPlace != null) {
            outputPlace.setInputActiveInstance(null);
        }

        outputPlace = place;
        if (place != null) {
            place.setInputActiveInstance(this);
        }
    }

    public void connectOutput(Output output) {
        this.connectedOutput = output;
    }

    public void disconnectInputPlace(String name) {
        assertNotNull(inputPlace, "No input place is connected");
        assertTrue(inputPlace.getName().equals(name),
                "Input place's name is '" + inputPlace.getName() + "' imposible to delete input place with name " + name + "'");

        inputPlace = null;
    }

    public void disconnectOutputPlace(String name) {
        assertNotNull(outputPlace, "No output place is connected");
        assertTrue(outputPlace.getName().equals(name),
                "Output place's name is '" + outputPlace.getName() + "' imposible to delete output place with name " + name + "'");

        outputPlace = null;
    }

    public Place getOutputPlace() {
        return outputPlace;
    }

    public Place getInputPlace() {
        return inputPlace;
    }

    public Output getConnectedOutput() {
        return connectedOutput;
    }
}
