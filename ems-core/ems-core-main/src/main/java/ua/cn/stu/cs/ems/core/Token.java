package ua.cn.stu.cs.ems.core;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.HashMap;

import ua.cn.stu.cs.ems.ecli.Value;
import static ua.cn.stu.cs.ems.core.utils.AssertUtils.*;

/**
 * Class Token - token that can be placed in place in Petri Networks.
 * Each token has a set of attributes.
 */
public class Token {

    HashMap<String, Value> values = new HashMap<String, Value>();

    /**
     * Create new token with no attributes.
     */
    public Token() {}

    /**
     * Create a new token with same attributes and values as a specified token has.
     * @param token - new token will have same attributes and values as this one.
     */
    public Token(Token token) {
        for (String key : token.values.keySet()) {
            this.values.put(key, new Value(token.getValue(key)));
        }
    }

    /**
     * Set token's attribute's value.
     * @param name - name of the token attribute
     * @param value - value of the attribute
     */
    public void setValue(String name, Value value) {
        assertNotNull(name, "Attribute name can not be null");

        values.put(name, value);
    }

    /**
     * Get token's attribute's value by name.  Null is returned if no attribute was set with a specified name.
     * @return value of the attribute
     * @param name - name of the attribute
     */
    public Value getValue(String name) {
        if (name == null) {
            throw new NullPointerException("Attribute name can not be null");
        }

        return values.get(name);
    }

    /**
     * Get names of attributes in a token
     * @return names of attributes in a token
     */
    public Set<String> getAttributeNames() {
        return new HashSet<String>(values.keySet());
    }

    /**
     * Add attributes to a token
     * @param attributes - array of attributes to add
     */
    public void addAttributes(Attribute... attributes) {
        for (Attribute a : attributes) {
            setValue(a.getName(), a.getValue());
        }

    }

    /**
     * Add attributes to a token
     * @param attributes - collection with attributes to add.
     */
    public void addAttributes(Collection<Attribute> attributes) {
        for (Attribute a : attributes) {
            setValue(a.getName(), a.getValue());
        }
    }

    @Override
    public final String toString() {
        StringBuilder res = new StringBuilder();
        res.append("Token {");
        for (String key : values.keySet()) {
            res.append(key);
            res.append(" : ");
            res.append(values.get(key));
            res.append(", ");
        }

        res.append("}");

        return res.toString();
    }

    @Override
    public final boolean equals(Object obj) {
        if (obj instanceof Token) {
            Token other = (Token) obj;
            return other.values.equals(this.values);
        }
        return false;
    }

    @Override
    public final int hashCode() {
        int hash = 3;
        hash = 17 * hash + (this.values != null ? this.values.hashCode() : 0);
        return hash;
    }
}
