package ua.cn.stu.cs.ems.core.experiment;

import ua.cn.stu.cs.ems.core.models.Model;

/**
 * Experiment that runs model specified number of times for each value of factor.
 * @author proger
 */
public class CountDownExperiment extends AbstractExperiment {

    // Number of times model should be run with each value of a factor    
    private int numberOfRuns;
    // Number of current run ( 0 <= currentRun <= numberOfRuns)
    private int currentRun;

    /**
     * Create CountDownExperiment. This constructor should be used when secondary statistics should be collected.
     * @param model - model to run experiment for.
     * @param factor - factor of an experiment.
     * @param respondVariable - variable that will be used to get respond of the factor changes.
     * @param theNumberOfRuns - how many time an experiment should be run with the same factor value.
     */
    public CountDownExperiment(Model model, Factor factor, Respond respond, int theNumberOfRuns) {
        super(model, factor, respond);
        numberOfRuns = theNumberOfRuns;
    }

    /**
     * Create CountDownExperiment. This constructor should be used when secondary statistics should NOT be collected for 
     * several factor values.
     * @param model - model to run experiment for.
     * @param factor - factor of an experiment.
     * @param theNumberOfRuns - how many time an experiment should be run with the same factor value.
     */
    public CountDownExperiment(Model model, Factor factor, int theNumberOfRuns) {
        super(model, factor);
        numberOfRuns = theNumberOfRuns;
    }

    /**
     * Create CountDownExperiment. This constructor should be used when secondary statistics should NOT be collected  
     * WITHOUT using factor.
     * @param model - model to run experiment for.
     * @param theNumberOfRuns - how many time an experiment should be run with the same factor value.
     */
    public CountDownExperiment(Model model, int theNumberOfRuns) {
        super(model);
        numberOfRuns = theNumberOfRuns;
    }

    @Override
    protected final void run() {
        getModel().start();
        currentRun++;
    }

    @Override
    protected void runWithStep() {
        getModel().step();
    }

    @Override
    protected final boolean lastRun() {
        return currentRun == numberOfRuns;
    }

    @Override
    protected final void specificReset() {
        currentRun = 0;
    }
}