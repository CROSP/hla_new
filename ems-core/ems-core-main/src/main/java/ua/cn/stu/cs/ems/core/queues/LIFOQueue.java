package ua.cn.stu.cs.ems.core.queues;

/**
 * LIFO queue - queue that implements LIFO discipline
 * @author proger
 */
public class LIFOQueue extends NonPriorityQueue {

    /**
     * Create LIFO queue with a specified name.
     * @param name - name of a new queue.
     */
    public LIFOQueue(String name) {
        super(name);
    }

    @Override
    protected final int tookenNumberToPeek() {
        return queue.size() - 1;
    }


}
