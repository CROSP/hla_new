package ua.cn.stu.cs.ems.core.experiment.statistics;

import java.util.HashMap;
import java.util.List;
import ua.cn.stu.cs.ems.core.aggregates.AggregateChild;
import ua.cn.stu.cs.ems.core.experiment.PrimaryStatisticsElement;
import ua.cn.stu.cs.ems.core.models.Model;
import static ua.cn.stu.cs.ems.core.utils.AssertUtils.assertNotNull;

/**
 * This is a parent class for all primary statistics collector. Primary
 * statistics shows how different parameters of a model changed at different
 * time intervals. To collect primary statistics a statistics collector for a
 * specified type of element in E-nets should be implemented. *
 */
public abstract class StatisticsCollector {
    // Model in which primary statistics is collected

    protected Model model;
    // true if statistics is collected, false otherwise
    protected boolean collectsStat;
    // Time when statistics collection was finished for this statistics collector
    protected double collectionFinishTime;
    protected HashMap<AggregateChild, List<PrimaryStatisticsElement>> statisticsResults = null;

    /**
     * Initialize statistics collector
     *
     * @param theModel - model to collect statistics for
     */
    protected StatisticsCollector(Model theModel) {
        assertNotNull(theModel, "Can't collect statistics with null model");

        this.model = theModel;
    }

    /**
     * Start collecting primary statistics. All events that could influence
     * statistics collector and happened before statistics collection start are
     * ignored.
     */
    public abstract void startStatCollection();

    /**
     * Stop collection primary statistics. All events that can influence
     * statistics collector and happened after statistics collector stopped are
     * ignored.
     */
    public abstract void stopStatCollection();

    /**
     * Return primary statistics collected by this object.
     *
     * @return primary statistics collected by this object
     */
    public abstract StatisticsResult getResult();

    /**
     * Reset statistics collector. Reset clears all statistics that was
     * collected by this statistics collector until this moment. Resetting
     * doesn't stop statistics collection.
     */
    public abstract void reset();

    /**
     * Get statistics parameters that is collected by this statistics collector.
     *
     * @return list of statistics parameters.
     */
    public abstract List<StatisticsParameters> getStatisticsParameters();

    /**
     * Get time when statistics collection was finished. If statistics
     * collection is going on, returns current model time otherwise returns time
     * when statistics collection was stopped.
     */
    protected final double getFinishTime() {
        if (collectsStat) {
            return model.getModelingTime();
        }

        return collectionFinishTime;
    }

    public final void setCollectedStatOnEvent(HashMap<AggregateChild, List<PrimaryStatisticsElement>> st) {
        statisticsResults = st;
    }
}
