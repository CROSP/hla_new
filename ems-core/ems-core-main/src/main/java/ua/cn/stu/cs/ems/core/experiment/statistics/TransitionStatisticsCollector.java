package ua.cn.stu.cs.ems.core.experiment.statistics;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.cn.stu.cs.ems.core.AbstractActiveInstance;
import ua.cn.stu.cs.ems.core.ActiveInstanceListener;
import ua.cn.stu.cs.ems.core.experiment.PrimaryStatisticsElement;
import ua.cn.stu.cs.ems.core.models.Model;
import ua.cn.stu.cs.ems.core.models.ModelListener;
import ua.cn.stu.cs.ems.core.transitions.Transition;
import ua.cn.stu.cs.ems.core.utils.JessUtils;

/**
 * Statistics collector for transition. It can collect following statistics
 * parameters: <ul> <li> Occupied coefficient = total time in delay / total
 * monitored time. <li> Number of inputs = number of tokens passed through this
 * transition <li> Average time in delay = total time in delay / number of
 * inputs </ul>
 *
 * @author proger
 */
public class TransitionStatisticsCollector extends StatisticsCollector implements ActiveInstanceListener, ModelListener {

    private Transition transition;
    // Time when transition was put in delay
    private double delayStartTime;
    // Sum of all time durations when transition was in delay
    private double totalDelayTime;
    // true if the transition now in delay, false otherwise
    private boolean transitionInDelay;
    // Number of tokens that passed through the transition
    private int numberOfPassedTokens;
    // Time when primary statistics collection started
    private double startTime;
    final static Logger logger = LoggerFactory.getLogger(TransitionStatisticsCollector.class);

    public TransitionStatisticsCollector(Model model, Transition transition) {
        super(model);

        this.transition = transition;
        model.addModelListener(this);
        transition.addListener(this);
    }

    public final List<StatisticsParameters> getStatisticsParameters() {
        return new ArrayList<StatisticsParameters>() {

            {
                add(StatisticsParameters.NUMBER_OF_PASSED_TOKENS);
                add(StatisticsParameters.OCCUPIED_COEFFICIENT);
                add(StatisticsParameters.AVERAGE_TIME_IN_DELAY);
            }
        };
    }

    @Override
    public final void startStatCollection() {
        if (!collectsStat) {
            collectsStat = true;
            reset();
        }
    }

    @Override
    public final void stopStatCollection() {
        if (collectsStat) {
            updateTimeInDelay();
            collectionFinishTime = model.getModelingTime();
            collectsStat = false;
        }
    }

    @Override
    public final StatisticsResult getResult() {
        if (collectsStat) {
            updateTimeInDelay();
        }

        StatisticsResult sr = new StatisticsResult();

        //long totalMonitoredTime = getFinishTime() - startTime;
        double totalMonitoredTime = model.getFinishTime() - startTime;
        double delayCoefficient = ((double) totalDelayTime) / totalMonitoredTime;
        sr.setResultValue(StatisticsParameters.OCCUPIED_COEFFICIENT, delayCoefficient);
        sr.setResultValue(StatisticsParameters.NUMBER_OF_PASSED_TOKENS, Double.valueOf(numberOfPassedTokens));

        double averageTimeInDelay = 0;

        if (numberOfPassedTokens != 0) {
            averageTimeInDelay = totalDelayTime / numberOfPassedTokens;
        }
        sr.setResultValue(StatisticsParameters.AVERAGE_TIME_IN_DELAY, averageTimeInDelay);

        return sr;
    }

    private StatisticsResult getCurrentResult() {

        StatisticsResult sr = new StatisticsResult();

        double totalMonitoredTime = model.getFinishTime() - startTime;
        double delayCoefficient = ((double) totalDelayTime) / totalMonitoredTime;
        sr.setResultValue(StatisticsParameters.OCCUPIED_COEFFICIENT, delayCoefficient);
        sr.setResultValue(StatisticsParameters.NUMBER_OF_PASSED_TOKENS, Double.valueOf(numberOfPassedTokens));

        double averageTimeInDelay = 0;

        if (numberOfPassedTokens != 0) {
            averageTimeInDelay = totalDelayTime / numberOfPassedTokens;
        }
        sr.setResultValue(StatisticsParameters.AVERAGE_TIME_IN_DELAY, averageTimeInDelay);

        return sr;
    }

    @Override
    public final void reset() {
        startTime = model.getModelingTime();
        numberOfPassedTokens = 0;
        totalDelayTime = 0;

        if (transitionInDelay) {
            delayStartTime = model.getModelingTime();
        }
    }

    public final void instanceDelayStarted(AbstractActiveInstance instance) {
        transitionInDelay = true;
        delayStartTime = model.getModelingTime();
        logger.info("Current statistics for transition {}: {}", JessUtils.getAggregateChildFullName(instance), getCurrentResult());
    }

    public final void instanceDelayFinished(AbstractActiveInstance instance) {
        if (collectsStat) {
            updateTimeInDelay();
            numberOfPassedTokens++;
        }
        transitionInDelay = false;
        if (statisticsResults != null) {
            StatisticsResult sr = getResult();
            PrimaryStatisticsElement psr = new PrimaryStatisticsElement(model.getModelingTime(), sr);
            statisticsResults.get(this.transition).add(psr);
        }
        logger.info("Current statistics for transition {}: {}", JessUtils.getAggregateChildFullName(instance), getCurrentResult());
    }

    public final void modelStarted(Model model) {
        transitionInDelay = false;
    }

    public final void modelFinished(Model model) {
    }

    public final void modelTimeChanged(Model model, double newModelTime) {
    }

    private void updateTimeInDelay() {
        if (transitionInDelay) {
            totalDelayTime += (model.getModelingTime() - delayStartTime);
            delayStartTime = model.getModelingTime();
        }
    }
}
