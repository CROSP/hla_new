package ua.cn.stu.cs.ems.core.aggregates;


/**
 * AggregateInstance - aggregate that should be used for modeling.
 * It has a reference to it's definition. Definition defines aggregates structure,
 * variables, function on transitions and queues.
 */
public class AggregateInstance extends AbstractAggregate implements Aggregate {

    AggregateDefinition definition;

    public AggregateInstance(String name, AggregateDefinition definition) {
        super(name);
        this.definition = definition;
    }

    @Deprecated
    // AggregateInstance should be created with specifing it's definition
    public AggregateInstance(String name) {
        super(name);
    }

    public void addChildAggregate(AggregateInstance child) {
        addAggreagate(child);
    }

    /**
     * Get definition of this aggregate instance.
     * @return aggregate definition
     */
    public AggregateDefinition getAggregateDefinition() {
        return definition;
    }
}
