package ua.cn.stu.cs.ems.core.aggregates;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import ua.cn.stu.cs.ems.core.ActiveInstance;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.queues.Queue;
import ua.cn.stu.cs.ems.core.transitions.Transition;

/**
 * AggregateValidator checks if some aggregate's structure doesn't violates structure rules.
 * For example there shouldn't be an aggregate with no child elements or there can be
 * no place that has only one or zero connections.
 * @author proger
 */
public class AggregateValidator {
    
    final static Logger logger = LoggerFactory.getLogger(AggregateValidator.class);

    /**
     * Validate aggregate
     * @param parent - aggregate to validate
     * @param validateChildAggregates - if true than all children of the parent
     * aggregate will be validated, otherwise only parent aggregate will be validated.
     * @return validation result.
     * @see ValidationResult
     */
    public ValidationResult validate(Aggregate parent, boolean validateChildAggregates)  {
        ArrayList<Aggregate> parents = new ArrayList<Aggregate>();
        ValidationResult result = new ValidationResult();

        validation(parents, parent, validateChildAggregates, result);

        return result;
    }

    private void validation(List<Aggregate> parents, Aggregate aggregate, boolean checkChilds, ValidationResult result) {
        validateAggregate(parents, aggregate, result);

        logger.debug("Validating aggregate: " + aggregate.getName());
        
        parents.add(aggregate);

        for (Place p : aggregate.getPlaces()) {
            validatePlace(parents, p, result);
        }

        for (Transition t : aggregate.getTransitions()) {
            validateTransition(parents, t, result);
        }

        for (Queue q : aggregate.getQueues()) {
            validateQueue(parents, q, result);
        }

        for (Input i : aggregate.getInputs()) {
            validateInput(parents, i, result);
        }

        for (Output o : aggregate.getOutputs()) {
            validateOutput(parents, o, result);
        }

        if (checkChilds) {
            for (Aggregate child : aggregate.getChildAggregates()) {
                validation(parents, child, checkChilds, result);
            }
        }
    }

    private void validateAggregate(List<Aggregate> parents, Aggregate aggregate, ValidationResult result) {
        if (aggregate.getChildAggregates().isEmpty() && aggregate.getPlaces().isEmpty() &&
            aggregate.getTransitions().isEmpty() && aggregate.getQueues().isEmpty()) {

            result.addValidationError(new ValidationError(parents, aggregate,
                    "Aggregate is empty"));
        }        
    }

    private void validatePlace(List<Aggregate> parents, Place p, ValidationResult result) {
        ActiveInstance inputAI = p.getInputActiveInstance();
        ActiveInstance outputAI = p.getOutputActiveInstance();

        if (inputAI == null && outputAI == null) {
            result.addValidationError(new ValidationError(parents, p,
                    "Place should has input or output connection"));
        }
    }

    private void validateTransition(List<Aggregate> parents, Transition t, ValidationResult result) {
        List<Place> inputPlaces = t.getInputPlaces();
        List<Place> outputPlaces = t.getOutputPlaces();

        if (inputPlaces.isEmpty() || outputPlaces.isEmpty()) {
            result.addValidationError(new ValidationError(parents, t,
                    "Transition should has both input and output places connected to it"));
        }
    }

    private void validateQueue(List<Aggregate> parents, Queue q, ValidationResult result) {
        Place inputPlace = q.getInputPlace();
        Place outputPlace = q.getOutputPlace();

        if (inputPlace == null || outputPlace == null) {
            result.addValidationError(new ValidationError(parents, q,
                    "Queue should be connected to both input and output place"));
        }
    }

    private void validateInput(List<Aggregate> parents, Input i, ValidationResult result) {
        if (i.getOutputPlaces().isEmpty()) {
            logger.debug("Error: input not connected. i=" + i.getName());
            result.addValidationError(new ValidationError(parents, i,
                    "Input should be connected to a place inside aggregate"));
        }
    }

    private void validateOutput(List<Aggregate> parents, Output o, ValidationResult result) {
        if (o.getInputPlaces().isEmpty()) {
            logger.debug("Error: output not connected: " + o.getName());
            result.addValidationError(new ValidationError(parents, o,
                    "Output should be connected to a place inside aggregate"));
        }
    }
}
