package ua.cn.stu.cs.ems.core.xml;

/**
 *
 * @author proger
 */
public class PNMLParsingException extends Exception {

    public PNMLParsingException(String msg) {
        super(msg);
    }

    public PNMLParsingException(Throwable t) {
        super(t);
    }

}
