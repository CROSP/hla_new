package ua.cn.stu.cs.ems.core.experiment;

import java.util.List;

/**
 * Primary statistics result during one model run for a certain factor value
 *
 * @see Experiment
 * @author proger
 */
public class PrimaryStatisticsResult {
    // Factor value during model run

    private double factorValue;
    //Name of parent aggregate of object on which the primary statistics collected
    private String parentAggregate;
    //Name of object on which the primary statistics collected
    private String objectName;
    //Type of object on which the primary statistics collected
    private String objectType;
    public static final String OBJECT_TYPE_PLACE = "place";
    public static final String OBJECT_TYPE_TRANSITION = "transition";
    public static final String OBJECT_TYPE_QUEUE = "queue";
    // Result of the primary statistics dumps during model run
    private List<PrimaryStatisticsElement> primaryStatisticsElement;

    /**
     * Default constructor without parameters.
     */
    public PrimaryStatisticsResult() {
    }

    /**
     * Convenience constructor with parameters.
     *
     * @param theFactorValue
     * @param thePrimaryStatisticsElements
     */
    public PrimaryStatisticsResult(double theFactorValue, List<PrimaryStatisticsElement> thePrimaryStatisticsElements) {
        factorValue = theFactorValue;
        primaryStatisticsElement = thePrimaryStatisticsElements;
    }

    /**
     * Get factor value during model run.
     *
     * @return factor value during model run
     */
    public final double getFactorValue() {
        return factorValue;
    }

    /**
     * Get primary statistics element that were dumped during model run.
     *
     * @return the primaryStatisticsElement
     */
    public final List<PrimaryStatisticsElement> getDumpedPrimaryStatisticsElements() {
        return primaryStatisticsElement;
    }

    @Override
    public final boolean equals(Object obj) {
        if (obj instanceof PrimaryStatisticsResult) {
            PrimaryStatisticsResult other = (PrimaryStatisticsResult) obj;

            if (this.factorValue != other.factorValue) {
                return false;
            }

            if (!this.primaryStatisticsElement.equals(other.primaryStatisticsElement)) {
                return false;
            }

            return true;
        }

        return false;
    }

    /**
     * @return the parentAggregate
     */
    public String getParentAggregate() {
        return parentAggregate;
    }

    /**
     * @param parentAggregate the parentAggregate to set
     */
    public void setParentAggregate(String parentAggregate) {
        this.parentAggregate = parentAggregate;
    }

    /**
     * @return the objectName
     */
    public String getObjectName() {
        return objectName;
    }

    /**
     * @param objectName the objectName to set
     */
    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    /**
     * @return the objectType
     */
    public String getObjectType() {
        return objectType;
    }

    /**
     * @param objectType the objectType to set
     */
    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }
}