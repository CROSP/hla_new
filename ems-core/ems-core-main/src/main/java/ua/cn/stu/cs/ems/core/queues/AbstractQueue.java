package ua.cn.stu.cs.ems.core.queues;

import java.util.ArrayList;
import java.util.List;
import ua.cn.stu.cs.ems.core.AbstractActiveInstance;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.Token;
import static ua.cn.stu.cs.ems.core.utils.AssertUtils.assertNotNull;
import static ua.cn.stu.cs.ems.core.utils.AssertUtils.assertTrue;

/**
 * Parent for all queue implementations.
 * @author proger
 */
public abstract class AbstractQueue extends AbstractActiveInstance implements Queue {

    private Place inputPlace;
    private Place outputPlace;
    ArrayList<QueueStateListener> listeners = new ArrayList<QueueStateListener>();

    public AbstractQueue(String name) {
        super(name);
    }

    public List<Place> getInputPlaces() {
        return createListWithSinglePlace(inputPlace);
    }

    public Place getInputPlace() {
        return inputPlace;
    }

    public void connectInputPlace(Place input) {
        if (inputPlace != null) {
            inputPlace.setOutputActiveInstance(null);
        }

        inputPlace = input;
        if (input != null) {
            input.setOutputActiveInstance(this);
        }
    }

    public List<Place> getOutputPlaces() {
        return createListWithSinglePlace(outputPlace);
    }

    public Place getOutputPlace() {
        return outputPlace;
    }

    public void connectOutputPlace(Place place) {
        if (outputPlace != null) {
            outputPlace.setInputActiveInstance(null);
        }

        outputPlace = place;
        if (place != null) {
            place.setInputActiveInstance(this);
        }
    }

    public void fireIfPosible() {
        if (canTakeInputToken()) {
            delayFor(0);
            return;
        }

        if (isQueueEmpty()) {
            return;
        }

        if (canPutTokenToOutput()) {
            delayFor(0);
        }
    }

    public void placeStateChanged(Place place) {
        fireIfPosible();
    }

    protected void add(Token token) {
        addToken(token);
        notifyTokenkenAdded(token);
    }

    protected Token remove() {
        Token token = removeTokenFromQueue();
        notifyTokenRemoved(token);
        return token;
    }

    protected void fireTransition() {
        if (canTakeInputToken()) {
            add(inputPlace.getToken());
            inputPlace.setToken(null);
        }

        if (canPutTokenToOutput()) {
            Token token = remove();
            outputPlace.setToken(token);
        }
    }

    public void disconnectInputPlace(String name) {
        assertNotNull(inputPlace, "Unable to dissconnect input place when no input place is connected");
        assertTrue(inputPlace.getName().equals(name),
                "Imposible to disconnect input place with name '" + name + "' when input place's name is '" + inputPlace.getName() + "'");

        inputPlace = null;
    }

    public void disconnectOutputPlace(String name) {
        assertNotNull(outputPlace, "Unable to dissconnect output place when no input place is connected");
        assertTrue(outputPlace.getName().equals(name),
                "Imposible to disconnect output place with name '" + name + "' when output place's name is '" + outputPlace.getName() + "'");

        outputPlace = null;
    }

    private boolean canPutTokenToOutput() {
        return outputPlace != null && outputPlace.isEmpty() && !isQueueEmpty();
    }

    private boolean canTakeInputToken() {
        return inputPlace != null && !inputPlace.isEmpty();
    }

    /**
     *
     * @return true if queue is empty, false if not.
     */
    protected abstract boolean isQueueEmpty();

    /**
     * Peek first token from the queue.
     * @return first token according to a queue discipline
     */
    protected abstract Token removeTokenFromQueue();

    /**
     * Add token to a queue
     * @param token - token to add
     */
    abstract protected void addToken(Token token);

    public void addQueueStateListener(QueueStateListener listener) {
        assertNotNull(listener, "Queue state listener can't be null");

        listeners.add(listener);
    }

    public void removeQueueStateListener(QueueStateListener listener) {
        assertNotNull(listener, "Queue state listener can't be null");

        listeners.remove(listener);
    }

    /**
     * Notify all listeners about added token
     */
    private void notifyTokenkenAdded(Token token) {
        for (QueueStateListener listener : listeners) {
            listener.tokenAddedToQueue(this, token);
        }
    }

    private void notifyTokenRemoved(Token token) {
        for (QueueStateListener listener : listeners) {
            listener.tokenRemovedFromQueue(this, token);
        }
    }

    @Override
    public final String toString() {
        String result = "";
        result += this.getClass().getSimpleName() + " ";
        result += "Name: " + getName() + " ";
        result += "(" + "Inputs: ";

        for (Place pos : getInputPlaces()) {
            result += pos.getName() + ", ";
        }

        result += "Outputs: ";

        for (Place pos : getOutputPlaces()) {
            result += pos.getName() + ", ";
        }

        result += ")";
        return result;
    }
}
