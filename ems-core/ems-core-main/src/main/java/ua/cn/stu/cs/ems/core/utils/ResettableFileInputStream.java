/****************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one   *
 * or more contributor license agreements.  See the NOTICE file *
 * distributed with this work for additional information        *
 * regarding copyright ownership.  The ASF licenses this file   *
 * to you under the Apache License, Version 2.0 (the            *
 * "License"); you may not use this file except in compliance   *
 * with the License.  You may obtain a copy of the License at   *
 *                                                              *
 *   http://www.apache.org/licenses/LICENSE-2.0                 *
 *                                                              *
 * Unless required by applicable law or agreed to in writing,   *
 * software distributed under the License is distributed on an  *
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY       *
 * KIND, either express or implied.  See the License for the    *
 * specific language governing permissions and limitations      *
 * under the License.                                           *
 ****************************************************************/

package ua.cn.stu.cs.ems.core.utils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author  Federico Barbieri <fede@apache.org>
 */
public class ResettableFileInputStream extends InputStream {
    protected static final int DEFAULT_BUFFER_SIZE = 1024;

    protected final String fileName;
    protected int bufferSize;
    protected InputStream inputStream;
    protected long position;
    protected long mark;
    protected boolean isMarkSet;

    public ResettableFileInputStream( final File file ) throws IOException {
        this( file.getCanonicalPath() );
    }

    public ResettableFileInputStream(final String filename) throws IOException {
        this(filename, DEFAULT_BUFFER_SIZE);
    }

    public ResettableFileInputStream(final String filename, final int bufferSize) throws IOException {
        this.bufferSize = bufferSize;
        fileName = filename;
        position = 0;

        inputStream = newStream();
    }

    @Override
    public void mark(final int readLimit) {
        isMarkSet = true;
        mark = position;
        inputStream.mark(readLimit);
    }

    @Override
    public boolean markSupported() {
        return true;
    }

    @Override
    public void reset() throws IOException {
        if(!isMarkSet) {
            throw new IOException( "Unmarked Stream" );
        }
        try {
            inputStream.reset();
        }
        catch(final IOException ioe) {
            try {
                inputStream.close();
                inputStream = newStream();
                inputStream.skip(mark);
                position = mark;
            }
            catch( final Exception e ) {
                throw new IOException( "Cannot reset current Stream: " + e.getMessage() );
            }
        }
    }

    protected InputStream newStream() throws IOException {
        return new BufferedInputStream( new FileInputStream( fileName ), bufferSize );
    }

    @Override
    public int available() throws IOException {
        return inputStream.available();
    }

    @Override
    public void close() throws IOException {
        inputStream.close();
    }

    public int read() throws IOException {
        position++;
        return inputStream.read();
    }

    @Override
    public int read( final byte[] bytes, final int offset, final int length ) throws IOException {
        final int count = inputStream.read( bytes, offset, length );
        position += count;
        return count;
    }

    @Override
    public long skip( final long count ) throws IOException {
        position += count;
        return inputStream.skip( count );
    }
}