package ua.cn.stu.cs.ems.core.experiment.statistics;

import java.util.HashMap;
import java.util.Map;
import ua.cn.stu.cs.ems.core.aggregates.AggregateVariable;
import static ua.cn.stu.cs.ems.core.utils.AssertUtils.assertNotNull;

/**
 *
 * @author leonid
 */
public class AggregateVariableStateCollector {
    
    private AggregateVariable var;
    private HashMap<Double, String> values;
    
    public AggregateVariableStateCollector(AggregateVariable av) {
        assertNotNull(av);
        this.var = av;
        values = new HashMap<Double, String>();
    }
    
    public void save(double time) {
        values.put(time, var.getValue().strValue());
    }
    
    public Map<Double, String> getValues() {
        return new HashMap<Double, String> (values);
    }

    public String getValue(double time) {
        return values.get(time);
    }
}
