package ua.cn.stu.cs.ems.core.transitions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.utils.JessUtils;

/**
 *
 * @author proger
 */
public class YTransition extends AbstractSpecialTransition {

    final static Logger logger = LoggerFactory.getLogger(YTransition.class);
    Integer permitingFunctionResult;

    public YTransition(String name) {
        super(name);
    }

    @Override
    protected boolean canFire() {
        if (getOutputPlace() == null) {
            return false;
        }

        if (!getOutputPlace().isEmpty()) {
            return false;
        }

        permitingFunctionResult = getPermitingFunction().getPlaceNumber(this);
        logger.debug("In {} transition permiting function return {}",
                JessUtils.getAggregateChildFullName(this), permitingFunctionResult);

        return permitingFunctionResultCorrect(permitingFunctionResult);
    }

    protected void fireTransition() {
        Place inputPlace = getInputPlaces().get(permitingFunctionResult);
        Token token = getTransformationFunction().transform(this, new Token(inputPlace.getToken()));

        getOutputPlace().setToken(token);
        inputPlace.setToken(null);
    }

    public void connectOutputPlace(Place output) {
        if (getOutputPlaces().size() > 0) {
            replaceOutputPlace(output, 0);
        } else {
            addOutputPlace(output, 0);
        }
    }

    public Place getOutputPlace() {
        if (getOutputPlaces().size() > 0) {
            return getOutputPlaces().get(0);
        } else {
            return null;
        }
    }

    private boolean permitingFunctionResultCorrect(Integer result) {
        if (result == null) {
            return false;
        }

        if (result < 0 || result >= getInputPlaces().size()) {
            return false;
        }

        if (getInputPlaces().get(result).isEmpty()) {
            return false;
        }

        return true;
    }

    @Override
    protected int maxInputSize() {
        return UNLIMITED;
    }

    @Override
    protected int maxOutputSize() {
        return 1;
    }
}
