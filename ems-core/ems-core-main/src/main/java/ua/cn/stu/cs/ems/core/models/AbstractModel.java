package ua.cn.stu.cs.ems.core.models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.cn.stu.cs.ems.core.ActiveInstance;
import ua.cn.stu.cs.ems.core.aggregates.Aggregate;
import ua.cn.stu.cs.ems.core.transitions.SpecialTransition;
import static ua.cn.stu.cs.ems.core.utils.AssertUtils.*;
import ua.cn.stu.cs.ems.core.utils.JessUtils;

/**
 * @author proger
 */
public abstract class AbstractModel implements Model {

    private static final Logger logger = LoggerFactory.getLogger(AbstractModel.class);

    protected AbstractModel() {
    }
    private DelayedQueue delayedTransitionQueue = new DelayedQueue();
    private Set<SpecialTransition> specialTransitionSet = new HashSet<SpecialTransition>();
    protected Set<SpecialTransition> currentDelayedSpecialTransitions = new HashSet<SpecialTransition>();
    protected Set<ModelListener> listeners = new HashSet<ModelListener>();
    protected ArrayList<ModelTimer> timers = new ArrayList<ModelTimer>();
    private Aggregate rootAggregate;
    protected boolean isInterrupted;
    private double finishTime;

    public void setRootAggregate(Aggregate aggregate) {
        rootAggregate = aggregate;

        if (aggregate != null) {
            aggregate.setModel(this);
        }
    }

    public Aggregate getRootAggregate() {
        return rootAggregate;
    }

    public boolean addDelayable(ActiveInstance instance, double delay) {
        assertNotNull(instance);
        assertTrue(delay >= 0);

        if (instance instanceof SpecialTransition) {
            SpecialTransition st = (SpecialTransition) instance;

            if (currentDelayedSpecialTransitions.contains(st)) {
                return false;
            } else {
                currentDelayedSpecialTransitions.add(st);
            }
        }

        double wakeUpTime = delay + getModelingTime();
        delayedTransitionQueue.addDelayable(instance, wakeUpTime);

        logger.info("Obejct {} add to delay until time {}",
            JessUtils.getAggregateChildFullName(instance), wakeUpTime);

        return true;
    }

    public void addSpecialTransition(SpecialTransition transition) {
        assertNotNull(transition);
        specialTransitionSet.add(transition);
    }

    public void addModelListener(ModelListener listener) {
        assertNotNull(listener, "Listener can't be null");
        listeners.add(listener);
    }

    public void removeModelListener(ModelListener listener) {
        listeners.remove(listener);
    }

    public void addTimer(ModelTimeListener timeListener, double delay) {
        assertNotNull(timeListener, "Time listener can't be null");
        assertTrue(delay >= 0, "Can't set timer with negative delay time");

        ModelTimer mt = new ModelTimer(timeListener, getModelingTime() + delay);

        int pos = Collections.binarySearch(timers, mt);
        if (pos < 0) {
            int insertPos = -(pos + 1);
            timers.add(insertPos, mt);
        }
    }

    public void reset() {
        isInterrupted = false;
        delayedTransitionQueue.clear();
        specialTransitionSet.clear();
        currentDelayedSpecialTransitions.clear();
        timers.clear();
        specializedReset();
    }

    protected abstract void specializedReset();

    protected Set<SpecialTransition> getSpecialTransitionSet() {
        return specialTransitionSet;
    }

    protected DelayedQueue getDelayedQueue() {
        return delayedTransitionQueue;
    }

    protected void notifyModelStarted() {
        for (ModelListener l : listeners) {
            l.modelStarted(this);
        }
    }

    protected void notifyModelFinished() {
        for (ModelListener l : listeners) {
            l.modelFinished(this);
        }
    }

    protected void notifyModelTimeChanged() {
        for (ModelListener l : listeners) {
            l.modelTimeChanged(this, getModelingTime());
        }
    }

    public void interrupt() {
        isInterrupted = true;
    }

    protected boolean isInterrupted() {
        return isInterrupted;
    }

    public void setFinishTime(double finishTime) {
        this.finishTime = finishTime;
    }

    public double getFinishTime() {
        return finishTime;
    }
}

class ModelTimer implements Comparable<ModelTimer> {

    ModelTimeListener listener;
    double time;

    public ModelTimer(ModelTimeListener listener, double time) {
        this.listener = listener;
        this.time = time;
    }

    public int compareTo(ModelTimer mt) {
        double res = time - mt.time;

        if (res > 0) {
            return 1;
        } else if (res < 0) {
            return -1;
        } else {
            return 0;
        }
    }

    @Override
    public String toString() {
        return "Timer wait for " + time;
    }
}