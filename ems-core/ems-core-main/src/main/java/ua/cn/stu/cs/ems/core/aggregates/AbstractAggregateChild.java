package ua.cn.stu.cs.ems.core.aggregates;
import static ua.cn.stu.cs.ems.core.utils.AssertUtils.assertNotNull;
import ua.cn.stu.cs.ems.core.utils.Coordinates;
/**
 *
 * Implementation of the methods of AggregateChild interface
 * @author proger
 */
public abstract class AbstractAggregateChild implements AggregateChild {
    protected String name;
    protected Aggregate parentAggregate;

    private Coordinates objectCoordinates;
    private Coordinates nameCoordinates;

    protected AbstractAggregateChild(String name) {
        assertNotNull(name, "Name can't be null");
        this.name = name;
    }


    public boolean changeName(String newName) {
        assertNotNull(newName, "ActiveInstance's name can't be null");

        if (name.equals(newName)) {
            return true;
        }

        if (parentAggregate != null &&
            parentAggregate.getAggregateChild(newName) != null) {
                return false;
        }

        String oldName = this.name;
        this.name = newName;
        notifyNameChanged(oldName, newName);
        return true;
    }

    public String getName() {
        return name;
    }

    public Aggregate getParentAggregate() {
        return parentAggregate;
    }

    public void setParentAggregate(Aggregate aggregate) {
        parentAggregate = aggregate;
    }

    
    protected void notifyNameChanged(String oldName, String newName) {
        if (parentAggregate != null)
            parentAggregate.childNameChanged(this, oldName, newName);
    }

    public Coordinates getNameCoordinates() {
        return nameCoordinates;
    }

    public void setNameCoordinates(Coordinates newCoordinates) {
        assertNotNull(newCoordinates, "Coordinates can't be null");
        nameCoordinates = newCoordinates;
    }

    public Coordinates getObjectCoordinates() {
        return objectCoordinates;
    }

    public void setObjectCoordinates(Coordinates newCoordinates) {
        assertNotNull(newCoordinates, "Coordinates can't be null");
        objectCoordinates = newCoordinates;
    }
}
