package ua.cn.stu.cs.ems.core.queues;

/**
 * FIFO queue - queue that implements FIFO discipline.
 * @author proger
 */
public class FIFOQueue extends NonPriorityQueue {

    /**
     * Create FIFO queue with a specified name.
     * @param name - name for a new queue.
     */
    public FIFOQueue(String name) {
        super(name);
    }

    @Override
    protected final int tookenNumberToPeek() {
        return 0;
    }

}
