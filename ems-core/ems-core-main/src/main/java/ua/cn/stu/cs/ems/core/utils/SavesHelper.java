package ua.cn.stu.cs.ems.core.utils;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import org.jdom.Document;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import ua.cn.stu.cs.ems.core.Constants;
import ua.cn.stu.cs.ems.core.aggregates.AggregateRegistry;
import ua.cn.stu.cs.ems.core.xml.AggregateTypesReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.cn.stu.cs.ems.core.aggregates.Aggregate;
import ua.cn.stu.cs.ems.core.aggregates.AggregateDefinition;
import ua.cn.stu.cs.ems.core.aggregates.AggregateDefinitionReference;
import ua.cn.stu.cs.ems.core.xml.AggregateBuilder;
import ua.cn.stu.cs.ems.core.xml.AggregateRegistryWriter;
import ua.cn.stu.cs.ems.core.xml.AggregateWriter;
import ua.cn.stu.cs.ems.core.xml.PNMLParsingException;

/**
 *
 * @author leonid
 */
public class SavesHelper {

    final static Logger logger = LoggerFactory.getLogger(SavesHelper.class);

    public static AggregateRegistry readAggregateTypes(File dir) {
        File[] aggregateDefinitions = dir.listFiles(new ExtensionFilter(
                Constants.AGGREGATE_PNML_EXTENSION));
        AggregateTypesReader atr = new AggregateTypesReader();
        try {
            return atr.readAggregateTypes(aggregateDefinitions);
        } catch (Exception e) {
            String msg = "Failed to read aggregate types";
            logger.error(msg, e);
            return null;
        }
    }

    private static void readAggregateDefinations(AggregateDefinition ad,
            AggregateRegistry ar) {
        for (Aggregate a : ad.getChildAggregates()) {
            AggregateDefinition adr = ((AggregateDefinitionReference) a).getAggregateDefinition();
            ar.addAggregateDefinition(adr);
            readAggregateDefinations(adr, ar);
        }
    }

    public static AggregateDefinition readModel(File modelFile,
            AggregateRegistry library) throws JDOMException, IOException,
            PNMLParsingException {
        AggregateBuilder ab = new AggregateBuilder();
        Document dom = new SAXBuilder().build(new FileReader(modelFile));
        return ab.buildAggregateDefinition(dom, library);
    }

    public static void saveAggregateType(AggregateDefinition aggregate,
            File path, boolean full) throws IOException {
        AggregateRegistry arr = new AggregateRegistry();
        AggregateRegistryWriter arw = new AggregateRegistryWriter();
        arr.addAggregateDefinition(aggregate);

        if (full) {
            readAggregateDefinations(aggregate, arr);
        }

        logger.debug("writing aggregate type: " + aggregate + " to path: " + path);
        arw.saveAggregateRegistry(arr, path);
    }

    public static void saveAggregate(AggregateDefinition aggregate, File file) 
            throws IOException {
        logger.debug("Saving aggregate: " + aggregate.getName() + " file: " + file);
        FileWriter fw = new FileWriter(file);

        AggregateWriter aw = new AggregateWriter();
        Document dom = aw.createDOMFromAggregateDefinition(aggregate);
        XMLOutputter outputter = new XMLOutputter();
        Format prettyFormat = Format.getPrettyFormat();
        outputter.setFormat(prettyFormat);

        logger.debug("writing aggregate " + aggregate.getName());
        fw.write(outputter.outputString(dom));
        fw.close();
    }
}
