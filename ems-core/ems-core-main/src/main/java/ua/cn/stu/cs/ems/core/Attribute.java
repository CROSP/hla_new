package ua.cn.stu.cs.ems.core;

import ua.cn.stu.cs.ems.ecli.Value;

/**
 * Token's attribute.
 * @author proger
 */
public class Attribute {

    private String name;
    private Value value;

    /**
     * Create an attribute with specified name and value
     * @param theName - name of a new attribute
     * @param theValue - value of a new attribute.
     */
    public Attribute(String theName, Value theValue) {
        name = theName;
        value = theValue;
    }

    /**
     * Get attribute's name.
     * @return attribute's name
     */
    public final String getName() {
        return name;
    }

    /**
     * Set attribute's name.
     * @param newName new attribute's name
     */
    public final void setName(String newName) {
        name = newName;
    }

    /**
     * Get attribute's value.
     * @return attribute's value
     */
    public final Value getValue() {
        return value;
    }

    /**
     * Set attribute's value.
     * @param newValue attribute's value
     */
    public final void setValue(Value newValue) {
        value = newValue;
    }
}
