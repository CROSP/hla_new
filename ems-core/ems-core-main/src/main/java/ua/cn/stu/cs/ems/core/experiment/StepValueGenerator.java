package ua.cn.stu.cs.ems.core.experiment;

/**
 * Simple value generator that generates sequence of values from some initial value
 * to an end value with a specified step.
 * For init = 0; end = 3; step = 1. It will generate sequence: 0, 1, 2
 * 
 * @author proger
 */
public class StepValueGenerator implements ValueGenerator {

    private double initValue;
    private double endValue;
    private double stepValue;
    private double currentValue;
    

    public StepValueGenerator(double init, double end, double step) {
        initValue = init;
        endValue  = end;
        stepValue = step;

        currentValue = initValue;
    }

    public final boolean hasMore() {
        return currentValue < endValue;
    }

    public final double nextValue() {
        double newValue = currentValue;
        currentValue += stepValue;
        return newValue;
    }

}
