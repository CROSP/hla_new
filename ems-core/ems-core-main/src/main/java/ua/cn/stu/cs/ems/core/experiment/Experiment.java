package ua.cn.stu.cs.ems.core.experiment;

import java.util.List;
import java.util.Set;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.aggregates.AggregateChild;
import ua.cn.stu.cs.ems.core.models.Model;
import ua.cn.stu.cs.ems.core.queues.Queue;
import ua.cn.stu.cs.ems.core.transitions.Transition;

/**
 * Experiment - interface for all forms of experiment. <p> The purpose of any
 * experiment is to collect statistics about model runs. There are two forms of
 * statistics: <ul> <li> Primary statistics (not mandatory) <li> Secondary
 * statistics (mandatory) </ul> <p> During experiment specified model is run
 * several times. Before each run a variable that is specified by a factor is
 * set. Number of model runs with a single factor value is specified by an
 * Experiment implementation. <p>
 *
 * Secondary statistics is just a value of a respond variable for each factor
 * value. For example, if respond_value = factor_value ^ 2 and factor values
 * are: 1, 2, 3, 4 following respond_values will be received after each run: 1,
 * 4, 9, 16. In real model relationship between factor and respond are usually
 * more difficult and respond value can change from one model run to another,
 * without factor value changes. <p>
 *
 * Primary statistics is collected by a dedicated statistics collector for each
 * element in a model. What and how values are collected for each element
 * depends on StatisticsCollector implementation. Primary statistics is dumped
 * periodically. Period of statistics dump is specified by a user. <p>
 *
 * If system is in a transient response it isn't recommended to select
 * statistics during it's period. To avoid collecting statistics during this
 * period user can specify delay from a model start during which statistics
 * won't be collected. <p>
 *
 * Also user can decide not to select primary statistics at all.
 *
 * @see StatisticsCollector
 * @see FactorImpl
 * @author proger
 */
// TODO Write normal description of Experiment.
public interface Experiment {

    /**
     * Start experiment.
     */
    void startExperiment();

    /**
     * Stop experiment.
     */
    void stopExperiment();

    /**
     * One step of experiment.
     */
    void stepExperiment();

    /**
     * Stop step experiment
     */
    void stopStepExperiment();

    /**
     * Get model that is used to run experiment.
     *
     * @return model that is used to run experiment.
     */
    Model getModel();

    /**
     * Select statistics on a specified place during experiment run.
     *
     * @param place - place to collect statistics on.
     */
    void collectStatisticsOn(Place place);

    /**
     * Select statistics on a specified transition during experiment run.
     *
     * @param transition - transition to select statistics on.
     */
    void collectStatisticsOn(Transition transition);

    /**
     * Select statistics on a specified queue during experiment run.
     *
     * @param queue - queue to select statistics on.
     */
    void collectStatisticsOn(Queue queue);

    /**
     * Select secondary statistics on a specified place during experiment run.
     *
     * @param place - place to collect secondary statistics on.
     */
    void collectSecondaryStatisticsOn(Place place);

    /**
     * Select secondary statistics on a specified transition during experiment
     * run.
     *
     * @param transition - transition to select secondary statistics on.
     */
    void collectSecondaryStatisticsOn(Transition transition);

    /**
     * Select secondary statistics on a specified queue during experiment run.
     *
     * @param queue - queue to select secondary statistics on.
     */
    void collectSecondaryStatisticsOn(Queue queue);

    /**
     * Get factor that is used by this experiment.
     *
     * @return factor that is used by this experiment.
     */
    Factor getFactor();

    /**
     * Get respond variable.
     *
     * @return respond variable.
     */
    Respond getRespond();

    /**
     * Get collected secondary statistics.
     *
     * @return collected secondary statistics.
     */
    List<SecondaryStatisticsElement> getSecondaryStatistics();

    /**
     * Get collected primary statistics for an aggregate child.
     *
     * @param element - aggregate child to collect primary statistics for
     * @return collected primary statistics for an aggregate child
     */
    List<PrimaryStatisticsResult> getPrimaryStatisticsFor(AggregateChild element);

    /**
     * Set primary statistics dump period.
     *
     * @param period - primary statistics dump period
     */
    void setPrimaryStatisticsDumpPeriod(double period);

    double getPrimaryStatisticsDumpPeriod();

    /**
     * Set delay before primary statistics collection.
     *
     * @param delay - delay before primary statistics collection
     */
    void setDelayBeforeCollectingPrimaryStatistics(double delay);

    double getDelayBeforeCollectingPrimaryStatistics();

    /**
     * Set if primary statistics should be collected.
     *
     * @param toCollect - true if primary statistics should be collected, false
     * otherwise
     */
    void setIsPrimaryStatisticsShouldBeCollected(boolean toCollect);

    boolean isPrimaryStatisticsShouldBeCollected();

    /**
     * Get elements to select statistics on during the experiment.
     *
     * @return - set of objects to select statistics
     */
    Set<AggregateChild> getStatisticsObjects();

    /**
     * Get report about an experiment run. Report contains primary and secondary
     * statistics if it was collected.
     *
     * @return report about an experiment run.
     */
    ExperimentReport getReport();

    /**
     * Get status of experiment
     *
     * @return true if experiment finished, otherwise false.
     */
    boolean isExperimentFinished();

    void setCollectedStatOnEvent(boolean onEvent);
}
