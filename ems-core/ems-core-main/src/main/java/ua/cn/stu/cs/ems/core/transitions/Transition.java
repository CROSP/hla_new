package ua.cn.stu.cs.ems.core.transitions;

import ua.cn.stu.cs.ems.core.transitions.functions.TransformationFunction;
import ua.cn.stu.cs.ems.core.transitions.functions.DelayFunction;
import ua.cn.stu.cs.ems.core.ActiveInstance;
import ua.cn.stu.cs.ems.core.Place;

/**
 * Transitions - interface that represent operations specific to transitions
 * in E nets.
 */
public interface Transition extends ActiveInstance {

    /**
     * Set transformation function for this transition.
     * @param transformationFunction - transformation function, can't be null.
     */
    public void setTransformationFunction(TransformationFunction transformationFunction);

    /**
     * Get transformation function for this transition/
     * @return transformation function
     */
    public TransformationFunction getTransformationFunction();

    /**
     * Get delay function for this transition.
     * @return delay function
     */
    public DelayFunction getDelayFunction();

    /**
     * Set delay function for this transition
     * @param delay function. Can't be null.
     */
    public void setDelayFunction(DelayFunction function);

    /**
     * Add input place.
     * @param place - input place to add.
     * @param pos - number in list of input places.
     */
    void addInputPlace(Place place, int pos);

    /**
     * Disconnects an input place with specified name from a transition and
     * disconnects transition from the place
     * @param placeName - name of a place to disconnect
     */
    void disconnectInputPlace(String placeName);

    /**
     * Add output place.
     * @param place - output place to add.
     * @param pos - number in list of ouput places.
     */
    void addOutputPlace(Place place, int pos);

    /**
     * Disconnects an output place with specified name from a transition and 
     * disconnects transition from the place
     * @param placeName - name of a place to disconnect
     */
    void disconnectOutputPlace(String placeName);

    /**
     * Replace Input place
     * @param newPlace - new place.
     * @param pos - number of place to replace.
     */
    void replaceInputPlace(Place newPlace, int pos);

    /**
     * Replace output place
     * @param newPlace - new place.
     * @param pos - number of place to replace.
     */
    void replaceOutputPlace(Place newPlace, int pos);

    public int numberOfConnectedInputPlaces();

    public int numberOfConnectedOutputPlaces();
}
