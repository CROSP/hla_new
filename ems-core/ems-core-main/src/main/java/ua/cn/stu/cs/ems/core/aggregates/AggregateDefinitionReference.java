package ua.cn.stu.cs.ems.core.aggregates;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.models.Model;
import ua.cn.stu.cs.ems.core.queues.Queue;
import ua.cn.stu.cs.ems.core.transitions.Transition;
import ua.cn.stu.cs.ems.ecli.Value;

/**
 * To store aggregate's structure we need to show that some aggregates types has
 * one or more child aggregates inside them. Because only one place can be connected to
 * one input/output we use this class. It holds coppy of inputs/outputs of a specific aggregate
 * type definition.
 * @author proger
 */
// TODO What if aggregate defintion's input/outpus already connected to some places? We should handle
// this
public class AggregateDefinitionReference extends AbstractAggregateChild implements Aggregate {

    final static Logger logger = LoggerFactory.getLogger(AggregateDefinitionReference.class);
    
    private AggregateDefinition aggregate;

    private ArrayList<Input> inputs = new ArrayList<Input>();
    private ArrayList<Output> outputs = new ArrayList<Output>();
    
    // used as dublicated, real aggregates cant be modified
    private List<Aggregate> childAggregates = new ArrayList<Aggregate>();
    private List<Place> places = new ArrayList<Place>();
    
    public AggregateDefinitionReference(String name, AggregateDefinition aggregate) {
        super(name);

//        logger.debug("Creating link to " + aggregate);
        this.aggregate = aggregate;
        
        for (Aggregate a: aggregate.getChildAggregates()) {
            AggregateDefinitionReference adr = (AggregateDefinitionReference) a;
            AggregateDefinitionReference copy = new AggregateDefinitionReference(a.getName(), adr.getAggregateDefinition());
            copy.setParentAggregate(this);
            childAggregates.add(copy);
        }

        for (Output o : aggregate.getOutputs()) {
            Output newOutput = new OutputImpl(o.getName());
            newOutput.setParentAggregate(this);
            outputs.add(newOutput);
        }
        
//        logger.debug("Adding inputs as dublicats");
        for (Input i : aggregate.getInputs()) {
            Input newInput = new InputImpl(i.getName());
            newInput.setParentAggregate(this);
            if (i.getInputPlace() != null) {
//                logger.debug("need set that has input place: input=" + i.getName() + " to " + i.getInputPlace());
                newInput.setInputPlace(new Place(i.getInputPlace().getName()));
            }
            inputs.add(newInput);
        }
    }
    
    public AggregateDefinition getAggregateDefinition() {
        return aggregate;
    }

    public void addInput(Input input, int pos) {
        inputs.add(pos, new InputImpl(input.getName()));

        aggregate.addInput(input, pos);
    }

    public boolean disconnectInput(Input input) {
        if (input.getParentAggregate() != this) {
            return false;
        }
        
        input.setInputPlace(null);
        input.connectOutput(null);
        return true;
    }

    public List<Input> getInputs() {
        return new ArrayList<Input>(inputs);
    }

    public Input getInput(String name) {
        for (Input i : inputs) {
            if (i.getName().equals(name)) {
                return i;
            }
        }
        return null;
    }

    public Integer getInputNumber(String name) {
        int j = 0;
        for (Input i : inputs) {
            if (i.getName().equals(name)) {
                return j;
            }
            j++;
        }

        return null;
    }

    public void addOutput(Output output, int pos) {
        outputs.add(new OutputImpl(name));

        aggregate.addOutput(output, pos);
    }

    public boolean disconnectOutput(Output output) {
        if (output.getParentAggregate() != this) {
            return false;
        }
        
        output.connectOutputPlace(null);
        
        for (int i=0;i<output.getConnectedInputs().size();i++) {
            output.disconnectInput(0);
        }
        
        return true;
        
    }

    public List<Output> getOutputs() {
         return new ArrayList<Output>(outputs);
    }

    public Output getOutput(String name) {
        for (Output o : outputs) {
            if (o.getName().equals(name)) {
                return o;
            }
        }
        return null;
    }

    public Integer getOutputNumber(String name) {
        int i = 0;
        for (Output o : outputs) {
            if (o.getName().equals(name)) {
                return i;
            }
            i++;
        }

        return null;
    }

    public void addTransition(Transition transition) {
        aggregate.addTransition(transition);
    }

    public void addTransitions(Transition... transitions) {
        aggregate.addTransitions(transitions);
    }

    public boolean disconnectChildTransition(Transition transition) {
        return aggregate.disconnectChildTransition(transition);
    }

    public Set<Transition> getTransitions() {
        return aggregate.getTransitions();
    }

    public Transition getTransition(String name) {
        return aggregate.getTransition(name);
    }

    public void addPlace(Place place) {
        aggregate.addPlace(place);
    }

    public void addPlaces(Place... places) {
        aggregate.addPlaces(places);
    }

    public Place getPlace(String name) {
        return aggregate.getPlace(name);
    }

    public void disconnectChildPlace(Place place) {
        aggregate.disconnectChildPlace(place);
    }

    public Set<Place> getPlaces() {
        return aggregate.getPlaces();
    }

    public void addQueue(Queue queue) {
        aggregate.addQueue(queue);
    }

    public void addQueues(Queue... queues) {
        aggregate.addQueues(queues);
    }

    public Set<Queue> getQueues() {
        return aggregate.getQueues();
    }

    public Queue getQueue(String name) {
        return aggregate.getQueue(name);
    }

    public boolean disconnectQueue(Queue queue) {
        return aggregate.disconnectQueue(queue);
    }

    @Override
    public Aggregate getAggregate(String name) {
        for (Aggregate a: childAggregates) {
            if (a.getName().equals(name)) {
                return a;
            }
        }
        return null;
    }

    @Override
    public Set<Aggregate> getChildAggregates() {
        Set<Aggregate> result = new HashSet<Aggregate>(childAggregates);
        return result;
    }

    @Override
    public boolean disconnectChildAggregate(Aggregate child) {
        childAggregates.remove(child);
        return aggregate.disconnectChildAggregate(aggregate.getAggregate(child.getName()));
    }

    public Model getModel() {
        return aggregate.getModel();
    }

    public void setModel(Model model) {
        aggregate.setModel(model);
    }

    public void setVariable(String name, Value value) {
        aggregate.setVariable(name, value);
    }

    public AggregateVariable getVariable(String name) {
        return aggregate.getVariable(name);
    }

    public Set<String> getVariablesNames() {
        return aggregate.getVariablesNames();
    }

    public boolean removeVariable(String name) {
        return aggregate.removeVariable(name);
    }

    public void clearVariables() {
        aggregate.clearVariables();
    }

    @Override
    public AggregateChild getAggregateChild(String name) {
        for (Input i : inputs) {
            if (i.getName().equals(name)) {
                return i;
            }
        }

        for (Output o : outputs) {
            if (o.getName().equals(name)) {
                return o;
            }
        }
        
        for (Aggregate a: childAggregates) {
            if (a.getName().equals(name)) {
                return a;
            }
        }

        return aggregate.getAggregateChild(name);
    }

    @Override
    public void childNameChanged(AggregateChild child, String oldName, String newName) {
        aggregate.childNameChanged(child, oldName, newName);
        if (child instanceof Input) {
            getInput(oldName).changeName(newName);
        }
        else if (child instanceof Output) {
            getOutput(oldName).changeName(newName);
        }
        else if (child instanceof Place) {
            getPlace(oldName).changeName(newName);
        }
        else if (child instanceof Aggregate) {
            getAggregate(name).changeName(newName);
        }
    }
                
    @Override
    public boolean changeName(String name) {
        return aggregate.changeName(name);
    }
}
