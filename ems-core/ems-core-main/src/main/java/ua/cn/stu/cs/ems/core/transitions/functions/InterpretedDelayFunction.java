package ua.cn.stu.cs.ems.core.transitions.functions;

import ua.cn.stu.cs.ems.core.InterpretedFunction;
import java.io.IOException;
import ua.cn.stu.cs.ems.core.transitions.Transition;
import org.codehaus.plexus.util.StringInputStream;
import ua.cn.stu.cs.ems.core.JessException;
import ua.cn.stu.cs.ems.core.ecli.EcliModelAccessor;
import ua.cn.stu.cs.ems.ecli.EclInterpreter;
import ua.cn.stu.cs.ems.ecli.InterpretationReport;

/**
 * InterpretedDelayFunction - function to calculate transition's delay due to algorithm defined by ecli function.
 * @author proger
 */
public class InterpretedDelayFunction extends InterpretedFunction implements DelayFunction {
    
    private EclInterpreter interpreter = new EclInterpreter();

    /**
     * Create delay function that calculates delay time according to an algorithm specified in a source.
     * @param source - source that specified algorithm for a delay time calculation.
     */
    public InterpretedDelayFunction(String source) {
        super(source);
    }

    public final double getDelayTime(Transition transition) {
        final EcliModelAccessor ema = new EcliModelAccessor(transition.getParentAggregate().getModel(), 
                                                      transition.getParentAggregate());
        try {
            final InterpretationReport ir = interpreter.interpret(new StringInputStream(source), ema);
            
            return  ir.getResult().doubleValue();           
        } 
        catch (IOException ex) {
            throw new JessException("IOException during interpreted delay function execution", ex);
        }
    }

    public final DelayFunction copy() {
        return new InterpretedDelayFunction(source);
    }
}
