package ua.cn.stu.cs.ems.core.transitions.functions;

import ua.cn.stu.cs.ems.core.transitions.Transition;

/**
 * Default delay function. It always returns 0 value of a delay.
 * @author proger
 */
public class DefaultDelayFunction implements DelayFunction {

    public DefaultDelayFunction() {
    }

    public final double getDelayTime(Transition transition) {
        return 0;
    }

    @Override
    public final boolean equals(Object o) {
        return (o instanceof DefaultDelayFunction);
    }

    public final DelayFunction copy() {
        return new DefaultDelayFunction();
    }

    @Override
    public final int hashCode() {
        int hash = 3;
        return hash;
    }

}
