package ua.cn.stu.cs.ems.core.transitions;

import ua.cn.stu.cs.ems.core.AbstractTransition;
import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.Place;

/**
 *
 * @author proger
 */
public class JTransition extends AbstractTransition {

    public JTransition(String name) {
        super(name);
    }

    @Override
    protected boolean canFire() {
        for (Place pos : getInputPlaces())
            if (pos.isEmpty())
                return false;

        if (! getOutputPlace().isEmpty())
            return false;

        return true;
    }

    protected void fireTransition() {
        Token inputToken = getInputPlaces().get(0).getToken();
        Token resultToken = getTransformationFunction().transform(this, new Token(inputToken));

        getOutputPlace().setToken(resultToken);

        for (Place pos : getInputPlaces())
            pos.setToken(null);
    }

    public void setOutputPlace(Place place) {
        if (getOutputPlaces().isEmpty())
            super.addOutputPlace(place, 0);
        else
            super.replaceOutputPlace(place, 0);
    }

    public Place getOutputPlace() {
        return getOutputPlaces().get(0);
    }

    @Override
    protected int maxInputSize() {
        return UNLIMITED;
    }

    @Override
    protected int maxOutputSize() {
        return 1;
    }

}
