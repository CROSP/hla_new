package ua.cn.stu.cs.ems.core.utils;

/**
 *
 * @author proger
 */
public class AssertUtils {
    public static void assertNotNull(Object obj) {
        assertNotNull(obj, "");
    }

    public static void assertNotNull(Object obj, String msg) {
        if (obj == null)
            throw new NullPointerException(msg);
    }

    public static void assertNull(Object obj) {
        assertNotNull(obj, "");
    }

    public static void assertNull(Object obj, String msg) {
        if (obj != null)
            throw new RuntimeException(msg);
    }    

    public static void assertTrue(boolean expr) {
        assertTrue(expr, "");
    }

    public static void assertTrue(boolean expr, String msg) {
        if (!expr)
            throw new RuntimeException(msg);
    }

    public static void assertFalse(boolean expr) {
        assertFalse(expr, "");
    }

    public static void assertFalse(boolean expr, String msg) {
        if (expr)
            throw new RuntimeException(msg);
    }

    public static void assertBounds(int val, int min, int max) {
        assertBounds(val, min, max, "");
    }

    public static void assertBounds(int val, int min, int max, String msg) {
        if (val < min || val > max)
            throw new IndexOutOfBoundsException(msg);
    }


}
