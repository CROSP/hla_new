package ua.cn.stu.cs.ems.core.aggregates;

/**
 *
 * @author proger
 */
public class NameClashException extends RuntimeException {
    public NameClashException() {
        super();
    }

    public NameClashException(String msg) {
        super(msg);
    }
}
