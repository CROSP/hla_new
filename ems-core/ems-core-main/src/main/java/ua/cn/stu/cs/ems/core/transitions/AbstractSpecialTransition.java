package ua.cn.stu.cs.ems.core.transitions;

import ua.cn.stu.cs.ems.core.AbstractTransition;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.aggregates.Aggregate;
import ua.cn.stu.cs.ems.core.models.Model;
import ua.cn.stu.cs.ems.core.transitions.functions.DefaultPermitingFunction;
import ua.cn.stu.cs.ems.core.transitions.functions.PermitingFunction;
import static ua.cn.stu.cs.ems.core.utils.AssertUtils.assertNotNull;
/**
 *
 * @author proger
 */
public abstract class AbstractSpecialTransition extends AbstractTransition implements SpecialTransition {

    private PermitingFunction permitingFunction;

    protected AbstractSpecialTransition(String name) {
        super(name);

        permitingFunction = new DefaultPermitingFunction();
    }

    /**
     * @return the permitingFunction
     */
    public PermitingFunction getPermitingFunction() {
        return permitingFunction;
    }

    /**
     * @param permitingFunction the permitingFunction to set
     */
    public void setPermitingFunction(PermitingFunction permitingFunction) {
        assertNotNull(permitingFunction);
        this.permitingFunction = permitingFunction;

    }

    @Override
    public void placeStateChanged(Place place) {
        Aggregate pAggregate = getParentAggregate();
        if (pAggregate == null) return;

        Model model = pAggregate.getModel();
        if (model == null) return;

        model.addSpecialTransition(this);
    }
    
}
