package ua.cn.stu.cs.ems.core.ecli;

import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.ecli.Value;

import java.util.HashMap;
import java.util.Map;

/**
 * Accessor that does not commit changes to the model. Should be used for testing purposes only.
 *
 * @author n0weak
 */
public class IdleTokenAccessor extends EcliTokenAccessor {

    private final Map<String, Object> modifiedData = new HashMap<String, Object>();

    public IdleTokenAccessor(Token token) {
        super(token);
    }

    @Override
    public boolean containsAttribute(Object key) {
        return modifiedData.containsKey(String.valueOf(key)) || super.containsAttribute(key);
    }

    @Override
    public boolean setAttribute(Object key, Value value) {
        modifiedData.put(String.valueOf(key), value.getValue());
        return true;
    }

    @Override
    public Object getAttribute(Object key) {
        Object value = modifiedData.get(String.valueOf(key));
        return null != value ? value : super.getAttribute(key);
    }
}
