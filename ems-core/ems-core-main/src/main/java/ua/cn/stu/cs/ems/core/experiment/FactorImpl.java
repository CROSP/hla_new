package ua.cn.stu.cs.ems.core.experiment;

import java.util.ArrayList;
import ua.cn.stu.cs.ems.core.aggregates.AggregateVariable;
import ua.cn.stu.cs.ems.ecli.Value;
import static ua.cn.stu.cs.ems.core.utils.AssertUtils.*;

/**
 * FactorImpl is an object that saves a value that will be changed during an experiment.
 * 
 * @author proger
 */
public class FactorImpl implements Factor {

    // variable that will be changed during an experiment
    AggregateVariable variable;
    // values that will be assigned to a variable
    Double[] values = new Double[0];
    //Factor's name
    String name;
    //Number of the current value
    int currentValueNumber = -1;

    /**
     * Create factor with given variables and values.
     * @param theVariable - variable of a factor.
     * @param valueGenerator - object that generates values for a variable
     */
    public FactorImpl(AggregateVariable theVariable, ValueGenerator valueGenerator) {
        assertNotNull(theVariable, "Variable can't be null");
        assertNotNull(valueGenerator, "Value generator can't be null");

        variable = theVariable;

        ArrayList<Double> generatorValues = new ArrayList<Double>();
        while (valueGenerator.hasMore()) {
            generatorValues.add(valueGenerator.nextValue());
        }

        values = generatorValues.toArray(values);
    }

    /**
     * Change current factor value.
     */
    public final void nextValue() {
        currentValueNumber = (++currentValueNumber) % numberOfValues();
    }

    /**
     * Set current factor value to an aggregate variable.
     */
    public final void setValue() {
        variable.setValue(new Value(getCurrentValue()));
    }

    /**
     * Get number of the current value.
     * @return number of the current value
     */
    public final int getValueNumber() {
        return currentValueNumber;
    }

    /**
     * Get current factor value.
     * @return current factor value.
     */
    public final double getCurrentValue() {
        return values[currentValueNumber];
    }

    /**
     * Get number of values in this factor.
     * @return number of values in this factor
     */
    public final int numberOfValues() {
        return values.length;
    }

    /**
     * Get variable that is changed by this factor.
     * @return variable that is changed by this factor
     */
    public final AggregateVariable getVariable() {
        return variable;
    }

    /**
     * Check if this factor has more values.
     * @return true if it has more values to set, false otherwise.
     */
    public final boolean hasMoreValues() {
        return currentValueNumber < values.length - 1;
    }

    /**
     * Get name of factor
     * @return name of factor
     */
    public final String getFactorName() {
        return name;
    }

    /**
     * Set name of factor
     */
    public final void setFactorName(String factorName) {
        name = factorName;
    }
}