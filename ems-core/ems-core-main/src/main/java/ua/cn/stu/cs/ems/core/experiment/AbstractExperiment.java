package ua.cn.stu.cs.ems.core.experiment;

import java.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.aggregates.AggregateChild;
import ua.cn.stu.cs.ems.core.experiment.statistics.*;
import ua.cn.stu.cs.ems.core.models.Model;
import ua.cn.stu.cs.ems.core.models.ModelListener;
import ua.cn.stu.cs.ems.core.models.ModelTimeListener;
import ua.cn.stu.cs.ems.core.queues.Queue;
import ua.cn.stu.cs.ems.core.transitions.Transition;
import ua.cn.stu.cs.ems.core.utils.JessUtils;

/**
 *
 * @author proger
 * @see Respond
 * @see RespondCollector
 */
public abstract class AbstractExperiment implements Experiment {

    final static Logger logger = LoggerFactory.getLogger(AbstractExperiment.class);
    // FactorImpl that is used to set new values
    private Factor factor;
    // What time should pass before the start of the primary statistics collection
    private double pauseBeforeStatisticCollection;
    // getTimePeriod of time at end of which statistics parameters are dumped
    private double dumpPeriod = -1;
    // Is primary statistics should be collected
    private boolean collectPrimaryStatistics;
    // Is secondary statistics should be collected
    private boolean collectSecondaryStatistics;
    //Init experiment for stepping
    private boolean initExperiment = false;
    private boolean modelFinished;
    private boolean experimentFinished = false;
    //Restorer for restor state of model
    private Restorer restor;
    // Model that is used to run an experiment
    private Model model;
    // Respond variable for this experiment
    private Respond respond;
    // Respond collector
    private RespondCollector respondCollector;
    private double lastTimePrimaryStatisticsWasCollected;
    // Statistics collector for each aggregate child
    private HashMap<AggregateChild, StatisticsCollector> statisticsCollectors =
            new HashMap<AggregateChild, StatisticsCollector>();
    // Result of primary statistics dump from model start untill current model state
    private HashMap<AggregateChild, List<PrimaryStatisticsElement>> statisticsResults =
            new HashMap<AggregateChild, List<PrimaryStatisticsElement>>();
    // Values of variable respond after each model run
    private List<SecondaryStatisticsElement> secondaryStatistics =
            new ArrayList<SecondaryStatisticsElement>();
    // Result of primary statistics collection for each aggregate child for all pased runs
    private HashMap<AggregateChild, List<PrimaryStatisticsResult>> primaryStatisticsResult =
            new HashMap<AggregateChild, List<PrimaryStatisticsResult>>();
    //Collector for secondary statistic
    private StatisticsCollector secondaryStatisticsCollector;
    //Values of factor that was used in experiment
    private List<Double> usedFactorValues = new ArrayList<Double>();
    private boolean stopExperiment;
    private boolean collectedStatOnEvent = false;
    private VariablesStateCollector variablesStateCollector;

    /**
     * Initialize experiment. Use this constructor if secondary statistics
     * should be collected. You should specify all parameters for secondary
     * statistics to be collected.
     *
     * @param theModel - model to run experiment
     * @param theFactor - factor of an experiment
     * @param theRespond - variable that will be used to get respond of the
     * factor changes
     */
    public AbstractExperiment(Model theModel, Factor theFactor, Respond theRespond) {
        model = theModel;
        factor = theFactor;
        respond = theRespond;
        collectSecondaryStatistics = true;
        respondCollector = new RespondCollector();
        variablesStateCollector = new VariablesStateCollector(model);
    }

    /**
     * Initialize experiment. Use this constructor if secondary statistics
     * shouldn't be collected.
     *
     * @param theModel - model to run experiment
     * @param theFactor - factor of an experiment
     */
    public AbstractExperiment(Model theModel, Factor theFactor) {
        model = theModel;
        factor = theFactor;
        collectSecondaryStatistics = false;
        variablesStateCollector = new VariablesStateCollector(theModel);
    }

    /**
     * Initialize experiment. Use this constructor if secondary statistics
     * shouldn't be collected and factor values shouldn't be used.
     *
     * @param theModel - model to run experiment
     * @param theFactor - factor of an experiment
     */
    public AbstractExperiment(Model theModel) {
        this(theModel, new NullFactor());
    }

    public final void startExperiment() {
        experimentFinished = false;
        stopExperiment = false;

        setDumpPeriodIfItNotSet();
        clearPrimaryStatistics();
        clearSecondaryStastics();
        usedFactorValues.clear();

        // Save model's state
        final Restorer restorer = new Restorer(model.getRootAggregate());
        //Collect statistics for each value of factor
        while (factor.hasMoreValues() && !stopExperiment) {
            factor.nextValue();
            saveFactorValue();
            // Set current values combination
            setFactorValue();
            resetExperiment();
            startRespondCollection();

            if (!((Double) factor.getCurrentValue()).isNaN()) {
                logger.debug("Factor {} with value {}:", factor.getFactorName(), factor.getCurrentValue());
            }

            int run = 1;

            do {
                clearStatistics();
                // Set timer that will measure pause before the start of primary statistics collection
                getModel().addTimer(skipStatisticsCollectionPeriodListener, pauseBeforeStatisticCollection);
                //If respond is not variable
                startSecondaryStatisticCollection();

                logger.debug("Run of model {}", run++);

                run();

                stopStatisticCollection();
                //If respond is not variable
                stopSecondaryStatisticsCollection();

                if (!collectedStatOnEvent) {
                    saveStatistics(getModel());
                }
                savePrimaryStatistics();
                saveSecondaryStatistics();

                // Restore model state
                restorer.restoreState();
                resetForRun();
                // Restorer restores token's in places and variables variables
                // So we need to set current factor values again
                setFactorValue();
            } while (!lastRun() && !stopExperiment);
            //Save data respond for this factor
            stopRespondCollection();
        }
        experimentFinished = true;
    }

    public final void stopExperiment() {
        getModel().interrupt();
        stopExperiment = true;
    }

    public final void stepExperiment() {
        if (!experimentFinished) {
            initExperiment();
            //Collect statistics for each value of factor and only for one run
            if (modelFinished) {
                stepWithNextFactor();
            }
            runWithStep();
        }
    }

    /**
     * Stop step experiment if user don't want continue it. Next step after
     * colling this method will start a new step experiment. Also if user
     * finished step experiment and want to began a new step expeiment, before
     * new run we need to call this method.
     */
    public final void stopStepExperiment() {
        if (!experimentFinished) {
            restor.restoreState();
        } else {
            experimentFinished = false;
        }
        getModel().setInitModel(false);
        initExperiment = false;
    }

    public final Model getModel() {
        return model;
    }

    public final Factor getFactor() {
        return factor;
    }

    public Set<AggregateChild> getStatisticsObjects() {
        return new HashSet<AggregateChild>(statisticsCollectors.keySet());
    }

    public List<SecondaryStatisticsElement> getSecondaryStatistics() {
        return new ArrayList<SecondaryStatisticsElement>(secondaryStatistics);
    }

    public void setPrimaryStatisticsDumpPeriod(double period) {
        dumpPeriod = period;
    }

    private void setDumpPeriodIfItNotSet() {
        if (dumpPeriod < 0) {
            dumpPeriod = model.getFinishTime();
        }
    }

    public double getPrimaryStatisticsDumpPeriod() {
        return dumpPeriod;
    }

    public void setDelayBeforeCollectingPrimaryStatistics(double delay) {
        pauseBeforeStatisticCollection = delay;
    }

    public double getDelayBeforeCollectingPrimaryStatistics() {
        return pauseBeforeStatisticCollection;
    }

    public void setIsPrimaryStatisticsShouldBeCollected(boolean toCollect) {
        collectPrimaryStatistics = toCollect;
    }

    public boolean isPrimaryStatisticsShouldBeCollected() {
        return collectPrimaryStatistics;
    }

    public void collectStatisticsOn(Place place) {
        PlaceStatisticsCollector psc = new PlaceStatisticsCollector(model, place);
        statisticsCollectors.put(place, psc);
        statisticsResults.put(place, new ArrayList<PrimaryStatisticsElement>());
        primaryStatisticsResult.put(place, new ArrayList<PrimaryStatisticsResult>());
        if (collectedStatOnEvent) {
            psc.setCollectedStatOnEvent(statisticsResults);
        }
    }

    public void collectStatisticsOn(Transition transition) {
        TransitionStatisticsCollector tsc = new TransitionStatisticsCollector(model, transition);
        statisticsCollectors.put(transition, tsc);
        statisticsResults.put(transition, new ArrayList<PrimaryStatisticsElement>());
        primaryStatisticsResult.put(transition, new ArrayList<PrimaryStatisticsResult>());
        if (collectedStatOnEvent) {
            tsc.setCollectedStatOnEvent(statisticsResults);
        }
    }

    public void collectStatisticsOn(Queue queue) {
        QueueStatisticsCollector qsc = new QueueStatisticsCollector(model, queue);
        statisticsCollectors.put(queue, qsc);
        statisticsResults.put(queue, new ArrayList<PrimaryStatisticsElement>());
        primaryStatisticsResult.put(queue, new ArrayList<PrimaryStatisticsResult>());
        if (collectedStatOnEvent) {
            qsc.setCollectedStatOnEvent(statisticsResults);
        }
    }

    public void collectSecondaryStatisticsOn(Place place) {
        if (collectSecondaryStatistics) {
            secondaryStatisticsCollector = new PlaceStatisticsCollector(model, place);
        }
    }

    public void collectSecondaryStatisticsOn(Transition transition) {
        if (collectSecondaryStatistics) {
            secondaryStatisticsCollector = new TransitionStatisticsCollector(model, transition);
        }
    }

    public void collectSecondaryStatisticsOn(Queue queue) {
        if (collectSecondaryStatistics) {
            secondaryStatisticsCollector = new QueueStatisticsCollector(model, queue);
        }
    }

    public List<PrimaryStatisticsResult> getPrimaryStatisticsFor(AggregateChild element) {
        return new ArrayList<PrimaryStatisticsResult>(primaryStatisticsResult.get(element));
    }

    public Respond getRespond() {
        return respond;
    }

    private void setFactorValue() {
        factor.setValue();
    }

    public final ExperimentReport getReport() {
        ExperimentReport er = new ExperimentReport();

        // Copying used factor values
        for (Double value : usedFactorValues) {
            er.addFactorValue(value);
        }

        // Copy primary statistics
        for (AggregateChild aggregateChild : primaryStatisticsResult.keySet()) {
            String childName = JessUtils.getAggregateChildFullName(aggregateChild);
            er.addPrimaryStatisticsObject(childName);

            List<PrimaryStatisticsResult> psrList = primaryStatisticsResult.get(aggregateChild);

            for (PrimaryStatisticsResult psr : psrList) {
                er.addPrimaryStatisticsResult(childName, psr);
            }
        }

        // Copy secondary statistics to a report
        for (SecondaryStatisticsElement sse : secondaryStatistics) {
            er.addSecondaryStatisticsElement(sse);
        }
        
        er.setVariablesState(variablesStateCollector);

        return er;
    }
    // This timer is notified periodicaly by a model to dump primary statistics
    private ModelTimeListener primaryStatisticsDumpListener = new ModelTimeListener() {

        @Override
        public void modelTimeChanged(Model model, double newModelTime) {
            if (!collectedStatOnEvent) {
                saveStatistics(model);
            }
            model.addTimer(primaryStatisticsDumpListener, dumpPeriod);
        }
    };
    // Timer that will be notified after interval during witch statistics
    // shouldn't be collected
    private ModelTimeListener skipStatisticsCollectionPeriodListener = new ModelTimeListener() {

        public void modelTimeChanged(Model model, double newModelTime) {
            if (collectPrimaryStatistics) {
                startStatisticCollection();
                getModel().addTimer(primaryStatisticsDumpListener, dumpPeriod);
            }
        }
    };
    private ModelListener listenerModel = new ModelListener() {

        public void modelStarted(Model model) {
        }

        //One run model finished
        public void modelFinished(Model model) {
            stopStatisticCollection();
            //If respond is not variable
            stopSecondaryStatisticsCollection();

            saveStatistics(getModel());
            savePrimaryStatistics();
            saveSecondaryStatistics();

            // Restore model state
            restor.restoreState();
            resetForRun();
            // Restorer restores token's in places and variables variables
            // So we need to set current factor values again
            setFactorValue();
            //Save data respond for this factor
            stopRespondCollection();
            modelFinished = true;

            if (!factor.hasMoreValues()) {
                experimentFinished = true;
            }
        }

        public void modelTimeChanged(Model model, double newModelTime) {
        }
    };

    /**
     * Dump primary statistics during last time period.
     *
     * @param model
     */
    private void saveStatistics(Model model) {
        // Statistics dump time
        double currentModelTime = model.getModelingTime();

        // Dump primary statistics if we collect it and if we haven't collected primary statistics in this time stamp
        // (this can happen) if model execution finished just after primary statistics was dumped
        if (collectPrimaryStatistics && !(Math.abs(currentModelTime - lastTimePrimaryStatisticsWasCollected) < JessUtils.EPSILON)) {
            // Dump statistics from each statistics collector
            for (AggregateChild child : statisticsResults.keySet()) {
                StatisticsCollector sc = statisticsCollectors.get(child);
                StatisticsResult sr = sc.getResult();
                List<PrimaryStatisticsElement> psList = statisticsResults.get(child);

                PrimaryStatisticsElement psr = new PrimaryStatisticsElement(currentModelTime, sr);
                psList.add(psr);

                // Reset statistics collector
                //sc.reset();
                
                variablesStateCollector.saveState(currentModelTime);
            }
        }

        lastTimePrimaryStatisticsWasCollected = getModel().getModelingTime();
    }

    /**
     * Pause all statistics collector. When this method is called all statistics
     * collectors temporary became inactive and don't collect statistics.
     */
    protected void stopStatisticCollection() {
        for (AggregateChild child : statisticsCollectors.keySet()) {
            StatisticsCollector sc = statisticsCollectors.get(child);
            sc.stopStatCollection();
        }
    }

    /**
     * Start to collect primary statistics.
     */
    protected void startStatisticCollection() {
        for (AggregateChild child : statisticsCollectors.keySet()) {
            StatisticsCollector sc = statisticsCollectors.get(child);
            sc.startStatCollection();
        }
    }

    /**
     * Run model
     */
    protected abstract void run();

    /**
     * Run stepping model
     */
    protected abstract void runWithStep();

    /**
     * Check if this model's run was the last one with current combination of
     * factors values.
     *
     * @return true if this was the last run of an experiment, false otherwise.
     */
    protected abstract boolean lastRun();

    protected abstract void specificReset();

    /**
     * Method that is called to reset experiment when new combination of factors
     * values was set.
     */
    private void resetExperiment() {
        specificReset();
        lastTimePrimaryStatisticsWasCollected = -1d;
    }

    /**
     * Reset for each run of model
     */
    private void resetForRun() {
        getModel().reset();
        lastTimePrimaryStatisticsWasCollected = -1d;
    }

    private void clearStatisticsCollectors() {
        for (AggregateChild child : statisticsCollectors.keySet()) {
            StatisticsCollector collector = statisticsCollectors.get(child);
            collector.reset();
        }
    }

    private void clearStatisticsResult() {
        for (AggregateChild child : statisticsResults.keySet()) {
            statisticsResults.put(child, new ArrayList<PrimaryStatisticsElement>());
        }
    }

    /**
     * Save primary statistics of the current model run.
     */
    private void savePrimaryStatistics() {
        if (collectPrimaryStatistics) {
            for (AggregateChild child : statisticsResults.keySet()) {
                List<PrimaryStatisticsResult> primaryStatisticsResultsList = primaryStatisticsResult.get(child);
                List<PrimaryStatisticsElement> collectedResults = statisticsResults.get(child);

                PrimaryStatisticsResult psr = new PrimaryStatisticsResult(factor.getCurrentValue(), collectedResults);
                //Added for table report
                if (child instanceof Place) {
                    psr.setObjectType(PrimaryStatisticsResult.OBJECT_TYPE_PLACE);
                } else if (child instanceof Transition) {
                    psr.setObjectType(PrimaryStatisticsResult.OBJECT_TYPE_TRANSITION);
                } else {
                    psr.setObjectType(PrimaryStatisticsResult.OBJECT_TYPE_QUEUE);
                }
                psr.setObjectName(child.getName());
                psr.setParentAggregate(child.getParentAggregate().getName());
                primaryStatisticsResultsList.add(psr);
            }
            // when saving - need saving for each run
            // for multifactor experiment
            variablesStateCollector.saveState(getModel().getModelingTime());
       }
    }

    /**
     * Save secondary statistics of the current model run.
     */
    private void saveSecondaryStatistics() {
        if (collectSecondaryStatistics && respondCollector.isStatisticsCollected()) {
            setRespond();
        }
    }

    /**
     * Clear collected primary statistics.
     */
    private void clearPrimaryStatistics() {
        for (AggregateChild child : primaryStatisticsResult.keySet()) {
            primaryStatisticsResult.put(child, new ArrayList<PrimaryStatisticsResult>());
        }
    }

    /**
     * Clear collected secondary statistics.
     */
    private void clearSecondaryStastics() {
        secondaryStatistics = new ArrayList<SecondaryStatisticsElement>();
    }

    private void clearStatistics() {
        clearStatisticsCollectors();
        clearStatisticsResult();
    }

    private void saveFactorValue() {
        Factor theFactor = getFactor();

        if (!(theFactor instanceof NullFactor)) {
            usedFactorValues.add(theFactor.getCurrentValue());
        }
    }

    /**
     * Set respond if it not variable. If respond is variable - response
     * variable is already setted in Respond
     */
    private void setRespond() {
        //If respond is not variable
        if (secondaryStatisticsCollector != null) {
            Iterator<StatisticsParameters> it = secondaryStatisticsCollector.getStatisticsParameters().iterator();
            while (it.hasNext()) {
                StatisticsParameters param = it.next();
                String paramName = param.getPresentation();
                if (respond.getName().equals(paramName)) {
                    respond.setValue(secondaryStatisticsCollector.getResult().getResultValue(param));
                    break;
                }
            }
        }
        respondCollector.updateStat((Double) respond.getValue());
    }

    /**
     * Start collect secondary statistics. If secondaryStatisticCollector==null
     * statistics collect was not setted on any object
     */
    private void startSecondaryStatisticCollection() {
        if (collectSecondaryStatistics && secondaryStatisticsCollector != null) {
            secondaryStatisticsCollector.startStatCollection();
        }
    }

    /**
     * Stop collect secondary statistics. If secondaryStatisticCollector==null
     * statistics collect was not setted on any object
     */
    private void stopSecondaryStatisticsCollection() {
        if (collectSecondaryStatistics && secondaryStatisticsCollector != null) {
            secondaryStatisticsCollector.stopStatCollection();
        }
    }

    /*
     * Start collect secondary statistics. If secondaryStatisticCollector==null
     * statistics collect was not setted on any object
     */
    private void startRespondCollection() {
        if (collectSecondaryStatistics) {
            if (!respond.getName().equals(Respond.TYPE_VARIABLE)) {
                if (secondaryStatisticsCollector != null) {
                    respondCollector.startStatCollection();
                }
            } else {
                respondCollector.startStatCollection();
            }
        }
    }

    /**
     * Add new element in secondary statistics
     */
    private void addElementOfSecondaryStatistics() {
        respondCollector.stopStatCollection();
        SecondaryStatisticsElement sse =
                new SecondaryStatisticsElement(factor.getCurrentValue(),
                respondCollector.getAverageRespond(),
                respondCollector.getVariance(),
                respondCollector.getCoefficientOfVariation(),
                respondCollector.getMinRespond(),
                respondCollector.getMaxRespond(),
                respondCollector.getSampleSize());
        secondaryStatistics.add(sse);
    }

    /**
     * Stop collect secondary statistics. If secondaryStatisticCollector==null
     * statistics collect was not setted on any object
     */
    private void stopRespondCollection() {
        if (collectSecondaryStatistics) {
            if (!respond.getName().equals(Respond.TYPE_VARIABLE)) {
                if (secondaryStatisticsCollector != null) {
                    addElementOfSecondaryStatistics();
                }
            } else {
                addElementOfSecondaryStatistics();
            }
        }
    }

    /**
     * Init experiment fore stepping
     */
    private void initExperiment() {
        if (!initExperiment) {
            setDumpPeriodIfItNotSet();
            clearPrimaryStatistics();
            clearSecondaryStastics();
            getModel().addModelListener(listenerModel);
            usedFactorValues.clear();
            // Save model's state
            restor = new Restorer(model.getRootAggregate());
            stepWithNextFactor();
            initExperiment = true;
            modelFinished = false;
        }
    }

    /**
     * Step with next factor's value
     */
    private void stepWithNextFactor() {
        if (factor.hasMoreValues()) {
            modelFinished = false;
            factor.nextValue();
            saveFactorValue();
            // Set current values combination
            setFactorValue();
            resetExperiment();
            startRespondCollection();

            clearStatistics();

            // Set timer that will measure pause before the start of primary statistics collection
            getModel().addTimer(skipStatisticsCollectionPeriodListener, pauseBeforeStatisticCollection);
            //If respond is not variable
            startSecondaryStatisticCollection();
        }
    }

    public boolean isExperimentFinished() {
        return experimentFinished;
    }

    public void setCollectedStatOnEvent(boolean stat) {
        collectedStatOnEvent = stat;
    }
}
