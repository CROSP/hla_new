package ua.cn.stu.cs.ems.core.aggregates;

import ua.cn.stu.cs.ems.core.queues.Queue;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import ua.cn.stu.cs.ems.core.models.Model;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.transitions.Transition;
import ua.cn.stu.cs.ems.ecli.Value;
import static ua.cn.stu.cs.ems.core.utils.AssertUtils.*;

/**
 * Class AbstractAggregate - base implementation of Aggregate interface.
 */
public abstract class AbstractAggregate extends AbstractAggregateChild implements Aggregate {

    public AbstractAggregate(String name) {
        super(name);
    }
    private Model model;
    private HashMap<String, AggregateChild> names = new HashMap<String, AggregateChild>();
    private List<Place> places = new ArrayList<Place>();
    private List<Transition> transitions = new ArrayList<Transition>();
    private List<Aggregate> childAggregates = new ArrayList<Aggregate>();
    private List<Output> outputs = new ArrayList<Output>();
    private List<Input> inputs = new ArrayList<Input>();
    private List<Queue> queues = new ArrayList<Queue>();
    HashMap<String, AggregateVariable> variables = new HashMap<String, AggregateVariable>();

    public void addInput(Input input, int pos) {
        assertBounds(pos, 0, inputs.size());
        assertNotNull(input, "Input can't be null");

        addAggregateChild(input);
        inputs.add(pos, input);
    }

    public boolean disconnectInput(Input input) {
        assertNotNull(input, "Input can't be null");

        if (inputs.remove(input)) {
            removeAggregateChild(input);
            return true;
        }

        return false;
    }

    public List<Input> getInputs() {
        List<Input> result = new ArrayList<Input>(inputs);
        return result;
    }

    public Input getInput(String name) {
        for (Input input : inputs) {
            if (input.getName().equals(name)) {
                return input;
            }
        }

        return null;

    }

    public Integer getInputNumber(String name) {
        int j = 0;
        for (Input i : inputs) {
            if (i.getName().equals(name)) {
                return j;
            }
            j++;
        }

        return null;
    }

    public void addOutput(Output output, int pos) {
        assertNotNull(output, "Output can't be null");

        addAggregateChild(output);
        outputs.add(pos, output);
    }

    public boolean disconnectOutput(Output output) {
        assertNotNull(output, "Output can't be null");

        if (outputs.remove(output)) {
            removeAggregateChild(output);
            return true;
        }
        return false;
    }

    public List<Output> getOutputs() {
        List<Output> result = new ArrayList<Output>(outputs);
        return result;
    }

    public Output getOutput(String name) {
        for (Output output : outputs) {
            if (output.getName().equals(name)) {
                return output;
            }
        }

        return null;
    }

    public Integer getOutputNumber(String name) {
        int j = 0;
        for (Output o : outputs) {
            if (o.getName().equals(name)) {
                return j;
            }
            j++;
        }

        return null;
    }

    public void addTransition(Transition transition) {
        assertNotNull(transition, "Transition can't be null");

        addAggregateChild(transition);
        transitions.add(transition);
    }

    public void addTransitions(Transition... transitions) {
        for (Transition t : transitions) {
            addTransition(t);
        }
    }

    public Set<Transition> getTransitions() {
        Set<Transition> result = new HashSet<Transition>(transitions);

        return result;
    }

    public Transition getTransition(String name) {
        for (Transition t : transitions) {
            if (t.getName().equals(name)) {
                return t;
            }
        }

        return null;
    }

    public boolean disconnectChildTransition(Transition transition) {
        assertNotNull(transition, "Transition can't be null");

        if (transitions.remove(transition)) {
            removeAggregateChild(transition);
            return true;
        }

        return false;
    }

    public void addPlace(Place place) {
        assertNotNull(place, "Place can't be null");

        addAggregateChild(place);
        places.add(place);
    }

    public void addPlaces(Place... places) {
        for (Place p : places) {
            addPlace(p);
        }
    }

    public Place getPlace(String name) {
        for (Place p : places) {
            if (p.getName().equals(name)) {
                return p;
            }
        }

        return null;
    }

    public Set<Place> getPlaces() {
        Set<Place> result = new HashSet<Place>(places);

        return result;
    }

    public void disconnectChildPlace(Place place) {
        assertNotNull(place, "Place can't be null");

        if (places.remove(place)) {
            removeAggregateChild(place);
        }
    }

    public void addQueue(Queue queue) {
        assertNotNull(queue);

        addAggregateChild(queue);
        queues.add(queue);

    }

    public void addQueues(Queue... queues) {
        assertNotNull(queues, "Queues can't be null");

        for (Queue q : queues) {
            addQueue(q);
        }
    }

    public Set<Queue> getQueues() {
        return new HashSet(queues);
    }

    public Queue getQueue(String name) {
        for (Queue q : queues) {
            if (q.getName().equals(name)) {
                return q;
            }
        }

        return null;
    }

    public boolean disconnectQueue(Queue queue) {
        assertNotNull(queue);

        if (queues.remove(queue)) {
            removeAggregateChild(queue);
            return true;
        }

        return false;
    }

    protected void addAggreagate(Aggregate aggregate) {
        assertNotNull(aggregate, "Aggregate can't be null");
        assertTrue(aggregate != this, "Aggregate can't be child for itself");

        addAggregateChild(aggregate);
        childAggregates.add(aggregate);
    }

    public Aggregate getAggregate(String name) {
        for (Aggregate aggregate : childAggregates) {
            if (aggregate.getName().equals(name)) {
                return aggregate;
            }
        }

        return null;
    }

    public boolean disconnectChildAggregate(Aggregate child) {
        assertNotNull(child, "Aggregate can't be null");

        if (childAggregates.remove(child)) {
            removeAggregateChild(child);
            return true;
        }

        return false;
    }

    public Set<Aggregate> getChildAggregates() {
        Set<Aggregate> result = new HashSet<Aggregate>(childAggregates);

        return result;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public Model getModel() {
        if (model != null) {
            return model;
        }

        if (parentAggregate != null) {
            return parentAggregate.getModel();
        }

        return null;
    }

    public void setVariable(String name, Value value) {
        assertNotNull(name, "Variable name can't be null");
        assertNotNull(value, "Variable value can't be null");

        if (!variables.containsKey(name)) {
            AggregateVariable newVar = new AggregateVariable(this, name, value);
            variables.put(name, newVar);
        } else {
            AggregateVariable var = variables.get(name);
            var.setValue(value);
        }
    }

    public AggregateVariable getVariable(String name) {
        return variables.get(name);
    }

    public Set<String> getVariablesNames() {
        Set<String> result = new HashSet<String>(variables.keySet());

        return result;
    }

    public boolean removeVariable(String name) {

        if (variables.containsKey(name)) {
            variables.remove(name);
            return true;
        }

        return false;
    }

    public void clearVariables() {
        variables.clear();
    }

    public AggregateChild getAggregateChild(String name) {
        return names.get(name);
    }

    public void childNameChanged(AggregateChild child, String oldName, String newName) {
        names.remove(oldName);
        names.put(newName, child);
    }

    private void addAggregateChild(AggregateChild ac) {
        if (names.get(ac.getName()) != null) {
            throw new NameClashException("Two aggregate childs shouldn't have same names");
        }

        names.put(ac.getName(), ac);
        ac.setParentAggregate(this);
    }

    private void removeAggregateChild(AggregateChild ac) {
        names.remove(ac.getName());
        ac.setParentAggregate(null);
    }

    @Override
    public String toString() {
        String result = "";
        result += this.getClass().getSimpleName() + " '" + getName() + "'";

        return result;
    }
}
