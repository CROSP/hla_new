package ua.cn.stu.cs.ems.core.experiment;

import ua.cn.stu.cs.ems.core.aggregates.AggregateVariable;

/**
 *
 * @author proger
 */
public interface Factor {

    /**
     * Get current factor value.
     * @return current factor value.
     */
    double getCurrentValue();

    /**
     * Get number of the current value.
     * @return number of the current value
     */
    int getValueNumber();

    /**
     * Get variable that is changed by this factor.
     * @return variable that is changed by this factor
     */
    AggregateVariable getVariable();

    /**
     * Check if this factor has more values.
     * @return true if it has more values to set, false otherwise.
     */
    boolean hasMoreValues();

    /**
     * Change current factor value.
     */
    void nextValue();

    /**
     * Get number of values in this factor.
     * @return number of values in this factor
     */
    int numberOfValues();

    /**
     * Set current factor value to an aggregate variable.
     */
    void setValue();
    
    /**
     * Get name of factor
     * @return name of factor
     */
    String getFactorName();
    
}