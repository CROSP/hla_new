package ua.cn.stu.cs.ems.core.experiment.statistics;

import java.util.ArrayList;
import java.util.List;

/**
 * Collector that collection data for respond
 *
 * @author Dmitry Krivenko <dmitrykrivenko@gmal.com>
 */
public class RespondCollector {

    // true if statistics is collected, false otherwise
    private boolean collectsStat;
    //Average of responds
    private double averageRespond;
    // Values of the respond
    private ArrayList<Double> respondValues = new ArrayList<Double>();
    //Variance of respond
    private double variance;
    //Coefficient of variation
    private double coefficientOfVariation;
    //Minimum of respond
    private double minRespond;
    //Maximum of respond
    private double maxRespond;
    //Sample size = count of runs model
    private double sampleSize;
    //For first initial minRespond and maxRespond
    private boolean firstUpdate;

    public RespondCollector() {
    }

    public RespondCollector(RespondCollector collector) {
        this.averageRespond = collector.getAverageRespond();
        this.coefficientOfVariation = collector.getCoefficientOfVariation();
        this.minRespond = collector.getMinRespond();
        this.maxRespond = collector.getMaxRespond();
        this.sampleSize = collector.getSampleSize();
        this.variance = collector.getVariance();
    }

    public final void startStatCollection() {
        if (!collectsStat) {
            collectsStat = true;
            reset();
        }
    }

    public final void stopStatCollection() {
        if (collectsStat) {
            setResult();
        }
        collectsStat = false;
    }

    private void reset() {
        variance = 0;
        coefficientOfVariation = 0;
        sampleSize = 0;
        respondValues.clear();
        firstUpdate = true;
    }

    public final void updateStat(Double respondValue) {
        if (firstUpdate) {
            minRespond = respondValue.doubleValue();
            maxRespond = respondValue.doubleValue();
            firstUpdate = false;
        }
        if (respondValue.doubleValue() < minRespond) {
            minRespond = respondValue.doubleValue();
        } else if (respondValue.doubleValue() > maxRespond) {
            maxRespond = respondValue.doubleValue();
        }
        respondValues.add(respondValue);
    }

    private void setResult() {
        sampleSize = (double) respondValues.size();
        double sum = sumOfResponds();
        double expectation = sum / (double) respondValues.size();
        variance = calcVariance(expectation);
        averageRespond = expectation;
        double standardDeviation = Math.sqrt(variance);
        coefficientOfVariation = standardDeviation / expectation;
    }

    /**
     * @return the variance
     */
    public final double getVariance() {
        return variance;
    }

    /**
     * @return the coefficientOfVariation
     */
    public final double getCoefficientOfVariation() {
        return coefficientOfVariation;
    }

    /**
     * @param coefficientOfVariation the coefficientOfVariation to set
     */
    public final void setCoefficientOfVariation(double coefficientOfVariation) {
        this.coefficientOfVariation = coefficientOfVariation;
    }

    /**
     * @return the minRespond
     */
    public final double getMinRespond() {
        return minRespond;
    }

    /**
     * @return the maxRespond
     */
    public final double getMaxRespond() {
        return maxRespond;
    }

    /**
     * @return the averageRespond
     */
    public final double getAverageRespond() {
        return averageRespond;
    }

    /**
     * @return the sampleSize
     */
    public final double getSampleSize() {
        return sampleSize;
    }

    /**
     * @return true if respond collector is empty, otherwise false
     */
    public boolean isEmpty() {
        return sampleSize == 0;
    }

    /**
     *
     * @return true if statistics collected, otherwise false
     */
    public boolean isStatisticsCollected() {
        return collectsStat;
    }

    /**
     * @return sum of responds for several run of model for one factor
     */
    private double sumOfResponds() {
        double result = 0;
        for (int i = 0; i < respondValues.size(); i++) {
            result += respondValues.get(i);
        }
        return result;
    }

    /**
     * @param expectation
     * @return variance
     */
    private double calcVariance(double expectation) {
        double result = 0;
        for (int i = 0; i < respondValues.size(); i++) {
            result += Math.pow(respondValues.get(i) - expectation, 2);
        }
        result = result / respondValues.size();
        return result;
    }

    /**
     * Get values of responds
     *
     * @return array of values of responds
     */
    public final List<Double> getValuesOfRespond() {
        return new ArrayList<Double>(respondValues);
    }
}
