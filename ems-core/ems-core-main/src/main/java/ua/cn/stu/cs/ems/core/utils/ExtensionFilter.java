
package ua.cn.stu.cs.ems.core.utils;

import java.io.File;
import java.io.FileFilter;

/**
 *
 * @author proger
 */
public class ExtensionFilter implements FileFilter {

    String filePathEnd;

    public ExtensionFilter(String extension) {
        filePathEnd = "." + extension;
    }


    public boolean accept(File file) {
        return file.getPath().endsWith(filePathEnd);
    }

}
