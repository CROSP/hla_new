package ua.cn.stu.cs.ems.core.aggregates;

import java.util.List;
import java.util.ArrayList;
import static ua.cn.stu.cs.ems.core.utils.AssertUtils.*;

/**
 * ValidationResult - is a container for validation errors that were find during
 * aggregate validation.
 * @see VerifyError
 * @see AggregateValidator
 * @author proger
 */
public class ValidationResult {
    private List<ValidationError> errors = new ArrayList<ValidationError>();

    /**
     * Add new validation error.
     * @param error - new error to add. Can't be null.
     */
    public void addValidationError(ValidationError error) {
        assertNotNull(error, "Validation error can't be null");

        errors.add(error);
    }

    /**
     * Get list of validation errors
     * @return list of validation errors
     */
    public List<ValidationError> getErrors() {
        return errors;
    }

    /**
     * Check if aggregate passed validation.
     * @return true is aggregate passed validation, false otherwise.
     */
    public boolean passedValidation() {
        return errors.isEmpty();
    }
}
