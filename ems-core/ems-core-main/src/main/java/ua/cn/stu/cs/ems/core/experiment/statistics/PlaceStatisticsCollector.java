package ua.cn.stu.cs.ems.core.experiment.statistics;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.PlaceStateChangedListener;
import ua.cn.stu.cs.ems.core.experiment.PrimaryStatisticsElement;
import ua.cn.stu.cs.ems.core.models.Model;
import static ua.cn.stu.cs.ems.core.utils.AssertUtils.assertNotNull;
import ua.cn.stu.cs.ems.core.utils.JessUtils;

/**
 * Statistics collector for place. It collectsStat following statistics
 * parameters: <li> Occupied coefficient = occupied time / total monitored time
 * <li> Number of passed tokens = number of tokens that left a place <li>
 * Average occupied time = occupied time / number of passed tokens
 *
 * @author proger
 */
public class PlaceStatisticsCollector extends StatisticsCollector implements PlaceStateChangedListener {

    private Place place;
    // Time when primary statistics collection started
    private double collectionStartTime;
    // Sum of durations of all time segments during which there were a token in the place
    private double totalOccupiedTime;
    // Total number of tokens that left the place
    private int totalNumberOfPassedTokens;
    // true, if there is a token in the place, false otherwise
    private boolean placeOccupied;
    // Time when a token was put in the place
    private double occupationStartTime;
    final static Logger logger = LoggerFactory.getLogger(PlaceStatisticsCollector.class);

    public PlaceStatisticsCollector(Model model, Place place) {
        super(model);
        assertNotNull(place, "Can't collect statistics on null place");

        this.place = place;
        place.addStateChangeListener(this);
    }

    @Override
    public final void reset() {
        collectionStartTime = model.getModelingTime();
        occupationStartTime = model.getModelingTime();
        totalNumberOfPassedTokens = 0;
        totalOccupiedTime = 0;
        // collectionFinishTime = model.getModelingTime();
        collectionFinishTime = model.getFinishTime();
    }

    @Override
    public final void placeStateChanged(Place place) {
        if (collectsStat) {
            //removed token from the place
            if (place.getToken() == null && placeOccupied) {
                totalOccupiedTime += model.getModelingTime() - occupationStartTime;
                placeOccupied = false;
                totalNumberOfPassedTokens++;
                if (statisticsResults != null) {
                    StatisticsResult sr = getResult();
                    PrimaryStatisticsElement psr = new PrimaryStatisticsElement(model.getModelingTime(), sr);
                    statisticsResults.get(this.place).add(psr);
                }
                logger.info("Current statistics for place {}: {}", JessUtils.getAggregateChildFullName(place), getResult());
                //puted token to the place 
            } else if (place.getToken() != null && !placeOccupied) {
                placeOccupied = true;
                occupationStartTime = model.getModelingTime();
            }

        }
    }

    @Override
    public final List<StatisticsParameters> getStatisticsParameters() {
        return new ArrayList<StatisticsParameters>() {

            {
                add(StatisticsParameters.AVERAGE_OCCUPIED_TIME);
                add(StatisticsParameters.NUMBER_OF_PASSED_TOKENS);
                add(StatisticsParameters.OCCUPIED_COEFFICIENT);
            }
        };
    }

    @Override
    public final void startStatCollection() {
        if (!collectsStat) {
            reset();
            collectsStat = true;

            if (place.getToken() != null) {
                placeOccupied = true;
            } else {
                placeOccupied = false;
            }
        }
    }

    @Override
    public final void stopStatCollection() {
        if (collectsStat) {
            updateStat();
            // collectionFinishTime = model.getModelingTime();
        }

        collectsStat = false;
    }

    @Override
    public final StatisticsResult getResult() {
        if (collectsStat) {
            updateStat();
        }

        StatisticsResult sr = new StatisticsResult();
        //long totalMonitoredTime = getFinishTime() - collectionStartTime;
        double totalMonitoredTime = model.getFinishTime() - collectionStartTime;

        double occupiedCoefficient = ((double) totalOccupiedTime) / totalMonitoredTime;
        sr.setResultValue(StatisticsParameters.OCCUPIED_COEFFICIENT, occupiedCoefficient);
        sr.setResultValue(StatisticsParameters.NUMBER_OF_PASSED_TOKENS, Double.valueOf(totalNumberOfPassedTokens));

        double avarageOccupiedTime = ((double) totalOccupiedTime) / totalNumberOfPassedTokens;

        sr.setResultValue(StatisticsParameters.AVERAGE_OCCUPIED_TIME, avarageOccupiedTime);

        return sr;
    }

    private void updateStat() {
        if (place.getToken() != null) {
            totalOccupiedTime += model.getModelingTime() - occupationStartTime;
            occupationStartTime = model.getModelingTime();
        }
    }
}
