package ua.cn.stu.cs.ems.core.models;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Iterator;
import java.util.PriorityQueue;
import ua.cn.stu.cs.ems.core.ActiveInstance;
import ua.cn.stu.cs.ems.core.aggregates.Input;
import ua.cn.stu.cs.ems.core.aggregates.Output;
import ua.cn.stu.cs.ems.core.queues.Queue;
import ua.cn.stu.cs.ems.core.transitions.Transition;

/**
 *
 * @author proger
 */
public class DelayedQueue {

    final static int INITIAL_CAPACITY = 5;
    private PriorityQueue<TimeInstancePair> priorityQueue =
            new PriorityQueue<TimeInstancePair>(INITIAL_CAPACITY,
            new TimeDelayablePairComparator());
    static int TRANSITION_QUEUE_PRIORITY = 0;
    static int INPUT_OUTPUT_PRIORITY = 1;

    public void addDelayable(ActiveInstance delayable, double time) {
        TimeInstancePair ttp = new TimeInstancePair(time, delayable);
        priorityQueue.add(ttp);
    }

    public double getFirstDelayableTime() {
        TimeInstancePair first = priorityQueue.peek();

        return first.time;
    }

    public int getFirstDelayablePriority() {
        TimeInstancePair first = priorityQueue.peek();

        return first.priority;
    }

    public ActiveInstance getFirstDelayedInstance() {
        if (isEmpty()) {
            return null;
        }

        TimeInstancePair first = priorityQueue.poll();
        return first.instance;
    }

    public int getSize() {
        return priorityQueue.size();
    }

    public boolean isEmpty() {
        return getSize() == 0;
    }

    void clear() {
        priorityQueue.clear();
    }

    @Override
    public final String toString() {
        String result = "";
        for (Iterator<TimeInstancePair> tip = priorityQueue.iterator(); tip.hasNext();) {
            TimeInstancePair timeInstancePair = tip.next();
            result += timeInstancePair.time + " " + timeInstancePair.instance + "; ";
        }
        return result;
    }
}

class TimeInstancePair {

    int priority;
    double time;
    ActiveInstance instance;

    public TimeInstancePair(double time, ActiveInstance instance) {
        this.time = time;
        this.instance = instance;
        this.priority = getPriority(instance);
    }

    private int getPriority(Object obj) {
        if (obj instanceof Transition || obj instanceof Queue) {
            return DelayedQueue.TRANSITION_QUEUE_PRIORITY;
        } else if (obj instanceof Output || obj instanceof Input) {
            return DelayedQueue.INPUT_OUTPUT_PRIORITY;
        } else {
            throw new IllegalArgumentException("Only transition, output or input can be put into delay");
        }

    }
}

class TimeDelayablePairComparator implements Comparator<TimeInstancePair>, Serializable {

    public int compare(TimeInstancePair t, TimeInstancePair t1) {
        double sub = t.time - t1.time;

        if (sub > 0) {
            return 1;
        } else if (sub < 0) {
            return -1;
        } else {
            return t.priority - t1.priority;
        }
    }
}
