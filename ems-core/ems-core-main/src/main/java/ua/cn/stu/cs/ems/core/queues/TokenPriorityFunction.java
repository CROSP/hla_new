package ua.cn.stu.cs.ems.core.queues;

import ua.cn.stu.cs.ems.core.Token;

/**
 * TokenPriorityFunction - defines priority for a specified token.
 * @see AbstractPriorityQueue
 * @author proger
 */
public interface TokenPriorityFunction {

    /**
     * Calculate priority of the token
     * @param token - to calculates token's priority
     * @param queue - queue that uses this function to calculate token's priority.
     * @return token's priority
     */
    int calculateTokenPriority(Queue queue, Token token);
}
