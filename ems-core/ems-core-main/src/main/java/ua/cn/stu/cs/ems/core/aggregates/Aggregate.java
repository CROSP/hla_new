package ua.cn.stu.cs.ems.core.aggregates;

import java.util.List;
import java.util.Set;
import ua.cn.stu.cs.ems.core.models.Model;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.queues.Queue;
import ua.cn.stu.cs.ems.core.transitions.Transition;
import ua.cn.stu.cs.ems.ecli.Value;

/**
 * Interface Aggregate - represent aggregates in E networks. Aggregate is a container
 * for inputs, outputs, queues, transitions, places and other aggregates.
 * Every child in an aggregate has it's name. Name clashes are forbiden.
 */
public interface Aggregate extends AggregateChild {

    /**
     * Add new child input to this aggregate. Aggregate has several inputs
     * and user should specify where new input should be placed.
     * @param input - input to add. Can't be null
     * @param pos - where should output be placed
     */
    public void addInput(Input input, int pos);

    /**
     * Disconnect input from this aggregate.
     * @param input - input to disconnect
     * @return true is input disconnected, false otherwise
     */
    public boolean disconnectInput(Input input);

    /**
     * Get list of inputs that connected to a current aggregate. Inputs order in
     * result list represents order of inputs connected to an aggregate.
     * @return list of inputs connected to the current aggregate
     */
    public List<Input> getInputs();    

    /**
     * Get input by input's name.
     * @param name - input's name.
     * @return input object if input with specified name exists, null otherwise.
     */
    public Input getInput(String name);

    /**
     * Get number of an input by its name.
     * @param name - name of an input
     * @return number of input with specified name if one exists, null otherwise.
     */
    public Integer getInputNumber(String name);

    /**
     * Add new child output to this aggregate. Aggregate has several outputs
     * and user should specify where new output should be placed.
     * @param output - output to add. Can't be null.
     * @param pos - where should  output be placed
     */
    public void addOutput(Output output, int pos);

    /**
     * Disconnect output from the current aggregate.
     * @param output - output to disconnect
     * @return true if input disconnected, false otherwise
     */
    public boolean disconnectOutput(Output output);

    /**
     * Get list of outputs that connected to a current aggregate. Inputs order in
     * result list represents order of inputs connected to an aggregate.
     * @return list of outputs connected to the current aggregate
     */
    public List<Output> getOutputs();

    /**
     * Get output by output's name.
     * @param name - output's name.
     * @return output object if output with specified name exists, null otherwise.
     */
    public Output getOutput(String name);

    /**
     * Get number of an output by its name.
     * @param name - name of an output
     * @return number of output with specified name if one exists, null otherwise.
     */
    public Integer getOutputNumber(String name);

    /**
     * Add new transition to this aggregate
     * @param transition - transition to add. Can't be null.
     */
    public void addTransition(Transition transition);

    public void addTransitions(Transition... transitions);

    /**
     * Disconnect transition from the current aggregate
     * @param transition - transition to disconnect
     * @return true if transition disconnected, false otherwise
     */
    public boolean disconnectChildTransition(Transition transition);

    /**
     * Return set of transitions in this aggregate
     * @return set of transitions connected to this aggregate. Empty set if there is no
     * transitions in this aggregate
     */
    public Set<Transition> getTransitions();

    /**
     * Get transition by transition's name.
     * @param name - transition's name.
     * @return transition object if transition with specified name exists, null otherwise.
     */
    public Transition getTransition(String name);

    /**
     * Add place to this aggregate
     * @param place - place to add. Can't be null.
     */
    public void addPlace(Place place);

    public void addPlaces(Place... places);

    /**
     * Return place by it's name.
     * @param name - name of the place.
     * @return place object if place with specified name exists, null otherwise.
     */
    public Place getPlace(String name);

    /**
     * Disconnect place from the current aggregate
     * @param place - place to disconnect
     * @return true if place disconnected, false otherwise
     */
    public void disconnectChildPlace(Place place);   

    /**
     * Return set of places in this aggregate
     * @return set of places connected to this aggregate. Empty set if there is no
     * places in this aggregate
     */
    public Set<Place> getPlaces();

    /**
     * Add queue to this aggregate
     * @param queue - queue to add. Can't be null.
     */
    public void addQueue(Queue queue);

    public void addQueues(Queue... queues);

    public Set<Queue> getQueues();

    /**
     * Return queue by it's name.
     * @param name - name of the queue.
     * @return queue object if queue with specified name exists, null otherwise.
     */
    public Queue getQueue(String name);

    /**
     * Disconnect queue from the current aggregate
     * @param queue - queue to disconnect
     * @return true if queue disconnected, false otherwise
     */
    public boolean disconnectQueue(Queue queue);

//    /**
//     * Add child aggregate to this aggregate
//     * @param aggregate - aggregate to add. Can't be null.
//     */
//    public void addChildAggreagate(Aggregate aggregate);

    /**
     * Return child aggregate by it's name.
     * @param name - name of the aggregate.
     * @return aggregate object if place with specified name exists, null otherwise.
     */
    public Aggregate getAggregate(String name);

    /**
     * Return set of child aggregates of this aggregate
     * @return set of aggregates connected to this aggregate. Empty set if this
     * aggregate doesn't has child aggregates.
     */
    public Set<Aggregate> getChildAggregates();

    /**
     * Disconnect child aggregate from the current aggregate
     * @param aggregate - aggregate to disconnect
     * @return true if child aggregate disconnected, false otherwise
     */
    public boolean disconnectChildAggregate(Aggregate child);

    /**
     * Get model of this aggregate. Model can't set to this aggregate (than this
     * aggregate is root) or can be taken from this aggregate's parent.
     * @return if this aggregate is root, return model that was set, otherwise returns model
     * of it's parent.
     */
    public Model getModel();

    /**
     * Set method for this aggregate.
     * @param model
     */
    public void setModel(Model model);

    /**
     * Set new value to a variable. If variable with specified name doesn't exist
     * new variable will be created.
     * @param name - variable's name
     * @param value - new value for a variable
     */
    public void setVariable(String name, Value value);

    /**
     * Get variable's value
     * @param name - name of a variable
     * @return aggregate's variable if one with specified name exists, null otherwise.
     * 
     */
    public AggregateVariable getVariable(String name);

    public Set<String> getVariablesNames();

    /**
     * Remove variable from aggregate by aggregate's name
     * @param name - name of the variable to remove from aggregate.
     * @return true if variable removed, false otherwise
     */
    public boolean removeVariable(String name);

    /**
     * Remove all aggregate's variables.
     */
    public void clearVariables();

    /**
     * All children inherit AggregateChild class, so this is a basic method to
     * get child by it's name. Can't be used to see if name clash is posible.
     * @param name - name of the aggregate child to get.
     * @return AggregateChild object if one with specified name exists. null otherwise.
     */
    public AggregateChild getAggregateChild(String name);

    /**
     * Method to inform parent aggregate that child's name changed.
     * @param child - child that changed it's name
     * @param oldName - old name of aggregate's child
     * @param newName - new name of aggregate's child
     */
    public void childNameChanged(AggregateChild child, String oldName, String newName);
    
}
