package ua.cn.stu.cs.ems.core.queues;

import java.util.ArrayList;
import java.util.List;
import ua.cn.stu.cs.ems.core.Token;

/**
 * Parent class for all not priority queues.
 * @author proger
 */
public abstract class NonPriorityQueue extends AbstractQueue {

    protected ArrayList<Token> queue = new ArrayList<Token>();

    public NonPriorityQueue(String name) {
        super(name);
    }    

    @Override
    protected boolean isQueueEmpty() {
        return queue.isEmpty();
    }

    @Override
    protected Token removeTokenFromQueue() {
        if (queue.isEmpty())
            return null;

        int tokenNumberToPeek = tookenNumberToPeek();
        Token token = queue.get(tokenNumberToPeek);
        queue.remove(tokenNumberToPeek);

        return token;
    }

    @Override
    protected void addToken(Token token) {
        queue.add(queue.size(), token);
    }

    public List<Token> getTokensInQueue() {
        return new ArrayList<Token>(queue);
    }

    public void clear() {
        queue.clear();
    }

    public int length() {
        return queue.size();
    }

    protected abstract int tookenNumberToPeek();

}
