package ua.cn.stu.cs.ems.core.aggregates;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.cn.stu.cs.ems.core.JessException;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.queues.*;
import ua.cn.stu.cs.ems.core.transitions.SpecialTransition;
import ua.cn.stu.cs.ems.core.transitions.Transition;
import ua.cn.stu.cs.ems.core.utils.JessUtils;

/**
 * Class AggregateDefinition - definition of aggregates. This is used to store
 * information about aggregates structure, because several aggregates can have
 * same structure (graph of places, transitions, queues connected together) and
 * differ only in initial marking and functions.
 */
public class AggregateDefinition extends AbstractAggregate {

    final static Logger logger = LoggerFactory.getLogger(AggregateDefinition.class);
    List<ArcCoordinates> arcCoordinateses = new ArrayList<ArcCoordinates>();

    public AggregateDefinition(String typeName) {
        super(typeName);
    }

    public List<ArcCoordinates> getArcCoordinates() {
        return arcCoordinateses;
    }

    public void addArcCoordinates(ArcCoordinates ac) {
        arcCoordinateses.add(ac);
    }

    public void removeArcCoordinates(String sourceParent, String sourceName, String targetParent, String targetName) {
        ArcCoordinates coordinates = getArcCoordinates(sourceParent, sourceName, targetParent, targetName);
        if (null != coordinates) {
            this.arcCoordinateses.remove(coordinates);
        }
    }

    public ArcCoordinates getArcCoordinates(String sourceParent, String sourceName, String targetParent, String targetName) {
        for (ArcCoordinates ac : arcCoordinateses) {
            if (((ac.getSourceParent() == null && sourceParent == null) || (ac.getSourceParent() != null && ac.getSourceParent().equals(sourceParent)))
                    && ((ac.getTargetParent() == null && targetParent == null) || (ac.getTargetParent() != null && ac.getTargetParent().equals(targetParent)))
                    && ((ac.getSourceName() == null && sourceName == null) || (ac.getSourceName() != null && ac.getSourceName().equals(sourceName)))
                    && ((ac.getTargetName() == null && targetName == null) || ac.getTargetName() != null && ac.getTargetName().equals(targetName))) {
                return ac;
            }
        }

        return null;
    }

    public void clearArcCoordinates() {
        arcCoordinateses.clear();
    }

    /**
     * Aggregate definition is used only for storing structure. If we need an
     * aggregate that can be used in modeling we should create it with this
     * method.
     *
     * @param instanceName - name of the new aggregate
     * @return new aggregate.
     */
    public AggregateInstance createInstance(String instanceName) {
        AggregateInstance ai = new AggregateInstance(instanceName, this);
        copyAggregates(ai);

        copyVariables(ai);
        copyTransitions(ai);
        copyQueues(ai);
        copyInputs(ai);
        copyOutputs(ai);
        copyPlaces(ai);

        copyIOConnections(ai);
        return ai;
    }

    /**
     * Add aggregate definition as a child of this aggregate definition. This
     * method return aggregate definition reference because one aggregate
     * definition can be used in several times in a single aggregate definition
     * or in several aggregate definitions. Places of different aggregate
     * definition should reference to different inputs/outputs. To do this each
     * aggregate definition reference has it's own copy of inputs/outputs.
     *
     * @param child - child aggregate definition.
     * @param childName - name of the aggregate of child's type in a parrent
     * aggregate.
     * @return aggregate definition reference to a child aggregate definition
     */
    public AggregateDefinitionReference addChildAggreagateDefinition(AggregateDefinition child, String childName) {

        AggregateDefinitionReference reference = new AggregateDefinitionReference(childName, child);
        addAggreagate(reference);

        return reference;
    }
    
    private void copyVariables(AggregateInstance ai) {
        for (String varName : getVariablesNames()) {
            ai.setVariable(varName, getVariable(varName).getValue());
        }
    }

    private void copyAggregates(AggregateInstance ai) {
        for (Aggregate child : getChildAggregates()) {
            AggregateDefinitionReference adr = (AggregateDefinitionReference) child;
            AggregateInstance childInstance = adr.getAggregateDefinition().createInstance(adr.getName());
            for (int i = 0; i < childInstance.getInputs().size(); i++) {
                Input adrInput = adr.getInputs().get(i);
                Place adrInputPlace = adrInput.getInputPlace();
                if (adrInputPlace != null) {
                    Place newInputPlace = getNewPlace(ai, adrInputPlace);
                    Input aiChildInput = childInstance.getInputs().get(i);
                    aiChildInput.setInputPlace(newInputPlace);
                }
            }
            for (int i = 0; i < childInstance.getOutputs().size(); i++) {
                Output adrOutput = adr.getOutputs().get(i);
                Place adrOutputPlace = adrOutput.getOutputPlace();
                if (adrOutputPlace != null) {
                    Place newOutputPlace = getNewPlace(ai, adrOutputPlace);
                    Output aiChildOutput = childInstance.getOutputs().get(i);
                    aiChildOutput.connectOutputPlace(newOutputPlace);
                }
            }
            ai.addChildAggregate(childInstance);
        }
    }

    private void copyOutputs(AggregateInstance ai) {
        for (int i = 0; i < getOutputs().size(); i++) {
            Output output = getOutputs().get(i);
            Output newOutput = createOutput(output);
            if (output.getInputPlace() != null) {
                Place newInputPlace = getNewPlace(ai, output.getInputPlace());
                newOutput.connectInputPlace(newInputPlace);
            }
            ai.addOutput(newOutput, i);
        }
    }

    private void copyInputs(AggregateInstance ai) {
        for (int i = 0; i < getInputs().size(); i++) {
            Input input = getInputs().get(i);
            Input newInput = createInput(input);
            if (input.getOutputPlace() != null) {
                Place newOutputPlace = getNewPlace(ai, input.getOutputPlace());
                newInput.setOutputPlace(newOutputPlace);
            }
            ai.addInput(newInput, i);
        }
    }

    private void copyQueues(AggregateInstance ai) {
        for (Queue q : getQueues()) {
            Queue newQueue = createQueue(q);
            Place newInputPlace = getNewPlace(ai, q.getInputPlace());
            Place newOutputPlace = getNewPlace(ai, q.getOutputPlace());
            newQueue.connectInputPlace(newInputPlace);
            newQueue.connectOutputPlace(newOutputPlace);
            ai.addQueue(newQueue);
        }
    }

    private void copyTransitions(AggregateInstance ai) {
        for (Transition t : getTransitions()) {
            Transition newTransition = createNewTransition(t);
            for (int i = 0; i < t.getInputPlaces().size(); i++) {
                Place inputPlace = t.getInputPlaces().get(i);
                Place newInput = getNewPlace(ai, inputPlace);
                newTransition.addInputPlace(newInput, i);
            }

            for (int i = 0; i < t.getOutputPlaces().size(); i++) {
                Place outputPlace = t.getOutputPlaces().get(i);
                Place newOutput = getNewPlace(ai, outputPlace);
                newTransition.addOutputPlace(newOutput, i);
            }

            newTransition.setDelayFunction(t.getDelayFunction().copy());
            newTransition.setTransformationFunction(t.getTransformationFunction().copy());

            if (newTransition instanceof SpecialTransition) {
                SpecialTransition specialT = (SpecialTransition) t;
                SpecialTransition newSpecialTransition = (SpecialTransition) newTransition;

                newSpecialTransition.setPermitingFunction(specialT.getPermitingFunction().copy());
            }

            ai.addTransition(newTransition);
        }
    }

    private Place getNewPlace(AggregateInstance ai, Place place) {
        if (place != null) {
            Place newInput = ai.getPlace(place.getName());
            if (newInput == null) {
//                logger.debug("copies place: " + place);
                newInput = new Place(place.getName());
                if (place.getToken() != null) {
                    newInput.setToken(new Token(place.getToken()));
                }
                ai.addPlace(newInput);
            }
            return newInput;
        }

        return null;
    }

    private Transition createNewTransition(Transition t) {
        try {
            String tname = t.getName();
            Constructor<Transition> ctor = (Constructor<Transition>) t.getClass().getConstructor(String.class);
            return ctor.newInstance(tname);
        } catch (Exception ex) {
            throw new JessException("Error during creating new Transition", ex);
        }
    }

    private Queue createQueue(Queue q) {

        if (q instanceof FIFOQueue) {
            return new FIFOQueue(q.getName());
        } else if (q instanceof LIFOQueue) {
            return new LIFOQueue(q.getName());
        } else if (q instanceof FIFOPriorityQueue) {
            FIFOPriorityQueue fpq = (FIFOPriorityQueue) q;
            return new FIFOPriorityQueue(fpq.getName(), fpq.getPriorityFunction());
        } else if (q instanceof LIFOPriorityQueue) {
            LIFOPriorityQueue lpq = (LIFOPriorityQueue) q;
            return new LIFOPriorityQueue(lpq.getName(), lpq.getPriorityFunction());
        } else {
            throw new JessException("Unknown queue type");
        }
    }

    // TODO there can be other input implementation, for distributed
    // calculations
    private Input createInput(Input input) {
        return new InputImpl(input.getName());
    }

    // TODO there can be other output implementation, for distributed
    // calculations
    private Output createOutput(Output output) {
        return new OutputImpl(output.getName());
    }

    private void copyPlaces(AggregateInstance ai) {
        for (Place p : getPlaces()) {
            getNewPlace(ai, p);
            
            if (p.getOutputActiveInstance() != null && p.getOutputActiveInstance() instanceof Input) {
                String inputPath = JessUtils.getAggregateChildName(this, p.getOutputActiveInstance());
                Input aiInput = (Input) JessUtils.getAggregateChild(ai, inputPath);
                Place aiPlace = getNewPlace(ai, p);
                if (aiPlace.getOutputActiveInstance() == null) {
                    aiInput.setInputPlace(aiPlace);
                }
            }
            if (p.getInputActiveInstance() != null && p.getInputActiveInstance() instanceof Output) {
                String outputPath = JessUtils.getAggregateChildName(this, p.getInputActiveInstance());
                Output aiOutput = (Output) JessUtils.getAggregateChild(ai, outputPath);
                Place aiPlace = getNewPlace(ai, p);
                if (aiPlace.getInputActiveInstance() == null) {
                    aiOutput.connectOutputPlace(p);
                }
            }
        }
    }

    private void copyIOConnections(AggregateInstance ai) {
        for (ArcCoordinates ac: getArcCoordinates()) {
            String sp = ac.getSourceParent();
            String tp = ac.getTargetParent();
            String s = ac.getSourceName();
            String t = ac.getTargetName();
            
            if (sp != null && tp != null) {
                Aggregate aiSource = (Aggregate) ai.getAggregate(sp);
                Aggregate aiTarget = (Aggregate) ai.getAggregate(tp);
                Output aiOutput = (Output) JessUtils.getAggregateChild(aiSource, s);
                Input aiInput = (Input) JessUtils.getAggregateChild(aiTarget, t);
                aiOutput.addInput(aiInput, 0);
            }
        }
    }
}
