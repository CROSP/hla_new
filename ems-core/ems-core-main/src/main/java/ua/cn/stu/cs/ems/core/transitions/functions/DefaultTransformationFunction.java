package ua.cn.stu.cs.ems.core.transitions.functions;

import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.transitions.Transition;

/**
 * Default transformation function doesn't perform any changes of a token it should transform. It returns the same 
 * token it receives.
 * @author proger
 */
public class DefaultTransformationFunction implements TransformationFunction {

    public DefaultTransformationFunction() {
    }

    public Token transform(Transition transition, Token token) {
        return token;
    }

    @Override
    public final boolean equals(Object o) {
        return (o instanceof DefaultTransformationFunction);
    }

    public final TransformationFunction copy() {
        return new DefaultTransformationFunction();
    }

    @Override
    public final int hashCode() {
        int hash = 5;
        return hash;
    }
}
