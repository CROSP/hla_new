package ua.cn.stu.cs.ems.core;


/**
 * Interface to observe changes in place's status. It's has single method
 * that is called when a token is set to a place or when token is cleared from a
 * place.
 * @author proger
 */
public interface PlaceStateChangedListener {

    /**
     * Method that called when place state was changed.
     * @param place - place wich state changed
     */
    void  placeStateChanged(Place place);
}
