package ua.cn.stu.cs.ems.core.experiment;

/**
 * Class that stores value of the respond value after a singe model run.
 * @see Experiment
 * @author proger
 */
public class SecondaryStatisticsElement {

    // Factor value during model run
    private double factorValue;
    // Value of the respond after model finished
    private double respond;
    //Variance of respond
    private double variance;
    //Coefficient of variation
    private double coefficientOfVariation;
    //Minimum of respond
    private double minRespond;
    //Maximum of respond
    private double maxRespond;
    //Sample size = count of runs model
    private double sampleSize;

    public SecondaryStatisticsElement(double theFactorValue, double theRespond,
            double theVariance, double theCoefficientOfVariation, double min,
            double max, double theSampleSize) {
        factorValue = theFactorValue;
        respond = theRespond;
        variance = theVariance;
        coefficientOfVariation = theCoefficientOfVariation;
        minRespond = min;
        maxRespond = max;
        sampleSize = theSampleSize;
    }

    /**
     * Get value of a factor during model run.
     * @return value of a factor during model run
     */
    public final double getFactorValue() {
        return factorValue;
    }

    /**
     * Get respond value.
     * @return respond value
     */
    public final double getRespond() {
        return respond;
    }

    @Override
    public final boolean equals(Object other) {
        if (other instanceof SecondaryStatisticsElement) {
            SecondaryStatisticsElement otherSSE = (SecondaryStatisticsElement) other;

            return (this.factorValue == otherSSE.factorValue)
                    && (this.respond == otherSSE.respond)
                    && (this.variance == otherSSE.variance)
                    && (this.coefficientOfVariation == otherSSE.coefficientOfVariation)
                    && (this.maxRespond == otherSSE.maxRespond)
                    && (this.minRespond == otherSSE.minRespond)
                    && (this.sampleSize == otherSSE.sampleSize);
        }

        return false;
    }

    @Override
    public final int hashCode() {
        int hash = 7;
        hash = 97 * hash + (int) (Double.doubleToLongBits(this.factorValue) ^ (Double.doubleToLongBits(this.factorValue) >>> 32));
        hash = 97 * hash + (int) (Double.doubleToLongBits(this.respond) ^ (Double.doubleToLongBits(this.respond) >>> 32));
        return hash;
    }

    @Override
    public final String toString() {
        return "SecondaryStatisticsElement(factorValue = " + factorValue
                + "; respondValue = " + respond + "; variance = " + variance
                + "; coeficient = " + coefficientOfVariation
                + "; min = " + minRespond + "; max = " + maxRespond
                + "; sampleSize = " + sampleSize;
    }

    /**
     * @return the variance
     */
    public double getVariance() {
        return variance;
    }

    /**
     * @return the coefficientOfVariation
     */
    public double getCoefficientOfVariation() {
        return coefficientOfVariation;
    }

    /**
     * @return the minRespond
     */
    public double getMinRespond() {
        return minRespond;
    }

    /**
     * @return the maxRespond
     */
    public double getMaxRespond() {
        return maxRespond;
    }

    /**
     * @return the sampleSize
     */
    public double getSampleSize() {
        return sampleSize;
    }
}