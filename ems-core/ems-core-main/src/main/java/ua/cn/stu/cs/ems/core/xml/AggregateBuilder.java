package ua.cn.stu.cs.ems.core.xml;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Namespace;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.aggregates.*;
import ua.cn.stu.cs.ems.core.queues.*;
import ua.cn.stu.cs.ems.core.transitions.*;
import ua.cn.stu.cs.ems.core.transitions.functions.InterpretedDelayFunction;
import ua.cn.stu.cs.ems.core.transitions.functions.InterpretedPermitingFunction;
import ua.cn.stu.cs.ems.core.transitions.functions.InterpretedTransformationFunction;
import ua.cn.stu.cs.ems.core.utils.Coordinates;
import ua.cn.stu.cs.ems.core.utils.Pair;
import ua.cn.stu.cs.ems.ecli.Value;

/**
 * AggregateBuilder builds aggregate definition from DOM tree created by JDOM.
 *
 * @author proger
 */
public class AggregateBuilder {

    final static Logger logger = LoggerFactory.getLogger(AggregateBuilder.class);
    Namespace ns;

    /**
     * Create aggregate definition from DOM tree
     *
     * @param document - DOM tree created by JDOM
     * @param registry - aggregate registry with aggregate definitions that can
     * be used to build aggregate with child aggregates
     * @return aggregate definition build from DOM tree
     * @throws PNMLParsingException - this exception is thrown if there is a
     * semantic error in PNML document.
     */
    //TODO add acrs coordinates parsing
    public AggregateDefinition buildAggregateDefinition(Document document, AggregateRegistry registry) throws PNMLParsingException {
        Element pnml = document.getRootElement();
        ns = pnml.getNamespace();
        Element netElement = pnml.getChild("net", ns);

        String adName = netElement.getAttributeValue("id");
        AggregateDefinition ad = new AggregateDefinition(adName);

        buildVariables(ad, netElement);

        Element page = netElement.getChild("page", ns);

        ListIterator<Element> transitionsIter = page.getChildren("transition", ns).listIterator();
        while (transitionsIter.hasNext()) {
            Element transitionElement = transitionsIter.next();
            buildTransition(transitionElement, ad, registry);
        }

        ListIterator<Element> placesIter = page.getChildren("place", ns).listIterator();
        while (placesIter.hasNext()) {
            Element placeElement = placesIter.next();
            buildPlace(placeElement, ad);
        }

        ListIterator<Element> arcsIter = page.getChildren("arc", ns).listIterator();
        while (arcsIter.hasNext()) {
            Element arcElement = arcsIter.next();
            buildArc(arcElement, ad);
        }

        return ad;
    }

    private void buildTransition(Element transitionElement, AggregateDefinition ad, AggregateRegistry registry) throws PNMLParsingException {
        String name = transitionElement.getAttributeValue("id");
        Element definitionElement = transitionElement.getChild("definition", ns);

        String transitionType = definitionElement.getAttributeValue("type");
        String transitionSubType = definitionElement.getAttributeValue("subType");

        AggregateChild aggregateChild;

        if (transitionType.equals("transition")) {
            Transition transition;
            if (transitionSubType.equals("T")) {
                transition = buildTTransition(definitionElement, name);
            } else if (transitionSubType.equals("F")) {
                transition = buildFTransition(definitionElement, name);
            } else if (transitionSubType.equals("J")) {
                transition = buildJTransition(definitionElement, name);
            } else if (transitionSubType.equals("X")) {
                transition = buildXTransition(definitionElement, name);
            } else if (transitionSubType.equals("Y")) {
                transition = buildYTransition(definitionElement, name);
            } else {
                throw new PNMLParsingException("Unknown transition type " + transitionSubType);
            }

            ad.addTransition(transition);
            aggregateChild = transition;
        } else if (transitionType.equals("aggregate")) {
            AggregateDefinition childAD = buildChildAggregateDefinition(definitionElement, registry);
            AggregateDefinitionReference adr = ad.addChildAggreagateDefinition(childAD, name);
            aggregateChild = adr;
        } else if (transitionType.equals("input")) {
            Input i = parseInput(definitionElement, name);
            ad.addInput(i, ad.getInputs().size());
            aggregateChild = i;
        } else if (transitionType.equals("output")) {
            Output o = parseOutput(definitionElement, name);
            ad.addOutput(o, ad.getOutputs().size());
            aggregateChild = o;
        } else if (transitionType.equals("queue")) {
            Queue queue;
            if (transitionSubType.equals("FIFO")) {
                queue = buildFIFOQueue(definitionElement, name);
            } else if (transitionSubType.equals("LIFO")) {
                queue = buildLIFOQueue(definitionElement, name);
            } else if (transitionSubType.equals("PriorityFIFO")) {
                queue = buildPriorityFIFO(definitionElement, name);
            } else if (transitionSubType.equals("PriorityLIFO")) {
                queue = buildPriorityLIFO(definitionElement, name);
            } else {
                throw new PNMLParsingException("Unknown queue type " + transitionSubType);
            }
            ad.addQueue(queue);
            aggregateChild = queue;
        } else {
            throw new PNMLParsingException("Unknown type of PNML transition " + transitionType);
        }

        Coordinates nameCoordinates = buildNameCoordinates(transitionElement.getChild("name", ns));
        Coordinates objectCoordinates = buildObjectCoordinates(transitionElement);

        aggregateChild.setNameCoordinates(nameCoordinates);
        aggregateChild.setObjectCoordinates(objectCoordinates);
    }

    private void buildPlace(Element placeElement, AggregateDefinition ad) throws PNMLParsingException {
        String placeName = placeElement.getAttributeValue("id");
        Place place = new Place(placeName);
        Element initialMarkingElement = placeElement.getChild("token", ns);

        if (initialMarkingElement != null) {
            Token token = new Token();
            try {
                for (Object element : initialMarkingElement.getChildren()) {
                    Element attributeElement = (Element) element;
                    String name = attributeElement.getAttributeValue("name");
                    String value = attributeElement.getAttributeValue("value");

                    token.setValue(name, new Value(value));
                }
            } catch (NumberFormatException ex) {
                throw new PNMLParsingException(ex);
            }
            place.setToken(token);
        }

        ad.addPlace(place);

        Coordinates nameCoordinates = buildNameCoordinates(placeElement.getChild("name", ns));
        Coordinates objectCoordinates = buildObjectCoordinates(placeElement);

        place.setNameCoordinates(nameCoordinates);
        place.setObjectCoordinates(objectCoordinates);
    }

    private void buildArc(Element arcElement, AggregateDefinition ad) throws PNMLParsingException {
        String sourceName = arcElement.getAttributeValue("source");
        String targetName = arcElement.getAttributeValue("target");

        Pair<Aggregate, AggregateChild> sourcePair = getAggregateChild(ad, sourceName);
        Pair<Aggregate, AggregateChild> targetPair = getAggregateChild(ad, targetName);

        Aggregate sourceParent = sourcePair.getFirst();
        Aggregate targetParent = targetPair.getFirst();

        AggregateChild source = sourcePair.getSecond();
        AggregateChild target = targetPair.getSecond();

//        logger.debug("buildArc : aggregate=" + ad.getName() + " arcElement=" + arcElement.hashCode());
//        logger.debug("source=" + sourceName + " target=" + targetName);
//        logger.debug("source=" + source + " target=" + target + " targetParent=" + targetParent);

        try {
            if (source instanceof Place) {
                Place sourcePlace = (Place) source;
                if (target instanceof Transition) {
                    Transition targetTransition = (Transition) target;
                    targetTransition.addInputPlace(sourcePlace, targetTransition.numberOfConnectedInputPlaces());
                } else if (target instanceof Queue) {
                    Queue targetQueue = (Queue) target;
                    // If no place was connected during loading
                    if (targetQueue.getInputPlace() == null) {
                        targetQueue.connectInputPlace(sourcePlace);
                    } else {
                        throw new PNMLParsingException("Queue can have only one input place "
                                + "but found more then one for " + targetName);
                    }
                } else if (target instanceof Input) {
                    Input targetInput = (Input) target;
                    if (targetInput.getInputPlace() == null) {
                        targetInput.setInputPlace(sourcePlace);
                    } else {
                        throw new PNMLParsingException("Input can have only one input place "
                                + "but found more then one for " + targetName);
                    }
                } else if (target instanceof Output) {
                    Output targetOuput = (Output) target;
                    if (targetOuput.getInputPlace() == null) {
                        targetOuput.connectInputPlace(sourcePlace);
                    } else {
                        throw new PNMLParsingException("Output can have only one input place "
                                + "but found more then one for " + targetName);
                    }
                } else {
                    throw new PNMLParsingException("Unable to connect " + sourceName + " and " + targetName);
                }
            } else if (target instanceof Place) {
                Place targetPlace = (Place) target;

                if (source instanceof Transition) {
                    Transition sourceTransition = (Transition) source;
                    sourceTransition.addOutputPlace(targetPlace, sourceTransition.numberOfConnectedOutputPlaces());
                } else if (source instanceof Queue) {
                    Queue sourceQueue = (Queue) source;
                    if (sourceQueue.getOutputPlace() == null) {
                        sourceQueue.connectOutputPlace(targetPlace);
                    } else {
                        throw new PNMLParsingException("Queue can have only one output place "
                                + "but found more then one for " + sourceName);
                    }
                } else if (source instanceof Input) {
                    Input sourceInput = (Input) source;
                    if (sourceInput.getOutputPlace() == null) {
                        sourceInput.setOutputPlace(targetPlace);
                    } else {
                        throw new PNMLParsingException("Input can have only one output place "
                                + "but found more then one for " + sourceName);
                    }
                } else if (source instanceof Output) {
                    Output sourceOutput = (Output) source;
                    if (sourceOutput.getOutputPlace() == null
                            && sourceOutput.getConnectedInputs().isEmpty()) {
                        sourceOutput.connectOutputPlace(targetPlace);
                    } else {
                        throw new PNMLParsingException("Output can have only one output place "
                                + "but found more then one for " + sourceName);
                    }
                } else {
                    throw new PNMLParsingException("Unable to connect " + sourceName + " and " + targetName);
                }
            } else if (source instanceof Output && target instanceof Input) {
//                logger.debug("connection aggregates");
                Output sourceOutput = (Output) source;
                Input targetInput = (Input) target;

                if (targetInput.getInputPlace() == null && targetInput.getConnectedOutput() == null) {
                    if (sourceOutput.getOutputPlace() == null) {
//                        logger.debug("Connect ok");
                        sourceOutput.addInput(targetInput, 0);
                    } else {
                        throw new PNMLParsingException("Output can be connected to single output or"
                                + "to several inputs");
                    }
                } else {
                    throw new PNMLParsingException("Input can have only one connected element "
                            + "but found more then one for " + targetName);
                }
            } else {
                throw new PNMLParsingException("Unable to connect " + sourceName + " and " + targetName);
            }

            List<Coordinates> coordinates = buildArcCoordinates(arcElement);
            ArcCoordinates arcCoordinates = new ArcCoordinates();

            if (sourceParent != null) {
                arcCoordinates.setSourceParent(sourceParent.getName());
            }
            if (sourceName.contains(".")) {
                sourceName = sourceName.substring(sourceName.indexOf('.') + 1);
            }
            arcCoordinates.setSourceName(sourceName);

            if (targetParent != null) {
                arcCoordinates.setTargetParent(targetParent.getName());
            }
            if (targetName.contains(".")) {
                targetName = targetName.substring(targetName.indexOf('.') + 1);
            }
            arcCoordinates.setTargetName(targetName);


            arcCoordinates.setCoordinates(coordinates);

//            logger.debug("build arc: adding: arc: sp=" + arcCoordinates.getSourceParent()
//                    + " s=" + arcCoordinates.getSourceName() + " tp="
//                    + arcCoordinates.getTargetParent() + " t=" + arcCoordinates.getTargetName());

            ad.addArcCoordinates(arcCoordinates);
        } catch (Exception ex) {
            throw new PNMLParsingException(ex);
        }
    }

    private List<Coordinates> buildArcCoordinates(Element arcElement) {
        Element graphicsElement = arcElement.getChild("graphics", ns);
        ListIterator<Element> listIter = graphicsElement.getChildren("position", ns).listIterator();
        List<Coordinates> coordinates = new ArrayList<Coordinates>();

        while (listIter.hasNext()) {
            Element positionElement = listIter.next();
            String xStr = positionElement.getAttributeValue("x");
            String yStr = positionElement.getAttributeValue("y");

            Coordinates coord = new Coordinates(Integer.parseInt(xStr), Integer.parseInt(yStr));
            coordinates.add(coord);
        }

        return coordinates;
    }

    private TTransition buildTTransition(Element definitionElement, String name) {
        TTransition transition = new TTransition(name);
        buildUsualTransitionFunctions(transition, definitionElement);
        return transition;
    }

    private FTransition buildFTransition(Element definitionElement, String name) {
        FTransition transition = new FTransition(name);
        buildUsualTransitionFunctions(transition, definitionElement);
        return transition;
    }

    private JTransition buildJTransition(Element definitionElement, String name) {
        JTransition transition = new JTransition(name);
        buildUsualTransitionFunctions(transition, definitionElement);
        return transition;
    }

    private void buildUsualTransitionFunctions(Transition transition, Element definitionElement) {
        Element delayFunctionElement = definitionElement.getChild("delayFunction", ns);
        Element transformationFunctionElement = definitionElement.getChild("transformationFunction", ns);

        if (delayFunctionElement != null) {
            transition.setDelayFunction(new InterpretedDelayFunction(delayFunctionElement.getText()));
        }

        if (transformationFunctionElement != null) {
            transition.setTransformationFunction(new InterpretedTransformationFunction(transformationFunctionElement.getText()));
        }
    }

    private XTransition buildXTransition(Element definitionElement, String name) {
        XTransition transition = new XTransition(name);
        buildSpecialTransitionFunctions(transition, definitionElement);
        return transition;
    }

    private YTransition buildYTransition(Element definitionElement, String name) {
        YTransition transition = new YTransition(name);
        buildSpecialTransitionFunctions(transition, definitionElement);
        return transition;
    }

    private void buildSpecialTransitionFunctions(SpecialTransition transition, Element definitionElement) {
        Element permitingFunctionElement = definitionElement.getChild("permitingFunction", ns);

        if (permitingFunctionElement != null) {
            transition.setPermitingFunction(new InterpretedPermitingFunction(permitingFunctionElement.getText()));
        }

        buildUsualTransitionFunctions(transition, definitionElement);
    }

    private Coordinates buildNameCoordinates(Element nameElement) throws PNMLParsingException {
        try {
            Element offsetElement = nameElement.getChild("graphics", ns).getChild("offset", ns);
            int x = Integer.parseInt(offsetElement.getAttributeValue("x"));
            int y = Integer.parseInt(offsetElement.getAttributeValue("y"));

            Coordinates coordinates = new Coordinates(x, y);
            return coordinates;
        } // Exception during int parsing
        catch (NumberFormatException ex) {
            throw new PNMLParsingException(ex);
        }
    }

    private Coordinates buildObjectCoordinates(Element transitionElement) throws PNMLParsingException {
        try {
            Element positionElement = transitionElement.getChild("graphics", ns).getChild("position", ns);
            int x = Integer.parseInt(positionElement.getAttributeValue("x"));
            int y = Integer.parseInt(positionElement.getAttributeValue("y"));

            Coordinates coordinates = new Coordinates(x, y);
            return coordinates;
        } // Exception during int parsing
        catch (NumberFormatException ex) {
            throw new PNMLParsingException(ex);
        }
    }

    private Input parseInput(Element definitionElement, String name) {
        return new InputImpl(name);
    }

    private Output parseOutput(Element definitionElement, String name) {
        return new OutputImpl(name);
    }

    private Queue buildFIFOQueue(Element definitionElement, String name) {
        return new FIFOQueue(name);
    }

    private Queue buildLIFOQueue(Element definitionElement, String name) {
        return new LIFOQueue(name);
    }

    private Queue buildPriorityFIFO(Element definitionElement, String name) {
        TokenPriorityFunction tpf = buildTokenPriorityFunction(definitionElement);
        FIFOPriorityQueue fpq = new FIFOPriorityQueue(name, tpf);

        return fpq;
    }

    private Queue buildPriorityLIFO(Element definitionElement, String name) {
        TokenPriorityFunction tpf = buildTokenPriorityFunction(definitionElement);
        LIFOPriorityQueue lpq = new LIFOPriorityQueue(name, tpf);

        return lpq;
    }

    private TokenPriorityFunction buildTokenPriorityFunction(Element definitionElement) {
        Element priorityFunctionElement = definitionElement.getChild("priorityFunction", ns);
        if (priorityFunctionElement == null) {
            return new DefaultTokenPriorityFunction();
        } else {
            return new InterpretedTokenPriorityFunction(priorityFunctionElement.getText());
        }
    }

    private AggregateDefinition buildChildAggregateDefinition(Element definitionElement, AggregateRegistry registry) throws PNMLParsingException {
        String childType = definitionElement.getAttributeValue("subType");
        AggregateDefinition childAD = registry.getAggregateDefinition(childType);

        if (childAD == null) {
            throw new PNMLParsingException("Aggregate type '" + childType + "' not exists");
        }

        return childAD;
    }

    private void buildVariables(AggregateDefinition ad, Element netElement) throws PNMLParsingException {
        Element variablesElement = netElement.getChild("variables", ns);

        if (variablesElement != null) {
            try {
                ListIterator<Element> variableIter = variablesElement.getChildren("variable", ns).listIterator();
                while (variableIter.hasNext()) {
                    Element variableElement = variableIter.next();

                    String name = variableElement.getAttributeValue("name");
                    String value = variableElement.getAttributeValue("value");

                    ad.setVariable(name, new Value(value));

                }
            } catch (NumberFormatException ex) {
                throw new PNMLParsingException(ex);
            }
        }
    }

    private Pair<Aggregate, AggregateChild> getAggregateChild(AggregateDefinition ad, String connectionElName)
            throws PNMLParsingException {
        int dotPos;
        Aggregate a = ad;
        dotPos = connectionElName.indexOf('.');
        Aggregate targetParent;
        if (dotPos < 0) {
            targetParent = null;
        } else {
            targetParent = a.getAggregate(connectionElName.substring(0, dotPos));
            if (targetParent == null) {
                throw new PNMLParsingException("Unknown aggregate child " + targetParent + " in aggregate " + ad.getName());
            }
        }

        do {
            dotPos = connectionElName.indexOf('.');
            if (dotPos > -1) {
                String childName = connectionElName.substring(0, dotPos);
                a = a.getAggregate(childName);
                if (a == null) {
                    throw new PNMLParsingException("Unknown aggregate child " + childName + " in path " + connectionElName + " of aggregate " + ad.getName() + " path");
                }
                connectionElName = connectionElName.substring(dotPos + 1);
            }
        } while (dotPos > -1);

        AggregateChild ac = a.getAggregateChild(connectionElName);
        if (ac == null) {
            throw new PNMLParsingException("Unknown aggregate child " + connectionElName);
        }
        return new Pair<Aggregate, AggregateChild>(targetParent, ac);
    }
}
