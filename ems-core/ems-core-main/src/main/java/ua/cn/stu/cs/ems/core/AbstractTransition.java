package ua.cn.stu.cs.ems.core;

import ua.cn.stu.cs.ems.core.transitions.functions.DefaultTransformationFunction;
import ua.cn.stu.cs.ems.core.transitions.functions.DefaultDelayFunction;
import java.util.ArrayList;
import java.util.List;
import ua.cn.stu.cs.ems.core.transitions.functions.DelayFunction;
import ua.cn.stu.cs.ems.core.transitions.functions.TransformationFunction;
import ua.cn.stu.cs.ems.core.transitions.Transition;
import static ua.cn.stu.cs.ems.core.utils.AssertUtils.*;

/**
 * Class AbstractTransition - parent class for all transition implementations.
 */
public abstract class AbstractTransition extends AbstractActiveInstance implements Transition {

    /**
     * Magic number that represents that it's permitied to connecect unlimited number
     * of input or output places.
     */
    public static final int UNLIMITED = 0;

    private DelayFunction delayFunction;
    private TransformationFunction transformationFunction;
    private List<Place> outputPlaces = new ArrayList<Place>();
    private List<Place> inputPlaces = new ArrayList<Place>();

    /**
     * Create AbstractTransition with a specified name.
     * @param name - name of a new transition
     */
    protected AbstractTransition(String name) {
        super(name);
        delayFunction = new DefaultDelayFunction();
        transformationFunction = new DefaultTransformationFunction();
    }

    /**
     * Get list of this transition's output places.
     * @return list of output places
     */
    public final List<Place> getOutputPlaces() {
        final ArrayList<Place> result = new ArrayList<Place>(outputPlaces);
        return result;
    }

    /**
     * Return number of output places connected to this transition.
     * @return number of output places
     */
    public final int numberOfOutputPlaces() {
        return outputPlaces.size();
    }

    /**
     * Get list of input places connected to this transition.
     * @return cloned list of input places
     */
    public final List<Place> getInputPlaces() {
        final ArrayList<Place> result = new ArrayList<Place>(inputPlaces);
        return result;
    }

    /**
     * Get number of input places connected to this transition.
     * @return  number of input places
     */
    public final int numberOfInputPlaces() {
        return inputPlaces.size();
    }

    /**
     * Get transformation function for this transition.
     * @return transformation function
     */
    public final TransformationFunction getTransformationFunction() {
        return transformationFunction;
    }

    /**
     * Set transformation function for this transition.
     * @param transformationFunction - transformation function to set.
     */
    public final void setTransformationFunction(TransformationFunction transformationFunction) {
        assertNotNull(transformationFunction, "Transformation function can't be null");

        this.transformationFunction = transformationFunction;
    }

    /**
     * Get this transition's delay function.
     * @return delay function
     */
    public final DelayFunction getDelayFunction() {
        return delayFunction;
    }

    /**
     * Set delay function for this transition.
     * @param delayFunction - delay function to set.
     */
    public final void setDelayFunction(DelayFunction delayFunction) {
        assertNotNull(delayFunction, "Delay function can't be null");

        this.delayFunction = delayFunction;
    }

    /**
     * Fire transition if state of the input and output places conform to transition fire conditions.
     */
    public final void fireIfPosible() {
        if (canFire()) {
            final double delay = getDelayFunction().getDelayTime(this);

            delayFor(delay);
        }
    }

    public void placeStateChanged(Place place) {
        fireIfPosible();
    }

    /**
     * Check if model state conforms to transition fire conditions.
     * @return true if trasition can fire, otherwise false.
     */
    protected abstract boolean canFire();

    /**
     * Connect place to this transition's input.
     * @param place - place to connect.
     * @param pos - number of place in list of places connected to this transition.
     */
    public final void addInputPlace(Place place, int pos) {
        checkAddParams(place, pos, inputPlaces.size());

        if (maxInputSize() != UNLIMITED) {
            assertBounds(pos, 0, maxInputSize() - 1, "pos should be less then max number of input places");
        }

        inputPlaces.add(pos, place);
        place.setOutputActiveInstance(this);
    }

    public final void disconnectInputPlace(String placeName) {

        for (int i = 0; i < inputPlaces.size(); i++) {
            final Place p = inputPlaces.get(i);
            if (p.getName().equals(placeName)) {
                disconnectInputPlace(i);
                // No more then one place with same name can exists in an agregate
                break;
            }
        }
    }

    public void addOutputPlace(Place place, int pos) {
        checkAddParams(place, pos, outputPlaces.size());

        if (maxOutputSize() != UNLIMITED) {
            assertBounds(pos, 0, maxOutputSize() - 1, "Positiont to add must be between 0 and maxOutputSize()");
        }

        outputPlaces.add(pos, place);
        place.setInputActiveInstance(this);
    }

    public void disconnectOutputPlace(String placeName) {
        for (int i = 0; i < outputPlaces.size(); i++) {
            final Place p = outputPlaces.get(i);
            if (p.getName().equals(placeName)) {
                disconnectOutputPlace(i);
                // No more then one place with same name can exists in an agregate
                break;
            }
        }
    }

    private void checkAddParams(Place place, int pos, int listSize) {
        assertNotNull(place, "Place can't be null");
        assertBounds(pos, 0, listSize, "pos should be > 0 and < number of places connected to transition");
    }

    public void replaceOutputPlace(Place newPlace, int pos) {
        checkReplaceParams(newPlace, pos, outputPlaces.size());

        final Place oldPlace = outputPlaces.get(pos);
        oldPlace.setInputActiveInstance(null);

        outputPlaces.set(pos, newPlace);
        newPlace.setInputActiveInstance(this);
    }

    public void replaceInputPlace(Place newPlace, int pos) {
        checkReplaceParams(newPlace, pos, inputPlaces.size());

        final Place oldPlace = inputPlaces.get(pos);
        oldPlace.setOutputActiveInstance(null);

        inputPlaces.set(pos, newPlace);
        newPlace.setOutputActiveInstance(this);
    }

    private void checkReplaceParams(Place place, int pos, int listSize) {
        assertNotNull(place, "Place can't be null");
        assertBounds(pos, 0, listSize - 1, "pos should be > 0 and < number of possitions connected to transition - 1");
    }

    /**
     * Get maximum number of input places that can be connected to this transition.
     * @return maximum number of input places that can be connected to this transition.
     */
    protected abstract int maxInputSize();

    /**
     * Get maximum number of output places that can be connected to this transition.
     * @return maximum number of output places that can be connected to this transition.
     */
    protected abstract int maxOutputSize();

    public final void disconnectInputPlace(int pos) {
        assertTrue(pos >= 0, "Can't delete position with number < 0");

        inputPlaces.get(pos).setOutputActiveInstance(null);
        inputPlaces.remove(pos);
    }

    public final void disconnectOutputPlace(int pos) {
        assertTrue(pos >= 0, "Can't delete position with number < 0");

        outputPlaces.get(pos).setInputActiveInstance(null);
        outputPlaces.remove(pos);
    }

    public final int numberOfConnectedInputPlaces() {
        return inputPlaces.size();
    }

    public final int numberOfConnectedOutputPlaces() {
        return outputPlaces.size();
    }

    @Override
    public final String toString() {
        String result = "";
        result += this.getClass().getSimpleName() + " ";
        result += "Name: " + getName() + " ";
        result += "(" + "Inputs: ";

        for (Place pos : getInputPlaces()) {
            result += pos.getName() + ", ";
        }

        result += "Outputs: ";

        for (Place pos : getOutputPlaces()) {
            result += pos.getName() + ", ";
        }

        result += ")";
        return result;
    }
}
