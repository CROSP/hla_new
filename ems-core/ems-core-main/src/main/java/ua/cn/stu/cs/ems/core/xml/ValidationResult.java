package ua.cn.stu.cs.ems.core.xml;

import java.util.ArrayList;
import java.util.List;

/**
 * ValidationResult - holds result of XML validation. *
 * @see XMLValidator
 * @author Ivan Mushketik
 */
public class ValidationResult {

    List<String> warnings = new ArrayList<String>();
    List<String> errors = new ArrayList<String>();
    List<String> fatalErrors = new ArrayList<String>();

    public void addWarning(String warning) {
        warnings.add(warning);
    }

    public void addError(String error) {
        errors.add(error);
    }

    public void addFatalError(String fatalError) {
        fatalErrors.add(fatalError);
    }

    public List<String> getWarnings() {
        return warnings;
    }

    public List<String> errors() {
        return errors;
    }

    public List<String> fatalErrors() {
        return fatalErrors;
    }

    boolean hasWarnings() {
        return !warnings.isEmpty();
    }

    boolean hasErrors() {
        return !errors.isEmpty();
    }

    boolean hasFatalErrors() {
        return !fatalErrors.isEmpty();
    }


}
