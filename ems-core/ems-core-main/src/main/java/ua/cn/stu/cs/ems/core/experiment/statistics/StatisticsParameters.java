package ua.cn.stu.cs.ems.core.experiment.statistics;

import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.aggregates.AggregateChild;
import ua.cn.stu.cs.ems.core.queues.Queue;
import ua.cn.stu.cs.ems.core.transitions.Transition;

/**
 * Parameters for statistics collectors
 *
 * @author proger
 */
public enum StatisticsParameters {
    // PlaceStatisticsCollector params
    OCCUPIED_COEFFICIENT("Occupied coefficient"),
    NUMBER_OF_PASSED_TOKENS("Number of passed tokens"),
    AVERAGE_OCCUPIED_TIME("Average occupied time"),

    // QueueStatisticsCollector params
    AVERAGE_QUEUE_LENGTH("Average queue length"),
    AVERAGE_TIME_IN_QUEUE("Average time in queue"),

    // Aditional TransitionStatisticsCollector params
    AVERAGE_TIME_IN_DELAY("Average time in delay");


    private String name;

    private StatisticsParameters(String theName) {
        this.name = theName;
    }
    
    public final String getPresentation() {
        return name;
    }

    @Override
    public final String toString() {
//        return "StatisticsParameter('" + name + "')";
        return name;
    }

    public static StatisticsParameters[] getApplicableParameters(AggregateChild entity) {
        if (entity instanceof Place) {
            return new StatisticsParameters[]{AVERAGE_OCCUPIED_TIME, OCCUPIED_COEFFICIENT, NUMBER_OF_PASSED_TOKENS};
        } else if (entity instanceof Queue) {
            return new StatisticsParameters[]{AVERAGE_QUEUE_LENGTH, NUMBER_OF_PASSED_TOKENS, AVERAGE_TIME_IN_QUEUE};
        } else if (entity instanceof Transition) {
            return new StatisticsParameters[]{AVERAGE_TIME_IN_DELAY, OCCUPIED_COEFFICIENT, NUMBER_OF_PASSED_TOKENS};
        } else {
            throw new IllegalArgumentException("Unsupported entity type!");
        }
    }
}
