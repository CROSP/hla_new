package ua.cn.stu.cs.ems.core.ecli;

import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.aggregates.Aggregate;
import ua.cn.stu.cs.ems.ecli.Value;

import java.util.HashMap;
import java.util.Map;

/**
 * Accessor that does not commit changes to the model. Should be used for testing purposes only.
 *
 * @author n0weak
 */
public class IdleAggregateAccessor extends EcliAggregateAccessor {

    private final Map<String, Object> modifiedData = new HashMap<String, Object>();
    private final Map<Aggregate, IdleAggregateAccessor> requestedAggregates = new HashMap<Aggregate, IdleAggregateAccessor>();
    private final Map<Token, IdleTokenAccessor> requestedTokens = new HashMap<Token, IdleTokenAccessor>();

    public IdleAggregateAccessor(Aggregate aggregate) {
        super(aggregate);
    }

    @Override
    public boolean containsVar(Object key) {
        return modifiedData.containsKey(String.valueOf(key)) || super.containsVar(key);
    }

    @Override
    public Object getVariable(Object key) {
        Object value = modifiedData.get(String.valueOf(key));
        return null != value ? value : super.getVariable(key);
    }

    @Override
    public boolean setVariable(Object key, Value value) {
        modifiedData.put(String.valueOf(key), value.getValue());
        return true;
    }

    public void clearModifications() {
        modifiedData.clear();
        requestedAggregates.clear();
        requestedTokens.clear();
    }

    @Override
    protected IdleTokenAccessor wrap(Token t) {
        if (null != t) {
            IdleTokenAccessor accessor = requestedTokens.get(t);
            if (null == accessor) {
                accessor = new IdleTokenAccessor(t);
                requestedTokens.put(t, accessor);
            }
            return accessor;
        } else {
            return null;
        }
    }

    @Override
    protected IdleAggregateAccessor wrap(Aggregate child) {
        if (null != child) {
            IdleAggregateAccessor accessor = requestedAggregates.get(child);
            if (null == accessor) {
                accessor = new IdleAggregateAccessor(child);
                requestedAggregates.put(child, accessor);
            }
            return accessor;
        } else {
            return null;
        }
    }
}
