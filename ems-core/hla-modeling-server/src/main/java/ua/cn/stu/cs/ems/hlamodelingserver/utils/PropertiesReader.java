package ua.cn.stu.cs.ems.hlamodelingserver.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author Dmitry Krivenko <dmitrykrivenko@gmal.com>
 */
public class PropertiesReader {

    final static Logger logger = LoggerFactory.getLogger(PropertiesReader.class);
    /**
     * Should be in the same directory where is executing jar
     */
    private static final String PROPS_FILE = "hla.properties";
    private static final Properties props = loadProperties();

    public enum Property {

        DB_CONNECTION_HOST("db.connection.host"),
        DB_CONNECTION_PORT("db.connection.port"),
        DB_CONNECTION_USERNAME("db.connection.username"),
        DB_CONNECTION_PASSWORD("db.connection.password"),
        DB_CONNECTION_DRIVER("db.connection.driver"),
        DB_CONNECTION_CHARSET("db.connection.charset"),
        DB_CONNECTION_COUNT("db.connection.count"),
        DB_TABLE_NAME("db.table.name"),
        DB_TABLE_PK("db.table.pk"),
        DB_TABLE_C_NAME("db.table.c.name"),
        DB_TABLE_C_DATA("db.table.c.data"),
        DB_TABLE_C_TYPE("db.table.c.type"),
        RTI_HOME("rti.home");
        private final String key;

        Property(String key) {
            this.key = key;
        }

        public String getKey() {
            return key;
        }
    }

    public static String read(Property property) {
        return props.getProperty(property.getKey());
    }

    private static Properties loadProperties() {
        Properties properties = new Properties();
        InputStream is = null;
        String userDir = System.getProperty("user.dir");
        String fileSeparator = System.getProperty("file.separator");
        String pathToPropsFile = userDir + fileSeparator + PROPS_FILE;
        try {
            is = new FileInputStream(pathToPropsFile);
        } catch (FileNotFoundException ex) {
            logger.error("Can't find properties file " + PROPS_FILE, ex);
        }
        try {
            properties.load(is);
        } catch (IOException ex) {
            logger.error("Failed to load properties from " + pathToPropsFile, ex);
            System.exit(0);
        }
        try {
            is.close();
        } catch (IOException ex) {
            logger.error("Failed to close inputstream while properties loading", ex);
        }
        return properties;
    }
    /**
     * Can be uncommented for testing
     */
//        Properties properties = new Properties();
//        InputStream in = PropertiesReader.class.getClassLoader().getResourceAsStream(PROPS_FILE);
//
//        try {
//            properties.load(in);
//        } catch (Exception e) {
//            logger.error("Failed to load properties from " + PROPS_FILE, e);
//            System.exit(0);
//        }
//
//        return properties;
//    }
}