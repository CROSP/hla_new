package ua.cn.stu.cs.ems.hlamodelingserver.utils.db;

import ua.cn.stu.cs.ems.hlamodelingserver.utils.AggregatesManager.AggregateType;

/**
 *
 * @author leonid
 */
public class AggregateEntity {

    private long id;
    private String name;
    private String data;
    private AggregateType type;

    public AggregateEntity(long id, String name, String data, String type) {
        this.id = id;
        this.name = name;
        this.data = data;
        this.type = AggregateType.valueOf(type.toUpperCase());
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getData() {
        return data;
    }

    public AggregateType getType() {
        return type;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setData(String data) {
        this.data = data;
    }

    public void setAggregateType(String type) {
        this.type = AggregateType.valueOf(type);
    }
};
