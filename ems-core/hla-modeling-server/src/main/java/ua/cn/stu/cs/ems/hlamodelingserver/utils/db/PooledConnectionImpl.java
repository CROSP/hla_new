/*$Id: PooledConnectionImpl.java,v 1.1.1.1 2001/11/08 10:55:22 trident Exp $*/
/**
 * Pooled Connection implementation of J2EE javax.sql interface.
 * @author Vladimir Kirichenko - $Author: trident $
 * @version $Revision: 1.1.1.1 $ 
 */
package ua.cn.stu.cs.ems.hlamodelingserver.utils.db;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Vector;
import javax.sql.ConnectionEvent;
import javax.sql.ConnectionEventListener;
import javax.sql.PooledConnection;

class PooledConnectionImpl implements PooledConnection {
	private Connection connection;
	private Vector connectionEventListeners = new Vector();
	private boolean busy = false;

	/**
	 * Constructor. Creates new PooledConnectionImpl and binds it to
	 * java.sql.Connection.
	 * 
	 * @param _conection
	 *            java.sql.Connection
	 */
	public PooledConnectionImpl(Connection _connection) {
		setConnection(_connection);
	}

	/**
	 * Bean 'get' method for bean property connection.
	 * 
	 * @return java.sql.Connection
	 * @excepton SQLException
	 */
	public Connection getConnection() throws SQLException {
		if (busy)
			return connection;
		else
			throw new SQLException("PooledConnection Closed!");
	}

	/**
	 * Bean 'set' method for bean property connection.
	 * 
	 * @param _connection
	 *            java.sql.Connection
	 */
	void setConnection(Connection _connection) {
		connection = _connection;
	}

	/**
	 * Returns if the connection is closed.
	 * 
	 * @return true/false
	 */
	public boolean isClosed() throws SQLException {
		return connection.isClosed();
	}

	/**
	 * Connection event listener add method. Implements J2EE Event Model.
	 * 
	 * @param _listener
	 *            Listener
	 */
	public void addConnectionEventListener(ConnectionEventListener _listener) {
		connectionEventListeners.add(_listener);
	}

	/**
	 * Connection event listener remove method. Implements J2EE Event Model.
	 * 
	 * @param _listener
	 *            Listener
	 */
	public void removeConnectionEventListener(ConnectionEventListener _listener) {
		connectionEventListeners.remove(_listener);
	}

	/**
	 * Notification of listeners that PooledConnection is closed.
	 */
	private void notifyConnectionClosed() {
		Vector v = (Vector) connectionEventListeners.clone();
		ConnectionEvent event = new ConnectionEvent(this);
		int i;
		for (i = 0; i < v.size(); i++) {
			((ConnectionEventListener) v.get(i)).connectionClosed(event);
		}
	}

	/**
	 * Closes PooledConnection.
	 */
	public void close() {
		notifyConnectionClosed();
	}

	/**
	 * Bean 'set' method for bean property busy.
	 * 
	 * @param _busy
	 *            value of property busy to be set.
	 */
	void setBusy(boolean _busy) {
		busy = _busy;
	}

	/**
	 * Bean 'get' method for bean property busy.
	 * 
	 * @return value of property busy.
	 */
	public boolean getBusy() {
		return busy;
	}

	public void removeStatementEventListener(javax.sql.StatementEventListener listener) {
	}

	public void addStatementEventListener(javax.sql.StatementEventListener listener) {
	}
}