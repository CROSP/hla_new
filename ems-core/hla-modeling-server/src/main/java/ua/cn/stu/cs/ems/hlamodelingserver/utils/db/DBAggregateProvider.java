package ua.cn.stu.cs.ems.hlamodelingserver.utils.db;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringReader;
import java.util.HashMap;
import java.util.List;
import org.jdom.Document;
import org.jdom.input.SAXBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.cn.stu.cs.ems.core.aggregates.*;
import ua.cn.stu.cs.ems.core.xml.AggregateBuilder;
import ua.cn.stu.cs.ems.core.xml.AggregateTypesReader;
import ua.cn.stu.cs.ems.hlamodelingserver.utils.AggregatesManager;
import ua.cn.stu.cs.ems.hlamodelingserver.utils.PropertiesReader;
import ua.cn.stu.cs.hla.network.AggregateProvider;

/**
 * Database implementation of the aggregate provider {@link AggregateProvider}.
 *
 * @author Dmitry Krivenko <dmitrykrivenko@gmal.com>
 */
public class DBAggregateProvider implements AggregateProvider {

    final static Logger logger = LoggerFactory.getLogger(DBAggregateProvider.class);

    public AggregateInstance getAggretate(String modelName, String aggName) {
        AggregateInstance ai = getAggregate(modelName, aggName);
        return ai;
    }

    private AggregateDefinition getModel(String name, AggregateRegistry registry) {
        AggregateDefinition agg = null;
        try {
            String data_table_cname = PropertiesReader.read(PropertiesReader.Property.DB_TABLE_C_NAME);
            AggregatesManager am = AggregatesManager.getFactory();
            List<AggregateEntity> ls = am.list(AggregatesManager.AggregateType.MODEL, data_table_cname + "='" + name + "'");
            AggregateBuilder ab = new AggregateBuilder();
            Document dom = new SAXBuilder().build(new StringReader(ls.get(0).getData()));
            return ab.buildAggregateDefinition(dom, registry);
        } catch (Exception e) {
            String msg = "Failed to load the model: " + name;
            logger.error(msg, e);
        }
        return agg;
    }

    private AggregateRegistry getAggregateRegestryCopy() {
        List<AggregateEntity> db = AggregatesManager.getFactory().list(AggregatesManager.AggregateType.TYPE);

        HashMap<String, InputStream> sources = new HashMap<String, InputStream>();
        for (AggregateEntity ae : db) {
            String name = ae.getName();
            InputStream is = new ByteArrayInputStream(ae.getData().getBytes());
            sources.put(name, is);
        }

        AggregateTypesReader atr = new AggregateTypesReader();
        try {
            return atr.readAggregateTypes(sources);
        } catch (Exception ex) {
            logger.error("Exception while reading aggregate types", ex);
            return null;
        }
    }

    private AggregateInstance getAggregate(String modelName, String name) {
        AggregateDefinition rootAgg = getModel(modelName, getAggregateRegestryCopy());
        AggregateDefinitionReference agg = (AggregateDefinitionReference) rootAgg.getAggregate(name);
        AggregateInstance ai = agg.getAggregateDefinition().createInstance(agg.getName());
        List<Input> inputs = agg.getInputs();
        for (Input input : inputs) {
            if (input.getConnectedOutput() != null) {
                ai.getInput(input.getName()).connectOutput(input.getConnectedOutput());
            }
        }
        List<Output> outputs = agg.getOutputs();
        for (Output output : outputs) {
            if (!output.getConnectedInputs().isEmpty()) {
                ai.getOutput(output.getName()).addInput(output.getConnectedInputs().get(0), 0);
            }
        }
        return ai;
    }
}
