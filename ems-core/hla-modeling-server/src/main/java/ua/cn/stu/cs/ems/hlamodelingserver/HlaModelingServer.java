package ua.cn.stu.cs.ems.hlamodelingserver;

import java.net.URL;
import org.portico.utils.classpath.Classpath;
import ua.cn.stu.cs.ems.hlamodelingserver.utils.PropertiesReader;
import ua.cn.stu.cs.ems.hlamodelingserver.utils.db.DBAggregateProvider;
import ua.cn.stu.cs.hla.core.utils.HlaUtils;
import ua.cn.stu.cs.hla.network.AggregateProvider;
import ua.cn.stu.cs.hla.network.modelingserver.ModelingServer;

/**
 *
 * @author Dmitry Krivenko <dmitrykrivenko@gmal.com>
 */
public class HlaModelingServer {
    
    private static ModelingServer ms = null;

    public static void main(String[] args) {
      //  Log.set(Log.LEVEL_NONE);

        if (System.getProperty("rti.home") != null) {
            // START as webapp, all must be intilized before
        }
        else {
            // MANUAL START !!!!
            //Need add path to portico.jar file
            HlaUtils.addToClasspath("file://" + PropertiesReader.read(PropertiesReader.Property.RTI_HOME));
            printClassPath();
            //Don't forget to set this property!!!
            System.setProperty("java.net.preferIPv4Stack", "true");
        }

        AggregateProvider aggProvider = new DBAggregateProvider();
        ms = new ModelingServer(null, aggProvider);

        ms.start();
    }
    
    public static void destroy() {
        if (ms != null) {
            ms.stop(); // TODO properly shutdown, not all threads are stopped
        }
    }

    private static void printClassPath() {
        Classpath classpath = new Classpath();
        URL[] originalSystemPath = classpath.getUrlSearchPath();
        System.out.println("Classpath:");
        for (URL url : originalSystemPath) {
            System.out.println(url.getPath());
        }
    }
}
