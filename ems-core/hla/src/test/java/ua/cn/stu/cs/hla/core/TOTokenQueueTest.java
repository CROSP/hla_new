package ua.cn.stu.cs.hla.core;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static ua.cn.stu.cs.ems.core.utils.AssertUtils.assertFalse;
import static ua.cn.stu.cs.ems.core.utils.AssertUtils.assertTrue;

import org.junit.*;
import ua.cn.stu.cs.ems.core.Token;

/**
 *
 * @author Dmitry Krivenko <dmitrykrivenko@gmal.com>
 */
public class TOTokenQueueTest {

    public TOTokenQueueTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of addTOToken method, of class TOTokenQueue.
     */
    @Test
    public void testAddTOToken() {
        System.out.println("addTOToken");

        Token token = new Token();
        TOTokenQueue toQueue = new TOTokenQueue();
        TOToken toToken = new TOToken(token, 0, null);
        toQueue.addTOToken(toToken, 0.0);
        assertSame(token, toQueue.getFirstTOToken().getToken());
    }

    /**
     * Test of getFirstTOTokenTime method, of class TOTokenQueue.
     */
    @Test
    public void testGetFirstTOTokenTime() {
        System.out.println("getFirstTOTokenTime");

        Token token1 = new Token();
        TOToken toToken1 = new TOToken(token1, 0, null);
        Token token2 = new Token();
        TOToken toToken2 = new TOToken(token2, 0, null);

        TOTokenQueue toQueue = new TOTokenQueue();

        toQueue.addTOToken(toToken1, 2.0);
        toQueue.addTOToken(toToken2, 1.0);

        double result = toQueue.getFirstTOTokenTime();
        assertEquals(1.0, result, 0.0);
    }

    /**
     * Test of getFirstTOToken method, of class TOTokenQueue.
     */
    @Test
    public void testGetFirstTOToken() {
        System.out.println("getFirstTOToken");

        Token token1 = new Token();
        TOToken toToken1 = new TOToken(token1, 0, null);
        Token token2 = new Token();
        TOToken toToken2 = new TOToken(token2, 0, null);

        TOTokenQueue toQueue = new TOTokenQueue();

        toQueue.addTOToken(toToken1, 2.0);
        toQueue.addTOToken(toToken2, 1.0);

        Token result = toQueue.getFirstTOToken().getToken();
        assertEquals(token2, result);
    }

    /**
     * Test of getSize method, of class TOTokenQueue.
     */
    @Test
    public void testGetSize() {
        System.out.println("getSize");

        Token token1 = new Token();
        TOToken toToken1 = new TOToken(token1, 0, null);
        Token token2 = new Token();
        TOToken toToken2 = new TOToken(token2, 0, null);
        Token token3 = new Token();
        TOToken toToken3 = new TOToken(token3, 0, null);

        TOTokenQueue toQueue = new TOTokenQueue();

        toQueue.addTOToken(toToken1, 1.0);
        toQueue.addTOToken(toToken2, 3.0);
        toQueue.addTOToken(toToken3, 2.0);

        int result = toQueue.getSize();
        assertEquals(3, result);
    }

    /**
     * Test of isEmpty method, of class TOTokenQueue.
     */
    @Test
    public void testIsEmpty() {
        System.out.println("isEmpty");

        Token token = new Token();
        TOToken toToken = new TOToken(token, 0, null);
        TOTokenQueue toQueue = new TOTokenQueue();
        toQueue.addTOToken(toToken, 0.0);
        assertFalse(toQueue.isEmpty());
    }

    /**
     * Test of clear method, of class TOTokenQueue.
     */
    @Test
    public void testClear() {
        System.out.println("clear");

        Token token = new Token();
        TOToken toToken = new TOToken(token, 0, null);
        TOTokenQueue toQueue = new TOTokenQueue();
        toQueue.addTOToken(toToken, 0.0);
        assertFalse(toQueue.isEmpty());

        toQueue.clear();
        assertTrue(toQueue.isEmpty());
    }
}
