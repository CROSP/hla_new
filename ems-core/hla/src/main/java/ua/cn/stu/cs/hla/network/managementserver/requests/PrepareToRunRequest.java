package ua.cn.stu.cs.hla.network.managementserver.requests;

import java.util.List;

/**
 * Request that central server {@link ManagementServer} send before run the
 * federate.
 *
 * @author Dmitry Krivenko <dmitrykrivenko@gmal.com>
 */
public class PrepareToRunRequest extends AbstractMgmntSRequest {

    public double finishTime;
    public double dumpPeriod;
    public boolean collectPrimaryStat;
    public boolean collectStatOnEvent;
    public List<String> objectsForCollectStat;

    @Override
    public String toString() {
        return "PrepareToRunRequest[federationName=" + federationName + "; federateName="
                + federateName + "; rootAggregateName=" + rootAggregateName
                + "; aggregateName=" + aggregateName + "; finishTime=" + finishTime
                + "; dumpPeriod=" + dumpPeriod + "; collectPrimaryStat=" + collectPrimaryStat
                + ";  collectStatOnEvent=" + collectStatOnEvent + "]";
    }
}
