package ua.cn.stu.cs.hla.examples;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.xml.sax.SAXException;
import ua.cn.stu.cs.hla.core.FomXmlParsing;
import ua.cn.stu.cs.hla.core.XmlInfo;

public class FomXmlExample {

    public void generateFomXml() {
        // tests to generating fom xml file
        List<XmlInfo> classData = new ArrayList<XmlInfo>();
        XmlInfo classInfo1 = new XmlInfo("Model1", new ArrayList<String>() {

            {
                add("attr1");
                add("attr2");
                add("attr3");
                add("attr4");
                add("attr5");
                add("attr6");
                add("attr7");
                add("attr8");
                add("attr9");
            }
        });
        classData.add(classInfo1);
        XmlInfo classInfo2 = new XmlInfo("Model2", new ArrayList<String>() {

            {
                add("attr1");
                add("attr2");
                add("attr3");
                add("attr4");
                add("attr5");
                add("attr6");
            }
        });
        classData.add(classInfo2);

        List<XmlInfo> interactionData = new ArrayList<XmlInfo>();
        XmlInfo interactionInfo1 = new XmlInfo("X1", new ArrayList<String>() {

            {
                add("attr1");
                add("attr2");
                add("attr3");
            }
        });
        interactionData.add(interactionInfo1);
        XmlInfo interactionInfo2 = new XmlInfo("X2", new ArrayList<String>() {

            {
                add("attr1");
                add("attr2");
                add("attr3");
                add("attr4");
            }
        });
        interactionData.add(interactionInfo2);

        try {
            FomXmlParsing.createFomFile(classData, interactionData, "c:\\FOM.xml");
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println("File created");
    }

    // FOM file should be in root project directory!!!
    public void viewFomXmlElements() {
        // tests to view elements
        try {
            List<String> objects = FomXmlParsing.getNamedElements("FOM.xml", "objectClass");
            System.out.println(String.format("Elements %s", objects));
            List<String> objectAttrs = FomXmlParsing.getAttributes("FOM.xml", "objectClass", "Model1");
            System.out.println(String.format("Attrs %s", objectAttrs));
            List<String> interactions = FomXmlParsing.getNamedElements("FOM.xml", "interactionClass");
            System.out.println(String.format("Elements %s", interactions));
            List<String> interactionAttrs = FomXmlParsing.getAttributes("FOM.xml", "interactionClass", "X1");
            System.out.println(String.format("Attrs %s", interactionAttrs));
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param args
     */
    public static void main(String[] args) {

        FomXmlExample example = new FomXmlExample();

        example.generateFomXml();

        example.viewFomXmlElements();

    }
}
