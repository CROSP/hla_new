package ua.cn.stu.cs.hla.core;

import hla.rti.*;
import hla.rti.jlc.RtiFactoryFactory;
import java.io.File;
import java.net.MalformedURLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Dmitrij999
 */
public class FederationExecution {

    final static Logger logger = LoggerFactory.getLogger(FederationExecution.class);
    private RTIambassador rtiAmbassador = null;
    private String fomFilePath = "";
    private volatile String destroyFederationName = "";
    private volatile boolean federationCreated = false;

    private boolean initRtiAmbassador() {
        boolean rtiAmbStatus = false;
        try {
            rtiAmbassador = RtiFactoryFactory.getRtiFactory().createRtiAmbassador();
            rtiAmbStatus = true;
        } catch (RTIinternalError e) {
            logger.error(e.getMessage());
        }
        return rtiAmbStatus;
    }

    public void createFederation(String federationName, String fom) {
        this.fomFilePath = fom;
        FederationThread federationThread = new FederationThread(federationName);
        federationThread.start();
    }

    public void destroyFederation(String federationName) throws FederatesCurrentlyJoined, FederationExecutionDoesNotExist, RTIinternalError,
            ConcurrentAccessAttempted {
        rtiAmbassador.destroyFederationExecution(federationName);
        destroyFederationName = federationName;
    }

    /**
     * @return the federationCreated
     */
    public boolean isFederationCreated() {
        return federationCreated;
    }

    public class FederationThread implements Runnable {

        private String federationName;
        private volatile Thread currentThread;

        public FederationThread(String federationName) {
            this.federationName = federationName;
        }

        public void start() {
            currentThread = new Thread(this);
            currentThread.setName(String.format("%s Federation Thread", federationName));
            currentThread.start();
        }

        public void stop() {
            currentThread = null;
        }

        private boolean createFederation() {
            boolean rtiAmbStatus = initRtiAmbassador();
            boolean federationStatus = false;
            File fom = new File(fomFilePath);
            if (rtiAmbStatus) {
                try {
                    rtiAmbassador.createFederationExecution(federationName, fom.toURI().toURL());
                    federationStatus = true;
                } catch (FederationExecutionAlreadyExists e) {
                    logger.error(e.getMessage());
                } catch (CouldNotOpenFED e) {
                    logger.error(e.getMessage());
                } catch (ErrorReadingFED e) {
                    logger.error(e.getMessage());
                } catch (RTIinternalError e) {
                    logger.error(e.getMessage());
                } catch (ConcurrentAccessAttempted e) {
                    logger.error(e.getMessage());
                } catch (MalformedURLException e) {
                    logger.error(e.getMessage());
                }
            }
            return federationStatus;
        }

        public void run() {
            boolean federationStatus = createFederation();
            if (federationStatus) {
                federationCreated = true;
                while (currentThread == Thread.currentThread() && !currentThread.getName().equals(String.format("%s Federation Thread", destroyFederationName))) {
                    try {
                        Thread.sleep(1 * 1000);
                    } catch (InterruptedException e) {
                        logger.error(e.getMessage());
                        Thread.currentThread().interrupt();
                    }
                }
            }
        }
    }
}
