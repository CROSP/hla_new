package ua.cn.stu.cs.hla.core;

import hla.rti.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class for time regulation.
 *
 * @author Dmitry Krivenko <dmitrykrivenko@gmal.com>
 */
public class TimeRegulation {

    private final static Logger logger = LoggerFactory.getLogger(TimeRegulation.class);
    private ModelFederateAmbassador federateAmbassador;
    private RTIambassador rtiAmbassador;
    private boolean wasAdvancedAfterFinish = false;

    public TimeRegulation(ModelFederateAmbassador federateAmbassador, RTIambassador rtiAmbassador) {
        this.federateAmbassador = federateAmbassador;
        this.rtiAmbassador = rtiAmbassador;
    }

    /**
     * Check if we can advice time to new value. If not - we should continue
     * modeling with the same time.
     *
     * @param timestep time step
     * @param modelingTime current modeling time
     * @return true if we can advice time, otherwise false
     */
    public double canAdvanceToTime(double timestep, double modelingTime, double finishTime, boolean endFlag) throws RTIinternalError, ConcurrentAccessAttempted, RTIexception, InterruptedException {
        //1. check the toTokenQueue;
        //2. if toTokenQueue is not empty get first time, else go to the step 4;
        //3. if first time < (modelingTime + timestep) set to the HLAInput token and return first time;
        //4. if toTokenQueue is empty - check if federateAmbassador.federateTime==modelingTime+timestep;
        //  4.1. if yes - return federateAmbassador.federateTime;
        //  4.2. if no - go to the next step 5;
        //5. call advanceTime(timestep);
        //6. while advanceTime(timestep) executing toTokenQueue can be filled
        //7. get TimeAdvanceGrant, set federateAmbassador.federateTime=federateTime+timestep;
        //8. go to the step 1.
        logger.debug("Advicing time - timestep:" + timestep + "; modelingTime:" + modelingTime + "; finishTime:" + finishTime);
        double advanceTime = modelingTime + timestep;
        while (true) {
            if (!federateAmbassador.toTokens.isEmpty()) {
                return federateAmbassador.toTokens.getFirstTOTokenTime();
            } else if (timestep == 0) {
                logger.debug("timestep == 0");
                if (modelingTime == finishTime && endFlag && !wasAdvancedAfterFinish) {
                    wasAdvancedAfterFinish = true;
                    advanceTime(1);
                }
                return advanceTime;
            } else {
                if (federateAmbassador.federateTime == advanceTime) {
                    logger.debug("federateAmbassador.federateTime == advanceTime");
                    if (federateAmbassador.federateTime == finishTime && endFlag && !wasAdvancedAfterFinish) {
                        wasAdvancedAfterFinish = true;
                        advanceTime(1);
                    }
                    return advanceTime;
                } else if (advanceTime > finishTime) {
                    logger.debug("advanceTime > finishTime");
                    if (finishTime != modelingTime) {
                        logger.debug("advanceTime(finishTime - modelingTime)");
                        advanceTime(finishTime - modelingTime);
                    }
                    return advanceTime;
                } else {
                    if (advanceTime <= federateAmbassador.federateTime) {
                        logger.debug("advanceTime <= federateAmbassador.federateTime");
                        return advanceTime;
                    }
                    advanceTime(timestep);
                }
            }
        }
    }

    /**
     * This method will request a time advance to the current time, plus the
     * given timestep. It will then wait until a notification of the time
     * advance grant has been received.
     */
    public void advanceTime(double timestep) throws RTIexception, InterruptedException {
        // request the advance
        federateAmbassador.isAdvancing = true;
        LogicalTime newTime = FederateExecution.convertTime(federateAmbassador.federateTime + timestep);
        rtiAmbassador.timeAdvanceRequest(newTime);
        // wait for the time advance to be granted. ticking will tell the
        // LRC to start delivering callbacks to the federate
        while (federateAmbassador.isAdvancing) {
            rtiAmbassador.tick();
        }
    }
}
