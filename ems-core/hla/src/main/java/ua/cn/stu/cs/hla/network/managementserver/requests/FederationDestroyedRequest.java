package ua.cn.stu.cs.hla.network.managementserver.requests;

/**
 * Send after federation has destroyed to remove federation from list of the
 * modeling server.
 *
 * @author Dmitry Krivenko <dmitrykrivenko@gmal.com>
 */
public class FederationDestroyedRequest extends AbstractMgmntSRequest {

    @Override
    public String toString() {
        return "FederationDestroyedRequest[federationName=" + federationName + "; federateName="
                + federateName + "; rootAggregateName=" + rootAggregateName
                + "; aggregateName=" + aggregateName + "]";
    }
}
