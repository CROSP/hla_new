package ua.cn.stu.cs.hla.examples.smo;

import hla.rti.RTIexception;
import ua.cn.stu.cs.hla.core.FederateExecution;
import ua.cn.stu.cs.hla.core.HlaModel;
import ua.cn.stu.cs.hla.examples.ExampleFom;

/**
 * Represents the generator of SMO.
 *
 * @author Dmitry Krivenko <dmitrykrivenko@gmal.com>
 */
public class FederateModelGenerator {

    public static void main(String[] args) {
        HlaModel modelGenerator = new ModelGenerator();
        FederateExecution federateExecution = new FederateExecution("FederationSmoExperiment", "FederateModelGenerator");
        federateExecution.setFomData(ExampleFom.createFomData());
        federateExecution.setWaitPressEnter(true);
        try {
            federateExecution.runModelFederate(modelGenerator);
        } catch (RTIexception ex) {
            System.out.println(ex);
        }
    }
}
