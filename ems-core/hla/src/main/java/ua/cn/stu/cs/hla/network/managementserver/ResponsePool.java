package ua.cn.stu.cs.hla.network.managementserver;

import java.util.HashSet;
import java.util.Set;
import ua.cn.stu.cs.ems.core.experiment.ExperimentReport;
import ua.cn.stu.cs.hla.network.modelingserver.responses.MdlnSResponse;

/**
 * Pool of responses, which send modeling servers to the management server.
 *
 * @author Dmitry Krivenko <dmitrykrivenko@gmal.com>
 */
public final class ResponsePool {

    private Set<MdlnSResponse> responsePoolList = new HashSet<MdlnSResponse>();
    private ExperimentReport report = new ExperimentReport();

    /**
     * Default constructor without parameters.
     */
    public ResponsePool() {
    }

    /**
     * Get the response pool list.
     *
     * @return the responsePoolList
     */
    public synchronized Set<MdlnSResponse> getResponsePoolList() {
        return responsePoolList;
    }

    /**
     * Set the response pool list.
     *
     * @param responsePoolList the responsePoolList to set
     */
    public synchronized void setResponsePoolList(Set<MdlnSResponse> responsePoolList) {
        this.responsePoolList = responsePoolList;
    }

    /**
     * Add new response to the response pool list.
     *
     * @param response
     */
    public synchronized void addResponse(MdlnSResponse response) {
        this.responsePoolList.add(response);
    }

    /**
     * Clear the response pool list.
     */
    public void clear() {
        if (responsePoolList != null && !responsePoolList.isEmpty()) {
            responsePoolList.clear();
        }
    }

    /**
     * @return the report
     */
    public ExperimentReport getReport() {
        return report;
    }
}
