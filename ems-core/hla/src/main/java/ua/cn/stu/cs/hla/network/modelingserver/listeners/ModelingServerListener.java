package ua.cn.stu.cs.hla.network.modelingserver.listeners;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import java.util.HashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.cn.stu.cs.ems.core.aggregates.AggregateInstance;
import ua.cn.stu.cs.hla.network.AggregateProvider;
import ua.cn.stu.cs.hla.network.managementserver.requests.FederationDestroyedRequest;
import ua.cn.stu.cs.hla.network.managementserver.requests.PrepareToRunRequest;
import ua.cn.stu.cs.hla.network.managementserver.requests.PrepareToStartRequest;
import ua.cn.stu.cs.hla.network.managementserver.requests.StartRequest;
import ua.cn.stu.cs.hla.network.modelingserver.responses.ReadyToRunResponse;

/**
 * Listener of all requests to the modeling server.
 *
 * @author Dmitry Krivenko <dmitrykrivenko@gmal.com>
 */
public class ModelingServerListener extends Listener {

    private static final Logger logger = LoggerFactory.getLogger(ModelingServerListener.class);
    private AggregateProvider aggregateProvider;
    //<FederationName<FederateName,ExperimentParamsStorage>>
    private HashMap<String, HashMap<String, ExperimentParamsStorage>> experimentParams = new HashMap<String, HashMap<String, ExperimentParamsStorage>>();
    //<FederationName<FederateName,ExperimentExecutionThread>>
    private HashMap<String, HashMap<String, ExperimentExecutionThread>> experimentExecThreads = new HashMap<String, HashMap<String, ExperimentExecutionThread>>();

    /**
     * Default constructor without parameters.
     */
    public ModelingServerListener() {
    }

    /**
     * Convenience constructor with parameters.
     *
     * @param aggProvider
     */
    public ModelingServerListener(AggregateProvider aggregateProvider) {
        this.aggregateProvider = aggregateProvider;
    }

    /**
     * Receive requests from management server.
     *
     * @param connection
     * @param object
     */
    @Override
    public void received(Connection connection, Object object) {
        if (object instanceof PrepareToRunRequest) {
            PrepareToRunRequest request = (PrepareToRunRequest) object;
            logger.debug("Receive request: " + request);
            processPrepareToRunRequest(request, connection);
        } else if (object instanceof PrepareToStartRequest) {
            PrepareToStartRequest request = (PrepareToStartRequest) object;
            logger.debug("Receive request: " + request);
            processPrepareToStartRequest(request, connection);
        } else if (object instanceof StartRequest) {
            StartRequest request = (StartRequest) object;
            logger.debug("Receive request: " + request);
            processStartRequest(request);
        } else if (object instanceof FederationDestroyedRequest) {
            FederationDestroyedRequest request = (FederationDestroyedRequest) object;
            logger.debug("Receive request: " + request);
            processFederationDestroyedRequest(request);
        }
    }

    /**
     * Set the aggregate provider.
     *
     * @param aggProvider the aggProvider to set
     */
    public void setAggProvider(AggregateProvider aggProvider) {
        this.aggregateProvider = aggProvider;
    }

    /**
     * Process the PrepareToRunRequest request.
     *
     * @param request
     * @param connection
     */
    private void processPrepareToRunRequest(PrepareToRunRequest request, Connection connection) {
        logger.debug("Process PrepareToRunRequest");
        AggregateInstance ai = aggregateProvider.getAggretate(request.rootAggregateName, request.aggregateName);
        ExperimentParamsStorage eps = new ExperimentParamsStorage(ai, request.rootAggregateName, request.finishTime, request.dumpPeriod, request.collectPrimaryStat, request.collectStatOnEvent, request.objectsForCollectStat);
        if (!experimentParams.isEmpty() && experimentParams.containsKey(request.federationName)) {
            experimentParams.get(request.federationName).put(request.federateName, eps);
        } else {
            HashMap<String, ExperimentParamsStorage> params = new HashMap<String, ExperimentParamsStorage>();
            params.put(request.federateName, eps);
            experimentParams.put(request.federationName, params);
        }
        logger.debug("(Federation: " + request.federationName + ") Size of experimentParams = " + experimentParams.get(request.federationName).size());
        //send responce to the management server            
        connection.sendTCP(new ReadyToRunResponse());
    }

    /**
     * Process the PrepareToStartRequest request.
     *
     * @param request
     */
    private void processPrepareToStartRequest(PrepareToStartRequest request, Connection connection) {
        logger.debug("Process PrepareToStartRequest");
        ExperimentParamsStorage eps = experimentParams.get(request.federationName).get(request.federateName);

        ExperimentExecutionThread expExec = new ExperimentExecutionThread(request.federationName, request.federateName, request.fomData, eps, connection);
        if (!experimentExecThreads.isEmpty() && experimentExecThreads.containsKey(request.federationName)) {
            experimentExecThreads.get(request.federationName).put(request.federateName, expExec);
        } else {
            HashMap<String, ExperimentExecutionThread> expExecThreads = new HashMap<String, ExperimentExecutionThread>();
            expExecThreads.put(request.federateName, expExec);
            experimentExecThreads.put(request.federationName, expExecThreads);
        }
        logger.debug("(Federation: " + request.federationName + ") Size of experimentExecThreads = " + experimentExecThreads.get(request.federationName).size());
        expExec.start();
    }

    /**
     * Process the StartRequest request.
     *
     * @param request
     */
    private void processStartRequest(StartRequest request) {
        logger.debug("Process StartRequest");
        ExperimentExecutionThread expExecThread = experimentExecThreads.get(request.federationName).get(request.federateName);
        expExecThread.setCanStart(true);
    }

    /**
     * Process the FederationDestroyedRequest request.
     *
     * @param request
     */
    private void processFederationDestroyedRequest(FederationDestroyedRequest request) {
        logger.debug("Process FederationDestroyedRequest");
        experimentParams.remove(request.federationName);
        experimentExecThreads.remove(request.federationName);
    }
}
