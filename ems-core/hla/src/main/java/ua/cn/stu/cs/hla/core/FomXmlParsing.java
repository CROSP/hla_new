package ua.cn.stu.cs.hla.core;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.apache.xerces.parsers.DOMParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

/**
 *
 * @author Dmitrij999
 */
public class FomXmlParsing {

    private static final Logger logger = LoggerFactory.getLogger(FomXmlParsing.class);
    public static final String fomFileName = "FOM.xml";

    private static Document getMainDocumet(String fomFileName) throws SAXException, IOException {
        DOMParser parser = new DOMParser();
        parser.parse(fomFileName);
        Document doc = parser.getDocument();
        return doc;
    }

    /**
     * @param fomFileName Name of fom file
     * @param elementName objectClass or interactionClass
     *
     * @return attributes names of element (pa, pb...)
     */
    public static List<String> getNamedElements(String fomFileName, String elementName) throws SAXException, IOException {
        Document doc = getMainDocumet(fomFileName);
        List<String> list = new LinkedList<String>();
        NodeList classElements = doc.getElementsByTagName(elementName);
        for (int i = 0; i < classElements.getLength(); i++) {
            NamedNodeMap attrs = classElements.item(i).getAttributes();
            for (int j = 0; j < attrs.getLength(); j++) {
                Node attr = attrs.item(j);
                if (attr.getNodeName().equals("name")) {
                    list.add(attr.getNodeValue());
                }
            }
        }
        list.remove(0);
        return list;
    }

    /**
     * @param fomFileName Name of fom file
     * @param elementName (element name) objectClass or interactionClass
     * @param classHandleName name of element
     *
     * @return attributes by name of class or interaction
     */
    public static List<String> getAttributes(String fomFileName, String elementName, String classHandleName) throws SAXException, IOException {
        Document doc = getMainDocumet(fomFileName);
        List<String> list = new LinkedList<String>();
        boolean ok = false;
        NodeList nodeList = doc.getElementsByTagName("*");

        for (int i = 0; i < nodeList.getLength(); i++) {
            Element elementNode = (Element) nodeList.item(i);
            NamedNodeMap attrs = elementNode.getAttributes();
            boolean isNodeElement = elementNode.getTagName().equals(elementName);
            boolean isNodeAttr = false;
            if (elementNode.getTagName().equals("attribute") || elementNode.getTagName().equals("parameter")) {
                isNodeAttr = true;
            }

            if (ok && isNodeAttr) {
                for (int j = 0; j < attrs.getLength(); j++) {
                    Node attr = attrs.item(j);
                    if (attr.getNodeName().equals("name")) {
                        list.add(attr.getNodeValue());
                        break;
                    }
                }
            } else {
                ok = false;
            }

            if (!isNodeAttr && isNodeElement) {
                for (int j = 0; j < attrs.getLength(); j++) {
                    Node attr = attrs.item(j);
                    if (attr.getNodeValue().equals(classHandleName)) {
                        ok = true;
                        logger.debug("Element " + elementNode.getTagName() + " " + classHandleName);
                        break;
                    }
                }
            }

        }
        return list;
    }

    /**
     * Create file by map - Class Name, attr count, path name.
     *
     * @param classData names of classes and class attrs that will be created
     * fom xml
     * @param interactionData names of interaction and interaction params that
     * will be created in fom xml
     * @param path path name in which fom file will be created
     *
     * @throws ParserConfigurationException
     * @throws FileNotFoundException
     * @throws TransformerException
     */
    public static void createFomFile(List<XmlInfo> classData, List<XmlInfo> interactionData, String path) throws ParserConfigurationException,
            FileNotFoundException, TransformerException {
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

        // root elements
        Document doc = docBuilder.newDocument();
        Element rootElement = doc.createElement("objectModel");
        doc.appendChild(rootElement);

        Attr attr = doc.createAttribute("type");
        attr.setValue("FOM");
        rootElement.setAttributeNode(attr);
        rootElement.setAttribute("DTDversion", "1516.2");

        // for objects
        Element objects = doc.createElement("objects");
        rootElement.appendChild(objects);

        Element rootObjectClass = doc.createElement("objectClass");
        rootObjectClass.setAttribute("name", "HLAobjectRoot");
        rootObjectClass.setAttribute("sharing", "Neither");
        objects.appendChild(rootObjectClass);

        Element rootAttr = doc.createElement("attribute");
        rootObjectClass.appendChild(rootAttr);
        rootAttr.setAttribute("name", "HLAprivilegeToDeleteObject");
        rootAttr.setAttribute("transportation", "HLAreliable");
        rootAttr.setAttribute("order", "TimeStamp");

        if (classData != null) {
            for (XmlInfo xmlClassInfo : classData) {
                Element classElement = doc.createElement("objectClass");
                rootObjectClass.appendChild(classElement);
                classElement.setAttribute("name", xmlClassInfo.getName());
                classElement.setAttribute("semantics", "NA");
                classElement.setAttribute("sharing", "PublishSubscribe");
                int attrCount = xmlClassInfo.getAttrCount();
                List<String> attrs = xmlClassInfo.getAttributes();
                for (int i = 0; i < attrCount; i++) {
                    Element attrElement = doc.createElement("attribute");
                    classElement.appendChild(attrElement);
                    attrElement.setAttribute("name", attrs.get(i));
                    attrElement.setAttribute("updateType", "Conditional");
                    attrElement.setAttribute("transportation", "HLAreliable");
                    attrElement.setAttribute("order", "TimeStamp");
                }
            }
        }

        // for interactions
        Element interactions = doc.createElement("interactions");
        rootElement.appendChild(interactions);

        Element rootInteractionClass = doc.createElement("interactionClass");
        interactions.appendChild(rootInteractionClass);
        rootInteractionClass.setAttribute("dimensions", "NA");
        rootInteractionClass.setAttribute("sharing", "PublishSubscribe");
        rootInteractionClass.setAttribute("name", "HLAinteractionRoot");
        rootInteractionClass.setAttribute("transportation", "HLAreliable");
        rootInteractionClass.setAttribute("order", "Receive");

        if (interactionData != null) {
            for (XmlInfo xmlInteractionInfo : interactionData) {
                Element interactionElement = doc.createElement("interactionClass");
                rootInteractionClass.appendChild(interactionElement);
                interactionElement.setAttribute("dimensions", "NA");
                interactionElement.setAttribute("semantics", "NA");
                interactionElement.setAttribute("sharing", "PublishSubscribe");
                interactionElement.setAttribute("name", xmlInteractionInfo.getName());
                interactionElement.setAttribute("transportation", "HLAreliable");
                interactionElement.setAttribute("order", "TimeStamp");
                int attrCount = xmlInteractionInfo.getAttrCount();
                List<String> attrs = xmlInteractionInfo.getAttributes();
                for (int i = 0; i < attrCount; i++) {
                    Element attrElement = doc.createElement("parameter");
                    interactionElement.appendChild(attrElement);
                    attrElement.setAttribute("name", attrs.get(i));
                    attrElement.setAttribute("semantics", "NA");
                }
            }
        }

        DOMSource source = new DOMSource(doc);

        // write the content into xml file
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        File cacheFile = new File(path);
        FileOutputStream outStream = new FileOutputStream(cacheFile);
        Result streamResult = new StreamResult(outStream);

        transformer.transform(source, streamResult);
    }

    public static void removeFomFile(String filePath) {
        // A File object to represent the filename
        File f = new File(filePath);

        // Make sure the file or directory exists and isn't write protected
        if (!f.exists()) {
            throw new IllegalArgumentException(
                    "Delete: no such file or directory: " + filePath);
        }
        if (!f.canWrite()) {
            throw new IllegalArgumentException("Delete: write protected: "
                    + filePath);
        }

        // If it is a directory, make sure it is empty
        if (f.isDirectory()) {
            String[] files = f.list();
            if (files.length > 0) {
                throw new IllegalArgumentException(
                        "Delete: directory not empty: " + filePath);
            }
        }

        // Attempt to delete it
        boolean success = f.delete();

        if (!success) {
            throw new IllegalArgumentException("Delete: deletion failed");
        }
    }
}
