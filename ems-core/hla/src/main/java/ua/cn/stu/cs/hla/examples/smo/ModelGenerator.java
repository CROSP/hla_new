package ua.cn.stu.cs.hla.examples.smo;

import java.util.ArrayList;
import java.util.List;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.aggregates.AggregateInstance;
import ua.cn.stu.cs.ems.core.aggregates.Input;
import ua.cn.stu.cs.ems.core.aggregates.Output;
import ua.cn.stu.cs.ems.core.aggregates.OutputImpl;
import ua.cn.stu.cs.ems.core.transitions.FTransition;
import ua.cn.stu.cs.ems.core.transitions.TTransition;
import ua.cn.stu.cs.ems.core.transitions.functions.ConstDelayFunction;
import ua.cn.stu.cs.ems.core.transitions.functions.InterpretedDelayFunction;
import ua.cn.stu.cs.ems.ecli.Value;
import ua.cn.stu.cs.hla.cm.CommunicateModule;
import ua.cn.stu.cs.hla.core.AbstractHlaModel;
import ua.cn.stu.cs.hla.core.FederateExecution;
import ua.cn.stu.cs.hla.core.HlaConstants;
import ua.cn.stu.cs.hla.core.HlaENetworksModel;

/**
 *
 * @author Dmitry Krivenko <dmitrykrivenko@gmal.com>
 */
public class ModelGenerator extends AbstractHlaModel {

    public void createModel(FederateExecution federateExecution) {
        AggregateInstance aggr = createModelGenerator();

        List<String> outputs = new ArrayList<String>();
        outputs.add("O0");

        AggregateInstance cm = CommunicateModule.getCommunicateModule(federateExecution, null, outputs);
        CommunicateModule.setFomElementToHlaOutput(cm, HlaConstants.HLA_OUTPUT_NAME_BEGIN + "O0", "Model1");

        AggregateInstance rootAggregate = createRootAgregate(cm, aggr);

        HlaENetworksModel hlaEModel = new HlaENetworksModel(federateExecution.getFederateAmbassador(), federateExecution.getRtiAmbassador());
        rootAggregate.setModel(hlaEModel);
        hlaEModel.setRootAggregate(rootAggregate);

        hlaEModel.setFinishTime(1000L);
        setModel(hlaEModel);
    }

    public void startModel() {
        getModel().start();
    }

    private static AggregateInstance createModelGenerator() {
        AggregateInstance aggr = createAggregateInstance("ModelGenerator");

        TTransition t0 = new TTransition("T0");

        FTransition f0 = new FTransition("F0");

        Output o0 = new OutputImpl("O0");

        Place p0 = new Place("P0");
        Place p1 = new Place("P1");
        Place p2 = new Place("P2");

        f0.setInputPlace(p0);
        f0.addOutputPlace(p2, 0);
        f0.addOutputPlace(p1, 1);

        t0.setInputPlace(p1);
        t0.setOutputPlace(p0);
        t0.setDelayFunction(new ConstDelayFunction(5));
//        t0.setDelayFunction(new InterpretedDelayFunction("RETURN EXPONENTIAL(12.5); "));

        o0.connectInputPlace(p2);

        Token token = new Token();
        Value longValue = new Value(0L);
        token.setValue("LongAttr", longValue);

        p0.setToken(token);

        aggr.addPlaces(p0, p1, p2);
        aggr.addTransitions(f0, t0);
        aggr.addOutput(o0, 0);

        return aggr;
    }

    private static AggregateInstance createRootAgregate(AggregateInstance cm, AggregateInstance aggr) {
        AggregateInstance rootAggregate = createAggregateInstance("RootAggregate");

        rootAggregate.addChildAggregate(aggr);
        rootAggregate.addChildAggregate(cm);

        Output output = aggr.getOutput("O0");
        Input input = cm.getInput(HlaConstants.HLA_OUTPUT_NAME_BEGIN + "O0");

        output.addInput(input, 0);

        return rootAggregate;
    }

    private static AggregateInstance createAggregateInstance(String name) {
        return new AggregateInstance(name);
    }
}
