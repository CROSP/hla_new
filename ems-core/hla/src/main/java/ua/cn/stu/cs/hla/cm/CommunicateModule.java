package ua.cn.stu.cs.hla.cm;

import java.util.List;
import java.util.Set;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.aggregates.*;
import ua.cn.stu.cs.ems.core.transitions.TTransition;
import ua.cn.stu.cs.hla.core.FederateExecution;
import ua.cn.stu.cs.hla.core.HLAInput;
import ua.cn.stu.cs.hla.core.HLAOutput;
import ua.cn.stu.cs.hla.core.HlaConstants;

/**
 *
 * @author Dmitrij999
 */
public class CommunicateModule {

    /**
     * @param federateName name of federate, in hlaIn or hlaOut we use it to get
     * right federate
     * @param fomElement name of fom xml element, in hlaIn or hlaOut we use it
     * to publish or subscribe to right data
     * @param hlaInputs hlaInput`s names, received from aggregate, use to create
     * right amount of hla inputs with names, like in aggregate
     * @param hlaOutputs hlaOutput`s names, received from aggregate, use to
     * create right amount of hla outputs with names, like in aggregate
     *
     * @return communicate model with given numbers of hla inputs or/and outputs
     */
    public static AggregateInstance getCommunicateModule(FederateExecution federateExecution, List<String> hlaInputs, List<String> hlaOutputs) {
        AggregateDefinition cmDef = new AggregateDefinition(HlaConstants.CM_DEF_NAME);
        AggregateInstance cm = cmDef.createInstance(HlaConstants.CM_NAME);

        setHlaInputs(cm, federateExecution, hlaInputs);
        setHlaOutputs(cm, federateExecution, hlaOutputs);

        return cm;
    }

    private static void setHlaInputs(AggregateInstance cm, FederateExecution federateExecution, List<String> hlaInputs) {
        if (hlaInputs != null) {
            for (int i = 0; i < hlaInputs.size(); i++) {
                Place pHlaIn = new Place(String.format(HlaConstants.HLA_IN_PLACE_NAME_BEGIN + "%d", i));
                Place pModelOut = new Place(String.format(HlaConstants.HLA_OUT_TO_MODEL_PLACE_NAME_BEGIN + "%d", i));

                Input hlaInput = new HLAInput(HlaConstants.HLA_INPUT_NAME_BEGIN + hlaInputs.get(i), federateExecution);
                hlaInput.setOutputPlace(pHlaIn);

                Output outToModel = new OutputImpl(HlaConstants.HLA_OUTPUT_TO_MODEL_NAME_BEGIN + hlaInputs.get(i));
                outToModel.connectInputPlace(pModelOut);

                TTransition tTransition = new TTransition(String.format(HlaConstants.HLA_T_NAME_BEGIN + "%d", i));
                tTransition.setInputPlace(pHlaIn);
                tTransition.setOutputPlace(pModelOut);

                cm.addPlaces(pHlaIn, pModelOut);
                cm.addTransition(tTransition);
                cm.addInput(hlaInput, 0);
                cm.addOutput(outToModel, 0);
            }
        }
    }

    // we should set fom elemName to each HlaOut or In!!!!! 
    private static void setHlaOutputs(AggregateInstance cm, FederateExecution federateExecution, List<String> hlaOutputs) {
        if (hlaOutputs != null) {
            for (int i = 0; i < hlaOutputs.size(); i++) {
                Place pHlaOut = new Place(String.format(HlaConstants.HLA_OUT_PLACE_NAME_BEGIN + "%d", i));

                Input hlaOuput = new HLAOutput(HlaConstants.HLA_OUTPUT_NAME_BEGIN + hlaOutputs.get(i), federateExecution, null);
                hlaOuput.setOutputPlace(pHlaOut);

                cm.addPlace(pHlaOut);
                cm.addInput(hlaOuput, 0);
            }
        }
    }

    /**
     * @param cm communicate model with our hlaOutputs
     * @param hlaOutputName name of hlaOutput
     * @param userTokenAttributes names of token attributes that will be sent
     * from current hlaOutput, null if all
     */
    public static void setUserAttributesToHlaOutput(AggregateInstance cm, String hlaOutputName, Set<String> userTokenAttributes) {
        HLAOutput hlaOutput = (HLAOutput) cm.getInput(hlaOutputName);
        hlaOutput.setUserTokenAttributes(userTokenAttributes);
    }

    /**
     * @param cm communicate model with hlaInputs
     * @param hlaInputName name of hlaInput
     * @param fomElementName name of fom element, this element will be used in
     * current hlaInput
     */
    public static void setFomElementToHlaInput(AggregateInstance cm, String hlaInputName, String fomElementName, String subscribeOutput) {
        HLAInput hlaInput = (HLAInput) cm.getInput(hlaInputName);
        hlaInput.setFomElement(fomElementName, subscribeOutput);
    }

    /**
     * @param cm communicate model with hlaOutputs
     * @param hlaOutputName name of hlaOutput
     * @param fomElementName name of fom element, this element will be used in
     * current hlaOuput
     */
    public static void setFomElementToHlaOutput(AggregateInstance cm, String hlaOutputName, String fomElementName) {
        HLAOutput hlaOutput = (HLAOutput) cm.getInput(hlaOutputName);
        hlaOutput.setFomElement(fomElementName);
    }
}
