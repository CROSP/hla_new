package ua.cn.stu.cs.hla.network;

import ua.cn.stu.cs.ems.core.aggregates.AggregateInstance;

/**
 * Aggregate provider. May has different realizations (for example - getting
 * aggregate from a database).
 *
 * @author Dmitry Krivenko <dmitrykrivenko@gmal.com>
 */
public interface AggregateProvider {

    /**
     * Get part of the model (of the root aggregate).
     *
     * @param modelName name of the model
     * @param aggName - name of the aggregate type
     * @return the aggregate
     */
    public AggregateInstance getAggretate(String modelName, String aggName);
}
