package ua.cn.stu.cs.hla.examples.smo;

import java.util.ArrayList;
import java.util.List;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.aggregates.AggregateInstance;
import ua.cn.stu.cs.ems.core.aggregates.Input;
import ua.cn.stu.cs.ems.core.aggregates.InputImpl;
import ua.cn.stu.cs.ems.core.aggregates.Output;
import ua.cn.stu.cs.ems.core.experiment.CountDownExperiment;
import ua.cn.stu.cs.ems.core.experiment.ExperimentReport;
import ua.cn.stu.cs.ems.core.experiment.PrimaryStatisticsElement;
import ua.cn.stu.cs.ems.core.experiment.PrimaryStatisticsResult;
import ua.cn.stu.cs.ems.core.experiment.statistics.StatisticsParameters;
import ua.cn.stu.cs.ems.core.experiment.statistics.StatisticsResult;
import ua.cn.stu.cs.ems.core.queues.FIFOQueue;
import ua.cn.stu.cs.ems.core.transitions.JTransition;
import ua.cn.stu.cs.ems.core.transitions.TTransition;
import ua.cn.stu.cs.ems.core.transitions.functions.ConstDelayFunction;
import ua.cn.stu.cs.ems.core.transitions.functions.InterpretedDelayFunction;
import ua.cn.stu.cs.ems.ecli.Value;
import ua.cn.stu.cs.hla.cm.CommunicateModule;
import ua.cn.stu.cs.hla.core.AbstractHlaModel;
import ua.cn.stu.cs.hla.core.FederateExecution;
import ua.cn.stu.cs.hla.core.HlaConstants;
import ua.cn.stu.cs.hla.core.HlaENetworksModel;

public class ModelReceiver extends AbstractHlaModel {

    public void startModel() {
        if (getExperiment() != null) {
            getExperiment().startExperiment();

            ExperimentReport er = getExperiment().getReport();

            List<PrimaryStatisticsResult> srs = er.getPrimaryStatisticsFor("RootAggregate.ModelFifo.QF0");

            for (PrimaryStatisticsResult sr : srs) {

                for (PrimaryStatisticsElement se : sr.getDumpedPrimaryStatisticsElements()) {
                    StatisticsResult result = se.getStatisticsResult();
                    log("Time: " + se.getTime() + "; Average queue length:"
                            + result.getResultValue(StatisticsParameters.AVERAGE_QUEUE_LENGTH)
                            + "; Average time in queue: " + result.getResultValue(StatisticsParameters.AVERAGE_TIME_IN_QUEUE)
                            + "; Number of passed tokens: " + result.getResultValue(StatisticsParameters.NUMBER_OF_PASSED_TOKENS));
                }
            }
        }
    }

    public void createModel(FederateExecution federateExecution) {
        AggregateInstance aggr = createModelFifo();

        List<String> inputs = new ArrayList<String>();
        inputs.add("I0");

        AggregateInstance cm = CommunicateModule.getCommunicateModule(federateExecution, inputs, null);
        CommunicateModule.setFomElementToHlaInput(cm, HlaConstants.HLA_INPUT_NAME_BEGIN+"I0", "Model1", "O0");

        AggregateInstance rootAgregate = createRootAgregate(cm, aggr);

        HlaENetworksModel model = new HlaENetworksModel(federateExecution.getFederateAmbassador(), federateExecution.getRtiAmbassador());
        rootAgregate.setModel(model);
        model.setRootAggregate(rootAgregate);

        model.setFinishTime(1000L);

        setModel(model);

        CountDownExperiment psc = new CountDownExperiment(getModel(), 1);
        psc.setDelayBeforeCollectingPrimaryStatistics(0);
        psc.setIsPrimaryStatisticsShouldBeCollected(true);

        psc.collectStatisticsOn(aggr.getQueue("QF0"));
        setExperiment(psc);
    }

    private AggregateInstance createModelFifo() {
        AggregateInstance aggr = createAggregateInstance("ModelFifo");

        TTransition t0 = new TTransition("T0");

        JTransition j0 = new JTransition("J0");

        FIFOQueue qf0 = new FIFOQueue("QF0");

        Input i0 = new InputImpl("I0");

        Place p0 = new Place("P0");
        Place p1 = new Place("P1");
        Place p2 = new Place("P2");
        Place p3 = new Place("P3");

        i0.setOutputPlace(p0);

        qf0.connectInputPlace(p0);
        qf0.connectOutputPlace(p1);

        j0.addInputPlace(p1, 0);
        j0.addInputPlace(p2, 1);
        j0.setOutputPlace(p3);

        j0.setDelayFunction(new ConstDelayFunction(10));
//        j0.setDelayFunction(new InterpretedDelayFunction("RETURN EXPONENTIAL(10); "));

        t0.setInputPlace(p3);
        t0.setOutputPlace(p2);

        Token token = new Token();
        Value longValue = new Value(0);
        token.setValue("LongAttr", longValue);
        p3.setToken(token);

        aggr.addPlaces(p0, p1, p2, p3);
        aggr.addTransitions(t0, j0);
        aggr.addQueue(qf0);
        aggr.addInput(i0, 0);

        return aggr;
    }

    private AggregateInstance createRootAgregate(AggregateInstance cm, AggregateInstance aggr) {
        AggregateInstance rootAggregate = createAggregateInstance("RootAggregate");

        rootAggregate.addChildAggregate(aggr);
        rootAggregate.addChildAggregate(cm);

        Input input0 = aggr.getInput("I0");
        Output cmOutput1 = cm.getOutput(HlaConstants.HLA_OUTPUT_TO_MODEL_NAME_BEGIN+"I0");
        cmOutput1.addInput(input0, 0);

        return rootAggregate;
    }

    private void log(Object o) {
        System.out.println(o);
    }

    private static AggregateInstance createAggregateInstance(String name) {
        return new AggregateInstance(name);
    }
}
