package ua.cn.stu.cs.hla.network.managementserver.listeners;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.cn.stu.cs.ems.core.experiment.ExperimentReport;
import ua.cn.stu.cs.ems.core.experiment.PrimaryStatisticsResult;
import ua.cn.stu.cs.hla.network.managementserver.ResponsePool;
import ua.cn.stu.cs.hla.network.modelingserver.responses.ReadyToRunResponse;
import ua.cn.stu.cs.hla.network.modelingserver.responses.ReadyToStartResponse;
import ua.cn.stu.cs.hla.network.modelingserver.responses.ReportResponse;

/**
 * Listener of all responses, that come from modeling servers.
 *
 * @author Dmitry Krivenko <dmitrykrivenko@gmal.com>
 */
public class ManagementServerListener extends Listener {

    private final static Logger logger = LoggerFactory.getLogger(ManagementServerListener.class);
    private final ResponsePool responsePool;

    /**
     * Convenience constructor with parameters.
     *
     * @param responsePool response pool
     */
    public ManagementServerListener(ResponsePool responsePool) {
        this.responsePool = responsePool;
    }

    /**
     * Receive responses from modeling servers.
     *
     * @param connection connection
     * @param object response that come from one of modeling servers
     */
    @Override
    public void received(Connection connection, Object object) {
        if (object instanceof ReadyToRunResponse) {
            ReadyToRunResponse response = (ReadyToRunResponse) object;
            logger.debug("Receive " + response);
            responsePool.addResponse(response);
        } else if (object instanceof ReadyToStartResponse) {
            ReadyToStartResponse response = (ReadyToStartResponse) object;
            logger.debug("Receive " + response);
            responsePool.addResponse(response);
        } else if (object instanceof ReportResponse) {
            ReportResponse response = (ReportResponse) object;
            logger.debug("Receive " + response);
            if (response.last) {
                responsePool.addResponse(response);
            } else {
                if (responsePool.getReport().getPrimaryStatisticsFor(response.objectName) == null) {
                    responsePool.getReport().addPrimaryStatisticsObject(response.objectName);
                }
                for (PrimaryStatisticsResult psr : response.report) {
                    responsePool.getReport().addPrimaryStatisticsResult(response.objectName, psr);
                }
            }
        }
    }
}
