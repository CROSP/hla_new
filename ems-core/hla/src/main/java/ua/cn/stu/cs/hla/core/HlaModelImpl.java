package ua.cn.stu.cs.hla.core;

import java.util.ArrayList;
import java.util.List;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.aggregates.*;
import ua.cn.stu.cs.ems.core.experiment.CountDownExperiment;
import ua.cn.stu.cs.ems.core.queues.Queue;
import ua.cn.stu.cs.ems.core.transitions.Transition;
import ua.cn.stu.cs.ems.core.utils.JessUtils;
import ua.cn.stu.cs.hla.cm.CommunicateModule;
import ua.cn.stu.cs.hla.network.modelingserver.listeners.ExperimentParamsStorage;

/**
 * Implementation of the {@link AbstractHlaModel}.
 *
 * @author Dmitry Krivenko <dmitrykrivenko@gmal.com>
 */
public class HlaModelImpl extends AbstractHlaModel {

    private ExperimentParamsStorage eps;

    /**
     * Default constructor without parameters.
     */
    public HlaModelImpl() {
    }

    /**
     * Convenience constructor with parameters.
     *
     * @param eps experiment parameters storage
     */
    public HlaModelImpl(ExperimentParamsStorage eps) {
        this.eps = eps;
    }

    @Override
    public void createModel(FederateExecution federateExecution) {
        AggregateInstance ai = eps.getAggregate();
        //Inputs
        List<String> inputs = new ArrayList<String>();
        for (Input input : ai.getInputs()) {
            if (input.getConnectedOutput() != null) {
                inputs.add(input.getName());
            }
        }
        //Outputs
        List<String> outputs = new ArrayList<String>();
        for (Output output : ai.getOutputs()) {
            if (!output.getConnectedInputs().isEmpty()) {
                outputs.add(output.getName());
            }
        }
        String aggNameForOutputs = null;
        if (!outputs.isEmpty()) {
            aggNameForOutputs = ai.getName();
        }
        //create cm
        AggregateInstance cm = CommunicateModule.getCommunicateModule(federateExecution, inputs, outputs);
        for (Input input : cm.getInputs()) {
            if (input instanceof HLAInput) {
                //set fom element for inputs
                int inputIndx = input.getName().indexOf(HlaConstants.HLA_INPUT_NAME_BEGIN);
                String inputName = input.getName().substring(inputIndx + HlaConstants.HLA_INPUT_NAME_BEGIN.length());
                SubscribeAggregateAndOutput subscribeAggAndOut = getAggregateNameForInput(ai, inputName);
                CommunicateModule.setFomElementToHlaInput(cm, input.getName(), subscribeAggAndOut.getAggregateName(), subscribeAggAndOut.getOutputName());
            } else {
                //set fom element for outputs
                CommunicateModule.setFomElementToHlaOutput(cm, input.getName(), aggNameForOutputs);
            }
        }
        //create root aggregate
        AggregateInstance rootAgregate = createRootAgregate(cm, ai);
        //create model
        HlaENetworksModel model = new HlaENetworksModel(federateExecution.getFederateAmbassador(), federateExecution.getRtiAmbassador());
        rootAgregate.setModel(model);
        model.setRootAggregate(rootAgregate);

        model.setFinishTime(eps.getFinishTime());

        setModel(model);
        //create experiment
        if (eps.isCollectPrimaryStat()) {
            CountDownExperiment psc = new CountDownExperiment(getModel(), 1);
            psc.setDelayBeforeCollectingPrimaryStatistics(0);
            psc.setIsPrimaryStatisticsShouldBeCollected(true);
            //set objects for collecting statistic
            if (eps.getObjectsForCollectStat() != null) {
                for (String object : eps.getObjectsForCollectStat()) {
                    int aggIndx = object.indexOf(ai.getName() + ".");
                    if (aggIndx >= 0) {
                        String fullPath = object.substring(aggIndx + ai.getName().length() + 1);
                        AggregateChild aggChild = JessUtils.getAggregateChild(ai, fullPath);
                        if (aggChild != null) {
                            if (aggChild instanceof Place) {
                                Place place = (Place) aggChild;
                                psc.collectStatisticsOn(place);
                            }
                            if (aggChild instanceof Transition) {
                                Transition transition = (Transition) aggChild;
                                psc.collectStatisticsOn(transition);
                            }
                            if (aggChild instanceof Queue) {
                                Queue queue = (Queue) aggChild;
                                psc.collectStatisticsOn(queue);
                            }
                        }
                    }
                }
            }
            if (eps.isCollectStatOnEvent()) {
                psc.setCollectedStatOnEvent(true);
            } else if (eps.getDumpPeriod() > 0) {
                psc.setPrimaryStatisticsDumpPeriod(eps.getDumpPeriod());
            }
            setExperiment(psc);
        }
    }

    @Override
    public void startModel() {
        if (getExperiment() != null) {
            getExperiment().startExperiment();
            setReport(getExperiment().getReport());
        }
    }

    /**
     * Create root aggregate and connect CM-module to model.
     *
     * @param cm CM-module
     * @param aggr model
     * @return root aggregate
     */
    private AggregateInstance createRootAgregate(AggregateInstance cm, AggregateInstance aggr) {
        AggregateInstance rootAggregate = createAggregateInstance(eps.getRootAggregateName());

        rootAggregate.addChildAggregate(aggr);
        rootAggregate.addChildAggregate(cm);

        if (!cm.getOutputs().isEmpty()) {
            for (Output output : cm.getOutputs()) {
                int outputIndx = output.getName().indexOf(HlaConstants.HLA_OUTPUT_TO_MODEL_NAME_BEGIN);
                String modelInputName = output.getName().substring(outputIndx + HlaConstants.HLA_OUTPUT_TO_MODEL_NAME_BEGIN.length());
                Input aggInput = aggr.getInput(modelInputName);
                output.addInput(aggInput, 0);
            }
        }
        if (!cm.getInputs().isEmpty()) {
            for (Input input : cm.getInputs()) {
                if (input instanceof HLAOutput) {
                    int outputIndx = input.getName().indexOf(HlaConstants.HLA_OUTPUT_NAME_BEGIN);
                    String modelOutputName = input.getName().substring(outputIndx + HlaConstants.HLA_OUTPUT_NAME_BEGIN.length());
                    Output output = aggr.getOutput(modelOutputName);
                    output.addInput(input, 0);
                }
            }
        }
        return rootAggregate;
    }

    /**
     * Create new aggregate instance for modeling.
     *
     * @param name name of aggregate instance
     * @return aggregate instance
     */
    private AggregateInstance createAggregateInstance(String name) {
        AggregateDefinition ad = new AggregateDefinition(name);
        return new AggregateInstance(name, ad);
    }

    private SubscribeAggregateAndOutput getAggregateNameForInput(AggregateInstance agg, String inName) {
        for (Input input : agg.getInputs()) {
            if (input.getName().equals(inName)) {
                return new SubscribeAggregateAndOutput(input.getConnectedOutput().
                        getParentAggregate().getName(), input.getConnectedOutput().getName());
            }
        }
        return null;
    }

    private class SubscribeAggregateAndOutput {

        private String aggregateName;
        private String outputName;

        public SubscribeAggregateAndOutput(String aggregateName, String outputName) {
            this.aggregateName = aggregateName;
            this.outputName = outputName;
        }

        /**
         * @return the aggregateName
         */
        public String getAggregateName() {
            return aggregateName;
        }

        /**
         * @return the outputName
         */
        public String getOutputName() {
            return outputName;
        }
    }
}
