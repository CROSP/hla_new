package ua.cn.stu.cs.hla.network.managementserver.requests;

/**
 * Request that central server {@link ManagementServer} send to start the
 * modeling.
 *
 * @author Dmitry Krivenko <dmitrykrivenko@gmal.com>
 */
public class StartRequest extends AbstractMgmntSRequest {

    @Override
    public String toString() {
        return "StartRequest[federationName=" + federationName + "; federateName="
                + federateName + "; rootAggregateName=" + rootAggregateName
                + "; aggregateName=" + aggregateName + "]";
    }
}
