package ua.cn.stu.cs.hla.network.managementserver.requests;

/**
 * Request, that contains base fields for all requests. All requests should
 * extend this request.
 *
 * @author Dmitry Krivenko <dmitrykrivenko@gmal.com>
 */
public class AbstractMgmntSRequest implements MgmntSRequest {

    public String federationName;
    public String federateName;
    public String rootAggregateName;
    public String aggregateName;

    public void initRequest(String federationName, String federateName, String rootAggregateName, String aggregateName) {
        this.federationName = federationName;
        this.federateName = federateName;
        this.rootAggregateName = rootAggregateName;
        this.aggregateName = aggregateName;
    }
}
