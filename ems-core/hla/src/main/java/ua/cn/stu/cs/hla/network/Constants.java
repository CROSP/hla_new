package ua.cn.stu.cs.hla.network;

/**
 * Represents constants for network.
 *
 * @author Dmitry Krivenko <dmitrykrivenko@gmal.com>
 */
public class Constants {

    public static int DEFAULT_SERVER_UDP_PORT = 54777;
    public static int DEFAULT_SERVER_TCP_PORT = 54555;
}
