package ua.cn.stu.cs.hla.core;

import java.io.Serializable;
import java.util.Comparator;
import java.util.PriorityQueue;

/**
 * This class represents time ordered queue of tokens.
 *
 * @author Dmitry Krivenko <dmitrykrivenko@gmal.com>
 */
public class TOTokenQueue {

    final static int INITIAL_CAPACITY = 5;
    private PriorityQueue<TimeTokenPair> toQueue =
            new PriorityQueue<TimeTokenPair>(INITIAL_CAPACITY,
            new TimePairComparator());

    /**
     * Add token to the time ordered queue.
     *
     * @param token
     * @param time
     */
    public void addTOToken(TOToken token, double time) {
        TimeTokenPair ttp = new TimeTokenPair(time, token);
        toQueue.add(ttp);
    }

    /**
     * Get time of the first time token from time ordered queues.
     *
     * @return time of the first time token from time ordered queues
     */
    public double getFirstTOTokenTime() {
        TimeTokenPair first = toQueue.peek();

        return first.time;
    }

    /**
     * Gest first token from time ordered queue.
     *
     * @return first token from time ordered queue
     */
    public TOToken getFirstTOToken() {
        if (isEmpty()) {
            return null;
        }

        TimeTokenPair first = toQueue.poll();
        return first.token;
    }

    /**
     * Get size of time ordered queue.
     *
     * @return size of time ordered queue
     */
    public int getSize() {
        return toQueue.size();
    }

    /**
     * Check if time ordered queue is empty.
     *
     * @return true if it is empty, otherwise - false
     */
    public boolean isEmpty() {
        return getSize() == 0;
    }

    /**
     * Clear time ordered queue.
     */
    void clear() {
        toQueue.clear();
    }
}

/**
 * Class that represents time ordered token in the time ordered queue.
 *
 * @author Dmitry Krivenko <dmitrykrivenko@gmal.com>
 */
class TimeTokenPair {

    double time;
    TOToken token;

    public TimeTokenPair(double time, TOToken token) {
        this.time = time;
        this.token = token;
    }
}

/**
 * Comparator for time ordered tokens.
 *
 * @author Dmitry Krivenko <dmitrykrivenko@gmal.com>
 */
class TimePairComparator implements Comparator<TimeTokenPair>, Serializable {

    public int compare(TimeTokenPair t, TimeTokenPair t1) {
        double sub = t.time - t1.time;

        if (sub > 0) {
            return 1;
        } else if (sub < 0) {
            return -1;
        } else {
            return 0;
        }
    }
}
