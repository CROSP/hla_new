package ua.cn.stu.cs.hla.network.modelingserver.responses;

import java.util.List;
import ua.cn.stu.cs.ems.core.experiment.PrimaryStatisticsResult;

/**
 *
 * @author Dmitry Krivenko <dmitrykrivenko@gmal.com>
 */
public class ReportResponse implements MdlnSResponse {

    public boolean last;
    public String objectName;
    public List<PrimaryStatisticsResult> report;
}
