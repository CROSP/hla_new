package ua.cn.stu.cs.hla.examples.network;


import hla.rti.ConcurrentAccessAttempted;
import hla.rti.FederatesCurrentlyJoined;
import hla.rti.FederationExecutionDoesNotExist;
import hla.rti.RTIinternalError;
import org.portico.utils.classpath.Classpath;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.aggregates.Aggregate;
import ua.cn.stu.cs.ems.core.aggregates.AggregateDefinition;
import ua.cn.stu.cs.ems.core.experiment.ExperimentReport;
import ua.cn.stu.cs.ems.core.experiment.PrimaryStatisticsElement;
import ua.cn.stu.cs.ems.core.experiment.PrimaryStatisticsResult;
import ua.cn.stu.cs.ems.core.experiment.statistics.StatisticsResult;
import ua.cn.stu.cs.hla.core.FederationExecution;
import ua.cn.stu.cs.hla.core.FomData;
import ua.cn.stu.cs.hla.core.FomXmlParsing;
import ua.cn.stu.cs.hla.core.XmlInfo;
import ua.cn.stu.cs.hla.core.utils.HlaUtils;
import ua.cn.stu.cs.hla.network.managementserver.ManagementServer;
import ua.cn.stu.cs.hla.network.managementserver.ModelingServerClient;
import ua.cn.stu.cs.hla.network.managementserver.requests.FederationDestroyedRequest;
import ua.cn.stu.cs.hla.network.managementserver.requests.PrepareToRunRequest;
import ua.cn.stu.cs.hla.network.managementserver.requests.PrepareToStartRequest;
import ua.cn.stu.cs.hla.network.managementserver.requests.StartRequest;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.FileNotFoundException;
import java.net.InetAddress;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Example of the management server {@link ManagementServer}.
 *
 * @author Dmitry Krivenko <dmitrykrivenko@gmal.com>
 */
public class ExampleManagementServer {

    public static void main(String[] args) {
        //Logger.setLevel(Level.OFF);
        //Need add path to portico.jar file
        //On ui level path should be get from properties file(jess.properties)
        HlaUtils.addToClasspath("file:///home/dimon/rtihome/lib/portico.jar");
        printClassPath();

        ManagementServer server = new ManagementServer();
        //On UI level will be filled combobox with addresses
        Set<InetAddress> mdlslist = server.discoverModelingServers(1000);
        for (InetAddress inetAddress : mdlslist) {
            System.out.println(inetAddress);
        }
        //Create federates on UI level
        List<InetAddress> addresses = new ArrayList<InetAddress>();
        for (InetAddress inetAddress : mdlslist) {
            addresses.add(inetAddress);
        }
        server.addMSClient(new ModelingServerClient("TestFederation", "TestFederate",
                "TestRootAggregate", "agg_part_0", addresses.get(0), server.getResponsePool()));
        server.addMSClient(new ModelingServerClient("TestFederation", "TestFederate2",
                "TestRootAggregate", "agg_part_1", addresses.get(0), server.getResponsePool()));
        //Fill experiment start window on UI level and press "Run"
        PrepareToRunRequest request = new PrepareToRunRequest();
        //set params of experiment
        request.collectPrimaryStat = true;
        request.collectStatOnEvent = false;
        request.dumpPeriod = 5.0;
        request.finishTime = 100.0;
        List<String> objectsForStat = new ArrayList<String>();
        objectsForStat.add("TestRootAggregate.agg_part_1.T0");
        request.objectsForCollectStat = objectsForStat;

        server.sendRequest(request);
        //wait for responce from all modeling servers
        server.waitForResponses();
        //when all modeling servers has responded we shoyl clear response pool
        server.getResponsePool().clear();
        //create FOM
        FomData fomData = createTmpFom(server);
        //create Federation
        FederationExecution federationExecution = new FederationExecution();
        federationExecution.createFederation("TestFederation", "TestFederation" + "FOM.xml");
        //wait unlil federation will be created
        while (!federationExecution.isFederationCreated()) {
        }
        System.out.println("Federation created");
        //start modeling
        PrepareToStartRequest ptosrequest = new PrepareToStartRequest();
        ptosrequest.fomData = fomData;
        server.sendRequest(ptosrequest);
        //wait for report responses from all modeling servers
        server.waitForResponses();
        //crear again
        server.getResponsePool().clear();
        //all federates has runned and now they all can to start
        server.sendRequest(new StartRequest());
        //wait for report responses from all modeling servers
        server.waitForResponses();
        printReport(server.getResponsePool().getReport());
        try {
            federationExecution.destroyFederation("TestFederation");
        } catch (FederatesCurrentlyJoined ex) {
            Logger.getLogger(ExampleManagementServer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FederationExecutionDoesNotExist ex) {
            Logger.getLogger(ExampleManagementServer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (RTIinternalError ex) {
            Logger.getLogger(ExampleManagementServer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ConcurrentAccessAttempted ex) {
            Logger.getLogger(ExampleManagementServer.class.getName()).log(Level.SEVERE, null, ex);
        }
        server.sendRequest(new FederationDestroyedRequest());
        server.stop();
        destroyTmpFom();
        System.out.println("End");
    }

    private static void printReport(ExperimentReport report) {
        if (report == null) {
            return;
        }
        Set<String> list = report.getStatisticsObjectsNames();
        for (String object : list) {
            List<PrimaryStatisticsResult> statresult = report.getPrimaryStatisticsFor(object);
            for (PrimaryStatisticsResult primaryStatisticsResult : statresult) {
                for (PrimaryStatisticsElement se : primaryStatisticsResult.getDumpedPrimaryStatisticsElements()) {
                    StatisticsResult result = se.getStatisticsResult();
                    System.out.println("Time: " + se.getTime() + " - " + result);
                }
            }
        }
    }

    private static FomData createTmpFom(ManagementServer server) {
        ExampleAggregateProvider provider = new ExampleAggregateProvider();
        //also create root Aggregate
        provider.getAggretate("TestRootAggregate", "agg_part_0");

        AggregateDefinition root = provider.getRootAggregate();
        List<String> aggs = new ArrayList<String>();
        for (ModelingServerClient client : server.getMsClients()) {
            aggs.add(client.getAggregateName());
        }
        if (aggs.isEmpty()) {
            return null;
        }
        List<XmlInfo> classData = new ArrayList<XmlInfo>();
        FomData fomData = null;
        Set<Aggregate> aggSet = root.getChildAggregates();
        for (Aggregate aggregate : aggSet) {
            for (String aggregateName : aggs) {
                if (aggregateName.equals(aggregate.getName())) {
                    Set<String> attrs = new HashSet<String>();
                    fillAttributes(aggregate, attrs);
                    List<String> attributes = new ArrayList<String>();
                    attributes.addAll(attrs);
                    XmlInfo classInfo = new XmlInfo(aggregateName, attributes);
                    classData.add(classInfo);
                }
            }
        }
        if (!classData.isEmpty()) {
            try {
                FomXmlParsing.createFomFile(classData, null, "TestFederation" + "FOM.xml");
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            } catch (TransformerException e) {
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            fomData = new FomData();
            fomData.setClassData(classData);
            return fomData;
        }
        return fomData;
    }

    private static void destroyTmpFom() {
        FomXmlParsing.removeFomFile("TestFederation" + "FOM.xml");
    }

    private static void fillAttributes(Aggregate aggregate, Set<String> attrs) {
        for (Aggregate agg : aggregate.getChildAggregates()) {
            fillAttributes(agg, attrs);
        }
        for (Place place : aggregate.getPlaces()) {
            if (place.getToken() != null) {
                for (String attrName : place.getToken().getAttributeNames()) {
                    attrs.add(attrName);
                }
            }
        }
    }

    private static void printClassPath() {
        Classpath classpath = new Classpath();
        URL[] originalSystemPath = classpath.getUrlSearchPath();
        System.out.println("Classpath:");
        for (URL url : originalSystemPath) {
            System.out.println(url.getPath());
        }
    }
}
