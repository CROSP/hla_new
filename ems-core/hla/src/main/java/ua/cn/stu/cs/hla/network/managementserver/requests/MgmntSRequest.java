package ua.cn.stu.cs.hla.network.managementserver.requests;

/**
 * Interface for management server requests.
 *
 * @author Dmitry Krivenko <dmitrykrivenko@gmal.com>
 */
public interface MgmntSRequest {

    public void initRequest(String federationName, String federateName, String rootAggregateName, String aggregateName);
}
