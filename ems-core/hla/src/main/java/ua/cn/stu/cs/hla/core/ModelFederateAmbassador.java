package ua.cn.stu.cs.hla.core;

import hla.rti.*;
import hla.rti.jlc.EncodingHelpers;
import hla.rti.jlc.NullFederateAmbassador;
import java.util.*;
import org.portico.impl.hla13.types.DoubleTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.ecli.Value;

/**
 *
 * @author Dmitrij999
 */
public class ModelFederateAmbassador extends NullFederateAmbassador {

    private static final Logger logger = LoggerFactory.getLogger(ModelFederateAmbassador.class);
    protected double federateTime = 0.0;
    protected double federateLookahead = 0.0;//10^-9
    protected boolean isRegulating = false;
    protected boolean isConstrained = false;
    protected boolean isAdvancing = false;
    protected boolean isAnnounced = false;
    protected boolean isReadyToRun = false;
    protected boolean isReadyToStart = false;
    protected boolean isReadyToEnd = false;
    private List<HLAInput> hlaInputs = new ArrayList<HLAInput>();
    private List<HLAOutput> hlaOutputs = new ArrayList<HLAOutput>();
    protected volatile boolean isReflect = false;
    protected TOTokenQueue toTokens = new TOTokenQueue();
    // we need not to replace elements with same key, because we lose same elements (if used map)
    // when uses map we are not depends about current position of element (because if another thread remove same element in map
    // position of our element will changed. And we can`t do concurrent iterate and modify through elements - List)
    protected volatile Map<Integer, List<Attribute>> allAttributes = new HashMap<Integer, List<Attribute>>();

    public HashMap<Integer, List<Attribute>> getAllAttributes() {
        return (HashMap<Integer, List<Attribute>>) allAttributes;
    }

    private double convertTime(LogicalTime logicalTime) {
        // PORTICO SPECIFIC!!
        return ((DoubleTime) logicalTime).getTime();
    }

    @Override
    public void synchronizationPointRegistrationFailed(String synchronizationPointLabel) {
        logger.debug("synchronizationPointRegistrationFailed");
    }

    @Override
    public void synchronizationPointRegistrationSucceeded(String synchronizationPointLabel) throws FederateInternalError {
        logger.debug("synchronizationPointRegistrationSucces");
    }

    @Override
    public void announceSynchronizationPoint(String synchronizationPointLabel, byte[] userSuppliedTag) throws FederateInternalError {
        logger.debug("Synchronize point announced:" + synchronizationPointLabel);
        if (synchronizationPointLabel.equals(FederateExecution.READY_TO_RUN)) {
            this.isAnnounced = true;
        }

    }

    @Override
    public void federationSynchronized(String synchronizationPointLabel) throws FederateInternalError {
        logger.debug("Federation Synchronized: " + synchronizationPointLabel);
        if (synchronizationPointLabel.equals(FederateExecution.READY_TO_RUN)) {
            this.isReadyToRun = true;
        }
        if (synchronizationPointLabel.equals(FederateExecution.READY_TO_START)) {
            this.isReadyToStart = true;
        }
        if (synchronizationPointLabel.equals(FederateExecution.READY_TO_END)) {
            this.isReadyToEnd = true;
        }
    }

    @Override
    public void timeRegulationEnabled(LogicalTime theFederateTime) throws InvalidFederationTime, EnableTimeRegulationWasNotPending,
            FederateInternalError {
        this.federateTime = convertTime(theFederateTime);
        this.isRegulating = true;
    }

    @Override
    public void timeConstrainedEnabled(LogicalTime theFederateTime) throws InvalidFederationTime, EnableTimeConstrainedWasNotPending,
            FederateInternalError {
        this.federateTime = convertTime(theFederateTime);
        this.isConstrained = true;
    }

    @Override
    public void timeAdvanceGrant(LogicalTime theTime) throws InvalidFederationTime, TimeAdvanceWasNotInProgress, FederateInternalError {
        this.federateTime = convertTime(theTime);
        this.isAdvancing = false;
    }

    @Override
    public void discoverObjectInstance(int theObject,
            int theObjectClass,
            String objectName) {
        logger.debug("Discoverd Object: handle=" + theObject + ", classHandle="
                + theObjectClass + ", name=" + objectName);
    }

    @Override
    public void reflectAttributeValues(int theObject, ReflectedAttributes theAttributes, byte[] userSuppliedTag)
            throws ObjectNotKnown, AttributeNotKnown, FederateOwnsAttributes, FederateInternalError {
        try {
            reflectAttributeValues(theObject, theAttributes, userSuppliedTag, null, null);
        } catch (InvalidFederationTime e) {
            logger.debug(e.toString());
        }
    }

    /**
     * In list of attributes the first attribute will contain objectHandle, with
     * this HlaInput can receive it own attributes(check class of attrs)
     */
    @Override
    public void reflectAttributeValues(int theObject, ReflectedAttributes attributes, byte[] userSuppliedTag, LogicalTime theTime,
            EventRetractionHandle retractionHandle) throws ObjectNotKnown, AttributeNotKnown, FederateOwnsAttributes, InvalidFederationTime,
            FederateInternalError {
        logger.debug("Reflect attribute values");
        List<Attribute> attrs = new LinkedList<Attribute>();
        int size = attributes.size();
        String attrOutputName = null;
        for (int i = 0; i < size; i++) {
            Attribute attribute = new Attribute();

            String stringAttribute = null;
            try {
                stringAttribute = EncodingHelpers.decodeString(attributes.getValue(i));
            } catch (ArrayIndexOutOfBounds e) {
                logger.error("ArrayIndexOutOfBounds exception while reflecting attribute values", e);
            }

            String[] masAttrs = stringAttribute.split(", ");

            String attrName = masAttrs[0];
            int attrType = Integer.parseInt(masAttrs[1]);
            Object attrValue = TypeParser.decodeAttribute(masAttrs[2], attrType);
            attrOutputName = masAttrs[3];

            attribute.setAttrName(attrName);
            attribute.setAttrType(attrType);
            attribute.setAttrValue(attrValue);

            attrs.add(attribute);
        }

        Token token = new Token();
        for (Attribute attribute : attrs) {           
            Value value = new Value(attribute.getAttrValue());
            token.setValue(attribute.getAttrName(), value);
        }
        double time = convertTime(theTime) - 1;
        TOToken toToken = new TOToken(token, theObject, attrOutputName);
        this.toTokens.addTOToken(toToken, time);
    }

    @Override
    public void receiveInteraction(int interactionClass, ReceivedInteraction theInteraction, byte[] userSuppliedTag)
            throws InteractionClassNotKnown, InteractionParameterNotKnown, FederateInternalError {
        super.receiveInteraction(interactionClass, theInteraction, userSuppliedTag);
    }

    @Override
    public void receiveInteraction(int interactionClass, ReceivedInteraction theInteraction, byte[] userSuppliedTag, LogicalTime theTime,
            EventRetractionHandle eventRetractionHandle) throws InteractionClassNotKnown, InteractionParameterNotKnown, InvalidFederationTime,
            FederateInternalError {
        super.receiveInteraction(interactionClass, theInteraction, userSuppliedTag, theTime, eventRetractionHandle);
    }

    @Override
    public void removeObjectInstance(int theObject, byte[] userSuppliedTag) throws ObjectNotKnown, FederateInternalError {
        logger.debug("Object removed with hadle:" + theObject);
    }

    /**
     * @return the hlaInputs
     */
    public List<HLAInput> getHlaInputs() {
        return hlaInputs;
    }

    /**
     * @param hlaInputs the hlaInputs to set
     */
    public void setHlaInputs(List<HLAInput> hlaInputs) {
        this.hlaInputs = hlaInputs;
    }

    /**
     * @return the hlaOutputs
     */
    public List<HLAOutput> getHlaOutputs() {
        return hlaOutputs;
    }

    /**
     * @param hlaOutputs the hlaOutputs to set
     */
    public void setHlaOutputs(List<HLAOutput> hlaOutputs) {
        this.hlaOutputs = hlaOutputs;
    }
}
