package ua.cn.stu.cs.hla.core;

/**
 * This information receive hlaInput.
 *
 * @author Dmitrij999
 *
 */
public class Attribute {

    private String attrName;
    private Integer attrType;
    private Object attrValue;
    private String outputName;

    protected Attribute() {
    }

    public Attribute(String attrName, Integer attrType, Object attrValue, String outputName) {
        this.attrName = attrName;
        this.attrType = attrType;
        this.attrValue = attrValue;
        this.outputName = outputName;
    }

    protected void setAttrName(String attrName) {
        this.attrName = attrName;
    }

    public String getAttrName() {
        return attrName;
    }

    protected void setAttrType(Integer attrType) {
        this.attrType = attrType;

    }

    public Integer getAttrType() {
        return attrType;
    }

    protected void setAttrValue(Object attValue) {
        this.attrValue = attValue;
    }

    public Object getAttrValue() {
        return attrValue;
    }

    @Override
    public String toString() {
        return String.format("Name: %s; Type: %d; Value: %s; Output: %s", attrName, attrType, attrValue, outputName);
    }   

    /**
     * @param outputName the outputName to set
     */
    public void setOutputName(String outputName) {
        this.outputName = outputName;
    }

    /**
     * @return the outputName
     */
    public String getOutputName() {
        return outputName;
    }
}
