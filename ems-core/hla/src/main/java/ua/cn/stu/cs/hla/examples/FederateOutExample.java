package ua.cn.stu.cs.hla.examples;

import hla.rti.RTIexception;
import ua.cn.stu.cs.hla.core.FederateExecution;
import ua.cn.stu.cs.hla.core.HlaModel;

public class FederateOutExample {

    public static void main(String[] args) {
        HlaModel modelOut = new ModelTFJOut();
        FederateExecution federateExecution = new FederateExecution("Federation", "FederateOut");
        federateExecution.setFomData(ExampleFom.createFomData());
        federateExecution.setWaitPressEnter(true);
        try {
            federateExecution.runModelFederate(modelOut);
        } catch (RTIexception ex) {
            System.out.println(ex);
        }
    }
}
