package ua.cn.stu.cs.hla.core;

import ua.cn.stu.cs.ems.core.Token;

/**
 *
 * @author Dmitry Krivenko <dmitrykrivenko@gmal.com>
 */
public class TOToken {

    private Token token;
    private int objectHandle;
    //name of output that has sent this token
    private String outputName;

    /**
     * Default constructor without parameters.
     */
    public TOToken() {
    }

    /**
     * Convenience constructor with parameters.
     *
     * @param objectHandle
     * @param outputName
     */
    public TOToken(Token token, int objectHandle, String outputName) {
        this.token = token;
        this.objectHandle = objectHandle;
        this.outputName = outputName;
    }

    /**
     * @return the objectHandle
     */
    public int getObjectHandle() {
        return objectHandle;
    }

    /**
     * @param objectHandle the objectHandle to set
     */
    public void setObjectHandle(int objectHandle) {
        this.objectHandle = objectHandle;
    }

    /**
     * @return the outputName
     */
    public String getOutputName() {
        return outputName;
    }

    /**
     * @param outputName the outputName to set
     */
    public void setOutputName(String outputName) {
        this.outputName = outputName;
    }

    /**
     * @return the token
     */
    public Token getToken() {
        return token;
    }

    /**
     * @param token the token to set
     */
    public void setToken(Token token) {
        this.token = token;
    }
}
