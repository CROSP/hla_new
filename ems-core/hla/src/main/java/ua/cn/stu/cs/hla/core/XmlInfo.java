package ua.cn.stu.cs.hla.core;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Dmitrij999
 *
 * This class presents information to create fom xml file
 */
public class XmlInfo {

    private String name;
    private List<String> attrs;

    /**
     * Default constructor without parameters.
     */
    public XmlInfo() {
    }

    /**
     * Convenience constructor with parameters.
     *
     * @param name
     * @param attrs
     */
    public XmlInfo(String name, List<String> attrs) {
        this.name = name;
        this.attrs = new ArrayList<String>(attrs);
    }

    public String getName() {
        return name;
    }

    public int getAttrCount() {
        return attrs.size();
    }

    public List<String> getAttributes() {
        return new ArrayList<String>(attrs);
    }
}
