package ua.cn.stu.cs.hla.network;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.EndPoint;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import ua.cn.stu.cs.ems.core.experiment.ExperimentReport;
import ua.cn.stu.cs.ems.core.experiment.PrimaryStatisticsElement;
import ua.cn.stu.cs.ems.core.experiment.PrimaryStatisticsResult;
import ua.cn.stu.cs.ems.core.experiment.statistics.StatisticsParameters;
import ua.cn.stu.cs.ems.core.experiment.statistics.StatisticsResult;
import ua.cn.stu.cs.ems.core.models.DelayedQueue;
import ua.cn.stu.cs.hla.core.FomData;
import ua.cn.stu.cs.hla.core.HlaENetworksModel;
import ua.cn.stu.cs.hla.core.XmlInfo;
import ua.cn.stu.cs.hla.network.managementserver.requests.FederationDestroyedRequest;
import ua.cn.stu.cs.hla.network.managementserver.requests.PrepareToRunRequest;
import ua.cn.stu.cs.hla.network.managementserver.requests.PrepareToStartRequest;
import ua.cn.stu.cs.hla.network.managementserver.requests.StartRequest;
import ua.cn.stu.cs.hla.network.modelingserver.responses.ReadyToRunResponse;
import ua.cn.stu.cs.hla.network.modelingserver.responses.ReadyToStartResponse;
import ua.cn.stu.cs.hla.network.modelingserver.responses.ReportResponse;

/**
 * This class is a convenient place to keep things common to both the client and
 * server.
 *
 * @author Dmitry Krivenko <dmitrykrivenko@gmal.com>
 */
public class Network {

    static public void register(EndPoint endPoint) {
        Kryo kryo = endPoint.getKryo();
        kryo.register(PrepareToRunRequest.class);
        kryo.register(PrepareToStartRequest.class);
        kryo.register(StartRequest.class);
        kryo.register(FederationDestroyedRequest.class);
        kryo.register(ReadyToRunResponse.class);
        kryo.register(ReadyToStartResponse.class);
        kryo.register(ReportResponse.class);
        kryo.register(ArrayList.class);
        kryo.register(HashMap.class);
        kryo.register(HashSet.class);
        kryo.register(HlaENetworksModel.class);
        kryo.register(DelayedQueue.class);
        kryo.register(PrimaryStatisticsResult.class);
        kryo.register(PrimaryStatisticsElement.class);
        kryo.register(StatisticsResult.class);
        kryo.register(StatisticsParameters.class);
        kryo.register(FomData.class);
        kryo.register(XmlInfo.class);
    }
}
