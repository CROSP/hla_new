package ua.cn.stu.cs.hla.network.managementserver;

import com.esotericsoftware.kryonet.Client;
import java.io.IOException;
import java.net.InetAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.cn.stu.cs.hla.network.Constants;
import ua.cn.stu.cs.hla.network.Network;
import ua.cn.stu.cs.hla.network.managementserver.listeners.ManagementServerListener;

/**
 * Modeling server client.
 *
 * @author Dmitry Krivenko <dmitrykrivenko@gmal.com>
 */
public class ModelingServerClient {

    private static final Logger logger = LoggerFactory.getLogger(ModelingServerClient.class);
    private String federationName;
    private String federateName;
    private String rootAggregateName;
    private String aggregateName;
    private Client client;

    /**
     * Default constructor without parameters.
     */
    public ModelingServerClient() {
    }

    /**
     * Convenience constructor with parameters.
     *
     * @param federationName name of federation
     * @param federateName name of federate
     * @param rootAggregateName name of root aggregate
     * @param aggregateName name of aggregate
     * @param serverAdress address of modeling server
     */
    public ModelingServerClient(String federationName, String federateName, String rootAggregateName,
            String aggregateName, InetAddress serverAdress, ResponsePool responsePool) {
        this.federationName = federationName;
        this.federateName = federateName;
        this.rootAggregateName = rootAggregateName;
        this.aggregateName = aggregateName;

        client = new Client(8192, 32768);
        client.start();
        client.addListener(new ManagementServerListener(responsePool));

        Network.register(client);

        try {
            client.connect(5000, serverAdress, Constants.DEFAULT_SERVER_TCP_PORT, Constants.DEFAULT_SERVER_UDP_PORT);
        } catch (IOException ex) {
            logger.error("IOException while creating connect", ex);
        }
    }

    /**
     * Get name of federation.
     *
     * @return the federationName
     */
    public String getFederationName() {
        return federationName;
    }

    /**
     * Set name of federation.
     *
     * @param federationName the federationName to set
     */
    public void setFederationName(String federationName) {
        this.federationName = federationName;
    }

    /**
     * Get name of federate.
     *
     * @return the federateName
     */
    public String getFederateName() {
        return federateName;
    }

    /**
     * Set name of federate.
     *
     * @param federateName the federateName to set
     */
    public void setFederateName(String federateName) {
        this.federateName = federateName;
    }

    /**
     * Get name of root aggregate.
     *
     * @return the rootAggregateName
     */
    public String getRootAggregateName() {
        return rootAggregateName;
    }

    /**
     * Set name of root aggregate.
     *
     * @param rootAggregateName the rootAggregateName to set
     */
    public void setRootAggregateName(String rootAggregateName) {
        this.rootAggregateName = rootAggregateName;
    }

    /**
     * Get client.
     *
     * @return the client
     */
    public Client getClient() {
        return client;
    }

    /**
     * Set client.
     *
     * @param client the client to set
     */
    public void setClient(Client client) {
        this.client = client;
    }

    /**
     * Get name of aggregate.
     *
     * @return the aggregateName
     */
    public String getAggregateName() {
        return aggregateName;
    }

    /**
     * Set name of aggregate.
     *
     * @param aggregateName the aggregateName to set
     */
    public void setAggregateName(String aggregateName) {
        this.aggregateName = aggregateName;
    }
}
