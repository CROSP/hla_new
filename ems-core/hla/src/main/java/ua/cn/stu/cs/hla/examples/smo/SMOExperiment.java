package ua.cn.stu.cs.hla.examples.smo;

import hla.rti.RTIexception;
import java.util.LinkedList;
import java.util.List;
import ua.cn.stu.cs.hla.core.FederateExecution;
import ua.cn.stu.cs.hla.core.FederationExecution;
import ua.cn.stu.cs.hla.core.FomXmlParsing;
import ua.cn.stu.cs.hla.core.HlaModel;
import ua.cn.stu.cs.hla.examples.ExampleFom;

/**
 *
 * @author Dmitry Krivenko <dmitrykrivenko@gmal.com>
 */
public class SMOExperiment {

    private static List<FederateExecution> federates = new LinkedList<FederateExecution>();

    public static void main(String[] args) throws InterruptedException {
        FederationExecution federationExecution = new FederationExecution();
        federationExecution.createFederation("FederationSmoExperiment", FomXmlParsing.fomFileName);
        System.out.println("Federation creation");
        Thread.sleep(10000L);

        FederateExecution fedExcGenerator = new FederateExecution("FederationSmoExperiment", "FederateModelGenerator");
        fedExcGenerator.setFomData(ExampleFom.createFomData());
        FederateExecution fedExcReceiver = new FederateExecution("FederationSmoExperiment", "FederateModelReceiver");
        fedExcReceiver.setFomData(ExampleFom.createFomData());
        
        federates.add(fedExcGenerator);
        federates.add(fedExcReceiver);

        FederateGeneratorThread generator = new FederateGeneratorThread(fedExcGenerator);
        FederateReceiverThread receiver = new FederateReceiverThread(fedExcReceiver);

        System.out.println("Start generator:");
        generator.start();

        System.out.println("Start receiver:");
        receiver.start();

        Thread.sleep(20000L);

        for (FederateExecution fe : federates) {
            fe.setReadyToStart(true);
        }
    }

    public static class FederateGeneratorThread implements Runnable {

        private FederateExecution federateExecution = null;
        private volatile Thread currentThread;

        public FederateGeneratorThread(FederateExecution federateExecution) {
            this.federateExecution = federateExecution;
        }

        public void start() {
            currentThread = new Thread(this);
            currentThread.setName(String.format("%s Federate Thread", "FederateModelGenerator"));
            currentThread.start();
        }

        public void stop() {
            currentThread = null;
        }

        public void run() {
            HlaModel modelGenerator = new ModelGenerator();
            try {
                federateExecution.runModelFederate(modelGenerator);
            } catch (RTIexception ex) {
                System.out.println(ex);
            }
        }
    }

    public static class FederateReceiverThread implements Runnable {

        private FederateExecution federateExecution = null;
        private volatile Thread currentThread;

        public FederateReceiverThread(FederateExecution federateExecution) {
            this.federateExecution = federateExecution;
        }

        public void start() {
            currentThread = new Thread(this);
            currentThread.setName(String.format("%s Federate Thread", "FederateModelReceiver"));
            currentThread.start();
        }

        public void stop() {
            currentThread = null;
        }

        public void run() {
            HlaModel modelReceiver = new ModelReceiver();
            try {
                federateExecution.runModelFederate(modelReceiver);
            } catch (RTIexception ex) {
                System.out.println(ex);
            }
        }
    }
}
