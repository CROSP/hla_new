package ua.cn.stu.cs.hla.examples;

import hla.rti.RTIexception;
import ua.cn.stu.cs.hla.core.FederateExecution;
import ua.cn.stu.cs.hla.core.HlaModel;

public class FederateInExample {

    public static void main(String[] args) {
        HlaModel model = new ModelTFJIn();
        FederateExecution federateExecution = new FederateExecution("Federation", "FederateIn");
        federateExecution.setFomData(ExampleFom.createFomData());
        federateExecution.setWaitPressEnter(true);
        try {
            federateExecution.runModelFederate(model);
        } catch (RTIexception ex) {
            System.out.println(ex);
        }
    }
}
