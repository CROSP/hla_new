package ua.cn.stu.cs.hla.network.modelingserver;

import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;
import java.io.IOException;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.cn.stu.cs.hla.network.AggregateProvider;
import ua.cn.stu.cs.hla.network.Constants;
import ua.cn.stu.cs.hla.network.Network;
import ua.cn.stu.cs.hla.network.modelingserver.listeners.ModelingServerListener;

/**
 * Modeling server.
 *
 * @author Dmitry Krivenko <dmitrykrivenko@gmal.com>
 */
public class ModelingServer {

    private static final Logger logger = LoggerFactory.getLogger(ModelingServer.class);
    private Server server;
    private boolean listOfListenersIsEmpty = true;
    private AggregateProvider aggregateProvider = null;

    /**
     * Default constructor without parameters.
     */
    public ModelingServer() {
        server = new Server(32768, 8192);
    }

    /**
     * Convenience constructor with parameters.
     *
     * @param listeners list of listeners
     */
    public ModelingServer(List<Listener> listeners, AggregateProvider aggregateProvider) {
        server = new Server(32768, 8192);
        if (listeners != null && !listeners.isEmpty()) {
            for (Listener serverListener : listeners) {
                server.addListener(serverListener);
            }
            listOfListenersIsEmpty = false;
        }
        this.aggregateProvider = aggregateProvider;
    }

    public void start() {
        initServer();
        try {
            server.bind(Constants.DEFAULT_SERVER_TCP_PORT, Constants.DEFAULT_SERVER_UDP_PORT);
        } catch (IOException ex) {
            logger.error("Error while starting modeling server", ex);
            return;
        }
        server.start();
        logger.info("Modeling server has started");
    }

    public void stop() {
        server.stop();
    }

    private void initServer() {
        //if list of listeners is empty - add default listener
        if (listOfListenersIsEmpty) {
            if (aggregateProvider != null) {
                server.addListener(new ModelingServerListener(aggregateProvider));
            } else {
                server.addListener(new ModelingServerListener());
            }
        }
        Network.register(server);
    }
}
