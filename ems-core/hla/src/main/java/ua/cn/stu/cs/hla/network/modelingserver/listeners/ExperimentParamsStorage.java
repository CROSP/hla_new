package ua.cn.stu.cs.hla.network.modelingserver.listeners;

import java.util.List;
import ua.cn.stu.cs.ems.core.aggregates.AggregateInstance;

/**
 * Storage of experiment parameters for the concrete aggregate.
 *
 * @author Dmitry Krivenko <dmitrykrivenko@gmal.com>
 */
public class ExperimentParamsStorage {

    private AggregateInstance aggregate;
    private String rootAggregateName;
    private double finishTime;
    private double dumpPeriod;
    private boolean collectPrimaryStat;
    private boolean collectStatOnEvent;
    private List<String> objectsForCollectStat;

    /**
     * Default constructor without parameters.
     */
    public ExperimentParamsStorage() {
    }

    /**
     * Convenience constructor with parameters.
     *
     * @param aggregate
     * @param finishTime
     * @param dumpPeriod
     * @param collectPrimaryStat
     * @param collectStatOnEvent
     * @param objectsForCollectStat
     */
    public ExperimentParamsStorage(AggregateInstance aggregate, String rootAggregateName, double finishTime, double dumpPeriod,
            boolean collectPrimaryStat, boolean collectStatOnEvent, List<String> objectsForCollectStat) {
        this.aggregate = aggregate;
        this.rootAggregateName = rootAggregateName;
        this.finishTime = finishTime;
        this.dumpPeriod = dumpPeriod;
        this.collectPrimaryStat = collectPrimaryStat;
        this.collectStatOnEvent = collectStatOnEvent;
        this.objectsForCollectStat = objectsForCollectStat;
    }

    /**
     * Get the aggregate.
     *
     * @return the aggregate
     */
    public AggregateInstance getAggregate() {
        return aggregate;
    }

    /**
     * Set the aggregate.
     *
     * @param aggregate the aggregate to set
     */
    public void setAggregate(AggregateInstance aggregate) {
        this.aggregate = aggregate;
    }

    /**
     * Get finish time.
     *
     * @return the finishTime
     */
    public double getFinishTime() {
        return finishTime;
    }

    /**
     * Set finish time.
     *
     * @param finishTime the finishTime to set
     */
    public void setFinishTime(double finishTime) {
        this.finishTime = finishTime;
    }

    /**
     * Get dump period of statistic collecting.
     *
     * @return the dumpPeriod
     */
    public double getDumpPeriod() {
        return dumpPeriod;
    }

    /**
     * Set dump period of statistic collecting.
     *
     * @param dumpPeriod the dumpPeriod to set
     */
    public void setDumpPeriod(double dumpPeriod) {
        this.dumpPeriod = dumpPeriod;
    }

    /**
     * Whether or not to collect primary statistic.
     *
     * @return the collectPrimaryStat
     */
    public boolean isCollectPrimaryStat() {
        return collectPrimaryStat;
    }

    /**
     * Set whether or not to collect primary statistic.
     *
     * @param collectPrimaryStat the collectPrimaryStat to set
     */
    public void setCollectPrimaryStat(boolean collectPrimaryStat) {
        this.collectPrimaryStat = collectPrimaryStat;
    }

    /**
     * Whether to collect statistics on the event or not.
     *
     * @return the collectStatOnEvent
     */
    public boolean isCollectStatOnEvent() {
        return collectStatOnEvent;
    }

    /**
     * Set to collect statistic on event.
     *
     * @param collectStatOnEvent the collectStatOnEvent to set
     */
    public void setCollectStatOnEvent(boolean collectStatOnEvent) {
        this.collectStatOnEvent = collectStatOnEvent;
    }

    /**
     * Get objects, the statistics to be collected.
     *
     * @return the objectsForCollectStat
     */
    public List<String> getObjectsForCollectStat() {
        return objectsForCollectStat;
    }

    /**
     * Set objects, the statistics to be collected.
     *
     * @param objectsForCollectStat the objectsForCollectStat to set
     */
    public void setObjectsForCollectStat(List<String> objectsForCollectStat) {
        this.objectsForCollectStat = objectsForCollectStat;
    }

    /**
     * @return the rootAggregateName
     */
    public String getRootAggregateName() {
        return rootAggregateName;
    }

    /**
     * @param rootAggregateName the rootAggregateName to set
     */
    public void setRootAggregateName(String rootAggregateName) {
        this.rootAggregateName = rootAggregateName;
    }
}
