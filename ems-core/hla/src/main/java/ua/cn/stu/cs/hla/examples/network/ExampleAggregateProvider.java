package ua.cn.stu.cs.hla.examples.network;

import java.util.List;
import ua.cn.stu.cs.ems.core.Attribute;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.aggregates.*;
import ua.cn.stu.cs.ems.core.transitions.TTransition;
import ua.cn.stu.cs.ems.core.transitions.functions.ConstDelayFunction;
import ua.cn.stu.cs.ems.ecli.Value;
import ua.cn.stu.cs.hla.network.AggregateProvider;

/**
 * Example of aggregate provider implementation{@link AggregateProvider}.
 *
 * @author Dmitry Krivenko <dmitrykrivenko@gmal.com>
 */
public class ExampleAggregateProvider implements AggregateProvider {

    private AggregateDefinition rootAgg;

    public AggregateInstance getAggretate(String modelName, String aggName) {
        AggregateInstance ai = createAggregate(aggName);
        return ai;
    }

    private AggregateInstance createAggregate(String name) {
        rootAgg = createRootAggregate();
        AggregateDefinitionReference agg = (AggregateDefinitionReference) getRootAggregate().getAggregate(name);
        AggregateInstance ai = agg.getAggregateDefinition().createInstance(agg.getName());
        List<Input> inputs = agg.getInputs();
        for (Input input : inputs) {
            if (input.getConnectedOutput() != null) {
                ai.getInput(input.getName()).connectOutput(input.getConnectedOutput());
            }
        }
        List<Output> outputs = agg.getOutputs();
        for (Output output : outputs) {
            if (!output.getConnectedInputs().isEmpty()) {
                ai.getOutput(output.getName()).addInput(output.getConnectedInputs().get(0), 0);
            }
        }
        return ai;
    }

    private AggregateDefinition createRootAggregate() {
        AggregateDefinition rootDef = new AggregateDefinition("TestRootAggregate");

        /*
         * -------------------Aggregate definition 0------------------------------
         */
        AggregateDefinition aggrDef0 = new AggregateDefinition("agg_part0");

        Input i0Def0 = new InputImpl("I0");
        Output o0Def0 = new OutputImpl("O0");

        Place p0Def0 = new Place("P0");
        Place p1Def0 = new Place("P1");

        TTransition t0Def0 = new TTransition("T0");

        t0Def0.setDelayFunction(new ConstDelayFunction(10));

        t0Def0.addInputPlace(p0Def0, 0);
        t0Def0.addOutputPlace(p1Def0, 0);

        i0Def0.setOutputPlace(p0Def0);
        o0Def0.connectInputPlace(p1Def0);

        aggrDef0.addPlaces(p0Def0, p1Def0);
        aggrDef0.addTransitions(t0Def0);
        aggrDef0.addInput(i0Def0, 0);
        aggrDef0.addOutput(o0Def0, 0);
        /*
         * ---------------------------------------------------------------------
         */
        
        /*
         * -------------------Aggregate definition 1------------------------------
         */
        AggregateDefinition aggrDef1 = new AggregateDefinition("agg_part1");

        Input i0Def1 = new InputImpl("I0");
        Output o0Def1 = new OutputImpl("O0");

        Place p0Def1 = new Place("P0");
        Place p1Def1 = new Place("P1");

        TTransition t0Def1 = new TTransition("T0");

        t0Def1.setDelayFunction(new ConstDelayFunction(10));

        t0Def1.addInputPlace(p0Def1, 0);
        t0Def1.addOutputPlace(p1Def1, 0);

        i0Def1.setOutputPlace(p0Def1);
        o0Def1.connectInputPlace(p1Def1);

        aggrDef1.addPlaces(p0Def1, p1Def1);
        aggrDef1.addTransitions(t0Def1);
        aggrDef1.addInput(i0Def1, 0);
        aggrDef1.addOutput(o0Def1, 0);
        /*
         * ---------------------------------------------------------------------
         */

        AggregateDefinitionReference agrReference0 = rootDef.addChildAggreagateDefinition(aggrDef0, "agg_part_0");
        AggregateDefinitionReference agrReference1 = rootDef.addChildAggreagateDefinition(aggrDef1, "agg_part_1");


        /*
         * ---------------------------2nd aggregate----------------------------
         */
        Token token = new Token();
        Attribute attr = new Attribute("testAttr", Value.TRUE);
        token.addAttributes(attr);

        agrReference0.getPlace("P0").setToken(token);
        agrReference0.getOutput("O0").addInput(agrReference1.getInput("I0"), 0);
        /*
         * ---------------------------------------------------------------------
         */

        return rootDef;
    }

    /**
     * @return the rootAgg
     */
    public AggregateDefinition getRootAggregate() {
        return rootAgg;
    }
}
