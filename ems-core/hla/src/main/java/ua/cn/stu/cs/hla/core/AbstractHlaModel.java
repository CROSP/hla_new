package ua.cn.stu.cs.hla.core;

import hla.rti.RTIambassador;
import ua.cn.stu.cs.ems.core.experiment.Experiment;
import ua.cn.stu.cs.ems.core.experiment.ExperimentReport;
import ua.cn.stu.cs.ems.core.models.Model;

/**
 * Should be realized by any model, that is used in
 * {@link FederateExecution#runModelFederate(ua.cn.stu.cs.hla.core.HlaModel)}.
 *
 * @see ModelFederateAmbassador
 * @see Model
 * @see Experimen
 * @see HlaModel
 *
 * @author Dmitry Krivenko <dmitrykrivenko@gmal.com>
 */
public abstract class AbstractHlaModel implements HlaModel {

    private ModelFederateAmbassador fedAmb;
    private RTIambassador rtiAmb;
    private Model model;
    private Experiment experiment;
    private ExperimentReport report;

    public abstract void createModel(FederateExecution federateExecution);

    public abstract void startModel();

    /**
     * @return the fedAmb
     */
    public ModelFederateAmbassador getFedAmb() {
        return fedAmb;
    }

    /**
     * @param fedAmb the fedAmb to set
     */
    public void setFedAmb(ModelFederateAmbassador fedAmb) {
        this.fedAmb = fedAmb;
    }

    /**
     * @return the rtiAmb
     */
    public RTIambassador getRtiAmb() {
        return rtiAmb;
    }

    /**
     * @param rtiAmb the rtiAmb to set
     */
    public void setRtiAmb(RTIambassador rtiAmb) {
        this.rtiAmb = rtiAmb;
    }

    /**
     * @return the experiment
     */
    public Experiment getExperiment() {
        return experiment;
    }

    /**
     * @param experiment the experiment to set
     */
    public void setExperiment(Experiment experiment) {
        this.experiment = experiment;
    }

    /**
     * @return the model
     */
    public Model getModel() {
        return model;
    }

    /**
     * @param model the model to set
     */
    public void setModel(Model model) {
        this.model = model;
    }

    /**
     * @return the report
     */
    public ExperimentReport getReport() {
        return report;
    }

    /**
     * @param report the report to set
     */
    public void setReport(ExperimentReport report) {
        this.report = report;
    }
}
