package ua.cn.stu.cs.hla.network.managementserver.requests;

import ua.cn.stu.cs.hla.core.FomData;
import ua.cn.stu.cs.hla.network.managementserver.ManagementServer;

/**
 * Request that central server {@link ManagementServer} send before start the
 * modeling.
 *
 * @author Dmitry Krivenko <dmitrykrivenko@gmal.com>
 */
public class PrepareToStartRequest extends AbstractMgmntSRequest {

    public FomData fomData;

    @Override
    public String toString() {
        return "PrepareToStartRequest[federationName=" + federationName + "; federateName="
                + federateName + "; rootAggregateName=" + rootAggregateName
                + "; aggregateName=" + aggregateName + "]";
    }
}
