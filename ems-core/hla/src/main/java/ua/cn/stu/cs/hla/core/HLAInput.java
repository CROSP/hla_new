package ua.cn.stu.cs.hla.core;

import hla.rti.FederateNotExecutionMember;
import hla.rti.NameNotFound;
import hla.rti.RTIexception;
import hla.rti.RTIinternalError;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.cn.stu.cs.ems.core.AbstractActiveInstance;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.aggregates.Input;
import ua.cn.stu.cs.ems.core.aggregates.Output;
import ua.cn.stu.cs.ems.core.utils.AssertUtils;

/**
 *
 * @author Dmitrij999
 */
public class HLAInput extends AbstractActiveInstance implements Input {

    private static final Logger logger = LoggerFactory.getLogger(HLAInput.class);
    private volatile ModelFederateAmbassador federateAmbassador;
    private volatile String fomElementName;
    private volatile FederateExecution federate;
    private boolean isFedAmbInitialized = false;
    private int classHandle;
    private Place inputPlace;
    private Place outputPlace;
    private Output connectedOutput;
    private String subscribeOutput;

    protected HLAInput(String name) {
        super(name);
    }

    /**
     * @param name name of Hla input
     * @param federateName name of federate, use it to get federate in which we
     * are interesting
     */
    public HLAInput(String name, FederateExecution federateExecution) {
        super(name);
        federate = federateExecution;
    }

    /**
     * @param fomElementName name of class or interaction in fom, use it to
     * publish and subscribe for data and register to right object
     */
    public void setFomElement(String fomElementName, String subscribeOutput) {
        this.fomElementName = fomElementName;
        this.subscribeOutput = subscribeOutput;
        initMethod();
    }

    private void initMethod() {
        try {
            federate.subscribeMethod(fomElementName);
            classHandle = federate.getRtiAmbassador().getObjectClassHandle(fomElementName);
        } catch (NameNotFound e) {
            logger.error(e.getMessage());
        } catch (FederateNotExecutionMember e) {
            logger.error(e.getMessage());
        } catch (RTIinternalError e) {
            logger.error(e.getMessage());
        } catch (RTIexception e) {
            logger.error(e.getMessage());
        }
        federateAmbassador = federate.getFederateAmbassador();
        federateAmbassador.getHlaInputs().add(this);
    }

    public void setToken(Token token) {
        //wait while federate ambassador initialized at first with fom 
        if (!isFedAmbInitialized) {
            while (federateAmbassador == null) {
            }
        }
        isFedAmbInitialized = true;
        if (token == null) {
            logger.error("Can't set token. Token is null.");
        }
        if (!outputPlace.isEmpty()) {
            logger.error("Can't set token. Output place is not empty.");
        }

        outputPlace.setToken(token);
    }

    public boolean canSetToken() {
        if (outputPlace == null) {
            return false;
        }

        if (!outputPlace.isEmpty()) {
            return false;
        }

        return true;
    }

    public List<Place> getInputPlaces() {
        return createListWithSinglePlace(inputPlace);
    }

    public List<Place> getOutputPlaces() {
        return createListWithSinglePlace(outputPlace);
    }

    public void placeStateChanged(Place place) {
        fireIfPosible();
    }

    public void fireIfPosible() {
        if (outputPlace == null) {
            return;
        }

        if (outputPlace.isEmpty()) {
            delayFor(0);
        }
    }

    protected void fireTransition() {
        if (outputPlace == null) {
            return;
        }

        if (connectedOutput != null) {
            connectedOutput.inputCanBeSet(this);
        } else if (inputPlace != null && !inputPlace.isEmpty()) {
            Token inputToken = new Token(inputPlace.getToken());
            outputPlace.setToken(inputToken);
            inputPlace.setToken(null);
        }
    }

    public void setInputPlace(Place place) {
        if (inputPlace != null) {
            inputPlace.setOutputActiveInstance(null);
        }

        inputPlace = place;
        if (place != null) {
            place.setOutputActiveInstance(this);
        }
    }

    public void setOutputPlace(Place place) {
        if (outputPlace != null) {
            outputPlace.setInputActiveInstance(null);
        }
        logger.debug("Set output " + outputPlace);
        outputPlace = place;
        if (place != null) {
            place.setInputActiveInstance(this);
        }
    }

    public void connectOutput(Output output) {
        this.connectedOutput = output;
    }

    public void disconnectInputPlace(String name) {
        AssertUtils.assertNotNull(inputPlace, "No input place is connected");
        AssertUtils.assertTrue(inputPlace.getName().equals(name),
                "Input place's name is '" + inputPlace.getName() + "' imposible to delete input place with name " + name + "'");

        inputPlace = null;
    }

    public void disconnectOutputPlace(String name) {
        AssertUtils.assertNotNull(outputPlace, "No output place is connected");
        AssertUtils.assertTrue(outputPlace.getName().equals(name),
                "Output place's name is '" + outputPlace.getName() + "' imposible to delete output place with name " + name + "'");

        outputPlace = null;
    }

    public Place getOutputPlace() {
        return outputPlace;
    }

    public Place getInputPlace() {
        return inputPlace;
    }

    public Output getConnectedOutput() {
        return connectedOutput;
    }

    /**
     * @return the classHandle
     */
    public int getClassHandle() {
        return classHandle;
    }

    /**
     * @return the subscribeOutput
     */
    public String getSubscribeOutput() {
        return subscribeOutput;
    }
}
