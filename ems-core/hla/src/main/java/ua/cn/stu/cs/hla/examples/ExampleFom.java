package ua.cn.stu.cs.hla.examples;

import java.util.ArrayList;
import java.util.List;
import ua.cn.stu.cs.hla.core.FomData;
import ua.cn.stu.cs.hla.core.XmlInfo;

/**
 *
 * @author Dmitry Krivenko <dmitrykrivenko@gmal.com>
 */
public final class ExampleFom {

    private ExampleFom() {
    }

    public static FomData createFomData() {
        FomData result = new FomData();
        List<XmlInfo> classData = new ArrayList<XmlInfo>();
        XmlInfo classInfo1 = new XmlInfo("Model1", new ArrayList<String>() {

            {
                add("attr1");
                add("attr2");
                add("attr3");
                add("attr4");
                add("attr5");
                add("attr6");
                add("attr7");
                add("attr8");
                add("attr9");
            }
        });
        classData.add(classInfo1);
        XmlInfo classInfo2 = new XmlInfo("Model2", new ArrayList<String>() {

            {
                add("attr1");
                add("attr2");
                add("attr3");
                add("attr4");
                add("attr5");
                add("attr6");
            }
        });
        classData.add(classInfo2);

        List<XmlInfo> interactionData = new ArrayList<XmlInfo>();
        XmlInfo interactionInfo1 = new XmlInfo("X1", new ArrayList<String>() {

            {
                add("attr1");
                add("attr2");
                add("attr3");
            }
        });
        interactionData.add(interactionInfo1);
        XmlInfo interactionInfo2 = new XmlInfo("X2", new ArrayList<String>() {

            {
                add("attr1");
                add("attr2");
                add("attr3");
                add("attr4");
            }
        });
        interactionData.add(interactionInfo2);
        result.setClassData(classData);
        result.setInteractionData(interactionData);
        return result;
    }
}
