package ua.cn.stu.cs.hla.examples;

import hla.rti.ConcurrentAccessAttempted;
import hla.rti.FederatesCurrentlyJoined;
import hla.rti.FederationExecutionDoesNotExist;
import hla.rti.RTIinternalError;
import ua.cn.stu.cs.hla.core.FederationExecution;
import ua.cn.stu.cs.hla.core.FomXmlParsing;

public class FederationExample {

    public static void main(String[] args) throws FederatesCurrentlyJoined, FederationExecutionDoesNotExist, RTIinternalError, ConcurrentAccessAttempted {
        FederationExecution federationExecution = new FederationExecution();
        federationExecution.createFederation("Federation", FomXmlParsing.fomFileName);
        System.out.println("Creating the Federation");
    }
}
