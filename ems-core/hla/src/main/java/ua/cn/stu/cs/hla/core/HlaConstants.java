package ua.cn.stu.cs.hla.core;

/**
 * Constants.
 *
 * @author Dmitry Krivenko <dmitrykrivenko@gmal.com>
 */
public class HlaConstants {

    public static String HLA_INPUT_NAME_BEGIN = "hlaInput_";
    public static String HLA_OUTPUT_NAME_BEGIN = "hlaOutput_";
    public static String HLA_OUTPUT_TO_MODEL_NAME_BEGIN = "outToModel_";
    public static String HLA_IN_PLACE_NAME_BEGIN = "PHlaIn";
    public static String HLA_OUT_TO_MODEL_PLACE_NAME_BEGIN = "PModelOut";
    public static String HLA_T_NAME_BEGIN = "T";
    public static String HLA_OUT_PLACE_NAME_BEGIN = "pHlaOut";
    public static String CM_DEF_NAME = "CMDefinition";
    public static String CM_NAME = "CM";
}
