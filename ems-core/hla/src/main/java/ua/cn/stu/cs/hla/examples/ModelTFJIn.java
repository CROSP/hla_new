package ua.cn.stu.cs.hla.examples;

import java.util.ArrayList;
import java.util.List;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.PlaceStateChangedListener;
import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.aggregates.AggregateInstance;
import ua.cn.stu.cs.ems.core.aggregates.Input;
import ua.cn.stu.cs.ems.core.aggregates.InputImpl;
import ua.cn.stu.cs.ems.core.aggregates.Output;
import ua.cn.stu.cs.ems.core.queues.FIFOQueue;
import ua.cn.stu.cs.ems.core.transitions.FTransition;
import ua.cn.stu.cs.ems.core.transitions.JTransition;
import ua.cn.stu.cs.ems.core.transitions.TTransition;
import ua.cn.stu.cs.ems.core.transitions.functions.ConstDelayFunction;
import ua.cn.stu.cs.ems.core.utils.JessUtils;
import ua.cn.stu.cs.hla.cm.CommunicateModule;
import ua.cn.stu.cs.hla.core.AbstractHlaModel;
import ua.cn.stu.cs.hla.core.FederateExecution;
import ua.cn.stu.cs.hla.core.HlaConstants;
import ua.cn.stu.cs.hla.core.HlaENetworksModel;

/**
 *
 * @author Dmitrij999
 */
public class ModelTFJIn extends AbstractHlaModel {

    public void startModel() {
        getModel().start();
    }

    public void createModel(FederateExecution federateExecution) {
        AggregateInstance a2 = createAgregateA2();

        List<String> inputs = new ArrayList<String>();
        inputs.add("I0");

        AggregateInstance cm = CommunicateModule.getCommunicateModule(federateExecution, inputs, null);

        // set fom element for hlaInput
        CommunicateModule.setFomElementToHlaInput(cm, HlaConstants.HLA_INPUT_NAME_BEGIN + "I0", "Model1", "outToCm");

        AggregateInstance rootAgregate = createRootAgregate(cm, a2);
        HlaENetworksModel model = new HlaENetworksModel(federateExecution.getFederateAmbassador(), federateExecution.getRtiAmbassador());
        rootAgregate.setModel(model);
        model.setRootAggregate(rootAgregate);

        model.setFinishTime(150L);

        setModel(model);
    }

    private static AggregateInstance createAgregateA2() {
        AggregateInstance A2 = new AggregateInstance("A2") {
        };

        Place p1 = new Place("P1");
        Place p2 = new Place("P2");
        Place p3 = new Place("P3");
        Place p4 = new Place("P4");
        Place p5 = new Place("P5");

        p4.setToken(new Token());

        Place jOuput = new Place("FJ1");

        JTransition j1 = new JTransition("J1");
        TTransition t1 = new TTransition("T1");
        FTransition f1 = new FTransition("F1");
        FIFOQueue fifo = new FIFOQueue("FIFO1");

        j1.setDelayFunction(new ConstDelayFunction(3));

        t1.setDelayFunction(new ConstDelayFunction(8));

        f1.setDelayFunction(new ConstDelayFunction(0));

        j1.addInputPlace(p1, 0);
        j1.addInputPlace(p4, 1);

        j1.setOutputPlace(jOuput);

        f1.setInputPlace(jOuput);

        f1.addOutputPlace(p2, 0);
        f1.addOutputPlace(p3, 0);

        t1.setInputPlace(p3);
        t1.setOutputPlace(p4);

        fifo.connectInputPlace(p2);
        fifo.connectOutputPlace(p5);

        InputImpl input = new InputImpl("I0");
        input.setOutputPlace(p1);

        A2.addInput(input, 0);

        A2.addPlace(p1);
        A2.addPlace(p2);
        A2.addPlace(p3);
        A2.addPlace(p4);
        A2.addPlace(jOuput);

        A2.addTransition(t1);
        A2.addTransition(f1);
        A2.addTransition(j1);
        A2.addQueue(fifo);

        PlaceStateChangedListener sysoutWriter = new PlaceListener1();

        p1.addStateChangeListener(sysoutWriter);
        p2.addStateChangeListener(sysoutWriter);
        p3.addStateChangeListener(sysoutWriter);
        p4.addStateChangeListener(sysoutWriter);
        jOuput.addStateChangeListener(sysoutWriter);

        return A2;
    }

    private static AggregateInstance createRootAgregate(AggregateInstance cm, AggregateInstance a2) {
        AggregateInstance rootAgregate = new AggregateInstance("RootAgregate") {
        };

        rootAgregate.addChildAggregate(cm);
        rootAgregate.addChildAggregate(a2);

        Output output = cm.getOutput("outToModel_I0");
        Input input = a2.getInput("I0");

        output.addInput(input, 0);

        return rootAgregate;
    }
}

class PlaceListener1 implements PlaceStateChangedListener {

    public void placeStateChanged(Place place) {
        System.out.println(place.getParentAggregate().getModel().getModelingTime()
                + " " + JessUtils.getAggregateChildFullName(place) + " state = " + place.getToken());
    }
}
