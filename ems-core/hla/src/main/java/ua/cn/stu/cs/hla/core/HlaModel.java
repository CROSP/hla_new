package ua.cn.stu.cs.hla.core;

import ua.cn.stu.cs.ems.core.experiment.ExperimentReport;

/**
 * 
 * @author Dmitry Krivenko <dmitrykrivenko@gmal.com>
 */
public interface HlaModel {

    void createModel(FederateExecution federateExecution);

    void startModel();
    
    ExperimentReport getReport();
}
