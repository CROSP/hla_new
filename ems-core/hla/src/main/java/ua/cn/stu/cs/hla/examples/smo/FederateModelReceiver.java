package ua.cn.stu.cs.hla.examples.smo;

import hla.rti.RTIexception;
import ua.cn.stu.cs.hla.core.FederateExecution;
import ua.cn.stu.cs.hla.core.HlaModel;
import ua.cn.stu.cs.hla.examples.ExampleFom;

/**
 * Represents the receiver (queue) of SMO.
 *
 * @author Dmitry Krivenko <dmitrykrivenko@gmal.com>
 */
public class FederateModelReceiver {

    public static void main(String[] args) {
        HlaModel modelReceiver = new ModelReceiver();
        FederateExecution federateExecution = new FederateExecution("FederationSmoExperiment", "FederateModelReceiver");
        federateExecution.setFomData(ExampleFom.createFomData());
        federateExecution.setWaitPressEnter(true);
        try {
            federateExecution.runModelFederate(modelReceiver);
        } catch (RTIexception ex) {
            System.out.println(ex);
        }
    }          
}
