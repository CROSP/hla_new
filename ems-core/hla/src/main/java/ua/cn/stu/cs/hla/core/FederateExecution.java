package ua.cn.stu.cs.hla.core;

import hla.rti.*;
import hla.rti.jlc.EncodingHelpers;
import hla.rti.jlc.RtiFactoryFactory;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import org.portico.impl.hla13.types.DoubleTime;
import org.portico.impl.hla13.types.DoubleTimeInterval;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Dmitrij999
 */
public class FederateExecution {

    private final static Logger logger = LoggerFactory.getLogger(FederateExecution.class);
    private static final String fomObjectClass = "objectClass";
    private static final String fomInteractionClass = "interactionClass";
    private ModelFederateAmbassador federateAmbassador = null;
    private RTIambassador rtiAmbassador = null;
    public static final String READY_TO_RUN = "ReadyToRun";
    public static final String READY_TO_START = "ReadyToStart";
    public static final String READY_TO_END = "ReadyToEnd";
    private volatile boolean readyToStart = false;
    private String federationName;
    private String federateName;
    private int objectHandle = -117;
    private boolean waitPressEnter = false;
    private boolean waitForOtherFederates = false;
    private boolean finished = false;
    private FomData fomData;

    public FederateExecution(String federationName, String federateName) {
        this.federationName = federationName;
        this.federateName = federateName;
    }

    public String getFederateName() {
        return federateName;
    }

    public ModelFederateAmbassador getFederateAmbassador() {
        return federateAmbassador;
    }

    public RTIambassador getRtiAmbassador() {
        return rtiAmbassador;
    }

    private void federatesWaiting() {
        waitForOtherFederates = true;
        if (!waitPressEnter) {
            logger.info("Waiting for others federates...");
            while (!readyToStart) {
                //waiting
            }
        } else {
            logger.info("Press Enter");
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            try {
                String s = reader.readLine();
            } catch (IOException e) {
                logger.error("IOException while waiting for user input:", e);
            }
        }
        waitForOtherFederates = false;
    }

    public void runModelFederate(HlaModel model) throws RTIexception {
        try {
            rtiAmbassador = RtiFactoryFactory.getRtiFactory().createRtiAmbassador();
        } catch (RTIinternalError e) {
            logger.error(e.getMessage());
        }

        federateAmbassador = new ModelFederateAmbassador();
        rtiAmbassador.joinFederationExecution(federateName, federationName, federateAmbassador);

        try {
            rtiAmbassador.registerFederationSynchronizationPoint(READY_TO_RUN, null);
            rtiAmbassador.registerFederationSynchronizationPoint(READY_TO_START, null);
            rtiAmbassador.registerFederationSynchronizationPoint(READY_TO_END, null);
        } catch (FederateNotExecutionMember e) {
            logger.error("FederateNotExecutionMember exception while running federate", e);
        }
        while (federateAmbassador.isAnnounced == false) {
            rtiAmbassador.tick();
        }

        federatesWaiting();

        try {
            rtiAmbassador.synchronizationPointAchieved(READY_TO_RUN);
        } catch (SynchronizationLabelNotAnnounced e) {
            logger.error("Synchronization label not announced", e);
        } catch (FederateNotExecutionMember e) {
            logger.error("Federate not executionMember", e);
        }
        logger.debug("Achieved sync point: " + READY_TO_RUN + ", waiting for federation...");
        while (federateAmbassador.isReadyToRun == false) {
            rtiAmbassador.tick();
        }

        //Before enabling time policy we should create model to know how much 
        //hlaInputs and hlaOuputs it has
        model.createModel(this);
        logger.debug("Model created");

        enableTimePolicy();
        logger.debug("Time Policy Enabled");

        rtiAmbassador.synchronizationPointAchieved(READY_TO_START);
        logger.debug("Achieved sync point: " + READY_TO_START + ", waiting for federation...");
        while (federateAmbassador.isReadyToStart == false) {
            rtiAmbassador.tick();
        }

        model.startModel();

        rtiAmbassador.synchronizationPointAchieved(READY_TO_END);
        logger.debug("Achieved sync point: " + READY_TO_END + ", waiting for federation...");
        while (federateAmbassador.isReadyToEnd == false) {
            rtiAmbassador.tick();
        }
        finished = true;
        logger.debug("Finished");
    }

    public void stopFederateExecution() {
        if (objectHandle != -117) {
            try {
                deleteObject(objectHandle);
            } catch (RTIexception ex) {
                logger.error("RTIexception while stopping (deleteObject) the federate: ", ex);
            }
        }
        try {
            rtiAmbassador.resignFederationExecution(ResignAction.NO_ACTION);
        } catch (RTIexception e) {
            logger.error("RTIexception while stopping (resignFederationExecution) the federate: ", e);
        }
        readyToStart = false;
    }

    public int registerObject(String className) {
        try {
            int classHandle = rtiAmbassador.getObjectClassHandle(String.format("HLAobjectRoot.%s", className));
            objectHandle = rtiAmbassador.registerObjectInstance(classHandle);
        } catch (RTIexception e) {
            logger.error("RTIexception while registering the object", e);
        }
        return objectHandle;
    }

    public void deleteObject(int handle) throws RTIexception {
        rtiAmbassador.deleteObjectInstance(handle, generateTag());
    }

    private AttributeHandleSet setAttributes(String classHandleName, int classHandle) throws RTIexception {
        List<String> attrs;
        attrs = getAttributes(fomObjectClass, classHandleName);
        AttributeHandleSet attributes = RtiFactoryFactory.getRtiFactory().createAttributeHandleSet();
        logger.debug("Set attributes");
        for (int i = 0; i < attrs.size(); i++) {
            logger.debug(attrs.get(i) + " " + rtiAmbassador.getAttributeHandle(attrs.get(i), classHandle));
            int attrHandle = rtiAmbassador.getAttributeHandle(attrs.get(i), classHandle);
            attributes.add(attrHandle);
        }
        return attributes;
    }

    public synchronized void publishMethod(String classHandleName) throws RTIexception {
        int classHandle = rtiAmbassador.getObjectClassHandle(String.format("HLAobjectRoot.%s", classHandleName));
        AttributeHandleSet attributes = setAttributes(classHandleName, classHandle);
        logger.debug("Publish attributes: " + attributes);
        rtiAmbassador.publishObjectClass(classHandle, attributes);
    }

    public synchronized void subscribeMethod(String classHandleName) throws RTIexception {
        int classHandle = rtiAmbassador.getObjectClassHandle(String.format("HLAobjectRoot.%s", classHandleName));
        AttributeHandleSet attributes = setAttributes(classHandleName, classHandle);
        //rtiAmbassador.publishObjectClass(classHandle, attributes);
        logger.debug("Subscribe attributes: " + attributes);
        rtiAmbassador.subscribeObjectClassAttributes(classHandle, attributes);
    }

    public synchronized void updateAttr(String classHandleName, int objectHandle, Attribute[] mas) throws RTIexception, hla.rti13.java1.RTIinternalError {
        SuppliedAttributes attributes = RtiFactoryFactory.getRtiFactory().createSuppliedAttributes();
        logger.debug("Update attributes of objectHandle: " + objectHandle);

        int classHandle = rtiAmbassador.getObjectClass(objectHandle);
        List<String> fomAttrs;

        fomAttrs = getAttributes(fomObjectClass, classHandleName);

        int j = 0;
        if (fomAttrs.size() < mas.length) {
            logger.error("Attributes amount in fom xml for current class less then amount of token attributes");
        } else {
            for (int i = 0; i < mas.length; i++) {
                Attribute attribute = mas[i];

                String fomAttr = fomAttrs.get(j++);

                int attrHandle = rtiAmbassador.getAttributeHandle(fomAttr, classHandle);

                logger.debug(attrHandle + " " + fomAttr + " ");

                String stringAttribute = String.format("%s, %d, %s, %s", attribute.getAttrName(), attribute.getAttrType(), attribute.getAttrValue().toString(), attribute.getOutputName().toString());
                byte[] byteAttribyte = EncodingHelpers.encodeString(stringAttribute);

                logger.debug(String.format("Attribute to update %s %s", fomAttr, stringAttribute));

                attributes.add(attrHandle, byteAttribyte);
            }
        }

        LogicalTime time = convertTime(federateAmbassador.federateTime + 1);
        rtiAmbassador.updateAttributeValues(objectHandle, attributes, generateTag(), time);
    }

    /**
     *
     * @throws RTIexception
     */
    private void sendInteraction() throws RTIexception {
        SuppliedParameters parameters = RtiFactoryFactory.getRtiFactory().createSuppliedParameters();

        byte[] xaValue = EncodingHelpers.encodeString("xa:" + getLbts());
        byte[] xbValue = EncodingHelpers.encodeString("xb:" + getLbts());

        // get the handles
        int classHandle = rtiAmbassador.getInteractionClassHandle("InteractionRoot.X");
        int xaHandle = rtiAmbassador.getParameterHandle("xa", classHandle);
        int xbHandle = rtiAmbassador.getParameterHandle("xb", classHandle);

        // put the values into the collection
        parameters.add(xaHandle, xaValue);
        parameters.add(xbHandle, xbValue);

        rtiAmbassador.sendInteraction(classHandle, parameters, generateTag());

        LogicalTime time = convertTime(federateAmbassador.federateTime + federateAmbassador.federateLookahead);
        rtiAmbassador.sendInteraction(classHandle, parameters, generateTag(), time);
    }

    private void enableTimePolicy() throws RTIexception {
        if (!federateAmbassador.getHlaOutputs().isEmpty()) {
            logger.debug("Enable time regulation");
            LogicalTime currentTime = convertTime(federateAmbassador.federateTime);
            LogicalTimeInterval lookahead = convertInterval(federateAmbassador.federateLookahead);

            this.rtiAmbassador.enableTimeRegulation(currentTime, lookahead);

            // tick until we get the callback
            while (federateAmbassador.isRegulating == false) {
                rtiAmbassador.tick();
            }
        }

        if (!federateAmbassador.getHlaInputs().isEmpty()) {
            logger.debug("Enable time constrained");
            this.rtiAmbassador.enableTimeConstrained();

            // tick until we get the callback
            while (federateAmbassador.isConstrained == false) {
                rtiAmbassador.tick();
            }
        }
    }

    /**
     * @param readyToStart the readyToStart to set
     */
    public void setReadyToStart(boolean readyToStart) {
        this.readyToStart = readyToStart;
    }

    protected static LogicalTime convertTime(double time) {
        // PORTICO SPECIFIC!!
        return new DoubleTime(time);
    }

    /**
     * Same as for {@link #convertTime(double)}
     */
    protected static LogicalTimeInterval convertInterval(double time) {
        return new DoubleTimeInterval(time);
    }

    private double getLbts() {
        return federateAmbassador.federateTime + federateAmbassador.federateLookahead;
    }

    private byte[] generateTag() {
        return ("" + System.currentTimeMillis()).getBytes();
    }

    @Override
    public String toString() {
        return getFederateName();
    }

    /**
     * @param waitPressEnter the waitPressEnter to set
     */
    public void setWaitPressEnter(boolean waitPressEnter) {
        this.waitPressEnter = waitPressEnter;
    }

    /**
     * @return the waitForOtherFederates
     */
    public boolean isWaitForOtherFederates() {
        return waitForOtherFederates;
    }

    /**
     * @return the finished
     */
    public boolean isFinished() {
        return finished;
    }

    /**
     *
     * @param fomData
     */
    public void setFomData(FomData fomData) {
        this.fomData = fomData;
    }

    private List<String> getAttributes(String elementName, String classHandleName) {
        if (elementName.equals(fomObjectClass)) {
            for (XmlInfo info : fomData.getClassData()) {
                if (info.getName().equals(classHandleName)) {
                    return info.getAttributes();
                }
            }
        }
        if (elementName.equals(fomInteractionClass)) {
            for (XmlInfo info : fomData.getInteractionData()) {
                if (info.getName().equals(classHandleName)) {
                    return info.getAttributes();
                }
            }
        }
        return null;
    }
}
