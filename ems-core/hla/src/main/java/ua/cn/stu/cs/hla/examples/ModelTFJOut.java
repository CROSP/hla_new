package ua.cn.stu.cs.hla.examples;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.PlaceStateChangedListener;
import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.aggregates.*;
import ua.cn.stu.cs.ems.core.transitions.FTransition;
import ua.cn.stu.cs.ems.core.transitions.JTransition;
import ua.cn.stu.cs.ems.core.transitions.TTransition;
import ua.cn.stu.cs.ems.core.transitions.Transition;
import ua.cn.stu.cs.ems.core.transitions.functions.ConstDelayFunction;
import ua.cn.stu.cs.ems.core.transitions.functions.TransformationFunction;
import ua.cn.stu.cs.ems.ecli.Value;
import ua.cn.stu.cs.hla.cm.CommunicateModule;
import ua.cn.stu.cs.hla.core.AbstractHlaModel;
import ua.cn.stu.cs.hla.core.FederateExecution;
import ua.cn.stu.cs.hla.core.HlaConstants;
import ua.cn.stu.cs.hla.core.HlaENetworksModel;

public class ModelTFJOut extends AbstractHlaModel {

    public void createModel(FederateExecution federateExecution) {
        AggregateInstance a1 = createAgregateA1();

        List<String> outputs = new ArrayList<String>();
        outputs.add("outToCm");

        AggregateInstance cmOut = CommunicateModule.getCommunicateModule(federateExecution, null, outputs);

        // set fom element for hlaOutput
        CommunicateModule.setFomElementToHlaOutput(cmOut, HlaConstants.HLA_OUTPUT_NAME_BEGIN + "outToCm", "Model1");

        // set attributes in which we are interesting to send, set null if all
        Set<String> userAttrs = new HashSet<String>();
        userAttrs.add("LongAttr");
        userAttrs.add("StringAttr");
        CommunicateModule.setUserAttributesToHlaOutput(cmOut, HlaConstants.HLA_OUTPUT_NAME_BEGIN + "outToCm", userAttrs);


        Aggregate rootAgregate = createRootAgregate(a1, cmOut);
        HlaENetworksModel model = new HlaENetworksModel(federateExecution.getFederateAmbassador(), federateExecution.getRtiAmbassador());
        rootAgregate.setModel(model);
        model.setRootAggregate(rootAgregate);

        model.setFinishTime(150L);

        setModel(model);
    }

    public void startModel() {
        getModel().start();
    }

    private static AggregateInstance createAgregateA1() {
        Place a1p1 = new Place("P1");
        Place a1p2 = new Place("P2");
        Place a1p3 = new Place("P3");
        Place a1p4 = new Place("P4");
        Place a1p5 = new Place("P5");

        Token token = new Token();

        Value longValue = new Value(2L);
        token.setValue("LongAttr", longValue);

        String s = "sss";
        Value stringValue = new Value(s);
        token.setValue("StringAttr", stringValue);

        Boolean b = true;
        Value boolenValue = new Value();
        boolenValue.setValue(b);
        token.setValue("BooleanAttr", boolenValue);

        Value doubleValue = new Value(2D);
        token.setValue("DoubleAttr", doubleValue);

        Value floatValue = new Value(1F);
        token.setValue("FloatAttr", floatValue);

        a1p4.setToken(token);

        TTransition a1t1 = new TTransition("T1");
        FTransition a1f1 = new FTransition("F1");
        JTransition a1j1 = new JTransition("J1");

        a1t1.setDelayFunction(new ConstDelayFunction(7));
        a1t1.setTransformationFunction(new TransformationFunction() {

            public Token transform(Transition transition, Token token) {
                Value val = token.getValue("LongAttr");
                Long i = (Long) val.getValue() + 1;
                val.setValue(i);
                token.setValue("LongAttr", val);

                return token;
            }

            public TransformationFunction copy() {
                return null;
            }
        });

        a1f1.setDelayFunction(new ConstDelayFunction(5));

        a1f1.setTransformationFunction(new TransformationFunction() {

            public Token transform(Transition transition, Token token) {

                Value val = token.getValue("LongAttr");
                Long i = (Long) val.getValue() + 2;
                val.setValue(i);
                token.setValue("LongAttr", val);

                return token;
            }

            public TransformationFunction copy() {
                return null;
            }
        });

        a1j1.setDelayFunction(new ConstDelayFunction(8));

        a1j1.setTransformationFunction(new TransformationFunction() {

            public Token transform(Transition transition, Token token) {

                Value val = token.getValue("LongAttr");
                Long i = (Long) val.getValue() + 3;
                val.setValue(i);
                token.setValue("LongAttr", val);

                return token;
            }

            public TransformationFunction copy() {
                return null;
            }
        });

        a1t1.setInputPlace(a1p4);
        a1t1.setOutputPlace(a1p1);

        a1f1.setInputPlace(a1p1);
        a1f1.addOutputPlace(a1p2, 0);
        a1f1.addOutputPlace(a1p3, 1);
        a1f1.addOutputPlace(a1p4, 2);

        a1j1.addInputPlace(a1p2, 0);

        a1j1.addInputPlace(a1p3, 0);

        a1j1.setOutputPlace(a1p5);

        // Aggregate A1 = new AbstractAggregate("A1") {};

        AggregateInstance A1 = new AggregateInstance("A1") {
        };

        A1.addPlace(a1p1);
        A1.addPlace(a1p2);
        A1.addPlace(a1p3);
        A1.addPlace(a1p4);
        A1.addPlace(a1p5);

        A1.addTransition(a1t1);
        A1.addTransition(a1f1);
        A1.addTransition(a1j1);

        PlaceStateChangedListener sysoutWriter = new PlaceListener1();

        a1p1.addStateChangeListener(sysoutWriter);
        a1p2.addStateChangeListener(sysoutWriter);
        a1p3.addStateChangeListener(sysoutWriter);
        a1p4.addStateChangeListener(sysoutWriter);
        a1p5.addStateChangeListener(sysoutWriter);

        Output output = new OutputImpl("outToCm");
        output.connectInputPlace(a1p5);

        A1.addOutput(output, 0);

        return A1;
    }

    private static Aggregate createRootAgregate(AggregateInstance a1, AggregateInstance cmOut) {

        AggregateInstance root = new AggregateInstance("root") {
        };

        root.addChildAggregate(a1);
        root.addChildAggregate(cmOut);

        Output output = a1.getOutput("outToCm");
        Input input = cmOut.getInput(HlaConstants.HLA_OUTPUT_NAME_BEGIN + "outToCm");

        output.addInput(input, 0);

        return root;
    }
}
