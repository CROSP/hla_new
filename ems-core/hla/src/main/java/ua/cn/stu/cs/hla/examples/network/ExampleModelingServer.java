package ua.cn.stu.cs.hla.examples.network;

import org.portico.utils.classpath.Classpath;
import ua.cn.stu.cs.hla.core.utils.HlaUtils;
import ua.cn.stu.cs.hla.network.AggregateProvider;
import ua.cn.stu.cs.hla.network.modelingserver.ModelingServer;

import java.net.URL;

/**
 * Example of modeling server {@link ModelingServer}.
 *
 * @author Dmitry Krivenko <dmitrykrivenko@gmal.com>
 */
public class ExampleModelingServer {

    public static void main(String[] args) {
        //Log.set(Log.LEVEL_NONE);
        //Need add path to portico.jar file
        //On ui level path should be get from properties file(jess.properties)
        HlaUtils.addToClasspath("file:///home/dimon/rtihome/lib/portico.jar");
        printClassPath();
        //Don't forget to set this property!!!
        System.setProperty("java.net.preferIPv4Stack", "true");

        AggregateProvider aggProvider = new ExampleAggregateProvider();

        ModelingServer server = new ModelingServer(null, aggProvider);

        server.start();
    }

    private static void printClassPath() {
        Classpath classpath = new Classpath();
        URL[] originalSystemPath = classpath.getUrlSearchPath();
        System.out.println("Classpath:");
        for (URL url : originalSystemPath) {
            System.out.println(url.getPath());
        }
    }
}
