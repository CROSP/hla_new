package ua.cn.stu.cs.hla.core;

import java.util.List;

/**
 *
 * @author Dmitry Krivenko <dmitrykrivenko@gmal.com>
 */
public class FomData {

    private List<XmlInfo> classData;
    private List<XmlInfo> interactionData;

    /**
     * @return the classData
     */
    public List<XmlInfo> getClassData() {
        return classData;
    }

    /**
     * @param classData the classData to set
     */
    public void setClassData(List<XmlInfo> classData) {
        this.classData = classData;
    }

    /**
     * @return the interactionData
     */
    public List<XmlInfo> getInteractionData() {
        return interactionData;
    }

    /**
     * @param interactionData the interactionData to set
     */
    public void setInteractionData(List<XmlInfo> interactionData) {
        this.interactionData = interactionData;
    }
}
