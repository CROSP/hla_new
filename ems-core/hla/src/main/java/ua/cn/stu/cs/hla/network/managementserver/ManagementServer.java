package ua.cn.stu.cs.hla.network.managementserver;

import com.esotericsoftware.kryonet.Client;
import java.net.InetAddress;
import java.security.AccessControlException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.cn.stu.cs.hla.network.Constants;
import ua.cn.stu.cs.hla.network.managementserver.requests.MgmntSRequest;

/**
 * Central management server.
 *
 * @author Dmitry Krivenko <dmitrykrivenko@gmal.com>
 */
public class ManagementServer {

    private static final Logger logger = LoggerFactory.getLogger(ManagementServer.class);

    static {
        try {
            //Clustering on Linux only works with IPv4. 
            //Therefore, when using a cluster under Linux, set the appropriate
            //property:
            System.setProperty("java.net.preferIPv4Stack", "true");
        } catch (AccessControlException ignored) {
            logger.warn("AccessControlException wlile setting java.net.preferIPv4Stack property", ignored);
        }
    }
    private Client disroverMSAdresses = new Client();
    private List<ModelingServerClient> msClients = new ArrayList<ModelingServerClient>();
    private final ResponsePool responsePool = new ResponsePool();

    /**
     * Send requests to modeling servers.
     *
     * @param request management server request
     */
    public void sendRequest(MgmntSRequest request) {
        for (ModelingServerClient mSClient : msClients) {
            request.initRequest(mSClient.getFederationName(), mSClient.getFederateName(), mSClient.getRootAggregateName(), mSClient.getAggregateName());
            mSClient.getClient().sendTCP(request);
        }
    }

    /**
     * Get addresses of all available modeling servers.
     *
     * @return
     */
    public Set<InetAddress> discoverModelingServers(int timeoutMillis) {
        List<InetAddress> resultList = disroverMSAdresses.discoverHosts(Constants.DEFAULT_SERVER_UDP_PORT, timeoutMillis);
        //remove duplicates if they exist
        return new HashSet<InetAddress>(resultList);
    }

    /**
     * Add new Set msClients to the list of msClients.
     *
     * @param msClient modeling server client
     */
    public void addMSClient(ModelingServerClient msClient) {
        if (msClient != null) {
            msClients.add(msClient);
        }
    }

    /**
     * Get list of msClients.
     *
     * @return the msClients
     */
    public List<ModelingServerClient> getMsClients() {
        return msClients;
    }

    /**
     * Set list of msClients.
     *
     * @param msClients the msClients to set
     */
    public void setMsClients(List<ModelingServerClient> msClients) {
        this.msClients = msClients;
    }

    /**
     * Stop the management server.
     */
    public void stop() {
        for (ModelingServerClient mSClient : msClients) {
            mSClient.getClient().stop();
        }
        msClients.clear();
    }

    /**
     * Get pool of responses, which send modeling servers.
     *
     * @return the responsePool
     */
    public ResponsePool getResponsePool() {
        return responsePool;
    }

    /**
     * Wait for responses from all modeling servers, to which we send requests.
     */
    public void waitForResponses() {
        logger.debug("Waiting for responses (" + msClients.size() + ")");
        while (true) {
            if (msClients.size() == responsePool.getResponsePoolList().size()) {
                logger.debug("ResponsePool list has filled");
                return;
            }
        }
    }
}
