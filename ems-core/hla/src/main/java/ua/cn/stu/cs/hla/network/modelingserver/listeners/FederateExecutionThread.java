package ua.cn.stu.cs.hla.network.modelingserver.listeners;

import hla.rti.RTIexception;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.cn.stu.cs.hla.core.FederateExecution;
import ua.cn.stu.cs.hla.core.HlaModel;

/**
 * Thread that executes the federate.
 *
 * @author Dmitry Krivenko <dmitrykrivenko@gmal.com>
 */
public class FederateExecutionThread implements Runnable {

    private static final Logger logger = LoggerFactory.getLogger(FederateExecutionThread.class);
    private FederateExecution federateExecution = null;
    private volatile Thread currentThread;
    private HlaModel model;

    public FederateExecutionThread(FederateExecution federateExecution, HlaModel model) {
        this.federateExecution = federateExecution;
        this.model = model;
    }

    public void start() {
        currentThread = new Thread(this);
        currentThread.setName(String.format("%s Federate Thread", federateExecution.getFederateName()));
        currentThread.start();
    }

    public void stop() {
        currentThread = null;
    }

    public void run() {
        try {
            federateExecution.runModelFederate(model);
        } catch (RTIexception ex) {
            logger.error("RTIexception while starting FederateExecutionThread", ex);
        }
    }
}
