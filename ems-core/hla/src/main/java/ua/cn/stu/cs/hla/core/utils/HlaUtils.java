package ua.cn.stu.cs.hla.core.utils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

/**
 *
 * @author Dmitry Krivenko <dmitrykrivenko@gmal.com>
 */
public final class HlaUtils {

    /**
     * Private default constructor.
     */
    private HlaUtils() {
        throw new UnsupportedOperationException(
                "This utility class is not intended to be instanciated!");
    }

    /**
     * Adds an URL to the classpath.
     *
     * @param url URL to add - Cannot be
     * <code>null</code>.
     */
    public static void addToClasspath(final String url) {
        checkNotNull("url", url);
        try {
            addToClasspath(new URL(url));
        } catch (final MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Adds an URL to the classpath.
     *
     * @param url URL to add - Cannot be
     * <code>null</code>.
     */
    public static void addToClasspath(final URL url) {
        checkNotNull("url", url);
        final ClassLoader classLoader = HlaUtils.class.getClassLoader();
        if (!(classLoader instanceof URLClassLoader)) {
            throw new IllegalArgumentException("Cannot add '" + url
                    + "' to classloader because it's not an URL classloader");
        }
        final URLClassLoader urlClassLoader = (URLClassLoader) classLoader;
        if (!containsURL(urlClassLoader.getURLs(), url)) {
            try {

                final Method addURL = URLClassLoader.class.getDeclaredMethod(
                        "addURL", new Class[]{URL.class});
                addURL.setAccessible(true);
                addURL.invoke(urlClassLoader, new Object[]{url});
            } catch (final NoSuchMethodException e) {
                throw new RuntimeException(e);
            } catch (final IllegalArgumentException e) {
                throw new RuntimeException(e);
            } catch (final IllegalAccessException e) {
                throw new RuntimeException(e);
            } catch (final InvocationTargetException e) {
                throw new RuntimeException(e);
            }
        }
    }

    /**
     * Checks if a variable is not
     * <code>null</code> and throws an
     * <code>IllegalArgumentException</code> if this rule is violated.
     *
     * @param name Name of the variable to be displayed in an error message.
     * @param value Value to check for
     * <code>null</code>.
     */
    public static void checkNotNull(final String name, final Object value) {
        if (value == null) {
            throw new IllegalArgumentException("The argument '" + name
                    + "' cannot be null!");
        }
    }

    /**
     * Checks if a variable is not
     * <code>empty</code> and throws an
     * <code>IllegalArgumentException</code> if this rule is violated. A String
     * with spaces is NOT considered empty!
     *
     * @param name Name of the variable to be displayed in an error message.
     * @param value Value to check for an empty String - Cannot be
     * <code>null</code>.
     */
    public static void checkNotEmpty(final String name, final String value) {
        if (value.length() == 0) {
            throw new IllegalArgumentException("The argument '" + name
                    + "' cannot be empty!");
        }
    }

    /**
     * Checks if the array or URLs contains the given URL.
     *
     * @param urls Array of URLs - Cannot be
     * <code>null</code>.
     * @param url URL to find - Cannot be
     * <code>null</code>.
     *
     * @return If the URL is in the array TRUE else FALSE.
     */
    public static boolean containsURL(final URL[] urls, final URL url) {
        checkNotNull("urls", urls);
        checkNotNull("url", url);
        for (int i = 0; i < urls.length; i++) {
            final URL element = urls[i];
            final String elementStr = element.toExternalForm();
            final String urlStr = url.toExternalForm();
            if (elementStr.equals(urlStr)) {
                return true;
            }
        }
        return false;
    }
}
