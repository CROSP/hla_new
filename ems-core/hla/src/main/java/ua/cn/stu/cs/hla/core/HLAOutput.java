package ua.cn.stu.cs.hla.core;

import hla.rti.RTIexception;
import hla.rti13.java1.RTIinternalError;
import java.util.List;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.cn.stu.cs.ems.core.AbstractActiveInstance;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.aggregates.Input;
import ua.cn.stu.cs.ems.core.aggregates.Output;
import ua.cn.stu.cs.ems.core.utils.AssertUtils;

/**
 *
 * @author Dmitrij999
 */
public class HLAOutput extends AbstractActiveInstance implements Input {

    private static final Logger logger = LoggerFactory.getLogger(HLAOutput.class);
    private String fomElementName;
    private FederateExecution federate;
    private int objectHandle;
    private Set<String> userTokenAttributes;
    private Place outputPlace;
    private Output connectedOutput;

    protected HLAOutput(String name) {
        super(name);
    }

    /**
     * Add necessary information and fields, decide with interaction or update.
     *
     * @param name name of Hla output
     * @param federateName name of federate, use it to get federate in which we
     * are interesting
     * @param userTokenAttributes set of attributes to send, if null or size =
     * 0, then we get and send all token attributes
     */
    public HLAOutput(String name, FederateExecution federateExecution, Set<String> userTokenAttributes) {
        super(name);
        this.userTokenAttributes = userTokenAttributes;
        federate = federateExecution;
    }

    public void setUserTokenAttributes(Set<String> userTokenAttributes) {
        this.userTokenAttributes = userTokenAttributes;
    }

    /**
     * @param fomElementName name of class or interaction in fom, use it to
     * publish for data and register to right object
     */
    public void setFomElement(String fomElementName) {
        this.fomElementName = fomElementName;
        initMethod();
    }

    private void initMethod() {
        try {
            federate.publishMethod(fomElementName);
        } catch (RTIexception e) {
            logger.error(e.getMessage());
        }
        objectHandle = federate.registerObject(fomElementName);
        federate.getFederateAmbassador().getHlaOutputs().add(this);
    }

    private Attribute[] setAttributes(Token token, Set<String> attrNames) {
        Attribute[] mas = new Attribute[attrNames.size()];
        int i = 0;
        for (String attrName : attrNames) {
            int attrType = TypeParser.getAttributeType(token.getValue(attrName).getValue());
            Object attrValue = token.getValue(attrName).getValue();
            int outputIndx = name.indexOf(HlaConstants.HLA_OUTPUT_NAME_BEGIN);
            String outputName = name.substring(outputIndx + HlaConstants.HLA_OUTPUT_NAME_BEGIN.length());
            Attribute attr = new Attribute(attrName, attrType, attrValue, outputName);
            mas[i] = attr;
            i++;
        }
        return mas;
    }

    public void setToken(Token token) {        
        logger.debug("Set token to the hla output");
        Set<String> attrNames = token.getAttributeNames();
        if (attrNames.isEmpty()) {
            logger.warn("There is no attributes in token");
        } else {
            Attribute[] mas;
            if (userTokenAttributes == null || userTokenAttributes.isEmpty()) {
                mas = setAttributes(token, attrNames);
            } else {
                logger.debug("UserTokenAttributes: " + userTokenAttributes);
                mas = setAttributes(token, userTokenAttributes);
            }

            try {
                logger.debug("objectHandle: " + objectHandle);
                federate.updateAttr(fomElementName, objectHandle, mas);
            } catch (RTIexception e) {
                logger.error(e.getMessage());
            } catch (RTIinternalError e) {
                logger.error(e.getMessage());
            }
        }
    }

    public List<Place> getInputPlaces() {
        return null;
    }

    public List<Place> getOutputPlaces() {
        return createListWithSinglePlace(outputPlace);
    }

    public void disconnectInputPlace(String name) {
    }

    public void disconnectOutputPlace(String name) {
        AssertUtils.assertNotNull(outputPlace, "No output place is connected");
        AssertUtils.assertTrue(outputPlace.getName().equals(name), "Output place's name is '" + outputPlace.getName() + "' imposible to delete output place with name " + name + "'");

        outputPlace = null;
    }

    public void fireIfPosible() {
    }

    public void placeStateChanged(Place place) {
        fireIfPosible();
    }

    public boolean canSetToken() {
        return true;
    }

    public void setInputPlace(Place place) {
    }

    public void setOutputPlace(Place place) {
        if (outputPlace != null) {
            outputPlace.setInputActiveInstance(null);
        }

        outputPlace = place;
        if (place != null) {
            place.setInputActiveInstance(this);
        }
    }

    public void connectOutput(Output output) {
        this.connectedOutput = output;
    }

    public Place getOutputPlace() {
        return outputPlace;
    }

    public Place getInputPlace() {
        return null;
    }

    public Output getConnectedOutput() {
        return connectedOutput;
    }

    @Override
    protected void fireTransition() {
        if (outputPlace == null) {
            return;
        }

        if (connectedOutput != null) {
            connectedOutput.inputCanBeSet(this);
        }
    }
}
