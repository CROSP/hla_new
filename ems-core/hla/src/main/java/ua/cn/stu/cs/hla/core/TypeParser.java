package ua.cn.stu.cs.hla.core;

import hla.rti13.java1.EncodingHelpers;
import hla.rti13.java1.FederateInternalError;
import hla.rti13.java1.RTIinternalError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Dmitrij999
 */
public class TypeParser {

    private final static Logger logger = LoggerFactory.getLogger(TypeParser.class);

    private TypeParser() {
    }
    public static final int stringType = 1;
    public static final int integerType = 2;
    public static final int longType = 3;
    public static final int doubleType = 4;
    public static final int floatType = 5;
    public static final int booleanType = 6;

    public static Integer getAttributeType(Object attr) {
        int attrType = 0;
        if (attr instanceof String) {
            attrType = stringType;
        } else if (attr instanceof Integer) {
            attrType = integerType;
        } else if (attr instanceof Long) {
            attrType = longType;
        } else if (attr instanceof Double) {
            attrType = doubleType;
        } else if (attr instanceof Float) {
            attrType = floatType;
        } else if (attr instanceof Boolean) {
            attrType = booleanType;
        }

        return attrType;
    }

    public static byte[] encodeAttribute(Object attr, int type) throws RTIinternalError {
        byte[] result = null;
        switch (type) {

            case stringType: {
                result = EncodingHelpers.encodeString((String) attr);
                break;
            }
            case integerType: {
                result = EncodingHelpers.encodeInt((Integer) attr);
                break;
            }
            case longType: {
                result = EncodingHelpers.encodeLong((Long) attr);
                break;
            }
            case doubleType: {
                result = EncodingHelpers.encodeDouble((Double) attr);
                break;
            }
            case floatType: {
                result = EncodingHelpers.encodeFloat((Float) attr);
                break;
            }
            case booleanType: {
                result = EncodingHelpers.encodeBoolean((Boolean) attr);
                break;
            }
            case 0: {
                logger.warn("Unknown type of attribute");
                break;
            }
        }
        return result;
    }

    public static Object decodeAttribute(String attr, int type) {
        Object obj = null;
        switch (type) {
            case stringType: {
                obj = attr;
                break;
            }
            case integerType: {
                obj = Integer.parseInt(attr);
                break;
            }
            case longType: {
                obj = Long.decode(attr);
                break;
            }
            case doubleType: {
                obj = Double.parseDouble(attr);
                break;
            }
            case floatType: {
                obj = Float.parseFloat(attr);
                break;
            }
            case booleanType: {
                obj = Boolean.parseBoolean(attr);
                break;
            }
            default:
                logger.warn("Unknown type of attribute");
                break;
        }
        return obj;
    }

    public static Object decodeAttribute(byte[] attr, int type) throws FederateInternalError {
        Object obj = null;
        switch (type) {
            case stringType: {
                obj = EncodingHelpers.decodeString(attr);
                break;
            }
            case integerType: {
                obj = EncodingHelpers.decodeInt(attr);
                break;
            }
            case longType: {
                obj = EncodingHelpers.decodeLong(attr);
                break;
            }
            case doubleType: {
                obj = EncodingHelpers.decodeDouble(attr);
                break;
            }
            case floatType: {
                obj = EncodingHelpers.decodeFloat(attr);
                break;
            }
            case booleanType: {
                obj = EncodingHelpers.decodeBoolean(attr);
                break;
            }
            default:
                logger.warn("Unknown type of attribute");
                break;
        }
        return obj;
    }
}
