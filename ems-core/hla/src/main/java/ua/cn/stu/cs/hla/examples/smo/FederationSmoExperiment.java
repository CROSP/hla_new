package ua.cn.stu.cs.hla.examples.smo;

import hla.rti.ConcurrentAccessAttempted;
import hla.rti.FederatesCurrentlyJoined;
import hla.rti.FederationExecutionDoesNotExist;
import hla.rti.RTIinternalError;
import ua.cn.stu.cs.hla.core.FederationExecution;
import ua.cn.stu.cs.hla.core.FomXmlParsing;

/**
 * Represents federation for SMO experiment.
 *
 * @author Dmitry Krivenko <dmitrykrivenko@gmal.com>
 */
public class FederationSmoExperiment {

    public static void main(String[] args) throws FederatesCurrentlyJoined, FederationExecutionDoesNotExist, RTIinternalError, ConcurrentAccessAttempted {
        FederationExecution federationExecution = new FederationExecution();
        federationExecution.createFederation("FederationSmoExperiment", FomXmlParsing.fomFileName);
        System.out.println("Creating the Federation");
    }
}
