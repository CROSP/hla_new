package ua.cn.stu.cs.hla.core;

import hla.rti.ConcurrentAccessAttempted;
import hla.rti.RTIambassador;
import hla.rti.RTIexception;
import hla.rti.RTIinternalError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.cn.stu.cs.ems.core.ActiveInstance;
import ua.cn.stu.cs.ems.core.models.ENetworksModel;
import ua.cn.stu.cs.ems.core.transitions.SpecialTransition;
import ua.cn.stu.cs.ems.core.utils.JessUtils;

/**
 * Model that should be used for distributed modeling.
 *
 * @see {@link ENetworksModel} model for non-distributed modeling
 *
 * @author Dmitry Krivenko <dmitrykrivenko@gmal.com>
 */
public class HlaENetworksModel extends ENetworksModel {

    private static final Logger logger = LoggerFactory.getLogger(HlaENetworksModel.class);
    private TimeRegulation timeRegulation;
    private ModelFederateAmbassador federateAmbassador;
    private RTIambassador rtiAmbassador;

    /**
     * Default constructor without parameters.
     */
    public HlaENetworksModel() {
        super();
    }

    /**
     * Convenience constructor with parameters.
     *
     * @param federateAmbassador
     * @param rtiAmbassador
     */
    public HlaENetworksModel(ModelFederateAmbassador federateAmbassador, RTIambassador rtiAmbassador) {
        super();
        timeRegulation = new TimeRegulation(federateAmbassador, rtiAmbassador);
        this.federateAmbassador = federateAmbassador;
        this.rtiAmbassador = rtiAmbassador;
    }

    /**
     * @return the timeRegulation
     */
    public TimeRegulation getTimeRegulation() {
        return timeRegulation;
    }

    /**
     * @param timeRegulation the timeRegulation to set
     */
    public void setTimeRegulation(TimeRegulation timeRegulation) {
        this.timeRegulation = timeRegulation;
    }

    @Override
    public void start(int delay) {
        if (timeRegulation == null) {
            logger.error("Can't start model. Please, set time regulation befor start.");
            return;
        }
        boolean shouldSetTokenToHlaInput = false;
        long time = System.currentTimeMillis();
        notifyModelStarted();
        setModelingTime(0);
        logger.debug("Begin  of model initializatioin:");
        initializeModel();
        logger.debug("End of model initializatioin");

        double prevModelingTime = -1;

        //If at the begining of modeling there are no instances in delayQueue.
        //if it is regulating federate it just advice time and -> increase federation LBTS;
        //if it is constrained federate it can fill toTokenQueue and after this delayQueue will not be empty;
        if (isStopConditions()) {
            try {
//                getTimeRegulation().canAdvanceToTime(getFinishTime() - getModelingTime(), getModelingTime(), getFinishTime(), false);
                 advanceTimeOptimized();
            } catch (RTIinternalError ex) {
                logger.error("RTIinternalError exception while time advancing before beginning of modeling cycle", ex);
            } catch (ConcurrentAccessAttempted ex) {
                logger.error("ConcurrentAccessAttempted exception while time advancing before beginning of modeling cycle", ex);
            } catch (RTIexception ex) {
                logger.error("RTIexception exception while time advancing before beginning of modeling cycle", ex);
            } catch (InterruptedException ex) {
                logger.error("InterruptedException exception while time advancing before beginning of modeling cycle", ex);
            }
            if (!federateAmbassador.toTokens.isEmpty()) {
                double newModelingTime = federateAmbassador.toTokens.getFirstTOTokenTime();
                prevModelingTime = setNewTimeAndTokens(prevModelingTime, newModelingTime);
            }
        }

        while (!isStopConditions() || !federateAmbassador.toTokens.isEmpty()) {
            if (!this.getDelayedQueue().isEmpty() && getSpecialTransitionSet().isEmpty() && !isInterrupted()) {
                double newModelingTime = getDelayedQueue().getFirstDelayableTime();
                double canAdviceTime = newModelingTime;
                try {
                    canAdviceTime = getTimeRegulation().canAdvanceToTime(newModelingTime - getModelingTime(), getModelingTime(), getFinishTime(), false);
                } catch (RTIinternalError ex) {
                    logger.error("RTIinternalError exception while time advancing in the beginning of modeling cycle", ex);
                } catch (ConcurrentAccessAttempted ex) {
                    logger.error("ConcurrentAccessAttempted exception while time advancing in the beginning of modeling cycle", ex);
                } catch (RTIexception ex) {
                    logger.error("RTIexception exception while time advancing in the beginning of modeling cycle", ex);
                } catch (InterruptedException ex) {
                    logger.error("InterruptedException exception while time advancing in the beginning of modeling cycle", ex);
                }

                if (canAdviceTime < newModelingTime) {
                    newModelingTime = canAdviceTime;
                    shouldSetTokenToHlaInput = true;
                }

                if (newModelingTime > getModelingTime()) {
                    if (getFinishTime() > 0 && newModelingTime > getFinishTime()) {
                        logger.debug("getFinishTime() > 0 && newModelingTime > getFinishTime()");
                        if (prevModelingTime != getFinishTime()) {
                            logger.debug("Modeling time {}:", getFinishTime());
                        }
                        if (federateAmbassador.federateTime <= getFinishTime()) {
                            try {
                                timeRegulation.advanceTime(getFinishTime() - getModelingTime() + 1);
                            } catch (RTIexception ex) {
                                logger.error("RTIexception exception while time advancing after end of modeling", ex);
                            } catch (InterruptedException ex) {
                                logger.error("InterruptedException exception while time advancing after end of modeling", ex);
                            }
                        }
                        //for saving dumped statistic
                        setModelingTime(getFinishTime());
                        break;
                    } else {
                        if (prevModelingTime != newModelingTime) {
                            logger.debug("Modeling time {}:", newModelingTime);
                            prevModelingTime = newModelingTime;
                        }
                        setModelingTime(newModelingTime);
                        if (shouldSetTokenToHlaInput && !federateAmbassador.toTokens.isEmpty()) {
                            if (newModelingTime == federateAmbassador.toTokens.getFirstTOTokenTime()) {
                                setTokensToHlaInputs();
                            }
                            shouldSetTokenToHlaInput = false;
                        }
                    }
                } else if (shouldSetTokenToHlaInput && !federateAmbassador.toTokens.isEmpty()) {
                    if (newModelingTime == federateAmbassador.toTokens.getFirstTOTokenTime()) {
                        setTokensToHlaInputs();
                    }
                }
            }

            while (moreDelayablesOfCurrentPriority() && !isInterrupted()) {
                ActiveInstance ai = getDelayedQueue().getFirstDelayedInstance();

                if (ai instanceof SpecialTransition) {
                    currentDelayedSpecialTransitions.remove((SpecialTransition) ai);
                }

                logger.debug("Activation of Object {}: ", JessUtils.getAggregateChildFullName(ai));

                ai.onDelayFinished();

                logger.debug("Object {} activated", JessUtils.getAggregateChildFullName(ai));
            }

            if (!getSpecialTransitionSet().isEmpty()) {
                int delayQueueSize = getDelayedQueue().getSize();
                while (getDelayedQueue().getSize() <= delayQueueSize
                        && !getSpecialTransitionSet().isEmpty()) {
                    SpecialTransition firstSpecialTransition = getSpecialTransition();

                    firstSpecialTransition.fireIfPosible();
                    removeSpecialTransition(firstSpecialTransition);
                }
            }

            //If regulating federate finished modeling before finishTime(there are no instances in delayQueue)
            //it just advance time and -> increase federation LBST.
            //If it is constrained federate and there no instances in delayQueue, we should check is toTokenQueue is empty.
            //If toTokenQueue is not empty delayQueue will be filled.
            if (isStopConditions()) {
                if (getModelingTime() < getFinishTime()) {
                    try {
//                        getTimeRegulation().canAdvanceToTime(getFinishTime() - getModelingTime(), getModelingTime(), getFinishTime(), true);
                         advanceTimeOptimized();
                    } catch (RTIinternalError ex) {
                        logger.error("RTIinternalError exception while time advancing in the end of modeling cycle", ex);
                    } catch (ConcurrentAccessAttempted ex) {
                        logger.error("ConcurrentAccessAttempted exception while time advancing in the end of modeling cycle", ex);
                    } catch (RTIexception ex) {
                        logger.error("RTIexception exception while time advancing in the end of modeling cycle", ex);
                    } catch (InterruptedException ex) {
                        logger.error("InterruptedException exception while time advancing in the end of modeling cycle", ex);
                    }
                    if (!federateAmbassador.toTokens.isEmpty()) {
                        double newModelingTime = federateAmbassador.toTokens.getFirstTOTokenTime();
                        if (newModelingTime <= getFinishTime()) {
                            prevModelingTime = setNewTimeAndTokens(prevModelingTime, newModelingTime);
                        }
                    }
                }
            }
        }
        notifyModelFinished();       
        logger.debug("Time execution: {}s", String.valueOf((float) (System.currentTimeMillis() - time) / 1000));
    }

    /**
     * Set new modeling time and tokens to hla inputs.
     *
     * @param prevModelingTime previous modeling time
     * @param newModelingTime new modeling time
     * @return new value of previous modeling time if it was changed, otherwise
     * - old value
     */
    private double setNewTimeAndTokens(double prevModelingTime, double newModelingTime) {
        setModelingTime(newModelingTime);
        if (prevModelingTime != newModelingTime) {
            logger.debug("Modeling time {}:", newModelingTime);
            prevModelingTime = newModelingTime;
        }
        setTokensToHlaInputs();
        return prevModelingTime;
    }

    /**
     * Set tokens to hla inputs from {@link TOTokenQueue}.
     */
    private void setTokensToHlaInputs() {
        TOToken toToken = federateAmbassador.toTokens.getFirstTOToken();
        int theClassHandle = 0;
        try {
            theClassHandle = rtiAmbassador.getObjectClass(toToken.getObjectHandle());
        } catch (RTIexception e) {
            logger.error(e.getMessage());
        }
        for (HLAInput input : federateAmbassador.getHlaInputs()) {
            if (theClassHandle == input.getClassHandle() && toToken.getOutputName().equals(input.getSubscribeOutput())) {
                input.setToken(toToken.getToken());
            }
        }
    }

    /**
     * Time advancing that can be used before beginning of modeling and in the
     * end of modeling cycle.
     *
     * @throws RTIinternalError
     * @throws ConcurrentAccessAttempted
     * @throws RTIexception
     * @throws InterruptedException
     */
    private void advanceTimeOptimized() throws RTIinternalError, ConcurrentAccessAttempted, RTIexception, InterruptedException {
       // int i = 0;
        //OR (federateAmbassador.federateTime <= getFinishTime())?!
        while (federateAmbassador.federateTime < getFinishTime()) {
            getTimeRegulation().canAdvanceToTime(1, getModelingTime(), getFinishTime(), true);
            if (!federateAmbassador.toTokens.isEmpty()) {
                break;
            }
            double newModelingTime = getModelingTime() + 1;
            if (newModelingTime < getFinishTime()) {
                setModelingTime(newModelingTime);
            }
        }
    }
}
