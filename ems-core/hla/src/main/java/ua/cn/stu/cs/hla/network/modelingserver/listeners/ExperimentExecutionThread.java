package ua.cn.stu.cs.hla.network.modelingserver.listeners;

import com.esotericsoftware.kryonet.Connection;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.cn.stu.cs.ems.core.experiment.ExperimentReport;
import ua.cn.stu.cs.ems.core.experiment.PrimaryStatisticsResult;
import ua.cn.stu.cs.hla.core.FederateExecution;
import ua.cn.stu.cs.hla.core.FomData;
import ua.cn.stu.cs.hla.core.HlaModel;
import ua.cn.stu.cs.hla.core.HlaModelImpl;
import ua.cn.stu.cs.hla.network.modelingserver.responses.ReadyToStartResponse;
import ua.cn.stu.cs.hla.network.modelingserver.responses.ReportResponse;

/**
 *
 * @author Dmitry Krivenko <dmitrykrivenko@gmal.com>
 */
public class ExperimentExecutionThread implements Runnable {

    private static final Logger logger = LoggerFactory.getLogger(ExperimentExecutionThread.class);
    private volatile Thread currentThread;
    private ExperimentParamsStorage eps;
    private String federationName;
    private String federateName;
    private Connection connection;
    private volatile FederateExecution federateExecution;
    private volatile boolean canStart = false;
    private FomData fomData;

    /**
     * Default constructor without parameters.
     */
    public ExperimentExecutionThread() {
    }

    /**
     * Convenience constructor with parameters.
     *
     * @param federationName
     * @param federateName
     * @param esp
     * @param connection
     */
    public ExperimentExecutionThread(String federationName, String federateName, FomData fomData, ExperimentParamsStorage esp, Connection connection) {
        this.federationName = federationName;
        this.federateName = federateName;
        this.fomData = fomData;
        this.eps = esp;
        this.connection = connection;
    }

    public void start() {
        currentThread = new Thread(this);
        currentThread.start();
    }

    public void stop() {
        currentThread = null;
    }

    public void run() {
        HlaModel model = new HlaModelImpl(eps);
        federateExecution = new FederateExecution(federationName, federateName);
        federateExecution.setFomData(fomData);
        FederateExecutionThread fethd = new FederateExecutionThread(federateExecution, model);
        //run in the new thread
        fethd.start();

        logger.debug("FederateExecutionThread has started");
        while (!federateExecution.isWaitForOtherFederates()) {
        }

        //federate has run and is waiting for other federates
        connection.sendTCP(new ReadyToStartResponse());
        logger.debug("Send ReadyToStartResponse");

        waitForCanStart();

        waitForFinish();
        //stop federate
        federateExecution.stopFederateExecution();

        ReportResponse response = new ReportResponse();

        ExperimentReport report = model.getReport();
        for (String objectName : report.getStatisticsObjectsNames()) {
            List<PrimaryStatisticsResult> psr = report.getPrimaryStatisticsFor(objectName);
            response.objectName = objectName;
            response.report = psr;
            //send report to the management server
            connection.sendTCP(response);
        }

        //send last report to the management server
        response.objectName = null;
        response.report = null;
        response.last = true;
        connection.sendTCP(response);
        logger.debug("Send ReportResponse");
    }

    private void waitForCanStart() {
        while (!canStart) {
        }
    }

    private void waitForFinish() {
        while (!federateExecution.isFinished()) {
        }
    }

    /**
     * @param canStart the canStart to set
     */
    public void setCanStart(boolean canStart) {
        federateExecution.setReadyToStart(canStart);
        this.canStart = canStart;
    }
}
