
package ua.cn.stu.cs.ems;

import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.PlaceStateChangedListener;
import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.aggregates.Aggregate;
import ua.cn.stu.cs.ems.core.aggregates.AggregateInstance;
import ua.cn.stu.cs.ems.core.aggregates.Input;
import ua.cn.stu.cs.ems.core.aggregates.InputImpl;
import ua.cn.stu.cs.ems.core.aggregates.Output;
import ua.cn.stu.cs.ems.core.aggregates.OutputImpl;
import ua.cn.stu.cs.ems.core.models.ENetworksModel;
import ua.cn.stu.cs.ems.core.transitions.FTransition;
import ua.cn.stu.cs.ems.core.transitions.JTransition;
import ua.cn.stu.cs.ems.core.transitions.TTransition;
import ua.cn.stu.cs.ems.core.transitions.Transition;
import ua.cn.stu.cs.ems.core.transitions.XTransition;
import ua.cn.stu.cs.ems.core.transitions.YTransition;
import ua.cn.stu.cs.ems.core.transitions.functions.ConstDelayFunction;
import ua.cn.stu.cs.ems.core.transitions.functions.DelayFunction;
import ua.cn.stu.cs.ems.core.utils.JessUtils;

/**
 *
 * @author proger
 */
public class XYAModel {
    public static void main(String... agrv) {
        System.out.println("X,Y prototype model");
        System.out.println("<modeling time> <place-name> <place's token>");

        AggregateInstance A1 = createA1();
        AggregateInstance A2 = createA2();
        AggregateInstance A3 = createA3();

        AggregateInstance rootAgregate = createRootAgregate();
        connectAgregates(rootAgregate, A1, A2, A3);

        ENetworksModel model = new ENetworksModel();
        model.setRootAggregate(rootAgregate);

        model.setFinishTime(200D);
        model.start();
    }

    private static AggregateInstance createA1() {

        AggregateInstance A1 = new AggregateInstance("A1");

        Output outP5 = new OutputImpl("outP5");
        Output outP8 = new OutputImpl("outP8");

        XTransition X1 = new XTransition("X1"); X1.setDelayFunction(constDelayFunction);
        TTransition T1 = new TTransition("T1"); T1.setDelayFunction(constDelayFunction);
        FTransition F1 = new FTransition("F1"); F1.setDelayFunction(constDelayFunction);
        TTransition T2 = new TTransition("T2"); T2.setDelayFunction(constDelayFunction);
        YTransition Y1 = new YTransition("Y1"); Y1.setDelayFunction(constDelayFunction);

        Place P1 = new Place("P1");
        Place P2 = new Place("P2");
        Place P3 = new Place("P3");
        Place P4 = new Place("P4");
        Place P5 = new Place("P5");
        Place P6 = new Place("P6");
        Place P7 = new Place("P7");
        Place P8 = new Place("P8");

        X1.setInputPlace(P1);

        X1.addOutputPlace(P2, 0);
        X1.addOutputPlace(P3, 1);
        X1.addOutputPlace(P4, 2);

        T1.setInputPlace(P4);
        T1.setOutputPlace(P1);

        F1.setInputPlace(P2);
        F1.addOutputPlace(P5, 0);
        F1.addOutputPlace(P6, 0);

        T2.setInputPlace(P6);
        T2.setOutputPlace(P7);

        Y1.addInputPlace(P7, 0);
        Y1.addInputPlace(P3, 1);

        Y1.connectOutputPlace(P8);

        outP5.connectInputPlace(P5);
        outP8.connectInputPlace(P8);

        A1.addOutput(outP5, 0);
        A1.addOutput(outP8, 0);

        addPlacesToAgregate(A1, P1, P2, P3, P4, P5, P6, P7, P8);
        addTransitionsToAgregate(A1, T1, X1, Y1, F1, T2);

        P4.setToken(new Token());

        return A1;
    }

    private static AggregateInstance createA2() {
        Input inP1 = new InputImpl("inP1");
        Output outP4 = new OutputImpl("outP4");

        Place P1 = new Place("P1");
        Place P2 = new Place("P2");
        Place P3 = new Place("P3");
        Place P4 = new Place("P4");

        FTransition F1 = new FTransition("F1"); F1.setDelayFunction(constDelayFunction);
        JTransition J1 = new JTransition("J1"); J1.setDelayFunction(constDelayFunction);

        AggregateInstance A2 = new AggregateInstance("A2");

        inP1.setOutputPlace(P1);

        F1.setInputPlace(P1);

        F1.addOutputPlace(P2, 0);
        F1.addOutputPlace(P3, 1);

        J1.addInputPlace(P2, 0);
        J1.addInputPlace(P3, 0);

        J1.setOutputPlace(P4);

        outP4.connectInputPlace(P4);

        A2.addInput(inP1, 0);
        A2.addOutput(outP4, 0);

        addPlacesToAgregate(A2, P1, P2, P3, P4);
        addTransitionsToAgregate(A2, F1, J1);

        return A2;
    }

    private static AggregateInstance createA3() {
        AggregateInstance A3 = new AggregateInstance("A3");

        Place P1 = new Place("P1");
        Place P2 = new Place("P2");
        Place P3 = new Place("P3");
        Place P4 = new Place("P4");
        Place P5 = new Place("P5");
        Place P6 = new Place("P6");
        Place P7 = new Place("P7");
        Place P8 = new Place("P8");
        Place P9 = new Place("P9");
        Place P10 = new Place("P10");

        TTransition T1 = new TTransition("T1"); T1.setDelayFunction(constDelayFunction);
        TTransition T2 = new TTransition("T2"); T2.setDelayFunction(constDelayFunction);
        TTransition T3 = new TTransition("T3"); T3.setDelayFunction(constDelayFunction);
        TTransition T4 = new TTransition("T4"); T4.setDelayFunction(constDelayFunction);
        TTransition T5 = new TTransition("T5"); T5.setDelayFunction(constDelayFunction);
        FTransition F1 = new FTransition("F1"); F1.setDelayFunction(constDelayFunction);
        YTransition Y1 = new YTransition("Y1"); Y1.setDelayFunction(constDelayFunction);

        Input inP1 = new InputImpl("inP1");
        Input inP2 = new InputImpl("inP2");
        
        T1.setInputPlace(P1);
        T1.setOutputPlace(P3);
        
        F1.setInputPlace(P2);
        F1.addOutputPlace(P4, 0);
        F1.addOutputPlace(P5, 1);

        T2.setInputPlace(P4);
        T2.setOutputPlace(P6);

        T4.setInputPlace(P6);
        T4.setOutputPlace(P8);

        T3.setInputPlace(P5);
        T3.setOutputPlace(P7);

        T5.setInputPlace(P10);
        T5.setOutputPlace(P9);

        Y1.connectOutputPlace(P10);
        Y1.addInputPlace(P3, 0);
        Y1.addInputPlace(P8, 1);
        Y1.addInputPlace(P7, 2);
        Y1.addInputPlace(P9, 3);

        inP1.setOutputPlace(P1);
        inP2.setOutputPlace(P2);

        A3.addInput(inP1, 0);
        A3.addInput(inP2, 0);

        addPlacesToAgregate(A3, P1, P2, P3, P4, P5, P6, P7, P8, P9, P10);
        addTransitionsToAgregate(A3, T1, T2, T3, T4, T5, F1, Y1);

        P9.setToken(new Token());

        Y1.setDelayFunction(new ConstDelayFunction(10));

        return A3;
    }

     private static AggregateInstance createRootAgregate() {
        return new AggregateInstance("rootAgregate");
    }

    private static void connectAgregates(AggregateInstance rootAgregate, AggregateInstance A1, AggregateInstance A2, AggregateInstance A3) {
        addAgregatesToAgregate(rootAgregate, A1, A2, A3);

        A1.getOutput("outP5").addInput(A2.getInput("inP1"), 0);
        A1.getOutput("outP8").addInput(A3.getInput("inP2"), 0);

        A2.getOutput("outP4").addInput(A3.getInput("inP1"), 0);


    }

    private static void addPlacesToAgregate(Aggregate a, Place... places) {
        for (Place p : places) {
            a.addPlace(p);
            p.addStateChangeListener(placeListener);
        }
    }

    private static void addTransitionsToAgregate(Aggregate a, Transition... transitions) {
        for (Transition t : transitions) {
            a.addTransition(t);
        }
    }

    private static void addAgregatesToAgregate(AggregateInstance parent, AggregateInstance... childs) {
        for (AggregateInstance child : childs) {
            parent.addChildAggregate(child);
        }
    }

    private static DelayFunction constDelayFunction = new ConstDelayFunction(1);

    private static PlaceListener placeListener = new PlaceListener();

    private static class PlaceListener implements PlaceStateChangedListener {

    public void placeStateChanged(Place place) {
        if (place.getParentAggregate() != null && place.getParentAggregate().getModel() != null)
            System.out.println(place.getParentAggregate().getModel().getModelingTime()
                + " " + JessUtils.getAggregateChildFullName(place) + " state = " + place.getToken());
        }
    }


}



