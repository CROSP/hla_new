package ua.cn.stu.cs.ems.experiments;

import java.util.List;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.PlaceStateChangedListener;
import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.aggregates.Aggregate;
import ua.cn.stu.cs.ems.core.aggregates.AggregateInstance;
import ua.cn.stu.cs.ems.core.experiment.CountDownExperiment;
import ua.cn.stu.cs.ems.core.experiment.ExperimentReport;
import ua.cn.stu.cs.ems.core.experiment.PrimaryStatisticsResult;
import ua.cn.stu.cs.ems.core.models.ENetworksModel;
import ua.cn.stu.cs.ems.core.transitions.TTransition;
import ua.cn.stu.cs.ems.core.transitions.functions.ConstDelayFunction;

/**
 *
 * @author proger
 */
public class ExperimentPrototype {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        TTransition t1 = new TTransition("T1");
        TTransition t2 = new TTransition("T2");
        TTransition t3 = new TTransition("T3");

        t2.setDelayFunction(new ConstDelayFunction(5));
        t3.setDelayFunction(new ConstDelayFunction(5));

        Place p1 = new Place("p1");
        Place p2 = new Place("p2");
        Place p3 = new Place("p3");
        Place p4 = new Place("p4");


        p1.setToken(new Token());

        t1.setInputPlace(p1);
        t1.setOutputPlace(p2);

        t2.setInputPlace(p2);
        t2.setOutputPlace(p3);

        t3.setInputPlace(p3);
        t3.setOutputPlace(p4);

        Aggregate root = createAggregateInstance();
        root.addPlaces(p1, p2, p3, p4);
        root.addTransitions(t1, t2, t3);

        final ENetworksModel model = new ENetworksModel();

        model.setRootAggregate(root);

        p2.addStateChangeListener(new PlaceStateChangedListener() {

            public void placeStateChanged(Place place) {
                log(model.getModelingTime() + " " + place.getToken());
            }
        });

        CountDownExperiment cde = new CountDownExperiment(model, 1);

        cde.setCollectedStatOnEvent(true);
        cde.collectStatisticsOn(p2);
        cde.setDelayBeforeCollectingPrimaryStatistics(0);
        //cde.setPrimaryStatisticsDumpPeriod(10);
        cde.setIsPrimaryStatisticsShouldBeCollected(true);

        model.setFinishTime(10D);
        cde.startExperiment();

        ExperimentReport report = cde.getReport();

        List<PrimaryStatisticsResult> psrList = report.getPrimaryStatisticsFor("A.p2");

        for (PrimaryStatisticsResult pse : psrList) {
            log(pse);
            log(pse.getDumpedPrimaryStatisticsElements());
        }
    }

    private static AggregateInstance createAggregateInstance() {
        return new AggregateInstance("A");
    }

    private static void log(Object o) {
        System.out.println(o);
    }
}
