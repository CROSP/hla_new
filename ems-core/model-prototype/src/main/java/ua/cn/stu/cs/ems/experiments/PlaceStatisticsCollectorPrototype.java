package ua.cn.stu.cs.ems.experiments;

import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.PlaceStateChangedListener;
import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.aggregates.Aggregate;
import ua.cn.stu.cs.ems.core.aggregates.AggregateInstance;
import ua.cn.stu.cs.ems.core.experiment.statistics.PlaceStatisticsCollector;
import ua.cn.stu.cs.ems.core.models.ENetworksModel;
import ua.cn.stu.cs.ems.core.transitions.TTransition;
import ua.cn.stu.cs.ems.core.transitions.functions.ConstDelayFunction;

/**
 *
 * @author proger
 */
public class PlaceStatisticsCollectorPrototype {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        TTransition t1 = new TTransition("T1");
        TTransition t2 = new TTransition("T2");

        t2.setDelayFunction(new ConstDelayFunction(5));

        Place p1 = new Place("p1");
        Place p2 = new Place("p2");
        Place p3 = new Place("p3");


        p1.setToken(new Token());

        t1.setInputPlace(p1);
        t1.setOutputPlace(p2);

        t2.setInputPlace(p2);
        t2.setOutputPlace(p3);

        Aggregate root = createAggregateInstance();
        root.addPlaces(p1, p2, p3);
        root.addTransitions(t1, t2);

        final ENetworksModel model = new ENetworksModel();

        model.setRootAggregate(root);

        p2.addStateChangeListener(new PlaceStateChangedListener() {

            public void placeStateChanged(Place place) {
                log(model.getModelingTime() + " " + place.getToken());
            }
        });

        PlaceStatisticsCollector psc = new PlaceStatisticsCollector(model, p2);
        psc.startStatCollection();

        model.setFinishTime(10D);
        model.start();

        log(psc.getResult());
    }

    private static AggregateInstance createAggregateInstance() {
        return new AggregateInstance("A");
    }

    private static void log(Object o) {
        System.out.println(o);
    }
}
