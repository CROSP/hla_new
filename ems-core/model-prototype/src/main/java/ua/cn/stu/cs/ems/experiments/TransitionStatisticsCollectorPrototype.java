package ua.cn.stu.cs.ems.experiments;

import java.util.List;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.aggregates.Aggregate;
import ua.cn.stu.cs.ems.core.aggregates.AggregateInstance;
import ua.cn.stu.cs.ems.core.experiment.CountDownExperiment;
import ua.cn.stu.cs.ems.core.experiment.ExperimentReport;
import ua.cn.stu.cs.ems.core.experiment.PrimaryStatisticsElement;
import ua.cn.stu.cs.ems.core.models.ENetworksModel;
import ua.cn.stu.cs.ems.core.transitions.TTransition;
import ua.cn.stu.cs.ems.core.transitions.functions.ConstDelayFunction;

/**
 *
 * @author proger
 */
public class TransitionStatisticsCollectorPrototype {

    public static void main(String... args) {
        Place p1 = new Place("P1");
        Place p2 = new Place("P2");
        Place p3 = new Place("P3");

        TTransition t = new TTransition("T");
        t.setDelayFunction(new ConstDelayFunction(15));

        TTransition t2 = new TTransition("T2");
        t2.setDelayFunction(new ConstDelayFunction(15));

        p1.setToken(new Token());

        t.setInputPlace(p1);
        t.setOutputPlace(p2);

        t2.setInputPlace(p2);
        t2.setOutputPlace(p3);

        Aggregate aggregate = createAggregateInstance();
        aggregate.addPlaces(p1, p2);
        aggregate.addTransitions(t, t2);

        ENetworksModel model = new ENetworksModel();
        model.setFinishTime(100D);
        model.setRootAggregate(aggregate);

        CountDownExperiment cde = new CountDownExperiment(model, 1);

        cde.collectStatisticsOn(p1);
        cde.setIsPrimaryStatisticsShouldBeCollected(true);
        cde.setDelayBeforeCollectingPrimaryStatistics(0);
        cde.setPrimaryStatisticsDumpPeriod(10);

        cde.startExperiment();

        ExperimentReport report = cde.getReport();

        List<PrimaryStatisticsElement> pseList = report.getPrimaryStatisticsFor("A.P1").get(0).getDumpedPrimaryStatisticsElements();

        for (PrimaryStatisticsElement pse : pseList) {
            log(pse);
        }

    }

    private static AggregateInstance createAggregateInstance() {
        return new AggregateInstance("A");
    }

    private static void log(Object o) {
        System.out.println(o);
    }
}
