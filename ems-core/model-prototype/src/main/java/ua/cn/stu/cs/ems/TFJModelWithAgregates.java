package ua.cn.stu.cs.ems;

import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.PlaceStateChangedListener;
import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.aggregates.AggregateInstance;
import ua.cn.stu.cs.ems.core.aggregates.Input;
import ua.cn.stu.cs.ems.core.aggregates.InputImpl;
import ua.cn.stu.cs.ems.core.aggregates.Output;
import ua.cn.stu.cs.ems.core.aggregates.OutputImpl;
import ua.cn.stu.cs.ems.core.models.ENetworksModel;
import ua.cn.stu.cs.ems.core.transitions.FTransition;
import ua.cn.stu.cs.ems.core.transitions.JTransition;
import ua.cn.stu.cs.ems.core.transitions.TTransition;
import ua.cn.stu.cs.ems.core.transitions.functions.ConstDelayFunction;
import ua.cn.stu.cs.ems.core.utils.JessUtils;

/**
 *
 * @author proger
 */
public class TFJModelWithAgregates {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        AggregateInstance a1 = createAgregateA1();
        AggregateInstance a2 = createAgregateA2();

        AggregateInstance rootAgregate = createRootAgregate(a1, a2);
        ENetworksModel model = new ENetworksModel();
        rootAgregate.setModel(model);
        model.setRootAggregate(rootAgregate);

        model.setFinishTime(200D);
        model.start();
    }

    private static AggregateInstance createAgregateA1() {
        Place a1p1 = new Place("P1");
        Place a1p2 = new Place("P2");
        Place a1p3 = new Place("P3");
        Place a1p4 = new Place("P4");
        Place a1p5 = new Place("P5");

        Token token = new Token();
        a1p4.setToken(token);

        TTransition a1t1 = new TTransition("T1");
        FTransition a1f1 = new FTransition("F1");
        JTransition a1j1 = new JTransition("J1");

        a1t1.setDelayFunction(new ConstDelayFunction(4));

        a1f1.setDelayFunction(new ConstDelayFunction(5));

        a1j1.setDelayFunction(new ConstDelayFunction(3));


        a1t1.setInputPlace(a1p4);
        a1t1.setOutputPlace(a1p1);

        a1f1.setInputPlace(a1p1);
        a1f1.addOutputPlace(a1p2, 0);
        a1f1.addOutputPlace(a1p3, 1);
        a1f1.addOutputPlace(a1p4, 2);

        a1j1.addInputPlace(a1p2, 0);

        a1j1.addInputPlace(a1p3, 0);

        a1j1.setOutputPlace(a1p5);

        AggregateInstance A1 = new AggregateInstance("A1") {};

        A1.addPlace(a1p1);
        A1.addPlace(a1p2);
        A1.addPlace(a1p3);
        A1.addPlace(a1p4);
        A1.addPlace(a1p5);

        A1.addTransition(a1t1);
        A1.addTransition(a1f1);
        A1.addTransition(a1j1);

        PlaceStateChangedListener sysoutWriter = new PlaceListener1();

        a1p1.addStateChangeListener(sysoutWriter);
        a1p2.addStateChangeListener(sysoutWriter);
        a1p3.addStateChangeListener(sysoutWriter);
        a1p4.addStateChangeListener(sysoutWriter);
        a1p5.addStateChangeListener(sysoutWriter);

        Output output = new OutputImpl("outP5");        
        output.connectInputPlace(a1p5);

        A1.addOutput(output, 0);
        return A1;
    }

    private static AggregateInstance createAgregateA2() {
        AggregateInstance A2 = new AggregateInstance("A2") {};

        Place p1 = new Place("P1");
        Place p2 = new Place("P2");
        Place p3 = new Place("P3");
        Place p4 = new Place("P4");

        p4.setToken(new Token());

        Place jOuput = new Place("FJ1");

        JTransition j1 = new JTransition("J1");
        TTransition t1 = new TTransition("T1");
        FTransition f1 = new FTransition("F1");

        j1.setDelayFunction(new ConstDelayFunction(3));

        t1.setDelayFunction(new ConstDelayFunction(8));

        f1.setDelayFunction(new ConstDelayFunction(6));

        j1.addInputPlace(p1, 0);
        j1.addInputPlace(p4, 1);

        j1.setOutputPlace(jOuput);

        f1.setInputPlace(jOuput);

        f1.addOutputPlace(p2, 0);
        f1.addOutputPlace(p3, 1);

        t1.setInputPlace(p3);
        t1.setOutputPlace(p4);

        InputImpl input = new InputImpl("inP1");
        input.setOutputPlace(p1);

        A2.addInput(input, 0);

        A2.addPlace(p1);
        A2.addPlace(p2);
        A2.addPlace(p3);
        A2.addPlace(p4);
        A2.addPlace(jOuput);
        
        A2.addTransition(t1);
        A2.addTransition(f1);
        A2.addTransition(j1);

        PlaceStateChangedListener sysoutWriter = new PlaceListener1();

        p1.addStateChangeListener(sysoutWriter);
        p2.addStateChangeListener(sysoutWriter);
        p3.addStateChangeListener(sysoutWriter);
        p4.addStateChangeListener(sysoutWriter);
        jOuput.addStateChangeListener(sysoutWriter);

        return A2;
    }

    private static AggregateInstance createRootAgregate(AggregateInstance a1, AggregateInstance a2) {
        AggregateInstance rootAgregate = new AggregateInstance("RootAgregate") {};

        rootAgregate.addChildAggregate(a1);
        rootAgregate.addChildAggregate(a2);

        Output output = a1.getOutput("outP5");
        Input input = a2.getInput("inP1");

        output.addInput(input, 0);

        return rootAgregate;
    }

}

class PlaceListener1 implements PlaceStateChangedListener {

    public void placeStateChanged(Place place) {
        System.out.println(place.getParentAggregate().getModel().getModelingTime()
                + " " + JessUtils.getAggregateChildFullName(place) + " state = " + place.getToken());
    }
}
