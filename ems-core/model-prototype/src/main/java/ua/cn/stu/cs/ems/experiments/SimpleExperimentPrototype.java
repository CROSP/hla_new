package ua.cn.stu.cs.ems.experiments;

import java.util.List;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.PlaceStateChangedListener;
import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.aggregates.Aggregate;
import ua.cn.stu.cs.ems.core.aggregates.AggregateInstance;
import ua.cn.stu.cs.ems.core.experiment.*;
import ua.cn.stu.cs.ems.core.experiment.statistics.StatisticsParameters;
import ua.cn.stu.cs.ems.core.models.ENetworksModel;
import ua.cn.stu.cs.ems.core.queues.FIFOQueue;
import ua.cn.stu.cs.ems.core.transitions.FTransition;
import ua.cn.stu.cs.ems.core.transitions.TTransition;
import ua.cn.stu.cs.ems.core.transitions.functions.InterpretedDelayFunction;
import ua.cn.stu.cs.ems.ecli.Value;

/**
 *
 * @author proger
 */
public class SimpleExperimentPrototype {

    public static void main(String[] args) {
        final ENetworksModel model = new ENetworksModel();
        Aggregate agr = createAggregateInstance();
        agr.setVariable("var", new Value(0));
        agr.setVariable("respond", new Value(0));

        FIFOQueue fifo = new FIFOQueue("FIFO");

        Place tInput = new Place("TInput");
        Place tOutput = new Place("TOutput");
        Place qOutput = new Place("QOutput");
        Place cycle = new Place("Cycle");

        PlaceStateChangedListener pscl = new PlaceStateChangedListener() {

            public void placeStateChanged(Place position) {
                System.out.println(model.getModelingTime() + " " + position.getName() + " token = " + position.getToken());
            }
        };
        tInput.addStateChangeListener(pscl);
        tOutput.addStateChangeListener(pscl);
        qOutput.addStateChangeListener(pscl);
        cycle.addStateChangeListener(pscl);

        FTransition f = new FTransition("F");
        f.addOutputPlace(tOutput, 0);
        f.addOutputPlace(cycle, 1);
        f.setInputPlace(tInput);

        TTransition t = new TTransition("T");
        t.setInputPlace(cycle);
        t.setOutputPlace(tInput);

        fifo.connectInputPlace(tOutput);
        fifo.connectOutputPlace(qOutput);

        qOutput.setToken(new Token());
        tInput.setToken(new Token());

        f.setDelayFunction(new InterpretedDelayFunction("V['respond']=V['var']; RETURN V['respond'];"));

        agr.addPlaces(tInput, tOutput, qOutput, cycle);
        agr.addQueue(fifo);
        agr.addTransitions(f, t);

        model.setRootAggregate(agr);
        model.setFinishTime(25D);

        FactorImpl factor = new FactorImpl(agr.getVariable("var"), new StepValueGenerator(1, 3, 1));
        factor.setFactorName("Intensity");
        Respond respond = new Respond(StatisticsParameters.AVERAGE_QUEUE_LENGTH.getPresentation());
        // Respond respond = new Respond(agr.getVariable("respond"));
        CountDownExperiment psc = new CountDownExperiment(model, factor, respond, 1);
        psc.setDelayBeforeCollectingPrimaryStatistics(0);
        psc.setIsPrimaryStatisticsShouldBeCollected(true);
        // psc.setPrimaryStatisticsDumpPeriod(10);

        psc.collectStatisticsOn(fifo);
        psc.collectSecondaryStatisticsOn(fifo);

        psc.startExperiment();


        ExperimentReport er = psc.getReport();
        List<PrimaryStatisticsResult> srs = er.getPrimaryStatisticsFor("A.FIFO");

        log("Average queue length");
        for (PrimaryStatisticsResult sr : srs) {
            log("Factor value = " + sr.getFactorValue() + " Factor name = " + factor.getFactorName());

            for (PrimaryStatisticsElement se : sr.getDumpedPrimaryStatisticsElements()) {
                log(se.getTime() + " " + se.getStatisticsResult().getResultValue(StatisticsParameters.AVERAGE_QUEUE_LENGTH));
            }
        }
        List<SecondaryStatisticsElement> sec = er.getSecondaryStatistics();
        for (SecondaryStatisticsElement el : sec) {
            log(el);
        }
    }

    private static AggregateInstance createAggregateInstance() {
        return new AggregateInstance("A");
    }

    private static void log(Object o) {
        System.out.println(o);
    }
}
