package ua.cn.stu.cs.ems;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.PlaceStateChangedListener;
import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.aggregates.Aggregate;
import ua.cn.stu.cs.ems.core.aggregates.AggregateInstance;
import ua.cn.stu.cs.ems.core.models.ENetworksModel;
import ua.cn.stu.cs.ems.core.models.Model;
import ua.cn.stu.cs.ems.core.models.ModelListener;
import ua.cn.stu.cs.ems.core.queues.FIFOPriorityQueue;
import ua.cn.stu.cs.ems.core.queues.FIFOQueue;
import ua.cn.stu.cs.ems.core.queues.LIFOPriorityQueue;
import ua.cn.stu.cs.ems.core.queues.LIFOQueue;
import ua.cn.stu.cs.ems.core.queues.Queue;
import ua.cn.stu.cs.ems.core.queues.QueueStateListener;
import ua.cn.stu.cs.ems.core.queues.TokenPriorityFunction;
import ua.cn.stu.cs.ems.core.transitions.TTransition;
import ua.cn.stu.cs.ems.core.transitions.Transition;
import ua.cn.stu.cs.ems.core.transitions.functions.DelayFunction;
import ua.cn.stu.cs.ems.core.utils.JessUtils;
import ua.cn.stu.cs.ems.ecli.Value;

/**
 *
 * @author proger
 */
public class SmallQueueModel {
    public static void main(String... argv) throws IOException {

        System.out.println("Please input number to select queue type:\n"
                + "1 - LIFO\n"
                + "2 - FIFO\n"
                + "3 - Priority LIFO\n"
                + "4 - Priority FIFO\n"
                + "Input: ");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));


       // String userInput = br.readLine();
        int userChoise = 3;
        if (userChoise < 1 || userChoise > 4) {
            System.out.println("Wrong number");
            System.exit(1);
        }


        Queue queue = createQueueFromUserChoise(userChoise);

        Aggregate rootAgregate = createRootAgregate(queue);
        ENetworksModel model = new ENetworksModel();
        model.setRootAggregate(rootAgregate);

        model.setFinishTime(50D);
        model.start();
    }

    public static AggregateInstance createRootAgregate(Queue queue) {
        AggregateInstance parent = new AggregateInstance("Root");

        Place queueInput = new Place("queueInput"); queueInput.addStateChangeListener(placeListener);
        Place queueOutput = new Place("queueOutput"); queueOutput.addStateChangeListener(placeListener);

        TTransition generator = createGenerator();
        TTransition consumer = createConsumer();

        generator.setOutputPlace(queueInput);
        consumer.setInputPlace(queueOutput);

        queue.connectInputPlace(queueInput);
        queue.connectOutputPlace(queueOutput);
        queue.addQueueStateListener(queueStdOutListener);

        parent.addTransitions(generator, consumer);
        parent.addPlaces(queueInput, queueOutput);
        parent.addQueue(queue);

        return parent;
    }

    private static Queue createQueueFromUserChoise(int userChoise) {
        String queueName = "queue";
        switch (userChoise) {
            case 1:
                return new LIFOQueue(queueName);


            case 2:
                return new FIFOQueue(queueName);

            case 3: {
                LIFOPriorityQueue lpq = new LIFOPriorityQueue(queueName, tokenPriorityFunction);
                return lpq;
            }

            case 4: {
                FIFOPriorityQueue fpq = new FIFOPriorityQueue(queueName, tokenPriorityFunction);
                return fpq;
            }

            default:
                throw new RuntimeException();
        }
    }

    private static DelayFunction consumerDelayFunction = new DelayFunction() {

        public double getDelayTime(Transition transition) {
            Random rand = new Random();
            return Math.abs(rand.nextDouble() % 10);
        }

        public DelayFunction copy() {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    };
    private static DelayFunction generatorDelayFunction = new DelayFunction() {

        public double getDelayTime(Transition transition) {
            Random rand = new Random();
            return Math.abs(rand.nextDouble() % 5);
        }

        public DelayFunction copy() {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    };

    private static TTransition createGenerator() {
        return new TTransition("generator") {
            int tokenNumber;

            {
                setDelayFunction(generatorDelayFunction);
            }

            @Override
            public boolean canFire() {
                return getOutputPlace().isEmpty();
            }

            @Override
            public void onDelayFinished() {
                System.out.println("Generator delay finished");
                Token newToken = new Token();
                newToken.setValue("number", new Value(new Double(++tokenNumber)));
                newToken.setValue("priority", new Value(new Double(tokenNumber % 5)));

                getOutputPlace().setToken(newToken);
            }
        };
    }

    private static TTransition createConsumer() {
        return new TTransition("consumer") {
            {
                setDelayFunction(consumerDelayFunction);
            }

            @Override
            public boolean canFire() {
                return !getInputPlace().isEmpty();
            }

            @Override
            public void onDelayFinished() {
                System.out.println("Consumer delay finished");

                getInputPlace().setToken(null);
            }
        };
    }

    static TokenPriorityFunction tokenPriorityFunction = new TokenPriorityFunction() {

        public int calculateTokenPriority(Queue queue, Token token) {
            return token.getValue("priority").intValue();
        }
    };

    static ModelListener modelListener = new ModelListener() {

        public void modelStarted(Model model) {
            System.out.println("Model started");
        }

        public void modelFinished(Model model) {
            System.out.println("Model finished");
        }

        public void modelTimeChanged(Model model, double newModelTime) {
            // nothing to do
        }
    };

    private static QueueStateListener queueStdOutListener = new QueueStateListener() {

        public void tokenAddedToQueue(Queue queue, Token token) {
            System.out.println(queue.getParentAggregate().getModel().getModelingTime() +
                    " " + queue.getName() + " tokens = " + queue.getTokensInQueue());
        }

        public void tokenRemovedFromQueue(Queue queue, Token token) {
            System.out.println(queue.getParentAggregate().getModel().getModelingTime() +
                    " " + queue.getName() + " tokens = " + queue.getTokensInQueue());
        }


    };
    
    private static PlaceStateChangedListener placeListener = new PlaceStateChangedListener() {

        public void placeStateChanged(Place place) {
            if (place.getParentAggregate() != null && place.getParentAggregate().getModel() != null) {
                System.out.println(place.getParentAggregate().getModel().getModelingTime()
                        + " " + JessUtils.getAggregateChildFullName(place) + " state = " + place.getToken());
            }
        }
    };
}