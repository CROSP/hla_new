package ua.cn.stu.cs.ems;

import java.util.Random;
import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.models.ENetworksModel;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.PlaceStateChangedListener;
import ua.cn.stu.cs.ems.core.aggregates.AbstractAggregate;
import ua.cn.stu.cs.ems.core.aggregates.Aggregate;
import ua.cn.stu.cs.ems.core.transitions.TTransition;
import ua.cn.stu.cs.ems.core.transitions.Transition;
import ua.cn.stu.cs.ems.core.transitions.functions.DelayFunction;
import ua.cn.stu.cs.ems.core.transitions.functions.TransformationFunction;
import ua.cn.stu.cs.ems.ecli.Value;

public class SimplestModel
{
    public static void main( String[] args )
    {
        final ENetworksModel model = new ENetworksModel();
        TTransition transiton1 = new TTransition("T1");
        TTransition transiton2 = new TTransition("T2");
        Place position1 = new Place("P1");
        Place position2 = new Place("P2");

        position1.addStateChangeListener(new PlaceStateChangedListener() {

            public void placeStateChanged(Place position) {
                System.out.println(model.getModelingTime() + " position1 token = " + position.getToken());
            }
        });

        position2.addStateChangeListener(new PlaceStateChangedListener() {

            public void placeStateChanged(Place position) {
                System.out.println(model.getModelingTime() + " position2 token = " + position.getToken());
            }
        });

        DelayFunction df = new DelayFunction() {

            public double getDelayTime(Transition transition) {
                Random rand = new Random();
                return Math.abs(rand.nextDouble() % 14);
            }

            public DelayFunction copy() {
                throw new UnsupportedOperationException("Not supported yet.");
            }
        };

        transiton1.setDelayFunction(df);
        transiton2.setDelayFunction(df);

        transiton1.setTransformationFunction(new TransformationFunction() {

            public Token transform(Transition transition, Token token) {
                token.setValue("attr", new Value(token.getValue("attr").doubleValue() + 1));
                return token;
            }

            public TransformationFunction copy() {
                throw new UnsupportedOperationException("Not supported yet.");
            }
        });

        transiton2.setTransformationFunction(new TransformationFunction() {

            public Token transform(Transition transition, Token token) {
                token.setValue("attr", new Value(token.getValue("attr").doubleValue() + 2));
                return token;
            }

            public TransformationFunction copy() {
                throw new UnsupportedOperationException("Not supported yet.");
            }
        });


        transiton1.setInputPlace(position1);
        transiton1.setOutputPlace(position2);
        transiton2.setInputPlace(position2);
        transiton2.setOutputPlace(position1);

        Aggregate agregate = new AbstractAggregate("A") {};

        agregate.addPlace(position2);
        agregate.addPlace(position1);

        agregate.addTransition(transiton1);
        agregate.addTransition(transiton2);
        transiton1.setParentAggregate(agregate);
        transiton2.setParentAggregate(agregate);


        model.setRootAggregate(agregate);
        model.setFinishTime(150D);

        Token token = new Token();
        token.setValue("attr", new Value(0d));

        position1.setToken(token);

        model.start(100);
    }
}



