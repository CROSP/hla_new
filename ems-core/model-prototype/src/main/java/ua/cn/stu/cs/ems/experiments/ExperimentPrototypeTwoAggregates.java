package ua.cn.stu.cs.ems.experiments;

import java.util.List;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.PlaceStateChangedListener;
import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.aggregates.*;
import ua.cn.stu.cs.ems.core.experiment.CountDownExperiment;
import ua.cn.stu.cs.ems.core.experiment.ExperimentReport;
import ua.cn.stu.cs.ems.core.experiment.PrimaryStatisticsElement;
import ua.cn.stu.cs.ems.core.experiment.PrimaryStatisticsResult;
import ua.cn.stu.cs.ems.core.experiment.statistics.StatisticsParameters;
import ua.cn.stu.cs.ems.core.models.ENetworksModel;
import ua.cn.stu.cs.ems.core.queues.FIFOQueue;
import ua.cn.stu.cs.ems.core.transitions.FTransition;
import ua.cn.stu.cs.ems.core.transitions.JTransition;
import ua.cn.stu.cs.ems.core.transitions.TTransition;
import ua.cn.stu.cs.ems.core.transitions.functions.ConstDelayFunction;

/**
 * 
 * @author Dmitry Krivenko <dmitrykrivenko@gmal.com>
 */
public class ExperimentPrototypeTwoAggregates {

    public static void main(String[] args) {
        final ENetworksModel model = new ENetworksModel();
        AggregateInstance agr = createAggregateInstance();

        /*---------------------------1st aggregate----------------------------*/
        AggregateInstance agr1 = new AggregateInstance("A1");

        Place pHlaIn = new Place("PHlaIn");
        Place pHlaOut = new Place("PHlaOut");
        Place pQueueIn = new Place("PQueueIn");
        Place pModelOut = new Place("PModelOut");

        Input hlaInput = new InputImpl("HlaInP1");
        hlaInput.setOutputPlace(pHlaIn);
        hlaInput.setToken(new Token());

        Output outToHla = new OutputImpl("outToHla");
        outToHla.connectInputPlace(pHlaOut);

        outToHla.addInput(hlaInput, 0);

        Output outToModel = new OutputImpl("outToModel");
        outToModel.connectInputPlace(pModelOut);

        FTransition fTransition = new FTransition("F");
        fTransition.setDelayFunction(new ConstDelayFunction(5));

        fTransition.setInputPlace(pHlaIn);
        fTransition.addOutputPlace(pQueueIn, 0);
        fTransition.addOutputPlace(pHlaOut, 1);

        FIFOQueue fifo = new FIFOQueue("FifoQueue");
        fifo.connectInputPlace(pQueueIn);
        fifo.connectOutputPlace(pModelOut);


        PlaceStateChangedListener pscl = new PlaceStateChangedListener() {

            public void placeStateChanged(Place position) {
                System.out.println(model.getModelingTime() + " " + position.getName() + " token = " + position.getToken());
            }
        };
        pHlaIn.addStateChangeListener(pscl);
        pHlaOut.addStateChangeListener(pscl);
        pQueueIn.addStateChangeListener(pscl);
        pModelOut.addStateChangeListener(pscl);

        agr1.addPlaces(pHlaIn, pHlaOut, pQueueIn, pModelOut);
        agr1.addTransition(fTransition);
        agr1.addQueue(fifo);
        agr1.addInput(hlaInput, 0);
        agr1.addOutput(outToHla, 0);
        agr1.addOutput(outToModel, 0);
        /* Place p1 = new Place("p1");
        AggregateInstance agr1 = new AggregateInstance("A1");
        
        Output out1 = new OutputImpl("Out1");
        out1.connectInputPlace(p1);
        
        Input in1 = new InputImpl("In1");
        in1.setOutputPlace(p1);
        in1.setToken(new Token());
        
        agr1.addPlace(p1);
        agr1.addInput(in1, 0);
        agr1.addOutput(out1, 0);*/
        /*--------------------------------------------------------------------*/

        /*---------------------------2nd aggregate----------------------------*/
        AggregateInstance agr2 = new AggregateInstance("A2");

        Place p1 = new Place("P1");
        Place p2 = new Place("P2");
        Place p3 = new Place("P3");
        Place p4 = new Place("P4");

        p4.setToken(new Token());

        Place jOuput = new Place("FJ1");

        JTransition j1 = new JTransition("J1");
        TTransition t1 = new TTransition("T1");
        TTransition f1 = new TTransition("F1");

        j1.setDelayFunction(new ConstDelayFunction(3));
        t1.setDelayFunction(new ConstDelayFunction(8));
        f1.setDelayFunction(new ConstDelayFunction(0));

        j1.addInputPlace(p1, 0);
        j1.addInputPlace(p4, 1);

        j1.setOutputPlace(jOuput);

        f1.setInputPlace(jOuput);

        f1.addOutputPlace(p2, 0);
        f1.addOutputPlace(p3, 0);

        t1.setInputPlace(p3);
        t1.setOutputPlace(p4);

        InputImpl input = new InputImpl("inP1");
        input.setOutputPlace(p1);

        agr2.addInput(input, 0);
        agr2.addPlaces(p1, p2, p3, p4, jOuput);
        agr2.addTransitions(t1, f1, j1);

        outToModel.addInput(input, 0);
        /*Place p2 = new Place("p2");
        Place p3 = new Place("p3");
        
        Input in2 = new InputImpl("In2");
        in2.setOutputPlace(p2);
        
        FIFOQueue fifo2 = new FIFOQueue("fifo2");
        fifo2.connectInputPlace(p2);
        fifo2.connectOutputPlace(p3);
        
        agr2.addPlaces(p2, p3);
        agr2.addQueue(fifo2);
        agr2.addInput(in2, 0);
        
        outToModel.addInput(in2, 0);
         */
        /*--------------------------------------------------------------------*/
        agr.addChildAggregate(agr1);
        agr.addChildAggregate(agr2);


        model.setRootAggregate(agr);
        model.setFinishTime(25D);


        CountDownExperiment psc = new CountDownExperiment(model, 1);
        psc.setDelayBeforeCollectingPrimaryStatistics(0);
        psc.setIsPrimaryStatisticsShouldBeCollected(true);


        psc.collectStatisticsOn(p1);
        psc.startExperiment();


        ExperimentReport er = psc.getReport();
        List<PrimaryStatisticsResult> srs = er.getPrimaryStatisticsFor("A.A2.P1");

        for (PrimaryStatisticsResult sr : srs) {

            for (PrimaryStatisticsElement se : sr.getDumpedPrimaryStatisticsElements()) {
                log(se.getTime() + " " + se.getStatisticsResult().getResultValue(StatisticsParameters.NUMBER_OF_PASSED_TOKENS));
            }
        }
    }

    private static AggregateInstance createAggregateInstance() {
        return new AggregateInstance("A");
    }

    private static void log(Object o) {
        System.out.println(o);
    }
}
