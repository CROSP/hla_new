package ua.cn.stu.cs.ems.experiments;

import java.util.List;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.PlaceStateChangedListener;
import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.aggregates.Aggregate;
import ua.cn.stu.cs.ems.core.aggregates.AggregateInstance;
import ua.cn.stu.cs.ems.core.experiment.CountDownExperiment;
import ua.cn.stu.cs.ems.core.experiment.ExperimentReport;
import ua.cn.stu.cs.ems.core.experiment.PrimaryStatisticsElement;
import ua.cn.stu.cs.ems.core.experiment.PrimaryStatisticsResult;
import ua.cn.stu.cs.ems.core.models.ENetworksModel;
import ua.cn.stu.cs.ems.core.transitions.TTransition;
import ua.cn.stu.cs.ems.core.transitions.functions.ConstDelayFunction;

/**
 *
 * @author Dmitry Krivenko <dmitrykrivenko@gmal.com>
 */
public class ExperimentPrototypeCircle {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        TTransition t1 = new TTransition("T1");
        TTransition t2 = new TTransition("T2");

        t1.setDelayFunction(new ConstDelayFunction(5));

        Place p0 = new Place("p0");
        Place p1 = new Place("p1");


        p0.setToken(new Token());

        t1.setInputPlace(p0);
        t1.setOutputPlace(p1);


        t2.setInputPlace(p1);
        t2.setOutputPlace(p0);


        Aggregate root = createAggregateInstance();
        root.addPlaces(p0, p1);
        root.addTransitions(t1, t2);

        final ENetworksModel model = new ENetworksModel();

        model.setRootAggregate(root);

        p1.addStateChangeListener(new PlaceStateChangedListener() {

            public void placeStateChanged(Place place) {
                log(model.getModelingTime() + " " + place.getToken());
            }
        });

        CountDownExperiment cde = new CountDownExperiment(model, 1);

        cde.collectStatisticsOn(p0);
        cde.setDelayBeforeCollectingPrimaryStatistics(0);
        cde.setPrimaryStatisticsDumpPeriod(10);
        cde.setIsPrimaryStatisticsShouldBeCollected(true);

        model.setFinishTime(50D);
        cde.startExperiment();

        ExperimentReport report = cde.getReport();

        List<PrimaryStatisticsResult> psrList = report.getPrimaryStatisticsFor("A.p0");

        for (PrimaryStatisticsResult pse : psrList) {
            log(pse);
            for (PrimaryStatisticsElement el : pse.getDumpedPrimaryStatisticsElements()) {
                log(el);
            }
        }
    }

    private static AggregateInstance createAggregateInstance() {
        return new AggregateInstance("A");
    }

    private static void log(Object o) {
        System.out.println(o);
    }
}
