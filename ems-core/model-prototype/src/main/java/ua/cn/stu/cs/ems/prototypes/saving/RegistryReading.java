package ua.cn.stu.cs.ems.prototypes.saving;

import java.io.File;
import java.io.IOException;
import org.jdom.JDOMException;
import ua.cn.stu.cs.ems.core.Constants;
import ua.cn.stu.cs.ems.core.aggregates.AggregateDefinition;
import ua.cn.stu.cs.ems.core.aggregates.AggregateRegistry;
import ua.cn.stu.cs.ems.core.utils.ExtensionFilter;
import ua.cn.stu.cs.ems.core.xml.AggregateTypesReader;
import ua.cn.stu.cs.ems.core.xml.PNMLParsingException;

/**
 *
 * @author proger
 */
public class RegistryReading {
    public static void main(String[] args) throws Exception {
        File dir = new File("/home/proger");

        AggregateRegistry ar = readFromDirectory(dir);

        for (AggregateDefinition ad : ar.getAggregateDefinitions()) {
            System.out.println(ad.getName());
        }
    }

    public static AggregateRegistry readFromDirectory(File dir) throws JDOMException, IOException, PNMLParsingException {
        File[] aggregateDefinitions = dir.listFiles(new ExtensionFilter(Constants.AGGREGATE_PNML_EXTENSION));

        System.out.println("Definition files:");
        for (File file : aggregateDefinitions) {
            System.out.println(file.getAbsoluteFile());
        }

        AggregateTypesReader atr = new AggregateTypesReader();

        return atr.readAggregateTypes(aggregateDefinitions);
    }
}
