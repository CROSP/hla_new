package ua.cn.stu.cs.ems.experiments;

import java.util.List;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.aggregates.*;
import ua.cn.stu.cs.ems.core.experiment.CountDownExperiment;
import ua.cn.stu.cs.ems.core.experiment.ExperimentReport;
import ua.cn.stu.cs.ems.core.experiment.PrimaryStatisticsElement;
import ua.cn.stu.cs.ems.core.experiment.PrimaryStatisticsResult;
import ua.cn.stu.cs.ems.core.experiment.statistics.StatisticsParameters;
import ua.cn.stu.cs.ems.core.models.ENetworksModel;
import ua.cn.stu.cs.ems.core.transitions.TTransition;
import ua.cn.stu.cs.ems.core.transitions.functions.ConstDelayFunction;

/**
 *
 * @author Dmitry Krivenko <dmitrykrivenko@gmal.com>
 */
public class PrototypeThreeAggregates {

    public static void main(String[] args) {
        final ENetworksModel model = new ENetworksModel();
        AggregateDefinition rootDef = new AggregateDefinition("rootDef");

        /*
         * -------------------Aggregate definition------------------------------
         */
        AggregateDefinition aggrDef = new AggregateDefinition("agg_part");

        Input i0Def = new InputImpl("I0");
        Output o0Def = new OutputImpl("O0");

        Place p0Def = new Place("P0");
        Place p1Def = new Place("P1");

        TTransition t0Def = new TTransition("T0");

        t0Def.setDelayFunction(new ConstDelayFunction(10));

        t0Def.addInputPlace(p0Def, 0);
        t0Def.addOutputPlace(p1Def, 0);

        i0Def.setOutputPlace(p0Def);
        o0Def.connectInputPlace(p1Def);

        aggrDef.addPlaces(p0Def, p1Def);
        aggrDef.addTransitions(t0Def);
        aggrDef.addInput(i0Def, 0);
        aggrDef.addOutput(o0Def, 0);
        /*
         * ---------------------------------------------------------------------
         */

        /*
         * --------------------Root Aggregate Definition------------------------
         */
        Place p0 = new Place("P0");
        Place p1 = new Place("P1");

        Input i0 = new InputImpl("I0");
        Output o0 = new OutputImpl("O0");

        i0.setOutputPlace(p1);
        i0.setToken(new Token());

        o0.connectInputPlace(p0);

        rootDef.addPlaces(p1, p0);
        rootDef.addInput(i0, 0);
        rootDef.addOutput(o0, 0);
        /*
         * --------------------------------------------------------------------
         */

        AggregateDefinitionReference agrReference0 = rootDef.addChildAggreagateDefinition(aggrDef, "agg_part_0");
        AggregateDefinitionReference agrReference1 = rootDef.addChildAggreagateDefinition(aggrDef, "agg_part_1");

        /*
         * ---------------------------1st aggregate----------------------------
         */

        agrReference0.getInput("I0").setInputPlace(rootDef.getPlace("P1"));
        /*
         * --------------------------------------------------------------------
         */

        /*
         * ---------------------------2nd aggregate----------------------------
         */

        agrReference1.getOutput("O0").connectOutputPlace(rootDef.getPlace("P0"));
        agrReference0.getOutput("O0").addInput(agrReference1.getInput("I0"), 0);
        /*
         * ---------------------------------------------------------------------
         */


        AggregateInstance agr = rootDef.createInstance("root");

        agr.getOutput("O0").addInput(agr.getInput("I0"), 0);



        model.setRootAggregate(agr);
        model.setFinishTime(100D);


        CountDownExperiment psc = new CountDownExperiment(model, 1);
        psc.setDelayBeforeCollectingPrimaryStatistics(0);
        psc.setIsPrimaryStatisticsShouldBeCollected(true);


        psc.collectStatisticsOn(agr.getAggregate("agg_part_0").getPlace("P0"));
        psc.startExperiment();


        ExperimentReport er = psc.getReport();
        List<PrimaryStatisticsResult> srs = er.getPrimaryStatisticsFor("root.agg_part_0.P0");

        for (PrimaryStatisticsResult sr : srs) {

            for (PrimaryStatisticsElement se : sr.getDumpedPrimaryStatisticsElements()) {
                log(se.getTime() + " " + se.getStatisticsResult().getResultValue(StatisticsParameters.NUMBER_OF_PASSED_TOKENS));
            }
        }
    }

    private static void log(Object o) {
        System.out.println(o);
    }
}
