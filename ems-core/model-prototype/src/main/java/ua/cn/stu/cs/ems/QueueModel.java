package ua.cn.stu.cs.ems;

import java.util.HashSet;
import java.util.Set;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.PlaceStateChangedListener;
import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.aggregates.AbstractAggregate;
import ua.cn.stu.cs.ems.core.aggregates.Aggregate;
import ua.cn.stu.cs.ems.core.aggregates.AggregateInstance;
import ua.cn.stu.cs.ems.core.aggregates.AggregateVariable;
import ua.cn.stu.cs.ems.core.aggregates.Input;
import ua.cn.stu.cs.ems.core.aggregates.InputImpl;
import ua.cn.stu.cs.ems.core.aggregates.Output;
import ua.cn.stu.cs.ems.core.aggregates.OutputImpl;
import ua.cn.stu.cs.ems.core.models.ENetworksModel;
import ua.cn.stu.cs.ems.core.queues.FIFOPriorityQueue;
import ua.cn.stu.cs.ems.core.queues.FIFOQueue;
import ua.cn.stu.cs.ems.core.queues.LIFOPriorityQueue;
import ua.cn.stu.cs.ems.core.queues.LIFOQueue;
import ua.cn.stu.cs.ems.core.queues.Queue;
import ua.cn.stu.cs.ems.core.queues.QueueStateListener;
import ua.cn.stu.cs.ems.core.queues.TokenPriorityFunction;
import ua.cn.stu.cs.ems.core.transitions.FTransition;
import ua.cn.stu.cs.ems.core.transitions.SpecialTransition;
import ua.cn.stu.cs.ems.core.transitions.TTransition;
import ua.cn.stu.cs.ems.core.transitions.Transition;
import ua.cn.stu.cs.ems.core.transitions.XTransition;
import ua.cn.stu.cs.ems.core.transitions.YTransition;
import ua.cn.stu.cs.ems.core.transitions.functions.ConstDelayFunction;
import ua.cn.stu.cs.ems.core.transitions.functions.DelayFunction;
import ua.cn.stu.cs.ems.core.transitions.functions.PermitingFunction;
import ua.cn.stu.cs.ems.core.transitions.functions.TransformationFunction;
import ua.cn.stu.cs.ems.core.utils.JessUtils;
import ua.cn.stu.cs.ems.ecli.Value;

/**
 *
 * @author proger
 */
public class QueueModel {

    private static final String PASSED = "passed";
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("X,Y prototype model");
        System.out.println("<modeling time> <place-name> <place's token>");

        AggregateInstance A1 = createA1();
        AggregateInstance A2 = createA2();

        AggregateInstance rootAgregate = createRootAgregate();
        connectAgregates(rootAgregate, A1, A2);

        ENetworksModel model = new ENetworksModel();
        model.setRootAggregate(rootAgregate);

        model.setFinishTime(200D);
        model.start();
    }

    private static AggregateInstance createRootAgregate() {
        return new AggregateInstance("rootAgregate");
    }

    private static AggregateInstance createA1() {


        AggregateInstance A1 = new AggregateInstance("A1");

        Output outP5 = new OutputImpl("outP5");
        Output outP8 = new OutputImpl("outP8");

        XTransition X1 = new XTransition("X1"); X1.setDelayFunction(constDelayFunction); X1.setPermitingFunction(new Permiter());
        TTransition T1 = new TTransition("T1"); T1.setDelayFunction(constDelayFunction);
        FTransition F1 = new FTransition("F1"); F1.setDelayFunction(constDelayFunction);
        TTransition T2 = new TTransition("T2"); T2.setDelayFunction(constDelayFunction);
        YTransition Y1 = new YTransition("Y1"); Y1.setDelayFunction(constDelayFunction); Y1.setPermitingFunction(new Permiter());

        Place P1 = new Place("P1");
        Place P2 = new Place("P2");
        Place P3 = new Place("P3");
        Place P4 = new Place("P4");
        Place P5 = new Place("P5");
        Place P6 = new Place("P6");
        Place P7 = new Place("P7");
        Place P8 = new Place("P8");

        X1.setInputPlace(P1);

        X1.addOutputPlace(P2, 0);
        X1.addOutputPlace(P3, 1);
        X1.addOutputPlace(P4, 2);

        T1.setInputPlace(P4);
        T1.setOutputPlace(P1);

        F1.setInputPlace(P2);
        F1.addOutputPlace(P5, 0);
        F1.addOutputPlace(P6, 0);

        T2.setInputPlace(P6);
        T2.setOutputPlace(P7);

        Y1.addInputPlace(P7, 0);
        Y1.addInputPlace(P3, 1);

        Y1.connectOutputPlace(P8);

        outP5.connectInputPlace(P5);
        outP8.connectInputPlace(P8);

        A1.addOutput(outP5, 0);
        A1.addOutput(outP8, 0);

        addPlacesToAgregate(A1, P1, P2, P3, P4, P5, P6, P7, P8);
        addTransitionsToAgregate(A1, T1, X1, Y1, F1, T2);

        P4.setToken(createToken());

        return A1;
    }

    private static AggregateInstance createA2() {
        AggregateInstance A2 = new AggregateInstance("A2");

        Place P1 = new Place("P1"); P1.setToken(createToken());
        Place P2 = new Place("P2"); P2.setToken(createToken());
        Place P3 = new Place("P3");
        Place P4 = new Place("P4");
        Place P5 = new Place("P5");
        Place P6 = new Place("P6");
        Place P7 = new Place("P7");
        Place P8 = new Place("P8");
        Place P9 = new Place("P9");
        Place P10 = new Place("P10");
        Place P11 = new Place("P11");
        Place P12 = new Place("P12");
        Place P13 = new Place("P13");
        Place P14 = new Place("P14");
        Place P15 = new Place("P15");
        Place P16 = new Place("P16");
        Place P17 = new Place("P17");
        Place P18 = new Place("P18");
        Place PQ = new Place("PQ");

        FTransition F1 = new FTransition("F1"); F1.setDelayFunction(constDelayFunction);
        FTransition F2 = new FTransition("F2"); F2.setDelayFunction(constDelayFunction);

        TTransition T7 = new TTransition("T7"); T7.setDelayFunction(constDelayFunction);
        TTransition T10= new TTransition("T10"); T10.setDelayFunction(constDelayFunction);
        TTransition T12 = new TTransition("T12"); T12.setDelayFunction(constDelayFunction);

        XTransition X3 = new XTransition("X3"); X3.setDelayFunction(constDelayFunction); X3.setPermitingFunction(new Permiter());
        XTransition X4 = new XTransition("X4"); X4.setDelayFunction(constDelayFunction); X4.setPermitingFunction(new Permiter());

        YTransition Y8 = new YTransition("Y8"); Y8.setDelayFunction(constDelayFunction); Y8.setPermitingFunction(new Permiter());
        YTransition Y13 = new YTransition("Y13"); Y13.setDelayFunction(constDelayFunction); Y13.setPermitingFunction(new Permiter());

        FIFOQueue FIFO = new FIFOQueue("FIFOQueue");
        LIFOQueue LIFO = new LIFOQueue("LIFOQueue");

        FIFOPriorityQueue FIFOPriority = new FIFOPriorityQueue("FIFOPriorityQueue", new TokenPriority());
        LIFOPriorityQueue LIFOPriority = new LIFOPriorityQueue("LIFOPriority", new TokenPriority());

        Input inP2 = new InputImpl("inP2");
        Input inP1 = new InputImpl("inP1");

        F1.setInputPlace(P1);
        F1.addOutputPlace(P3, 0);
        F1.addOutputPlace(P4, 1);

        F2.setInputPlace(P2);
        F2.addOutputPlace(PQ, 0);
        F2.addOutputPlace(P5, 1);

        X3.setInputPlace(P3);
        X3.addOutputPlace(P6, 0);
        X3.addOutputPlace(P7, 1);

        X4.setInputPlace(P4);
        X4.addOutputPlace(P8, 0);
        X4.addOutputPlace(P9, 1);

        T7.setInputPlace(P6);
        T7.setOutputPlace(P11);

        Y8.addInputPlace(P7, 0);
        Y8.addInputPlace(P8, 1);
        Y8.connectOutputPlace(P14);

        FIFO.connectInputPlace(P9);
        FIFO.connectOutputPlace(P15);

        LIFO.connectInputPlace(P14);
        LIFO.connectOutputPlace(P16);

        T12.setInputPlace(P15);
        T12.setOutputPlace(P17);

        FIFOPriority.connectInputPlace(PQ);
        FIFOPriority.connectOutputPlace(P10);

        LIFOPriority.connectInputPlace(P5);
        LIFOPriority.connectOutputPlace(P13);

        T10.setInputPlace(P10);
        T10.setOutputPlace(P12);

        Y13.addInputPlace(P17, 0);
        Y13.addInputPlace(P12, 1);
        Y13.connectOutputPlace(P18);

        inP1.setOutputPlace(P1);
        inP2.setOutputPlace(P2);

        addPlacesToAgregate(A2, P1, P2, P3, P4, P5, P6, P7, P8, P9, P10, P11, P12, P13, P14, P15, P16, P17, P18, PQ);
        addTransitionsToAgregate(A2, F1, F2, T7, T10, T12, X3, X4, Y8, Y13);
        addQueuesToAgregate(A2, FIFO, LIFO, FIFOPriority, LIFOPriority);

        A2.addInput(inP1, 0);
        A2.addInput(inP2, 1);

        return A2;
    }

    private static void connectAgregates(AggregateInstance rootAgregate, AggregateInstance A1, AggregateInstance A2) {
        addAgregatesToAgregate(rootAgregate, A1, A2);
        
        A1.getOutput("outP5").addInput(A2.getInput("inP1"), 0);
        A1.getOutput("outP8").addInput(A2.getInput("inP2"), 0);
    }

    private static void addPlacesToAgregate(Aggregate a, Place... places) {
        for (Place p : places) {
            a.addPlace(p);
            p.addStateChangeListener(placeListener);
        }
    }

    private static void addTransitionsToAgregate(Aggregate a, Transition... transitions) {
        for (Transition t : transitions) {
            a.addTransition(t);
            t.setTransformationFunction(new Transformation());
        }
    }

    private static void addAgregatesToAgregate(AggregateInstance parent, AggregateInstance... childs) {
        for (AggregateInstance child : childs) {
            parent.addChildAggregate(child);
        }
    }

    private static void addQueuesToAgregate(Aggregate parent, Queue... queues) {
        for (Queue q : queues) {
            parent.addQueue(q);
            q.addQueueStateListener(queueStdOutListener);
        }
    }

    private static Token createToken() {
        Token token = new Token();
        token.setValue(PASSED, new Value(0d));

        return token;
    }

    private static class Transformation implements TransformationFunction {

        public Token transform(Transition transition, Token token) {
            if (token == null)
                System.out.println(transition.getName() + " returned null token");

            Token newToken = new Token(token);

            Double oldValue = token.getValue(PASSED).doubleValue();
            if (oldValue == null)
                System.out.println(transition.getName() + " returned token with null attribute");

            newToken.setValue(PASSED, new Value(oldValue + 1));
            return newToken;
        }

        public TransformationFunction copy() {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }

    private static DelayFunction constDelayFunction = new ConstDelayFunction(1);

    private static PlaceListener placeListener = new PlaceListener();

    private static class PlaceListener implements PlaceStateChangedListener {

        public void placeStateChanged(Place place) {
            if (place.getParentAggregate() != null && place.getParentAggregate().getModel() != null) {
                System.out.println(place.getParentAggregate().getModel().getModelingTime()
                        + " " + JessUtils.getAggregateChildFullName(place) + " state = " + place.getToken());
            }
        }
    }

    private static QueueStateListener queueStdOutListener = new QueueStateListener() {

        public void tokenAddedToQueue(Queue queue, Token token) {
            System.out.println(queue.getParentAggregate().getModel().getModelingTime() +
                    " " + queue.getName() + " tokens = " + queue.getTokensInQueue());
        }

        public void tokenRemovedFromQueue(Queue queue, Token token) {
            System.out.println(queue.getParentAggregate().getModel().getModelingTime() +
                    " " + queue.getName() + " tokens = " + queue.getTokensInQueue());
        }


    };

    private static class TokenPriority implements TokenPriorityFunction {


        public int calculateTokenPriority(Queue queue, Token token) {
            return token.getValue(PASSED).intValue();
        }
        
    }

    private static class Permiter implements PermitingFunction {
        int i = -1;

        public Integer getPlaceNumber(SpecialTransition trasntion) {
            i++;
            if (trasntion instanceof XTransition) {
                return i % trasntion.getOutputPlaces().size();
            }
            else {
                return i % trasntion.getInputPlaces().size();
            }
        }

        public Set<Place> getPlaceDependencies(SpecialTransition trasntion) {
            return new HashSet<Place>();
        }

        public Set<AggregateVariable> getVariableDependencies(SpecialTransition transition) {
            return new HashSet<AggregateVariable>();
        }

        public PermitingFunction copy() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

    }


}
