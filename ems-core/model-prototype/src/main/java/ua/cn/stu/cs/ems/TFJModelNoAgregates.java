package ua.cn.stu.cs.ems;

import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.PlaceStateChangedListener;
import ua.cn.stu.cs.ems.core.aggregates.AbstractAggregate;
import ua.cn.stu.cs.ems.core.aggregates.Aggregate;
import ua.cn.stu.cs.ems.core.models.ENetworksModel;
import ua.cn.stu.cs.ems.core.transitions.FTransition;
import ua.cn.stu.cs.ems.core.transitions.JTransition;
import ua.cn.stu.cs.ems.core.transitions.TTransition;
import ua.cn.stu.cs.ems.core.transitions.functions.ConstDelayFunction;

/**
 *
 * @author proger
 */
public class TFJModelNoAgregates {
    public static void main(String argv[]) {
        Place a1p1 = new Place("P1");
        Place a1p2 = new Place("P2");
        Place a1p3 = new Place("P3");
        Place a1p4 = new Place("P4");
        Place a1p5 = new Place("P5") {

            @Override
            public void setToken(Token token) {
                for (PlaceStateChangedListener listener : getPlaceStateChangedListeners())
                    listener.placeStateChanged(this);
                /**
                 * Just do nothing. Let it place always be free to not freeze the model.
                 */
            }

        };

        Token token = new Token();
        a1p4.setToken(token);

        TTransition a1t1 = new TTransition("T1");
        FTransition a1f1 = new FTransition("F1");
        JTransition a1j1 = new JTransition("J1");

        a1t1.setDelayFunction(new ConstDelayFunction(4));

        a1f1.setDelayFunction(new ConstDelayFunction(5));

        a1j1.setDelayFunction(new ConstDelayFunction(3));


        a1t1.setInputPlace(a1p4);
        a1t1.setOutputPlace(a1p1);

        a1f1.setInputPlace(a1p1);
        a1f1.addOutputPlace(a1p2, 0);
        a1f1.addOutputPlace(a1p3, 1);
        a1f1.addOutputPlace(a1p4, 2);
        
        a1j1.addInputPlace(a1p2, 0);
        a1j1.addInputPlace(a1p3, 0);

        a1j1.setOutputPlace(a1p5);

        Aggregate rootAgregate = new AbstractAggregate("A") {};
        ENetworksModel model = new ENetworksModel();

        rootAgregate.setModel(model);
        model.setRootAggregate(rootAgregate);

        rootAgregate.addPlace(a1p1);
        rootAgregate.addPlace(a1p2);
        rootAgregate.addPlace(a1p3);
        rootAgregate.addPlace(a1p4);
        rootAgregate.addPlace(a1p5);

        a1p1.setParentAggregate(rootAgregate);
        a1p2.setParentAggregate(rootAgregate);
        a1p3.setParentAggregate(rootAgregate);
        a1p4.setParentAggregate(rootAgregate);
        a1p5.setParentAggregate(rootAgregate);

        rootAgregate.addTransition(a1t1);
        rootAgregate.addTransition(a1f1);
        rootAgregate.addTransition(a1j1);

        a1t1.setParentAggregate(rootAgregate);
        a1f1.setParentAggregate(rootAgregate);
        a1j1.setParentAggregate(rootAgregate);

        PlaceStateChangedListener sysoutWriter = new PlaceStateChangedListener() {
            public void placeStateChanged(Place position) {
                System.out.println(position.getParentAggregate().getModel().getModelingTime() +
                                   " " + position.getName() + " state = " + position.getToken());
            }
        };

        a1p1.addStateChangeListener(sysoutWriter);
        a1p2.addStateChangeListener(sysoutWriter);
        a1p3.addStateChangeListener(sysoutWriter);
        a1p4.addStateChangeListener(sysoutWriter);
        a1p5.addStateChangeListener(sysoutWriter);
        model.setFinishTime(100D);
        model.start();
    }
}
