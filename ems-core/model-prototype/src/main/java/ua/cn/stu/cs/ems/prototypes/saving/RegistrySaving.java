package ua.cn.stu.cs.ems.prototypes.saving;

import java.io.File;
import java.io.IOException;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.aggregates.AggregateDefinition;
import ua.cn.stu.cs.ems.core.aggregates.AggregateDefinitionReference;
import ua.cn.stu.cs.ems.core.aggregates.AggregateRegistry;
import ua.cn.stu.cs.ems.core.aggregates.Input;
import ua.cn.stu.cs.ems.core.aggregates.InputImpl;
import ua.cn.stu.cs.ems.core.aggregates.Output;
import ua.cn.stu.cs.ems.core.aggregates.OutputImpl;
import ua.cn.stu.cs.ems.core.queues.FIFOQueue;
import ua.cn.stu.cs.ems.core.utils.Coordinates;
import ua.cn.stu.cs.ems.core.xml.AggregateRegistryWriter;

/**
 *
 * @author proger
 */
public class RegistrySaving {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        AggregateRegistry ar = new AggregateRegistry();
        AggregateDefinition ad1 = new AggregateDefinition("child");
        Input ic = new InputImpl("input");
        ad1.addInput(ic, 0);
        ic.setNameCoordinates(new Coordinates(1, 2));
        ic.setObjectCoordinates(new Coordinates(100, 200));

        Output oc = new OutputImpl("output");
        ad1.addOutput(oc, 0);
        oc.setNameCoordinates(new Coordinates(1, 2));
        oc.setObjectCoordinates(new Coordinates(100, 200));

        Place p = new Place("p");
        p.setNameCoordinates(new Coordinates(1, 2));
        p.setObjectCoordinates(new Coordinates(100, 200));

        ic.setOutputPlace(p);
        oc.connectInputPlace(p);

        ad1.addPlace(p);

        ar.addAggregateDefinition(ad1);

        AggregateDefinition ad2 = new AggregateDefinition("root");
        AggregateDefinitionReference adr = ad2.addChildAggreagateDefinition(ad1, "baby");
        adr.setNameCoordinates(new Coordinates(1, 2));
        adr.setObjectCoordinates(new Coordinates(100, 200));

        FIFOQueue fifo = new FIFOQueue("fifo");
        fifo.setNameCoordinates(new Coordinates(1, 2));
        fifo.setObjectCoordinates(new Coordinates(100, 200));
        ad2.addQueue(fifo);

        AggregateRegistryWriter arw = new AggregateRegistryWriter();

        ar.addAggregateDefinition(ad1);
        ar.addAggregateDefinition(ad2);

        File tmp = new File("/home/proger");
        tmp.mkdirs();
        arw.saveAggregateRegistry(ar, tmp);

    }

}
