package ua.cn.stu.cs.ems.experiments;

import java.util.List;
import ua.cn.stu.cs.ems.core.Place;
import ua.cn.stu.cs.ems.core.Token;
import ua.cn.stu.cs.ems.core.aggregates.Aggregate;
import ua.cn.stu.cs.ems.core.aggregates.AggregateInstance;
import ua.cn.stu.cs.ems.core.experiment.*;
import ua.cn.stu.cs.ems.core.experiment.statistics.StatisticsParameters;
import ua.cn.stu.cs.ems.core.models.ENetworksModel;
import ua.cn.stu.cs.ems.core.queues.FIFOQueue;
import ua.cn.stu.cs.ems.core.transitions.FTransition;
import ua.cn.stu.cs.ems.core.transitions.JTransition;
import ua.cn.stu.cs.ems.core.transitions.TTransition;
import ua.cn.stu.cs.ems.core.transitions.functions.InterpretedDelayFunction;
import ua.cn.stu.cs.ems.ecli.Value;

/**
 * 
 * @author Dmitry Krivenko <dmitrykrivenko@gmal.com>
 */
public class ExperimentPrototypeSMO {

    public static void main(String[] args) {
        TTransition t0 = new TTransition("T0");
        TTransition t1 = new TTransition("T1");

        FTransition f0 = new FTransition("F0");
        JTransition j0 = new JTransition("J0");

        FIFOQueue qf0 = new FIFOQueue("QF0");

        Place p0 = new Place("P0");
        Place p1 = new Place("P1");
        Place p2 = new Place("P2");
        Place p3 = new Place("P3");
        Place p4 = new Place("P4");
        Place p5 = new Place("P5");

        f0.setInputPlace(p0);
        f0.addOutputPlace(p1, 0);
        f0.addOutputPlace(p2, 1);
        f0.setDelayFunction(new InterpretedDelayFunction("RETURN EXPONENTIAL(V['t1']); "));

        qf0.connectInputPlace(p1);
        qf0.connectOutputPlace(p3);

        j0.addInputPlace(p3, 0);
        j0.addInputPlace(p4, 1);
        j0.setOutputPlace(p5);
        j0.setDelayFunction(new InterpretedDelayFunction("RETURN EXPONENTIAL(10); "));

        t0.setInputPlace(p5);
        t0.setOutputPlace(p4);

        t1.setInputPlace(p2);
        t1.setOutputPlace(p0);

        p2.setToken(new Token());
        p4.setToken(new Token());

        Aggregate root = createAggregateInstance();
        root.addPlaces(p0, p1, p2, p3, p4, p5);
        root.addTransitions(t0, t1, f0, j0);
        root.addQueues(qf0);
        root.setVariable("t1", new Value(1));

        final ENetworksModel model = new ENetworksModel();

        model.setRootAggregate(root);
        model.setFinishTime(1000D);

        FactorImpl factor = new FactorImpl(root.getVariable("t1"), new StepValueGenerator(12.5, 55, 7.5));
        factor.setFactorName("Intensity");

        Respond respond = new Respond(StatisticsParameters.AVERAGE_QUEUE_LENGTH.getPresentation());

        CountDownExperiment cde = new CountDownExperiment(model, factor, respond, 5);

        //cde.setCollectedStatOnEvent(true);
       // cde.setIsPrimaryStatisticsShouldBeCollected(true);
       // cde.collectStatisticsOn(qf0);
        cde.collectSecondaryStatisticsOn(qf0);

        cde.startExperiment();

        ExperimentReport er = cde.getReport();

        List<SecondaryStatisticsElement> sec = er.getSecondaryStatistics();
        for (SecondaryStatisticsElement el : sec) {
            log(el);
        }
    }

    private static AggregateInstance createAggregateInstance() {
        return new AggregateInstance("A");
    }

    private static void log(Object o) {
        System.out.println(o);
    }
}
